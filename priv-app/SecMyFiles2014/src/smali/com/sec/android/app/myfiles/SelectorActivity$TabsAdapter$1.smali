.class Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$1;
.super Ljava/lang/Object;
.source "SelectorActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->createSelectOptionClickListener()Landroid/widget/AdapterView$OnItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;)V
    .locals 0

    .prologue
    .line 1191
    iput-object p1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$1;->this$1:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1195
    .local p1, "list":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectionLock:Ljava/lang/Object;
    invoke-static {}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$800()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 1196
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$1;->this$1:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectOptionsIds:[I
    invoke-static {v0}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$1000(Lcom/sec/android/app/myfiles/SelectorActivity;)[I

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$1;->this$1:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectOptionsIds:[I
    invoke-static {v0}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$1000(Lcom/sec/android/app/myfiles/SelectorActivity;)[I

    move-result-object v0

    array-length v0, v0

    add-int/lit8 v2, p3, 0x1

    if-lt v0, v2, :cond_0

    .line 1197
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$1;->this$1:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectOptionsIds:[I
    invoke-static {v0}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$1000(Lcom/sec/android/app/myfiles/SelectorActivity;)[I

    move-result-object v0

    aget v0, v0, p3

    if-nez v0, :cond_1

    .line 1198
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$1;->this$1:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p2}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 1203
    :cond_0
    :goto_0
    monitor-exit v1

    .line 1204
    return-void

    .line 1200
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$1;->this$1:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mUnselectAllClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p2}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0

    .line 1203
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
