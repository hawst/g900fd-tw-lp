.class public Lcom/sec/android/app/myfiles/SortCursor$SortEntry;
.super Ljava/lang/Object;
.source "SortCursor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/SortCursor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SortEntry"
.end annotation


# instance fields
.field public isDirectory:Z

.field public name:Ljava/lang/String;

.field public order:I

.field public size:J

.field public time:J

.field public type:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->name:Ljava/lang/String;

    return-object v0
.end method
