.class Lcom/sec/android/app/myfiles/fragment/HistoryFragment$6;
.super Ljava/lang/Object;
.source "HistoryFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->onCreateItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;)V
    .locals 0

    .prologue
    .line 841
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .local p1, "parentView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 846
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->isSearchMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 850
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/MainActivity;->finishSearchMode()V

    .line 854
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->isSelectMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 856
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    invoke-virtual {v0, v1, p3, v2}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->startSelectMode(III)V

    move v0, v1

    .line 864
    :goto_0
    return v0

    :cond_1
    move v0, v2

    goto :goto_0
.end method
