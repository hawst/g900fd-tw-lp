.class public Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter;
.super Lcom/sec/android/app/myfiles/adapter/AbsCategoryBrowserAdapter;
.source "AudioBrowserAdapter.java"


# instance fields
.field private previousListener:Landroid/view/View$OnHoverListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "c"    # Landroid/database/Cursor;
    .param p4, "flags"    # I
    .param p5, "navigation"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .prologue
    .line 50
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/myfiles/adapter/AbsCategoryBrowserAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter;->previousListener:Landroid/view/View$OnHoverListener;

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter;->mContext:Landroid/content/Context;

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-static {v0, v1}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    new-instance v0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 58
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter;)Landroid/view/View$OnHoverListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter;->previousListener:Landroid/view/View$OnHoverListener;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter;Landroid/view/View$OnHoverListener;)Landroid/view/View$OnHoverListener;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter;
    .param p1, "x1"    # Landroid/view/View$OnHoverListener;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter;->previousListener:Landroid/view/View$OnHoverListener;

    return-object p1
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 11
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v10, 0x0

    .line 63
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/adapter/AbsCategoryBrowserAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 65
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;

    .line 67
    .local v7, "vh":Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;
    iget v8, p0, Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter;->mDisplayMode:I

    if-nez v8, :cond_9

    .line 69
    const-string v8, "_data"

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 71
    .local v1, "filePath":Ljava/lang/String;
    const/16 v8, 0x2f

    invoke-virtual {v1, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 73
    .local v0, "fileName":Ljava/lang/String;
    iget-object v8, v7, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    if-eqz v8, :cond_0

    .line 74
    iget-object v8, v7, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 76
    :cond_0
    iget-object v8, v7, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v8, :cond_1

    .line 77
    iget-object v8, v7, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 79
    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 82
    iget-object v8, p0, Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowFileExtensionStatus()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 84
    iget-object v8, v7, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    :cond_2
    :goto_0
    iget-object v8, v7, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v8, :cond_3

    .line 104
    iget-object v8, v7, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v8, v10}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 105
    invoke-virtual {p0, v7, v1}, Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter;->loadThumbnail(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;Ljava/lang/String;)V

    .line 109
    :cond_3
    const-string v8, "_data"

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 110
    .local v4, "hoverFilePath":Ljava/lang/String;
    invoke-static {v4, p2}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;Landroid/content/Context;)I

    move-result v3

    .line 111
    .local v3, "fileType":I
    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    .line 113
    .local v2, "filePosition":I
    new-instance v5, Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter$1;

    invoke-direct {v5, p0, v3, v4, v2}, Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter$1;-><init>(Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter;ILjava/lang/String;I)V

    .line 199
    .local v5, "hoverListener":Landroid/view/View$OnHoverListener;
    iget-object v8, p0, Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter;->mContext:Landroid/content/Context;

    const-string v9, "com.sec.feature.hovering_ui"

    invoke-static {v8, v9}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 202
    iget v8, p0, Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter;->mViewMode:I

    if-eqz v8, :cond_4

    iget v8, p0, Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter;->mViewMode:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_8

    .line 204
    :cond_4
    iget-object v8, v7, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    if-eqz v8, :cond_5

    .line 205
    iget-object v8, v7, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v8, v5}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 220
    .end local v0    # "fileName":Ljava/lang/String;
    .end local v1    # "filePath":Ljava/lang/String;
    .end local v2    # "filePosition":I
    .end local v3    # "fileType":I
    .end local v4    # "hoverFilePath":Ljava/lang/String;
    .end local v5    # "hoverListener":Landroid/view/View$OnHoverListener;
    :cond_5
    :goto_1
    return-void

    .line 88
    .restart local v0    # "fileName":Ljava/lang/String;
    .restart local v1    # "filePath":Ljava/lang/String;
    :cond_6
    const/16 v8, 0x2e

    invoke-virtual {v0, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v6

    .line 91
    .local v6, "lastDot":I
    if-lez v6, :cond_7

    .line 93
    iget-object v8, v7, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    const/4 v9, 0x0

    invoke-virtual {v0, v9, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 97
    :cond_7
    iget-object v8, v7, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 208
    .end local v6    # "lastDot":I
    .restart local v2    # "filePosition":I
    .restart local v3    # "fileType":I
    .restart local v4    # "hoverFilePath":Ljava/lang/String;
    .restart local v5    # "hoverListener":Landroid/view/View$OnHoverListener;
    :cond_8
    iget-object v8, v7, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v8, :cond_5

    .line 209
    iget-object v8, v7, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v8, v5}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    goto :goto_1

    .line 218
    .end local v0    # "fileName":Ljava/lang/String;
    .end local v1    # "filePath":Ljava/lang/String;
    .end local v2    # "filePosition":I
    .end local v3    # "fileType":I
    .end local v4    # "hoverFilePath":Ljava/lang/String;
    .end local v5    # "hoverListener":Landroid/view/View$OnHoverListener;
    :cond_9
    iget-object v8, v7, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const v9, 0x7f0200af

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method

.method protected getContentType()I
    .locals 1

    .prologue
    .line 340
    const/4 v0, 0x4

    return v0
.end method
