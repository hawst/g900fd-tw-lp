.class public Lcom/sec/android/app/myfiles/utils/MediaLoader;
.super Ljava/lang/Object;
.source "MediaLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/utils/MediaLoader$1;,
        Lcom/sec/android/app/myfiles/utils/MediaLoader$GolfDecoder;,
        Lcom/sec/android/app/myfiles/utils/MediaLoader$ConcreteProgressListener;,
        Lcom/sec/android/app/myfiles/utils/MediaLoader$SizedThumbnailBitmap;
    }
.end annotation


# static fields
.field private static final GET_THUMBNAIL_TIME:I = 0xe4e1c0

.field public static final GOLF_TEMP_PATH:Ljava/lang/String;

.field public static final GOLF_TEMP_VIDEO_PATH:Ljava/lang/String;

.field private static final MAX_NUM_PIXELS_MICRO_THUMBNAIL:I = 0x4b00

.field private static final MAX_NUM_PIXELS_THUMBNAIL:I = 0x30000

.field private static final MICRO_HEIGHT:I = 0x8c

.field private static final MICRO_WIDTH:I = 0xbe

.field private static final MODULE:Ljava/lang/String; = "MediaLoader"

.field public static final TARGET_SIZE_MICRO_THUMBNAIL:I = 0x60

.field public static final TARGET_SIZE_MINI_THUMBNAIL:I = 0x140

.field private static final UNCONSTRAINED:I = -0x1

.field private static final mThumbnailKeyList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sThumbnailCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 72
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/MediaLoader;->sThumbnailCache:Ljava/util/HashMap;

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/MediaLoader;->mThumbnailKeyList:Ljava/util/ArrayList;

    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/.thumbnails/golf/tmp"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/utils/MediaLoader;->GOLF_TEMP_PATH:Ljava/lang/String;

    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Golf/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/utils/MediaLoader;->GOLF_TEMP_VIDEO_PATH:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1457
    return-void
.end method

.method private static CheckNCreateImageThumbnail(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "CachePath"    # Ljava/lang/String;

    .prologue
    .line 523
    const/4 v1, 0x0

    .line 525
    .local v1, "retBmp":Landroid/graphics/Bitmap;
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 527
    const/4 v2, 0x3

    invoke-static {p1, v2}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->createImageThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 529
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->getExifOrientation(Ljava/lang/String;)I

    move-result v0

    .line 531
    .local v0, "degree":I
    if-eqz v0, :cond_0

    .line 533
    invoke-static {v1, v0}, Lcom/sec/android/app/myfiles/utils/BitmapUtils;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 548
    :cond_0
    :goto_0
    if-nez v1, :cond_1

    .line 550
    const/4 v2, 0x0

    const-string v3, "MediaLoader"

    const-string v4, "CheckNCreateImageThumbnail: retBmp is null."

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 553
    :cond_1
    return-object v1

    .line 538
    .end local v0    # "degree":I
    :cond_2
    const/4 v2, 0x1

    invoke-static {p1, v2}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->createImageThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 540
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->getExifOrientation(Ljava/lang/String;)I

    move-result v0

    .line 542
    .restart local v0    # "degree":I
    if-eqz v0, :cond_0

    .line 544
    invoke-static {v1, v0}, Lcom/sec/android/app/myfiles/utils/BitmapUtils;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0
.end method

.method public static CreateThumbnails(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3
    .param p0, "golfFilePath"    # Ljava/lang/String;

    .prologue
    .line 1434
    sget-object v1, Lcom/sec/android/app/myfiles/utils/MediaLoader;->GOLF_TEMP_PATH:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v1, p0, v2}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->getJpgTempFilePath(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 1435
    .local v0, "tempFilePath":Ljava/lang/String;
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method public static calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I
    .locals 6
    .param p0, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p1, "reqWidth"    # I
    .param p2, "reqHeight"    # I

    .prologue
    .line 289
    iget v2, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 290
    .local v2, "height":I
    iget v4, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 291
    .local v4, "width":I
    const/4 v3, 0x1

    .line 293
    .local v3, "inSampleSize":I
    if-gt v2, p2, :cond_0

    if-le v4, p1, :cond_1

    .line 295
    :cond_0
    div-int/lit8 v0, v2, 0x2

    .line 296
    .local v0, "halfHeight":I
    div-int/lit8 v1, v4, 0x2

    .line 302
    .local v1, "halfWidth":I
    :goto_0
    div-int v5, v0, v3

    if-le v5, p2, :cond_1

    div-int v5, v1, v3

    if-le v5, p1, :cond_1

    .line 303
    mul-int/lit8 v3, v3, 0x2

    goto :goto_0

    .line 307
    .end local v0    # "halfHeight":I
    .end local v1    # "halfWidth":I
    :cond_1
    return v3
.end method

.method public static changeThumbnailCache(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "srcPath"    # Ljava/lang/String;
    .param p1, "dstPath"    # Ljava/lang/String;

    .prologue
    .line 585
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->isThumbnailExist(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 587
    sget-object v1, Lcom/sec/android/app/myfiles/utils/MediaLoader;->sThumbnailCache:Ljava/util/HashMap;

    monitor-enter v1

    .line 589
    :try_start_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/MediaLoader;->mThumbnailKeyList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 591
    sget-object v0, Lcom/sec/android/app/myfiles/utils/MediaLoader;->sThumbnailCache:Ljava/util/HashMap;

    sget-object v2, Lcom/sec/android/app/myfiles/utils/MediaLoader;->sThumbnailCache:Ljava/util/HashMap;

    invoke-virtual {v2, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 593
    sget-object v0, Lcom/sec/android/app/myfiles/utils/MediaLoader;->sThumbnailCache:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 595
    sget-object v0, Lcom/sec/android/app/myfiles/utils/MediaLoader;->mThumbnailKeyList:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 596
    monitor-exit v1

    .line 598
    :cond_0
    return-void

    .line 596
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static clearThumbnailCache()V
    .locals 2

    .prologue
    .line 572
    sget-object v0, Lcom/sec/android/app/myfiles/utils/MediaLoader;->sThumbnailCache:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/myfiles/utils/MediaLoader;->mThumbnailKeyList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 573
    sget-object v1, Lcom/sec/android/app/myfiles/utils/MediaLoader;->sThumbnailCache:Ljava/util/HashMap;

    monitor-enter v1

    .line 575
    :try_start_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/MediaLoader;->sThumbnailCache:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 577
    sget-object v0, Lcom/sec/android/app/myfiles/utils/MediaLoader;->mThumbnailKeyList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 579
    monitor-exit v1

    .line 581
    :cond_0
    return-void

    .line 579
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static computeInitialSampleSize(Landroid/graphics/BitmapFactory$Options;II)I
    .locals 13
    .param p0, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p1, "minSideLength"    # I
    .param p2, "maxNumOfPixels"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v12, -0x1

    .line 496
    iget v7, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-double v4, v7

    .line 497
    .local v4, "w":D
    iget v7, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-double v0, v7

    .line 499
    .local v0, "h":D
    if-ne p2, v12, :cond_1

    move v2, v6

    .line 501
    .local v2, "lowerBound":I
    :goto_0
    if-ne p1, v12, :cond_2

    const/16 v3, 0x80

    .line 505
    .local v3, "upperBound":I
    :goto_1
    if-ge v3, v2, :cond_3

    .line 516
    .end local v2    # "lowerBound":I
    :cond_0
    :goto_2
    return v2

    .line 499
    .end local v3    # "upperBound":I
    :cond_1
    mul-double v8, v4, v0

    int-to-double v10, p2

    div-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v2, v8

    goto :goto_0

    .line 501
    .restart local v2    # "lowerBound":I
    :cond_2
    int-to-double v8, p1

    div-double v8, v4, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->floor(D)D

    move-result-wide v8

    int-to-double v10, p1

    div-double v10, v0, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->floor(D)D

    move-result-wide v10

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->min(DD)D

    move-result-wide v8

    double-to-int v3, v8

    goto :goto_1

    .line 510
    .restart local v3    # "upperBound":I
    :cond_3
    if-ne p2, v12, :cond_4

    if-ne p1, v12, :cond_4

    move v2, v6

    .line 512
    goto :goto_2

    .line 513
    :cond_4
    if-eq p1, v12, :cond_0

    move v2, v3

    .line 516
    goto :goto_2
.end method

.method public static computeSampleSize(Landroid/graphics/BitmapFactory$Options;I)I
    .locals 7
    .param p0, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p1, "target"    # I

    .prologue
    const/4 v5, 0x1

    .line 1139
    iget v4, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 1141
    .local v4, "w":I
    iget v3, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 1144
    .local v3, "h":I
    div-int v2, v4, p1

    .line 1146
    .local v2, "candidateW":I
    div-int v1, v3, p1

    .line 1148
    .local v1, "candidateH":I
    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1151
    .local v0, "candidate":I
    if-nez v0, :cond_0

    .line 1172
    :goto_0
    return v5

    .line 1156
    :cond_0
    if-le v0, v5, :cond_1

    .line 1158
    if-le v4, p1, :cond_1

    div-int v6, v4, v0

    if-ge v6, p1, :cond_1

    .line 1160
    add-int/lit8 v0, v0, -0x1

    .line 1164
    :cond_1
    if-le v0, v5, :cond_2

    .line 1166
    if-le v3, p1, :cond_2

    div-int v5, v3, v0

    if-ge v5, p1, :cond_2

    .line 1168
    add-int/lit8 v0, v0, -0x1

    :cond_2
    move v5, v0

    .line 1172
    goto :goto_0
.end method

.method private static computeSampleSize(Landroid/graphics/BitmapFactory$Options;II)I
    .locals 3
    .param p0, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p1, "minSideLength"    # I
    .param p2, "maxNumOfPixels"    # I

    .prologue
    .line 477
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->computeInitialSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v0

    .line 481
    .local v0, "initialSize":I
    const/16 v2, 0x8

    if-gt v0, v2, :cond_0

    .line 482
    const/4 v1, 0x1

    .line 483
    .local v1, "roundedSize":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 484
    shl-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 487
    .end local v1    # "roundedSize":I
    :cond_0
    add-int/lit8 v2, v0, 0x7

    div-int/lit8 v2, v2, 0x8

    mul-int/lit8 v1, v2, 0x8

    .line 490
    .restart local v1    # "roundedSize":I
    :cond_1
    return v1
.end method

.method public static createImageThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    .locals 17
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "kind"    # I

    .prologue
    .line 341
    const/4 v13, 0x1

    move/from16 v0, p1

    if-ne v0, v13, :cond_4

    const/4 v12, 0x1

    .line 342
    .local v12, "wantMini":Z
    :goto_0
    if-eqz v12, :cond_5

    const/16 v11, 0x140

    .line 345
    .local v11, "targetSize":I
    :goto_1
    if-eqz v12, :cond_6

    const/high16 v5, 0x30000

    .line 348
    .local v5, "maxPixels":I
    :goto_2
    new-instance v8, Lcom/sec/android/app/myfiles/utils/MediaLoader$SizedThumbnailBitmap;

    const/4 v13, 0x0

    invoke-direct {v8, v13}, Lcom/sec/android/app/myfiles/utils/MediaLoader$SizedThumbnailBitmap;-><init>(Lcom/sec/android/app/myfiles/utils/MediaLoader$1;)V

    .line 349
    .local v8, "sizedThumbnailBitmap":Lcom/sec/android/app/myfiles/utils/MediaLoader$SizedThumbnailBitmap;
    const/4 v1, 0x0

    .line 350
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/myfiles/MediaFile;->getFileType(Ljava/lang/String;)Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;

    move-result-object v4

    .line 351
    .local v4, "fileType":Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;
    if-eqz v4, :cond_1

    iget v13, v4, Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;->fileType:I

    const/16 v14, 0x3d

    if-eq v13, v14, :cond_0

    iget v13, v4, Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;->fileType:I

    const/16 v14, 0x12c

    if-ne v13, v14, :cond_1

    .line 352
    :cond_0
    move-object/from16 v0, p0

    invoke-static {v0, v11, v5, v8}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->createThumbnailFromEXIF(Ljava/lang/String;IILcom/sec/android/app/myfiles/utils/MediaLoader$SizedThumbnailBitmap;)V

    .line 353
    iget-object v1, v8, Lcom/sec/android/app/myfiles/utils/MediaLoader$SizedThumbnailBitmap;->mBitmap:Landroid/graphics/Bitmap;

    .line 356
    :cond_1
    if-nez v1, :cond_8

    .line 357
    const/4 v9, 0x0

    .line 359
    .local v9, "stream":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v10, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 360
    .end local v9    # "stream":Ljava/io/FileInputStream;
    .local v10, "stream":Ljava/io/FileInputStream;
    :try_start_1
    invoke-virtual {v10}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v3

    .line 361
    .local v3, "fd":Ljava/io/FileDescriptor;
    new-instance v7, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v7}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 362
    .local v7, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v13, 0x1

    iput v13, v7, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 363
    const/4 v13, 0x1

    iput-boolean v13, v7, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 364
    const/4 v13, 0x0

    invoke-static {v3, v13, v7}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 365
    iget-boolean v13, v7, Landroid/graphics/BitmapFactory$Options;->mCancel:Z

    if-nez v13, :cond_2

    iget v13, v7, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    const/4 v14, -0x1

    if-eq v13, v14, :cond_2

    iget v13, v7, Landroid/graphics/BitmapFactory$Options;->outHeight:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 v14, -0x1

    if-ne v13, v14, :cond_7

    .line 367
    :cond_2
    const/4 v13, 0x0

    .line 382
    if-eqz v10, :cond_3

    .line 383
    :try_start_2
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 398
    .end local v3    # "fd":Ljava/io/FileDescriptor;
    .end local v7    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v10    # "stream":Ljava/io/FileInputStream;
    :cond_3
    :goto_3
    return-object v13

    .line 341
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .end local v4    # "fileType":Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;
    .end local v5    # "maxPixels":I
    .end local v8    # "sizedThumbnailBitmap":Lcom/sec/android/app/myfiles/utils/MediaLoader$SizedThumbnailBitmap;
    .end local v11    # "targetSize":I
    .end local v12    # "wantMini":Z
    :cond_4
    const/4 v12, 0x0

    goto :goto_0

    .line 342
    .restart local v12    # "wantMini":Z
    :cond_5
    const/16 v11, 0x60

    goto :goto_1

    .line 345
    .restart local v11    # "targetSize":I
    :cond_6
    const/16 v5, 0x4b00

    goto :goto_2

    .line 385
    .restart local v1    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v3    # "fd":Ljava/io/FileDescriptor;
    .restart local v4    # "fileType":Lcom/sec/android/app/myfiles/MediaFile$MediaFileType;
    .restart local v5    # "maxPixels":I
    .restart local v7    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v8    # "sizedThumbnailBitmap":Lcom/sec/android/app/myfiles/utils/MediaLoader$SizedThumbnailBitmap;
    .restart local v10    # "stream":Ljava/io/FileInputStream;
    :catch_0
    move-exception v2

    .line 386
    .local v2, "ex":Ljava/io/IOException;
    const/4 v14, 0x0

    const-string v15, "MediaLoader"

    const-string v16, ""

    move-object/from16 v0, v16

    invoke-static {v14, v15, v0, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    .line 369
    .end local v2    # "ex":Ljava/io/IOException;
    :cond_7
    :try_start_3
    invoke-static {v7, v11, v5}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->computeSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v13

    iput v13, v7, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 371
    const/4 v13, 0x0

    iput-boolean v13, v7, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 373
    const/4 v13, 0x0

    iput-boolean v13, v7, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 374
    sget-object v13, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v13, v7, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 375
    const/4 v13, 0x0

    invoke-static {v3, v13, v7}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v1

    .line 382
    if-eqz v10, :cond_8

    .line 383
    :try_start_4
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 392
    .end local v3    # "fd":Ljava/io/FileDescriptor;
    .end local v7    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v10    # "stream":Ljava/io/FileInputStream;
    :cond_8
    :goto_4
    const/4 v13, 0x3

    move/from16 v0, p1

    if-ne v0, v13, :cond_9

    .line 394
    const/16 v13, 0x60

    const/16 v14, 0x60

    const/4 v15, 0x2

    invoke-static {v1, v13, v14, v15}, Landroid/media/ThumbnailUtils;->extractThumbnail(Landroid/graphics/Bitmap;III)Landroid/graphics/Bitmap;

    move-result-object v1

    :cond_9
    move-object v13, v1

    .line 398
    goto :goto_3

    .line 385
    .restart local v3    # "fd":Ljava/io/FileDescriptor;
    .restart local v7    # "options":Landroid/graphics/BitmapFactory$Options;
    .restart local v10    # "stream":Ljava/io/FileInputStream;
    :catch_1
    move-exception v2

    .line 386
    .restart local v2    # "ex":Ljava/io/IOException;
    const/4 v13, 0x0

    const-string v14, "MediaLoader"

    const-string v15, ""

    invoke-static {v13, v14, v15, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 376
    .end local v2    # "ex":Ljava/io/IOException;
    .end local v3    # "fd":Ljava/io/FileDescriptor;
    .end local v7    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v10    # "stream":Ljava/io/FileInputStream;
    .restart local v9    # "stream":Ljava/io/FileInputStream;
    :catch_2
    move-exception v2

    .line 377
    .restart local v2    # "ex":Ljava/io/IOException;
    :goto_5
    const/4 v13, 0x0

    :try_start_5
    const-string v14, "MediaLoader"

    const-string v15, ""

    invoke-static {v13, v14, v15, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 382
    if-eqz v9, :cond_8

    .line 383
    :try_start_6
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_4

    .line 385
    :catch_3
    move-exception v2

    .line 386
    const/4 v13, 0x0

    const-string v14, "MediaLoader"

    const-string v15, ""

    invoke-static {v13, v14, v15, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 378
    .end local v2    # "ex":Ljava/io/IOException;
    :catch_4
    move-exception v6

    .line 379
    .local v6, "oom":Ljava/lang/OutOfMemoryError;
    :goto_6
    const/4 v13, 0x0

    :try_start_7
    const-string v14, "MediaLoader"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Unable to decode file "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ". OutOfMemoryError."

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 382
    if-eqz v9, :cond_8

    .line 383
    :try_start_8
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    goto :goto_4

    .line 385
    :catch_5
    move-exception v2

    .line 386
    .restart local v2    # "ex":Ljava/io/IOException;
    const/4 v13, 0x0

    const-string v14, "MediaLoader"

    const-string v15, ""

    invoke-static {v13, v14, v15, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 381
    .end local v2    # "ex":Ljava/io/IOException;
    .end local v6    # "oom":Ljava/lang/OutOfMemoryError;
    :catchall_0
    move-exception v13

    .line 382
    :goto_7
    if-eqz v9, :cond_a

    .line 383
    :try_start_9
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 387
    :cond_a
    :goto_8
    throw v13

    .line 385
    :catch_6
    move-exception v2

    .line 386
    .restart local v2    # "ex":Ljava/io/IOException;
    const/4 v14, 0x0

    const-string v15, "MediaLoader"

    const-string v16, ""

    move-object/from16 v0, v16

    invoke-static {v14, v15, v0, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_8

    .line 381
    .end local v2    # "ex":Ljava/io/IOException;
    .end local v9    # "stream":Ljava/io/FileInputStream;
    .restart local v10    # "stream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v13

    move-object v9, v10

    .end local v10    # "stream":Ljava/io/FileInputStream;
    .restart local v9    # "stream":Ljava/io/FileInputStream;
    goto :goto_7

    .line 378
    .end local v9    # "stream":Ljava/io/FileInputStream;
    .restart local v10    # "stream":Ljava/io/FileInputStream;
    :catch_7
    move-exception v6

    move-object v9, v10

    .end local v10    # "stream":Ljava/io/FileInputStream;
    .restart local v9    # "stream":Ljava/io/FileInputStream;
    goto :goto_6

    .line 376
    .end local v9    # "stream":Ljava/io/FileInputStream;
    .restart local v10    # "stream":Ljava/io/FileInputStream;
    :catch_8
    move-exception v2

    move-object v9, v10

    .end local v10    # "stream":Ljava/io/FileInputStream;
    .restart local v9    # "stream":Ljava/io/FileInputStream;
    goto :goto_5
.end method

.method private static createThumbnailFromEXIF(Ljava/lang/String;IILcom/sec/android/app/myfiles/utils/MediaLoader$SizedThumbnailBitmap;)V
    .locals 14
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "targetSize"    # I
    .param p2, "maxPixels"    # I
    .param p3, "sizedThumbBitmap"    # Lcom/sec/android/app/myfiles/utils/MediaLoader$SizedThumbnailBitmap;

    .prologue
    .line 424
    if-nez p0, :cond_1

    .line 472
    :cond_0
    :goto_0
    return-void

    .line 426
    :cond_1
    const/4 v2, 0x0

    .line 427
    .local v2, "exif":Landroid/media/ExifInterface;
    const/4 v9, 0x0

    .line 429
    .local v9, "thumbData":[B
    :try_start_0
    new-instance v3, Landroid/media/ExifInterface;

    invoke-direct {v3, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 430
    .end local v2    # "exif":Landroid/media/ExifInterface;
    .local v3, "exif":Landroid/media/ExifInterface;
    if-eqz v3, :cond_2

    .line 431
    :try_start_1
    invoke-virtual {v3}, Landroid/media/ExifInterface;->getThumbnail()[B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v9

    :cond_2
    move-object v2, v3

    .line 437
    .end local v3    # "exif":Landroid/media/ExifInterface;
    .restart local v2    # "exif":Landroid/media/ExifInterface;
    :goto_1
    new-instance v6, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v6}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 438
    .local v6, "fullOptions":Landroid/graphics/BitmapFactory$Options;
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 439
    .local v4, "exifOptions":Landroid/graphics/BitmapFactory$Options;
    const/4 v5, 0x0

    .line 440
    .local v5, "exifThumbWidth":I
    const/4 v7, 0x0

    .line 443
    .local v7, "fullThumbWidth":I
    if-eqz v9, :cond_3

    .line 444
    const/4 v11, 0x1

    iput-boolean v11, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 445
    const/4 v11, 0x0

    array-length v12, v9

    invoke-static {v9, v11, v12, v4}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 446
    move/from16 v0, p2

    invoke-static {v4, p1, v0}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->computeSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v11

    iput v11, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 447
    iget v11, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v12, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    div-int v5, v11, v12

    .line 451
    :cond_3
    const/4 v11, 0x1

    iput-boolean v11, v6, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 452
    invoke-static {p0, v6}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 453
    move/from16 v0, p2

    invoke-static {v6, p1, v0}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->computeSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v11

    iput v11, v6, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 454
    iget v11, v6, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v12, v6, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    div-int v7, v11, v12

    .line 457
    if-eqz v9, :cond_4

    if-lt v5, v7, :cond_4

    .line 458
    iget v10, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 459
    .local v10, "width":I
    iget v8, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 460
    .local v8, "height":I
    const/4 v11, 0x0

    iput-boolean v11, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 461
    const/4 v11, 0x0

    array-length v12, v9

    invoke-static {v9, v11, v12, v4}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v11

    move-object/from16 v0, p3

    iput-object v11, v0, Lcom/sec/android/app/myfiles/utils/MediaLoader$SizedThumbnailBitmap;->mBitmap:Landroid/graphics/Bitmap;

    .line 463
    move-object/from16 v0, p3

    iget-object v11, v0, Lcom/sec/android/app/myfiles/utils/MediaLoader$SizedThumbnailBitmap;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v11, :cond_0

    .line 464
    move-object/from16 v0, p3

    iput-object v9, v0, Lcom/sec/android/app/myfiles/utils/MediaLoader$SizedThumbnailBitmap;->mThumbnailData:[B

    .line 465
    move-object/from16 v0, p3

    iput v10, v0, Lcom/sec/android/app/myfiles/utils/MediaLoader$SizedThumbnailBitmap;->mThumbnailWidth:I

    .line 466
    move-object/from16 v0, p3

    iput v8, v0, Lcom/sec/android/app/myfiles/utils/MediaLoader$SizedThumbnailBitmap;->mThumbnailHeight:I

    goto :goto_0

    .line 433
    .end local v4    # "exifOptions":Landroid/graphics/BitmapFactory$Options;
    .end local v5    # "exifThumbWidth":I
    .end local v6    # "fullOptions":Landroid/graphics/BitmapFactory$Options;
    .end local v7    # "fullThumbWidth":I
    .end local v8    # "height":I
    .end local v10    # "width":I
    :catch_0
    move-exception v1

    .line 434
    .local v1, "ex":Ljava/io/IOException;
    :goto_2
    const/4 v11, 0x0

    const-string v12, "MediaLoader"

    const-string v13, ""

    invoke-static {v11, v12, v13, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 469
    .end local v1    # "ex":Ljava/io/IOException;
    .restart local v4    # "exifOptions":Landroid/graphics/BitmapFactory$Options;
    .restart local v5    # "exifThumbWidth":I
    .restart local v6    # "fullOptions":Landroid/graphics/BitmapFactory$Options;
    .restart local v7    # "fullThumbWidth":I
    :cond_4
    const/4 v11, 0x0

    iput-boolean v11, v6, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 470
    invoke-static {p0, v6}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v11

    move-object/from16 v0, p3

    iput-object v11, v0, Lcom/sec/android/app/myfiles/utils/MediaLoader$SizedThumbnailBitmap;->mBitmap:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 433
    .end local v2    # "exif":Landroid/media/ExifInterface;
    .end local v4    # "exifOptions":Landroid/graphics/BitmapFactory$Options;
    .end local v5    # "exifThumbWidth":I
    .end local v6    # "fullOptions":Landroid/graphics/BitmapFactory$Options;
    .end local v7    # "fullThumbWidth":I
    .restart local v3    # "exif":Landroid/media/ExifInterface;
    :catch_1
    move-exception v1

    move-object v2, v3

    .end local v3    # "exif":Landroid/media/ExifInterface;
    .restart local v2    # "exif":Landroid/media/ExifInterface;
    goto :goto_2
.end method

.method public static deleteImageBitmap(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 84
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 86
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    if-eqz v0, :cond_0

    .line 90
    :try_start_0
    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_data=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 93
    :catch_0
    move-exception v1

    .line 95
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static deleteVideoBitmap(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 103
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 105
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    if-eqz v0, :cond_0

    .line 109
    :try_start_0
    sget-object v2, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_data=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    :cond_0
    :goto_0
    return-void

    .line 112
    :catch_0
    move-exception v1

    .line 114
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static getAlbumartBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumID"    # I

    .prologue
    const/4 v12, 0x0

    const/4 v10, 0x0

    .line 1288
    const/4 v1, 0x0

    .line 1290
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    new-instance v6, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v6}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1292
    .local v6, "sBitmapOptions":Landroid/graphics/BitmapFactory$Options;
    sget-object v11, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v11, v6, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 1294
    iput-boolean v12, v6, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 1296
    const-string v11, "content://media/external/audio/albumart"

    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1298
    .local v0, "artworkUri":Landroid/net/Uri;
    const/16 v9, 0x80

    .line 1300
    .local v9, "width":I
    const/16 v5, 0x80

    .line 1302
    .local v5, "height":I
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1304
    const/16 v9, 0x100

    .line 1306
    const/16 v5, 0x100

    .line 1309
    :cond_0
    const/4 v4, 0x0

    .line 1313
    .local v4, "fileDescriptor":Landroid/os/ParcelFileDescriptor;
    int-to-long v12, p1

    :try_start_0
    invoke-static {v0, v12, v13}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v8

    .line 1315
    .local v8, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    const-string v12, "r"

    invoke-virtual {v11, v8, v12}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 1317
    if-nez v4, :cond_2

    .line 1375
    if-eqz v4, :cond_1

    .line 1381
    :try_start_1
    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1388
    :goto_0
    if-nez v1, :cond_1

    .line 1395
    .end local v8    # "uri":Landroid/net/Uri;
    :cond_1
    :goto_1
    return-object v10

    .line 1322
    .restart local v8    # "uri":Landroid/net/Uri;
    :cond_2
    const/4 v7, 0x1

    .line 1324
    .local v7, "sampleSize":I
    const/4 v10, 0x1

    :try_start_2
    iput-boolean v10, v6, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1326
    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v10

    const/4 v11, 0x0

    invoke-static {v10, v11, v6}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 1328
    iget v10, v6, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-gt v10, v5, :cond_3

    iget v10, v6, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-le v10, v9, :cond_4

    .line 1330
    :cond_3
    iget v10, v6, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v11, v6, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-le v10, v11, :cond_8

    .line 1332
    iget v10, v6, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-float v10, v10

    int-to-float v11, v5

    div-float/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 1340
    :cond_4
    :goto_2
    iput v7, v6, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 1342
    const/4 v10, 0x0

    iput-boolean v10, v6, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1344
    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v10

    const/4 v11, 0x0

    invoke-static {v10, v11, v6}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1347
    .local v2, "bm":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_6

    .line 1349
    iget v10, v6, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-ne v10, v9, :cond_5

    iget v10, v6, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-eq v10, v5, :cond_9

    .line 1351
    :cond_5
    const/4 v10, 0x1

    invoke-static {v2, v9, v5, v10}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1353
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1375
    :cond_6
    :goto_3
    if-eqz v4, :cond_7

    .line 1381
    :try_start_3
    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 1388
    :goto_4
    if-nez v1, :cond_7

    .end local v2    # "bm":Landroid/graphics/Bitmap;
    .end local v7    # "sampleSize":I
    .end local v8    # "uri":Landroid/net/Uri;
    :cond_7
    :goto_5
    move-object v10, v1

    .line 1395
    goto :goto_1

    .line 1336
    .restart local v7    # "sampleSize":I
    .restart local v8    # "uri":Landroid/net/Uri;
    :cond_8
    :try_start_4
    iget v10, v6, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v10, v10

    int-to-float v11, v9

    div-float/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Math;->round(F)I
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v7

    goto :goto_2

    .line 1357
    .restart local v2    # "bm":Landroid/graphics/Bitmap;
    :cond_9
    move-object v1, v2

    goto :goto_3

    .line 1363
    .end local v2    # "bm":Landroid/graphics/Bitmap;
    .end local v7    # "sampleSize":I
    .end local v8    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v10

    .line 1375
    if-eqz v4, :cond_7

    .line 1381
    :try_start_5
    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 1388
    :goto_6
    if-nez v1, :cond_7

    goto :goto_5

    .line 1367
    :catch_1
    move-exception v3

    .line 1371
    .local v3, "e":Ljava/lang/OutOfMemoryError;
    :try_start_6
    throw v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 1375
    .end local v3    # "e":Ljava/lang/OutOfMemoryError;
    :catchall_0
    move-exception v10

    if-eqz v4, :cond_a

    .line 1381
    :try_start_7
    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 1388
    :goto_7
    if-nez v1, :cond_a

    :cond_a
    throw v10

    .line 1383
    .restart local v8    # "uri":Landroid/net/Uri;
    :catch_2
    move-exception v11

    goto :goto_0

    .restart local v2    # "bm":Landroid/graphics/Bitmap;
    .restart local v7    # "sampleSize":I
    :catch_3
    move-exception v10

    goto :goto_4

    .end local v2    # "bm":Landroid/graphics/Bitmap;
    .end local v7    # "sampleSize":I
    .end local v8    # "uri":Landroid/net/Uri;
    :catch_4
    move-exception v10

    goto :goto_6

    :catch_5
    move-exception v11

    goto :goto_7
.end method

.method public static getAlbumartBitmap(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 1254
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1255
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    const/4 v7, 0x0

    .line 1256
    .local v7, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isMediaScannerScannig()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1258
    const/4 v8, 0x0

    .line 1259
    .local v8, "c":Landroid/database/Cursor;
    const/4 v6, -0x1

    .line 1262
    .local v6, "albumId":I
    :try_start_0
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_data=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1264
    if-eqz v8, :cond_1

    .line 1266
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1267
    const-string v1, "album_id"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 1270
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1273
    :cond_1
    const/4 v1, -0x1

    if-eq v6, v1, :cond_2

    .line 1274
    invoke-static {p0, v6}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->getAlbumartBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 1284
    .end local v6    # "albumId":I
    .end local v8    # "c":Landroid/database/Cursor;
    :cond_2
    :goto_0
    return-object v7

    .line 1276
    .restart local v6    # "albumId":I
    .restart local v8    # "c":Landroid/database/Cursor;
    :catch_0
    move-exception v9

    .line 1277
    .local v9, "e":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    .line 1278
    if-eqz v8, :cond_2

    .line 1279
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public static getApkBitmap(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 1064
    const/4 v3, 0x0

    .line 1066
    .local v3, "retBmp":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, p1, v5}, Landroid/content/pm/PackageManager;->getPackageArchiveInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 1068
    .local v2, "mPkgInfo":Landroid/content/pm/PackageInfo;
    if-eqz v2, :cond_0

    .line 1072
    :try_start_0
    iget-object v4, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-static {p0, v4, p1}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->getApkDrawable(Landroid/content/Context;Landroid/content/pm/ApplicationInfo;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1074
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    .line 1076
    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->getResBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 1091
    .end local v0    # "d":Landroid/graphics/drawable/Drawable;
    :cond_0
    :goto_0
    return-object v3

    .line 1079
    :catch_0
    move-exception v1

    .line 1081
    .local v1, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 1083
    const/4 v4, 0x0

    const-string v5, "MediaLoader"

    const-string v6, "OutOfMemoryError occurs when get the bitmap of apk"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1085
    .end local v1    # "e":Ljava/lang/OutOfMemoryError;
    :catch_1
    move-exception v1

    .line 1087
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static getApkDrawable(Landroid/content/Context;Landroid/content/pm/ApplicationInfo;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appInfo"    # Landroid/content/pm/ApplicationInfo;
    .param p2, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 1097
    const/4 v0, 0x0

    .line 1099
    .local v0, "icon":Landroid/graphics/drawable/Drawable;
    iput-object p2, p1, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    .line 1101
    iput-object p2, p1, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    .line 1103
    iget v1, p1, Landroid/content/pm/ApplicationInfo;->icon:I

    if-eqz v1, :cond_0

    .line 1105
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1108
    :cond_0
    return-object v0
.end method

.method public static getDocBitmap(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/16 v11, 0x60

    const/4 v2, 0x0

    .line 243
    const-string v0, "content://com.samsung.docthumbnail.thumbnailprovider/thumbnailpath"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 245
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v3, p1

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 247
    .local v10, "thumbnailCursor":Landroid/database/Cursor;
    const-string v8, ""

    .line 248
    .local v8, "path":Ljava/lang/String;
    if-eqz v10, :cond_1

    .line 249
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250
    const-string v0, "thumbnail_path"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 255
    const-string v0, "thumbnail_path"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 259
    :cond_0
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 261
    :cond_1
    const/4 v9, 0x0

    .line 262
    .local v9, "ret":Landroid/graphics/Bitmap;
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 263
    .local v6, "bitmapFile":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 265
    new-instance v7, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v7}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 266
    .local v7, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v0, 0x1

    iput-boolean v0, v7, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 267
    invoke-static {v8, v7}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 270
    invoke-static {v7, v11, v11}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v0

    iput v0, v7, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 273
    const/4 v0, 0x0

    iput-boolean v0, v7, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 274
    invoke-static {v8, v7}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 277
    .end local v7    # "options":Landroid/graphics/BitmapFactory$Options;
    :cond_2
    if-eqz v10, :cond_3

    .line 279
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 282
    :cond_3
    return-object v9
.end method

.method public static getDropboxImageBitmap(Landroid/content/Context;ILjava/lang/String;)Landroid/graphics/Bitmap;
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "CategoryType"    # I
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x5

    const/4 v4, 0x0

    const/16 v12, 0x20

    const/16 v11, 0x1f

    .line 602
    const/4 v9, 0x0

    .line 604
    .local v9, "retBmp":Landroid/graphics/Bitmap;
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 606
    :cond_0
    const/4 v2, 0x0

    .line 658
    :goto_0
    return-object v2

    .line 609
    :cond_1
    invoke-static {p2}, Lcom/sec/android/app/myfiles/utils/Utils;->getFileThumbnailFolderName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 611
    .local v10, "serverFolderName":Ljava/lang/String;
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 613
    .local v8, "ThumbnailCacheFolder":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_2

    .line 615
    invoke-virtual {v8}, Ljava/io/File;->mkdirs()Z

    .line 619
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "/"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "large.jpg"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 621
    .local v7, "ThumbnailCacheFilePath":Ljava/lang/String;
    const/4 v6, 0x0

    .line 622
    .local v6, "BaiduAPIHelper":Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;
    const/4 v0, 0x0

    .line 624
    .local v0, "DBAPIHelper":Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;
    if-eq p1, v11, :cond_3

    if-ne p1, v12, :cond_6

    .line 625
    :cond_3
    invoke-static {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    move-result-object v6

    .line 629
    :goto_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 631
    .local v1, "ThumbnailCacheFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 635
    invoke-static {p0, v7}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->CheckNCreateImageThumbnail(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 637
    if-nez v9, :cond_5

    .line 639
    if-eq p1, v11, :cond_4

    if-ne p1, v12, :cond_7

    .line 640
    :cond_4
    new-instance v2, Lcom/sec/android/app/myfiles/utils/MediaLoader$ConcreteProgressListener;

    invoke-direct {v2}, Lcom/sec/android/app/myfiles/utils/MediaLoader$ConcreteProgressListener;-><init>()V

    invoke-virtual {v6, v1, p2, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->getThumbnail(Ljava/io/File;Ljava/lang/String;Lcom/samsung/scloud/response/ProgressListener;)V

    .line 644
    :goto_2
    invoke-static {p0, v7}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->CheckNCreateImageThumbnail(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v9

    :cond_5
    :goto_3
    move-object v2, v9

    .line 658
    goto :goto_0

    .line 627
    .end local v1    # "ThumbnailCacheFile":Ljava/io/File;
    :cond_6
    invoke-static {p0}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    move-result-object v0

    goto :goto_1

    .line 642
    .restart local v1    # "ThumbnailCacheFile":Ljava/io/File;
    :cond_7
    new-instance v5, Lcom/sec/android/app/myfiles/utils/MediaLoader$ConcreteProgressListener;

    invoke-direct {v5}, Lcom/sec/android/app/myfiles/utils/MediaLoader$ConcreteProgressListener;-><init>()V

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->getFileThumbnail(Ljava/io/File;Ljava/lang/String;IILcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;

    goto :goto_2

    .line 649
    :cond_8
    if-eq p1, v11, :cond_9

    if-ne p1, v12, :cond_a

    .line 650
    :cond_9
    new-instance v2, Lcom/sec/android/app/myfiles/utils/MediaLoader$ConcreteProgressListener;

    invoke-direct {v2}, Lcom/sec/android/app/myfiles/utils/MediaLoader$ConcreteProgressListener;-><init>()V

    invoke-virtual {v6, v1, p2, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->getThumbnail(Ljava/io/File;Ljava/lang/String;Lcom/samsung/scloud/response/ProgressListener;)V

    .line 654
    :goto_4
    invoke-static {p0, v7}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->CheckNCreateImageThumbnail(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v9

    goto :goto_3

    .line 652
    :cond_a
    new-instance v5, Lcom/sec/android/app/myfiles/utils/MediaLoader$ConcreteProgressListener;

    invoke-direct {v5}, Lcom/sec/android/app/myfiles/utils/MediaLoader$ConcreteProgressListener;-><init>()V

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->getFileThumbnail(Ljava/io/File;Ljava/lang/String;IILcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;

    goto :goto_4
.end method

.method public static getDropboxVideoBitmap(Landroid/content/Context;ILjava/lang/String;)Landroid/graphics/Bitmap;
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "CategoryType"    # I
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x5

    const/4 v4, 0x0

    const/16 v12, 0x20

    const/16 v11, 0x1f

    .line 664
    const/4 v7, 0x0

    .line 666
    .local v7, "retBmp":Landroid/graphics/Bitmap;
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 668
    :cond_0
    const/4 v2, 0x0

    .line 718
    :goto_0
    return-object v2

    .line 671
    :cond_1
    const/4 v6, 0x0

    .line 672
    .local v6, "BaiduAPIHelper":Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;
    const/4 v0, 0x0

    .line 674
    .local v0, "DBAPIHelper":Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;
    if-eq p1, v11, :cond_2

    if-ne p1, v12, :cond_6

    .line 675
    :cond_2
    invoke-static {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    move-result-object v6

    .line 679
    :goto_1
    invoke-static {p2}, Lcom/sec/android/app/myfiles/utils/Utils;->getFileThumbnailFolderName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 681
    .local v8, "videoServerFolderName":Ljava/lang/String;
    new-instance v10, Ljava/io/File;

    invoke-direct {v10, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 683
    .local v10, "videoThumbnailCacheFolder":Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_3

    .line 685
    invoke-virtual {v10}, Ljava/io/File;->mkdirs()Z

    .line 689
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "/"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "large.jpg"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 691
    .local v9, "videoThumbnailCacheFilePath":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 693
    .local v1, "ThumbnailCacheFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 695
    invoke-static {p0, v9}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->CheckNCreateImageThumbnail(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 696
    if-nez v7, :cond_5

    .line 698
    if-eq p1, v11, :cond_4

    if-ne p1, v12, :cond_7

    .line 699
    :cond_4
    new-instance v2, Lcom/sec/android/app/myfiles/utils/MediaLoader$ConcreteProgressListener;

    invoke-direct {v2}, Lcom/sec/android/app/myfiles/utils/MediaLoader$ConcreteProgressListener;-><init>()V

    invoke-virtual {v6, v1, p2, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->getThumbnail(Ljava/io/File;Ljava/lang/String;Lcom/samsung/scloud/response/ProgressListener;)V

    .line 703
    :goto_2
    invoke-static {p0, v9}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->CheckNCreateImageThumbnail(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v7

    :cond_5
    :goto_3
    move-object v2, v7

    .line 718
    goto :goto_0

    .line 677
    .end local v1    # "ThumbnailCacheFile":Ljava/io/File;
    .end local v8    # "videoServerFolderName":Ljava/lang/String;
    .end local v9    # "videoThumbnailCacheFilePath":Ljava/lang/String;
    .end local v10    # "videoThumbnailCacheFolder":Ljava/io/File;
    :cond_6
    invoke-static {p0}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    move-result-object v0

    goto :goto_1

    .line 701
    .restart local v1    # "ThumbnailCacheFile":Ljava/io/File;
    .restart local v8    # "videoServerFolderName":Ljava/lang/String;
    .restart local v9    # "videoThumbnailCacheFilePath":Ljava/lang/String;
    .restart local v10    # "videoThumbnailCacheFolder":Ljava/io/File;
    :cond_7
    new-instance v5, Lcom/sec/android/app/myfiles/utils/MediaLoader$ConcreteProgressListener;

    invoke-direct {v5}, Lcom/sec/android/app/myfiles/utils/MediaLoader$ConcreteProgressListener;-><init>()V

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->getFileThumbnail(Ljava/io/File;Ljava/lang/String;IILcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;

    goto :goto_2

    .line 709
    :cond_8
    if-eq p1, v11, :cond_9

    if-ne p1, v12, :cond_a

    .line 710
    :cond_9
    new-instance v2, Lcom/sec/android/app/myfiles/utils/MediaLoader$ConcreteProgressListener;

    invoke-direct {v2}, Lcom/sec/android/app/myfiles/utils/MediaLoader$ConcreteProgressListener;-><init>()V

    invoke-virtual {v6, v1, p2, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->getThumbnail(Ljava/io/File;Ljava/lang/String;Lcom/samsung/scloud/response/ProgressListener;)V

    .line 714
    :goto_4
    invoke-static {p0, v9}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->CheckNCreateImageThumbnail(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v7

    goto :goto_3

    .line 712
    :cond_a
    new-instance v5, Lcom/sec/android/app/myfiles/utils/MediaLoader$ConcreteProgressListener;

    invoke-direct {v5}, Lcom/sec/android/app/myfiles/utils/MediaLoader$ConcreteProgressListener;-><init>()V

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->getFileThumbnail(Ljava/io/File;Ljava/lang/String;IILcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;

    goto :goto_4
.end method

.method public static getExifOrientation(Ljava/lang/String;)I
    .locals 9
    .param p0, "filepath"    # Ljava/lang/String;

    .prologue
    const/4 v8, -0x1

    .line 1210
    const/4 v0, 0x0

    .line 1212
    .local v0, "degree":I
    const/4 v2, 0x0

    .line 1216
    .local v2, "exif":Landroid/media/ExifInterface;
    :try_start_0
    new-instance v3, Landroid/media/ExifInterface;

    invoke-direct {v3, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "exif":Landroid/media/ExifInterface;
    .local v3, "exif":Landroid/media/ExifInterface;
    move-object v2, v3

    .line 1223
    .end local v3    # "exif":Landroid/media/ExifInterface;
    .restart local v2    # "exif":Landroid/media/ExifInterface;
    :goto_0
    if-eqz v2, :cond_0

    .line 1225
    const-string v5, "Orientation"

    invoke-virtual {v2, v5, v8}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v4

    .line 1227
    .local v4, "orientation":I
    if-eq v4, v8, :cond_0

    .line 1230
    packed-switch v4, :pswitch_data_0

    .line 1250
    .end local v4    # "orientation":I
    :cond_0
    :goto_1
    :pswitch_0
    return v0

    .line 1218
    :catch_0
    move-exception v1

    .line 1220
    .local v1, "ex":Ljava/io/IOException;
    const/4 v5, 0x0

    const-string v6, "MediaLoader"

    const-string v7, "cannot read exif"

    invoke-static {v5, v6, v7, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 1233
    .end local v1    # "ex":Ljava/io/IOException;
    .restart local v4    # "orientation":I
    :pswitch_1
    const/16 v0, 0x5a

    .line 1234
    goto :goto_1

    .line 1237
    :pswitch_2
    const/16 v0, 0xb4

    .line 1238
    goto :goto_1

    .line 1241
    :pswitch_3
    const/16 v0, 0x10e

    .line 1242
    goto :goto_1

    .line 1230
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getHomeShortcutBitmap(Landroid/content/Context;IILjava/lang/String;Z)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "path"    # Ljava/lang/String;
    .param p4, "isFile"    # Z

    .prologue
    .line 1011
    const/4 v5, 0x0

    .line 1019
    .local v5, "shortcutIcon":Landroid/graphics/Bitmap;
    const/high16 v4, 0x3f800000    # 1.0f

    .line 1021
    .local v4, "scaleRatio":F
    if-eqz p4, :cond_2

    .line 1023
    invoke-static {p3}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->getThumbnailDrawableWithoutMakeCache(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 1025
    .local v0, "icon":Landroid/graphics/drawable/BitmapDrawable;
    if-nez v0, :cond_1

    .line 1027
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-static {p3}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v7

    invoke-static {p3, p0, v7}, Lcom/sec/android/app/myfiles/MediaFile;->getLargeIcon(Ljava/lang/String;Landroid/content/Context;I)I

    move-result v7

    invoke-static {v6, v7}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 1034
    :goto_0
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 1036
    .local v3, "originWidth":I
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 1038
    .local v2, "originHeight":I
    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1040
    .local v1, "longerLineLength":I
    int-to-float v6, p1

    int-to-float v7, v1

    div-float v4, v6, v7

    .line 1047
    if-eq p1, v1, :cond_0

    .line 1049
    int-to-float v6, v3

    mul-float/2addr v6, v4

    float-to-int v6, v6

    int-to-float v7, v2

    mul-float/2addr v7, v4

    float-to-int v7, v7

    const/4 v8, 0x1

    invoke-static {v5, v6, v7, v8}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 1058
    .end local v0    # "icon":Landroid/graphics/drawable/BitmapDrawable;
    .end local v1    # "longerLineLength":I
    .end local v2    # "originHeight":I
    .end local v3    # "originWidth":I
    :cond_0
    :goto_1
    return-object v5

    .line 1031
    .restart local v0    # "icon":Landroid/graphics/drawable/BitmapDrawable;
    :cond_1
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v5

    goto :goto_0

    .line 1055
    .end local v0    # "icon":Landroid/graphics/drawable/BitmapDrawable;
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f020032

    invoke-static {v6, v7}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    goto :goto_1
.end method

.method public static getImageBitmap(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 23
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 724
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 726
    .local v2, "contentResolver":Landroid/content/ContentResolver;
    const/16 v18, 0x0

    .line 728
    .local v18, "retBmp":Landroid/graphics/Bitmap;
    new-instance v19, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 730
    .local v19, "sBitmapOptions":Landroid/graphics/BitmapFactory$Options;
    const/16 v20, 0x4

    .line 732
    .local v20, "sampleSize":I
    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 734
    const/4 v3, 0x0

    move-object/from16 v0, v19

    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 736
    if-eqz v2, :cond_2

    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isMediaScannerScannig()Z

    move-result v3

    if-nez v3, :cond_2

    .line 738
    const/4 v9, 0x0

    .line 743
    .local v9, "c":Landroid/database/Cursor;
    :try_start_0
    sget-object v3, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const-string v5, "_data= ? COLLATE LOCALIZED"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 746
    if-eqz v9, :cond_2

    .line 748
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 750
    const-string v3, "_id"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 752
    .local v13, "index":I
    invoke-interface {v9, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 755
    .local v14, "id":J
    const/4 v8, 0x3

    .line 757
    .local v8, "Thumnail_kind":I
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v3

    if-nez v3, :cond_5

    .line 759
    const/4 v8, 0x3

    .line 768
    :goto_0
    :try_start_1
    move-object/from16 v0, v19

    invoke-static {v2, v14, v15, v8, v0}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v18

    .line 776
    :goto_1
    if-eqz v18, :cond_1

    .line 780
    :try_start_2
    const-string v3, "orientation"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 782
    .local v16, "index2":I
    move/from16 v0, v16

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 784
    .local v10, "degree":I
    if-nez v10, :cond_0

    .line 786
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->getExifOrientation(Ljava/lang/String;)I

    move-result v10

    .line 789
    :cond_0
    if-eqz v10, :cond_1

    .line 791
    move-object/from16 v0, v18

    invoke-static {v0, v10}, Lcom/sec/android/app/myfiles/utils/BitmapUtils;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v18

    .line 796
    .end local v8    # "Thumnail_kind":I
    .end local v10    # "degree":I
    .end local v13    # "index":I
    .end local v14    # "id":J
    .end local v16    # "index2":I
    :cond_1
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 810
    .end local v9    # "c":Landroid/database/Cursor;
    :cond_2
    :goto_2
    if-nez v18, :cond_4

    .line 813
    new-instance v17, Landroid/graphics/Matrix;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Matrix;-><init>()V

    .line 815
    .local v17, "m":Landroid/graphics/Matrix;
    const/16 v21, 0x0

    .line 819
    .local v21, "tempBmp":Landroid/graphics/Bitmap;
    :try_start_3
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    move-result v3

    if-nez v3, :cond_7

    .line 826
    :try_start_4
    const-string v3, ".golf"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 827
    new-instance v22, Ljava/io/File;

    sget-object v3, Lcom/sec/android/app/myfiles/utils/MediaLoader;->GOLF_TEMP_PATH:Ljava/lang/String;

    move-object/from16 v0, v22

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 828
    .local v22, "tmpDir":Ljava/io/File;
    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->mkdirs()Z

    .line 829
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->CreateThumbnails(Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    move-result-object v18

    .line 840
    .end local v22    # "tmpDir":Ljava/io/File;
    :goto_3
    :try_start_5
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->getExifOrientation(Ljava/lang/String;)I

    move-result v10

    .line 842
    .restart local v10    # "degree":I
    if-eqz v10, :cond_3

    .line 844
    move-object/from16 v0, v18

    invoke-static {v0, v10}, Lcom/sec/android/app/myfiles/utils/BitmapUtils;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v18

    .line 862
    :cond_3
    :goto_4
    if-nez v18, :cond_4

    .line 864
    const/4 v3, 0x0

    const-string v4, "MediaLoader"

    const-string v5, "getImageBitmap: retBmp is null."

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 889
    .end local v10    # "degree":I
    .end local v17    # "m":Landroid/graphics/Matrix;
    .end local v21    # "tempBmp":Landroid/graphics/Bitmap;
    :cond_4
    :goto_5
    return-object v18

    .line 763
    .restart local v8    # "Thumnail_kind":I
    .restart local v9    # "c":Landroid/database/Cursor;
    .restart local v13    # "index":I
    .restart local v14    # "id":J
    :cond_5
    const/4 v8, 0x1

    goto :goto_0

    .line 771
    :catch_0
    move-exception v12

    .line 773
    .local v12, "ex":Ljava/lang/OutOfMemoryError;
    :try_start_6
    invoke-virtual {v12}, Ljava/lang/OutOfMemoryError;->printStackTrace()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_1

    .line 799
    .end local v8    # "Thumnail_kind":I
    .end local v12    # "ex":Ljava/lang/OutOfMemoryError;
    .end local v13    # "index":I
    .end local v14    # "id":J
    :catch_1
    move-exception v11

    .line 801
    .local v11, "e":Ljava/lang/Exception;
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V

    .line 803
    if-eqz v9, :cond_2

    .line 805
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 832
    .end local v9    # "c":Landroid/database/Cursor;
    .end local v11    # "e":Ljava/lang/Exception;
    .restart local v17    # "m":Landroid/graphics/Matrix;
    .restart local v21    # "tempBmp":Landroid/graphics/Bitmap;
    :cond_6
    const/4 v3, 0x3

    :try_start_7
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->createImageThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    :try_end_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    move-result-object v18

    goto :goto_3

    .line 835
    :catch_2
    move-exception v12

    .line 837
    .restart local v12    # "ex":Ljava/lang/OutOfMemoryError;
    :try_start_8
    invoke-virtual {v12}, Ljava/lang/OutOfMemoryError;->printStackTrace()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3

    goto :goto_3

    .line 871
    .end local v12    # "ex":Ljava/lang/OutOfMemoryError;
    :catch_3
    move-exception v11

    .line 878
    .restart local v11    # "e":Ljava/lang/Exception;
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    .line 851
    .end local v11    # "e":Ljava/lang/Exception;
    :cond_7
    const/4 v3, 0x1

    :try_start_9
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->createImageThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v18

    .line 853
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->getExifOrientation(Ljava/lang/String;)I

    move-result v10

    .line 855
    .restart local v10    # "degree":I
    if-eqz v10, :cond_3

    .line 857
    move-object/from16 v0, v18

    invoke-static {v0, v10}, Lcom/sec/android/app/myfiles/utils/BitmapUtils;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3

    move-result-object v18

    goto :goto_4
.end method

.method public static getJpgTempFilePath(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 7
    .param p0, "folder"    # Ljava/lang/String;
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "allFrame"    # Z

    .prologue
    .line 1440
    :try_start_0
    const-string v6, ".golf"

    invoke-virtual {p1, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    .line 1441
    .local v3, "indexEnd":I
    const-string v6, "/"

    invoke-virtual {p1, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    .line 1442
    .local v2, "indexBegin":I
    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 1443
    .local v1, "fileName":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1444
    .local v4, "strBuild":Ljava/lang/StringBuilder;
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1445
    const-string v6, "_"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1446
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1447
    .local v5, "tempPath":Ljava/lang/String;
    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1448
    const-string v6, ".jpg"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1449
    invoke-static {v5, p1, p2}, Lcom/sec/android/app/myfiles/utils/MediaLoader$GolfDecoder;->generateGolfFile(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1450
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 1454
    .end local v1    # "fileName":Ljava/lang/String;
    .end local v2    # "indexBegin":I
    .end local v3    # "indexEnd":I
    .end local v4    # "strBuild":Ljava/lang/StringBuilder;
    .end local v5    # "tempPath":Ljava/lang/String;
    :goto_0
    return-object v6

    .line 1451
    :catch_0
    move-exception v0

    .line 1452
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1454
    const/4 v6, 0x0

    goto :goto_0
.end method

.method private static getResBitmap(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "d"    # Landroid/graphics/drawable/Drawable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/OutOfMemoryError;
        }
    .end annotation

    .prologue
    const/16 v6, 0x95

    const/4 v5, 0x0

    .line 1114
    const/4 v0, 0x0

    .line 1121
    .local v0, "bmp":Landroid/graphics/Bitmap;
    const/16 v3, 0x96

    .line 1123
    .local v3, "w":I
    const/16 v2, 0x96

    .line 1125
    .local v2, "h":I
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v2, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1127
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1129
    .local v1, "c":Landroid/graphics/Canvas;
    invoke-virtual {p0, v5, v5, v6, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1131
    invoke-virtual {p0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1133
    return-object v0
.end method

.method public static getSampleSizeBitmap(ILjava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4
    .param p0, "targetWidthHeight"    # I
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 1178
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1182
    .local v2, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v3, 0x1

    iput-boolean v3, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1184
    invoke-static {p1, v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1186
    .local v0, "bm":Landroid/graphics/Bitmap;
    const/4 v3, 0x0

    iput-boolean v3, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 1188
    const/4 v3, -0x1

    if-eq p0, v3, :cond_0

    .line 1190
    invoke-static {v2, p0}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->computeSampleSize(Landroid/graphics/BitmapFactory$Options;I)I

    move-result v3

    iput v3, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 1193
    :cond_0
    invoke-static {p1, v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1195
    const/4 v1, 0x0

    .line 1197
    .local v1, "degree":I
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->getExifOrientation(Ljava/lang/String;)I

    move-result v1

    .line 1199
    if-eqz v1, :cond_1

    .line 1201
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/utils/BitmapUtils;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1204
    :cond_1
    return-object v0
.end method

.method public static getThumbnailDrawableWithMakeCache(Landroid/content/Context;IILjava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "CategoryType"    # I
    .param p2, "fileType"    # I
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 140
    const/4 v7, 0x2

    const-string v8, "MediaLoader"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getThumbnailDrawableWithMakeCache : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 142
    const/4 v6, 0x0

    .line 144
    .local v6, "resultDrawable":Landroid/graphics/drawable/Drawable;
    const/4 v5, 0x0

    .line 146
    .local v5, "resultBitmap":Landroid/graphics/Bitmap;
    sget-object v8, Lcom/sec/android/app/myfiles/utils/MediaLoader;->sThumbnailCache:Ljava/util/HashMap;

    monitor-enter v8

    .line 148
    :try_start_0
    sget-object v7, Lcom/sec/android/app/myfiles/utils/MediaLoader;->sThumbnailCache:Ljava/util/HashMap;

    invoke-virtual {v7, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Landroid/graphics/drawable/Drawable;

    move-object v6, v0

    .line 149
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    if-nez v6, :cond_5

    .line 153
    invoke-static {p2}, Lcom/sec/android/app/myfiles/MediaFile;->isDrmFileType(I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 154
    invoke-static {p0, p3}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->getRealMimeTypeOfDRM(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 155
    .local v3, "mime":Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeForMimeType(Ljava/lang/String;)I

    move-result p2

    .line 158
    .end local v3    # "mime":Ljava/lang/String;
    :cond_0
    invoke-static {p2}, Lcom/sec/android/app/myfiles/MediaFile;->isImageFileType(I)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 160
    const/16 v7, 0x8

    if-eq p1, v7, :cond_1

    const/16 v7, 0x1f

    if-eq p1, v7, :cond_1

    const/16 v7, 0x14

    if-eq p1, v7, :cond_1

    const/16 v7, 0x20

    if-ne p1, v7, :cond_6

    .line 161
    :cond_1
    invoke-static {p0, p1, p3}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->getDropboxImageBitmap(Landroid/content/Context;ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 186
    :cond_2
    :goto_0
    if-eqz v5, :cond_11

    .line 188
    new-instance v6, Landroid/graphics/drawable/BitmapDrawable;

    .end local v6    # "resultDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-direct {v6, v7, v5}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 190
    .restart local v6    # "resultDrawable":Landroid/graphics/drawable/Drawable;
    sget-object v8, Lcom/sec/android/app/myfiles/utils/MediaLoader;->sThumbnailCache:Ljava/util/HashMap;

    monitor-enter v8

    .line 192
    :try_start_1
    sget-object v7, Lcom/sec/android/app/myfiles/utils/MediaLoader;->mThumbnailKeyList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    const/16 v9, 0x96

    if-le v7, v9, :cond_4

    .line 194
    const/4 v7, 0x0

    const-string v9, "MediaLoader"

    const-string v10, "FULL Thumbnail! Delete old thumbnails"

    invoke-static {v7, v9, v10}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 196
    const/4 v2, 0x0

    .line 200
    .local v2, "i":I
    :cond_3
    sget-object v7, Lcom/sec/android/app/myfiles/utils/MediaLoader;->sThumbnailCache:Ljava/util/HashMap;

    sget-object v9, Lcom/sec/android/app/myfiles/utils/MediaLoader;->mThumbnailKeyList:Ljava/util/ArrayList;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    sget-object v7, Lcom/sec/android/app/myfiles/utils/MediaLoader;->mThumbnailKeyList:Ljava/util/ArrayList;

    const/4 v9, 0x0

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 204
    add-int/lit8 v2, v2, 0x1

    .line 206
    const/16 v7, 0x32

    if-lt v2, v7, :cond_3

    .line 209
    .end local v2    # "i":I
    :cond_4
    sget-object v7, Lcom/sec/android/app/myfiles/utils/MediaLoader;->mThumbnailKeyList:Ljava/util/ArrayList;

    invoke-virtual {v7, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 211
    sget-object v7, Lcom/sec/android/app/myfiles/utils/MediaLoader;->sThumbnailCache:Ljava/util/HashMap;

    invoke-virtual {v7, p3, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 237
    :cond_5
    :goto_1
    return-object v6

    .line 149
    :catchall_0
    move-exception v7

    :try_start_2
    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v7

    .line 163
    :cond_6
    invoke-static {p0, p3}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->getImageBitmap(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v5

    goto :goto_0

    .line 166
    :cond_7
    invoke-static {p2}, Lcom/sec/android/app/myfiles/MediaFile;->isVideoFileType(I)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 168
    const/16 v7, 0x8

    if-eq p1, v7, :cond_8

    const/16 v7, 0x1f

    if-eq p1, v7, :cond_8

    const/16 v7, 0x14

    if-eq p1, v7, :cond_8

    const/16 v7, 0x20

    if-ne p1, v7, :cond_9

    .line 169
    :cond_8
    invoke-static {p0, p1, p3}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->getDropboxVideoBitmap(Landroid/content/Context;ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v5

    goto :goto_0

    .line 171
    :cond_9
    invoke-static {p0, p3}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->getVideoBitmap(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v5

    goto :goto_0

    .line 173
    :cond_a
    invoke-static {p2}, Lcom/sec/android/app/myfiles/MediaFile;->isAudioFileType(I)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 175
    const/16 v7, 0x8

    if-eq p1, v7, :cond_b

    const/16 v7, 0x1f

    if-ne p1, v7, :cond_c

    :cond_b
    const/16 v7, 0x20

    if-ne p1, v7, :cond_2

    .line 176
    :cond_c
    invoke-static {p0, p3}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->getAlbumartBitmap(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v5

    goto/16 :goto_0

    .line 178
    :cond_d
    invoke-static {p2}, Lcom/sec/android/app/myfiles/MediaFile;->isInstallFileType(I)Z

    move-result v7

    if-eqz v7, :cond_10

    .line 180
    const/16 v7, 0x8

    if-eq p1, v7, :cond_e

    const/16 v7, 0x1f

    if-ne p1, v7, :cond_f

    :cond_e
    const/16 v7, 0x20

    if-ne p1, v7, :cond_2

    .line 181
    :cond_f
    invoke-static {p0, p3}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->getApkBitmap(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v5

    goto/16 :goto_0

    .line 183
    :cond_10
    invoke-static {p2}, Lcom/sec/android/app/myfiles/MediaFile;->isDocumentFileType(I)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 184
    invoke-static {p0, p3}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->getDocBitmap(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v5

    goto/16 :goto_0

    .line 212
    :catchall_1
    move-exception v7

    :try_start_3
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v7

    .line 214
    :cond_11
    const/16 v7, 0x8

    if-eq p1, v7, :cond_5

    const/16 v7, 0x1f

    if-eq p1, v7, :cond_5

    const/16 v7, 0x14

    if-eq p1, v7, :cond_5

    const/16 v7, 0x20

    if-eq p1, v7, :cond_5

    .line 227
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 228
    .local v1, "fileName":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_5

    .line 230
    invoke-static {p3}, Lcom/sec/android/app/myfiles/MediaFile;->getSmallIcon(Ljava/lang/String;)I

    move-result v4

    .line 231
    .local v4, "resId":I
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-static {v7, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 232
    new-instance v6, Landroid/graphics/drawable/BitmapDrawable;

    .end local v6    # "resultDrawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-direct {v6, v7, v5}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .restart local v6    # "resultDrawable":Landroid/graphics/drawable/Drawable;
    goto/16 :goto_1
.end method

.method public static getThumbnailDrawableWithoutMakeCache(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 131
    sget-object v1, Lcom/sec/android/app/myfiles/utils/MediaLoader;->sThumbnailCache:Ljava/util/HashMap;

    monitor-enter v1

    .line 133
    :try_start_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/MediaLoader;->sThumbnailCache:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    monitor-exit v1

    return-object v0

    .line 134
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getVideoBitmap(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 17
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 929
    const/4 v8, 0x0

    .line 931
    .local v8, "b":Landroid/graphics/Bitmap;
    new-instance v15, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v15}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 933
    .local v15, "sBitmapOptions":Landroid/graphics/BitmapFactory$Options;
    const/16 v16, 0x4

    .line 935
    .local v16, "sampleSize":I
    move/from16 v0, v16

    iput v0, v15, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 937
    const/4 v3, 0x0

    iput-boolean v3, v15, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 939
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 941
    .local v2, "contentResolver":Landroid/content/ContentResolver;
    if-eqz v2, :cond_1

    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isMediaScannerScannig()Z

    move-result v3

    if-nez v3, :cond_1

    .line 943
    const/4 v9, 0x0

    .line 947
    .local v9, "c":Landroid/database/Cursor;
    :try_start_0
    sget-object v3, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_data=\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 950
    if-eqz v9, :cond_1

    .line 952
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 954
    const-string v3, "_id"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 956
    .local v14, "index":I
    invoke-interface {v9, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 958
    .local v12, "id":J
    const/4 v3, 0x3

    invoke-static {v2, v12, v13, v3, v15}, Landroid/provider/MediaStore$Video$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 962
    .end local v12    # "id":J
    .end local v14    # "index":I
    :cond_0
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 971
    .end local v9    # "c":Landroid/database/Cursor;
    :cond_1
    :goto_0
    if-nez v8, :cond_2

    .line 973
    new-instance v11, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v11, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 975
    .local v11, "file":Ljava/io/File;
    invoke-static {v11}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->getVideoThumbBitmap(Ljava/io/File;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 990
    .end local v11    # "file":Ljava/io/File;
    :cond_2
    return-object v8

    .line 965
    .restart local v9    # "c":Landroid/database/Cursor;
    :catch_0
    move-exception v10

    .line 967
    .local v10, "e":Ljava/lang/Exception;
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static getVideoBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 895
    const/4 v0, 0x0

    .line 916
    .local v0, "b":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 918
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 920
    .local v1, "file":Ljava/io/File;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->getVideoThumbBitmap(Ljava/io/File;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 923
    .end local v1    # "file":Ljava/io/File;
    :cond_0
    return-object v0
.end method

.method private static getVideoThumbBitmap(Ljava/io/File;)Landroid/graphics/Bitmap;
    .locals 4
    .param p0, "file"    # Ljava/io/File;

    .prologue
    .line 996
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Landroid/media/ThumbnailUtils;->createVideoThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 998
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 1000
    const/4 v1, 0x0

    .line 1005
    :goto_0
    return-object v1

    .line 1003
    :cond_0
    const/16 v1, 0xbe

    const/16 v2, 0x8c

    const/4 v3, 0x2

    invoke-static {v0, v1, v2, v3}, Landroid/media/ThumbnailUtils;->extractThumbnail(Landroid/graphics/Bitmap;III)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, v0

    .line 1005
    goto :goto_0
.end method

.method public static isThumbnailExist(Ljava/lang/String;)Z
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 122
    sget-object v1, Lcom/sec/android/app/myfiles/utils/MediaLoader;->sThumbnailCache:Ljava/util/HashMap;

    monitor-enter v1

    .line 124
    :try_start_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/MediaLoader;->mThumbnailKeyList:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 125
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static removeThumbnailCache(Ljava/lang/String;)V
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 559
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->isThumbnailExist(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 561
    sget-object v1, Lcom/sec/android/app/myfiles/utils/MediaLoader;->sThumbnailCache:Ljava/util/HashMap;

    monitor-enter v1

    .line 563
    :try_start_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/MediaLoader;->sThumbnailCache:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 565
    sget-object v0, Lcom/sec/android/app/myfiles/utils/MediaLoader;->mThumbnailKeyList:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 566
    monitor-exit v1

    .line 568
    :cond_0
    return-void

    .line 566
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public thumbBitmap(Ljava/lang/String;Ljava/lang/String;I)Landroid/graphics/Bitmap;
    .locals 9
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "mimeType"    # Ljava/lang/String;
    .param p3, "size"    # I

    .prologue
    const/4 v1, 0x0

    .line 1401
    invoke-static {p3, p1}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->getSampleSizeBitmap(ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1403
    .local v0, "b":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 1405
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 1407
    .local v5, "m":Landroid/graphics/Matrix;
    const/high16 v2, 0x3f800000    # 1.0f

    int-to-float v3, p3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v7

    .line 1409
    .local v7, "scale":F
    invoke-virtual {v5, v7, v7}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 1411
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 1413
    .local v8, "scaledBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1419
    .end local v5    # "m":Landroid/graphics/Matrix;
    .end local v7    # "scale":F
    .end local v8    # "scaledBitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-object v8

    :cond_0
    const/4 v8, 0x0

    goto :goto_0
.end method
