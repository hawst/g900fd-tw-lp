.class Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$3;
.super Ljava/lang/Object;
.source "DropboxAdapter.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

.field final synthetic val$filePath:Ljava/lang/String;

.field final synthetic val$format:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 366
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$3;->this$0:Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    iput p2, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$3;->val$format:I

    iput-object p3, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$3;->val$filePath:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 13
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    .line 371
    invoke-virtual {p2}, Landroid/view/DragEvent;->getAction()I

    move-result v0

    .line 373
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 495
    :cond_0
    :goto_0
    const/4 v11, 0x1

    :goto_1
    return v11

    .line 376
    :pswitch_0
    iget-object v11, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$3;->this$0:Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    iget-boolean v11, v11, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mIsDragMode:Z

    if-nez v11, :cond_0

    iget v11, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$3;->val$format:I

    const/16 v12, 0x3001

    if-ne v11, v12, :cond_0

    .line 378
    iget-object v11, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$3;->this$0:Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mSelectedItemBackground:I
    invoke-static {v11}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->access$300(Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;)I

    move-result v11

    invoke-virtual {p1, v11}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 383
    :pswitch_1
    iget-object v11, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$3;->this$0:Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    iget-boolean v11, v11, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mIsDragMode:Z

    if-nez v11, :cond_0

    iget v11, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$3;->val$format:I

    const/16 v12, 0x3001

    if-ne v11, v12, :cond_0

    .line 385
    const/4 v11, 0x0

    invoke-virtual {p1, v11}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 390
    :pswitch_2
    iget-object v11, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$3;->this$0:Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    iget-boolean v11, v11, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mIsDragMode:Z

    if-nez v11, :cond_9

    .line 392
    const/4 v11, 0x0

    invoke-virtual {p1, v11}, Landroid/view/View;->setBackgroundColor(I)V

    .line 394
    iget-object v11, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$3;->this$0:Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    iget-object v11, v11, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mDropListener:Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;

    if-eqz v11, :cond_8

    .line 396
    invoke-virtual {p2}, Landroid/view/DragEvent;->getClipData()Landroid/content/ClipData;

    move-result-object v1

    .line 398
    .local v1, "clipData":Landroid/content/ClipData;
    if-eqz v1, :cond_7

    invoke-virtual {v1}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v11

    if-eqz v11, :cond_7

    .line 400
    invoke-virtual {v1}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/ClipDescription;->getLabel()Ljava/lang/CharSequence;

    move-result-object v7

    .line 402
    .local v7, "label":Ljava/lang/CharSequence;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_5

    const-string v11, "selectedUri"

    invoke-virtual {v11, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 404
    invoke-virtual {v1}, Landroid/content/ClipData;->getItemCount()I

    move-result v5

    .line 406
    .local v5, "itemCount":I
    const/4 v9, 0x0

    .line 408
    .local v9, "selectedFilePath":Ljava/lang/String;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 410
    .local v8, "selectedFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    if-ge v3, v5, :cond_3

    .line 412
    invoke-virtual {v1, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v6

    .line 414
    .local v6, "itemUri":Landroid/content/ClipData$Item;
    if-eqz v6, :cond_1

    .line 416
    invoke-virtual {v6}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v10

    .line 418
    .local v10, "uri":Landroid/net/Uri;
    if-eqz v10, :cond_2

    .line 420
    iget-object v11, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$3;->this$0:Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;)Landroid/content/Context;

    move-result-object v11

    invoke-static {v11, v10}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getFilePathFromUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v9

    .line 430
    :goto_3
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_1

    .line 432
    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 410
    .end local v10    # "uri":Landroid/net/Uri;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 424
    .restart local v10    # "uri":Landroid/net/Uri;
    :cond_2
    invoke-virtual {v6}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v11

    invoke-interface {v11}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    goto :goto_3

    .line 437
    .end local v6    # "itemUri":Landroid/content/ClipData$Item;
    .end local v10    # "uri":Landroid/net/Uri;
    :cond_3
    const/4 v2, 0x0

    .line 439
    .local v2, "dstFolder":Ljava/lang/String;
    iget v11, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$3;->val$format:I

    const/16 v12, 0x3001

    if-ne v11, v12, :cond_4

    .line 441
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$3;->val$filePath:Ljava/lang/String;

    .line 448
    :goto_4
    iget-object v11, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$3;->this$0:Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    iget-object v11, v11, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mDropListener:Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;

    invoke-interface {v11, v2, v8}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;->onDrop(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 445
    :cond_4
    iget-object v11, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$3;->val$filePath:Ljava/lang/String;

    invoke-static {v11}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getParentFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    .line 453
    .end local v2    # "dstFolder":Ljava/lang/String;
    .end local v3    # "i":I
    .end local v5    # "itemCount":I
    .end local v8    # "selectedFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v9    # "selectedFilePath":Ljava/lang/String;
    :cond_5
    iget v11, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$3;->val$format:I

    const/16 v12, 0x3001

    if-ne v11, v12, :cond_6

    .line 454
    iget-object v11, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$3;->this$0:Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;)Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$3;->val$filePath:Ljava/lang/String;

    invoke-virtual {v11, v12}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setmPathForDropboxMultiDrag(Ljava/lang/String;)V

    .line 459
    :goto_5
    const/4 v11, 0x0

    goto/16 :goto_1

    .line 457
    :cond_6
    iget-object v11, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$3;->this$0:Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;)Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$3;->val$filePath:Ljava/lang/String;

    invoke-static {v12}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getParentFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setmPathForDropboxMultiDrag(Ljava/lang/String;)V

    goto :goto_5

    .line 465
    .end local v7    # "label":Ljava/lang/CharSequence;
    :cond_7
    const/4 v11, 0x0

    goto/16 :goto_1

    .line 472
    .end local v1    # "clipData":Landroid/content/ClipData;
    :cond_8
    const/4 v11, 0x0

    goto/16 :goto_1

    .line 477
    :cond_9
    new-instance v4, Landroid/content/Intent;

    const-string v11, "com.sec.android.action.FILE_OPERATION_DONE"

    invoke-direct {v4, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 479
    .local v4, "intent":Landroid/content/Intent;
    const-string v11, "success"

    const/4 v12, 0x0

    invoke-virtual {v4, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 481
    iget-object v11, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$3;->this$0:Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;)Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 488
    .end local v4    # "intent":Landroid/content/Intent;
    :pswitch_3
    iget-object v11, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$3;->this$0:Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    iget-boolean v11, v11, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->mIsDragMode:Z

    if-eqz v11, :cond_0

    .line 490
    iget-object v11, p0, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter$3;->this$0:Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->finishDrag()V

    goto/16 :goto_0

    .line 373
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
