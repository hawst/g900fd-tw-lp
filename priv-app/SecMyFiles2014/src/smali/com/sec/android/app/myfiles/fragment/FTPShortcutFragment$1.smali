.class Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment$1;
.super Ljava/lang/Object;
.source "FTPShortcutFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;->onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 10
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parentView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v9, 0x0

    .line 100
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v6}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 101
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v6

    invoke-interface {v6, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/Cursor;

    .line 102
    .local v1, "cursor":Landroid/database/Cursor;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Security;->getInstance()Lcom/sec/android/app/myfiles/utils/Security;

    move-result-object v6

    const-string v7, "_data"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/myfiles/utils/Security;->decrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 104
    .local v4, "ftpParams":Ljava/lang/String;
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    .line 105
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 106
    .local v0, "arg":Landroid/os/Bundle;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    .line 107
    .local v5, "initialArgument":Landroid/os/Bundle;
    if-eqz v5, :cond_1

    .line 108
    const-string v6, "CONTENT_TYPE"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 109
    .local v3, "filterMimetype":Ljava/lang/String;
    const-string v6, "CONTENT_EXTENSION"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 110
    .local v2, "filterExtension":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 111
    const-string v6, "CONTENT_EXTENSION"

    invoke-virtual {v0, v6, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :cond_0
    :goto_0
    const-string v6, "shortcut_working_index_intent_key"

    const-string v7, "shortcut_working_index_intent_key"

    invoke-virtual {v5, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 119
    const-string v6, "from_add_shortcut"

    const-string v7, "from_add_shortcut"

    invoke-virtual {v5, v7, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 122
    .end local v2    # "filterExtension":Ljava/lang/String;
    .end local v3    # "filterMimetype":Ljava/lang/String;
    :cond_1
    const-string v6, "id"

    const/16 v7, 0xa

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 123
    const-string v6, "run_from"

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;

    iget v7, v7, Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;->mRunFrom:I

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 124
    const-string v6, "FOLDERPATH"

    invoke-virtual {v0, v6, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    const-string v6, "SELECT_PATH_MODE"

    const/4 v7, 0x1

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 126
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v6, :cond_2

    .line 127
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v6, v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView(Landroid/os/Bundle;)V

    .line 133
    .end local v0    # "arg":Landroid/os/Bundle;
    .end local v1    # "cursor":Landroid/database/Cursor;
    .end local v4    # "ftpParams":Ljava/lang/String;
    .end local v5    # "initialArgument":Landroid/os/Bundle;
    :cond_2
    :goto_1
    return-void

    .line 113
    .restart local v0    # "arg":Landroid/os/Bundle;
    .restart local v1    # "cursor":Landroid/database/Cursor;
    .restart local v2    # "filterExtension":Ljava/lang/String;
    .restart local v3    # "filterMimetype":Ljava/lang/String;
    .restart local v4    # "ftpParams":Ljava/lang/String;
    .restart local v5    # "initialArgument":Landroid/os/Bundle;
    :cond_3
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 114
    const-string v6, "CONTENT_TYPE"

    invoke-virtual {v0, v6, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 131
    .end local v0    # "arg":Landroid/os/Bundle;
    .end local v1    # "cursor":Landroid/database/Cursor;
    .end local v2    # "filterExtension":Ljava/lang/String;
    .end local v3    # "filterMimetype":Ljava/lang/String;
    .end local v4    # "ftpParams":Ljava/lang/String;
    .end local v5    # "initialArgument":Landroid/os/Bundle;
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v8, 0x7f0b00c7

    invoke-virtual {v7, v8}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method
