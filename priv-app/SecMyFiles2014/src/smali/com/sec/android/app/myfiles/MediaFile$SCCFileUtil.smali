.class public Lcom/sec/android/app/myfiles/MediaFile$SCCFileUtil;
.super Ljava/lang/Object;
.source "MediaFile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/MediaFile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SCCFileUtil"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1316
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static byteToInt([BLjava/nio/ByteOrder;)I
    .locals 2
    .param p0, "bytes"    # [B
    .param p1, "order"    # Ljava/nio/ByteOrder;

    .prologue
    .line 1372
    const/4 v1, 0x4

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 1373
    .local v0, "buff":Ljava/nio/ByteBuffer;
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 1374
    invoke-virtual {v0, p0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1375
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 1376
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    return v1
.end method

.method public static getLargeIcon(Ljava/lang/String;)I
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 1408
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1412
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile$SCCFileUtil;->getMimetypeFromSCCFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1414
    .local v1, "mimetype":Ljava/lang/String;
    const-string v2, "application/vnd.samsung.scc.storyalbum"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1416
    const v2, 0x7f0200ba

    .line 1429
    .end local v1    # "mimetype":Ljava/lang/String;
    :goto_0
    return v2

    .line 1418
    .restart local v1    # "mimetype":Ljava/lang/String;
    :cond_0
    const-string v2, "application/vnd.samsung.scc.pinall"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_1

    .line 1420
    const v2, 0x7f020091

    goto :goto_0

    .line 1423
    .end local v1    # "mimetype":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1425
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 1429
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    const v2, 0x7f0200ae

    goto :goto_0
.end method

.method public static getMimetypeFromSCCFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x4

    .line 1319
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1320
    .local v6, "sccFile":Ljava/io/File;
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v6}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 1321
    .local v1, "fis":Ljava/io/FileInputStream;
    new-array v4, v7, [B

    .line 1322
    .local v4, "pklenBuf":[B
    const/16 v7, 0x80

    new-array v2, v7, [B

    .line 1325
    .local v2, "mimetypeBuf":[B
    const-wide/16 v8, 0x16

    :try_start_0
    invoke-virtual {v1, v8, v9}, Ljava/io/FileInputStream;->skip(J)J

    .line 1326
    const/4 v7, 0x0

    const/4 v8, 0x4

    invoke-virtual {v1, v4, v7, v8}, Ljava/io/FileInputStream;->read([BII)I

    .line 1327
    sget-object v7, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-static {v4, v7}, Lcom/sec/android/app/myfiles/MediaFile$SCCFileUtil;->byteToInt([BLjava/nio/ByteOrder;)I

    move-result v3

    .line 1328
    .local v3, "pklen":I
    const-wide/16 v8, 0xc

    invoke-virtual {v1, v8, v9}, Ljava/io/FileInputStream;->skip(J)J

    .line 1329
    const/4 v7, 0x0

    invoke-virtual {v1, v2, v7, v3}, Ljava/io/FileInputStream;->read([BII)I

    .line 1330
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v2}, Ljava/lang/String;-><init>([B)V

    .line 1331
    .local v5, "result":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 1332
    const-string v7, "application/vnd.samsung.scc"

    invoke-virtual {v5, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1333
    const-string v7, "ERR_NOT_SCC_FILE"

    .line 1340
    .end local v3    # "pklen":I
    .end local v5    # "result":Ljava/lang/String;
    :goto_0
    return-object v7

    .line 1335
    .restart local v3    # "pklen":I
    .restart local v5    # "result":Ljava/lang/String;
    :cond_0
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    goto :goto_0

    .line 1337
    .end local v3    # "pklen":I
    .end local v5    # "result":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1339
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 1340
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_0
.end method

.method public static getSmallIcon(Ljava/lang/String;)I
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 1381
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1385
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile$SCCFileUtil;->getMimetypeFromSCCFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1387
    .local v1, "mimetype":Ljava/lang/String;
    const-string v2, "application/vnd.samsung.scc.storyalbum"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1389
    const v2, 0x7f0200b9

    .line 1402
    .end local v1    # "mimetype":Ljava/lang/String;
    :goto_0
    return v2

    .line 1391
    .restart local v1    # "mimetype":Ljava/lang/String;
    :cond_0
    const-string v2, "application/vnd.samsung.scc.pinall"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_1

    .line 1393
    const v2, 0x7f020090

    goto :goto_0

    .line 1396
    .end local v1    # "mimetype":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1398
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 1402
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    const v2, 0x7f0200ad

    goto :goto_0
.end method

.method public static isSCCFile(Ljava/lang/String;)Z
    .locals 12
    .param p0, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x0

    .line 1345
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1346
    .local v7, "sccFile":Ljava/io/File;
    const/4 v1, 0x0

    .line 1347
    .local v1, "fis":Ljava/io/FileInputStream;
    new-array v5, v9, [B

    .line 1348
    .local v5, "pklenBuf":[B
    const/16 v9, 0x80

    new-array v3, v9, [B

    .line 1351
    .local v3, "mimetypeBuf":[B
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v7}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1353
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .local v2, "fis":Ljava/io/FileInputStream;
    const-wide/16 v10, 0x16

    :try_start_1
    invoke-virtual {v2, v10, v11}, Ljava/io/FileInputStream;->skip(J)J

    .line 1354
    const/4 v9, 0x0

    const/4 v10, 0x4

    invoke-virtual {v2, v5, v9, v10}, Ljava/io/FileInputStream;->read([BII)I

    .line 1355
    sget-object v9, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-static {v5, v9}, Lcom/sec/android/app/myfiles/MediaFile$SCCFileUtil;->byteToInt([BLjava/nio/ByteOrder;)I

    move-result v4

    .line 1356
    .local v4, "pklen":I
    const-wide/16 v10, 0xc

    invoke-virtual {v2, v10, v11}, Ljava/io/FileInputStream;->skip(J)J

    .line 1357
    const/4 v9, 0x0

    invoke-virtual {v2, v3, v9, v4}, Ljava/io/FileInputStream;->read([BII)I

    .line 1358
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v3}, Ljava/lang/String;-><init>([B)V

    .line 1359
    .local v6, "result":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 1360
    const-string v9, "application/vnd.samsung.scc"

    invoke-virtual {v6, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v9

    if-eqz v9, :cond_1

    const/4 v8, 0x1

    move-object v1, v2

    .line 1367
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .end local v4    # "pklen":I
    .end local v6    # "result":Ljava/lang/String;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    :cond_0
    :goto_0
    return v8

    .end local v1    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "pklen":I
    .restart local v6    # "result":Ljava/lang/String;
    :cond_1
    move-object v1, v2

    .line 1361
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    goto :goto_0

    .line 1363
    .end local v4    # "pklen":I
    .end local v6    # "result":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1365
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    if-eqz v1, :cond_0

    .line 1366
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    goto :goto_0

    .line 1363
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    :catch_1
    move-exception v0

    move-object v1, v2

    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    goto :goto_1
.end method
