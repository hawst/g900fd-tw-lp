.class Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;
.super Ljava/lang/Object;
.source "CategoryHomeFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V
    .locals 0

    .prologue
    .line 3179
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 3184
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    move-result-object v5

    invoke-virtual {v5, p3}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 3185
    .local v3, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {p2}, Landroid/view/View;->isEnabled()Z

    move-result v5

    if-nez v5, :cond_1

    .line 3321
    :cond_0
    :goto_0
    return-void

    .line 3188
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->isSelectMode()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 3189
    const v5, 0x7f0f0037

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    .line 3190
    .local v2, "checkbox":Landroid/widget/CheckBox;
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->isSelected()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 3191
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 3192
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->setSelected(Z)V

    .line 3197
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setAllItemCount()V

    .line 3198
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->invalidateOptionsMenu()V

    goto :goto_0

    .line 3194
    :cond_2
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 3195
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->setSelected(Z)V

    goto :goto_1

    .line 3200
    .end local v2    # "checkbox":Landroid/widget/CheckBox;
    :cond_3
    long-to-int v1, p4

    .line 3204
    .local v1, "categoryType":I
    const/4 v0, 0x0

    .line 3206
    .local v0, "argument":Landroid/os/Bundle;
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    goto :goto_0

    .line 3242
    :sswitch_0
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v5

    const/16 v6, 0x8

    if-ne v5, v6, :cond_4

    .line 3243
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v6, "DROP"

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/myfiles/utils/Utils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 3245
    :cond_4
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v1

    .line 3249
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->isWifiOn(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_9

    .line 3250
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    move-result-object v5

    iput-object v3, v5, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mShortCutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 3251
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iput v1, v5, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mModeForWarningDialog:I

    .line 3252
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckDropBox()Z

    move-result v5

    if-nez v5, :cond_9

    .line 3254
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setDropBoxFromShortcut(Z)V

    .line 3255
    const/16 v5, 0x1f

    if-ne v1, v5, :cond_8

    .line 3256
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/4 v6, 0x5

    invoke-virtual {v5, v6}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->showDialog(I)V

    goto/16 :goto_0

    .line 3209
    :sswitch_1
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->isWifiOn(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_5

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckFTP()Z

    move-result v5

    if-nez v5, :cond_5

    .line 3211
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    move-result-object v5

    iput-object v3, v5, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mShortCutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 3212
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->showDataWarningDialog(I)V

    goto/16 :goto_0

    .line 3217
    :cond_5
    sget-boolean v5, Lcom/sec/android/app/myfiles/utils/Utils;->mIsActiveWarningUsingWLAN:Z

    if-eqz v5, :cond_6

    const-string v5, "ChinaNalSecurity"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v6

    const-string v7, "CscFeature_Common_ConfigLocalSecurityPolicy"

    invoke-virtual {v6, v7}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 3220
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->isWifiOn(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckWLANOn()Z

    move-result v5

    if-nez v5, :cond_6

    .line 3222
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    move-result-object v5

    iput-object v3, v5, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mShortCutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 3223
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v6, "FTPC"

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/myfiles/utils/Utils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 3224
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    iput-object v6, v5, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->FTPargument:Landroid/os/Bundle;

    .line 3225
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    check-cast v3, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 3226
    .restart local v3    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->FTPargument:Landroid/os/Bundle;

    const-string v6, "FOLDERPATH"

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3227
    const/16 v1, 0xa

    .line 3228
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/16 v6, 0xa

    invoke-virtual {v5, v6}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->showDataWarningDialog(I)V

    goto/16 :goto_0

    .line 3234
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v6, "FTPC"

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/myfiles/utils/Utils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 3235
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "argument":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 3236
    .restart local v0    # "argument":Landroid/os/Bundle;
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    check-cast v3, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 3237
    .restart local v3    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    const-string v5, "FOLDERPATH"

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3238
    const/16 v1, 0xa

    .line 3316
    :cond_7
    :goto_2
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v5, v5, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v5, :cond_0

    .line 3317
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setStartBrowserState(Z)V

    .line 3318
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v5, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v5, v1, v0}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto/16 :goto_0

    .line 3258
    :cond_8
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/4 v6, 0x3

    invoke-virtual {v5, v6}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->showDialog(I)V

    goto/16 :goto_0

    .line 3264
    :cond_9
    sget-boolean v5, Lcom/sec/android/app/myfiles/utils/Utils;->mIsActiveWarningUsingWLAN:Z

    if-eqz v5, :cond_7

    const-string v5, "ChinaNalSecurity"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v6

    const-string v7, "CscFeature_Common_ConfigLocalSecurityPolicy"

    invoke-virtual {v6, v7}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 3266
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->isWifiOn(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 3267
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    move-result-object v5

    iput-object v3, v5, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mShortCutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 3268
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iput v1, v5, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mModeForWarningDialog:I

    .line 3269
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckWLANOn()Z

    move-result v5

    if-nez v5, :cond_7

    .line 3270
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setDropBoxFromShortcut(Z)V

    .line 3271
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/16 v6, 0xb

    invoke-virtual {v5, v6}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->showDialog(I)V

    goto/16 :goto_0

    .line 3279
    :sswitch_2
    const/16 v1, 0x25

    .line 3280
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "argument":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 3281
    .restart local v0    # "argument":Landroid/os/Bundle;
    const-string v5, "REMOTE_SHARE_DIR"

    const/16 v6, 0x26

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_2

    .line 3284
    :sswitch_3
    const/16 v1, 0x25

    .line 3285
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "argument":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 3286
    .restart local v0    # "argument":Landroid/os/Bundle;
    const-string v5, "REMOTE_SHARE_DIR"

    const/16 v6, 0x27

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_2

    .line 3289
    :sswitch_4
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v1

    .line 3290
    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;
    invoke-static {}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$1500()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    move-result-object v5

    if-nez v5, :cond_a

    .line 3292
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getNearbyDevicesProvider()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->setNearbyDevicesProvider(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)V

    .line 3294
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    move-result-object v5

    invoke-static {}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getCategoryAdapterDevicesCollection()Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->setDevicesCollection(Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;)V

    .line 3298
    :cond_a
    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;
    invoke-static {}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$1500()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    move-result-object v5

    if-eqz v5, :cond_7

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;
    invoke-static {}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$1500()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->checkAllshareEnabled()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 3300
    sget-object v5, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    if-eqz v5, :cond_7

    .line 3302
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v4

    .line 3303
    .local v4, "path":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v5, v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setCurrentDirectory(Ljava/lang/String;)V

    .line 3304
    invoke-static {}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->getInstance()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    invoke-virtual {v6, v4}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->getDevice(Ljava/lang/String;)Lcom/samsung/android/allshare/Device;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->registerDevice(Ljava/lang/String;Lcom/samsung/android/allshare/Device;)V

    .line 3306
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "argument":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 3307
    .restart local v0    # "argument":Landroid/os/Bundle;
    const-string v5, "FOLDERPATH"

    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 3206
    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_0
        0x9 -> :sswitch_4
        0xa -> :sswitch_1
        0x1f -> :sswitch_0
        0x26 -> :sswitch_2
        0x27 -> :sswitch_3
    .end sparse-switch
.end method
