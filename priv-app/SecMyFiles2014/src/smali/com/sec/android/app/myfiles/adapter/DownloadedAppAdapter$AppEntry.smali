.class public Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
.super Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$SizeInfo;
.source "DownloadedAppAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AppEntry"
.end annotation


# instance fields
.field final apkFile:Ljava/io/File;

.field appEntryContext:Landroid/content/Context;

.field cacheSizeStr:Ljava/lang/String;

.field codeSizeStr:Ljava/lang/String;

.field dataSizeStr:Ljava/lang/String;

.field date:J

.field externalSize:J

.field externalSizeStr:Ljava/lang/String;

.field icon:Landroid/graphics/drawable/Drawable;

.field final id:J

.field public info:Landroid/content/pm/ApplicationInfo;

.field internalSize:J

.field internalSizeStr:Ljava/lang/String;

.field isSelected:Z

.field label:Ljava/lang/String;

.field final mPackageManager:Landroid/content/pm/PackageManager;

.field mStatsObserver:Landroid/content/pm/IPackageStatsObserver$Stub;

.field mounted:Z

.field normalizedLabel:Ljava/lang/String;

.field size:J

.field sizeLoadStart:J

.field sizeStale:Z

.field sizeStr:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/pm/ApplicationInfo;J)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "info"    # Landroid/content/pm/ApplicationInfo;
    .param p3, "id"    # J

    .prologue
    .line 871
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$SizeInfo;-><init>()V

    .line 1001
    new-instance v2, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry$1;-><init>(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;)V

    iput-object v2, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->mStatsObserver:Landroid/content/pm/IPackageStatsObserver$Stub;

    .line 872
    new-instance v2, Ljava/io/File;

    iget-object v3, p2, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->apkFile:Ljava/io/File;

    .line 873
    iput-wide p3, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->id:J

    .line 874
    iput-object p2, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->info:Landroid/content/pm/ApplicationInfo;

    .line 875
    new-instance v2, Ljava/io/File;

    iget-object v3, p2, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->date:J

    .line 876
    new-instance v2, Ljava/io/File;

    iget-object v3, p2, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->size:J

    .line 877
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->sizeStale:Z

    .line 879
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->appEntryContext:Landroid/content/Context;

    .line 880
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->appEntryContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 885
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v0

    .line 886
    .local v0, "currentUser":I
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->appEntryContext:Landroid/content/Context;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->ensureLabel(Landroid/content/Context;)V

    .line 887
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->appEntryContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->ensureIconLocked(Landroid/content/Context;Landroid/content/pm/PackageManager;)Z

    .line 888
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->mStatsObserver:Landroid/content/pm/IPackageStatsObserver$Stub;

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/pm/PackageManager;->getPackageSizeInfo(Ljava/lang/String;ILandroid/content/pm/IPackageStatsObserver;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 901
    .end local v0    # "currentUser":I
    :goto_0
    return-void

    .line 890
    :catch_0
    move-exception v1

    .line 892
    .local v1, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 893
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->icon:Landroid/graphics/drawable/Drawable;

    .line 894
    const/4 v2, 0x2

    const-string v3, "AppEntry"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OutOfMemoryError occurs"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 896
    .end local v1    # "e":Ljava/lang/OutOfMemoryError;
    :catch_1
    move-exception v1

    .line 898
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;Landroid/content/pm/PackageStats;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    .param p1, "x1"    # Landroid/content/pm/PackageStats;

    .prologue
    .line 811
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->getTotalInternalSize(Landroid/content/pm/PackageStats;)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;J)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    .param p1, "x1"    # J

    .prologue
    .line 811
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->getSizeStr(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;Landroid/content/pm/PackageStats;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    .param p1, "x1"    # Landroid/content/pm/PackageStats;

    .prologue
    .line 811
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->getTotalExternalSize(Landroid/content/pm/PackageStats;)J

    move-result-wide v0

    return-wide v0
.end method

.method private getSizeStr(J)Ljava/lang/String;
    .locals 3
    .param p1, "size"    # J

    .prologue
    .line 1068
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    .line 1069
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->appEntryContext:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 1071
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getTotalExternalSize(Landroid/content/pm/PackageStats;)J
    .locals 4
    .param p1, "ps"    # Landroid/content/pm/PackageStats;

    .prologue
    .line 1057
    if-eqz p1, :cond_0

    .line 1060
    iget-wide v0, p1, Landroid/content/pm/PackageStats;->externalCodeSize:J

    iget-wide v2, p1, Landroid/content/pm/PackageStats;->externalDataSize:J

    add-long/2addr v0, v2

    iget-wide v2, p1, Landroid/content/pm/PackageStats;->externalCacheSize:J

    add-long/2addr v0, v2

    iget-wide v2, p1, Landroid/content/pm/PackageStats;->externalMediaSize:J

    add-long/2addr v0, v2

    iget-wide v2, p1, Landroid/content/pm/PackageStats;->externalObbSize:J

    add-long/2addr v0, v2

    .line 1064
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x2

    goto :goto_0
.end method

.method private getTotalInternalSize(Landroid/content/pm/PackageStats;)J
    .locals 4
    .param p1, "ps"    # Landroid/content/pm/PackageStats;

    .prologue
    .line 1050
    if-eqz p1, :cond_0

    .line 1051
    iget-wide v0, p1, Landroid/content/pm/PackageStats;->codeSize:J

    iget-wide v2, p1, Landroid/content/pm/PackageStats;->dataSize:J

    add-long/2addr v0, v2

    .line 1053
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x2

    goto :goto_0
.end method


# virtual methods
.method ensureIconLocked(Landroid/content/Context;Landroid/content/pm/PackageManager;)Z
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pm"    # Landroid/content/pm/PackageManager;

    .prologue
    const/16 v7, 0x96

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 917
    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->icon:Landroid/graphics/drawable/Drawable;

    if-nez v6, :cond_3

    .line 918
    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->apkFile:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 921
    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->info:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v6, p2}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 923
    .local v0, "TmpDraw":Landroid/graphics/drawable/Drawable;
    if-nez v0, :cond_1

    .line 949
    .end local v0    # "TmpDraw":Landroid/graphics/drawable/Drawable;
    :cond_0
    :goto_0
    return v4

    .line 928
    .restart local v0    # "TmpDraw":Landroid/graphics/drawable/Drawable;
    :cond_1
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .end local v0    # "TmpDraw":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 929
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v1, v7, v7, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-direct {v3, v4, v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 930
    .local v3, "icondraw":Landroid/graphics/drawable/Drawable;
    iput-object v3, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->icon:Landroid/graphics/drawable/Drawable;

    move v4, v5

    .line 932
    goto :goto_0

    .line 935
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .end local v3    # "icondraw":Landroid/graphics/drawable/Drawable;
    :cond_2
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->mounted:Z

    .line 936
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const-string v6, "sym_app_on_sd_unavailable_icon"

    const-string v7, "drawable"

    const-string v8, "android"

    invoke-virtual {v5, v6, v7, v8}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 937
    .local v2, "iconResId":I
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->icon:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 939
    .end local v2    # "iconResId":I
    :cond_3
    iget-boolean v6, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->mounted:Z

    if-nez v6, :cond_0

    .line 942
    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->apkFile:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 943
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->mounted:Z

    .line 944
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->info:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v4, p2}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->icon:Landroid/graphics/drawable/Drawable;

    move v4, v5

    .line 945
    goto :goto_0
.end method

.method public ensureLabel(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 904
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->label:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->mounted:Z

    if-nez v1, :cond_1

    .line 905
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->apkFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_2

    .line 906
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->mounted:Z

    .line 907
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->label:Ljava/lang/String;

    .line 914
    :cond_1
    :goto_0
    return-void

    .line 909
    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->mounted:Z

    .line 910
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->info:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 911
    .local v0, "label":Ljava/lang/CharSequence;
    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_1
    iput-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->label:Ljava/lang/String;

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    goto :goto_1
.end method

.method public getCacheSize()Ljava/lang/String;
    .locals 1

    .prologue
    .line 998
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->cacheSizeStr:Ljava/lang/String;

    return-object v0
.end method

.method public getCodeSize()Ljava/lang/String;
    .locals 1

    .prologue
    .line 988
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->codeSizeStr:Ljava/lang/String;

    return-object v0
.end method

.method public getDataSize()Ljava/lang/String;
    .locals 1

    .prologue
    .line 993
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->dataSizeStr:Ljava/lang/String;

    return-object v0
.end method

.method public getDate()J
    .locals 2

    .prologue
    .line 1075
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->date:J

    return-wide v0
.end method

.method public getLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 978
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->label:Ljava/lang/String;

    return-object v0
.end method

.method public getLaunchIntent(Landroid/content/pm/PackageManager;)Landroid/content/Intent;
    .locals 1
    .param p1, "pm"    # Landroid/content/pm/PackageManager;

    .prologue
    .line 967
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method getNormalizedLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 836
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->normalizedLabel:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 838
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->normalizedLabel:Ljava/lang/String;

    .line 843
    :goto_0
    return-object v0

    .line 841
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->label:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->normalizedLabel:Ljava/lang/String;

    .line 843
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->normalizedLabel:Ljava/lang/String;

    goto :goto_0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 973
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->info:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public getTotalSize()Ljava/lang/String;
    .locals 1

    .prologue
    .line 983
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->sizeStr:Ljava/lang/String;

    return-object v0
.end method

.method isSelected()Z
    .locals 1

    .prologue
    .line 955
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->isSelected:Z

    return v0
.end method

.method setSelected(Z)V
    .locals 0
    .param p1, "selected"    # Z

    .prologue
    .line 961
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->isSelected:Z

    .line 962
    return-void
.end method
