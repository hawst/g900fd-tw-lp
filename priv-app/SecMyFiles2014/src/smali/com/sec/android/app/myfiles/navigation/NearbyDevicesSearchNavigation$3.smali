.class Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$3;
.super Ljava/lang/Object;
.source "NearbyDevicesSearchNavigation.java"

# interfaces
.implements Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;)V
    .locals 0

    .prologue
    .line 197
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSearchResponseReceived(Ljava/util/ArrayList;IILcom/samsung/android/allshare/media/SearchCriteria;ZLcom/samsung/android/allshare/ERROR;)V
    .locals 7
    .param p2, "requestedStartIndex"    # I
    .param p3, "requestedCount"    # I
    .param p4, "searchCriteria"    # Lcom/samsung/android/allshare/media/SearchCriteria;
    .param p5, "endOfItems"    # Z
    .param p6, "err"    # Lcom/samsung/android/allshare/ERROR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Item;",
            ">;II",
            "Lcom/samsung/android/allshare/media/SearchCriteria;",
            "Z",
            "Lcom/samsung/android/allshare/ERROR;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    const/4 v6, 0x1

    .line 201
    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    if-eq p6, v3, :cond_1

    .line 202
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mAsyncUi:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    if-eqz v3, :cond_0

    .line 203
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mAsyncUi:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->setCompletedCondition(Z)V

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 207
    :cond_1
    if-eqz p2, :cond_2

    if-eqz p5, :cond_2

    .line 208
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b008a

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 211
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    if-eqz v3, :cond_5

    .line 212
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 213
    if-nez p2, :cond_3

    .line 214
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mCurrentItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 215
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mCurrItemsIdProp:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 216
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->clearNearByData()J

    .line 218
    :cond_3
    if-eqz p1, :cond_6

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_6

    .line 219
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/Item;

    .line 220
    .local v2, "item":Lcom/samsung/android/allshare/Item;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mCurrentItems:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/media/Provider;->getID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Lcom/sec/android/app/myfiles/db/CacheDB;->insertNearByData(Ljava/lang/String;Lcom/samsung/android/allshare/Item;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 222
    .local v1, "id":Ljava/lang/Long;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mCurrItemsIdProp:Ljava/util/Map;

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 224
    .end local v1    # "id":Ljava/lang/Long;
    .end local v2    # "item":Lcom/samsung/android/allshare/Item;
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 225
    if-lez p2, :cond_5

    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v3, :cond_5

    .line 226
    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$3$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$3$1;-><init>(Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$3;)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    .line 238
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_5
    :goto_2
    if-nez p2, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mAsyncUi:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    if-eqz v3, :cond_0

    .line 239
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mAsyncUi:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->setCompletedCondition(Z)V

    goto/16 :goto_0

    .line 234
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    goto :goto_2
.end method
