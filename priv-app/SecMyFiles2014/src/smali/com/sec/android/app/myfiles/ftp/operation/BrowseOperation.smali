.class public Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;
.super Landroid/os/AsyncTask;
.source "BrowseOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
        ">;>;"
    }
.end annotation


# static fields
.field public static volatile mAbortBrowsing:Z


# instance fields
.field private final mCache:Lcom/sec/android/app/myfiles/ftp/FTPCache;

.field private final mDialogSync:Ljava/lang/Object;

.field private mErrorCode:I

.field private final mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

.field private final mInitialBrowse:Z

.field private volatile mItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

.field private final mOrder:I

.field private final mParam:Lcom/sec/android/app/myfiles/ftp/FTPParams;

.field private final mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

.field private final mSortBy:I

.field private mTimestamp:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mAbortBrowsing:Z

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Lcom/sec/android/app/myfiles/ftp/FTPCache;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;IIZ)V
    .locals 1
    .param p1, "arg"    # Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;
    .param p2, "cache"    # Lcom/sec/android/app/myfiles/ftp/FTPCache;
    .param p3, "timestamp"    # Ljava/lang/String;
    .param p4, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;
    .param p5, "sortBy"    # I
    .param p6, "order"    # I
    .param p7, "initialBrowse"    # Z

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 36
    iput-boolean p7, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mInitialBrowse:Z

    .line 37
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->getDialogSync()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mDialogSync:Ljava/lang/Object;

    .line 38
    iput-object p4, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    .line 39
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->getFTPConnectionHandler()Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    .line 40
    iput-object p2, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mCache:Lcom/sec/android/app/myfiles/ftp/FTPCache;

    .line 41
    iput p5, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mSortBy:I

    .line 42
    iput p6, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mOrder:I

    .line 43
    iput-object p3, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mTimestamp:Ljava/lang/String;

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v0}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->getFtpParams()Lcom/sec/android/app/myfiles/ftp/FTPParams;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mParam:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .line 49
    :goto_0
    return-void

    .line 47
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mParam:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    goto :goto_0
.end method


# virtual methods
.method public abortBrowsing()V
    .locals 3

    .prologue
    .line 98
    const/4 v0, 0x2

    const-string v1, "ftp"

    const-string v2, "mAbortBrowsing set !!!!!!!!!!!!!!!!!"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 100
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mAbortBrowsing:Z

    .line 102
    return-void
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 34
    check-cast p1, [Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->doInBackground([Lcom/sec/android/app/myfiles/ftp/IFTPItem;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Lcom/sec/android/app/myfiles/ftp/IFTPItem;)Ljava/util/List;
    .locals 8
    .param p1, "items"    # [Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    iget-object v7, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mDialogSync:Ljava/lang/Object;

    monitor-enter v7

    .line 56
    const/4 v4, 0x0

    .line 58
    .local v4, "ftpItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    :try_start_0
    array-length v0, p1

    const/4 v1, 0x1

    if-lt v0, v1, :cond_0

    .line 60
    const/4 v0, 0x0

    aget-object v0, p1, v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v0}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncPrepareConnection()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mAbortBrowsing:Z

    if-nez v0, :cond_2

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    invoke-interface {v0, v1}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncBrowseForItemContent(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)Ljava/util/List;

    move-result-object v4

    .line 68
    if-nez v4, :cond_1

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v0}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->getLastErrorCode()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mErrorCode:I

    .line 73
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mTimestamp:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v0}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncSupportsMdtm()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    invoke-interface {v1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->getTimestamp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mTimestamp:Ljava/lang/String;

    .line 79
    :cond_2
    if-eqz v4, :cond_4

    .line 81
    sget-boolean v0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mAbortBrowsing:Z

    if-eqz v0, :cond_3

    .line 82
    const/4 v0, 0x2

    const-string v1, "ftp"

    const-string v2, "mAbortBrowsing ture insertItemsAndLoadBuffer !!! RETURN"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 83
    const/4 v0, 0x0

    monitor-exit v7

    move-object v4, v0

    .line 91
    .end local v4    # "ftpItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    :goto_0
    return-object v4

    .line 86
    .restart local v4    # "ftpItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mCache:Lcom/sec/android/app/myfiles/ftp/FTPCache;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mParam:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mTimestamp:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mTimestamp:Ljava/lang/String;

    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    invoke-interface {v3}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v3

    iget v5, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mSortBy:I

    iget v6, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mOrder:I

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/myfiles/ftp/FTPCache;->insertItemsAndLoadBuffer(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;II)V

    .line 91
    :cond_4
    monitor-exit v7

    goto :goto_0

    .line 92
    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 86
    :cond_5
    :try_start_1
    const-string v2, "no_tstmp"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 34
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 106
    sget-boolean v0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mAbortBrowsing:Z

    if-nez v0, :cond_1

    .line 107
    if-eqz p1, :cond_0

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    invoke-interface {v0, p1, v1}, Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;->onBrowseResponseReceived(Ljava/util/List;Lcom/sec/android/app/myfiles/ftp/IFTPItem;)V

    .line 126
    :goto_0
    const/4 v0, 0x2

    const-string v1, "ftp"

    const-string v2, "mAbortBrowsing clear !!!!!!!!!!!!!!!!!"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 127
    sput-boolean v3, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mAbortBrowsing:Z

    .line 128
    return-void

    .line 111
    :cond_0
    iget v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mErrorCode:I

    packed-switch v0, :pswitch_data_0

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mInitialBrowse:Z

    invoke-interface {v0, v1, v2, v4}, Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;->onBrowseOperationFailed(ZZI)V

    goto :goto_0

    .line 114
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mInitialBrowse:Z

    invoke-interface {v0, v3, v1, v3}, Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;->onBrowseOperationFailed(ZZI)V

    goto :goto_0

    .line 123
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mInitialBrowse:Z

    invoke-interface {v0, v3, v1, v4}, Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;->onBrowseOperationFailed(ZZI)V

    goto :goto_0

    .line 111
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
