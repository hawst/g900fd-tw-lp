.class Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$8;
.super Ljava/lang/Object;
.source "VideoHoverPopup.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->initViews()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;)V
    .locals 0

    .prologue
    .line 682
    iput-object p1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$8;->this$0:Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x2

    .line 697
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 699
    const/16 v1, 0xa

    const/4 v2, -0x1

    invoke-static {v1, v2}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 708
    :cond_0
    :goto_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 760
    :cond_1
    :goto_1
    :pswitch_0
    return v5

    .line 703
    :catch_0
    move-exception v0

    .line 705
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 712
    .end local v0    # "e":Landroid/os/RemoteException;
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$8;->this$0:Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;

    iput-boolean v5, v1, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mHoverOverLayout:Z

    .line 714
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$8;->this$0:Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 716
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$8;->this$0:Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_1

    .line 729
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$8;->this$0:Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 731
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$8;->this$0:Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_1

    .line 750
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$8;->this$0:Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;

    iput-boolean v4, v1, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->mHoverOverLayout:Z

    .line 751
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$8;->this$0:Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 752
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$8;->this$0:Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->removeDelayedMessage()V

    goto :goto_1

    .line 708
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
