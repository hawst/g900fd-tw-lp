.class public Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;
.super Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;
.source "AddShortcutDialog.java"


# static fields
.field private static final CURRENT_SELECTED_MENU_INDEX:Ljava/lang/String; = "current_selected_menu_index"

.field private static final MODULE:Ljava/lang/String; = "AddShortcutDialog"

.field private static final mShortcutTypes:[I


# instance fields
.field private mCurrentChoice:I

.field private mCurrentSelectedMenuIndex:I

.field private mShortcutIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->mShortcutTypes:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0b005b
        0x7f0b011b
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;-><init>()V

    .line 182
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->mShortcutIndex:I

    .line 183
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->mCurrentChoice:I

    .line 49
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;
    .param p1, "x1"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->mCurrentChoice:I

    return p1
.end method

.method static synthetic access$100()[I
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->mShortcutTypes:[I

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;
    .param p1, "x1"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->mCurrentSelectedMenuIndex:I

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;
    .param p1, "x1"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->clickOk(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->clickCancel()V

    return-void
.end method

.method private clickCancel()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 154
    const-string v1, "category_home"

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->mTargetFragmentTag:Ljava/lang/String;

    .line 156
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->setTargetFragmentAgain()V

    .line 159
    iput v3, p0, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->mCurrentSelectedMenuIndex:I

    .line 161
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 162
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "add_shortcut_tag"

    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->mCurrentChoice:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 163
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->getTargetRequestCode()I

    move-result v2

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 165
    return-void
.end method

.method private clickOk(I)V
    .locals 4
    .param p1, "button"    # I

    .prologue
    .line 138
    const-string v1, "category_home"

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->mTargetFragmentTag:Ljava/lang/String;

    .line 140
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->setTargetFragmentAgain()V

    .line 142
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 144
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "add_shortcut_tag"

    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->mCurrentChoice:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 146
    const-string v1, "shortcut_working_index_intent_key"

    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->mShortcutIndex:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 148
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->getTargetRequestCode()I

    move-result v2

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 150
    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 63
    if-eqz p1, :cond_0

    .line 65
    const-string v4, "current_selected_menu_index"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->mCurrentSelectedMenuIndex:I

    .line 68
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 70
    .local v0, "arg":Landroid/os/Bundle;
    if-eqz v0, :cond_1

    .line 72
    const-string v4, "shortcut_working_index_intent_key"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->mShortcutIndex:I

    .line 75
    :cond_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 76
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    const/4 v3, 0x0

    .line 78
    .local v3, "shortcuts":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->isInKNOXMode(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 79
    const/4 v4, 0x1

    new-array v3, v4, [Ljava/lang/String;

    .line 80
    const v4, 0x7f0b005b

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    .line 87
    :cond_2
    const v4, 0x7f0b00a8

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 88
    iget v4, p0, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->mCurrentSelectedMenuIndex:I

    new-instance v5, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog$1;-><init>(Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;)V

    invoke-virtual {v1, v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 97
    sget-object v4, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->mShortcutTypes:[I

    aget v4, v4, v6

    iput v4, p0, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->mCurrentChoice:I

    .line 98
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->setupDialog(Landroid/app/AlertDialog$Builder;)V

    .line 100
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    return-object v4

    .line 82
    :cond_3
    sget-object v4, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->mShortcutTypes:[I

    array-length v4, v4

    new-array v3, v4, [Ljava/lang/String;

    .line 83
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    sget-object v4, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->mShortcutTypes:[I

    array-length v4, v4

    if-ge v2, v4, :cond_2

    .line 84
    sget-object v4, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->mShortcutTypes:[I

    aget v4, v4, v2

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    .line 83
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 176
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->setShowsDialog(Z)V

    .line 177
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->clickCancel()V

    .line 178
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->onDetach()V

    .line 180
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 54
    const/4 v0, 0x0

    const-string v1, "AddShortcutDialog"

    const-string v2, "onPause "

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 55
    const-string v0, "category_home"

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->mTargetFragmentTag:Ljava/lang/String;

    .line 56
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->onPause()V

    .line 57
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 107
    const/4 v0, 0x0

    const-string v1, "AddShortcutDialog"

    const-string v2, "onSaveInstanceState "

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 108
    const-string v0, "category_home"

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->mTargetFragmentTag:Ljava/lang/String;

    .line 109
    const-string v0, "current_selected_menu_index"

    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->mCurrentSelectedMenuIndex:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 111
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 112
    return-void
.end method

.method protected setupDialog(Landroid/app/AlertDialog$Builder;)V
    .locals 2
    .param p1, "builder"    # Landroid/app/AlertDialog$Builder;

    .prologue
    .line 116
    const v0, 0x7f0b0015

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog$2;-><init>(Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 122
    const v0, 0x7f0b0017

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog$3;-><init>(Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 129
    return-void
.end method

.method protected setupInputEditText(Landroid/widget/EditText;)V
    .locals 0
    .param p1, "editText"    # Landroid/widget/EditText;

    .prologue
    .line 134
    return-void
.end method
