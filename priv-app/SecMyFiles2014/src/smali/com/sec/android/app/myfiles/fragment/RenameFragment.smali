.class public Lcom/sec/android/app/myfiles/fragment/RenameFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;
.source "RenameFragment.java"


# static fields
.field private static final MESSAGE_RENAME_ERROR:I = 0x1

.field private static final MESSAGE_RENAME_SUCCESS:I = 0x0

.field private static final MODULE:Ljava/lang/String; = "RenameFragment"


# instance fields
.field private mDialog:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

.field private mHandler:Landroid/os/Handler;

.field private mPath:Ljava/lang/String;

.field private mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

.field private mSourceFragmentId:I

.field private mSrcFileName:Ljava/lang/String;

.field private mToast:Landroid/widget/Toast;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;-><init>()V

    .line 60
    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mToast:Landroid/widget/Toast;

    .line 61
    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mDialog:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    .line 67
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/RenameFragment$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/RenameFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/RenameFragment;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/fragment/RenameFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->clickOK()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/fragment/RenameFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSrcFileName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/fragment/RenameFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSourceFragmentId:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/fragment/RenameFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/fragment/RenameFragment;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/myfiles/fragment/RenameFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/myfiles/fragment/RenameFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/RenameFragment;
    .param p1, "x1"    # I

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->showToast(I)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/myfiles/fragment/RenameFragment;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/myfiles/fragment/RenameFragment;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/RenameFragment;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mToast:Landroid/widget/Toast;

    return-object p1
.end method

.method private clickOK()V
    .locals 17

    .prologue
    .line 263
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "current_item"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 265
    .local v8, "currentPath":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSourceFragmentId:I

    packed-switch v1, :pswitch_data_0

    .line 307
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 309
    .local v7, "srcFile":Ljava/io/File;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSourceFragmentId:I

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSourceFragmentId:I

    const/16 v2, 0x1f

    if-eq v1, v2, :cond_0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSourceFragmentId:I

    const/16 v2, 0x12

    if-ne v1, v2, :cond_9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mPath:Ljava/lang/String;

    const-string v2, "/storage"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 311
    :cond_0
    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    .line 312
    .local v6, "srcName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mInputEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    .line 313
    .local v11, "dstName":Ljava/lang/String;
    move-object v12, v11

    .line 315
    .local v12, "dstNameWithoutExt":Ljava/lang/String;
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_3

    .line 317
    const v1, 0x7f0b0075

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->showToast(I)V

    .line 608
    .end local v6    # "srcName":Ljava/lang/String;
    .end local v7    # "srcFile":Ljava/io/File;
    .end local v11    # "dstName":Ljava/lang/String;
    .end local v12    # "dstNameWithoutExt":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 269
    :pswitch_0
    invoke-static {v8}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getItem(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPItem;

    move-result-object v14

    .line 271
    .local v14, "item":Lcom/sec/android/app/myfiles/ftp/FTPItem;
    if-eqz v14, :cond_1

    .line 273
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mInputEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v11

    .line 275
    .restart local v11    # "dstName":Ljava/lang/String;
    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getTitle()Ljava/lang/String;

    move-result-object v6

    .line 277
    .restart local v6    # "srcName":Ljava/lang/String;
    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->isFile()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 279
    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getTitle()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x2e

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v13

    .line 281
    .local v13, "index":I
    const/4 v1, -0x1

    if-eq v13, v1, :cond_2

    .line 283
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v6, v13, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 289
    .end local v13    # "index":I
    :cond_2
    new-instance v9, Landroid/content/Intent;

    invoke-direct {v9}, Landroid/content/Intent;-><init>()V

    .line 291
    .local v9, "data":Landroid/content/Intent;
    const-string v1, "FILE_OPERATION"

    const/4 v2, 0x7

    invoke-virtual {v9, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 293
    const-string v1, "from"

    invoke-virtual {v9, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 295
    const-string v1, "to"

    invoke-virtual {v9, v1, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 297
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v1

    const/16 v2, 0x13

    const/16 v16, -0x1

    move/from16 v0, v16

    invoke-virtual {v1, v2, v0, v9}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 299
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->dismiss()V

    goto :goto_0

    .line 321
    .end local v9    # "data":Landroid/content/Intent;
    .end local v14    # "item":Lcom/sec/android/app/myfiles/ftp/FTPItem;
    .restart local v7    # "srcFile":Ljava/io/File;
    .restart local v12    # "dstNameWithoutExt":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSourceFragmentId:I

    const/16 v2, 0x8

    if-eq v1, v2, :cond_4

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSourceFragmentId:I

    const/16 v2, 0x1f

    if-eq v1, v2, :cond_4

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSourceFragmentId:I

    const/16 v2, 0x12

    if-ne v1, v2, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mPath:Ljava/lang/String;

    const-string v2, "/storage"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 324
    :cond_4
    new-instance v10, Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v10, v1}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    .line 325
    .local v10, "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    invoke-virtual {v10, v8}, Lcom/sec/android/app/myfiles/db/CacheDB;->isDirectory(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 327
    const/16 v1, 0x2e

    invoke-virtual {v6, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v13

    .line 328
    .restart local v13    # "index":I
    const/4 v1, -0x1

    if-eq v13, v1, :cond_5

    .line 330
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v6, v13, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 343
    .end local v10    # "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    .end local v13    # "index":I
    :cond_5
    :goto_1
    invoke-virtual {v11, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 345
    const v1, 0x7f0b012a

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->showToast(I)V

    goto/16 :goto_0

    .line 334
    :cond_6
    invoke-virtual {v7}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 336
    const/16 v1, 0x2e

    invoke-virtual {v6, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v13

    .line 337
    .restart local v13    # "index":I
    const/4 v1, -0x1

    if-eq v13, v1, :cond_5

    .line 339
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v6, v13, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    goto :goto_1

    .line 349
    .end local v13    # "index":I
    :cond_7
    new-instance v5, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x2f

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v5, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 350
    .local v5, "dstFile":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0xff

    if-le v1, v2, :cond_8

    .line 352
    const v1, 0x7f0b0077

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->showToast(I)V

    goto/16 :goto_0

    .line 356
    :cond_8
    move-object v4, v12

    .line 357
    .local v4, "dstName1":Ljava/lang/String;
    move-object v3, v8

    .line 358
    .local v3, "currentPath1":Ljava/lang/String;
    new-instance v15, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;-><init>(Lcom/sec/android/app/myfiles/fragment/RenameFragment;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;Ljava/io/File;)V

    invoke-direct {v15, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 597
    .local v15, "t":Ljava/lang/Thread;
    invoke-virtual {v15}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    .line 600
    .end local v3    # "currentPath1":Ljava/lang/String;
    .end local v4    # "dstName1":Ljava/lang/String;
    .end local v5    # "dstFile":Ljava/io/File;
    .end local v6    # "srcName":Ljava/lang/String;
    .end local v11    # "dstName":Ljava/lang/String;
    .end local v12    # "dstNameWithoutExt":Ljava/lang/String;
    .end local v15    # "t":Ljava/lang/Thread;
    :cond_9
    const v1, 0x7f0b0075

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->showToast(I)V

    goto/16 :goto_0

    .line 265
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method

.method private insertInDatabase(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "renameFilePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 637
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 638
    .local v6, "contentValues":Landroid/content/ContentValues;
    const-string v0, "external"

    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 639
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "_data = ?"

    new-array v4, v8, [Ljava/lang/String;

    aput-object p1, v4, v9

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 640
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_1

    .line 641
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 642
    const-string v0, "_data"

    invoke-virtual {v6, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 643
    const-string v0, "_display_name"

    const-string v2, "_display_name"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 644
    const-string v0, "format"

    const-string v2, "format"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v6, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 645
    const-string v0, "is_drm"

    const-string v2, "is_drm"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v6, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 646
    const-string v0, "duration"

    const-string v2, "duration"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v6, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 647
    const-string v0, "artist"

    const-string v2, "artist"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 648
    const-string v0, "album"

    const-string v2, "album"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 649
    const-string v0, "resolution"

    const-string v2, "resolution"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 650
    const-string v0, "width"

    const-string v2, "width"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v6, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 651
    const-string v0, "height"

    const-string v2, "height"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v6, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 652
    const-string v0, "orientation"

    const-string v2, "orientation"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v6, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 653
    const-string v0, "album_id"

    const-string v2, "album_id"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v6, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 654
    const-string v0, "artist_id"

    const-string v2, "artist_id"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v6, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 655
    const-string v0, "album"

    const-string v2, "album"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 656
    const-string v0, "duration"

    const-string v2, "duration"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v6, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 657
    const-string v0, "is_alarm"

    const-string v2, "is_alarm"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v6, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 658
    const-string v0, "is_music"

    const-string v2, "is_music"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v6, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 659
    const-string v0, "is_notification"

    const-string v2, "is_notification"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v6, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 660
    const-string v0, "is_podcast"

    const-string v2, "is_podcast"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v6, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 661
    const-string v0, "is_ringtone"

    const-string v2, "is_ringtone"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v6, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 663
    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 665
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, v1, v6}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v8

    .line 668
    :goto_0
    return v0

    :cond_2
    move v0, v9

    goto :goto_0
.end method

.method private rename(Ljava/io/File;Ljava/io/File;)Z
    .locals 3
    .param p1, "src"    # Ljava/io/File;
    .param p2, "dst"    # Ljava/io/File;

    .prologue
    .line 611
    invoke-virtual {p1, p2}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 613
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->insertInDatabase(Ljava/lang/String;Ljava/lang/String;)Z

    .line 614
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->deleteInDatabase(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 618
    :goto_0
    const/4 v1, 0x1

    .line 621
    :goto_1
    return v1

    .line 615
    :catch_0
    move-exception v0

    .line 616
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 620
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const v1, 0x7f0b0075

    invoke-direct {p0, v1}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->showToast(I)V

    .line 621
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private showToast(I)V
    .locals 2
    .param p1, "resId"    # I

    .prologue
    .line 685
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 687
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/RenameFragment$6;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/myfiles/fragment/RenameFragment$6;-><init>(Lcom/sec/android/app/myfiles/fragment/RenameFragment;I)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 704
    :cond_0
    return-void
.end method

.method private updateInDatabase(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "renameFilePath"    # Ljava/lang/String;
    .param p3, "renameFileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 672
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 673
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v4, "external"

    invoke-static {v4}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 674
    .local v1, "uri":Landroid/net/Uri;
    const-string v4, "title"

    invoke-virtual {v0, v4, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 675
    const-string v4, "_data"

    invoke-virtual {v0, v4, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 677
    const-string v2, "_data =  ? "

    .line 678
    .local v2, "where":Ljava/lang/String;
    const/4 v4, 0x1

    new-array v3, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    .line 679
    .local v3, "whereArgs":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, v1, v0, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 681
    return-void
.end method


# virtual methods
.method protected deleteInDatabase(Ljava/lang/String;)Z
    .locals 6
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 626
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 633
    :cond_0
    :goto_0
    return v1

    .line 629
    :cond_1
    const-string v3, "external"

    invoke-static {v3}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 630
    .local v0, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "_data = ?"

    new-array v5, v2, [Ljava/lang/String;

    aput-object p1, v5, v1

    invoke-virtual {v3, v0, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_0

    move v1, v2

    .line 631
    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 121
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->onDestroyView()V

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    .line 124
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 94
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    .line 96
    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSourceFragmentId:I

    const/16 v2, 0x8

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSourceFragmentId:I

    const/16 v2, 0xa

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSourceFragmentId:I

    const/16 v2, 0x1f

    if-eq v1, v2, :cond_1

    .line 98
    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSourceFragmentId:I

    const/16 v2, 0x12

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mPath:Ljava/lang/String;

    const-string v2, "/storage"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 99
    :cond_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 100
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 102
    const-string v1, "RenameFragment"

    const-string v2, "onResume : MESSAGE_RENAME_ERROR - !file.exists() "

    invoke-static {v3, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 103
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 108
    .end local v0    # "file":Ljava/io/File;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    check-cast v1, Landroid/app/AlertDialog;

    if-eqz v1, :cond_2

    .line 109
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    check-cast v1, Landroid/app/AlertDialog;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mOkButton:Landroid/widget/Button;

    .line 110
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    check-cast v1, Landroid/app/AlertDialog;

    const/4 v2, -0x2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mCancelButton:Landroid/widget/Button;

    .line 111
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSrcFileName:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mInputEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 112
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mOkButton:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 115
    :cond_2
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->onResume()V

    .line 116
    return-void
.end method

.method protected setupDialog(Landroid/app/AlertDialog$Builder;)V
    .locals 8
    .param p1, "builder"    # Landroid/app/AlertDialog$Builder;

    .prologue
    const/16 v7, 0x2e

    const/4 v6, 0x0

    .line 128
    new-instance v4, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mDialog:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    .line 129
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mDialog:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->setDialogInstance(Lcom/sec/android/app/myfiles/utils/MyFilesDialog;)V

    .line 130
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "current_item"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mPath:Ljava/lang/String;

    .line 131
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "src_fragment_id"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSourceFragmentId:I

    .line 133
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mPath:Ljava/lang/String;

    if-eqz v4, :cond_1

    .line 135
    iget v4, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSourceFragmentId:I

    packed-switch v4, :pswitch_data_0

    .line 165
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mPath:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 166
    .local v3, "srcFile":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSrcFileName:Ljava/lang/String;

    .line 168
    iget v4, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSourceFragmentId:I

    const/16 v5, 0x8

    if-eq v4, v5, :cond_0

    iget v4, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSourceFragmentId:I

    const/16 v5, 0x1f

    if-eq v4, v5, :cond_0

    iget v4, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSourceFragmentId:I

    const/16 v5, 0x12

    if-ne v4, v5, :cond_4

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mPath:Ljava/lang/String;

    const-string v5, "/storage"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 171
    :cond_0
    new-instance v0, Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    .line 172
    .local v0, "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mPath:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/myfiles/db/CacheDB;->isDirectory(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 174
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSrcFileName:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 175
    .local v1, "dotIndex":I
    if-ltz v1, :cond_1

    .line 176
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSrcFileName:Ljava/lang/String;

    invoke-virtual {v4, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSrcFileName:Ljava/lang/String;

    .line 190
    .end local v0    # "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    .end local v1    # "dotIndex":I
    .end local v3    # "srcFile":Ljava/io/File;
    :cond_1
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mInputEditText:Landroid/widget/EditText;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSrcFileName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 191
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mInputEditText:Landroid/widget/EditText;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 194
    const v4, 0x7f0b0023

    new-instance v5, Lcom/sec/android/app/myfiles/fragment/RenameFragment$2;

    invoke-direct {v5, p0}, Lcom/sec/android/app/myfiles/fragment/RenameFragment$2;-><init>(Lcom/sec/android/app/myfiles/fragment/RenameFragment;)V

    invoke-virtual {p1, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 203
    const v4, 0x7f0b0017

    new-instance v5, Lcom/sec/android/app/myfiles/fragment/RenameFragment$3;

    invoke-direct {v5, p0}, Lcom/sec/android/app/myfiles/fragment/RenameFragment$3;-><init>(Lcom/sec/android/app/myfiles/fragment/RenameFragment;)V

    invoke-virtual {p1, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 210
    return-void

    .line 139
    :pswitch_0
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mPath:Ljava/lang/String;

    invoke-static {v4}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getItem(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPItem;

    move-result-object v2

    .line 141
    .local v2, "item":Lcom/sec/android/app/myfiles/ftp/FTPItem;
    if-eqz v2, :cond_1

    .line 143
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->isFile()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 145
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 147
    .restart local v1    # "dotIndex":I
    if-ltz v1, :cond_1

    .line 149
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSrcFileName:Ljava/lang/String;

    goto :goto_0

    .line 153
    .end local v1    # "dotIndex":I
    :cond_2
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->isDirectory()Z

    move-result v4

    if-nez v4, :cond_3

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->isSymbolicLink()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 155
    :cond_3
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getTitle()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSrcFileName:Ljava/lang/String;

    goto :goto_0

    .line 179
    .end local v2    # "item":Lcom/sec/android/app/myfiles/ftp/FTPItem;
    .restart local v3    # "srcFile":Ljava/io/File;
    :cond_4
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-nez v4, :cond_1

    .line 181
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSrcFileName:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 182
    .restart local v1    # "dotIndex":I
    if-ltz v1, :cond_1

    .line 183
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSrcFileName:Ljava/lang/String;

    invoke-virtual {v4, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSrcFileName:Ljava/lang/String;

    goto :goto_0

    .line 135
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method

.method protected setupInputEditText(Landroid/widget/EditText;)V
    .locals 4
    .param p1, "editText"    # Landroid/widget/EditText;

    .prologue
    const/4 v3, 0x1

    .line 215
    if-nez p1, :cond_0

    .line 259
    :goto_0
    return-void

    .line 219
    :cond_0
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mPath:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v0

    .line 221
    .local v0, "curItemParentPathLength":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/app/myfiles/utils/Utils;->getEditTextFilter(Landroid/content/Context;I)[Landroid/text/InputFilter;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 222
    invoke-virtual {p1, v3}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 223
    invoke-virtual {p1, v3}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 225
    const-string v1, "inputType=PredictionOff;inputType=filename;disableEmoticonInput=true"

    invoke-virtual {p1, v1}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 228
    const v1, 0x84001

    invoke-virtual {p1, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 231
    new-instance v1, Lcom/sec/android/app/myfiles/fragment/RenameFragment$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/RenameFragment$4;-><init>(Lcom/sec/android/app/myfiles/fragment/RenameFragment;)V

    invoke-virtual {p1, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    goto :goto_0
.end method
