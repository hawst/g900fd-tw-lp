.class public Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;
.source "CategoryBrowserFragment.java"


# static fields
.field private static final MODULE:Ljava/lang/String; = "CategoryBrowserFragment"

.field private static final MSG_REFRESH:I


# instance fields
.field mCategoryNameResId:I

.field private mPrevListGridState:Landroid/os/Parcelable;

.field private mReciver:Landroid/content/BroadcastReceiver;

.field private mRefreshHandler:Landroid/os/Handler;

.field private mScanFileReceiver:Landroid/content/BroadcastReceiver;

.field private mScanFileReceiverFilter:Landroid/content/IntentFilter;

.field private mfilter:Landroid/content/IntentFilter;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 71
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;-><init>()V

    .line 78
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mCategoryNameResId:I

    .line 79
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mReciver:Landroid/content/BroadcastReceiver;

    .line 80
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mfilter:Landroid/content/IntentFilter;

    .line 81
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mScanFileReceiver:Landroid/content/BroadcastReceiver;

    .line 82
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mScanFileReceiverFilter:Landroid/content/IntentFilter;

    .line 217
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$3;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mRefreshHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mRefreshHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->verifySelectedItem()Z

    move-result v0

    return v0
.end method

.method private setScanFileReceiver()V
    .locals 3

    .prologue
    .line 164
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mScanFileReceiverFilter:Landroid/content/IntentFilter;

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mScanFileReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mScanFileReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mScanFileReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mScanFileReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mScanFileReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 177
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$2;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mScanFileReceiver:Landroid/content/BroadcastReceiver;

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mScanFileReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mScanFileReceiverFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 214
    return-void
.end method

.method private verifySelectedItem()Z
    .locals 7

    .prologue
    .line 669
    const/4 v0, 0x0

    .line 673
    .local v0, "deletedFileExist":Z
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->getFragmentId()I

    move-result v5

    const/4 v6, 0x7

    if-ne v5, v6, :cond_1

    .line 676
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v4

    .line 678
    .local v4, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    const/4 v2, 0x0

    .line 680
    .local v2, "selectedFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 682
    .local v3, "selectedItem":Ljava/lang/Object;
    new-instance v2, Ljava/io/File;

    .end local v2    # "selectedFile":Ljava/io/File;
    check-cast v3, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .end local v3    # "selectedItem":Ljava/lang/Object;
    iget-object v5, v3, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 685
    .restart local v2    # "selectedFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    .line 687
    const/4 v0, 0x1

    .line 694
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "selectedFile":Ljava/io/File;
    .end local v4    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_1
    if-nez v0, :cond_2

    const/4 v5, 0x1

    :goto_0
    return v5

    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method


# virtual methods
.method public AsyncOpenCompleted()V
    .locals 0

    .prologue
    .line 933
    return-void
.end method

.method public getOptionsMenuResId(Z)I
    .locals 2
    .param p1, "isSelectMode"    # Z

    .prologue
    .line 635
    if-eqz p1, :cond_0

    .line 637
    const v0, 0x7f0e0018

    .line 647
    :goto_0
    return v0

    .line 641
    :cond_0
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mCategoryType:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_1

    .line 643
    const v0, 0x7f0e0014

    goto :goto_0

    .line 647
    :cond_1
    const v0, 0x7f0e0006

    goto :goto_0
.end method

.method public onBackPressed()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 395
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->initFileObserver()V

    .line 397
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 399
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mFragmentId:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->isSelectMode(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 401
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->getSelectModeFrom()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 402
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->finish()V

    .line 425
    :goto_0
    return v0

    .line 404
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->finishSelectMode()V

    goto :goto_0

    .line 410
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 412
    goto :goto_0

    .line 416
    :cond_2
    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader;->SCROLL_DONE:Z

    .line 417
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView()V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 425
    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1056
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1057
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mRunFrom:I

    const/16 v1, 0x15

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->getSelectionType()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 1058
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->selectmodeActionbar()V

    .line 1060
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 90
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onCreate(Landroid/os/Bundle;)V

    .line 92
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 94
    .local v0, "argument":Landroid/os/Bundle;
    const-string v1, "category_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mCategoryType:I

    .line 96
    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mCategoryType:I

    packed-switch v1, :pswitch_data_0

    .line 119
    :goto_0
    :pswitch_0
    const-string v1, "run_from"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const/16 v2, 0x15

    if-ne v1, v2, :cond_0

    .line 121
    const-string v1, "selection_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->startSelectMode(III)V

    .line 128
    :cond_0
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mfilter:Landroid/content/IntentFilter;

    .line 129
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mfilter:Landroid/content/IntentFilter;

    const-string v2, "Media_DB_Update_Finsished"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 130
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mfilter:Landroid/content/IntentFilter;

    const-string v2, "Media_DB_Update_Finsished_by_Scanner"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 131
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mfilter:Landroid/content/IntentFilter;

    const-string v2, "com.android.MTP.OBJECT_REMOVED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 133
    new-instance v1, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mReciver:Landroid/content/BroadcastReceiver;

    .line 157
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mReciver:Landroid/content/BroadcastReceiver;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mfilter:Landroid/content/IntentFilter;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 159
    return-void

    .line 99
    :pswitch_1
    const v1, 0x7f0b0003

    iput v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mCategoryNameResId:I

    goto :goto_0

    .line 103
    :pswitch_2
    const v1, 0x7f0b0004

    iput v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mCategoryNameResId:I

    goto :goto_0

    .line 107
    :pswitch_3
    const v1, 0x7f0b0147

    iput v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mCategoryNameResId:I

    goto :goto_0

    .line 111
    :pswitch_4
    const v1, 0x7f0b0006

    iput v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mCategoryNameResId:I

    goto :goto_0

    .line 115
    :pswitch_5
    const v1, 0x7f0b0012

    iput v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mCategoryNameResId:I

    goto :goto_0

    .line 96
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method protected onCreateDetailFragmentListener()Landroid/app/DialogFragment;
    .locals 1

    .prologue
    .line 915
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/DetailFragment;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/fragment/DetailFragment;-><init>()V

    return-object v0
.end method

.method protected onCreateFileOperationListener()Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;
    .locals 1

    .prologue
    .line 895
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$7;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;)V

    return-object v0
.end method

.method protected onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 701
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;)V

    return-object v0
.end method

.method protected onCreateItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;
    .locals 1

    .prologue
    .line 808
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$6;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;)V

    return-object v0
.end method

.method protected onCreateMultipleDetailFragmentListener()Landroid/app/DialogFragment;
    .locals 1

    .prologue
    .line 921
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/MultipleDetailFragment;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/fragment/MultipleDetailFragment;-><init>()V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 240
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 244
    .local v0, "view":Landroid/view/View;
    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mCategoryNameResId:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 246
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mTitleBarTextView:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mCategoryNameResId:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 249
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 251
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 253
    return-object v0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 259
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mReciver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 260
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mAdapterManager:Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    if-eqz v1, :cond_0

    .line 261
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mAdapterManager:Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mFragmentId:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getCategoryCursor(I)Landroid/database/Cursor;

    move-result-object v0

    .line 262
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 263
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 266
    .end local v0    # "c":Landroid/database/Cursor;
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onDestroy()V

    .line 267
    return-void
.end method

.method public onDirectDragStarted(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "argument"    # Landroid/os/Bundle;

    .prologue
    .line 656
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->verifySelectedItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 658
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->startDragMode(Landroid/view/View;Landroid/os/Bundle;)V

    .line 664
    :goto_0
    return-void

    .line 662
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v1, 0x7f0b005a

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 10
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v9, 0x0

    const/4 v8, -0x1

    const/4 v6, 0x1

    .line 538
    if-eqz p1, :cond_7

    .line 539
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    .line 602
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v5

    .line 605
    :goto_0
    return v5

    .line 543
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v5

    iget v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mFragmentId:I

    invoke-virtual {v5, v7}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->isSelectMode(I)Z

    move-result v5

    if-nez v5, :cond_1

    .line 545
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v5, v5, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/AbsMainActivity;->isSearchMode()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 549
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v5, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/MainActivity;->finishSearchMode()V

    .line 552
    :cond_0
    invoke-virtual {p0, v6, v8, v9}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->startSelectMode(III)V

    :cond_1
    move v5, v6

    .line 555
    goto :goto_0

    .line 558
    :sswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v5

    iget v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mFragmentId:I

    invoke-virtual {v5, v7}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->isSelectMode(I)Z

    move-result v5

    if-nez v5, :cond_3

    .line 560
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v5, v5, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/AbsMainActivity;->isSearchMode()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 564
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v5, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/MainActivity;->finishSearchMode()V

    .line 567
    :cond_2
    const/4 v5, 0x5

    invoke-virtual {p0, v6, v8, v5}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->startSelectMode(III)V

    :cond_3
    move v5, v6

    .line 569
    goto :goto_0

    .line 572
    :sswitch_2
    iget v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mCategoryType:I

    const/4 v7, 0x7

    if-ne v5, v7, :cond_6

    .line 574
    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v4

    .line 576
    .local v4, "uri":Landroid/net/Uri;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 578
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v2

    .line 580
    .local v2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 582
    .local v3, "selectedItem":Ljava/lang/Object;
    const-string v5, "_data = ?"

    new-array v7, v6, [Ljava/lang/String;

    check-cast v3, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .end local v3    # "selectedItem":Ljava/lang/Object;
    iget-object v8, v3, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    aput-object v8, v7, v9

    invoke-virtual {v0, v4, v5, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    if-lez v5, :cond_4

    .line 585
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->finishSelectMode()V

    .line 587
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->onRefresh()V

    goto :goto_1

    :cond_5
    move v5, v6

    .line 593
    goto/16 :goto_0

    .line 595
    .end local v0    # "contentResolver":Landroid/content/ContentResolver;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v4    # "uri":Landroid/net/Uri;
    :cond_6
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v5

    goto/16 :goto_0

    .line 598
    :sswitch_3
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v7, "com.vcast.mediamanager.ACTION_FILES"

    invoke-static {v5, v7}, Lcom/sec/android/app/myfiles/utils/VZCloudUtils;->launchVZCloud(Landroid/content/Context;Ljava/lang/String;)V

    move v5, v6

    .line 599
    goto/16 :goto_0

    :cond_7
    move v5, v6

    .line 605
    goto/16 :goto_0

    .line 539
    :sswitch_data_0
    .sparse-switch
        0x7f0f0121 -> :sswitch_2
        0x7f0f0137 -> :sswitch_0
        0x7f0f0138 -> :sswitch_1
        0x7f0f0139 -> :sswitch_3
    .end sparse-switch
.end method

.method public onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 322
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isMediaScannerScannig()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 323
    const-string v0, "CategoryBrowserFragment"

    const-string v1, "scanning but onPause state, hide progress"

    invoke-static {v2, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 324
    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->showWaitProgressDialog(Z)V

    .line 327
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    if-eqz v0, :cond_1

    .line 329
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mPrevListGridState:Landroid/os/Parcelable;

    .line 332
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v0, :cond_2

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;

    if-eqz v0, :cond_3

    .line 336
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    if-eqz v0, :cond_2

    .line 337
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->removeDelayedMessage()V

    .line 352
    :cond_2
    :goto_0
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onPause()V

    .line 353
    return-void

    .line 339
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter;

    if-eqz v0, :cond_4

    .line 341
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    if-eqz v0, :cond_2

    .line 342
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->removeDelayedMessage()V

    goto :goto_0

    .line 344
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;

    if-eqz v0, :cond_2

    .line 346
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    if-eqz v0, :cond_2

    .line 347
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->removeDelayedMessage()V

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 8
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v7, 0x7f0f013a

    const/4 v6, 0x7

    const/4 v5, 0x6

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 432
    const/4 v0, 0x0

    .line 434
    .local v0, "menuItem":Landroid/view/MenuItem;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-nez v1, :cond_1

    .line 532
    :cond_0
    :goto_0
    return-void

    .line 437
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->isSelectMode()Z

    move-result v1

    if-nez v1, :cond_4

    .line 439
    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 441
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 445
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v2, 0x7f0f0139

    invoke-static {v1, p1, v2}, Lcom/sec/android/app/myfiles/utils/VZCloudUtils;->prepareOptionsMenu(Landroid/content/Context;Landroid/view/Menu;I)V

    .line 523
    :cond_3
    :goto_1
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 525
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    if-eqz v1, :cond_0

    .line 527
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 529
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 449
    :cond_4
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    if-eqz v1, :cond_3

    .line 451
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v4, 0x7f0f012b

    invoke-interface {v1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 453
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 457
    :cond_5
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v4, 0x7f0f0123

    invoke-interface {v1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 459
    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mCategoryType:I

    if-eq v1, v6, :cond_e

    move v1, v2

    :goto_2
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 462
    :cond_6
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v4, 0x7f0f0122

    invoke-interface {v1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 464
    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mCategoryType:I

    if-eq v1, v6, :cond_f

    move v1, v2

    :goto_3
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 467
    :cond_7
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v4, 0x7f0f0128

    invoke-interface {v1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 469
    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mCategoryType:I

    if-eq v1, v6, :cond_10

    move v1, v2

    :goto_4
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 472
    :cond_8
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v4, 0x7f0f0124

    invoke-interface {v1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v1, :cond_9

    .line 473
    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mCategoryType:I

    if-eq v1, v5, :cond_12

    .line 474
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v1, v1, Lcom/sec/android/app/myfiles/MainActivity;->mEnableMoveToPrivate:Z

    if-eqz v1, :cond_11

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v1, v1, Lcom/sec/android/app/myfiles/MainActivity;->mDisableMoveToPrivate:Z

    if-nez v1, :cond_11

    .line 475
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 484
    :cond_9
    :goto_5
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v4, 0x7f0f0125

    invoke-interface {v1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_a

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v1, :cond_a

    .line 485
    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mCategoryType:I

    if-eq v1, v5, :cond_14

    .line 486
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v1, v1, Lcom/sec/android/app/myfiles/MainActivity;->mEnableMoveToPrivate:Z

    if-nez v1, :cond_13

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v1, v1, Lcom/sec/android/app/myfiles/MainActivity;->mDisableMoveToPrivate:Z

    if-eqz v1, :cond_13

    .line 487
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 496
    :cond_a
    :goto_6
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v4, 0x7f0f0126

    invoke-interface {v1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_c

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v1, :cond_c

    .line 497
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v1, v1, Lcom/sec/android/app/myfiles/MainActivity;->blockSendtoKNOX:Z

    if-nez v1, :cond_b

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isKNOXFileRelayAvailable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isKNOXMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 498
    :cond_b
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 508
    :cond_c
    :goto_7
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v4, 0x7f0f0127

    invoke-interface {v1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v1, :cond_3

    .line 510
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v1, v1, Lcom/sec/android/app/myfiles/MainActivity;->blockSendtoKNOX:Z

    if-nez v1, :cond_d

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isKNOXFileRelayAvailable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isKNOXMode(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_17

    .line 511
    :cond_d
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_1

    :cond_e
    move v1, v3

    .line 459
    goto/16 :goto_2

    :cond_f
    move v1, v3

    .line 464
    goto/16 :goto_3

    :cond_10
    move v1, v3

    .line 469
    goto/16 :goto_4

    .line 477
    :cond_11
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_5

    .line 480
    :cond_12
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_5

    .line 489
    :cond_13
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_6

    .line 492
    :cond_14
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_6

    .line 500
    :cond_15
    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mCategoryType:I

    if-eq v1, v5, :cond_16

    .line 501
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_7

    .line 503
    :cond_16
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_7

    .line 513
    :cond_17
    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mCategoryType:I

    if-eq v1, v5, :cond_18

    .line 514
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_1

    .line 516
    :cond_18
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_1
.end method

.method public onRefresh()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 364
    const-string v1, "CategoryBrowserFragment"

    const-string v2, "onRefresh"

    invoke-static {v3, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 366
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v0

    .line 368
    .local v0, "adapterManager":Lcom/sec/android/app/myfiles/adapter/AdapterManager;
    if-eqz v0, :cond_0

    .line 370
    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mFragmentId:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getCategoryCursor(I)Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 373
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mStartMoveToKnox:Z

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mCategoryType:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mCategoryType:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mCategoryType:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mCategoryType:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_2

    .line 377
    :cond_1
    new-instance v1, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$4;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;)V

    new-array v2, v3, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$4;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 389
    :cond_2
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 277
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onResume()V

    .line 279
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isMediaScannerScannig()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 280
    const-string v0, "CategoryBrowserFragment"

    const-string v1, "scanning and onResume state, show progress"

    invoke-static {v2, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 281
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->showWaitProgressDialog(Z)V

    .line 284
    :cond_0
    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->showPathIndicator(Z)V

    .line 286
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->onRefresh()V

    .line 288
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mPrevListGridState:Landroid/os/Parcelable;

    if-eqz v0, :cond_1

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    if-eqz v0, :cond_1

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mPrevListGridState:Landroid/os/Parcelable;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 296
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mCategoryType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->setCategoryItemListSelection(Ljava/lang/String;)I

    .line 315
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 271
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onStart()V

    .line 272
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->setScanFileReceiver()V

    .line 273
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 357
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onStop()V

    .line 358
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mScanFileReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 359
    return-void
.end method

.method protected selectAllItem()V
    .locals 1

    .prologue
    .line 612
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->selectAllItem()V

    .line 614
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 616
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->selectAllItem()V

    .line 618
    :cond_0
    return-void
.end method

.method public selectmodeActionbar()V
    .locals 8

    .prologue
    const v7, 0x7f0b0014

    const v6, 0x7f0b0013

    const/4 v4, 0x1

    const v5, 0x7f0b0018

    .line 940
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 941
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 942
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 943
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 951
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->getSelectionType()I

    move-result v2

    if-eq v2, v4, :cond_3

    .line 952
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f040008

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 953
    .local v0, "customview":Landroid/view/View;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 954
    const v2, 0x7f0f0008

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mActionbar_cancel:Landroid/widget/TextView;

    .line 955
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mActionbar_cancel:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f0b0017

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0, v5}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 956
    const v2, 0x7f0f0009

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    .line 957
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f0b0016

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0, v5}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 960
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v2, v2, Lcom/sec/android/app/myfiles/SelectorActivity;

    if-eqz v2, :cond_0

    .line 961
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v2, Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/SelectorActivity;->getmFileOperation()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 962
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v2, Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/SelectorActivity;->getmFileOperation()I

    move-result v2

    if-nez v2, :cond_2

    .line 963
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(I)V

    .line 964
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v6}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0, v5}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 971
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mActionbar_cancel:Landroid/widget/TextView;

    new-instance v3, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$8;

    invoke-direct {v3, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$8;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 987
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    new-instance v3, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$9;

    invoke-direct {v3, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$9;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1000
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v2, v2, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v2, :cond_1

    .line 1001
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1053
    .end local v0    # "customview":Landroid/view/View;
    :cond_1
    :goto_1
    return-void

    .line 945
    :catch_0
    move-exception v1

    .line 947
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_1

    .line 966
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "customview":Landroid/view/View;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(I)V

    .line 967
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0, v5}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1006
    .end local v0    # "customview":Landroid/view/View;
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f040009

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1007
    .restart local v0    # "customview":Landroid/view/View;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 1008
    const v2, 0x7f0f0009

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    .line 1010
    const v2, 0x7f0f0003

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mSelectAllButton:Landroid/widget/TextView;

    .line 1022
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    new-instance v3, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$10;

    invoke-direct {v3, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$10;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method protected unselectAllItem()V
    .locals 1

    .prologue
    .line 623
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->unselectAllItem()V

    .line 625
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 627
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->unselectAllItem()V

    .line 629
    :cond_0
    return-void
.end method
