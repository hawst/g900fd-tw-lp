.class Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;
.super Ljava/lang/Object;
.source "AbsFileOperationTask.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;)V
    .locals 0

    .prologue
    .line 892
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 897
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget-boolean v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->isAllOverwriteCheck:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mTempOverwriteFileCount:I

    if-ne v1, v4, :cond_5

    .line 898
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iput-boolean v4, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->isCancelled:Z

    .line 900
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRemainOverwriteFileCount:I

    if-nez v1, :cond_1

    .line 901
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->cancel(Z)Z

    .line 904
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iput-boolean v3, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRunRename:Z

    .line 905
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iput-boolean v3, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mOverwrite:Z

    .line 906
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRemainOverwriteFileCount:I

    if-nez v1, :cond_4

    .line 907
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iput-boolean v4, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRenameCancelled:Z

    .line 912
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget-object v2, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRunnable:Ljava/lang/Runnable;

    monitor-enter v2

    .line 916
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    const/4 v3, 0x0

    # setter for: Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mWaitDialog:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->access$002(Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;Z)Z

    .line 918
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 924
    :goto_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 926
    if-eqz p1, :cond_2

    .line 928
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 963
    :cond_2
    :goto_2
    if-eqz p1, :cond_3

    .line 965
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 967
    :cond_3
    return-void

    .line 909
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iput-boolean v3, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRenameCancelled:Z

    goto :goto_0

    .line 920
    :catch_0
    move-exception v0

    .line 922
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 924
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 932
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iput-boolean v3, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRunRename:Z

    .line 933
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iput-boolean v3, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mOverwrite:Z

    .line 934
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRemainOverwriteFileCount:I

    if-nez v1, :cond_8

    .line 935
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iput-boolean v4, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRenameCancelled:Z

    .line 940
    :goto_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRemainOverwriteFileCount:I

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mTempOverwriteFileCount:I

    if-nez v1, :cond_6

    .line 941
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->cancel(Z)Z

    .line 944
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget v2, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mTempOverwriteFileCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mTempOverwriteFileCount:I

    .line 945
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mTempOverwriteFileCount:I

    if-gt v1, v4, :cond_7

    .line 946
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iput v3, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mTempOverwriteFileCount:I

    .line 948
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget-object v2, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRunnable:Ljava/lang/Runnable;

    monitor-enter v2

    .line 952
    :try_start_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    const/4 v3, 0x0

    # setter for: Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mWaitDialog:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->access$002(Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;Z)Z

    .line 954
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 960
    :goto_4
    :try_start_4
    monitor-exit v2

    goto :goto_2

    :catchall_1
    move-exception v1

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v1

    .line 938
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$4;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iput-boolean v3, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRenameCancelled:Z

    goto :goto_3

    .line 956
    :catch_1
    move-exception v0

    .line 958
    .restart local v0    # "ex":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_4
.end method
