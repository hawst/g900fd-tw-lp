.class public Lcom/sec/android/app/myfiles/utils/MyFilesDialog;
.super Ljava/lang/Object;
.source "MyFilesDialog.java"


# static fields
.field private static final nMsg:I = 0xa


# instance fields
.field private mContext:Landroid/content/Context;

.field private mPleaseWaitString:Ljava/lang/String;

.field private mProgressHandler:Landroid/os/Handler;

.field private mWaitProgressDlg:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object v0, p0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->mContext:Landroid/content/Context;

    .line 36
    iput-object v0, p0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->mWaitProgressDlg:Landroid/app/ProgressDialog;

    .line 48
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/myfiles/utils/MyFilesDialog$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/utils/MyFilesDialog$1;-><init>(Lcom/sec/android/app/myfiles/utils/MyFilesDialog;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->mProgressHandler:Landroid/os/Handler;

    .line 44
    iput-object p1, p0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->mContext:Landroid/content/Context;

    .line 45
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/utils/MyFilesDialog;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->mPleaseWaitString:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/myfiles/utils/MyFilesDialog;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/utils/MyFilesDialog;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->mPleaseWaitString:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/utils/MyFilesDialog;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/utils/MyFilesDialog;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->mWaitProgressDlg:Landroid/app/ProgressDialog;

    return-object v0
.end method


# virtual methods
.method public dismissWaitProgressDialog()V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 91
    :goto_0
    return-void

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->mProgressHandler:Landroid/os/Handler;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->mPleaseWaitString:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->showWaitProgressDialog(Ljava/lang/String;Z)V

    .line 90
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->mWaitProgressDlg:Landroid/app/ProgressDialog;

    goto :goto_0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->mWaitProgressDlg:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->mWaitProgressDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    .line 98
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDisplayString(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->mPleaseWaitString:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public showWaitProgressDialog()V
    .locals 4

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->mProgressHandler:Landroid/os/Handler;

    const/16 v1, 0xa

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 70
    return-void
.end method

.method public showWaitProgressDialog(Ljava/lang/String;Z)V
    .locals 5
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "isShow"    # Z

    .prologue
    const/4 v4, 0x0

    .line 105
    iget-object v1, p0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->mContext:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 110
    :cond_1
    if-eqz p2, :cond_3

    .line 112
    iget-object v1, p0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->mWaitProgressDlg:Landroid/app/ProgressDialog;

    if-nez v1, :cond_2

    .line 114
    iget-object v1, p0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v1, v2, p1, v3, v4}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->mWaitProgressDlg:Landroid/app/ProgressDialog;

    .line 116
    iget-object v1, p0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->mWaitProgressDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 118
    iget-object v1, p0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->mWaitProgressDlg:Landroid/app/ProgressDialog;

    new-instance v2, Lcom/sec/android/app/myfiles/utils/MyFilesDialog$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/utils/MyFilesDialog$2;-><init>(Lcom/sec/android/app/myfiles/utils/MyFilesDialog;)V

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    goto :goto_0

    .line 159
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->mWaitProgressDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v1, p1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 161
    iget-object v1, p0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->mWaitProgressDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0

    .line 166
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->mWaitProgressDlg:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_0

    .line 168
    iget-object v1, p0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->mWaitProgressDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->mWaitProgressDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 171
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->mWaitProgressDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 172
    :catch_0
    move-exception v0

    .line 173
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0
.end method
