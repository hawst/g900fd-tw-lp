.class public Lcom/sec/android/app/myfiles/element/HistoryItem;
.super Ljava/lang/Object;
.source "HistoryItem.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;,
        Lcom/sec/android/app/myfiles/element/HistoryItem$Category;
    }
.end annotation


# static fields
.field private static final CAT_APPLICATION:I = 0x0

.field private static final CAT_FILE:I = 0x1

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/myfiles/element/HistoryItem;",
            ">;"
        }
    .end annotation
.end field

.field private static final DEFAULT_VERSION_STR:Ljava/lang/String; = "1.0"

.field private static final NOT_APPLICABLE_INT:I = -0x1

.field private static final NOT_APPLICABLE_STR:Ljava/lang/String; = "N/A"


# instance fields
.field private final mCategory:Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

.field private mData:Ljava/lang/String;

.field private final mDate:J

.field private final mDescription:Ljava/lang/String;

.field private final mExternalId:J

.field private final mMimeType:Ljava/lang/String;

.field private final mName:Ljava/lang/String;

.field private final mNameforPinyinSort:Ljava/lang/String;

.field private final mSize:J

.field private final mSource:Ljava/lang/String;

.field private final mStatus:I

.field private final mType:I

.field private final mVendor:Ljava/lang/String;

.field private final mVersion:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 452
    new-instance v0, Lcom/sec/android/app/myfiles/element/HistoryItem$1;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/element/HistoryItem$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/element/HistoryItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(ILcom/sec/android/app/myfiles/element/HistoryItem$Category;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "category"    # Lcom/sec/android/app/myfiles/element/HistoryItem$Category;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "nameforPinyinSort"    # Ljava/lang/String;
    .param p5, "date"    # J
    .param p7, "size"    # J
    .param p9, "data"    # Ljava/lang/String;
    .param p10, "source"    # Ljava/lang/String;
    .param p11, "externalId"    # J
    .param p13, "vendor"    # Ljava/lang/String;
    .param p14, "version"    # Ljava/lang/String;
    .param p15, "mimeType"    # Ljava/lang/String;
    .param p16, "description"    # Ljava/lang/String;
    .param p17, "status"    # I

    .prologue
    .line 201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 203
    iput p1, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mType:I

    .line 204
    iput-object p2, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mCategory:Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

    .line 205
    iput-object p3, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mName:Ljava/lang/String;

    .line 206
    iput-object p4, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mNameforPinyinSort:Ljava/lang/String;

    .line 207
    iput-wide p5, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mDate:J

    .line 208
    iput-wide p7, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mSize:J

    .line 209
    iput-object p9, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mData:Ljava/lang/String;

    .line 210
    iput-object p10, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mSource:Ljava/lang/String;

    .line 211
    iput-wide p11, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mExternalId:J

    .line 212
    iput-object p13, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mVendor:Ljava/lang/String;

    .line 213
    iput-object p14, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mVersion:Ljava/lang/String;

    .line 214
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mMimeType:Ljava/lang/String;

    .line 215
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mDescription:Ljava/lang/String;

    .line 216
    move/from16 v0, p17

    iput v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mStatus:I

    .line 218
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 227
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mCategory:Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

    .line 228
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mType:I

    .line 229
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mName:Ljava/lang/String;

    .line 230
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mNameforPinyinSort:Ljava/lang/String;

    .line 231
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mDate:J

    .line 232
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mSize:J

    .line 233
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mData:Ljava/lang/String;

    .line 234
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mSource:Ljava/lang/String;

    .line 235
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mExternalId:J

    .line 236
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mVendor:Ljava/lang/String;

    .line 237
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mVersion:Ljava/lang/String;

    .line 238
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mMimeType:Ljava/lang/String;

    .line 239
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mDescription:Ljava/lang/String;

    .line 240
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mStatus:I

    .line 242
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/app/myfiles/element/HistoryItem$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/android/app/myfiles/element/HistoryItem$1;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/element/HistoryItem;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getApplicationItem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)Lcom/sec/android/app/myfiles/element/HistoryItem;
    .locals 19
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "nameforPinyinSort"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "date"    # J
    .param p5, "size"    # J

    .prologue
    .line 133
    new-instance v1, Lcom/sec/android/app/myfiles/element/HistoryItem;

    const/4 v2, 0x0

    sget-object v3, Lcom/sec/android/app/myfiles/element/HistoryItem$Category;->APPLICATION:Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

    const-string v11, "N/A"

    const-wide/16 v12, -0x1

    const-string v14, "N/A"

    const-string v15, "N/A"

    const-string v16, "N/A"

    const-string v17, "N/A"

    const/16 v18, -0x1

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-object/from16 v10, p2

    invoke-direct/range {v1 .. v18}, Lcom/sec/android/app/myfiles/element/HistoryItem;-><init>(ILcom/sec/android/app/myfiles/element/HistoryItem$Category;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v1
.end method

.method public static getCorrectFileName(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "cr"    # Landroid/database/Cursor;
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "columnName"    # Ljava/lang/String;

    .prologue
    .line 415
    invoke-interface {p0, p2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 416
    .local v2, "name":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 417
    :cond_0
    const/4 v4, 0x0

    invoke-static {p1}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {p1, v4, v5}, Landroid/webkit/URLUtil;->guessFileName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 420
    :cond_1
    const-string v4, "."

    invoke-virtual {v2, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 421
    .local v3, "nameExtension":Ljava/lang/String;
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v1

    .line 422
    .local v1, "mimeTypeMap":Landroid/webkit/MimeTypeMap;
    if-eqz v3, :cond_2

    invoke-virtual {v1, v3}, Landroid/webkit/MimeTypeMap;->hasExtension(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 423
    :cond_2
    invoke-static {p1}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 424
    .local v0, "mimeTypeExtension":Ljava/lang/String;
    if-eqz v0, :cond_3

    invoke-virtual {v1, v0}, Landroid/webkit/MimeTypeMap;->hasExtension(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 425
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x2e

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {p1}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 427
    .end local v0    # "mimeTypeExtension":Ljava/lang/String;
    :cond_3
    return-object v2
.end method

.method public static getFileItem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJJLjava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/sec/android/app/myfiles/element/HistoryItem;
    .locals 20
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "nameforPinyinSort"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "mediaType"    # I
    .param p4, "date"    # J
    .param p6, "size"    # J
    .param p8, "source"    # Ljava/lang/String;
    .param p9, "externalId"    # J
    .param p11, "vendor"    # Ljava/lang/String;
    .param p12, "version"    # Ljava/lang/String;
    .param p13, "mimeType"    # Ljava/lang/String;
    .param p14, "description"    # Ljava/lang/String;
    .param p15, "status"    # I

    .prologue
    .line 169
    new-instance v1, Lcom/sec/android/app/myfiles/element/HistoryItem;

    sget-object v3, Lcom/sec/android/app/myfiles/element/HistoryItem$Category;->FILE:Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

    move/from16 v2, p3

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-wide/from16 v6, p4

    move-wide/from16 v8, p6

    move-object/from16 v10, p2

    move-object/from16 v11, p8

    move-wide/from16 v12, p9

    move-object/from16 v14, p11

    move-object/from16 v15, p12

    move-object/from16 v16, p13

    move-object/from16 v17, p14

    move/from16 v18, p15

    invoke-direct/range {v1 .. v18}, Lcom/sec/android/app/myfiles/element/HistoryItem;-><init>(ILcom/sec/android/app/myfiles/element/HistoryItem$Category;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v1
.end method

.method public static getFromCacheCursor(Landroid/database/Cursor;)Lcom/sec/android/app/myfiles/element/HistoryItem;
    .locals 21
    .param p0, "cr"    # Landroid/database/Cursor;

    .prologue
    .line 317
    if-eqz p0, :cond_0

    .line 319
    const-string v2, "_category"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/element/HistoryItem$Category;->getCategory(I)Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

    move-result-object v5

    .line 321
    .local v5, "category":Lcom/sec/android/app/myfiles/element/HistoryItem$Category;
    const-string v2, "_type"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 322
    .local v4, "type":I
    const-string v2, "_name"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 323
    .local v6, "name":Ljava/lang/String;
    const-string v2, "_name_for_pinyin_sort"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 324
    .local v7, "nameforPinyinSort":Ljava/lang/String;
    const-string v2, "_date"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 325
    .local v8, "date":J
    const-string v2, "_size"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 326
    .local v10, "size":J
    const-string v2, "_data"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 327
    .local v12, "data":Ljava/lang/String;
    const-string v2, "_source"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 328
    .local v13, "source":Ljava/lang/String;
    const-string v2, "_extern_id"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 329
    .local v14, "externalId":J
    const-string v2, "_vendor"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 330
    .local v16, "vendor":Ljava/lang/String;
    const-string v2, "_version"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 331
    .local v17, "version":Ljava/lang/String;
    const-string v2, "_mime_type"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 332
    .local v18, "mimeType":Ljava/lang/String;
    const-string v2, "_description"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 333
    .local v19, "description":Ljava/lang/String;
    const-string v2, "_status"

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    .line 335
    .local v20, "status":I
    new-instance v3, Lcom/sec/android/app/myfiles/element/HistoryItem;

    invoke-direct/range {v3 .. v20}, Lcom/sec/android/app/myfiles/element/HistoryItem;-><init>(ILcom/sec/android/app/myfiles/element/HistoryItem$Category;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v3

    .line 340
    .end local v4    # "type":I
    .end local v5    # "category":Lcom/sec/android/app/myfiles/element/HistoryItem$Category;
    .end local v6    # "name":Ljava/lang/String;
    .end local v7    # "nameforPinyinSort":Ljava/lang/String;
    .end local v8    # "date":J
    .end local v10    # "size":J
    .end local v12    # "data":Ljava/lang/String;
    .end local v13    # "source":Ljava/lang/String;
    .end local v14    # "externalId":J
    .end local v16    # "vendor":Ljava/lang/String;
    .end local v17    # "version":Ljava/lang/String;
    .end local v18    # "mimeType":Ljava/lang/String;
    .end local v19    # "description":Ljava/lang/String;
    .end local v20    # "status":I
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Cursor cannot be null."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public static getFromDownloadManagerCursor(Landroid/database/Cursor;)Lcom/sec/android/app/myfiles/element/HistoryItem;
    .locals 21
    .param p0, "cr"    # Landroid/database/Cursor;

    .prologue
    .line 352
    if-eqz p0, :cond_3

    .line 361
    const-string v3, "last_modified_timestamp"

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 362
    .local v8, "date":J
    const-string v3, "uri"

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 363
    .local v13, "source":Ljava/lang/String;
    const-string v3, "_id"

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 365
    .local v14, "externalId":J
    const-string v3, "status"

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    .line 366
    .local v20, "status":I
    const/16 v3, 0x10

    move/from16 v0, v20

    if-eq v0, v3, :cond_0

    .line 367
    const-string v3, "local_filename"

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 370
    .local v12, "path":Ljava/lang/String;
    :goto_0
    const/16 v3, 0x40

    move/from16 v0, v20

    if-ne v0, v3, :cond_1

    .line 371
    const-string v3, "dd_fileName"

    move-object/from16 v0, p0

    invoke-static {v0, v13, v3}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getCorrectFileName(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 372
    .local v6, "name":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->getIntance()Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->getSortKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    .line 373
    .local v7, "nameforPinyinSort":Ljava/lang/String;
    const-string v3, "dd_contentSize"

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 374
    .local v10, "size":J
    const-string v3, "dd_vendor"

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 375
    .local v16, "vendor":Ljava/lang/String;
    const-string v3, "dd_majorVersion"

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 376
    .local v2, "version":Ljava/lang/String;
    const-string v3, "dd_primaryMimeType"

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 377
    .local v18, "mimeType":Ljava/lang/String;
    const-string v3, "dd_description"

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 388
    .local v19, "description":Ljava/lang/String;
    :goto_1
    new-instance v3, Lcom/sec/android/app/myfiles/element/HistoryItem;

    invoke-static {v12}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v4

    sget-object v5, Lcom/sec/android/app/myfiles/element/HistoryItem$Category;->FILE:Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

    if-nez v2, :cond_2

    const-string v17, "1.0"

    :goto_2
    invoke-direct/range {v3 .. v20}, Lcom/sec/android/app/myfiles/element/HistoryItem;-><init>(ILcom/sec/android/app/myfiles/element/HistoryItem$Category;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v3

    .line 369
    .end local v2    # "version":Ljava/lang/String;
    .end local v6    # "name":Ljava/lang/String;
    .end local v7    # "nameforPinyinSort":Ljava/lang/String;
    .end local v10    # "size":J
    .end local v12    # "path":Ljava/lang/String;
    .end local v16    # "vendor":Ljava/lang/String;
    .end local v18    # "mimeType":Ljava/lang/String;
    .end local v19    # "description":Ljava/lang/String;
    :cond_0
    const-string v12, ""

    .restart local v12    # "path":Ljava/lang/String;
    goto :goto_0

    .line 379
    :cond_1
    const-string v3, "title"

    move-object/from16 v0, p0

    invoke-static {v0, v13, v3}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getCorrectFileName(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 380
    .restart local v6    # "name":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->getIntance()Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->getSortKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    .line 381
    .restart local v7    # "nameforPinyinSort":Ljava/lang/String;
    const-string v3, "total_size"

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 382
    .restart local v10    # "size":J
    const-string v16, "N/A"

    .line 383
    .restart local v16    # "vendor":Ljava/lang/String;
    const-string v2, "N/A"

    .line 384
    .restart local v2    # "version":Ljava/lang/String;
    const-string v3, "media_type"

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 385
    .restart local v18    # "mimeType":Ljava/lang/String;
    const-string v3, "description"

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .restart local v19    # "description":Ljava/lang/String;
    goto :goto_1

    .line 388
    :cond_2
    const-string v17, "N/A"

    goto :goto_2

    .line 408
    .end local v2    # "version":Ljava/lang/String;
    .end local v6    # "name":Ljava/lang/String;
    .end local v7    # "nameforPinyinSort":Ljava/lang/String;
    .end local v8    # "date":J
    .end local v10    # "size":J
    .end local v12    # "path":Ljava/lang/String;
    .end local v13    # "source":Ljava/lang/String;
    .end local v14    # "externalId":J
    .end local v16    # "vendor":Ljava/lang/String;
    .end local v18    # "mimeType":Ljava/lang/String;
    .end local v19    # "description":Ljava/lang/String;
    .end local v20    # "status":I
    :cond_3
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Cursor cannot be null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public static isOmaPendingFromCursor(Landroid/database/Cursor;)Z
    .locals 3
    .param p0, "cr"    # Landroid/database/Cursor;

    .prologue
    const/4 v1, 0x0

    .line 301
    if-eqz p0, :cond_0

    .line 303
    const-string v2, "_status"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 305
    .local v0, "status":I
    const/16 v2, 0x40

    if-ne v0, v2, :cond_0

    const/4 v1, 0x1

    .line 307
    .end local v0    # "status":I
    :cond_0
    return v1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 474
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 480
    instance-of v0, p1, Lcom/sec/android/app/myfiles/element/HistoryItem;

    if-eqz v0, :cond_1

    .line 481
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mExternalId:J

    check-cast p1, Lcom/sec/android/app/myfiles/element/HistoryItem;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getExternalId()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 482
    :goto_0
    return v0

    .line 481
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 482
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_1
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public getCategory()Lcom/sec/android/app/myfiles/element/HistoryItem$Category;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mCategory:Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

    return-object v0
.end method

.method public getContentValues()Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 273
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 275
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v1, "_name"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    const-string v1, "_name_for_pinyin_sort"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getNameforPinyinSort()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    const-string v1, "_date"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getDate()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 278
    const-string v1, "_size"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getSize()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 279
    const-string v1, "_type"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 280
    const-string v1, "_category"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getCategory()Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/HistoryItem$Category;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 281
    const-string v1, "_data"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getData()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    const-string v1, "_source"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getSource()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    const-string v1, "_extern_id"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getExternalId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 284
    const-string v1, "_vendor"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getVendor()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    const-string v1, "_version"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    const-string v1, "_mime_type"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getMimeType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    const-string v1, "_description"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    const-string v1, "_status"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getStatus()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 290
    return-object v0
.end method

.method public getData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mData:Ljava/lang/String;

    return-object v0
.end method

.method public getDate()J
    .locals 2

    .prologue
    .line 249
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mDate:J

    return-wide v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getExternalId()J
    .locals 2

    .prologue
    .line 253
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mExternalId:J

    return-wide v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 256
    iget-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getNameforPinyinSort()Ljava/lang/String;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mNameforPinyinSort:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 250
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mSize:J

    return-wide v0
.end method

.method public getSource()Ljava/lang/String;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mSource:Ljava/lang/String;

    return-object v0
.end method

.method public getStatus()I
    .locals 1

    .prologue
    .line 258
    iget v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mStatus:I

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 245
    iget v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mType:I

    return v0
.end method

.method public getVendor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mVendor:Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mVersion:Ljava/lang/String;

    return-object v0
.end method

.method public isOmaPending()Z
    .locals 2

    .prologue
    .line 264
    iget v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mStatus:I

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setData(Ljava/lang/String;)V
    .locals 1
    .param p1, "newData"    # Ljava/lang/String;

    .prologue
    .line 507
    iget-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mData:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 509
    iput-object p1, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mData:Ljava/lang/String;

    .line 510
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 433
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 435
    .local v0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "_type"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getType()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 436
    const-string v1, "_name"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 437
    const-string v1, "_name_for_pinyin_sort"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getNameforPinyinSort()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 438
    const-string v1, "_date"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getDate()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 439
    const-string v1, "_size"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getSize()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 440
    const-string v1, "_data"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getData()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 441
    const-string v1, "_vendor"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getVendor()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 442
    const-string v1, "_version"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 443
    const-string v1, "_mime_type"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getMimeType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 444
    const-string v1, "_description"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 446
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 488
    iget-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mCategory:Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 489
    iget v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 490
    iget-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 491
    iget-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mNameforPinyinSort:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 492
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mDate:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 493
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mSize:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 494
    iget-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mData:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 495
    iget-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mSource:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 496
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mExternalId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 497
    iget-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mVendor:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 498
    iget-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mVersion:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 499
    iget-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mMimeType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 500
    iget-object v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mDescription:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 501
    iget v0, p0, Lcom/sec/android/app/myfiles/element/HistoryItem;->mStatus:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 503
    return-void
.end method
