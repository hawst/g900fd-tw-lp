.class Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$1;
.super Ljava/lang/Object;
.source "BaiduFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)V
    .locals 0

    .prologue
    .line 228
    iput-object p1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$1;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 232
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$1;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$000(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->getCurrentItemIndex()I

    move-result v0

    .line 234
    .local v0, "index":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$1;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$100(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->getFirstVisiblePosition()I

    move-result v1

    if-lt v0, v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$1;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$200(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->getLastVisiblePosition()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 236
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$1;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$400(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$1;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$300(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->getCurrentItemIndex()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->setTreeViewYPos(I)V

    .line 238
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$1;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->restoreTreeYPosition()V

    .line 240
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$1;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$500(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 242
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$1;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$600(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->notifyUpdate(Z)V

    .line 244
    :cond_1
    return-void
.end method
