.class Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$1;
.super Ljava/lang/Object;
.source "AllFileBrowserAdapter.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

.field final synthetic val$filePath:Ljava/lang/String;

.field final synthetic val$format:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 343
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    iput p2, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$1;->val$format:I

    iput-object p3, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$1;->val$filePath:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 16
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    .line 348
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getAction()I

    move-result v1

    .line 350
    .local v1, "action":I
    const/4 v12, 0x0

    const-string v13, "AllFileBrowserAdapter"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "onDrag() receive : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 352
    packed-switch v1, :pswitch_data_0

    .line 476
    :cond_0
    :goto_0
    const/4 v12, 0x1

    :goto_1
    return v12

    .line 355
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    iget-boolean v12, v12, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mIsDragMode:Z

    if-nez v12, :cond_0

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$1;->val$format:I

    const/16 v13, 0x3001

    if-ne v12, v13, :cond_0

    .line 357
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mSelectedItemBackground:I
    invoke-static {v12}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)I

    move-result v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 362
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    iget-boolean v12, v12, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mIsDragMode:Z

    if-nez v12, :cond_0

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$1;->val$format:I

    const/16 v13, 0x3001

    if-ne v12, v13, :cond_0

    .line 364
    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 369
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    iget-boolean v12, v12, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mIsDragMode:Z

    if-nez v12, :cond_a

    .line 371
    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/view/View;->setBackgroundColor(I)V

    .line 373
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mDropListener:Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;

    if-eqz v12, :cond_9

    .line 375
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getClipData()Landroid/content/ClipData;

    move-result-object v2

    .line 377
    .local v2, "clipData":Landroid/content/ClipData;
    if-eqz v2, :cond_8

    invoke-virtual {v2}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v12

    if-eqz v12, :cond_8

    .line 379
    invoke-virtual {v2}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/ClipDescription;->getLabel()Ljava/lang/CharSequence;

    move-result-object v8

    .line 381
    .local v8, "label":Ljava/lang/CharSequence;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_7

    const-string v12, "selectedUri"

    invoke-virtual {v12, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_1

    const-string v13, "galleryURI"

    move-object v12, v8

    check-cast v12, Ljava/lang/String;

    invoke-virtual {v13, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_1

    const-string v12, "selectedFTPUri"

    invoke-virtual {v12, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_1

    const-string v12, "selectedCloudUri"

    invoke-virtual {v12, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 386
    :cond_1
    invoke-virtual {v2}, Landroid/content/ClipData;->getItemCount()I

    move-result v6

    .line 388
    .local v6, "itemCount":I
    const/4 v10, 0x0

    .line 390
    .local v10, "selectedFilePath":Ljava/lang/String;
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 392
    .local v9, "selectedFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    if-ge v4, v6, :cond_5

    .line 394
    invoke-virtual {v2, v4}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v7

    .line 396
    .local v7, "itemUri":Landroid/content/ClipData$Item;
    if-eqz v7, :cond_2

    .line 398
    invoke-virtual {v7}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v11

    .line 400
    .local v11, "uri":Landroid/net/Uri;
    if-eqz v11, :cond_4

    .line 402
    const-string v12, "selectedFTPUri"

    invoke-virtual {v12, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 404
    invoke-static {v11}, Lcom/sec/android/app/myfiles/ftp/FTPUtils;->getItemPathFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v10

    .line 418
    :goto_3
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_2

    .line 420
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 392
    .end local v11    # "uri":Landroid/net/Uri;
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 408
    .restart local v11    # "uri":Landroid/net/Uri;
    :cond_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$100(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Landroid/content/Context;

    move-result-object v12

    invoke-static {v12, v11}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getFilePathFromUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v10

    goto :goto_3

    .line 414
    :cond_4
    invoke-virtual {v7}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v12

    invoke-interface {v12}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    goto :goto_3

    .line 425
    .end local v7    # "itemUri":Landroid/content/ClipData$Item;
    .end local v11    # "uri":Landroid/net/Uri;
    :cond_5
    const/4 v3, 0x0

    .line 427
    .local v3, "dstFolder":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$1;->val$format:I

    const/16 v13, 0x3001

    if-ne v12, v13, :cond_6

    .line 429
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$1;->val$filePath:Ljava/lang/String;

    .line 436
    :goto_4
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mDropListener:Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;

    invoke-interface {v12, v3, v9}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;->onDrop(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 433
    :cond_6
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$1;->val$filePath:Ljava/lang/String;

    invoke-static {v12}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getParentFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_4

    .line 441
    .end local v3    # "dstFolder":Ljava/lang/String;
    .end local v4    # "i":I
    .end local v6    # "itemCount":I
    .end local v9    # "selectedFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v10    # "selectedFilePath":Ljava/lang/String;
    :cond_7
    const/4 v12, 0x0

    goto/16 :goto_1

    .line 447
    .end local v8    # "label":Ljava/lang/CharSequence;
    :cond_8
    const/4 v12, 0x0

    goto/16 :goto_1

    .line 453
    .end local v2    # "clipData":Landroid/content/ClipData;
    :cond_9
    const/4 v12, 0x0

    goto/16 :goto_1

    .line 458
    :cond_a
    new-instance v5, Landroid/content/Intent;

    const-string v12, "com.sec.android.action.FILE_OPERATION_DONE"

    invoke-direct {v5, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 460
    .local v5, "intent":Landroid/content/Intent;
    const-string v12, "success"

    const/4 v13, 0x0

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 462
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$100(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 469
    .end local v5    # "intent":Landroid/content/Intent;
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    iget-boolean v12, v12, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mIsDragMode:Z

    if-eqz v12, :cond_0

    .line 471
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    invoke-virtual {v12}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->finishDrag()V

    goto/16 :goto_0

    .line 352
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
