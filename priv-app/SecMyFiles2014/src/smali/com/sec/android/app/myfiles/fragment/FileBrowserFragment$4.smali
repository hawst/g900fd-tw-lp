.class Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$4;
.super Ljava/lang/Object;
.source "FileBrowserFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->onRefresh()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)V
    .locals 0

    .prologue
    .line 532
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 537
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mLoadingHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 539
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mTempCursor:Landroid/database/Cursor;

    .line 540
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mTempCursor:Landroid/database/Cursor;

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 542
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mLoadingHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 543
    return-void
.end method
