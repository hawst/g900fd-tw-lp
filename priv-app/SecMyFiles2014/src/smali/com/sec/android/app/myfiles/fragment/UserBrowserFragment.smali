.class public Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;
.source "UserBrowserFragment.java"


# static fields
.field private static final MODULE:Ljava/lang/String; = "UserBrowserFragment"

.field private static final REQUEST_CREATE_FOLDER:I = 0x4

.field private static final REQUEST_EXTRACT:I = 0x6

.field private static final REQUEST_ZIP:I = 0x5


# instance fields
.field public mCurrentFolder:Ljava/lang/String;

.field private mExtractExecuteNormalMode:Z

.field private mFromFindoFile:Ljava/lang/String;

.field private mFromShortcutSelector:Z

.field private mIsFolderFromFindo:Z

.field private mModeFromFindo:Z

.field private mMtpRemoveReceiver:Landroid/content/BroadcastReceiver;

.field private mPathChangeListener:Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;

.field private mPreviousfolder:Ljava/lang/String;

.field private mReceiverIsRegistered:Z

.field private mRemoveUSBStorage:Landroid/content/BroadcastReceiver;

.field private mSaveStartPathFromFindo:Ljava/lang/String;

.field private mScanFileReceiver:Landroid/content/BroadcastReceiver;

.field private mSelectedPathForExtract:Ljava/lang/String;

.field private mShortcutIndex:I

.field private startFolder:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 73
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;-><init>()V

    .line 84
    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mFromFindoFile:Ljava/lang/String;

    .line 86
    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mSaveStartPathFromFindo:Ljava/lang/String;

    .line 88
    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mPreviousfolder:Ljava/lang/String;

    .line 92
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mIsFolderFromFindo:Z

    .line 94
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mModeFromFindo:Z

    .line 96
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mReceiverIsRegistered:Z

    .line 100
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mExtractExecuteNormalMode:Z

    .line 102
    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mSelectedPathForExtract:Ljava/lang/String;

    .line 104
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mFromShortcutSelector:Z

    .line 106
    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->startFolder:Ljava/lang/String;

    .line 108
    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mCurrentFolder:Ljava/lang/String;

    .line 110
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mScanFileReceiver:Landroid/content/BroadcastReceiver;

    .line 121
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$2;-><init>(Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mMtpRemoveReceiver:Landroid/content/BroadcastReceiver;

    .line 1182
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$6;-><init>(Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mPathChangeListener:Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/Boolean;

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->moveToFolder(Ljava/lang/String;Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic access$102(Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mExtractExecuteNormalMode:Z

    return p1
.end method

.method static synthetic access$202(Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mSelectedPathForExtract:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->createFolderForExtract(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private createFolderForExtract(Ljava/lang/String;)Z
    .locals 8
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x6

    const/4 v5, 0x0

    .line 889
    const/4 v2, 0x0

    .line 893
    .local v2, "fileName":Ljava/lang/String;
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_0

    .line 895
    invoke-static {p1}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v3

    .line 897
    .local v3, "fileTypeInt":I
    invoke-static {v3}, Lcom/sec/android/app/myfiles/MediaFile;->isArchiveFileType(I)Z

    move-result v6

    if-nez v6, :cond_1

    .line 899
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v7, "Please select .zip file"

    invoke-static {v6, v7, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 961
    .end local v3    # "fileTypeInt":I
    :cond_0
    :goto_0
    return v5

    .line 904
    .restart local v3    # "fileTypeInt":I
    :cond_1
    const/16 v6, 0x2f

    invoke-virtual {p1, v6}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {p1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 906
    const-string v6, ".zip"

    invoke-virtual {v2, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 908
    const/16 v6, 0x2e

    invoke-virtual {v2, v6}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 910
    .local v1, "dotIndex":I
    if-ltz v1, :cond_2

    .line 912
    invoke-virtual {v2, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 941
    .end local v1    # "dotIndex":I
    :cond_2
    new-instance v4, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    invoke-direct {v4}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;-><init>()V

    .line 943
    .local v4, "inputName":Landroid/app/DialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 945
    .local v0, "argument":Landroid/os/Bundle;
    const-string v5, "title"

    const v6, 0x7f0b0074

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 947
    const-string v5, "kind_of_operation"

    invoke-virtual {v0, v5, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 949
    const-string v5, "current_item"

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 951
    const-string v5, "file_name"

    invoke-virtual {v0, v5, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 953
    const-string v5, "src_fragment_id"

    iget v6, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mFragmentId:I

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 955
    invoke-virtual {v4, v0}, Landroid/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 957
    invoke-virtual {v4, p0, v7}, Landroid/app/DialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 959
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const-string v6, "extract"

    invoke-virtual {v4, v5, v6}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 961
    const/4 v5, 0x1

    goto :goto_0
.end method

.method private extractNotification(Landroid/content/Intent;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    .line 798
    const-string v2, "used"

    invoke-virtual {p1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 800
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 802
    .local v1, "uri":Landroid/net/Uri;
    if-eqz v1, :cond_1

    .line 804
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getFilePathFromUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 806
    .local v0, "path":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v2, :cond_0

    .line 808
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getParentFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z

    .line 811
    :cond_0
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mExtractExecuteNormalMode:Z

    .line 813
    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mSelectedPathForExtract:Ljava/lang/String;

    .line 815
    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->createFolderForExtract(Ljava/lang/String;)Z

    .line 817
    .end local v0    # "path":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private moveToFolder(Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 6
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "isBack"    # Ljava/lang/Boolean;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 1220
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mCurrentFolder:Ljava/lang/String;

    .line 1221
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v2, p1, p2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1222
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setCurrentDirectory(Ljava/lang/String;)V

    .line 1224
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mIsSelector:Z

    .line 1225
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 1228
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v0

    .line 1230
    .local v0, "c":Landroid/database/Cursor;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v2, :cond_1

    .line 1232
    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 1239
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->setPositionZero()V

    .line 1241
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getSelectModeFrom()I

    move-result v2

    if-nez v2, :cond_2

    .line 1243
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->isSelectMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1245
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->finishSelectMode()V

    .line 1265
    :cond_0
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->invalidateOptionsMenu()V

    .line 1272
    .end local v0    # "c":Landroid/database/Cursor;
    :goto_2
    return-void

    .line 1236
    .restart local v0    # "c":Landroid/database/Cursor;
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1250
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getSelectionType()I

    move-result v2

    if-eq v2, v5, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getSelectionType()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_0

    .line 1253
    sget-object v2, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    if-eqz v2, :cond_0

    .line 1255
    sget-object v2, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v3, 0x7f0f0148

    invoke-interface {v2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1257
    .local v1, "item":Landroid/view/MenuItem;
    if-eqz v1, :cond_0

    .line 1259
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 1269
    .end local v0    # "c":Landroid/database/Cursor;
    .end local v1    # "item":Landroid/view/MenuItem;
    :cond_3
    const-string v2, "UserBrowserFragment"

    const-string v3, "Can\'t go to the folder"

    invoke-static {v4, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1270
    const-string v2, "UserBrowserFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "=> path = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private setExternalStorageReceiver()V
    .locals 3

    .prologue
    .line 1276
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1279
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1281
    const-string v1, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1283
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 1285
    new-instance v1, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$8;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$8;-><init>(Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mRemoveUSBStorage:Landroid/content/BroadcastReceiver;

    .line 1332
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mRemoveUSBStorage:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1333
    return-void
.end method

.method private setPositionZero()V
    .locals 2

    .prologue
    .line 1205
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$7;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$7;-><init>(Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->post(Ljava/lang/Runnable;)Z

    .line 1213
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setSelection(I)V

    .line 1216
    return-void
.end method

.method private startCompress(Ljava/lang/String;)V
    .locals 11
    .param p1, "targetZipFile"    # Ljava/lang/String;

    .prologue
    .line 851
    const/4 v8, 0x3

    invoke-static {v8}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(I)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v3

    .line 855
    .local v3, "fragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    const/4 v8, 0x2

    :try_start_0
    invoke-virtual {v3, p0, v8}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setTargetFragment(Landroid/app/Fragment;I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 862
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v6

    .line 864
    .local v6, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 866
    .local v7, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 868
    .local v5, "selectedItem":Ljava/lang/Object;
    check-cast v5, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .end local v5    # "selectedItem":Ljava/lang/Object;
    iget-object v8, v5, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 857
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v6    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v7    # "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_0
    move-exception v1

    .line 885
    :cond_0
    :goto_1
    return-void

    .line 871
    .restart local v4    # "i$":Ljava/util/Iterator;
    .restart local v6    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .restart local v7    # "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 873
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v8, "src_folder"

    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 875
    const-string v8, "dst_folder"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 877
    const-string v8, "target_datas"

    invoke-virtual {v0, v8, v7}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 879
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    .line 881
    .local v2, "fm":Landroid/app/FragmentManager;
    if-eqz v2, :cond_0

    .line 883
    const-string v8, "compress"

    invoke-virtual {v3, v2, v8}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private startExtract(Ljava/lang/String;)V
    .locals 10
    .param p1, "targetFoler"    # Ljava/lang/String;

    .prologue
    .line 966
    const/4 v8, 0x4

    invoke-static {v8}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(I)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v3

    .line 970
    .local v3, "fragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    const/4 v8, 0x2

    :try_start_0
    invoke-virtual {v3, p0, v8}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setTargetFragment(Landroid/app/Fragment;I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 977
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 979
    .local v7, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-boolean v8, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mExtractExecuteNormalMode:Z

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mSelectedPathForExtract:Ljava/lang/String;

    if-eqz v8, :cond_2

    .line 981
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mSelectedPathForExtract:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 993
    :cond_0
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mExtractExecuteNormalMode:Z

    .line 995
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 997
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v8, "src_folder"

    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 999
    const-string v8, "dst_folder"

    invoke-virtual {v0, v8, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1001
    const-string v8, "target_datas"

    invoke-virtual {v0, v8, v7}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1003
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    .line 1005
    .local v2, "fm":Landroid/app/FragmentManager;
    if-eqz v2, :cond_1

    .line 1007
    const-string v8, "extract"

    invoke-virtual {v3, v2, v8}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 1009
    .end local v0    # "arguments":Landroid/os/Bundle;
    .end local v2    # "fm":Landroid/app/FragmentManager;
    .end local v7    # "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    :goto_0
    return-void

    .line 972
    :catch_0
    move-exception v1

    .line 974
    .local v1, "ex":Ljava/lang/IllegalStateException;
    goto :goto_0

    .line 985
    .end local v1    # "ex":Ljava/lang/IllegalStateException;
    .restart local v7    # "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v6

    .line 987
    .local v6, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 989
    .local v5, "selectedItem":Ljava/lang/Object;
    check-cast v5, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .end local v5    # "selectedItem":Ljava/lang/Object;
    iget-object v8, v5, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method public AsyncOpenCompleted()V
    .locals 0

    .prologue
    .line 1337
    return-void
.end method

.method public addCurrentFolderToShortcut()V
    .locals 7

    .prologue
    .line 788
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v1

    .line 790
    .local v1, "path":Ljava/lang/String;
    sget-char v0, Ljava/io/File;->separatorChar:C

    invoke-virtual {v1, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 792
    .local v2, "shortcutName":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/16 v3, 0x1a

    const/4 v4, -0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/myfiles/utils/Utils;->addShortCut(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/drawable/Drawable;Z)V

    .line 794
    return-void
.end method

.method public getOptionsMenuResId(Z)I
    .locals 1
    .param p1, "isSelectMode"    # Z

    .prologue
    .line 1015
    const v0, 0x7f0e0018

    return v0
.end method

.method protected initTreeView()V
    .locals 2

    .prologue
    .line 742
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->initTreeView()V

    .line 750
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->setNavigation(Lcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 754
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->setAutoExpand(Z)V

    .line 756
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->setOnDropListener(Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;)V

    .line 758
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 759
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v3, -0x1

    .line 701
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 703
    packed-switch p1, :pswitch_data_0

    .line 737
    :cond_0
    :goto_0
    return-void

    .line 707
    :pswitch_0
    if-ne p2, v3, :cond_0

    .line 709
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "result_value"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".zip"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 711
    .local v2, "targetZipFile":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->startCompress(Ljava/lang/String;)V

    goto :goto_0

    .line 719
    .end local v2    # "targetZipFile":Ljava/lang/String;
    :pswitch_1
    if-ne p2, v3, :cond_0

    .line 721
    const-string v3, "result_value"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 723
    .local v0, "targetFolderName":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 725
    .local v1, "targetFolderPath":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->startExtract(Ljava/lang/String;)V

    goto :goto_0

    .line 733
    .end local v0    # "targetFolderName":Ljava/lang/String;
    .end local v1    # "targetFolderPath":Ljava/lang/String;
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->onRefresh()V

    goto :goto_0

    .line 703
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onBackPressed()Z
    .locals 13

    .prologue
    const/4 v12, -0x1

    const v11, 0x7f0f0148

    const/4 v10, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 402
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-nez v5, :cond_1

    .line 404
    const-string v5, "UserBrowserFragment"

    const-string v7, "mNavigation == null"

    invoke-static {v6, v5, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    move v7, v6

    .line 583
    :cond_0
    :goto_0
    return v7

    .line 409
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mPreviousfolder:Ljava/lang/String;

    .line 410
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mPreviousfolder:Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->startFolder:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 411
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 412
    .local v1, "b":Landroid/os/Bundle;
    const-string v8, "shortcut_working_index_intent_key"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string v9, "shortcut_working_index_intent_key"

    invoke-virtual {v5, v9, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    :goto_1
    invoke-virtual {v1, v8, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 413
    const-string v5, "id"

    const/16 v6, 0x24

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 414
    const-string v5, "FOLDERPATH"

    const-string v6, "/"

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v5, v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView(Landroid/os/Bundle;)V

    goto :goto_0

    :cond_2
    move v5, v6

    .line 412
    goto :goto_1

    .line 420
    .end local v1    # "b":Landroid/os/Bundle;
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getSelectModeFrom()I

    move-result v5

    if-eq v5, v10, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getSelectModeFrom()I

    move-result v5

    if-nez v5, :cond_5

    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->isSelectMode()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 423
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->finishSelectMode()V

    goto :goto_0

    .line 425
    :cond_5
    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mIsHomeBtnSelected:Z

    if-nez v5, :cond_d

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->isSelectMode()Z

    move-result v5

    if-nez v5, :cond_d

    .line 426
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goBack()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 428
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v3

    .line 430
    .local v3, "path":Ljava/lang/String;
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct {p0, v3, v5}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->moveToFolder(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 432
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isCheckMountedStorage()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 434
    const/4 v2, 0x0

    .line 436
    .local v2, "menuItem":Landroid/view/MenuItem;
    sget-object v5, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v5, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 438
    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isRootFolder(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_6

    move v6, v7

    :cond_6
    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 443
    .end local v2    # "menuItem":Landroid/view/MenuItem;
    .end local v3    # "path":Ljava/lang/String;
    :cond_7
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    iget-object v3, v5, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mWrongUpPath:Ljava/lang/String;

    .line 444
    .restart local v3    # "path":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isCheckMountedStorage()Z

    move-result v5

    if-nez v5, :cond_8

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isInExternalSdStorage(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    sget-boolean v5, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v5, :cond_9

    :cond_8
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isCheckMountedStorage()Z

    move-result v5

    if-eqz v5, :cond_a

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isUsbStorageFolder(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_a

    sget-boolean v5, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-nez v5, :cond_a

    .line 447
    :cond_9
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getHistoricalPath()Ljava/lang/String;

    move-result-object v4

    .line 448
    .local v4, "prePath":Ljava/lang/String;
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->moveToFolder(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/16 :goto_0

    .line 449
    .end local v4    # "prePath":Ljava/lang/String;
    :cond_a
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isCheckMountedStorage()Z

    move-result v5

    if-nez v5, :cond_b

    .line 450
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView()V

    goto/16 :goto_0

    .line 452
    :cond_b
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mPreviousfolder:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_c

    .line 453
    const-string v5, "/storage"

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-direct {p0, v5, v6}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->moveToFolder(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/16 :goto_0

    .line 455
    :cond_c
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView()V

    goto/16 :goto_0

    .line 461
    .end local v3    # "path":Ljava/lang/String;
    :cond_d
    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mIsHomeBtnSelected:Z

    if-nez v5, :cond_e

    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mIsSelector:Z

    if-nez v5, :cond_15

    .line 462
    :cond_e
    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mIsHomeBtnSelected:Z

    .line 464
    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mIsSelector:Z

    .line 466
    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mIsShortcutFolder:Z

    if-eqz v5, :cond_11

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mShortcutRootFolder:Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 469
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getSelectModeFrom()I

    move-result v5

    if-eqz v5, :cond_f

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getSelectModeFrom()I

    move-result v5

    if-eq v5, v10, :cond_f

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getSelectModeFrom()I

    move-result v5

    if-ne v5, v12, :cond_10

    .line 473
    :cond_f
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 474
    .restart local v1    # "b":Landroid/os/Bundle;
    const-string v5, "shortcut_working_index_intent_key"

    iget v6, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mShortcutIndex:I

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 475
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v5, v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView(Landroid/os/Bundle;)V

    .line 476
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->clearHistoricalPaths()V

    goto/16 :goto_0

    .line 481
    .end local v1    # "b":Landroid/os/Bundle;
    :cond_10
    const-string v5, "UserBrowserFragment"

    const-string v7, " failed to go up"

    invoke-static {v6, v5, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    move v7, v6

    .line 483
    goto/16 :goto_0

    .line 486
    :cond_11
    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mModeFromFindo:Z

    if-eqz v5, :cond_12

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mPreviousfolder:Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mSaveStartPathFromFindo:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_12

    .line 488
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v5, :cond_0

    .line 489
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/AbsMainActivity;->finish()V

    goto/16 :goto_0

    .line 493
    :cond_12
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goUp()Z

    move-result v5

    if-eqz v5, :cond_19

    .line 496
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->isSelectMode()Z

    move-result v5

    if-eqz v5, :cond_13

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v5, :cond_13

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v5

    if-lez v5, :cond_13

    .line 498
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->unselectAllItem()V

    .line 501
    :cond_13
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v3

    .line 503
    .restart local v3    # "path":Ljava/lang/String;
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct {p0, v3, v5}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->moveToFolder(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 505
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v5, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mPreviousfolder:Ljava/lang/String;

    invoke-virtual {v5, v9}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->getPositionFromPath(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v8, v5, v6}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setSelectionFromTop(II)V

    .line 507
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isCheckMountedStorage()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 509
    const/4 v2, 0x0

    .line 511
    .restart local v2    # "menuItem":Landroid/view/MenuItem;
    sget-object v5, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v5, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 513
    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isRootFolder(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_14

    move v6, v7

    :cond_14
    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 520
    .end local v2    # "menuItem":Landroid/view/MenuItem;
    .end local v3    # "path":Ljava/lang/String;
    :cond_15
    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mIsSelector:Z

    if-eqz v5, :cond_19

    .line 521
    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mIsSelector:Z

    .line 523
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string v8, "ISSELECTOR"

    iget-boolean v9, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mIsSelector:Z

    invoke-virtual {v5, v8, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 525
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string v8, "RETURNPATH"

    invoke-virtual {v5, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_17

    .line 527
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 528
    .local v0, "argument":Landroid/os/Bundle;
    const-string v5, "FOLDERPATH"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v8, "RETURNPATH"

    invoke-virtual {v6, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    const-string v5, "RETURNPATH"

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    const-string v5, "src_fragment_id"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v8, "src_fragment_id"

    invoke-virtual {v6, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 531
    const-string v5, "dest_fragment_id"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v8, "dest_fragment_id"

    invoke-virtual {v6, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 532
    const-string v5, "from_back"

    invoke-virtual {v0, v5, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 534
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "src-ftp-param"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_16

    .line 536
    const-string v5, "src-ftp-param"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v8, "src-ftp-param"

    invoke-virtual {v6, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    :cond_16
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->clearHistoricalPaths()V

    .line 539
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mCallback:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$onCopyMoveSelectedListener;

    const-string v6, "FOLDERPATH"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v0, v6}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$onCopyMoveSelectedListener;->onCopyMoveSelected(Landroid/os/Bundle;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 542
    .end local v0    # "argument":Landroid/os/Bundle;
    :cond_17
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goBack()Z

    move-result v5

    if-eqz v5, :cond_19

    .line 544
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v3

    .line 546
    .restart local v3    # "path":Ljava/lang/String;
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-direct {p0, v3, v5}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->moveToFolder(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 548
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isCheckMountedStorage()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 550
    const/4 v2, 0x0

    .line 552
    .restart local v2    # "menuItem":Landroid/view/MenuItem;
    sget-object v5, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v5, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 554
    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isRootFolder(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_18

    move v6, v7

    :cond_18
    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 561
    .end local v2    # "menuItem":Landroid/view/MenuItem;
    .end local v3    # "path":Ljava/lang/String;
    :cond_19
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getSelectModeFrom()I

    move-result v5

    if-eqz v5, :cond_1a

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getSelectModeFrom()I

    move-result v5

    if-eq v5, v10, :cond_1a

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getSelectModeFrom()I

    move-result v5

    if-ne v5, v12, :cond_1b

    .line 564
    :cond_1a
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->clearHistoricalPaths()V

    .line 565
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView()V

    goto/16 :goto_0

    .line 568
    :cond_1b
    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mFromShortcutSelector:Z

    if-eqz v5, :cond_1d

    .line 569
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 570
    .restart local v1    # "b":Landroid/os/Bundle;
    const-string v5, "shortcut_working_index_intent_key"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v8

    if-eqz v8, :cond_1c

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "shortcut_working_index_intent_key"

    invoke-virtual {v8, v9, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    :cond_1c
    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 571
    const-string v5, "id"

    const/16 v6, 0x24

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 572
    const-string v5, "FOLDERPATH"

    const-string v6, "/"

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 573
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v5, v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 577
    .end local v1    # "b":Landroid/os/Bundle;
    :cond_1d
    const-string v5, "UserBrowserFragment"

    const-string v7, " failed to go up"

    invoke-static {v6, v5, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    move v7, v6

    .line 579
    goto/16 :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1351
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1352
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mRunFrom:I

    const/16 v1, 0x15

    if-ne v0, v1, :cond_0

    .line 1353
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->selectmodeActionbar()V

    .line 1355
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 137
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onCreate(Landroid/os/Bundle;)V

    .line 139
    const/16 v0, 0x201

    iput v0, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mCategoryType:I

    .line 140
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 141
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "selection_type"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->setmSelectionType(I)V

    .line 143
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 144
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->setExternalStorageReceiver()V

    .line 146
    :cond_1
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mRunFrom:I

    const/16 v1, 0x15

    if-ne v0, v1, :cond_2

    .line 147
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->selectmodeActionbar()V

    .line 149
    :cond_2
    return-void
.end method

.method protected onCreateDetailFragmentListener()Landroid/app/DialogFragment;
    .locals 1

    .prologue
    .line 1171
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/DetailFragment;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/fragment/DetailFragment;-><init>()V

    return-object v0
.end method

.method protected onCreateFileOperationListener()Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;
    .locals 1

    .prologue
    .line 1151
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$5;-><init>(Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;)V

    return-object v0
.end method

.method protected onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 1023
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$3;-><init>(Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;)V

    return-object v0
.end method

.method protected onCreateItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;
    .locals 1

    .prologue
    .line 1092
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$4;-><init>(Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;)V

    return-object v0
.end method

.method protected onCreateMultipleDetailFragmentListener()Landroid/app/DialogFragment;
    .locals 1

    .prologue
    .line 1178
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/MultipleDetailFragment;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/fragment/MultipleDetailFragment;-><init>()V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 155
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v6

    .line 157
    .local v6, "view":Landroid/view/View;
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mPathChangeListener:Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setOnPathChangeListener(Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;)V

    .line 158
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getSelectionType()I

    move-result v7

    if-le v7, v10, :cond_0

    .line 159
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mCreateFolderContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 162
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 164
    .local v0, "argument":Landroid/os/Bundle;
    if-eqz v0, :cond_1

    .line 166
    const-string v7, "FOLDERPATH"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->startFolder:Ljava/lang/String;

    .line 168
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->startFolder:Ljava/lang/String;

    iput-object v7, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mCurrentFolder:Ljava/lang/String;

    .line 170
    const-string v7, "shortcut_working_index_intent_key"

    invoke-virtual {v0, v7, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    iput v7, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mShortcutIndex:I

    .line 172
    const-string v7, "from_findo_file_position"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mFromFindoFile:Ljava/lang/String;

    .line 174
    const-string v7, "from_findo_is_folder"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    iput-boolean v7, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mIsFolderFromFindo:Z

    .line 176
    const-string v7, "shortcut_mode_enable_intent_key"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    iput-boolean v7, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mFromShortcutSelector:Z

    .line 178
    const-string v7, "hide_storage"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v2

    .line 180
    .local v2, "hideStorages":[I
    if-eqz v2, :cond_1

    .line 182
    move-object v1, v2

    .local v1, "arr$":[I
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget v5, v1, v3

    .line 184
    .local v5, "storage":I
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v7, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    invoke-virtual {v7, v5}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->addHideStorage(I)V

    .line 182
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 189
    .end local v1    # "arr$":[I
    .end local v2    # "hideStorages":[I
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "storage":I
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->startFolder:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 192
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->startFolder:Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 194
    const-string v7, "UserBrowserFragment"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "fail to move the startFolder : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->startFolder:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "so go to the root"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v11, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 196
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goToRoot(Ljava/lang/Boolean;)Z

    .line 205
    :cond_2
    :goto_1
    return-object v6

    .line 201
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goToRoot(Ljava/lang/Boolean;)Z

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 356
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onDestroy()V

    .line 358
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 360
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mRemoveUSBStorage:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 364
    :cond_0
    return-void
.end method

.method public onDrop(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 7
    .param p1, "targetFolder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 298
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v6, "activity"

    invoke-virtual {v4, v6}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 300
    .local v0, "activityManager":Landroid/app/ActivityManager;
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v1

    .line 302
    .local v1, "infos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-nez v1, :cond_0

    .line 333
    :goto_0
    return-void

    .line 306
    :cond_0
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v4, v4, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const-string v6, "com.sec.android.app.myfiles"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v4, v4, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const-string v6, "com.sec.android.app.myfiles"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v2, v5

    .line 309
    .local v2, "move":Z
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDragSourceFragmentId()I

    move-result v3

    .line 311
    .local v3, "srcFragment":I
    packed-switch v3, :pswitch_data_0

    .line 329
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onDrop(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 315
    :pswitch_0
    if-eqz v2, :cond_2

    .line 317
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getFragmentId()I

    move-result v4

    invoke-virtual {p0, v4, p1, p2, v5}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->handleOnDropFromFtp(ILjava/lang/String;Ljava/util/ArrayList;Z)V

    goto :goto_0

    .line 321
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onDrop(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 311
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 15
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 590
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v11

    sparse-switch v11, :sswitch_data_0

    .line 694
    invoke-super/range {p0 .. p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v11

    :goto_0
    return v11

    .line 594
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->addCurrentFolderToShortcut()V

    .line 596
    const/4 v11, 0x1

    goto :goto_0

    .line 601
    :sswitch_1
    new-instance v1, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    invoke-direct {v1}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;-><init>()V

    .line 603
    .local v1, "createFolder":Landroid/app/DialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 605
    .local v0, "argument":Landroid/os/Bundle;
    const-string v11, "title"

    const v12, 0x7f0b0019

    invoke-virtual {v0, v11, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 607
    const-string v11, "kind_of_operation"

    const/4 v12, 0x4

    invoke-virtual {v0, v11, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 609
    const-string v11, "current_folder"

    iget-object v12, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v12}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v11, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 611
    const-string v11, "src_fragment_id"

    iget v12, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mFragmentId:I

    invoke-virtual {v0, v11, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 613
    invoke-virtual {v1, v0}, Landroid/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 615
    const/4 v11, 0x4

    invoke-virtual {v1, p0, v11}, Landroid/app/DialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 617
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v11

    const-string v12, "createFolder"

    invoke-virtual {v1, v11, v12}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 619
    const/4 v11, 0x1

    goto :goto_0

    .line 624
    .end local v0    # "argument":Landroid/os/Bundle;
    .end local v1    # "createFolder":Landroid/app/DialogFragment;
    :sswitch_2
    new-instance v7, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    invoke-direct {v7}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;-><init>()V

    .line 626
    .local v7, "newName":Landroid/app/DialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 628
    .restart local v0    # "argument":Landroid/os/Bundle;
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v10

    .line 630
    .local v10, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .line 632
    .local v9, "selectedItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    const-string v11, "current_item"

    iget-object v12, v9, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    invoke-virtual {v0, v11, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    const-string v11, "title"

    const v12, 0x7f0b0027

    invoke-virtual {v0, v11, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 636
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v5

    .line 638
    .local v5, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    if-eqz v5, :cond_2

    const/4 v11, 0x0

    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    if-eqz v11, :cond_2

    .line 640
    const/4 v11, 0x0

    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/myfiles/element/BrowserItem;

    iget-object v8, v11, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    .line 642
    .local v8, "path":Ljava/lang/String;
    sget-char v11, Ljava/io/File;->separatorChar:C

    invoke-virtual {v8, v11}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    invoke-virtual {v8, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 644
    .local v3, "fileName":Ljava/lang/String;
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v11

    const/4 v12, 0x1

    if-le v11, v12, :cond_0

    .line 645
    const-string v11, "file_name"

    iget-object v12, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v12}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v12

    iget-object v13, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v13

    add-int/lit8 v13, v13, 0x1

    invoke-virtual {v12, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v11, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 659
    .end local v3    # "fileName":Ljava/lang/String;
    .end local v8    # "path":Ljava/lang/String;
    :goto_1
    invoke-virtual {v7, v0}, Landroid/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 661
    const/4 v11, 0x5

    invoke-virtual {v7, p0, v11}, Landroid/app/DialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 663
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v11

    const-string v12, "zip"

    invoke-virtual {v7, v11, v12}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 665
    const/4 v11, 0x1

    goto/16 :goto_0

    .line 647
    .restart local v3    # "fileName":Ljava/lang/String;
    .restart local v8    # "path":Ljava/lang/String;
    :cond_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 648
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 649
    const-string v11, "file_name"

    invoke-virtual {v0, v11, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 651
    :cond_1
    const-string v11, "file_name"

    const/4 v12, 0x0

    const-string v13, "."

    invoke-virtual {v3, v13}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v13

    invoke-virtual {v3, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v11, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 656
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "fileName":Ljava/lang/String;
    .end local v8    # "path":Ljava/lang/String;
    :cond_2
    const-string v11, "name"

    const-string v12, ""

    invoke-virtual {v0, v11, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 677
    .end local v0    # "argument":Landroid/os/Bundle;
    .end local v5    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v7    # "newName":Landroid/app/DialogFragment;
    .end local v9    # "selectedItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    .end local v10    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :sswitch_3
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v6

    .line 679
    .local v6, "listTemp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    const/4 v11, 0x0

    invoke-virtual {v6, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/myfiles/element/BrowserItem;

    iget-object v4, v11, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    .line 681
    .local v4, "filePath":Ljava/lang/String;
    const/4 v11, 0x0

    iput-boolean v11, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mExtractExecuteNormalMode:Z

    .line 682
    const/4 v11, 0x0

    iput-object v11, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mSelectedPathForExtract:Ljava/lang/String;

    .line 683
    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->createFolderForExtract(Ljava/lang/String;)Z

    move-result v11

    goto/16 :goto_0

    .line 688
    .end local v4    # "filePath":Ljava/lang/String;
    .end local v6    # "listTemp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :sswitch_4
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v11}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->startExtract(Ljava/lang/String;)V

    .line 690
    const/4 v11, 0x1

    goto/16 :goto_0

    .line 590
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0f0129 -> :sswitch_0
        0x7f0f012b -> :sswitch_2
        0x7f0f012c -> :sswitch_3
        0x7f0f012d -> :sswitch_4
        0x7f0f013a -> :sswitch_1
    .end sparse-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 339
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onPause()V

    .line 342
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mReceiverIsRegistered:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 344
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mScanFileReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 346
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mMtpRemoveReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 348
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mReceiverIsRegistered:Z

    .line 351
    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v2, 0x7f0f013a

    .line 1341
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 1342
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1343
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1344
    .local v0, "item":Landroid/view/MenuItem;
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1346
    .end local v0    # "item":Landroid/view/MenuItem;
    :cond_0
    return-void
.end method

.method public onRefresh()V
    .locals 4

    .prologue
    .line 370
    const/4 v1, 0x0

    const-string v2, "UserBrowserFragment"

    const-string v3, "refresh"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 372
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v0

    .line 374
    .local v0, "adapterManager":Lcom/sec/android/app/myfiles/adapter/AdapterManager;
    if-eqz v0, :cond_0

    .line 376
    const/16 v1, 0x201

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->refreshAdapter(I)V

    .line 380
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v1, :cond_2

    .line 382
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->refreshNavigation()V

    .line 384
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    if-eqz v1, :cond_1

    .line 386
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 390
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 392
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->invalidateOptionsMenu()V

    .line 396
    :cond_2
    return-void
.end method

.method public onResume()V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 227
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onResume()V

    .line 228
    const-string v3, "UserBrowserFragment"

    const-string v4, "onResume"

    invoke-static {v6, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 230
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 232
    .local v2, "intent":Landroid/content/Intent;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v3

    const-string v4, "application/zip"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "used"

    invoke-virtual {v2, v3, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_0

    .line 235
    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->extractNotification(Landroid/content/Intent;)V

    .line 238
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->onRefresh()V

    .line 241
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getSelectModeFrom()I

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->validateSelectedItems()Z

    move-result v3

    if-nez v3, :cond_1

    .line 244
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->finishSelectMode()V

    .line 248
    :cond_1
    invoke-virtual {p0, v5}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->showPathIndicator(Z)V

    .line 252
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mReceiverIsRegistered:Z

    if-nez v3, :cond_2

    .line 254
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 256
    .local v0, "filter":Landroid/content/IntentFilter;
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 258
    .local v1, "filter2":Landroid/content/IntentFilter;
    const-string v3, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 260
    const-string v3, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 262
    const-string v3, "file"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 264
    const-string v3, "com.android.MTP.OBJECT_REMOVED"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 266
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 268
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mScanFileReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v4, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 270
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mMtpRemoveReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v4, v1}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 272
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mReceiverIsRegistered:Z

    .line 279
    .end local v0    # "filter":Landroid/content/IntentFilter;
    .end local v1    # "filter2":Landroid/content/IntentFilter;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mFromFindoFile:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 281
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mModeFromFindo:Z

    .line 282
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v3, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mFromFindoFile:Ljava/lang/String;

    invoke-virtual {v3, v5}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->getPositionFromPath(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v4, v3}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setSelection(I)V

    .line 284
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mIsFolderFromFindo:Z

    if-eqz v3, :cond_3

    .line 285
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mFromFindoFile:Ljava/lang/String;

    iput-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mSaveStartPathFromFindo:Ljava/lang/String;

    .line 293
    :goto_0
    return-void

    .line 287
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mFromFindoFile:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mFromFindoFile:Ljava/lang/String;

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mSaveStartPathFromFindo:Ljava/lang/String;

    goto :goto_0

    .line 291
    :cond_4
    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mModeFromFindo:Z

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 211
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 213
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 215
    .local v0, "argument":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    const-string v1, "run_from"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const/16 v2, 0x15

    if-ne v1, v2, :cond_0

    .line 218
    const-string v1, "selection_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->startSelectMode(III)V

    .line 222
    :cond_0
    return-void
.end method

.method protected selectAllItem()V
    .locals 2

    .prologue
    .line 764
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->selectAllItem()V

    .line 765
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 766
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getSelectModeFrom()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 767
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->selectAllFileItem()V

    .line 772
    :cond_0
    :goto_0
    return-void

    .line 769
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->selectAllItem()V

    goto :goto_0
.end method

.method public selectmodeActionbar()V
    .locals 7

    .prologue
    const v6, 0x7f0b0014

    const v5, 0x7f0b0013

    const/4 v3, 0x1

    const/4 v2, 0x0

    const v4, 0x7f0b0018

    .line 1359
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 1360
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 1361
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 1362
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 1364
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getSelectionType()I

    move-result v1

    if-eq v1, v3, :cond_2

    .line 1365
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040008

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1367
    .local v0, "customview":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 1368
    const v1, 0x7f0f0008

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mActionbar_cancel:Landroid/widget/TextView;

    .line 1369
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mActionbar_cancel:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0b0017

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1370
    const v1, 0x7f0f0009

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    .line 1371
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0b0016

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1374
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/SelectorActivity;

    if-eqz v1, :cond_0

    .line 1375
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->getmFileOperation()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 1376
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->getmFileOperation()I

    move-result v1

    if-nez v1, :cond_1

    .line 1377
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1378
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v5}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1385
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mActionbar_cancel:Landroid/widget/TextView;

    new-instance v2, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$9;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$9;-><init>(Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1406
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    new-instance v2, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$10;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$10;-><init>(Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1417
    return-void

    .line 1380
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(I)V

    .line 1381
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v6}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1402
    .end local v0    # "customview":Landroid/view/View;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/high16 v2, 0x7f040000

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1403
    .restart local v0    # "customview":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 1404
    const v1, 0x7f0f0003

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mSelectAllButton:Landroid/widget/TextView;

    goto :goto_1
.end method

.method protected unselectAllItem()V
    .locals 1

    .prologue
    .line 777
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->unselectAllItem()V

    .line 779
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 781
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->unselectAllItem()V

    .line 783
    :cond_0
    return-void
.end method
