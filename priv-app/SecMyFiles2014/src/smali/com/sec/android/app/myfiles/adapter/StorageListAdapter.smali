.class public Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;
.super Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
.source "StorageListAdapter.java"


# instance fields
.field private categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

.field private mContext:Landroid/content/Context;

.field private mIsSelectMode:Z

.field private mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "categoryHomeFragment"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->mContext:Landroid/content/Context;

    .line 23
    iput-object p2, p0, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .line 24
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isMountedStateCheck(Landroid/content/Context;)V

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->mItems:Ljava/util/ArrayList;

    .line 26
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->loadStorages()V

    .line 27
    return-void
.end method


# virtual methods
.method public finishSelectMode()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->mIsSelectMode:Z

    .line 39
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->notifyDataSetChanged()V

    .line 41
    return-void
.end method

.method public getCount()I
    .locals 3

    .prologue
    .line 78
    const/4 v0, 0x0

    .line 80
    .local v0, "value":I
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v2

    .line 82
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 83
    monitor-exit v2

    .line 85
    return v0

    .line 83
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 91
    const/4 v1, 0x0

    .line 95
    .local v1, "obj":Ljava/lang/Object;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 98
    monitor-exit v3

    .line 105
    .end local v1    # "obj":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 98
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 100
    :catch_0
    move-exception v0

    .line 102
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 111
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getSelectedItem()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSelectedItemCount()I
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    return v0
.end method

.method public getSelectedItemPosition()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    const/4 v0, 0x0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v8, 0x0

    .line 116
    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->mContext:Landroid/content/Context;

    const-string v7, "layout_inflater"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 117
    .local v1, "inflater":Landroid/view/LayoutInflater;
    if-nez p2, :cond_0

    .line 118
    const v6, 0x7f040031

    invoke-virtual {v1, v6, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 120
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 121
    .local v2, "item":Ljava/lang/String;
    const v6, 0x7f0f0031

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 122
    .local v0, "icon":Landroid/widget/ImageView;
    const v6, 0x7f0f0032

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 124
    .local v4, "title":Landroid/widget/TextView;
    sget-object v6, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 125
    const v6, 0x7f020022

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 126
    const v6, 0x7f0b003e

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(I)V

    .line 152
    :cond_1
    :goto_0
    iget-boolean v6, p0, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->mIsSelectMode:Z

    if-eqz v6, :cond_a

    .line 153
    invoke-virtual {p2, v8}, Landroid/view/View;->setEnabled(Z)V

    .line 154
    const v6, 0x3ecccccd    # 0.4f

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 155
    const v6, -0x777778

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 161
    :goto_1
    return-object p2

    .line 127
    :cond_2
    const-string v6, "/storage/extSdCard"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 128
    const v6, 0x7f02002f

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 129
    const v6, 0x7f0b003f

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 130
    :cond_3
    sget-object v6, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 131
    const v6, 0x7f02002a

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 132
    const v6, 0x7f0b0151

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 134
    :cond_4
    const v6, 0x7f020069

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 135
    const/16 v6, 0x2f

    invoke-virtual {v2, v6}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    .line 136
    .local v3, "lastDot":I
    add-int/lit8 v6, v3, 0x1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 137
    .local v5, "usbname":Ljava/lang/String;
    const-string v6, "UsbDriveA"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 138
    const v6, 0x7f0b0042

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 139
    :cond_5
    const-string v6, "UsbDriveB"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 140
    const v6, 0x7f0b0043

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 141
    :cond_6
    const-string v6, "UsbDriveC"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 142
    const v6, 0x7f0b0044

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 143
    :cond_7
    const-string v6, "UsbDriveD"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 144
    const v6, 0x7f0b0045

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 145
    :cond_8
    const-string v6, "UsbDriveE"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 146
    const v6, 0x7f0b0046

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 147
    :cond_9
    const-string v6, "UsbDriveF"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 148
    const v6, 0x7f0b0047

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 157
    .end local v3    # "lastDot":I
    .end local v5    # "usbname":Ljava/lang/String;
    :cond_a
    const/4 v6, 0x1

    invoke-virtual {p2, v6}, Landroid/view/View;->setEnabled(Z)V

    .line 158
    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 159
    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080019

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_1
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 187
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->mIsSelectMode:Z

    if-nez v0, :cond_0

    .line 188
    const/4 v0, 0x1

    .line 190
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSelectMode()Z
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    return v0
.end method

.method public loadStorages()V
    .locals 5

    .prologue
    .line 170
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->mItems:Ljava/util/ArrayList;

    sget-object v4, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 171
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretMode(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 172
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->mItems:Ljava/util/ArrayList;

    sget-object v4, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 174
    :cond_0
    sget-boolean v3, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v3, :cond_1

    .line 175
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->mItems:Ljava/util/ArrayList;

    const-string v4, "/storage/extSdCard"

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 177
    :cond_1
    sget-boolean v3, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-eqz v3, :cond_2

    .line 178
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 179
    .local v2, "usbstorage":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;>;"
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getMountedUsbStorage()Ljava/util/ArrayList;

    move-result-object v2

    .line 180
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;

    .line 181
    .local v0, "addusb":Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->getStoragePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 184
    .end local v0    # "addusb":Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "usbstorage":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;>;"
    :cond_2
    return-void
.end method

.method public onRefresh()V
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 166
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->loadStorages()V

    .line 167
    invoke-super {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;->onRefresh()V

    .line 168
    return-void
.end method

.method public selectAllItem()V
    .locals 0

    .prologue
    .line 54
    return-void
.end method

.method public selectItem(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 50
    return-void
.end method

.method public selectMode(Z)V
    .locals 0
    .param p1, "selectMode"    # Z

    .prologue
    .line 196
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->mIsSelectMode:Z

    .line 197
    return-void
.end method

.method public startSelectMode(II)V
    .locals 1
    .param p1, "selectType"    # I
    .param p2, "from"    # I

    .prologue
    .line 30
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->mIsSelectMode:Z

    .line 32
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->notifyDataSetChanged()V

    .line 33
    return-void
.end method

.method public unselectAllItem()V
    .locals 0

    .prologue
    .line 58
    return-void
.end method
