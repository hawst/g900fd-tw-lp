.class Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$4;
.super Ljava/lang/Object;
.source "DownloadedAppAdapter.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;)V
    .locals 0

    .prologue
    .line 637
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$4;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;)I
    .locals 5
    .param p1, "lhs"    # Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    .param p2, "rhs"    # Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .prologue
    .line 642
    const/4 v2, 0x0

    .line 643
    .local v2, "result":I
    const-string v0, ""

    .line 644
    .local v0, "entryLocaleName1":Ljava/lang/String;
    const-string v1, ""

    .line 646
    .local v1, "entryLocaleName2":Ljava/lang/String;
    iget-object v3, p1, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->label:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p2, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->label:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 648
    iget-object v3, p1, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->label:Ljava/lang/String;

    iget-object v4, p2, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->label:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    .line 650
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v3

    const-string v4, "CscFeature_MyFiles_EnablePinyinSort"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 651
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->getIntance()Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;

    move-result-object v3

    iget-object v4, p1, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->label:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->getSortKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 652
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->getIntance()Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;

    move-result-object v3

    iget-object v4, p2, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->label:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->getSortKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 653
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$4;->compareName(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 657
    :goto_0
    return v3

    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$4;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    iget v3, v3, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mCurrentInOrder:I

    if-nez v3, :cond_1

    move v3, v2

    goto :goto_0

    :cond_1
    neg-int v3, v2

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 637
    check-cast p1, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$4;->compare(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;)I

    move-result v0

    return v0
.end method

.method public compareName(Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p1, "entry1"    # Ljava/lang/String;
    .param p2, "entry2"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 661
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    .line 662
    .local v0, "collator":Ljava/text/Collator;
    invoke-virtual {v0, v2}, Ljava/text/Collator;->setStrength(I)V

    .line 663
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$4;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    iget v1, v1, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mCurrentInOrder:I

    if-nez v1, :cond_0

    .line 664
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 668
    :goto_0
    return v1

    .line 665
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$4;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    iget v1, v1, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mCurrentInOrder:I

    if-ne v1, v2, :cond_1

    .line 666
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    .line 668
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
