.class public Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;
.super Ljava/lang/Object;
.source "FTPOperationArg.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDialogSync:Ljava/lang/Object;

.field private final mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;Ljava/lang/Object;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "ftpHandler"    # Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    .param p3, "dialogSync"    # Ljava/lang/Object;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->mContext:Landroid/content/Context;

    .line 29
    iput-object p2, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    .line 30
    iput-object p3, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->mDialogSync:Ljava/lang/Object;

    .line 31
    return-void
.end method


# virtual methods
.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getDialogSync()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->mDialogSync:Ljava/lang/Object;

    return-object v0
.end method

.method public getFTPConnectionHandler()Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    return-object v0
.end method
