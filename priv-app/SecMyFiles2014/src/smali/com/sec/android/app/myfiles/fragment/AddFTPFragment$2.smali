.class Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$2;
.super Ljava/lang/Object;
.source "AddFTPFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mAnonymousView:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mAnonymousView:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mUsernameView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPasswordView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mUsernameView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPasswordView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 164
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->validate()V

    .line 165
    return-void

    .line 155
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mAnonymousView:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPortView:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)Landroid/widget/EditText;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPortView:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPortView:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 159
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mUsernameView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPasswordView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mUsernameView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPasswordView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0
.end method
