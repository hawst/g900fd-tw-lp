.class Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$8;
.super Ljava/lang/Object;
.source "AddFTPFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

.field final synthetic val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;Lcom/sec/android/app/myfiles/utils/SharedDataStore;)V
    .locals 0

    .prologue
    .line 507
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    iput-object p2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$8;->val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/16 v3, 0xc

    .line 513
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 514
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->removeDialog(I)V

    .line 532
    :goto_0
    return-void

    .line 518
    :cond_0
    if-nez p2, :cond_1

    .line 520
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mModeView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->access$800(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    const v2, 0x7f0b00ba

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 527
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->validate()V

    .line 529
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$8;->val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setCurrentView(I)V

    .line 531
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->removeDialog(I)V

    goto :goto_0

    .line 524
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mModeView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->access$800(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    const v2, 0x7f0b00bb

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
