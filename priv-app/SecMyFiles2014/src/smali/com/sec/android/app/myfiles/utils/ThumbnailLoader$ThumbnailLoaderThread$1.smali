.class Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailLoaderThread$1;
.super Ljava/lang/Object;
.source "ThumbnailLoader.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailLoaderThread;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailLoaderThread;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailLoaderThread;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailLoaderThread$1;->this$0:Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailLoaderThread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 86
    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader;->SCROLL_DONE:Z

    if-nez v2, :cond_0

    .line 88
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;

    .line 89
    .local v1, "info":Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailLoaderThread$1;->this$0:Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailLoaderThread;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailLoaderThread;->mBackThreadHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailLoaderThread$1;->this$0:Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailLoaderThread;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailLoaderThread;->mBackThreadHandler:Landroid/os/Handler;

    invoke-virtual {v3, v6, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 124
    :goto_0
    return v7

    .line 94
    .end local v1    # "info":Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;
    :cond_0
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;

    .line 98
    .restart local v1    # "info":Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;
    :try_start_0
    iget v2, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mViewMode:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_1

    iget v2, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mFileType:I

    invoke-static {v2}, Lcom/sec/android/app/myfiles/MediaFile;->isDocumentFileType(I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 100
    :cond_1
    iget-object v2, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mThumbnailInfoContext:Landroid/content/Context;

    iget v3, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mCategoryType:I

    iget v4, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mFileType:I

    iget-object v5, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mPath:Ljava/lang/String;

    invoke-static {v2, v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->getThumbnailDrawableWithMakeCache(Landroid/content/Context;IILjava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mThumbnailDrawable:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    :cond_2
    :goto_1
    iget v2, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mScrollState:I

    if-eqz v2, :cond_3

    .line 113
    const-wide/16 v2, 0xc8

    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 121
    :cond_3
    :goto_2
    # getter for: Lcom/sec/android/app/myfiles/utils/ThumbnailLoader;->sThumbnailUpdateHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader;->access$000()Landroid/os/Handler;

    move-result-object v2

    # getter for: Lcom/sec/android/app/myfiles/utils/ThumbnailLoader;->sThumbnailUpdateHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader;->access$000()Landroid/os/Handler;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    const/4 v5, -0x1

    invoke-virtual {v3, v6, v4, v5, v1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    goto :goto_0

    .line 103
    :catch_0
    move-exception v0

    .line 105
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 115
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 117
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2
.end method
