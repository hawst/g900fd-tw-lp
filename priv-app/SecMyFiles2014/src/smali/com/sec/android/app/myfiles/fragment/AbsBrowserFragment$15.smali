.class Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$15;
.super Ljava/lang/Object;
.source "AbsBrowserFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

.field final synthetic val$checkDoNotShow:Landroid/widget/CheckBox;

.field final synthetic val$dialogId:I

.field final synthetic val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;ILcom/sec/android/app/myfiles/utils/SharedDataStore;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 2812
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$15;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iput p2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$15;->val$dialogId:I

    iput-object p3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$15;->val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    iput-object p4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$15;->val$checkDoNotShow:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x0

    .line 2817
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 2819
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$15;->val$dialogId:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$15;->val$dialogId:I

    if-ne v0, v3, :cond_3

    .line 2821
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$15;->val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$15;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->bDoNotCheckDropBox:Z
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setDoNotShowCheckDropBox(Z)V

    .line 2822
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$15;->val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->commitShowDataWarning()Z

    .line 2823
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$15;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v0, :cond_1

    .line 2824
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$15;->val$dialogId:I

    if-ne v0, v3, :cond_2

    .line 2825
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$15;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    const/16 v1, 0x1f

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    .line 2837
    :cond_1
    :goto_0
    return-void

    .line 2827
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$15;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    const/16 v1, 0x8

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 2830
    :cond_3
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$15;->val$dialogId:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 2831
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$15;->val$checkDoNotShow:Landroid/widget/CheckBox;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$15;->val$checkDoNotShow:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2832
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$15;->val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setDoNotShowCheckFTP(Z)V

    .line 2833
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$15;->val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->commitShowDataWarning()Z

    .line 2835
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$15;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    const/16 v1, 0x11

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto :goto_0
.end method
