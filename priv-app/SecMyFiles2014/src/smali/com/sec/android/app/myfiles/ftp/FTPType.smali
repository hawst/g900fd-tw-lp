.class public final enum Lcom/sec/android/app/myfiles/ftp/FTPType;
.super Ljava/lang/Enum;
.source "FTPType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/myfiles/ftp/FTPType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/myfiles/ftp/FTPType;

.field public static final enum FTP:Lcom/sec/android/app/myfiles/ftp/FTPType;

.field public static final enum FTPS:Lcom/sec/android/app/myfiles/ftp/FTPType;

.field public static final enum SFTP:Lcom/sec/android/app/myfiles/ftp/FTPType;


# instance fields
.field public final ID:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 20
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPType;

    const-string v1, "FTP"

    invoke-direct {v0, v1, v2, v2}, Lcom/sec/android/app/myfiles/ftp/FTPType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPType;->FTP:Lcom/sec/android/app/myfiles/ftp/FTPType;

    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPType;

    const-string v1, "FTPS"

    invoke-direct {v0, v1, v3, v3}, Lcom/sec/android/app/myfiles/ftp/FTPType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPType;->FTPS:Lcom/sec/android/app/myfiles/ftp/FTPType;

    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPType;

    const-string v1, "SFTP"

    invoke-direct {v0, v1, v4, v4}, Lcom/sec/android/app/myfiles/ftp/FTPType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPType;->SFTP:Lcom/sec/android/app/myfiles/ftp/FTPType;

    .line 19
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/myfiles/ftp/FTPType;

    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPType;->FTP:Lcom/sec/android/app/myfiles/ftp/FTPType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPType;->FTPS:Lcom/sec/android/app/myfiles/ftp/FTPType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPType;->SFTP:Lcom/sec/android/app/myfiles/ftp/FTPType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPType;->$VALUES:[Lcom/sec/android/app/myfiles/ftp/FTPType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "id"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 25
    iput p3, p0, Lcom/sec/android/app/myfiles/ftp/FTPType;->ID:I

    .line 26
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 19
    const-class v0, Lcom/sec/android/app/myfiles/ftp/FTPType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/ftp/FTPType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/myfiles/ftp/FTPType;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/app/myfiles/ftp/FTPType;->$VALUES:[Lcom/sec/android/app/myfiles/ftp/FTPType;

    invoke-virtual {v0}, [Lcom/sec/android/app/myfiles/ftp/FTPType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/myfiles/ftp/FTPType;

    return-object v0
.end method
