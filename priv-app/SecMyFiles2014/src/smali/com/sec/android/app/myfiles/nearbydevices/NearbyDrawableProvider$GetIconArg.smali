.class Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;
.super Ljava/lang/Object;
.source "NearbyDrawableProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GetIconArg"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mResult:Landroid/graphics/drawable/Drawable;

.field private final mUpdatable:Lcom/sec/android/app/myfiles/nearbydevices/DrawableUpdatable;

.field private final mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;Lcom/sec/android/app/myfiles/nearbydevices/DrawableUpdatable;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "updatable"    # Lcom/sec/android/app/myfiles/nearbydevices/DrawableUpdatable;

    .prologue
    .line 256
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 257
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 258
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 260
    :cond_1
    iput-object p1, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;->mContext:Landroid/content/Context;

    .line 261
    iput-object p2, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;->mUri:Landroid/net/Uri;

    .line 262
    iput-object p3, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;->mUpdatable:Lcom/sec/android/app/myfiles/nearbydevices/DrawableUpdatable;

    .line 263
    return-void
.end method


# virtual methods
.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getDrawableUpdatable()Lcom/sec/android/app/myfiles/nearbydevices/DrawableUpdatable;
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;->mUpdatable:Lcom/sec/android/app/myfiles/nearbydevices/DrawableUpdatable;

    return-object v0
.end method

.method public getResult()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;->mResult:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method public setResult(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "result"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 282
    iput-object p1, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider$GetIconArg;->mResult:Landroid/graphics/drawable/Drawable;

    .line 283
    return-void
.end method
