.class public Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;
.super Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
.source "NearByDevicesBrowserNavigation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$NearyByDevicesListFields;
    }
.end annotation


# static fields
.field private static final ASYNC_OPEN_FRAGMENT_TAG:Ljava/lang/String; = "OpenOperation"

.field private static final MODULE:Ljava/lang/String; = "NearByDevicesBrowserNavigation"


# instance fields
.field protected final DEFAULT_BROWSE_MAX_SIZE:I

.field protected final DEFAULT_BROWSE_SIZE:I

.field protected final VISIBLE_ITEMS_OFFSET:I

.field protected mAsyncUi:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

.field protected mCurrItemsIdProp:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/samsung/android/allshare/Item;",
            ">;"
        }
    .end annotation
.end field

.field protected mCurrentItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/allshare/Item;",
            ">;"
        }
    .end annotation
.end field

.field protected mCurrentPathItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/allshare/Item;",
            ">;"
        }
    .end annotation
.end field

.field private mFromLeftPanel:Z

.field protected mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

.field protected mProvider:Lcom/samsung/android/allshare/media/Provider;

.field protected final mProviderResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderBrowseResponseListener;

.field protected mRequests:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/samsung/android/allshare/Item;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field protected mRootItem:Lcom/samsung/android/allshare/Item;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 86
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;-><init>()V

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentItems:Ljava/util/List;

    .line 63
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrItemsIdProp:Ljava/util/Map;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentPathItems:Ljava/util/List;

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mAsyncUi:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    .line 67
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mRequests:Ljava/util/Map;

    .line 71
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->DEFAULT_BROWSE_SIZE:I

    .line 72
    const/16 v0, 0xc8

    iput v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->DEFAULT_BROWSE_MAX_SIZE:I

    .line 73
    iput v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->VISIBLE_ITEMS_OFFSET:I

    .line 75
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mFromLeftPanel:Z

    .line 518
    new-instance v0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$3;-><init>(Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mProviderResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderBrowseResponseListener;

    .line 87
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I

    .prologue
    const/4 v1, 0x0

    .line 90
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;-><init>(Landroid/content/Context;I)V

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentItems:Ljava/util/List;

    .line 63
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrItemsIdProp:Ljava/util/Map;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentPathItems:Ljava/util/List;

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mAsyncUi:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    .line 67
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mRequests:Ljava/util/Map;

    .line 71
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->DEFAULT_BROWSE_SIZE:I

    .line 72
    const/16 v0, 0xc8

    iput v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->DEFAULT_BROWSE_MAX_SIZE:I

    .line 73
    iput v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->VISIBLE_ITEMS_OFFSET:I

    .line 75
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mFromLeftPanel:Z

    .line 518
    new-instance v0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$3;-><init>(Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mProviderResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderBrowseResponseListener;

    .line 92
    new-instance v0, Lcom/sec/android/app/myfiles/db/CacheDB;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->clearNearByData()J

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->setOnScrollListener()V

    .line 98
    return-void
.end method

.method private getDevicesCollection()Landroid/database/Cursor;
    .locals 14

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 159
    invoke-static {}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getCategoryAdapterDevicesCollection()Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    move-result-object v2

    .line 160
    .local v2, "devices":Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;
    if-eqz v2, :cond_2

    .line 161
    new-instance v0, Landroid/database/MatrixCursor;

    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/String;

    const-string v7, "_id"

    aput-object v7, v6, v9

    const-string v7, "deviceid"

    aput-object v7, v6, v10

    const-string v7, "name"

    aput-object v7, v6, v11

    const-string v7, "model"

    aput-object v7, v6, v12

    const-string v7, "thumbnail"

    aput-object v7, v6, v13

    invoke-direct {v0, v6}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 163
    .local v0, "cursor":Landroid/database/Cursor;
    const/4 v3, 0x0

    .line 164
    .local v3, "i":I
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/Device;

    .line 165
    .local v1, "dev":Lcom/samsung/android/allshare/Device;
    if-eqz v2, :cond_0

    .line 166
    invoke-static {}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->getInstance()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;

    move-result-object v6

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->registerDevice(Ljava/lang/String;Lcom/samsung/android/allshare/Device;)V

    :cond_0
    move-object v6, v0

    .line 168
    check-cast v6, Landroid/database/MatrixCursor;

    const/4 v7, 0x5

    new-array v8, v7, [Ljava/lang/String;

    add-int/lit8 v4, v3, 0x1

    .end local v3    # "i":I
    .local v4, "i":I
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v8, v9

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v8, v10

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v8, v11

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Device;->getModelName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v8, v12

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Device;->getIcon()Landroid/net/Uri;

    move-result-object v7

    if-eqz v7, :cond_1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Device;->getIcon()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    :goto_1
    aput-object v7, v8, v13

    invoke-virtual {v6, v8}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    move v3, v4

    .line 170
    .end local v4    # "i":I
    .restart local v3    # "i":I
    goto :goto_0

    .line 168
    .end local v3    # "i":I
    .restart local v4    # "i":I
    :cond_1
    const/4 v7, 0x0

    goto :goto_1

    .line 173
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v1    # "dev":Lcom/samsung/android/allshare/Device;
    .end local v4    # "i":I
    .end local v5    # "i$":Ljava/util/Iterator;
    :cond_2
    const/4 v0, 0x0

    :cond_3
    return-object v0
.end method


# virtual methods
.method public AsyncOpen()V
    .locals 3

    .prologue
    .line 275
    new-instance v1, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$1;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$1;-><init>(Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;Lcom/sec/android/app/myfiles/fragment/AbsFragment;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mAsyncUi:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    .line 304
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 306
    .local v0, "ft":Landroid/app/FragmentTransaction;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mAsyncUi:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    const-string v2, "OpenOperation"

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 308
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 310
    return-void
.end method

.method public clean()V
    .locals 1

    .prologue
    .line 451
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    if-eqz v0, :cond_0

    .line 452
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 453
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->clearNearByData()J

    .line 454
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 456
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 457
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 458
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrItemsIdProp:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 459
    return-void
.end method

.method public getCurFolder()Lcom/samsung/android/allshare/Item;
    .locals 1

    .prologue
    .line 399
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->getCurrentItem()Lcom/samsung/android/allshare/Item;

    move-result-object v0

    return-object v0
.end method

.method public getCurFolderItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/allshare/Item;",
            ">;"
        }
    .end annotation

    .prologue
    .line 395
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentItems:Ljava/util/List;

    return-object v0
.end method

.method public getCurItem(I)Lcom/samsung/android/allshare/Item;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 391
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/Item;

    return-object v0
.end method

.method protected getCurrentItem()Lcom/samsung/android/allshare/Item;
    .locals 3

    .prologue
    .line 478
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 479
    .local v0, "size":I
    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentPathItems:Ljava/util/List;

    add-int/lit8 v2, v0, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/Item;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected getCurrentPathString()[Ljava/lang/String;
    .locals 6

    .prologue
    .line 483
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 484
    .local v2, "size":I
    new-array v1, v2, [Ljava/lang/String;

    .line 485
    .local v1, "names":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 486
    if-nez v0, :cond_0

    .line 487
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    if-eqz v3, :cond_0

    .line 488
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/media/Provider;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    .line 485
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 492
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/Item;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x2f

    const/16 v5, 0x5c

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    goto :goto_1

    .line 495
    :cond_1
    return-object v1
.end method

.method public getFileCount()I
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getFilesInCurrentFolder()Landroid/database/Cursor;
    .locals 5

    .prologue
    .line 224
    const/4 v0, 0x0

    .line 226
    .local v0, "cursor":Landroid/database/Cursor;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    if-eqz v1, :cond_0

    .line 227
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 228
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/media/Provider;->getID()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentSortBy:I

    iget v4, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentInOrder:I

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/myfiles/db/CacheDB;->getNearByList(Ljava/lang/String;II)Landroid/database/Cursor;

    move-result-object v0

    .line 229
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 234
    :goto_0
    return-object v0

    .line 231
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->getDevicesCollection()Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method

.method public getFilesInFolder(Ljava/lang/Object;)Landroid/database/Cursor;
    .locals 1
    .param p1, "folderPath"    # Ljava/lang/Object;

    .prologue
    .line 151
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 152
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v0

    .line 155
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFilesInFolder(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 247
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getFilesInFolder(Ljava/lang/String;I)Landroid/database/Cursor;
    .locals 2
    .param p1, "folderPath"    # Ljava/lang/String;
    .param p2, "showFolderFileType"    # I

    .prologue
    .line 260
    iget v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mShowFolderFileType:I

    .line 262
    .local v1, "prevShowFolderFileType":I
    iput p2, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mShowFolderFileType:I

    .line 264
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->getFilesInFolder(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 266
    .local v0, "c":Landroid/database/Cursor;
    iput v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mShowFolderFileType:I

    .line 268
    return-object v0
.end method

.method public getItemOfId(J)Lcom/samsung/android/allshare/Item;
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 386
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrItemsIdProp:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrItemsIdProp:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/Item;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemsOfId(Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/allshare/Item;",
            ">;"
        }
    .end annotation

    .prologue
    .line 377
    .local p1, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 378
    .local v2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 379
    .local v1, "id":Ljava/lang/Long;
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->getItemOfId(J)Lcom/samsung/android/allshare/Item;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 382
    .end local v1    # "id":Ljava/lang/Long;
    :cond_0
    return-object v2
.end method

.method public getProvider()Lcom/samsung/android/allshare/media/Provider;
    .locals 1

    .prologue
    .line 407
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    return-object v0
.end method

.method protected getRoot()Lcom/samsung/android/allshare/Item;
    .locals 1

    .prologue
    .line 468
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mRootItem:Lcom/samsung/android/allshare/Item;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    if-eqz v0, :cond_0

    .line 469
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/media/Provider;->getRootFolder()Lcom/samsung/android/allshare/Item;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mRootItem:Lcom/samsung/android/allshare/Item;

    .line 474
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mRootItem:Lcom/samsung/android/allshare/Item;

    return-object v0
.end method

.method public goBack()Z
    .locals 1

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->goUp()Z

    move-result v0

    return v0
.end method

.method public goTo(J)Z
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 114
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrItemsIdProp:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 115
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrItemsIdProp:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/Item;

    .line 116
    .local v0, "item":Lcom/samsung/android/allshare/Item;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->getCurrentItem()Lcom/samsung/android/allshare/Item;

    move-result-object v1

    if-eq v1, v0, :cond_0

    .line 117
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->AsyncOpen()V

    .line 121
    const/4 v1, 0x1

    .line 124
    .end local v0    # "item":Lcom/samsung/android/allshare/Item;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z
    .locals 5
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "isBack"    # Ljava/lang/Boolean;

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 128
    if-nez p1, :cond_0

    .line 147
    :goto_0
    return v2

    .line 132
    :cond_0
    const-string v4, "/"

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 133
    .local v1, "titles":[Ljava/lang/String;
    array-length v4, v1

    add-int/lit8 v0, v4, -0x1

    .line 134
    .local v0, "len":I
    if-ne v0, v3, :cond_2

    .line 135
    iget-boolean v4, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mFromLeftPanel:Z

    if-eqz v4, :cond_1

    aget-object v2, v1, v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    aget-object v2, v1, v3

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 136
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    .line 138
    :cond_1
    invoke-virtual {p0, p2}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->goToRoot(Ljava/lang/Boolean;)Z

    move-result v2

    goto :goto_0

    .line 139
    :cond_2
    if-le v0, v3, :cond_3

    .line 140
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 141
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->popItem()V

    goto :goto_1

    .line 145
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->AsyncOpen()V

    move v2, v3

    .line 147
    goto :goto_0
.end method

.method public goToRoot(Ljava/lang/Boolean;)Z
    .locals 3
    .param p1, "isBack"    # Ljava/lang/Boolean;

    .prologue
    const/4 v2, 0x0

    .line 178
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    if-eqz v1, :cond_1

    .line 179
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 180
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mFromLeftPanel:Z

    if-eqz v1, :cond_0

    .line 181
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 183
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 184
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrItemsIdProp:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 185
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->getRoot()Lcom/samsung/android/allshare/Item;

    move-result-object v0

    .line 186
    .local v0, "root":Lcom/samsung/android/allshare/Item;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->AsyncOpen()V

    .line 195
    .end local v0    # "root":Lcom/samsung/android/allshare/Item;
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 190
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 191
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 192
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->AsyncOpenCompleted()V

    goto :goto_0
.end method

.method public goUp()Z
    .locals 1

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->isCurrentFolderRoot()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->clearNearByData()J

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 204
    const/4 v0, 0x0

    .line 214
    :goto_0
    return v0

    .line 207
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->popItem()V

    .line 208
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->getCurrentItem()Lcom/samsung/android/allshare/Item;

    move-result-object v0

    if-nez v0, :cond_1

    .line 209
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    .line 210
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->AsyncOpenCompleted()V

    .line 214
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 212
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->AsyncOpen()V

    goto :goto_1
.end method

.method public isCurrentFolderRoot()Z
    .locals 2

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->getCurrentItem()Lcom/samsung/android/allshare/Item;

    move-result-object v0

    .line 107
    .local v0, "currentItem":Lcom/samsung/android/allshare/Item;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    if-nez v0, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mFromLeftPanel:Z

    if-eqz v1, :cond_0

    .line 108
    const/4 v1, 0x1

    .line 110
    :goto_0
    return v1

    :cond_0
    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mFromLeftPanel:Z

    if-eqz v1, :cond_2

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item;->isRootFolder()Z

    move-result v1

    goto :goto_0
.end method

.method protected isFolder(Lcom/samsung/android/allshare/Item;)Z
    .locals 3
    .param p1, "item"    # Lcom/samsung/android/allshare/Item;

    .prologue
    const/4 v0, 0x0

    .line 499
    if-eqz p1, :cond_0

    .line 500
    invoke-virtual {p1}, Lcom/samsung/android/allshare/Item;->getType()Lcom/samsung/android/allshare/Item$MediaType;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_FOLDER:Lcom/samsung/android/allshare/Item$MediaType;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 501
    :cond_0
    return v0
.end method

.method public loadMoreItems(I)V
    .locals 6
    .param p1, "lastPosition"    # I

    .prologue
    const/16 v5, 0x32

    .line 313
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowHiddenFileStatus()Z

    move-result v3

    if-nez v3, :cond_0

    .line 314
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->getFileCount()I

    move-result v3

    if-ge p1, v3, :cond_0

    .line 315
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->getFileCount()I

    move-result p1

    .line 318
    :cond_0
    add-int/lit8 v3, p1, 0x1

    div-int/lit8 v3, v3, 0x32

    mul-int/lit8 v3, v3, 0x32

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 319
    .local v2, "startIndex":Ljava/lang/Integer;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->getCurrentItem()Lcom/samsung/android/allshare/Item;

    move-result-object v0

    .line 320
    .local v0, "current":Lcom/samsung/android/allshare/Item;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-lt v3, v5, :cond_2

    .line 321
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mRequests:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 322
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mRequests:Ljava/util/Map;

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mRequests:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    .line 326
    .local v1, "requests":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 327
    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 328
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    if-eqz v3, :cond_2

    .line 329
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v0, v4, v5}, Lcom/samsung/android/allshare/media/Provider;->browse(Lcom/samsung/android/allshare/Item;II)V

    .line 330
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-lez v3, :cond_2

    .line 331
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0089

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 336
    .end local v1    # "requests":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :cond_2
    return-void
.end method

.method protected openItem(Lcom/samsung/android/allshare/Item;)V
    .locals 5
    .param p1, "item"    # Lcom/samsung/android/allshare/Item;

    .prologue
    .line 505
    if-nez p1, :cond_0

    .line 515
    :goto_0
    return-void

    .line 508
    :cond_0
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Item;->getMimetype()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 511
    .local v1, "intent":Landroid/content/Intent;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 512
    :catch_0
    move-exception v0

    .line 513
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "NearByDevicesBrowserNavigation"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cannot find activity for the mime type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Item;->getMimetype()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected popItem()V
    .locals 2

    .prologue
    .line 462
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 463
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentPathItems:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 465
    :cond_0
    return-void
.end method

.method public refreshNavigation()V
    .locals 1

    .prologue
    .line 242
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mRootItem:Lcom/samsung/android/allshare/Item;

    .line 243
    return-void
.end method

.method public removeProvider()V
    .locals 1

    .prologue
    .line 411
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    .line 412
    return-void
.end method

.method public setCurrentPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 253
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 255
    return-void
.end method

.method public setFromLeftPanel(Z)V
    .locals 0
    .param p1, "fromLeftPanel"    # Z

    .prologue
    .line 101
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mFromLeftPanel:Z

    .line 102
    return-void
.end method

.method protected setOnScrollListener()V
    .locals 1

    .prologue
    .line 339
    new-instance v0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$2;-><init>(Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    .line 374
    return-void
.end method

.method public setProvider(Lcom/samsung/android/allshare/media/Provider;)V
    .locals 2
    .param p1, "provider"    # Lcom/samsung/android/allshare/media/Provider;

    .prologue
    .line 432
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    .line 434
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    if-eqz v0, :cond_1

    .line 435
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 436
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/media/Provider;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->insertNearByDeviceID(Ljava/lang/String;)J

    .line 437
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 439
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 440
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 441
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mFromLeftPanel:Z

    if-eqz v0, :cond_0

    .line 442
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentPathItems:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 444
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrItemsIdProp:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 447
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mProviderResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderBrowseResponseListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/media/Provider;->setBrowseItemsResponseListener(Lcom/samsung/android/allshare/media/Provider$IProviderBrowseResponseListener;)V

    .line 448
    return-void
.end method

.method public setProvider(Ljava/lang/String;)V
    .locals 2
    .param p1, "providerId"    # Ljava/lang/String;

    .prologue
    .line 416
    invoke-static {}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->getInstance()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->resolveProvider(Ljava/lang/String;)Lcom/samsung/android/allshare/media/Provider;

    move-result-object v0

    .line 419
    .local v0, "device":Lcom/samsung/android/allshare/Device;
    if-eqz v0, :cond_1

    .line 420
    instance-of v1, v0, Lcom/samsung/android/allshare/media/Provider;

    if-eqz v1, :cond_0

    .line 421
    check-cast v0, Lcom/samsung/android/allshare/media/Provider;

    .end local v0    # "device":Lcom/samsung/android/allshare/Device;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->setProvider(Lcom/samsung/android/allshare/media/Provider;)V

    .line 424
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    if-eqz v1, :cond_1

    .line 425
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/media/Provider;->getRootFolder()Lcom/samsung/android/allshare/Item;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mRootItem:Lcom/samsung/android/allshare/Item;

    .line 428
    :cond_1
    return-void
.end method
