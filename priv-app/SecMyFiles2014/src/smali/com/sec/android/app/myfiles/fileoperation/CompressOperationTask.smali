.class public Lcom/sec/android/app/myfiles/fileoperation/CompressOperationTask;
.super Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;
.source "CompressOperationTask.java"


# instance fields
.field private mCompressManager:Lcom/sec/android/app/myfiles/compression/CompressManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILandroid/view/View;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "operation"    # I
    .param p3, "contentType"    # I
    .param p4, "progressView"    # Landroid/view/View;

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;-><init>(Landroid/content/Context;IILandroid/view/View;)V

    .line 36
    new-instance v0, Lcom/sec/android/app/myfiles/compression/CompressManager;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/compression/CompressManager;-><init>(Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/CompressOperationTask;->mCompressManager:Lcom/sec/android/app/myfiles/compression/CompressManager;

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/CompressOperationTask;->mCompressManager:Lcom/sec/android/app/myfiles/compression/CompressManager;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/CompressOperationTask;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/compression/CompressManager;->setProgressBar(Landroid/widget/ProgressBar;)V

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/CompressOperationTask;->mCompressManager:Lcom/sec/android/app/myfiles/compression/CompressManager;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/CompressOperationTask;->mPercentText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/compression/CompressManager;->setProgressBarPercent(Landroid/widget/TextView;)V

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/CompressOperationTask;->mCompressManager:Lcom/sec/android/app/myfiles/compression/CompressManager;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/CompressOperationTask;->mCountText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/compression/CompressManager;->setProgressBarCounter(Landroid/widget/TextView;)V

    .line 41
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;
    .locals 8
    .param p1, "params"    # [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .prologue
    const/4 v7, 0x0

    .line 53
    const/4 v3, 0x0

    aget-object v2, p1, v3

    .line 55
    .local v2, "param":Lcom/sec/android/app/myfiles/element/FileOperationParam;
    iget-object v3, v2, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mSrcFolder:Ljava/lang/String;

    iput-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CompressOperationTask;->mSrcFolder:Ljava/lang/String;

    .line 57
    iget-object v3, v2, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    iput-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CompressOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    .line 59
    iget-object v3, v2, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    iput-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CompressOperationTask;->mTargetFolder:Ljava/lang/String;

    .line 61
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CompressOperationTask;->mTargetFolder:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/myfiles/fileoperation/CompressOperationTask;->checkDirOrFileName(Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v1

    .line 63
    .local v1, "file":Ljava/io/File;
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CompressOperationTask;->mRenameCancelled:Z

    if-nez v3, :cond_0

    if-nez v1, :cond_1

    .line 85
    :cond_0
    :goto_0
    return-object v7

    .line 69
    :cond_1
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CompressOperationTask;->mOverwrite:Z

    if-eqz v3, :cond_2

    .line 73
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fileoperation/CompressOperationTask;->deleteInFileSystem(Ljava/io/File;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    :cond_2
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/CompressOperationTask;->mCompressManager:Lcom/sec/android/app/myfiles/compression/CompressManager;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/CompressOperationTask;->mSrcFolder:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/CompressOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/android/app/myfiles/compression/CompressManager;->doCompress(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)V

    goto :goto_0

    .line 75
    :catch_0
    move-exception v0

    .line 77
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 28
    check-cast p1, [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/CompressOperationTask;->doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 0

    .prologue
    .line 92
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->onCancelled()V

    .line 93
    return-void
.end method

.method protected setupProgressView()V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/CompressOperationTask;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 48
    return-void
.end method
