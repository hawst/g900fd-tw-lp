.class Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$10;
.super Landroid/content/BroadcastReceiver;
.source "FTPBrowserFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)V
    .locals 0

    .prologue
    .line 2093
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v10, 0x1

    .line 2098
    # getter for: Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->MODULE:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->access$400()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onReceive, intent: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v10, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    .line 2110
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->isSelectMode()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2111
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getSelectionType()I

    move-result v6

    .line 2112
    .local v6, "selectionType":I
    if-eqz v6, :cond_0

    if-ne v6, v10, :cond_1

    .line 2162
    .end local v6    # "selectionType":I
    :cond_0
    :goto_0
    return-void

    .line 2119
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 2121
    .local v3, "extra":Landroid/os/Bundle;
    if-eqz v3, :cond_0

    .line 2123
    sget-object v7, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;->FTP_PARAMS:Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2125
    .local v4, "ftpParamStr":Ljava/lang/String;
    sget-object v7, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;->BROWSE_PATH:Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2127
    .local v0, "browsePath":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v2

    .line 2129
    .local v2, "currentPath":Ljava/lang/String;
    if-eqz v4, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mBrowseHandler:Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;
    invoke-static {v7}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->access$900(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;
    invoke-static {v8}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->access$800(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)Lcom/sec/android/app/myfiles/ftp/FTPParams;

    move-result-object v8

    invoke-virtual {v7, v8, v0}, Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;->isBrowsePending(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2132
    new-instance v5, Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-direct {v5, v4}, Lcom/sec/android/app/myfiles/ftp/FTPParams;-><init>(Ljava/lang/String;)V

    .line 2134
    .local v5, "ftpParams":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;
    invoke-static {v7}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->access$800(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)Lcom/sec/android/app/myfiles/ftp/FTPParams;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->equals(Lcom/sec/android/app/myfiles/ftp/FTPParams;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2136
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v1

    .line 2138
    .local v1, "c":Landroid/database/Cursor;
    if-eqz v1, :cond_2

    .line 2140
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    invoke-virtual {v7, v1}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 2142
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->updatePathIndicator()V

    .line 2144
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/AbsMainActivity;->invalidateOptionsMenu()V

    .line 2158
    .end local v1    # "c":Landroid/database/Cursor;
    .end local v5    # "ftpParams":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mBrowseHandler:Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;
    invoke-static {v7}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->access$900(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;
    invoke-static {v8}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->access$800(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)Lcom/sec/android/app/myfiles/ftp/FTPParams;

    move-result-object v8

    invoke-virtual {v7, v8, v0}, Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;->popBrowseRequest(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;)V

    goto :goto_0
.end method
