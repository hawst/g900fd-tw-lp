.class Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$6;
.super Ljava/lang/Object;
.source "UserBrowserFragment.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;)V
    .locals 0

    .prologue
    .line 1182
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPathSelected(ILjava/lang/String;)V
    .locals 5
    .param p1, "targetFolderID"    # I
    .param p2, "targetFolderPath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1187
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    # invokes: Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->moveToFolder(Ljava/lang/String;Ljava/lang/Boolean;)V
    invoke-static {v3, p2, v4}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1188
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getSelectionType()I

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getSelectionType()I

    move-result v3

    if-ne v3, v1, :cond_1

    .line 1189
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->unselectAllItem()V

    .line 1191
    :cond_1
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isCheckMountedStorage()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1193
    const/4 v0, 0x0

    .line 1194
    .local v0, "menuItem":Landroid/view/MenuItem;
    sget-object v3, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mOptionsMenu:Landroid/view/Menu;

    const v4, 0x7f0f0148

    invoke-interface {v3, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1196
    invoke-static {p2}, Lcom/sec/android/app/myfiles/utils/Utils;->isRootFolder(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    :goto_0
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1199
    .end local v0    # "menuItem":Landroid/view/MenuItem;
    :cond_2
    return-void

    .restart local v0    # "menuItem":Landroid/view/MenuItem;
    :cond_3
    move v1, v2

    .line 1196
    goto :goto_0
.end method
