.class public abstract Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;
.super Landroid/os/AsyncTask;
.source "AbsFileOperationTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$MediaBulkDeleter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/sec/android/app/myfiles/element/FileOperationParam;",
        "Lcom/sec/android/app/myfiles/element/FileOperationProgress;",
        "Lcom/sec/android/app/myfiles/element/FileOperationResult;",
        ">;"
    }
.end annotation


# static fields
.field protected static final ERR_CODE_DESTINATION_IS_SUBFOLDER:I = 0x5

.field protected static final ERR_CODE_DROPBOX_ERROR:I = 0x2

.field protected static final ERR_CODE_FAILED_TO_COPY:I = 0x1

.field protected static final ERR_CODE_FAILED_TO_DELETE_FILE:I = 0x7

.field protected static final ERR_CODE_FAIL_TO_CREATE_FOLDER:I = 0x3

.field protected static final ERR_CODE_FILE_PATH_IS_TOO_LONG:I = 0x8

.field protected static final ERR_CODE_NOT_ENOUGH_SPACE:I = 0x6

.field protected static final ERR_CODE_SAME_SOURCE_AD_DESTINATION:I = 0x4

.field protected static final ERR_CODE_WRITE_DENIED:I = 0x9

.field protected static final MESSAGE_RENAME_CANCEL:I = 0x1

.field protected static final MESSAGE_RENAME_SUCCESS:I = 0x0

.field private static final MODULE:Ljava/lang/String; = "FileOperationTask"


# instance fields
.field protected isAllOverwriteCheck:Z

.field protected volatile isCancelled:Z

.field protected mContainerId:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mContentResolver:Landroid/content/ContentResolver;

.field protected mContentType:I

.field protected mContext:Landroid/content/Context;

.field protected mCountText:Landroid/widget/TextView;

.field protected mDoneFileList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mDoneFolderList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mFileName:Landroid/widget/TextView;

.field protected mFileNameHandler:Landroid/os/Handler;

.field protected mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

.field protected mFormat:I

.field protected mFragment:Landroid/app/Fragment;

.field protected mHandler:Landroid/os/Handler;

.field protected mOperationType:I

.field protected mOverwrite:Z

.field protected mOverwriteFileCount:I

.field protected mPath:Ljava/lang/String;

.field protected mPercentText:Landroid/widget/TextView;

.field protected mPrefixFile:Landroid/widget/TextView;

.field protected mProcessing:Landroid/widget/TextView;

.field protected mProgressBar:Landroid/widget/ProgressBar;

.field protected mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

.field private mProgressView:Landroid/view/View;

.field protected mRemainOverwriteFileCount:I

.field protected mRename:Ljava/lang/String;

.field protected mRenameCancelled:Z

.field protected mRenameOverwriteDialog:Landroid/app/AlertDialog;

.field protected mRunRename:Z

.field protected mRunStop:Z

.field protected mRunnable:Ljava/lang/Runnable;

.field protected mSameFolder:Z

.field protected mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

.field protected mSourceFragmentId:I

.field protected mSrcFolder:Ljava/lang/String;

.field protected mStopDialog:Landroid/app/AlertDialog;

.field protected mTargetDatas:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mTargetFolder:Ljava/lang/String;

.field protected mTargetFragmentId:I

.field protected mTargetName:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mTempOverwriteFileCount:I

.field protected mToast:Landroid/widget/Toast;

.field private mWaitDialog:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;IILandroid/view/View;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "operation"    # I
    .param p3, "contentType"    # I
    .param p4, "progressView"    # Landroid/view/View;

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 173
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 123
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mContentResolver:Landroid/content/ContentResolver;

    .line 127
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mFragment:Landroid/app/Fragment;

    .line 129
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mSameFolder:Z

    .line 131
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRunRename:Z

    .line 133
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mOverwrite:Z

    .line 135
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRenameCancelled:Z

    .line 137
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRename:Ljava/lang/String;

    .line 139
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mPath:Ljava/lang/String;

    .line 141
    const/16 v0, 0x3001

    iput v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mFormat:I

    .line 143
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRenameOverwriteDialog:Landroid/app/AlertDialog;

    .line 145
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mStopDialog:Landroid/app/AlertDialog;

    .line 147
    iput v3, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mSourceFragmentId:I

    .line 149
    iput v3, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mTargetFragmentId:I

    .line 151
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mToast:Landroid/widget/Toast;

    .line 153
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->isCancelled:Z

    .line 155
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRunnable:Ljava/lang/Runnable;

    .line 157
    iput v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mOverwriteFileCount:I

    .line 159
    iput v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mTempOverwriteFileCount:I

    .line 161
    iput v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRemainOverwriteFileCount:I

    .line 163
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->isAllOverwriteCheck:Z

    .line 165
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRunStop:Z

    .line 169
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mWaitDialog:Z

    .line 218
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$1;-><init>(Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mFileNameHandler:Landroid/os/Handler;

    .line 233
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$2;-><init>(Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mHandler:Landroid/os/Handler;

    .line 175
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mContext:Landroid/content/Context;

    .line 177
    iput p2, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mOperationType:I

    .line 179
    iput-object p4, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mProgressView:Landroid/view/View;

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mProgressView:Landroid/view/View;

    const v1, 0x7f0f00d0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mProcessing:Landroid/widget/TextView;

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mProgressView:Landroid/view/View;

    const v1, 0x7f0f00d2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mPrefixFile:Landroid/widget/TextView;

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mProgressView:Landroid/view/View;

    const v1, 0x7f0f00d3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mFileName:Landroid/widget/TextView;

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mProgressView:Landroid/view/View;

    const v1, 0x7f0f00d4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mProgressBar:Landroid/widget/ProgressBar;

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mProgressView:Landroid/view/View;

    const v1, 0x7f0f00d5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mCountText:Landroid/widget/TextView;

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mProgressView:Landroid/view/View;

    const v1, 0x7f0f00d6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mPercentText:Landroid/widget/TextView;

    .line 193
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->setupProgressView()V

    .line 195
    new-instance v0, Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/element/FileOperationProgress;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    .line 197
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mDoneFileList:Ljava/util/ArrayList;

    .line 199
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mDoneFolderList:Ljava/util/ArrayList;

    .line 201
    iput p3, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mContentType:I

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mContentResolver:Landroid/content/ContentResolver;

    .line 205
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mTargetName:Ljava/util/ArrayList;

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    .line 208
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mWaitDialog:Z

    return p1
.end method


# virtual methods
.method protected addPostfix(Ljava/lang/String;IZ)Ljava/lang/String;
    .locals 12
    .param p1, "fullName"    # Ljava/lang/String;
    .param p2, "postfixNum"    # I
    .param p3, "isFile"    # Z

    .prologue
    const/16 v11, 0x2e

    const/4 v10, 0x0

    const/4 v9, -0x1

    const/16 v8, 0x29

    .line 556
    const/4 v5, 0x0

    .line 558
    .local v5, "resultStr":Ljava/lang/String;
    if-eqz p3, :cond_2

    .line 560
    const-string v0, ""

    .line 561
    .local v0, "encExt":Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->isEncryptionFile(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 562
    invoke-virtual {p1, v11}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    .line 563
    .local v2, "index":I
    if-eq v2, v9, :cond_0

    .line 564
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {p1, v2, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 565
    invoke-virtual {p1, v10, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 569
    .end local v2    # "index":I
    :cond_0
    invoke-virtual {p1, v11}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    .line 571
    .local v3, "lastDot":I
    if-eq v3, v9, :cond_1

    .line 573
    invoke-virtual {p1, v10, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 575
    .local v4, "name":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {p1, v3, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 577
    .local v1, "ext":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 579
    .local v6, "sb":Ljava/lang/StringBuffer;
    const-string v7, " ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 581
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    .line 601
    .end local v0    # "encExt":Ljava/lang/String;
    .end local v1    # "ext":Ljava/lang/String;
    .end local v3    # "lastDot":I
    .end local v4    # "name":Ljava/lang/String;
    :goto_0
    return-object v5

    .line 585
    .end local v6    # "sb":Ljava/lang/StringBuffer;
    .restart local v0    # "encExt":Ljava/lang/String;
    .restart local v3    # "lastDot":I
    :cond_1
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6, p1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 587
    .restart local v6    # "sb":Ljava/lang/StringBuffer;
    const-string v7, " ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 589
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 594
    .end local v0    # "encExt":Ljava/lang/String;
    .end local v3    # "lastDot":I
    .end local v6    # "sb":Ljava/lang/StringBuffer;
    :cond_2
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6, p1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 596
    .restart local v6    # "sb":Ljava/lang/StringBuffer;
    const-string v7, " ("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 598
    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method protected cancelOperation()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 213
    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->isCancelled:Z

    .line 215
    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->cancel(Z)Z

    .line 216
    return-void
.end method

.method public checkDirOrFileName(Ljava/io/File;Z)Ljava/io/File;
    .locals 7
    .param p1, "dstFile"    # Ljava/io/File;
    .param p2, "isFile"    # Z

    .prologue
    const/16 v4, 0x3001

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 616
    if-nez p1, :cond_0

    move-object v3, v5

    .line 707
    :goto_0
    return-object v3

    .line 621
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-nez v6, :cond_2

    :cond_1
    move-object v3, v5

    .line 623
    goto :goto_0

    .line 626
    :cond_2
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 628
    .local v1, "dstPath":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-eq v6, v3, :cond_3

    instance-of v6, p0, Lcom/sec/android/app/myfiles/fileoperation/CompressOperationTask;

    if-eqz v6, :cond_9

    :cond_3
    iget-boolean v6, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mSameFolder:Z

    if-nez v6, :cond_9

    .line 630
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 632
    if-eqz p2, :cond_5

    :goto_1
    iput v3, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mFormat:I

    .line 634
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mPath:Ljava/lang/String;

    .line 636
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->showRenameDialog()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 638
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mPath:Ljava/lang/String;

    invoke-virtual {p0, v3, p2}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->startRename(Ljava/lang/String;Z)V

    .line 645
    :cond_4
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRunRename:Z

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRename:Ljava/lang/String;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRename:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_7

    .line 647
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRename:Ljava/lang/String;

    invoke-direct {v0, v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .end local p1    # "dstFile":Ljava/io/File;
    .local v0, "dstFile":Ljava/io/File;
    move-object p1, v0

    .end local v0    # "dstFile":Ljava/io/File;
    .restart local p1    # "dstFile":Ljava/io/File;
    :goto_2
    move-object v3, p1

    .line 707
    goto :goto_0

    :cond_5
    move v3, v4

    .line 632
    goto :goto_1

    :cond_6
    move-object v3, p1

    .line 642
    goto :goto_0

    .line 649
    :cond_7
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mOverwrite:Z

    if-eqz v3, :cond_8

    .line 651
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->removeThumbnailCache(Ljava/lang/String;)V

    move-object v3, p1

    .line 653
    goto :goto_0

    :cond_8
    move-object v3, v5

    .line 657
    goto :goto_0

    .line 662
    :cond_9
    iget-boolean v6, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mSameFolder:Z

    if-eqz v6, :cond_b

    .line 664
    const/4 v2, 0x0

    .line 666
    .local v2, "i":I
    :goto_3
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 668
    new-instance p1, Ljava/io/File;

    .end local p1    # "dstFile":Ljava/io/File;
    invoke-virtual {p0, v1, v2, p2}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->addPostfix(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 670
    .restart local p1    # "dstFile":Ljava/io/File;
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_a
    move-object v3, p1

    .line 673
    goto/16 :goto_0

    .line 676
    .end local v2    # "i":I
    :cond_b
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_e

    .line 678
    if-eqz p2, :cond_d

    :goto_4
    iput v3, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mFormat:I

    .line 680
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mPath:Ljava/lang/String;

    .line 682
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->showRenameDialog()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 684
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mPath:Ljava/lang/String;

    invoke-virtual {p0, v3, p2}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->startRename(Ljava/lang/String;Z)V

    .line 691
    :cond_c
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRunRename:Z

    if-eqz v3, :cond_f

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRename:Ljava/lang/String;

    if-eqz v3, :cond_f

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRename:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_f

    .line 693
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRename:Ljava/lang/String;

    invoke-direct {v0, v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .end local p1    # "dstFile":Ljava/io/File;
    .restart local v0    # "dstFile":Ljava/io/File;
    move-object p1, v0

    .end local v0    # "dstFile":Ljava/io/File;
    .restart local p1    # "dstFile":Ljava/io/File;
    goto :goto_2

    :cond_d
    move v3, v4

    .line 678
    goto :goto_4

    :cond_e
    move-object v3, p1

    .line 688
    goto/16 :goto_0

    .line 695
    :cond_f
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mOverwrite:Z

    if-eqz v3, :cond_10

    .line 697
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->removeThumbnailCache(Ljava/lang/String;)V

    move-object v3, p1

    .line 699
    goto/16 :goto_0

    :cond_10
    move-object v3, v5

    .line 703
    goto/16 :goto_0
.end method

.method public checkDirOrFileName(Ljava/lang/String;Z)Ljava/io/File;
    .locals 1
    .param p1, "dstPath"    # Ljava/lang/String;
    .param p2, "isFile"    # Z

    .prologue
    .line 606
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 608
    const/4 v0, 0x0

    .line 611
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, p2}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->checkDirOrFileName(Ljava/io/File;Z)Ljava/io/File;

    move-result-object v0

    goto :goto_0
.end method

.method protected deleteInDatabase(Ljava/lang/String;)Z
    .locals 6
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 447
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 485
    :cond_0
    :goto_0
    return v1

    .line 452
    :cond_1
    const-string v3, "external"

    invoke-static {v3}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 478
    .local v0, "uri":Landroid/net/Uri;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mContentResolver:Landroid/content/ContentResolver;

    const-string v4, "_data = ?"

    new-array v5, v2, [Ljava/lang/String;

    aput-object p1, v5, v1

    invoke-virtual {v3, v0, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_0

    .line 480
    const-string v3, "FileOperationTask"

    const-string v4, "deleteInDatabase success"

    invoke-static {v1, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    move v1, v2

    .line 482
    goto :goto_0
.end method

.method protected deleteInFileSystem(Ljava/io/File;)Z
    .locals 15
    .param p1, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 355
    const/4 v11, 0x0

    const-string v12, "FileOperationTask"

    const-string v13, "deleteInFileSystem start"

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 356
    const/4 v5, 0x1

    .line 357
    .local v5, "retVal":Z
    const/4 v10, 0x0

    .local v10, "totalFileCount":I
    const/4 v9, 0x0

    .line 358
    .local v9, "successfulFileCount":I
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->isCancelled()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 359
    :cond_0
    const/4 v11, 0x0

    const-string v12, "FileOperationTask"

    const-string v13, "file is null or not exists or IsCancelled"

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 360
    const/4 v11, 0x0

    .line 441
    :goto_0
    return v11

    .line 365
    :cond_1
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->delete()Z

    move-result v11

    if-nez v11, :cond_a

    .line 367
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->canWrite()Z

    move-result v11

    if-nez v11, :cond_2

    .line 369
    const/4 v11, 0x0

    const-string v12, "FileOperationTask"

    const-string v13, "file is not writable"

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 371
    const/4 v11, 0x0

    goto :goto_0

    .line 376
    :cond_2
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v8

    .line 378
    .local v8, "subFiles":[Ljava/io/File;
    if-eqz v8, :cond_3

    array-length v11, v8

    if-nez v11, :cond_4

    .line 379
    :cond_3
    const/4 v11, 0x0

    const-string v12, "FileOperationTask"

    const-string v13, "subFiles is null or length 0"

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 380
    const/4 v11, 0x0

    const-string v12, "FileOperationTask"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Attempted Delete on : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " Successful : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 381
    const/4 v11, 0x0

    goto :goto_0

    .line 385
    :cond_4
    move-object v0, v8

    .local v0, "arr$":[Ljava/io/File;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_6

    aget-object v6, v0, v3

    .line 386
    .local v6, "sf":Ljava/io/File;
    add-int/lit8 v10, v10, 0x1

    .line 387
    invoke-virtual {p0, v6}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->deleteInFileSystem(Ljava/io/File;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 388
    add-int/lit8 v9, v9, 0x1

    .line 385
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 390
    :cond_5
    const/4 v5, 0x0

    goto :goto_2

    .line 395
    .end local v6    # "sf":Ljava/io/File;
    :cond_6
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 397
    .local v2, "filePath":Ljava/lang/String;
    if-eqz v2, :cond_9

    .line 399
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v11, v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSetHoemScreenPath(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 402
    sget-char v11, Ljava/io/File;->separatorChar:C

    invoke-virtual {v2, v11}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    invoke-virtual {v2, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 403
    .local v7, "shortcutName":Ljava/lang/String;
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mContext:Landroid/content/Context;

    invoke-static {v11, v2, v7}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteShortcutFromHome(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    .end local v7    # "shortcutName":Ljava/lang/String;
    :cond_7
    add-int/lit8 v10, v10, 0x1

    .line 407
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->delete()Z

    move-result v11

    if-eqz v11, :cond_8

    .line 408
    add-int/lit8 v9, v9, 0x1

    .line 410
    :cond_8
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->removeThumbnailCache(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 438
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v2    # "filePath":Ljava/lang/String;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v8    # "subFiles":[Ljava/io/File;
    :cond_9
    :goto_3
    const/4 v11, 0x0

    const-string v12, "FileOperationTask"

    const-string v13, "deleteInFileSystem finish"

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 439
    const/4 v11, 0x0

    const-string v12, "FileOperationTask"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Attempted Delete on : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " Successful : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    move v11, v5

    .line 441
    goto/16 :goto_0

    .line 415
    :cond_a
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 416
    .restart local v2    # "filePath":Ljava/lang/String;
    add-int/lit8 v10, v10, 0x1

    .line 417
    add-int/lit8 v9, v9, 0x1

    .line 418
    if-eqz v2, :cond_9

    .line 420
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v11, v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSetHoemScreenPath(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_b

    .line 423
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mContext:Landroid/content/Context;

    invoke-static {v11, v2}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteFileShortcutFromHome(Landroid/content/Context;Ljava/lang/String;)V

    .line 426
    :cond_b
    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->removeThumbnailCache(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 432
    .end local v2    # "filePath":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 434
    .local v1, "ex":Ljava/lang/SecurityException;
    const/4 v11, 0x0

    const-string v12, "FileOperationTask"

    const-string v13, "delete() - delete is failed"

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 435
    const/4 v11, 0x0

    const-string v12, "FileOperationTask"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Attempted Delete on : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " Successful : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method public isError()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 1292
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 3

    .prologue
    .line 328
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 330
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;->onCancelled(Ljava/util/ArrayList;I)V

    .line 334
    :cond_0
    return-void
.end method

.method protected onCancelled(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V
    .locals 2
    .param p1, "result"    # Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .prologue
    .line 340
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mTargetFolder:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/utils/Utils;->sendScan(Landroid/content/Context;Ljava/lang/String;)V

    .line 342
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onCancelled(Ljava/lang/Object;)V

    .line 343
    return-void
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 60
    check-cast p1, Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->onCancelled(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V

    return-void
.end method

.method protected onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V
    .locals 5
    .param p1, "result"    # Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .prologue
    const/4 v4, 0x0

    .line 275
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mTargetFolder:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/android/app/myfiles/utils/Utils;->sendScan(Landroid/content/Context;Ljava/lang/String;)V

    .line 276
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isKNOXFileRelayAvailable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isKNOXMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 277
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mSrcFolder:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/android/app/myfiles/utils/Utils;->sendScan(Landroid/content/Context;Ljava/lang/String;)V

    .line 280
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    if-eqz v1, :cond_3

    .line 282
    const/4 v0, 0x0

    .line 284
    .local v0, "data":Landroid/os/Bundle;
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mOperationType:I

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mOperationType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mTargetFolder:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 287
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "data":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 289
    .restart local v0    # "data":Landroid/os/Bundle;
    const-string v1, "target_folder"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mTargetFolder:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    :cond_2
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRenameCancelled:Z

    if-eqz v1, :cond_5

    .line 294
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-interface {v1, v2, v3}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;->onCancelled(Ljava/util/ArrayList;I)V

    .line 317
    .end local v0    # "data":Landroid/os/Bundle;
    :cond_3
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mTargetName:Ljava/util/ArrayList;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mTargetName:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_4

    .line 319
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mTargetName:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 322
    :cond_4
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 323
    return-void

    .line 298
    .restart local v0    # "data":Landroid/os/Bundle;
    :cond_5
    if-nez v0, :cond_6

    .line 300
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "data":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 303
    .restart local v0    # "data":Landroid/os/Bundle;
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mDoneFileList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_7

    .line 305
    const-string v1, "done_file_list"

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mDoneFileList:Ljava/util/ArrayList;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 308
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mDoneFolderList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_8

    .line 310
    const-string v1, "done_folder_list"

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mDoneFolderList:Ljava/util/ArrayList;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 313
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    invoke-interface {v1, v4, v4, v0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;->onCompleted(IILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 60
    check-cast p1, Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V

    return-void
.end method

.method public setFileOperationListener(Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    .prologue
    .line 348
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    .line 349
    return-void
.end method

.method protected abstract setupProgressView()V
.end method

.method protected showRenameDialog()Z
    .locals 4

    .prologue
    .line 761
    new-instance v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;-><init>(Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRunnable:Ljava/lang/Runnable;

    .line 1024
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->isAllOverwriteCheck:Z

    if-nez v1, :cond_1

    .line 1025
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRunnable:Ljava/lang/Runnable;

    monitor-enter v2

    .line 1026
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1029
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1036
    :goto_0
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRenameOverwriteDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    .line 1038
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRenameOverwriteDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 1040
    :cond_0
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1043
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRunRename:Z

    return v1

    .line 1031
    :catch_0
    move-exception v0

    .line 1033
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 1040
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1
.end method

.method protected showStopDialog(Ljava/io/File;)V
    .locals 4
    .param p1, "dstFile"    # Ljava/io/File;

    .prologue
    .line 1151
    new-instance v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$4;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$4;-><init>(Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;Ljava/io/File;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRunnable:Ljava/lang/Runnable;

    .line 1272
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRunnable:Ljava/lang/Runnable;

    monitor-enter v2

    .line 1273
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1276
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mWaitDialog:Z

    .line 1278
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mWaitDialog:Z

    if-eqz v1, :cond_0

    .line 1280
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1283
    :catch_0
    move-exception v0

    .line 1285
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 1287
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_0
    monitor-exit v2

    .line 1288
    return-void

    .line 1287
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method protected showToast(I)V
    .locals 2
    .param p1, "resId"    # I

    .prologue
    .line 1049
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 1061
    :goto_0
    return-void

    .line 1052
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mToast:Landroid/widget/Toast;

    if-nez v0, :cond_1

    .line 1054
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mContext:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mToast:Landroid/widget/Toast;

    .line 1060
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1058
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(I)V

    goto :goto_1
.end method

.method protected showToast(Ljava/lang/String;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/String;

    .prologue
    .line 1065
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 1077
    :goto_0
    return-void

    .line 1068
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mToast:Landroid/widget/Toast;

    if-nez v0, :cond_1

    .line 1070
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mContext:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mToast:Landroid/widget/Toast;

    .line 1076
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1074
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method protected showToastPrurals(II)V
    .locals 6
    .param p1, "resId"    # I
    .param p2, "quantity"    # I

    .prologue
    const/4 v5, 0x1

    .line 1081
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 1094
    :goto_0
    return-void

    .line 1084
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, p1, p2, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1085
    .local v0, "msg":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mToast:Landroid/widget/Toast;

    if-nez v1, :cond_1

    .line 1087
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mContext:Landroid/content/Context;

    invoke-static {v1, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mToast:Landroid/widget/Toast;

    .line 1093
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1091
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mToast:Landroid/widget/Toast;

    invoke-virtual {v1, v0}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method protected startRename(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "isFile"    # Z

    .prologue
    .line 736
    const/4 v0, 0x0

    .line 738
    .local v0, "i":I
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 742
    .local v1, "renamedFile":Ljava/io/File;
    :goto_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 745
    new-instance v1, Ljava/io/File;

    .end local v1    # "renamedFile":Ljava/io/File;
    invoke-virtual {p0, p1, v0, p2}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->addPostfix(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 747
    .restart local v1    # "renamedFile":Ljava/io/File;
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 750
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRename:Ljava/lang/String;

    .line 756
    return-void
.end method
