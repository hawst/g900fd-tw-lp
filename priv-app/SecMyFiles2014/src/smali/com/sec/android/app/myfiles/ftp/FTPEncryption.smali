.class public final enum Lcom/sec/android/app/myfiles/ftp/FTPEncryption;
.super Ljava/lang/Enum;
.source "FTPEncryption.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/myfiles/ftp/FTPEncryption;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

.field public static final enum EXPLICIT:Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

.field public static final enum IMPLICIT:Lcom/sec/android/app/myfiles/ftp/FTPEncryption;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 20
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    const-string v1, "IMPLICIT"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/FTPEncryption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPEncryption;->IMPLICIT:Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    .line 21
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    const-string v1, "EXPLICIT"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/myfiles/ftp/FTPEncryption;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPEncryption;->EXPLICIT:Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    .line 19
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPEncryption;->IMPLICIT:Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPEncryption;->EXPLICIT:Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPEncryption;->$VALUES:[Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPEncryption;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 19
    const-class v0, Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/myfiles/ftp/FTPEncryption;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/app/myfiles/ftp/FTPEncryption;->$VALUES:[Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    invoke-virtual {v0}, [Lcom/sec/android/app/myfiles/ftp/FTPEncryption;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    return-object v0
.end method
