.class public Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsFragment;
.source "CategoryHomeFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$34;,
        Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$ViewPagerType;
    }
.end annotation


# static fields
.field private static final MODULE:Ljava/lang/String; = "CategoryHomeFragment"

.field private static final REQUEST_ADD_SHORTCUT:I = 0x3

.field static mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

.field private static mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;


# instance fields
.field FTPargument:Landroid/os/Bundle;

.field private bDoNotCheckDropBox:Z

.field private bDoNotCheckFTP:Z

.field public bDropBoxFromShortcut:Z

.field private bRotateDevice:Z

.field public downloadHistory:Landroid/widget/RelativeLayout;

.field public downloadHistoryIcon:Landroid/widget/ImageView;

.field public downloadHistoryTitle:Landroid/widget/TextView;

.field private isReceiverRegistered:Z

.field public isResetNeeded:Z

.field private mActivity:Lcom/sec/android/app/myfiles/MainActivity;

.field private mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

.field private mCategoryGridView:Landroid/widget/GridView;

.field private mCategoryGridViewContainer:Landroid/widget/LinearLayout;

.field private mCloudHeaderContainer:Landroid/view/View;

.field private mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

.field private mCloudListView:Landroid/widget/ListView;

.field mConnectionChangeReceiver:Landroid/content/BroadcastReceiver;

.field private mDailog:Landroid/app/Dialog;

.field private mDevicesCollectionUpdatedListener:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;

.field private mDoNotCheckWLANOn:Z

.field private mDownloadedAppCount:I

.field private mDownloadedAppTotalSize:J

.field private mHandler:Landroid/os/Handler;

.field private mIsEasyMode:Z

.field private mLastOrientation:I

.field public mModeForWarningDialog:I

.field private mRemoteShareHeaderContainer:Landroid/view/View;

.field private mRemoteShareListAdapter:Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;

.field private mRemoteShareListContainer:Landroid/view/View;

.field private mRemoteShareListView:Landroid/widget/ListView;

.field public mResumeSate:Z

.field private mScreenState:I

.field private mScrollView:Landroid/widget/ScrollView;

.field private mSearchView:Landroid/widget/SearchView;

.field protected mSelectAllPopupWindow:Landroid/widget/ListPopupWindow;

.field private mSelectedShortcutType:I

.field private mShortcutActionReceiver:Landroid/content/BroadcastReceiver;

.field public mShortcutItemForWarning:Lcom/sec/android/app/myfiles/element/ShortcutItem;

.field private mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

.field private mShortcutListView:Landroid/widget/ListView;

.field private mShortcutRenameDialog:Landroid/app/DialogFragment;

.field private mShortcutsHeaderContainer:Landroid/view/View;

.field private mShortcutsListContainer:Landroid/view/View;

.field private mSizeLoadedAppCount:I

.field private mStartBrowserState:Z

.field private mStorageListAdapter:Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;

.field private mStorageListView:Landroid/widget/ListView;

.field private mTwCloudMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;

.field private mTwDragSelectedItemMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mTwShortcutMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;

.field private mUpdateCategorySizeHandler:Landroid/os/Handler;

.field private mWasShortCutInSelectModeEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 174
    sput-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    .line 176
    sput-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 104
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;-><init>()V

    .line 151
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mHandler:Landroid/os/Handler;

    .line 190
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->isReceiverRegistered:Z

    .line 192
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->isResetNeeded:Z

    .line 194
    iput v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mModeForWarningDialog:I

    .line 196
    iput-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->bDoNotCheckDropBox:Z

    .line 198
    iput-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->bDoNotCheckFTP:Z

    .line 201
    iput-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDoNotCheckWLANOn:Z

    .line 203
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->bDropBoxFromShortcut:Z

    .line 205
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mResumeSate:Z

    .line 209
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mIsEasyMode:Z

    .line 217
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->FTPargument:Landroid/os/Bundle;

    .line 229
    iput v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSelectedShortcutType:I

    .line 236
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->bRotateDevice:Z

    .line 238
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mConnectionChangeReceiver:Landroid/content/BroadcastReceiver;

    .line 286
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$2;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutActionReceiver:Landroid/content/BroadcastReceiver;

    .line 1656
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mUpdateCategorySizeHandler:Landroid/os/Handler;

    .line 3477
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$32;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$32;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mTwShortcutMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;

    .line 3542
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$33;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$33;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mTwCloudMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getDocumentsTotalSize()V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .prologue
    .line 104
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDownloadedAppTotalSize:J

    return-wide v0
.end method

.method static synthetic access$1114(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;J)J
    .locals 3
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;
    .param p1, "x1"    # J

    .prologue
    .line 104
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDownloadedAppTotalSize:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDownloadedAppTotalSize:J

    return-wide v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .prologue
    .line 104
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSizeLoadedAppCount:I

    return v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;
    .param p1, "x1"    # I

    .prologue
    .line 104
    iput p1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSizeLoadedAppCount:I

    return p1
.end method

.method static synthetic access$1208(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .prologue
    .line 104
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSizeLoadedAppCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSizeLoadedAppCount:I

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .prologue
    .line 104
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDownloadedAppCount:I

    return v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;
    .param p1, "x1"    # I

    .prologue
    .line 104
    iput p1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDownloadedAppCount:I

    return p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->deleteSelectedUserShortcut()V

    return-void
.end method

.method static synthetic access$1500()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;
    .locals 1

    .prologue
    .line 104
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDoNotCheckWLANOn:Z

    return v0
.end method

.method static synthetic access$1602(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 104
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDoNotCheckWLANOn:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->bDoNotCheckDropBox:Z

    return v0
.end method

.method static synthetic access$1702(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 104
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->bDoNotCheckDropBox:Z

    return p1
.end method

.method static synthetic access$1800(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->bDoNotCheckFTP:Z

    return v0
.end method

.method static synthetic access$1802(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 104
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->bDoNotCheckFTP:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mTwDragSelectedItemMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .prologue
    .line 104
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mScreenState:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mStorageListAdapter:Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mRemoteShareListAdapter:Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Landroid/widget/ScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mScrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mUpdateCategorySizeHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getDownloadedAppSize()V

    return-void
.end method

.method private deleteSelectedUserShortcut()V
    .locals 13

    .prologue
    const/4 v12, -0x1

    const/4 v9, 0x1

    const/4 v11, 0x0

    .line 2203
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v4

    .line 2204
    .local v4, "mShortcutItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v3

    .line 2205
    .local v3, "mCloudItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 2206
    .local v2, "item":Ljava/lang/Object;
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2208
    .end local v2    # "item":Ljava/lang/Object;
    :cond_0
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v7, v4}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteShortcuts(Landroid/content/Context;Ljava/util/ArrayList;)I

    move-result v0

    .line 2209
    .local v0, "deletedRows":I
    if-lez v0, :cond_1

    iget v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSelectedShortcutType:I

    if-eq v7, v12, :cond_1

    .line 2211
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v7, v4}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->removeAllItem(Ljava/util/ArrayList;)Z

    .line 2212
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListView:Landroid/widget/ListView;

    invoke-static {v7}, Lcom/sec/android/app/myfiles/ListViewHeightRefresh;->getListViewSize(Landroid/widget/ListView;)V

    .line 2214
    const-string v5, ""

    .line 2215
    .local v5, "selectText":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 2217
    .local v6, "selectedItemCount":I
    iget v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSelectedShortcutType:I

    sparse-switch v7, :sswitch_data_0

    .line 2237
    :goto_1
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v7, v5, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    .line 2245
    .end local v5    # "selectText":Ljava/lang/String;
    .end local v6    # "selectedItemCount":I
    :goto_2
    iput v12, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSelectedShortcutType:I

    .line 2247
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->finishSelectMode()V

    .line 2248
    return-void

    .line 2221
    .restart local v5    # "selectText":Ljava/lang/String;
    .restart local v6    # "selectedItemCount":I
    :sswitch_0
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0c0013

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-virtual {v7, v8, v6, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2222
    goto :goto_1

    .line 2225
    :sswitch_1
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0c0015

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-virtual {v7, v8, v6, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2226
    goto :goto_1

    .line 2229
    :sswitch_2
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0c0014

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-virtual {v7, v8, v6, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2230
    goto :goto_1

    .line 2233
    :sswitch_3
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0c000b

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-virtual {v7, v8, v6, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 2242
    .end local v5    # "selectText":Ljava/lang/String;
    .end local v6    # "selectedItemCount":I
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v8, 0x7f0b0063

    invoke-static {v7, v8, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto :goto_2

    .line 2217
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_2
        0xa -> :sswitch_1
        0x1a -> :sswitch_0
        0x23 -> :sswitch_3
    .end sparse-switch
.end method

.method public static getCategoryAdapterDevicesCollection()Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;
    .locals 1

    .prologue
    .line 2457
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    return-object v0
.end method

.method private getDocumentsTotalSize()V
    .locals 2

    .prologue
    .line 3396
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mUpdateCategorySizeHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 3397
    return-void
.end method

.method private getDownloadedAppSize()V
    .locals 12

    .prologue
    const/4 v8, 0x0

    const-wide/16 v10, 0x0

    .line 3410
    iput-wide v10, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDownloadedAppTotalSize:J

    .line 3412
    iput v8, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDownloadedAppCount:I

    .line 3414
    iput v8, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSizeLoadedAppCount:I

    .line 3416
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 3418
    .local v5, "packageManager":Landroid/content/pm/PackageManager;
    invoke-virtual {v5, v8}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v1

    .line 3420
    .local v1, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 3422
    .local v0, "appInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v2

    .line 3425
    .local v2, "currentUser":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    if-ge v3, v7, :cond_1

    .line 3427
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ApplicationInfo;

    .line 3429
    .local v4, "info":Landroid/content/pm/ApplicationInfo;
    iget v7, v4, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v7, v7, 0x1

    if-nez v7, :cond_0

    .line 3431
    iget v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDownloadedAppCount:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDownloadedAppCount:I

    .line 3425
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 3435
    .end local v4    # "info":Landroid/content/pm/ApplicationInfo;
    :cond_1
    iget v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDownloadedAppCount:I

    if-nez v7, :cond_3

    .line 3437
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    const/4 v8, 0x6

    invoke-virtual {v7, v8}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->getItemByType(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/myfiles/element/CategoryItem;

    invoke-virtual {v7, v10, v11}, Lcom/sec/android/app/myfiles/element/CategoryItem;->setTotalSize(J)V

    .line 3438
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7, v10, v11}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setDownloadedAppTotalSize(J)V

    .line 3439
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7, v10, v11}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSavedDownloadedAppTotalSize(J)V

    .line 3440
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->commitDownAppSize()Z

    .line 3475
    :cond_2
    return-void

    .line 3446
    :cond_3
    const/4 v3, 0x0

    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    if-ge v3, v7, :cond_2

    .line 3448
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ApplicationInfo;

    .line 3450
    .restart local v4    # "info":Landroid/content/pm/ApplicationInfo;
    iget v7, v4, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v7, v7, 0x1

    if-nez v7, :cond_4

    .line 3452
    new-instance v6, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$31;

    invoke-direct {v6, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$31;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    .line 3471
    .local v6, "packageStatsObserver":Landroid/content/pm/IPackageStatsObserver$Stub;
    iget-object v7, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v5, v7, v2, v6}, Landroid/content/pm/PackageManager;->getPackageSizeInfo(Ljava/lang/String;ILandroid/content/pm/IPackageStatsObserver;)V

    .line 3446
    .end local v6    # "packageStatsObserver":Landroid/content/pm/IPackageStatsObserver$Stub;
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public static getNearBydeviceCollectionSize()I
    .locals 1

    .prologue
    .line 1296
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->size()I

    move-result v0

    goto :goto_0
.end method

.method private static runAddFTP(Lcom/sec/android/app/myfiles/ftp/FTPType;Landroid/app/Activity;I)V
    .locals 4
    .param p0, "ftp"    # Lcom/sec/android/app/myfiles/ftp/FTPType;
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "shortcutIndex"    # I

    .prologue
    .line 2315
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2316
    .local v0, "argument":Landroid/os/Bundle;
    const-string v2, "shortcut_working_index_intent_key"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2317
    const-class v2, Lcom/sec/android/app/myfiles/ftp/FTPType;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPType;->ID:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2318
    const/16 v1, 0xd

    .line 2319
    .local v1, "fragmentId":I
    sget-object v2, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$34;->$SwitchMap$com$sec$android$app$myfiles$ftp$FTPType:[I

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2330
    :goto_0
    instance-of v2, p1, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v2, :cond_0

    .line 2331
    check-cast p1, Lcom/sec/android/app/myfiles/MainActivity;

    .end local p1    # "activity":Landroid/app/Activity;
    invoke-virtual {p1, v1, v0}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    .line 2333
    :cond_0
    return-void

    .line 2321
    .restart local p1    # "activity":Landroid/app/Activity;
    :pswitch_0
    const/16 v1, 0xd

    .line 2322
    goto :goto_0

    .line 2324
    :pswitch_1
    const/16 v1, 0xe

    .line 2325
    goto :goto_0

    .line 2327
    :pswitch_2
    const/16 v1, 0xf

    goto :goto_0

    .line 2319
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static scanForNearbyDevices(Landroid/app/Activity;Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;)V
    .locals 4
    .param p0, "parentActivity"    # Landroid/app/Activity;
    .param p1, "devicesCollectionUpdatedListener"    # Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;

    .prologue
    const/4 v3, 0x1

    .line 1301
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    if-nez v0, :cond_0

    .line 1303
    new-instance v0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;-><init>(Landroid/content/Context;Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;)V

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$10;

    invoke-direct {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$10;-><init>()V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;->refreshView(Ljava/lang/Runnable;)Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;->build()Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    .line 1314
    new-instance v0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;-><init>(Landroid/content/Context;Ljava/util/Collection;)V

    sput-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    .line 1318
    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    if-eqz v0, :cond_1

    .line 1320
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->checkAllshareEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1322
    const-string v0, "ChinaNalSecurity"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Common_ConfigLocalSecurityPolicy"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1324
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckWLANOn()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1325
    const/16 v0, 0x9

    invoke-interface {p1, v0}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;->showDialog(I)V

    .line 1354
    :goto_0
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    if-eqz v0, :cond_1

    .line 1362
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    invoke-interface {p1, v0}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;->setNearbyDevicesProvider(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)V

    .line 1363
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    invoke-interface {p1, v0}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;->setDevicesCollection(Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;)V

    .line 1367
    :cond_1
    return-void

    .line 1327
    :cond_2
    invoke-interface {p1, v3}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;->showToastForScanningNearbyDevices(Z)V

    .line 1328
    invoke-static {}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;->getInstance()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;->registerProvider(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)V

    .line 1329
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->loadDevices()V

    goto :goto_0

    .line 1334
    :cond_3
    invoke-interface {p1, v3}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;->showToastForScanningNearbyDevices(Z)V

    .line 1335
    invoke-static {}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;->getInstance()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;->registerProvider(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)V

    .line 1336
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->loadDevices()V

    goto :goto_0

    .line 1345
    :cond_4
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;->showToastForScanningNearbyDevices(Z)V

    .line 1347
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    goto :goto_0
.end method

.method private setActionBarIcon(I)V
    .locals 4
    .param p1, "orientation"    # I

    .prologue
    const/4 v3, 0x0

    const v2, 0x7f0f0144

    const v1, 0x7f0f0143

    .line 2614
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mOptionsMenu:Landroid/view/Menu;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2615
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 2617
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2621
    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2639
    :cond_1
    :goto_0
    return-void

    .line 2628
    :cond_2
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2630
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 2632
    :cond_3
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2634
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method private setupShortcutAdapter()V
    .locals 0

    .prologue
    .line 2297
    return-void
.end method

.method private setupViews(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 510
    const v0, 0x7f0f0071

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryGridViewContainer:Landroid/widget/LinearLayout;

    .line 511
    const v0, 0x7f0f0072

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryGridView:Landroid/widget/GridView;

    .line 512
    const v0, 0x7f0f007b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListView:Landroid/widget/ListView;

    .line 513
    const v0, 0x7f0f007e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mStorageListView:Landroid/widget/ListView;

    .line 514
    const v0, 0x7f0f0085

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListView:Landroid/widget/ListView;

    .line 515
    const v0, 0x7f0f0082

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mRemoteShareListView:Landroid/widget/ListView;

    .line 520
    const v0, 0x7f0f0070

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mScrollView:Landroid/widget/ScrollView;

    .line 521
    const v0, 0x7f0f0078

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutsHeaderContainer:Landroid/view/View;

    .line 522
    const v0, 0x7f0f007a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutsListContainer:Landroid/view/View;

    .line 523
    const v0, 0x7f0f0083

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudHeaderContainer:Landroid/view/View;

    .line 524
    const v0, 0x7f0f007f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mRemoteShareHeaderContainer:Landroid/view/View;

    .line 525
    const v0, 0x7f0f0081

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mRemoteShareListContainer:Landroid/view/View;

    .line 527
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->RemoteShareListRefresh()V

    .line 529
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mTwShortcutMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setTwMultiSelectedListener(Landroid/widget/AdapterView$OnTwMultiSelectedListener;)V

    .line 530
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mTwCloudMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setTwMultiSelectedListener(Landroid/widget/AdapterView$OnTwMultiSelectedListener;)V

    .line 532
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 533
    const v0, 0x7f0f0075

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->downloadHistory:Landroid/widget/RelativeLayout;

    .line 534
    const v0, 0x7f0f0076

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->downloadHistoryIcon:Landroid/widget/ImageView;

    .line 535
    const v0, 0x7f0f0077

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->downloadHistoryTitle:Landroid/widget/TextView;

    .line 536
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->downloadHistory:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 537
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->downloadHistory:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$5;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 554
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getShortcutArrayList()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mWasShortCutInSelectModeEnabled:Z

    if-eqz v0, :cond_7

    .line 557
    new-instance v0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getShortcutArrayList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    .line 568
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->refreshShortcutHeader()V

    .line 570
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getCloudArrayList()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 571
    new-instance v0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getCloudArrayList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-direct {v0, v1, p0, v2}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    .line 576
    :goto_1
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$6;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDevicesCollectionUpdatedListener:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;

    .line 616
    new-instance v0, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mRemoteShareListAdapter:Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;

    .line 619
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getAdapter(I)Landroid/widget/BaseAdapter;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    .line 620
    new-instance v0, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mStorageListAdapter:Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;

    .line 624
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mWasShortCutInSelectModeEnabled:Z

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->selectMode(Z)V

    .line 625
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mWasShortCutInSelectModeEnabled:Z

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->selectMode(Z)V

    .line 626
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mWasShortCutInSelectModeEnabled:Z

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->selectMode(Z)V

    .line 627
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mStorageListAdapter:Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;

    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mWasShortCutInSelectModeEnabled:Z

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->selectMode(Z)V

    .line 628
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mRemoteShareListAdapter:Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;

    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mWasShortCutInSelectModeEnabled:Z

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->selectMode(Z)V

    .line 648
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getmDevicesCollection()Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 650
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getmDevicesCollection()Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    .line 652
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->setShortcutAdapter(Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesShortcutAdapter;)V

    .line 654
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDevicesCollectionUpdatedListener:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->setDevicesCollectionUpdatedListener(Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;)V

    .line 656
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    sget-object v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->setDevicesCollection(Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;)V

    .line 672
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryGridView:Landroid/widget/GridView;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 680
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListView:Landroid/widget/ListView;

    if-eqz v0, :cond_2

    .line 681
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 682
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListView:Landroid/widget/ListView;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/ListViewHeightRefresh;->getListViewSize(Landroid/widget/ListView;)V

    .line 683
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 684
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListView:Landroid/widget/ListView;

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$7;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$7;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 745
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryGridView:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 747
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mRemoteShareListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mRemoteShareListAdapter:Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 749
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mRemoteShareListView:Landroid/widget/ListView;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/ListViewHeightRefresh;->getListViewSize(Landroid/widget/ListView;)V

    .line 751
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mRemoteShareListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->remoteShareClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 753
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 755
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListView:Landroid/widget/ListView;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/ListViewHeightRefresh;->getListViewSize(Landroid/widget/ListView;)V

    .line 756
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->getCount()I

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 757
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudHeaderContainer:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 758
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudHeaderContainer:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 760
    :cond_4
    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->setCloudListShowing(Z)V

    .line 763
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mStorageListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mStorageListAdapter:Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 764
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mStorageListView:Landroid/widget/ListView;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/ListViewHeightRefresh;->getListViewSize(Landroid/widget/ListView;)V

    .line 765
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mStorageListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mLocalItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 767
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 768
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListView:Landroid/widget/ListView;

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$8;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$8;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 803
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mScreenState:I

    packed-switch v0, :pswitch_data_0

    .line 825
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryGridView:Landroid/widget/GridView;

    invoke-virtual {v0, v4}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 829
    :cond_6
    :goto_2
    return-void

    .line 561
    :cond_7
    new-instance v0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    goto/16 :goto_0

    .line 573
    :cond_8
    new-instance v0, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    goto/16 :goto_1

    .line 809
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getEasyMode()Z

    move-result v0

    if-nez v0, :cond_6

    .line 811
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mScreenState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_9

    .line 812
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryGridView:Landroid/widget/GridView;

    invoke-virtual {v0, v5}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 816
    :goto_3
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mScreenState:I

    if-ne v0, v5, :cond_a

    .line 817
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryGridViewContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09009a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_2

    .line 814
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryGridView:Landroid/widget/GridView;

    invoke-virtual {v0, v4}, Landroid/widget/GridView;->setNumColumns(I)V

    goto :goto_3

    .line 819
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryGridViewContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09006a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_2

    .line 803
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private updateCategorySize()V
    .locals 2

    .prologue
    .line 1521
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1654
    return-void
.end method


# virtual methods
.method public AsyncOpenCompleted()V
    .locals 0

    .prologue
    .line 2476
    return-void
.end method

.method public CloudListRefresh()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3146
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    if-eqz v0, :cond_2

    .line 3147
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListView:Landroid/widget/ListView;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/ListViewHeightRefresh;->getListViewSize(Landroid/widget/ListView;)V

    .line 3148
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3149
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudHeaderContainer:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 3150
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudHeaderContainer:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 3152
    :cond_1
    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->setCloudListShowing(Z)V

    .line 3160
    :cond_2
    :goto_0
    return-void

    .line 3154
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudHeaderContainer:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 3155
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudHeaderContainer:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 3157
    :cond_4
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->setCloudListShowing(Z)V

    goto :goto_0
.end method

.method public RemoteShareListRefresh()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 3163
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mRemoteShareHeaderContainer:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mRemoteShareListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mRemoteShareListContainer:Landroid/view/View;

    if-nez v0, :cond_1

    .line 3175
    :cond_0
    :goto_0
    return-void

    .line 3166
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isRemoteShareEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 3167
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mRemoteShareHeaderContainer:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 3168
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mRemoteShareListView:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 3169
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mRemoteShareListContainer:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 3171
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mRemoteShareHeaderContainer:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 3172
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mRemoteShareListView:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 3173
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mRemoteShareListContainer:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public StorageListRefresh()V
    .locals 1

    .prologue
    .line 3140
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mStorageListAdapter:Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;

    if-eqz v0, :cond_0

    .line 3141
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mStorageListAdapter:Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->onRefresh()V

    .line 3142
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mStorageListView:Landroid/widget/ListView;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/ListViewHeightRefresh;->getListViewSize(Landroid/widget/ListView;)V

    .line 3144
    :cond_0
    return-void
.end method

.method public clickShortcutEditFtp()V
    .locals 2

    .prologue
    .line 2913
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->getSelectedItemCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2914
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->shortcutEdit()V

    .line 2918
    :goto_0
    return-void

    .line 2916
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->shortcutEdit()V

    goto :goto_0
.end method

.method public clickShortcutRename()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 2874
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 2876
    .local v1, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v5

    const/16 v6, 0x1a

    if-ne v5, v6, :cond_7

    .line 2877
    new-instance v0, Ljava/io/File;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2878
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxRootFolder(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretMode(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    const-string v6, "/storage/extSdCard"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    sget-boolean v5, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v5, :cond_2

    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    const-string v6, "/storage/UsbDrive"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_7

    sget-boolean v5, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-nez v5, :cond_7

    .line 2883
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 2884
    .local v3, "res":Landroid/content/res/Resources;
    const v5, 0x7f0c0013

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v3, v5, v9, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2885
    .local v2, "notiStr":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v5, v2, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 2886
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteShortcut(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v4

    .line 2887
    .local v4, "rowsDeleted":I
    if-lez v4, :cond_6

    .line 2888
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v5, v1}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->removeItem(Ljava/lang/Object;)Z

    .line 2889
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->onRefresh()V

    .line 2890
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListView:Landroid/widget/ListView;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/ListViewHeightRefresh;->getListViewSize(Landroid/widget/ListView;)V

    .line 2891
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setAllItemCount()V

    .line 2892
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 2893
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->getCount()I

    move-result v5

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->getCount()I

    move-result v5

    if-nez v5, :cond_4

    .line 2894
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->finishSelectMode()V

    .line 2911
    .end local v0    # "file":Ljava/io/File;
    .end local v2    # "notiStr":Ljava/lang/String;
    .end local v3    # "res":Landroid/content/res/Resources;
    .end local v4    # "rowsDeleted":I
    :cond_3
    :goto_0
    return-void

    .line 2895
    .restart local v0    # "file":Ljava/io/File;
    .restart local v2    # "notiStr":Ljava/lang/String;
    .restart local v3    # "res":Landroid/content/res/Resources;
    .restart local v4    # "rowsDeleted":I
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->getCount()I

    move-result v5

    if-ne v5, v9, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->getCount()I

    move-result v5

    if-nez v5, :cond_3

    .line 2896
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    invoke-virtual {v5, v8}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 2897
    .restart local v1    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v5

    const/16 v6, 0x8

    if-eq v5, v6, :cond_5

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v5

    const/16 v6, 0x1f

    if-ne v5, v6, :cond_3

    .line 2898
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->finishSelectMode()V

    goto :goto_0

    .line 2904
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v6, 0x7f0b0063

    invoke-static {v5, v6, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 2910
    .end local v0    # "file":Ljava/io/File;
    .end local v2    # "notiStr":Ljava/lang/String;
    .end local v3    # "res":Landroid/content/res/Resources;
    .end local v4    # "rowsDeleted":I
    :cond_7
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->shortcutRename()Landroid/app/DialogFragment;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutRenameDialog:Landroid/app/DialogFragment;

    goto :goto_0
.end method

.method public dismissDialog()V
    .locals 1

    .prologue
    .line 1998
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDailog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDailog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1999
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDailog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 2000
    :cond_0
    return-void
.end method

.method protected dismissPopupMenu()V
    .locals 1

    .prologue
    .line 2762
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSelectAllPopupWindow:Landroid/widget/ListPopupWindow;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v0, :cond_0

    .line 2764
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/MainActivity;->getSelectAllPopupWindow()Landroid/widget/ListPopupWindow;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSelectAllPopupWindow:Landroid/widget/ListPopupWindow;

    .line 2766
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSelectAllPopupWindow:Landroid/widget/ListPopupWindow;

    if-eqz v0, :cond_1

    .line 2768
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSelectAllPopupWindow:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->dismiss()V

    .line 2769
    :cond_1
    return-void
.end method

.method public finishDragMode()V
    .locals 0

    .prologue
    .line 2657
    return-void
.end method

.method public finishSelectMode()V
    .locals 3

    .prologue
    .line 2254
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->finishSelectMode()V

    .line 2255
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    if-eqz v0, :cond_0

    .line 2256
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->finishSelectMode()V

    .line 2258
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    if-eqz v0, :cond_1

    .line 2259
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->finishSelectMode()V

    .line 2261
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mStorageListAdapter:Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;

    if-eqz v0, :cond_2

    .line 2262
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mStorageListAdapter:Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->finishSelectMode()V

    .line 2264
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    if-eqz v0, :cond_3

    .line 2265
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->finishSelectMode()V

    .line 2267
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mRemoteShareListAdapter:Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;

    if-eqz v0, :cond_4

    .line 2268
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mRemoteShareListAdapter:Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->finishSelectMode()V

    .line 2270
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2272
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->downloadHistory:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->downloadHistoryIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->downloadHistoryTitle:Landroid/widget/TextView;

    if-nez v0, :cond_6

    .line 2281
    :cond_5
    :goto_0
    return-void

    .line 2276
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->downloadHistory:Landroid/widget/RelativeLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 2277
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->downloadHistoryIcon:Landroid/widget/ImageView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 2278
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->downloadHistoryTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public getMlastOrientation()I
    .locals 1

    .prologue
    .line 2644
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mLastOrientation:I

    return v0
.end method

.method public getNearbyDevicesProvider()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;
    .locals 1

    .prologue
    .line 2454
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    return-object v0
.end method

.method public getOptionsMenuResId(Z)I
    .locals 1
    .param p1, "isSelectMode"    # Z

    .prologue
    .line 2303
    if-eqz p1, :cond_0

    .line 2305
    const v0, 0x7f0e0008

    .line 2309
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0e0007

    goto :goto_0
.end method

.method public getStartBrowserState()Z
    .locals 1

    .prologue
    .line 1276
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mStartBrowserState:Z

    return v0
.end method

.method public getmCloudListAdapter()Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;
    .locals 1

    .prologue
    .line 2715
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    return-object v0
.end method

.method public getmShortcutListAdapter()Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;
    .locals 1

    .prologue
    .line 2711
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    return-object v0
.end method

.method public mCloudItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 3179
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$29;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    return-object v0
.end method

.method public mLocalItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 3108
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$28;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$28;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    return-object v0
.end method

.method public mShortcutItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 2921
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$27;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    return-object v0
.end method

.method public nearByDevicesConnected()Z
    .locals 1

    .prologue
    .line 1285
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    if-nez v0, :cond_0

    .line 1287
    const/4 v0, 0x0

    .line 1291
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 15
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 1372
    invoke-super/range {p0 .. p3}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1379
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v1, :cond_7

    const/4 v1, -0x1

    move/from16 v0, p2

    if-ne v0, v1, :cond_7

    .line 1381
    sparse-switch p1, :sswitch_data_0

    .line 1502
    :cond_0
    :goto_0
    return-void

    .line 1386
    :sswitch_0
    const-string v1, "FILE"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1389
    .local v2, "path":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "Baidu/"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1390
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0b00fa

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1400
    .local v3, "shortcutName":Ljava/lang/String;
    :goto_1
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1401
    const-string v1, "Baidu/"

    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1402
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/16 v4, 0x1f

    const/4 v5, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Lcom/sec/android/app/myfiles/utils/Utils;->addShortCut(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/drawable/Drawable;Z)V

    .line 1419
    :goto_2
    new-instance v1, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-direct {v1, v4, p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    .line 1420
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListView:Landroid/widget/ListView;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1421
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListView:Landroid/widget/ListView;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/ListViewHeightRefresh;->getListViewSize(Landroid/widget/ListView;)V

    .line 1422
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutsHeaderContainer:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutsListContainer:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 1423
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutsHeaderContainer:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1424
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutsListContainer:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1392
    .end local v3    # "shortcutName":Ljava/lang/String;
    :cond_1
    const-string v1, "Dropbox/"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1393
    const-string v3, "Dropbox"

    .restart local v3    # "shortcutName":Ljava/lang/String;
    goto :goto_1

    .line 1395
    .end local v3    # "shortcutName":Ljava/lang/String;
    :cond_2
    sget-char v1, Ljava/io/File;->separatorChar:C

    invoke-virtual {v2, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v2, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "shortcutName":Ljava/lang/String;
    goto :goto_1

    .line 1405
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/16 v4, 0x1a

    const/4 v5, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Lcom/sec/android/app/myfiles/utils/Utils;->addShortCut(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/drawable/Drawable;Z)V

    goto :goto_2

    .line 1410
    :cond_4
    const-string v1, "Dropbox/"

    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1411
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/16 v4, 0x8

    const/4 v5, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Lcom/sec/android/app/myfiles/utils/Utils;->addShortCut(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/drawable/Drawable;Z)V

    goto :goto_2

    .line 1414
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/16 v4, 0x1a

    const/4 v5, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Lcom/sec/android/app/myfiles/utils/Utils;->addShortCut(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/drawable/Drawable;Z)V

    goto :goto_2

    .line 1430
    .end local v2    # "path":Ljava/lang/String;
    .end local v3    # "shortcutName":Ljava/lang/String;
    :sswitch_1
    if-eqz p3, :cond_0

    .line 1432
    const-string v1, "add_shortcut_tag"

    const v4, 0x7f0b005b

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    .line 1434
    .local v11, "result":I
    const-string v1, "shortcut_working_index_intent_key"

    const/4 v4, -0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v12

    .line 1437
    .local v12, "shortcutIndex":I
    sparse-switch v11, :sswitch_data_1

    goto/16 :goto_0

    .line 1440
    :sswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1441
    new-instance v10, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.myfiles.PICK_SELECT_PATH"

    invoke-direct {v10, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1442
    .local v10, "intent":Landroid/content/Intent;
    const-string v1, "from_add_shortcut"

    const/4 v4, 0x1

    invoke-virtual {v10, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1443
    const-string v1, "shortcut_working_index_intent_key"

    invoke-virtual {v10, v1, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1447
    const/4 v1, 0x3

    :try_start_0
    invoke-virtual {p0, v10, v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1448
    :catch_0
    move-exception v9

    .line 1450
    .local v9, "e":Landroid/content/ActivityNotFoundException;
    const/4 v1, 0x2

    const-string v4, "CategoryHomeFragment"

    const-string v5, "com.sec.android.app.myfiles.PICK_SELECT_PATH not found - result - add shortcut"

    invoke-static {v1, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1456
    .end local v9    # "e":Landroid/content/ActivityNotFoundException;
    .end local v10    # "intent":Landroid/content/Intent;
    :sswitch_3
    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPType;->FTP:Lcom/sec/android/app/myfiles/ftp/FTPType;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1, v4, v12}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->runAddFTP(Lcom/sec/android/app/myfiles/ftp/FTPType;Landroid/app/Activity;I)V

    goto/16 :goto_0

    .line 1460
    :sswitch_4
    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPType;->FTPS:Lcom/sec/android/app/myfiles/ftp/FTPType;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1, v4, v12}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->runAddFTP(Lcom/sec/android/app/myfiles/ftp/FTPType;Landroid/app/Activity;I)V

    goto/16 :goto_0

    .line 1464
    :sswitch_5
    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPType;->SFTP:Lcom/sec/android/app/myfiles/ftp/FTPType;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1, v4, v12}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->runAddFTP(Lcom/sec/android/app/myfiles/ftp/FTPType;Landroid/app/Activity;I)V

    goto/16 :goto_0

    .line 1473
    .end local v11    # "result":I
    .end local v12    # "shortcutIndex":I
    :sswitch_6
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 1474
    .local v8, "cv":Landroid/content/ContentValues;
    const-string v1, "title"

    const-string v4, "result_value"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1475
    const-string v13, "_data = ?"

    .line 1476
    .local v13, "where":Ljava/lang/String;
    const/4 v1, 0x1

    new-array v14, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v4, "FILE"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v14, v1

    .line 1477
    .local v14, "whereArgs":[Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Shortcut;->getContentUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v1, v4, v8, v13, v14}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1479
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v1, :cond_0

    .line 1481
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v4, "category_home"

    invoke-virtual {v1, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 1492
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->onRefresh()V

    goto/16 :goto_0

    .line 1496
    .end local v8    # "cv":Landroid/content/ContentValues;
    .end local v13    # "where":Ljava/lang/String;
    .end local v14    # "whereArgs":[Ljava/lang/String;
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    move/from16 v0, p1

    if-ne v0, v1, :cond_0

    .line 1497
    new-instance v1, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-direct {v1, v4, p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    .line 1498
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListView:Landroid/widget/ListView;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1499
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListView:Landroid/widget/ListView;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/ListViewHeightRefresh;->getListViewSize(Landroid/widget/ListView;)V

    goto/16 :goto_0

    .line 1381
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x7 -> :sswitch_1
        0x10 -> :sswitch_6
    .end sparse-switch

    .line 1437
    :sswitch_data_1
    .sparse-switch
        0x7f0b005b -> :sswitch_2
        0x7f0b00a6 -> :sswitch_4
        0x7f0b00a7 -> :sswitch_5
        0x7f0b011b -> :sswitch_3
    .end sparse-switch
.end method

.method public onBackPressed()Z
    .locals 1

    .prologue
    .line 949
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 951
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->finishSelectMode()V

    .line 953
    const/4 v0, 0x1

    .line 956
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 9
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x3

    .line 2536
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    iput v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mLastOrientation:I

    .line 2538
    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mScreenState:I

    .line 2540
    .local v1, "oldScreenState":I
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->getScreenState(Landroid/content/Context;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mScreenState:I

    .line 2541
    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mScreenState:I

    packed-switch v2, :pswitch_data_0

    .line 2564
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryGridView:Landroid/widget/GridView;

    invoke-virtual {v2, v8}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 2568
    :cond_0
    :goto_0
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setActionBarIcon(I)V

    .line 2570
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v2, v2, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v2, :cond_6

    .line 2571
    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mScreenState:I

    if-eq v2, v6, :cond_1

    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mScreenState:I

    if-ne v2, v8, :cond_2

    .line 2573
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v2, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mIsSearchMode:Z

    if-eqz v2, :cond_2

    .line 2574
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$26;

    invoke-direct {v3, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$26;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2584
    :cond_2
    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mScreenState:I

    if-eq v2, v6, :cond_3

    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mScreenState:I

    if-eq v2, v8, :cond_3

    if-ne v1, v6, :cond_5

    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mScreenState:I

    if-nez v2, :cond_5

    .line 2587
    :cond_3
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2589
    .local v0, "b":Landroid/os/Bundle;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    if-eqz v2, :cond_4

    .line 2590
    const-string v2, "shortcut_mode_enable_intent_key"

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->isSelectMode()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2591
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView(Landroid/os/Bundle;)V

    .line 2592
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutRenameDialog:Landroid/app/DialogFragment;

    if-eqz v2, :cond_5

    .line 2593
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutRenameDialog:Landroid/app/DialogFragment;

    invoke-virtual {v2}, Landroid/app/DialogFragment;->dismissAllowingStateLoss()V

    .line 2597
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_5
    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mScreenState:I

    if-eq v1, v2, :cond_a

    .line 2599
    iput-boolean v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->bRotateDevice:Z

    .line 2606
    :cond_6
    :goto_1
    return-void

    .line 2547
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "easy_mode_switch"

    invoke-static {v2, v3, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "easy_mode_myfiles"

    invoke-static {v2, v3, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_0

    .line 2550
    :cond_7
    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mScreenState:I

    if-ne v2, v7, :cond_8

    .line 2551
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryGridView:Landroid/widget/GridView;

    invoke-virtual {v2, v6}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 2555
    :goto_2
    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mScreenState:I

    if-ne v2, v6, :cond_9

    .line 2556
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryGridViewContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09009a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto/16 :goto_0

    .line 2553
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryGridView:Landroid/widget/GridView;

    invoke-virtual {v2, v8}, Landroid/widget/GridView;->setNumColumns(I)V

    goto :goto_2

    .line 2558
    :cond_9
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryGridViewContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09006a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto/16 :goto_0

    .line 2603
    :cond_a
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->bRotateDevice:Z

    goto :goto_1

    .line 2541
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 252
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 254
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 256
    .local v0, "argument":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 258
    const-string v2, "id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mFragmentId:I

    .line 260
    const-string v2, "shortcut_mode_enable_intent_key"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mWasShortCutInSelectModeEnabled:Z

    .line 267
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutActionReceiver:Landroid/content/BroadcastReceiver;

    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "com.sec.android.action.SHORTCUT_ACTION"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/myfiles/AbsMainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 270
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->isReceiverRegistered:Z

    if-nez v2, :cond_1

    .line 271
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mConnectionChangeReceiver:Landroid/content/BroadcastReceiver;

    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/myfiles/AbsMainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 272
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->isReceiverRegistered:Z

    .line 275
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->getScreenState(Landroid/content/Context;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mScreenState:I

    .line 277
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v2, Lcom/sec/android/app/myfiles/MainActivity;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    .line 279
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSearchView:Landroid/widget/SearchView;

    .line 281
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v1

    .line 282
    .local v1, "easyModeState":Z
    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->setEasyMode(Z)V

    .line 283
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 10
    .param p1, "id"    # I

    .prologue
    const/4 v9, 0x0

    const v8, 0x7f0b0017

    const/4 v7, 0x0

    .line 1744
    const/4 v1, 0x0

    .line 1746
    .local v1, "dialog":Landroid/app/AlertDialog$Builder;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v6, :cond_7

    .line 1748
    new-instance v1, Landroid/app/AlertDialog$Builder;

    .end local v1    # "dialog":Landroid/app/AlertDialog$Builder;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-direct {v1, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1750
    .restart local v1    # "dialog":Landroid/app/AlertDialog$Builder;
    packed-switch p1, :pswitch_data_0

    .line 1992
    :goto_0
    :pswitch_0
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDailog:Landroid/app/Dialog;

    .line 1994
    :goto_1
    return-object v6

    .line 1755
    :pswitch_1
    const v6, 0x7f0b0020

    new-instance v7, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$13;

    invoke-direct {v7, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$13;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    invoke-virtual {v1, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1767
    new-instance v6, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$14;

    invoke-direct {v6, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$14;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    invoke-virtual {v1, v8, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1776
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDailog:Landroid/app/Dialog;

    goto :goto_0

    .line 1784
    :pswitch_2
    move v2, p1

    .line 1785
    .local v2, "dialogId":I
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 1787
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v6, 0x7f040033

    invoke-virtual {v3, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1789
    .local v0, "customView":Landroid/view/View;
    const v6, 0x7f0b00df

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1790
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 1791
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isChinaModel()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1792
    const v6, 0x7f0f00c8

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 1793
    .local v4, "mobileDataWarningDialog":Landroid/widget/TextView;
    const v6, 0x7f0b0087

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(I)V

    .line 1795
    .end local v4    # "mobileDataWarningDialog":Landroid/widget/TextView;
    :cond_0
    new-instance v6, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$15;

    invoke-direct {v6, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$15;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 1809
    const v6, 0x7f0b0015

    new-instance v7, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$16;

    invoke-direct {v7, p0, v2}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$16;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;I)V

    invoke-virtual {v1, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1848
    new-instance v6, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$17;

    invoke-direct {v6, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$17;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    invoke-virtual {v1, v8, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1856
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDailog:Landroid/app/Dialog;

    goto :goto_0

    .line 1865
    .end local v0    # "customView":Landroid/view/View;
    .end local v2    # "dialogId":I
    .end local v3    # "inflater":Landroid/view/LayoutInflater;
    :pswitch_3
    move v2, p1

    .line 1867
    .restart local v2    # "dialogId":I
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v6}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v5

    .line 1869
    .local v5, "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    const/4 v6, 0x4

    if-ne v2, v6, :cond_1

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckFTP()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1872
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->startBrowserFTPorDropBox()V

    move-object v6, v7

    .line 1873
    goto/16 :goto_1

    .line 1875
    :cond_1
    const/4 v6, 0x3

    if-eq v2, v6, :cond_2

    const/4 v6, 0x5

    if-ne v2, v6, :cond_5

    :cond_2
    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckDropBox()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1878
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v6, v6, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v6, :cond_3

    .line 1880
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setStartBrowserState(Z)V

    .line 1882
    iget-boolean v6, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->bDropBoxFromShortcut:Z

    if-eqz v6, :cond_4

    .line 1885
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->startBrowserFTPorDropBox()V

    :cond_3
    :goto_2
    move-object v6, v7

    .line 1892
    goto/16 :goto_1

    .line 1888
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v6, Lcom/sec/android/app/myfiles/MainActivity;

    iget v8, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mModeForWarningDialog:I

    invoke-virtual {v6, v8, v7}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto :goto_2

    .line 1896
    :cond_5
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 1898
    .restart local v3    # "inflater":Landroid/view/LayoutInflater;
    const v6, 0x7f040032

    invoke-virtual {v3, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1900
    .restart local v0    # "customView":Landroid/view/View;
    const v6, 0x7f0b0125

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1902
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isChinaModel()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1903
    const v6, 0x7f0f00c5

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 1904
    .restart local v4    # "mobileDataWarningDialog":Landroid/widget/TextView;
    const v6, 0x7f0b00e0

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(I)V

    .line 1906
    .end local v4    # "mobileDataWarningDialog":Landroid/widget/TextView;
    :cond_6
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 1908
    new-instance v6, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$18;

    invoke-direct {v6, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$18;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 1922
    const v6, 0x7f0b017a

    new-instance v7, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$19;

    invoke-direct {v7, p0, v2, v5}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$19;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;ILcom/sec/android/app/myfiles/utils/SharedDataStore;)V

    invoke-virtual {v1, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1963
    new-instance v6, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$20;

    invoke-direct {v6, p0, v2}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$20;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;I)V

    invoke-virtual {v1, v8, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1986
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDailog:Landroid/app/Dialog;

    goto/16 :goto_0

    .end local v0    # "customView":Landroid/view/View;
    .end local v2    # "dialogId":I
    .end local v3    # "inflater":Landroid/view/LayoutInflater;
    .end local v5    # "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    :cond_7
    move-object v6, v7

    .line 1994
    goto/16 :goto_1

    .line 1750
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method protected onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 2347
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$25;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$25;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    return-object v0
.end method

.method protected onCreateItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;
    .locals 1

    .prologue
    .line 2463
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreateSelectedItemChangeListener()Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;
    .locals 1

    .prologue
    .line 2470
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 322
    iget v9, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mScreenState:I

    packed-switch v9, :pswitch_data_0

    .line 345
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getEasyMode()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 347
    const v1, 0x7f04001b

    .line 349
    .local v1, "layoutResId":I
    iput-boolean v11, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mIsEasyMode:Z

    .line 361
    :goto_0
    invoke-virtual {p1, v1, p2, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    .line 363
    .local v8, "view":Landroid/view/View;
    iget-boolean v9, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mIsEasyMode:Z

    if-nez v9, :cond_0

    .line 373
    const v9, 0x7f0f0074

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 375
    .local v3, "mDownloadHeader":Landroid/widget/TextView;
    const v9, 0x7f0b014d

    invoke-virtual {p0, v9}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 377
    .local v0, "description":Ljava/lang/String;
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 379
    const v9, 0x7f0f0079

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 381
    .local v6, "mShortcutHeader":Landroid/widget/TextView;
    const v9, 0x7f0b015e

    invoke-virtual {p0, v9}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 383
    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 385
    const v9, 0x7f0f007d

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 387
    .local v4, "mLocalStorageHeader":Landroid/widget/TextView;
    const v9, 0x7f0b015a

    invoke-virtual {p0, v9}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 389
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 391
    const v9, 0x7f0f0080

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 393
    .local v5, "mRemoteShareHeader":Landroid/widget/TextView;
    const v9, 0x7f0b000f

    invoke-virtual {p0, v9}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 395
    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 397
    const v9, 0x7f0f0084

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 399
    .local v2, "mCloudHeader":Landroid/widget/TextView;
    const v9, 0x7f0b0154

    invoke-virtual {p0, v9}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 401
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 404
    .end local v0    # "description":Ljava/lang/String;
    .end local v2    # "mCloudHeader":Landroid/widget/TextView;
    .end local v3    # "mDownloadHeader":Landroid/widget/TextView;
    .end local v4    # "mLocalStorageHeader":Landroid/widget/TextView;
    .end local v5    # "mRemoteShareHeader":Landroid/widget/TextView;
    .end local v6    # "mShortcutHeader":Landroid/widget/TextView;
    :cond_0
    new-instance v9, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$3;

    invoke-direct {v9, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$3;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    invoke-virtual {v8, v9}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 467
    invoke-virtual {v8, v11}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 472
    const v9, 0x7f0f0070

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ScrollView;

    .line 474
    .local v7, "sv":Landroid/widget/ScrollView;
    if-eqz v7, :cond_1

    .line 476
    new-instance v9, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$4;

    invoke-direct {v9, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$4;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    invoke-virtual {v7, v9}, Landroid/widget/ScrollView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 498
    :cond_1
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mTwDragSelectedItemMap:Ljava/util/HashMap;

    if-nez v9, :cond_2

    .line 499
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    iput-object v9, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mTwDragSelectedItemMap:Ljava/util/HashMap;

    .line 502
    :cond_2
    invoke-direct {p0, v8}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setupViews(Landroid/view/View;)V

    .line 504
    return-object v8

    .line 326
    .end local v1    # "layoutResId":I
    .end local v7    # "sv":Landroid/widget/ScrollView;
    .end local v8    # "view":Landroid/view/View;
    :pswitch_1
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getEasyMode()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 327
    const v1, 0x7f04001b

    .line 328
    .restart local v1    # "layoutResId":I
    iput-boolean v11, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mIsEasyMode:Z

    goto/16 :goto_0

    .line 330
    .end local v1    # "layoutResId":I
    :cond_3
    const v1, 0x7f04001a

    .line 331
    .restart local v1    # "layoutResId":I
    iput-boolean v10, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mIsEasyMode:Z

    goto/16 :goto_0

    .line 337
    .end local v1    # "layoutResId":I
    :pswitch_2
    const v1, 0x7f04001c

    .line 339
    .restart local v1    # "layoutResId":I
    iput-boolean v10, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mIsEasyMode:Z

    goto/16 :goto_0

    .line 353
    .end local v1    # "layoutResId":I
    :cond_4
    const v1, 0x7f04001a

    .line 355
    .restart local v1    # "layoutResId":I
    iput-boolean v10, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mIsEasyMode:Z

    goto/16 :goto_0

    .line 322
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    .line 2664
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v2, :cond_3

    .line 2666
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutActionReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2668
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->getArrayList()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2669
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->getArrayList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setShortcutArrayList(Ljava/util/ArrayList;)V

    .line 2671
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->getArrayList()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2672
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->getArrayList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setCloudArrayList(Ljava/util/ArrayList;)V

    .line 2674
    :cond_1
    sget-object v2, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    if-eqz v2, :cond_2

    .line 2675
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    sget-object v3, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setmDevicesCollection(Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;)V

    .line 2677
    :cond_2
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->bRotateDevice:Z

    if-nez v2, :cond_3

    .line 2679
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->getOtherConfirmDeleteInstance(Landroid/app/Dialog;)Landroid/app/Dialog;

    move-result-object v1

    .line 2680
    .local v1, "otherInstance":Landroid/app/Dialog;
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/app/Dialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2682
    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 2687
    .end local v1    # "otherInstance":Landroid/app/Dialog;
    :cond_3
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->isReceiverRegistered:Z

    if-eqz v2, :cond_5

    .line 2689
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v2, :cond_4

    .line 2690
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mConnectionChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2695
    :cond_4
    :goto_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->isReceiverRegistered:Z

    .line 2700
    :cond_5
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onDestroy()V

    .line 2701
    return-void

    .line 2692
    :catch_0
    move-exception v0

    .line 2693
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 11
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v10, -0x2

    const/4 v9, 0x0

    const/4 v8, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1126
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    .line 1240
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v5

    :goto_0
    return v5

    .line 1129
    :sswitch_0
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->getStorageUsageIsShowing(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->isResumed()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1130
    new-instance v3, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;

    invoke-direct {v3}, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;-><init>()V

    .line 1131
    .local v3, "storageUsage":Landroid/app/DialogFragment;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const-string v7, "storage_usage"

    invoke-virtual {v3, v5, v7}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .end local v3    # "storageUsage":Landroid/app/DialogFragment;
    :cond_0
    move v5, v6

    .line 1133
    goto :goto_0

    .line 1136
    :sswitch_1
    new-instance v1, Landroid/content/Intent;

    const-string v5, "com.sec.android.app.myfiles.PICK_SELECT_PATH"

    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1137
    .local v1, "intent":Landroid/content/Intent;
    const-string v5, "SELECTOR_SOURCE_TYPE"

    invoke-virtual {v1, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1138
    const-string v5, "from_add_shortcut"

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1142
    const/4 v5, 0x3

    :try_start_0
    invoke-virtual {p0, v1, v5}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move v5, v6

    .line 1148
    goto :goto_0

    .line 1143
    :catch_0
    move-exception v0

    .line 1145
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v5, "CategoryHomeFragment"

    const-string v7, "com.sec.android.app.myfiles.PICK_SELECT_PATH not found - item select - add shortcut"

    invoke-static {v8, v5, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v1    # "intent":Landroid/content/Intent;
    :sswitch_2
    move v5, v6

    .line 1156
    goto :goto_0

    .line 1159
    :sswitch_3
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->isKNOXMode(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1160
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const-string v6, "knox_feature_disabled_toast"

    const-string v8, "string"

    const-string v9, "android"

    invoke-virtual {v5, v6, v8, v9}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 1161
    .local v4, "stringResId":I
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    move v5, v7

    .line 1162
    goto :goto_0

    .line 1164
    .end local v4    # "stringResId":I
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v7, "NEAR"

    invoke-static {v5, v7, v9}, Lcom/sec/android/app/myfiles/utils/Utils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1165
    sput-object v9, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    .line 1166
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDevicesCollectionUpdatedListener:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;

    invoke-static {v5, v7}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->scanForNearbyDevices(Landroid/app/Activity;Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;)V

    move v5, v6

    .line 1167
    goto/16 :goto_0

    .line 1172
    :sswitch_4
    sget-object v5, Lcom/sec/android/app/myfiles/ftp/FTPType;->FTP:Lcom/sec/android/app/myfiles/ftp/FTPType;

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/4 v8, -0x1

    invoke-static {v5, v7, v8}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->runAddFTP(Lcom/sec/android/app/myfiles/ftp/FTPType;Landroid/app/Activity;I)V

    move v5, v6

    .line 1174
    goto/16 :goto_0

    .line 1182
    :sswitch_5
    const/4 v5, 0x2

    :try_start_1
    invoke-virtual {p0, v5}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->showDialog(I)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    move v5, v6

    .line 1194
    goto/16 :goto_0

    .line 1184
    :catch_1
    move-exception v0

    .line 1186
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-static {v9}, Lcom/sec/android/app/myfiles/utils/Utils;->getOtherConfirmDeleteInstance(Landroid/app/Dialog;)Landroid/app/Dialog;

    move-result-object v2

    .line 1187
    .local v2, "otherInstance":Landroid/app/Dialog;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/app/Dialog;->isShowing()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1188
    invoke-virtual {v2}, Landroid/app/Dialog;->dismiss()V

    .line 1191
    :cond_2
    invoke-virtual {p0, v8}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->showDialog(I)V

    goto :goto_2

    .line 1196
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    .end local v2    # "otherInstance":Landroid/app/Dialog;
    :sswitch_6
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->finishSelectMode()V

    move v5, v6

    .line 1197
    goto/16 :goto_0

    .line 1199
    :sswitch_7
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v5, v5, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v5, :cond_3

    .line 1200
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v5, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mIsSearchMode:Z

    if-eqz v5, :cond_3

    move v5, v6

    .line 1201
    goto/16 :goto_0

    .line 1205
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->isSelectMode()Z

    move-result v5

    if-nez v5, :cond_4

    .line 1206
    invoke-virtual {p0, v6, v10, v7}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->startSelectMode(III)V

    .line 1207
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v5, v7, v7}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->startSelectMode(II)V

    .line 1208
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    invoke-virtual {v5, v7, v7}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->startSelectMode(II)V

    .line 1209
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    invoke-virtual {v5, v7, v7}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->startSelectMode(II)V

    .line 1210
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mStorageListAdapter:Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;

    invoke-virtual {v5, v7, v7}, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->startSelectMode(II)V

    .line 1211
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mRemoteShareListAdapter:Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;

    invoke-virtual {v5, v7, v7}, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->startSelectMode(II)V

    .line 1212
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setAllItemCount()V

    :cond_4
    move v5, v6

    .line 1215
    goto/16 :goto_0

    .line 1217
    :sswitch_8
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v5, v5, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v5, :cond_5

    .line 1218
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v5, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mIsSearchMode:Z

    if-eqz v5, :cond_5

    move v5, v6

    .line 1219
    goto/16 :goto_0

    .line 1223
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->isSelectMode()Z

    move-result v5

    if-nez v5, :cond_6

    .line 1224
    const/4 v5, 0x5

    invoke-virtual {p0, v6, v10, v5}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->startSelectMode(III)V

    .line 1225
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v5, v7, v7}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->startSelectMode(II)V

    .line 1226
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    invoke-virtual {v5, v7, v7}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->startSelectMode(II)V

    .line 1227
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    invoke-virtual {v5, v7, v7}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->startSelectMode(II)V

    .line 1228
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mStorageListAdapter:Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;

    invoke-virtual {v5, v7, v7}, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->startSelectMode(II)V

    .line 1229
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mRemoteShareListAdapter:Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;

    invoke-virtual {v5, v7, v7}, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->startSelectMode(II)V

    .line 1230
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setAllItemCount()V

    :cond_6
    move v5, v6

    .line 1233
    goto/16 :goto_0

    .line 1237
    :sswitch_9
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v7, "com.vcast.mediamanager.ACTION_FILES"

    invoke-static {v5, v7}, Lcom/sec/android/app/myfiles/utils/VZCloudUtils;->launchVZCloud(Landroid/content/Context;Ljava/lang/String;)V

    move v5, v6

    .line 1238
    goto/16 :goto_0

    .line 1126
    :sswitch_data_0
    .sparse-switch
        0x7f0f0129 -> :sswitch_1
        0x7f0f0137 -> :sswitch_7
        0x7f0f0138 -> :sswitch_8
        0x7f0f0139 -> :sswitch_9
        0x7f0f013f -> :sswitch_2
        0x7f0f0140 -> :sswitch_4
        0x7f0f0141 -> :sswitch_3
        0x7f0f0142 -> :sswitch_0
        0x7f0f0143 -> :sswitch_6
        0x7f0f0144 -> :sswitch_5
    .end sparse-switch
.end method

.method public onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 835
    const-string v0, "CategoryHomeFragment"

    const-string v1, "onPause "

    invoke-static {v2, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 837
    iput-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mResumeSate:Z

    .line 839
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onPause()V

    .line 841
    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 23
    .param p1, "id"    # I
    .param p2, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 2010
    invoke-super/range {p0 .. p2}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 2012
    packed-switch p1, :pswitch_data_0

    .line 2199
    .end local p2    # "dialog":Landroid/app/Dialog;
    :goto_0
    :pswitch_0
    return-void

    .line 2016
    .restart local p2    # "dialog":Landroid/app/Dialog;
    :pswitch_1
    invoke-static/range {p2 .. p2}, Lcom/sec/android/app/myfiles/utils/Utils;->getOtherConfirmDeleteInstance(Landroid/app/Dialog;)Landroid/app/Dialog;

    move-result-object v9

    .line 2017
    .local v9, "otherInstance":Landroid/app/Dialog;
    if-eqz v9, :cond_0

    move-object/from16 v0, p2

    if-eq v9, v0, :cond_0

    .line 2018
    invoke-virtual {v9}, Landroid/app/Dialog;->dismiss()V

    .line 2021
    :cond_0
    const/4 v10, 0x0

    .line 2023
    .local v10, "selectItem":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->getSelectedItemCount()I

    move-result v16

    .line 2024
    .local v16, "selectedShocrtcutItemCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->getSelectedItemCount()I

    move-result v12

    .line 2025
    .local v12, "selectedCloudItemCount":I
    add-int v14, v16, v12

    .line 2027
    .local v14, "selectedItemCount":I
    const/4 v13, 0x0

    .line 2028
    .local v13, "selectedFtpItemCount":I
    const/4 v15, 0x0

    .line 2030
    .local v15, "selectedNearByDeviceCount":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    if-ge v6, v12, :cond_2

    .line 2032
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "selectItem":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    check-cast v10, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 2034
    .restart local v10    # "selectItem":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v18

    const/16 v19, 0x9

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_1

    .line 2036
    add-int/lit8 v13, v13, 0x1

    .line 2030
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 2039
    :cond_1
    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    .line 2043
    :cond_2
    const-string v11, ""

    .line 2045
    .local v11, "selectText":Ljava/lang/String;
    if-lez v16, :cond_4

    if-nez v12, :cond_4

    .line 2047
    const/16 v18, 0x1

    move/from16 v0, v16

    move/from16 v1, v18

    if-ne v0, v1, :cond_3

    .line 2049
    const v18, 0x7f0b00e6

    move-object/from16 v0, p2

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    .line 2055
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0c0018

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    move-object/from16 v0, v18

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v14, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 2057
    const/16 v18, 0x1a

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSelectedShortcutType:I

    .line 2085
    :goto_4
    check-cast p2, Landroid/app/AlertDialog;

    .end local p2    # "dialog":Landroid/app/Dialog;
    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2052
    .restart local p2    # "dialog":Landroid/app/Dialog;
    :cond_3
    const v18, 0x7f0b005e

    move-object/from16 v0, p2

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    goto :goto_3

    .line 2059
    :cond_4
    if-nez v16, :cond_5

    if-ne v13, v14, :cond_5

    .line 2061
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0c0016

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    move-object/from16 v0, v18

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v14, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 2063
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0c0019

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    move-object/from16 v0, v18

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v14, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 2065
    const/16 v18, 0xa

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSelectedShortcutType:I

    goto :goto_4

    .line 2067
    :cond_5
    if-nez v16, :cond_6

    if-ne v15, v14, :cond_6

    .line 2069
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0c0017

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    move-object/from16 v0, v18

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v14, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 2071
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0c001a

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    move-object/from16 v0, v18

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v14, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 2073
    const/16 v18, 0x9

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSelectedShortcutType:I

    goto/16 :goto_4

    .line 2077
    :cond_6
    const v18, 0x7f0b00f2

    move-object/from16 v0, p2

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    .line 2079
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0b00f1

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 2081
    const/16 v18, 0x23

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSelectedShortcutType:I

    goto/16 :goto_4

    .line 2093
    .end local v6    # "i":I
    .end local v9    # "otherInstance":Landroid/app/Dialog;
    .end local v10    # "selectItem":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    .end local v11    # "selectText":Ljava/lang/String;
    .end local v12    # "selectedCloudItemCount":I
    .end local v13    # "selectedFtpItemCount":I
    .end local v14    # "selectedItemCount":I
    .end local v15    # "selectedNearByDeviceCount":I
    .end local v16    # "selectedShocrtcutItemCount":I
    :pswitch_2
    move/from16 v5, p1

    .line 2095
    .local v5, "dialogId":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    .line 2097
    .local v7, "inflater":Landroid/view/LayoutInflater;
    const v18, 0x7f040032

    const/16 v19, 0x0

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v7, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 2099
    .local v4, "customView":Landroid/view/View;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isChinaModel()Z

    move-result v18

    if-eqz v18, :cond_7

    .line 2100
    const v18, 0x7f0f00c5

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 2101
    .local v8, "mobileDataWarningDialog":Landroid/widget/TextView;
    const v18, 0x7f0b00e0

    move/from16 v0, v18

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(I)V

    .line 2103
    .end local v8    # "mobileDataWarningDialog":Landroid/widget/TextView;
    :cond_7
    const v18, 0x7f0f00c7

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    .line 2105
    .local v3, "checkDoNotShow":Landroid/widget/CheckBox;
    const v18, 0x7f0f00a8

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    .line 2119
    .local v17, "textDoNotShow":Landroid/widget/TextView;
    new-instance v18, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$21;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v5, v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$21;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;ILandroid/widget/CheckBox;)V

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2141
    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setSoundEffectsEnabled(Z)V

    .line 2142
    new-instance v18, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$22;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$22;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;Landroid/widget/CheckBox;)V

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2151
    check-cast p2, Landroid/app/AlertDialog;

    .end local p2    # "dialog":Landroid/app/Dialog;
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 2162
    .end local v3    # "checkDoNotShow":Landroid/widget/CheckBox;
    .end local v4    # "customView":Landroid/view/View;
    .end local v5    # "dialogId":I
    .end local v7    # "inflater":Landroid/view/LayoutInflater;
    .end local v17    # "textDoNotShow":Landroid/widget/TextView;
    .restart local p2    # "dialog":Landroid/app/Dialog;
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    .line 2163
    .restart local v7    # "inflater":Landroid/view/LayoutInflater;
    const v18, 0x7f040033

    const/16 v19, 0x0

    move/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v7, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 2164
    .restart local v4    # "customView":Landroid/view/View;
    const v18, 0x7f0f00c9

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    .line 2165
    .restart local v3    # "checkDoNotShow":Landroid/widget/CheckBox;
    const v18, 0x7f0f00a8

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    .line 2167
    .restart local v17    # "textDoNotShow":Landroid/widget/TextView;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isChinaModel()Z

    move-result v18

    if-eqz v18, :cond_8

    .line 2168
    const v18, 0x7f0f00c8

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 2169
    .restart local v8    # "mobileDataWarningDialog":Landroid/widget/TextView;
    const v18, 0x7f0b0087

    move/from16 v0, v18

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(I)V

    .line 2171
    .end local v8    # "mobileDataWarningDialog":Landroid/widget/TextView;
    :cond_8
    new-instance v18, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$23;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$23;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;Landroid/widget/CheckBox;)V

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2184
    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setSoundEffectsEnabled(Z)V

    .line 2185
    new-instance v18, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$24;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$24;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;Landroid/widget/CheckBox;)V

    invoke-virtual/range {v17 .. v18}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2193
    check-cast p2, Landroid/app/AlertDialog;

    .end local p2    # "dialog":Landroid/app/Dialog;
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 2012
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 8
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v7, 0x7f0f0138

    const v6, 0x7f0f0137

    const v5, 0x7f0f0129

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 974
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 977
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-nez v1, :cond_0

    .line 1121
    :goto_0
    return-void

    .line 980
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setActionBarIcon(I)V

    .line 982
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 984
    const v1, 0x7f0f0141

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 985
    const v1, 0x7f0f0141

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 989
    :cond_1
    const v1, 0x7f0f0140

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 990
    const v1, 0x7f0f0140

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 994
    :cond_2
    const v1, 0x7f0f013d

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 995
    const v1, 0x7f0f013d

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 998
    :cond_3
    const v1, 0x7f0f013f

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 999
    const v1, 0x7f0f013f

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1002
    :cond_4
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 1003
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1102
    :cond_5
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1103
    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 1104
    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1106
    :cond_6
    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 1107
    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1120
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v2, 0x7f0f0139

    invoke-static {v1, p1, v2}, Lcom/sec/android/app/myfiles/utils/VZCloudUtils;->prepareOptionsMenu(Landroid/content/Context;Landroid/view/Menu;I)V

    goto/16 :goto_0

    .line 1019
    :cond_8
    const v1, 0x7f0f013f

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 1020
    const v1, 0x7f0f013f

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1022
    :cond_9
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->isSelectMode()Z

    move-result v1

    if-nez v1, :cond_d

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v1, :cond_d

    .line 1031
    const v1, 0x7f0f0140

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_a

    .line 1033
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isKNOXMode(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 1035
    const v1, 0x7f0f0140

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1045
    :cond_a
    :goto_2
    const v1, 0x7f0f0141

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_b

    .line 1047
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isKNOXMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 1049
    const v1, 0x7f0f0141

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1060
    :cond_b
    :goto_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->getSelectableItemsCount()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->getCount()I

    move-result v2

    add-int v0, v1, v2

    .line 1062
    .local v0, "selectableItemsCount":I
    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_c

    .line 1064
    if-lez v0, :cond_10

    .line 1066
    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1074
    :cond_c
    :goto_4
    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_d

    .line 1076
    if-lez v0, :cond_11

    .line 1078
    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1094
    .end local v0    # "selectableItemsCount":I
    :cond_d
    :goto_5
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 1095
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-static {v1, v4}, Lcom/sec/android/app/myfiles/utils/Utils;->getShortCutCount(Landroid/content/Context;Z)I

    move-result v1

    const/16 v2, 0x32

    if-ne v1, v2, :cond_12

    .line 1096
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_1

    .line 1039
    :cond_e
    const v1, 0x7f0f0140

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_2

    .line 1055
    :cond_f
    const v1, 0x7f0f0141

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_3

    .line 1069
    .restart local v0    # "selectableItemsCount":I
    :cond_10
    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_4

    .line 1082
    :cond_11
    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_5

    .line 1098
    .end local v0    # "selectableItemsCount":I
    :cond_12
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_1
.end method

.method public onRefresh()V
    .locals 1

    .prologue
    .line 932
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    if-eqz v0, :cond_0

    .line 934
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->onRefresh()V

    .line 938
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v0, :cond_1

    .line 940
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->updateCategorySize()V

    .line 943
    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 847
    const-string v1, "CategoryHomeFragment"

    const-string v2, "onResume "

    invoke-static {v3, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 849
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mResumeSate:Z

    .line 852
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onResume()V

    .line 854
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSearchView:Landroid/widget/SearchView;

    if-eqz v1, :cond_0

    .line 856
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->requestFocus()Z

    .line 859
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mIsEasyMode:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mIsEasyMode:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 860
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getCreateAdapter(I)Landroid/widget/BaseAdapter;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    .line 861
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView(Landroid/os/Bundle;)V

    .line 924
    :goto_0
    return-void

    .line 871
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->onRefresh()V

    .line 873
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    if-eqz v1, :cond_4

    .line 875
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->isSelectMode()Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v1, v1, Lcom/sec/android/app/myfiles/MainActivity;->mIsSearchMode:Z

    if-nez v1, :cond_6

    .line 877
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->finishSelectMode()V

    .line 889
    :cond_4
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 890
    .local v0, "arg":Landroid/os/Bundle;
    if-eqz v0, :cond_5

    .line 891
    const-string v1, "add_ftp"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 892
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mScrollView:Landroid/widget/ScrollView;

    new-instance v2, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$9;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$9;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 902
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->updateCategorySize()V

    goto :goto_0

    .line 879
    .end local v0    # "arg":Landroid/os/Bundle;
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->isSelectMode()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 885
    const/4 v1, -0x1

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getSelectModeFrom()I

    move-result v2

    invoke-virtual {p0, v4, v1, v2}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->startSelectMode(III)V

    goto :goto_1
.end method

.method public refreshShortcutHeader()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 3379
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutsHeaderContainer:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutsListContainer:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 3380
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/MainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 3382
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutsHeaderContainer:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 3383
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutsListContainer:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 3392
    :cond_0
    :goto_0
    return-void

    .line 3387
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutsHeaderContainer:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 3388
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutsListContainer:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public remoteShareClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 3329
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$30;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$30;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    return-object v0
.end method

.method public resetNearByDevicesList()V
    .locals 1

    .prologue
    .line 2442
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->disConnect()V

    .line 2443
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    .line 2445
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 2446
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->clear()V

    .line 2448
    :cond_0
    invoke-static {}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->getInstance()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->unRegisterDevice()V

    .line 2450
    return-void
.end method

.method protected selectAllItem()V
    .locals 3

    .prologue
    .line 2727
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v0, :cond_0

    .line 2729
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/MainActivity;->setSelectOptionsVisibility(ZZ)V

    .line 2732
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->dismissPopupMenu()V

    .line 2734
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2737
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->selectAllItem()V

    .line 2738
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->selectAllItem()V

    .line 2740
    :cond_1
    return-void
.end method

.method public selectItemControl(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 2719
    if-nez p1, :cond_0

    .line 2720
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->selectAllItem()V

    .line 2724
    :goto_0
    return-void

    .line 2722
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->unselectAllItem()V

    goto :goto_0
.end method

.method public sendShortcutAdapterRefreshBroadCast()V
    .locals 2

    .prologue
    .line 1506
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1508
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.action.SHORTCUT_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1510
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1511
    return-void
.end method

.method public setAllItemCount()V
    .locals 15

    .prologue
    const v14, 0x7f0b0016

    const/4 v13, 0x6

    const/4 v12, 0x5

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 2772
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b0038

    new-array v7, v11, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->getSelectedItemCount()I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->getSelectedItemCount()I

    move-result v9

    add-int/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2777
    .local v4, "msg":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    if-nez v5, :cond_1

    .line 2871
    :cond_0
    :goto_0
    return-void

    .line 2782
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllButton:Landroid/widget/TextView;

    if-eqz v5, :cond_2

    .line 2783
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllButton:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2784
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v5, v4}, Lcom/sec/android/app/myfiles/MainActivity;->setSpinnerWidth(Ljava/lang/String;)V

    .line 2788
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->getSelectedItemCount()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->getSelectedItemCount()I

    move-result v6

    add-int/2addr v5, v6

    if-nez v5, :cond_a

    .line 2789
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v5, v11, v10}, Lcom/sec/android/app/myfiles/MainActivity;->setSelectOptionsVisibility(ZZ)V

    .line 2790
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    if-eqz v5, :cond_3

    .line 2791
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    invoke-virtual {v5, v10}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 2793
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeRename:Landroid/view/MenuItem;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeFtpEdit:Landroid/view/MenuItem;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    if-eqz v5, :cond_5

    .line 2794
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeRename:Landroid/view/MenuItem;

    invoke-interface {v5, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2795
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeFtpEdit:Landroid/view/MenuItem;

    invoke-interface {v5, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2796
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getSelectModeFrom()I

    move-result v5

    if-eq v5, v12, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getSelectModeFrom()I

    move-result v5

    if-ne v5, v13, :cond_9

    .line 2797
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 2798
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    invoke-interface {v5, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2799
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    invoke-interface {v5, v14}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 2813
    :cond_5
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->getSelectedItemCount()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->getSelectedItemCount()I

    move-result v6

    add-int/2addr v5, v6

    if-ne v5, v11, :cond_12

    .line 2814
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    if-eqz v5, :cond_7

    .line 2815
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getSelectModeFrom()I

    move-result v5

    if-eq v5, v12, :cond_6

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getSelectModeFrom()I

    move-result v5

    if-ne v5, v13, :cond_c

    .line 2816
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 2817
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    invoke-interface {v5, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2818
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    invoke-interface {v5, v11}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 2819
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    invoke-interface {v5, v14}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 2824
    :cond_7
    :goto_2
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v2

    .line 2825
    .local v2, "mShortcutList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v0

    .line 2829
    .local v0, "mCloudList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->getSelectedItemCount()I

    move-result v5

    if-ne v5, v11, :cond_d

    .line 2830
    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 2831
    .local v1, "mItem":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v3

    .line 2837
    .local v3, "mShortcutType":I
    :goto_3
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeRename:Landroid/view/MenuItem;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeFtpEdit:Landroid/view/MenuItem;

    if-eqz v5, :cond_0

    .line 2838
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getSelectModeFrom()I

    move-result v5

    if-eq v5, v12, :cond_11

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getSelectModeFrom()I

    move-result v5

    if-eq v5, v13, :cond_11

    .line 2839
    const/16 v5, 0x1a

    if-eq v3, v5, :cond_8

    const/16 v5, 0x8

    if-ne v3, v5, :cond_e

    .line 2840
    :cond_8
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeRename:Landroid/view/MenuItem;

    invoke-interface {v5, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2841
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeFtpEdit:Landroid/view/MenuItem;

    invoke-interface {v5, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 2801
    .end local v0    # "mCloudList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v1    # "mItem":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    .end local v2    # "mShortcutList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v3    # "mShortcutType":I
    :cond_9
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    invoke-interface {v5, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_1

    .line 2804
    :cond_a
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->getSelectedItemCount()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->getSelectedItemCount()I

    move-result v6

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v6, v10}, Lcom/sec/android/app/myfiles/utils/Utils;->getShortCutCount(Landroid/content/Context;Z)I

    move-result v6

    if-ne v5, v6, :cond_b

    .line 2806
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v5, v10, v11}, Lcom/sec/android/app/myfiles/MainActivity;->setSelectOptionsVisibility(ZZ)V

    .line 2807
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    invoke-virtual {v5, v11}, Landroid/widget/ImageView;->setSelected(Z)V

    goto/16 :goto_1

    .line 2810
    :cond_b
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v5, v11, v11}, Lcom/sec/android/app/myfiles/MainActivity;->setSelectOptionsVisibility(ZZ)V

    .line 2811
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    invoke-virtual {v5, v10}, Landroid/widget/ImageView;->setSelected(Z)V

    goto/16 :goto_1

    .line 2821
    :cond_c
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    invoke-interface {v5, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_2

    .line 2833
    .restart local v0    # "mCloudList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .restart local v2    # "mShortcutList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_d
    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 2834
    .restart local v1    # "mItem":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v3

    .restart local v3    # "mShortcutType":I
    goto :goto_3

    .line 2842
    :cond_e
    const/16 v5, 0x1c

    if-eq v3, v5, :cond_f

    const/16 v5, 0xa

    if-ne v3, v5, :cond_10

    .line 2843
    :cond_f
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeRename:Landroid/view/MenuItem;

    invoke-interface {v5, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2844
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeFtpEdit:Landroid/view/MenuItem;

    invoke-interface {v5, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 2846
    :cond_10
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeRename:Landroid/view/MenuItem;

    invoke-interface {v5, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2847
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeFtpEdit:Landroid/view/MenuItem;

    invoke-interface {v5, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 2850
    :cond_11
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeRename:Landroid/view/MenuItem;

    invoke-interface {v5, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2851
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeFtpEdit:Landroid/view/MenuItem;

    invoke-interface {v5, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 2855
    .end local v0    # "mCloudList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v1    # "mItem":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    .end local v2    # "mShortcutList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v3    # "mShortcutType":I
    :cond_12
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeRename:Landroid/view/MenuItem;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeFtpEdit:Landroid/view/MenuItem;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    if-eqz v5, :cond_0

    .line 2857
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->getSelectedItemCount()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->getSelectedItemCount()I

    move-result v6

    add-int/2addr v5, v6

    if-eqz v5, :cond_14

    .line 2858
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getSelectModeFrom()I

    move-result v5

    if-eq v5, v12, :cond_13

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getSelectModeFrom()I

    move-result v5

    if-ne v5, v13, :cond_15

    .line 2859
    :cond_13
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 2860
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    invoke-interface {v5, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2861
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    invoke-interface {v5, v11}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 2862
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    invoke-interface {v5, v14}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 2867
    :cond_14
    :goto_4
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeRename:Landroid/view/MenuItem;

    invoke-interface {v5, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2868
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeFtpEdit:Landroid/view/MenuItem;

    invoke-interface {v5, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 2864
    :cond_15
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    invoke-interface {v5, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_4
.end method

.method public setDropBoxFromShortcut(Z)V
    .locals 0
    .param p1, "isShortcut"    # Z

    .prologue
    .line 2003
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->bDropBoxFromShortcut:Z

    .line 2004
    return-void
.end method

.method public setSelectModeActionBar()V
    .locals 2

    .prologue
    .line 963
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getSelectModeFrom()I

    move-result v0

    if-nez v0, :cond_0

    .line 965
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActionBarManager:Lcom/sec/android/app/myfiles/view/ActionBarManager;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/ActionBarManager;->setActionBar(I)V

    .line 969
    :cond_0
    return-void
.end method

.method public setStartBrowserState(Z)V
    .locals 0
    .param p1, "mode"    # Z

    .prologue
    .line 1280
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mStartBrowserState:Z

    .line 1281
    return-void
.end method

.method public showDataWarningDialog(I)V
    .locals 0
    .param p1, "warningDialogId"    # I

    .prologue
    .line 1251
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->showDialog(I)V

    .line 1252
    return-void
.end method

.method public showDataWarningDialog(ILandroid/content/Context;Lcom/sec/android/app/myfiles/element/ShortcutItem;)V
    .locals 1
    .param p1, "warningDialogId"    # I
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "item"    # Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .prologue
    .line 1256
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    .line 1257
    check-cast p2, Lcom/sec/android/app/myfiles/AbsMainActivity;

    .end local p2    # "context":Landroid/content/Context;
    iput-object p2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    .line 1259
    :cond_0
    if-eqz p3, :cond_1

    .line 1260
    iput-object p3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutItemForWarning:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 1262
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->showDialog(I)V

    .line 1263
    return-void
.end method

.method public showDeleteDailog()V
    .locals 1

    .prologue
    .line 1246
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->showDialog(I)V

    .line 1247
    return-void
.end method

.method public startDragMode(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "arg"    # Landroid/os/Bundle;

    .prologue
    .line 2651
    return-void
.end method

.method public startSelectMode(III)V
    .locals 2
    .param p1, "selectMode"    # I
    .param p2, "initialSelectedPosition"    # I
    .param p3, "from"    # I

    .prologue
    .line 3370
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->startSelectMode(III)V

    .line 3371
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3372
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->downloadHistory:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 3373
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->downloadHistoryIcon:Landroid/widget/ImageView;

    const v1, 0x3ecccccd    # 0.4f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 3374
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->downloadHistoryTitle:Landroid/widget/TextView;

    const v1, -0x777778

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 3376
    :cond_0
    return-void
.end method

.method protected unselectAllItem()V
    .locals 3

    .prologue
    .line 2745
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v0, :cond_0

    .line 2747
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/MainActivity;->setSelectOptionsVisibility(ZZ)V

    .line 2750
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->dismissPopupMenu()V

    .line 2752
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2755
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->unselectAllItem()V

    .line 2756
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->unselectAllItem()V

    .line 2758
    :cond_1
    return-void
.end method
