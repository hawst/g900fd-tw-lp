.class public Lcom/sec/android/app/myfiles/ftp/DownloadItem;
.super Ljava/lang/Object;
.source "DownloadItem.java"


# instance fields
.field private final mFtpItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

.field private final mTarget:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/myfiles/ftp/IFTPItem;Ljava/lang/String;)V
    .locals 0
    .param p1, "item"    # Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .param p2, "target"    # Ljava/lang/String;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p2, p0, Lcom/sec/android/app/myfiles/ftp/DownloadItem;->mTarget:Ljava/lang/String;

    .line 22
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ftp/DownloadItem;->mFtpItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .line 23
    return-void
.end method


# virtual methods
.method public getFtpItem()Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/DownloadItem;->mFtpItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    return-object v0
.end method

.method public getTarget()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/DownloadItem;->mTarget:Ljava/lang/String;

    return-object v0
.end method
