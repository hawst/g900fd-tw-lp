.class public final Lcom/sec/android/app/myfiles/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final FTP:I = 0x7f0b00a4

.field public static final FTPS:I = 0x7f0b00a6

.field public static final FTP_connected:I = 0x7f0b00e3

.field public static final SFTP:I = 0x7f0b00a7

.field public static final add_ftp:I = 0x7f0b00a3

.field public static final add_ftp_server:I = 0x7f0b011b

.field public static final add_shortcut:I = 0x7f0b005b

.field public static final add_shortcut_on:I = 0x7f0b005c

.field public static final add_shortcut_on_home_screen:I = 0x7f0b005d

.field public static final add_shortcut_to_home:I = 0x7f0b0180

.field public static final add_to_home_screen:I = 0x7f0b0130

.field public static final add_to_myfiles:I = 0x7f0b012f

.field public static final added_mode:I = 0x7f0b00a8

.field public static final advanced_search:I = 0x7f0b0145

.field public static final all_file_types:I = 0x7f0b0002

.field public static final all_files:I = 0x7f0b0001

.field public static final already_exist_file_copy:I = 0x7f0b0129

.field public static final already_exist_file_move:I = 0x7f0b0128

.field public static final already_exists:I = 0x7f0b0076

.field public static final app_name:I = 0x7f0b0000

.field public static final app_size:I = 0x7f0b0098

.field public static final applications_will_be_uninstalled:I = 0x7f0b0138

.field public static final apply_to_all_items:I = 0x7f0b00fe

.field public static final as_image_file:I = 0x7f0b00da

.field public static final as_video_file:I = 0x7f0b00d9

.field public static final ascending:I = 0x7f0b0039

.field public static final audio:I = 0x7f0b0147

.field public static final baidu:I = 0x7f0b00fa

.field public static final button:I = 0x7f0b0018

.field public static final bytes:I = 0x7f0b006f

.field public static final cache_size:I = 0x7f0b009a

.field public static final calculating:I = 0x7f0b006c

.field public static final cancel:I = 0x7f0b0017

.field public static final cancelled_to_move:I = 0x7f0b00cc

.field public static final cannot_extract_already_exist:I = 0x7f0b00ed

.field public static final cannot_open_zip_file:I = 0x7f0b0102

.field public static final change_pin:I = 0x7f0b010d

.field public static final change_pin_summary:I = 0x7f0b010e

.field public static final clear_history:I = 0x7f0b0169

.field public static final cloud_action:I = 0x7f0b00a5

.field public static final cloud_storage:I = 0x7f0b011a

.field public static final cloud_storage_header:I = 0x7f0b0121

.field public static final collapse_list:I = 0x7f0b00fd

.field public static final confirm_oma_description:I = 0x7f0b0175

.field public static final confirm_oma_name:I = 0x7f0b0170

.field public static final confirm_oma_negative:I = 0x7f0b016f

.field public static final confirm_oma_positive:I = 0x7f0b016e

.field public static final confirm_oma_size:I = 0x7f0b0172

.field public static final confirm_oma_title:I = 0x7f0b016d

.field public static final confirm_oma_type:I = 0x7f0b0174

.field public static final confirm_oma_vendor:I = 0x7f0b0171

.field public static final confirm_oma_version:I = 0x7f0b0173

.field public static final connect:I = 0x7f0b017a

.field public static final connect_to_mobile_network:I = 0x7f0b0125

.field public static final contains:I = 0x7f0b00ab

.field public static final content:I = 0x7f0b00e5

.field public static final copy:I = 0x7f0b0022

.field public static final copying:I = 0x7f0b00c8

.field public static final create:I = 0x7f0b016a

.field public static final create_folder:I = 0x7f0b0019

.field public static final create_zip_file:I = 0x7f0b0073

.field public static final creation_failed:I = 0x7f0b009e

.field public static final data_size:I = 0x7f0b0099

.field public static final delete:I = 0x7f0b0020

.field public static final delete_items:I = 0x7f0b00f2

.field public static final delete_selected_item:I = 0x7f0b003c

.field public static final delete_shortcut:I = 0x7f0b00e6

.field public static final delete_shortcuts:I = 0x7f0b005e

.field public static final deleting:I = 0x7f0b00cd

.field public static final descending:I = 0x7f0b003a

.field public static final deselect_all:I = 0x7f0b0037

.field public static final destination_folder_is_same_as_source_folder:I = 0x7f0b0069

.field public static final destination_folder_is_subfolder_of_source_folder:I = 0x7f0b006a

.field public static final detailed_list:I = 0x7f0b0126

.field public static final details:I = 0x7f0b0025

.field public static final details_path:I = 0x7f0b00e7

.field public static final device_storage:I = 0x7f0b003e

.field public static final discovering:I = 0x7f0b00b0

.field public static final do_not_show_again:I = 0x7f0b00e2

.field public static final documents:I = 0x7f0b0006

.field public static final done:I = 0x7f0b0016

.field public static final download:I = 0x7f0b016b

.field public static final download_all:I = 0x7f0b016c

.field public static final download_history:I = 0x7f0b014d

.field public static final download_history_header:I = 0x7f0b011d

.field public static final download_large_files:I = 0x7f0b000b

.field public static final download_manager_is_diabled:I = 0x7f0b017d

.field public static final download_settings:I = 0x7f0b017b

.field public static final download_using_wifi_only:I = 0x7f0b000c

.field public static final download_using_wifi_only_msg:I = 0x7f0b000d

.field public static final downloaded_apps:I = 0x7f0b0007

.field public static final downloading:I = 0x7f0b00d2

.field public static final downloads:I = 0x7f0b0148

.field public static final dropbox:I = 0x7f0b0008

.field public static final edit:I = 0x7f0b00d7

.field public static final empty:I = 0x7f0b003b

.field public static final enter_name:I = 0x7f0b009d

.field public static final enter_pin_lock:I = 0x7f0b0109

.field public static final exceed_select_file_number:I = 0x7f0b0066

.field public static final exist_file_copy:I = 0x7f0b00ce

.field public static final exist_file_extract:I = 0x7f0b00d0

.field public static final exist_file_move:I = 0x7f0b00cf

.field public static final exist_file_zip:I = 0x7f0b00d1

.field public static final expand_list:I = 0x7f0b00fc

.field public static final expired:I = 0x7f0b0011

.field public static final extract:I = 0x7f0b0028

.field public static final extract_here:I = 0x7f0b005f

.field public static final extract_name:I = 0x7f0b0074

.field public static final failed_to_add_shortcut:I = 0x7f0b0062

.field public static final failed_to_copy:I = 0x7f0b00c9

.field public static final failed_to_delete:I = 0x7f0b009f

.field public static final failed_to_delete_shortcut:I = 0x7f0b0063

.field public static final failed_to_download:I = 0x7f0b00ad

.field public static final failed_to_move:I = 0x7f0b00cb

.field public static final failed_to_remove_device:I = 0x7f0b00f7

.field public static final failed_to_remove_ftp_server:I = 0x7f0b00f8

.field public static final failed_to_rename:I = 0x7f0b00a0

.field public static final fetching:I = 0x7f0b00ac

.field public static final file_does_not_exist:I = 0x7f0b00ef

.field public static final file_lock:I = 0x7f0b0105

.field public static final file_name_already:I = 0x7f0b012a

.field public static final file_name_in_use:I = 0x7f0b0127

.field public static final file_name_maximum_length:I = 0x7f0b0176

.field public static final file_stored_in_default_dir:I = 0x7f0b00ae

.field public static final file_transfer_cancelled:I = 0x7f0b0177

.field public static final folder_does_not_exist:I = 0x7f0b00f0

.field public static final ftp_active:I = 0x7f0b00ba

.field public static final ftp_anonymous:I = 0x7f0b00b7

.field public static final ftp_conection_problem:I = 0x7f0b00c7

.field public static final ftp_default_port:I = 0x7f0b00c4

.field public static final ftp_encoding:I = 0x7f0b00b8

.field public static final ftp_encoding_auto:I = 0x7f0b00bc

.field public static final ftp_encoding_big5:I = 0x7f0b00bd

.field public static final ftp_encoding_big5hkscs:I = 0x7f0b00be

.field public static final ftp_encoding_cesu8:I = 0x7f0b00c0

.field public static final ftp_encoding_docu1:I = 0x7f0b00bf

.field public static final ftp_encoding_utf8:I = 0x7f0b00c1

.field public static final ftp_encryption:I = 0x7f0b00b9

.field public static final ftp_encryption_explicit:I = 0x7f0b00c3

.field public static final ftp_encryption_implicit:I = 0x7f0b00c2

.field public static final ftp_mode:I = 0x7f0b00b4

.field public static final ftp_passive:I = 0x7f0b00bb

.field public static final ftp_password:I = 0x7f0b00b6

.field public static final ftp_port:I = 0x7f0b00b3

.field public static final ftp_server:I = 0x7f0b00b2

.field public static final ftp_sessionname:I = 0x7f0b00b1

.field public static final ftp_username:I = 0x7f0b00b5

.field public static final ftps_default_port:I = 0x7f0b00c5

.field public static final galaxy_gear:I = 0x7f0b0104

.field public static final giga_byte:I = 0x7f0b0141

.field public static final giga_bytes:I = 0x7f0b0142

.field public static final grid:I = 0x7f0b0165

.field public static final history_dialog_content:I = 0x7f0b0167

.field public static final history_download_unavailable:I = 0x7f0b0166

.field public static final history_retry:I = 0x7f0b0168

.field public static final home:I = 0x7f0b0026

.field public static final images:I = 0x7f0b0003

.field public static final in_order:I = 0x7f0b003d

.field public static final invalid_character:I = 0x7f0b00a2

.field public static final item_deleted:I = 0x7f0b00d8

.field public static final item_selected:I = 0x7f0b0038

.field public static final items:I = 0x7f0b0070

.field public static final items_deleted:I = 0x7f0b00f6

.field public static final items_will_be_deleted:I = 0x7f0b00f1

.field public static final kilo_byte:I = 0x7f0b013d

.field public static final kilo_bytes:I = 0x7f0b013e

.field public static final last_modified:I = 0x7f0b006e

.field public static final last_modified_time:I = 0x7f0b006d

.field public static final list:I = 0x7f0b002c

.field public static final list_and_details:I = 0x7f0b002d

.field public static final list_update:I = 0x7f0b009b

.field public static final list_view:I = 0x7f0b0030

.field public static final local_storage:I = 0x7f0b012b

.field public static final local_storage_header:I = 0x7f0b011f

.field public static final lock:I = 0x7f0b0106

.field public static final lock_files:I = 0x7f0b0108

.field public static final lock_notification:I = 0x7f0b010a

.field public static final locking_big_file:I = 0x7f0b0136

.field public static final locking_file:I = 0x7f0b0134

.field public static final lockpassword_cancel_label:I = 0x7f0b010f

.field public static final lockpassword_choose_your_pin_header:I = 0x7f0b0113

.field public static final lockpassword_confirm_pins_dont_match:I = 0x7f0b0114

.field public static final lockpassword_confirm_your_pin_header:I = 0x7f0b0112

.field public static final lockpassword_continue_label:I = 0x7f0b0110

.field public static final lockpassword_pin_contains_non_digits:I = 0x7f0b0117

.field public static final lockpassword_pin_too_long:I = 0x7f0b0116

.field public static final lockpassword_pin_too_short:I = 0x7f0b0115

.field public static final lockpassword_press_continue:I = 0x7f0b0118

.field public static final max_char_reached_msg:I = 0x7f0b0077

.field public static final max_character:I = 0x7f0b00a1

.field public static final maximum_number_of_shortcuts:I = 0x7f0b00d5

.field public static final mega_byte:I = 0x7f0b013f

.field public static final mega_bytes:I = 0x7f0b0140

.field public static final mobile_data_connect_title:I = 0x7f0b00df

.field public static final mobile_data_disabled:I = 0x7f0b0133

.field public static final mobile_data_disabled_chn:I = 0x7f0b0132

.field public static final mobile_data_warning:I = 0x7f0b00e1

.field public static final mobile_data_warning_chn:I = 0x7f0b00e0

.field public static final mobile_data_warning_title:I = 0x7f0b00de

.field public static final move:I = 0x7f0b0021

.field public static final move_here:I = 0x7f0b0014

.field public static final move_to_KNOX:I = 0x7f0b002a

.field public static final move_to_multi:I = 0x7f0b014c

.field public static final move_to_private:I = 0x7f0b0163

.field public static final move_to_single:I = 0x7f0b014b

.field public static final moving:I = 0x7f0b00ca

.field public static final multiple_files:I = 0x7f0b00a9

.field public static final music:I = 0x7f0b0005

.field public static final name:I = 0x7f0b0033

.field public static final name_is_empty:I = 0x7f0b009c

.field public static final nearby_devices:I = 0x7f0b007c

.field public static final nearby_devices_attempt_to_connect:I = 0x7f0b0088

.field public static final nearby_devices_attempt_to_connect_chn:I = 0x7f0b0087

.field public static final nearby_devices_download_menu_option:I = 0x7f0b0082

.field public static final nearby_devices_end_of_items:I = 0x7f0b008a

.field public static final nearby_devices_found_msg:I = 0x7f0b007b

.field public static final nearby_devices_more_items:I = 0x7f0b0089

.field public static final nearby_devices_need_to_wifi:I = 0x7f0b0086

.field public static final nearby_devices_need_to_wifi_chn:I = 0x7f0b0085

.field public static final nearby_devices_no_files_selected:I = 0x7f0b0084

.field public static final nearby_devices_provider_removed:I = 0x7f0b0083

.field public static final new_folder:I = 0x7f0b001a

.field public static final no_albumart:I = 0x7f0b007d

.field public static final no_application:I = 0x7f0b00ee

.field public static final no_devices_found_msg:I = 0x7f0b007a

.field public static final no_downloads:I = 0x7f0b0149

.field public static final no_exist_directory:I = 0x7f0b00fb

.field public static final no_files:I = 0x7f0b005a

.field public static final no_play_during_call:I = 0x7f0b007e

.field public static final no_wifi_connection:I = 0x7f0b013a

.field public static final no_wifi_connection_chn:I = 0x7f0b0139

.field public static final not_enough_memory_msg:I = 0x7f0b0067

.field public static final not_enough_space_in_db:I = 0x7f0b0068

.field public static final not_enough_storage:I = 0x7f0b00f5

.field public static final not_enough_storage_device_storage:I = 0x7f0b00f4

.field public static final not_enough_storage_sd_card:I = 0x7f0b00f3

.field public static final not_signed_in:I = 0x7f0b014f

.field public static final ok:I = 0x7f0b0015

.field public static final open_file:I = 0x7f0b0064

.field public static final opening:I = 0x7f0b0065

.field public static final others:I = 0x7f0b004b

.field public static final overwrite:I = 0x7f0b0024

.field public static final passcode_lock:I = 0x7f0b0111

.field public static final paste_here:I = 0x7f0b0013

.field public static final permission_copy:I = 0x7f0b0058

.field public static final permission_display:I = 0x7f0b0055

.field public static final permission_execute:I = 0x7f0b0056

.field public static final permission_move:I = 0x7f0b0059

.field public static final permission_play:I = 0x7f0b0054

.field public static final permission_print:I = 0x7f0b0057

.field public static final personal_mode:I = 0x7f0b014a

.field public static final preparing_to_copy:I = 0x7f0b0123

.field public static final private_content:I = 0x7f0b0150

.field public static final private_storage:I = 0x7f0b0151

.field public static final proceed:I = 0x7f0b000e

.field public static final processing:I = 0x7f0b006b

.field public static final quick_search:I = 0x7f0b0119

.field public static final quick_search_header:I = 0x7f0b011c

.field public static final recent_files:I = 0x7f0b0012

.field public static final remote_share:I = 0x7f0b000f

.field public static final remote_share_downloaded:I = 0x7f0b0010

.field public static final remote_share_header:I = 0x7f0b0120

.field public static final remote_share_inbox:I = 0x7f0b0009

.field public static final remote_share_outbox:I = 0x7f0b000a

.field public static final remove_from_KNOX:I = 0x7f0b00ff

.field public static final remove_from_KNOX_1item:I = 0x7f0b0101

.field public static final remove_from_personal_folder_nitems:I = 0x7f0b0100

.field public static final remove_from_private:I = 0x7f0b0164

.field public static final rename:I = 0x7f0b0023

.field public static final replace:I = 0x7f0b0152

.field public static final right_forward_lock:I = 0x7f0b00e9

.field public static final right_status:I = 0x7f0b00e8

.field public static final root:I = 0x7f0b00d3

.field public static final scan_for_nearby_devices:I = 0x7f0b0078

.field public static final scan_for_nearby_devices_need_to_wifi:I = 0x7f0b0124

.field public static final scan_for_nearby_devices_started_msg:I = 0x7f0b0079

.field public static final sd_card:I = 0x7f0b003f

.field public static final sd_memory_card:I = 0x7f0b0040

.field public static final search:I = 0x7f0b001d

.field public static final search_advanced:I = 0x7f0b0090

.field public static final search_all:I = 0x7f0b008b

.field public static final search_date:I = 0x7f0b008e

.field public static final search_date_from:I = 0x7f0b0092

.field public static final search_date_hint:I = 0x7f0b0162

.field public static final search_date_to:I = 0x7f0b0093

.field public static final search_extention:I = 0x7f0b008f

.field public static final search_file_extention:I = 0x7f0b0097

.field public static final search_file_name:I = 0x7f0b0096

.field public static final search_file_type:I = 0x7f0b008d

.field public static final search_for:I = 0x7f0b0146

.field public static final search_location:I = 0x7f0b008c

.field public static final search_result_no_matches:I = 0x7f0b0095

.field public static final search_searching:I = 0x7f0b0094

.field public static final search_title_set:I = 0x7f0b0091

.field public static final secretbox:I = 0x7f0b0049

.field public static final security_policy_restrics_this_action:I = 0x7f0b014e

.field public static final select:I = 0x7f0b012d

.field public static final select_all:I = 0x7f0b0035

.field public static final select_item:I = 0x7f0b002b

.field public static final set_date:I = 0x7f0b00ea

.field public static final set_filetype:I = 0x7f0b00eb

.field public static final settings:I = 0x7f0b001e

.field public static final sftp_default_port:I = 0x7f0b00c6

.field public static final share:I = 0x7f0b001f

.field public static final share_sound_shot:I = 0x7f0b00db

.field public static final sheader_audio:I = 0x7f0b0153

.field public static final sheader_cloud_storage:I = 0x7f0b0154

.field public static final sheader_content:I = 0x7f0b0155

.field public static final sheader_documents:I = 0x7f0b0156

.field public static final sheader_download_history:I = 0x7f0b0157

.field public static final sheader_downloaded_apps:I = 0x7f0b0158

.field public static final sheader_images:I = 0x7f0b0159

.field public static final sheader_local_storage:I = 0x7f0b015a

.field public static final sheader_order:I = 0x7f0b015b

.field public static final sheader_quick_search:I = 0x7f0b015c

.field public static final sheader_recent_files:I = 0x7f0b015d

.field public static final sheader_shortcuts:I = 0x7f0b015e

.field public static final sheader_title:I = 0x7f0b015f

.field public static final sheader_videos:I = 0x7f0b0160

.field public static final shortcut:I = 0x7f0b00d4

.field public static final shortcut_added:I = 0x7f0b0060

.field public static final shortcut_exists:I = 0x7f0b0061

.field public static final shortcuts_header:I = 0x7f0b011e

.field public static final show_file_extension:I = 0x7f0b0072

.field public static final show_hidden_files:I = 0x7f0b0071

.field public static final show_noti:I = 0x7f0b010c

.field public static final sign_in:I = 0x7f0b00dc

.field public static final single_shortcut_deleted:I = 0x7f0b0122

.field public static final size:I = 0x7f0b0034

.field public static final skip:I = 0x7f0b0178

.field public static final sort_by:I = 0x7f0b001c

.field public static final stms_appgroup:I = 0x7f0b017f

.field public static final stop:I = 0x7f0b0179

.field public static final storage:I = 0x7f0b012e

.field public static final storage_available:I = 0x7f0b00dd

.field public static final storage_byte:I = 0x7f0b013b

.field public static final storage_bytes:I = 0x7f0b013c

.field public static final storage_usage:I = 0x7f0b00d6

.field public static final tera_byte:I = 0x7f0b0143

.field public static final tera_bytes:I = 0x7f0b0144

.field public static final this_shortcut_will_be_deleted:I = 0x7f0b00f9

.field public static final thumbnail:I = 0x7f0b002e

.field public static final thumbnail_view:I = 0x7f0b002f

.field public static final time:I = 0x7f0b0031

.field public static final title:I = 0x7f0b00e4

.field public static final total:I = 0x7f0b004a

.field public static final total_size:I = 0x7f0b00aa

.field public static final total_storage:I = 0x7f0b0161

.field public static final tts_clear_query:I = 0x7f0b017e

.field public static final tts_header:I = 0x7f0b017c

.field public static final type:I = 0x7f0b0032

.field public static final type_accum_time:I = 0x7f0b0051

.field public static final type_count:I = 0x7f0b004f

.field public static final type_date:I = 0x7f0b0050

.field public static final type_interval:I = 0x7f0b004e

.field public static final type_time:I = 0x7f0b004d

.field public static final type_time_from:I = 0x7f0b0052

.field public static final type_time_until:I = 0x7f0b0053

.field public static final type_unlimited:I = 0x7f0b004c

.field public static final unable_to_connect_network:I = 0x7f0b0131

.field public static final unable_to_extract:I = 0x7f0b0029

.field public static final unable_to_rename:I = 0x7f0b0075

.field public static final unknown:I = 0x7f0b0048

.field public static final unlock:I = 0x7f0b0107

.field public static final unlock_notification:I = 0x7f0b010b

.field public static final unlocking_big_file:I = 0x7f0b0137

.field public static final unlocking_file:I = 0x7f0b0135

.field public static final unselect_all:I = 0x7f0b0036

.field public static final unsupport_audio_codec:I = 0x7f0b0081

.field public static final unsupport_audio_file:I = 0x7f0b0080

.field public static final unsupported_file_encoding:I = 0x7f0b0103

.field public static final unsupported_file_type:I = 0x7f0b007f

.field public static final usb_storage:I = 0x7f0b0041

.field public static final usb_storage_a:I = 0x7f0b0042

.field public static final usb_storage_b:I = 0x7f0b0043

.field public static final usb_storage_c:I = 0x7f0b0044

.field public static final usb_storage_d:I = 0x7f0b0045

.field public static final usb_storage_e:I = 0x7f0b0046

.field public static final usb_storage_f:I = 0x7f0b0047

.field public static final videos:I = 0x7f0b0004

.field public static final view_as:I = 0x7f0b001b

.field public static final view_mode:I = 0x7f0b012c

.field public static final x_of_y:I = 0x7f0b00af

.field public static final zip:I = 0x7f0b0027

.field public static final zip_file_cannot_be_opened:I = 0x7f0b00ec


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2030
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
