.class public interface abstract Lcom/sec/android/app/myfiles/adapter/ISelectModeAdapter;
.super Ljava/lang/Object;
.source "ISelectModeAdapter.java"


# virtual methods
.method public abstract finishSelectMode()V
.end method

.method public abstract getSelectedItem()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getSelectedItemCount()I
.end method

.method public abstract getSelectedItemPosition()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isDragMode()Z
.end method

.method public abstract isSelectMode()Z
.end method

.method public abstract selectAllItem()V
.end method

.method public abstract selectItem(I)V
.end method

.method public abstract setSelectedItemChangeListener(Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;)V
.end method

.method public abstract startSelectMode(II)V
.end method

.method public abstract unselectAllItem()V
.end method
