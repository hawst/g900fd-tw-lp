.class Lcom/sec/android/app/myfiles/receiver/MyFilesReceiver$1;
.super Landroid/content/pm/IPackageStatsObserver$Stub;
.source "MyFilesReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/receiver/MyFilesReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/receiver/MyFilesReceiver;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/receiver/MyFilesReceiver;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 350
    iput-object p1, p0, Lcom/sec/android/app/myfiles/receiver/MyFilesReceiver$1;->this$0:Lcom/sec/android/app/myfiles/receiver/MyFilesReceiver;

    iput-object p2, p0, Lcom/sec/android/app/myfiles/receiver/MyFilesReceiver$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Landroid/content/pm/IPackageStatsObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetStatsCompleted(Landroid/content/pm/PackageStats;Z)V
    .locals 10
    .param p1, "stats"    # Landroid/content/pm/PackageStats;
    .param p2, "succeeded"    # Z

    .prologue
    .line 355
    iget-wide v6, p1, Landroid/content/pm/PackageStats;->externalCodeSize:J

    iget-wide v8, p1, Landroid/content/pm/PackageStats;->externalObbSize:J

    add-long/2addr v6, v8

    iget-wide v8, p1, Landroid/content/pm/PackageStats;->externalDataSize:J

    add-long/2addr v6, v8

    iget-wide v8, p1, Landroid/content/pm/PackageStats;->externalMediaSize:J

    add-long/2addr v6, v8

    iget-wide v8, p1, Landroid/content/pm/PackageStats;->codeSize:J

    add-long/2addr v6, v8

    iget-wide v8, p1, Landroid/content/pm/PackageStats;->dataSize:J

    add-long v2, v6, v8

    .line 360
    .local v2, "size":J
    iget-object v1, p0, Lcom/sec/android/app/myfiles/receiver/MyFilesReceiver$1;->val$context:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v0

    .line 362
    .local v0, "sharedData":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getSavedDownloadedAppTotalSize()J

    move-result-wide v6

    add-long v4, v6, v2

    .line 365
    .local v4, "total":J
    invoke-virtual {v0, v4, v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSavedDownloadedAppTotalSize(J)V

    .line 367
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->commitDownAppSize()Z

    .line 370
    return-void
.end method
