.class synthetic Lcom/sec/android/app/myfiles/db/CacheDB$1;
.super Ljava/lang/Object;
.source "CacheDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/db/CacheDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$samsung$android$allshare$Item$MediaType:[I

.field static final synthetic $SwitchMap$com$sec$android$app$myfiles$utils$FileType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1024
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/FileType;->values()[Lcom/sec/android/app/myfiles/utils/FileType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/sec/android/app/myfiles/db/CacheDB$1;->$SwitchMap$com$sec$android$app$myfiles$utils$FileType:[I

    :try_start_0
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB$1;->$SwitchMap$com$sec$android$app$myfiles$utils$FileType:[I

    sget-object v1, Lcom/sec/android/app/myfiles/utils/FileType;->DOCUMENT:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_7

    :goto_0
    :try_start_1
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB$1;->$SwitchMap$com$sec$android$app$myfiles$utils$FileType:[I

    sget-object v1, Lcom/sec/android/app/myfiles/utils/FileType;->IMAGE:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_6

    :goto_1
    :try_start_2
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB$1;->$SwitchMap$com$sec$android$app$myfiles$utils$FileType:[I

    sget-object v1, Lcom/sec/android/app/myfiles/utils/FileType;->AUDIO:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_5

    :goto_2
    :try_start_3
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB$1;->$SwitchMap$com$sec$android$app$myfiles$utils$FileType:[I

    sget-object v1, Lcom/sec/android/app/myfiles/utils/FileType;->VIDEO:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_4

    .line 894
    :goto_3
    invoke-static {}, Lcom/samsung/android/allshare/Item$MediaType;->values()[Lcom/samsung/android/allshare/Item$MediaType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/sec/android/app/myfiles/db/CacheDB$1;->$SwitchMap$com$samsung$android$allshare$Item$MediaType:[I

    :try_start_4
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB$1;->$SwitchMap$com$samsung$android$allshare$Item$MediaType:[I

    sget-object v1, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_AUDIO:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_3

    :goto_4
    :try_start_5
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB$1;->$SwitchMap$com$samsung$android$allshare$Item$MediaType:[I

    sget-object v1, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_IMAGE:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_2

    :goto_5
    :try_start_6
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB$1;->$SwitchMap$com$samsung$android$allshare$Item$MediaType:[I

    sget-object v1, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_VIDEO:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_1

    :goto_6
    :try_start_7
    sget-object v0, Lcom/sec/android/app/myfiles/db/CacheDB$1;->$SwitchMap$com$samsung$android$allshare$Item$MediaType:[I

    sget-object v1, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_FOLDER:Lcom/samsung/android/allshare/Item$MediaType;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Item$MediaType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_0

    :goto_7
    return-void

    :catch_0
    move-exception v0

    goto :goto_7

    :catch_1
    move-exception v0

    goto :goto_6

    :catch_2
    move-exception v0

    goto :goto_5

    :catch_3
    move-exception v0

    goto :goto_4

    .line 1024
    :catch_4
    move-exception v0

    goto :goto_3

    :catch_5
    move-exception v0

    goto :goto_2

    :catch_6
    move-exception v0

    goto :goto_1

    :catch_7
    move-exception v0

    goto :goto_0
.end method
