.class public Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;
.source "FTPShortcutFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public AsyncOpenCompleted()V
    .locals 0

    .prologue
    .line 92
    return-void
.end method

.method public getOptionsMenuResId(Z)I
    .locals 1
    .param p1, "isSelectMode"    # Z

    .prologue
    .line 88
    const v0, 0x7f0e0010

    return v0
.end method

.method public onBackPressed()Z
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 42
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onCreate(Landroid/os/Bundle;)V

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .line 55
    :cond_0
    return-void
.end method

.method protected onCreateDetailFragmentListener()Landroid/app/DialogFragment;
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onCreateFileOperationListener()Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 96
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;)V

    return-object v0
.end method

.method protected onCreateItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;
    .locals 1

    .prologue
    .line 139
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment$2;-><init>(Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;)V

    return-object v0
.end method

.method protected onCreateMultipleDetailFragmentListener()Landroid/app/DialogFragment;
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 71
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 73
    .local v0, "view":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 75
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 60
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onDestroy()V

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->closeCursor()V

    .line 66
    :cond_0
    return-void
.end method

.method public onRefresh()V
    .locals 0

    .prologue
    .line 79
    return-void
.end method
