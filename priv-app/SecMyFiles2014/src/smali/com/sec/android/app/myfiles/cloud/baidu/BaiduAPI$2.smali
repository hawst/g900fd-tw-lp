.class Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI$2;
.super Lcom/baidu/pcs/BaiduPCSStatusListener;
.source "BaiduAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->putFile(Ljava/io/File;Ljava/lang/String;ZLjava/lang/String;Lcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

.field final synthetic val$progressListener:Lcom/samsung/scloud/response/ProgressListener;

.field final synthetic val$sourceFileName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;Lcom/samsung/scloud/response/ProgressListener;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2263
    iput-object p1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI$2;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    iput-object p2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI$2;->val$progressListener:Lcom/samsung/scloud/response/ProgressListener;

    iput-object p3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI$2;->val$sourceFileName:Ljava/lang/String;

    invoke-direct {p0}, Lcom/baidu/pcs/BaiduPCSStatusListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgress(JJ)V
    .locals 7
    .param p1, "bytes"    # J
    .param p3, "total"    # J

    .prologue
    const-wide/16 v4, 0x64

    .line 2266
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI$2;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bytes uploaded:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "---->"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    mul-long v2, p1, v4

    div-long/2addr v2, p3

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->access$000(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;Ljava/lang/String;)V

    .line 2267
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI$2;->val$progressListener:Lcom/samsung/scloud/response/ProgressListener;

    if-eqz v0, :cond_0

    .line 2268
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI$2;->val$progressListener:Lcom/samsung/scloud/response/ProgressListener;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI$2;->val$sourceFileName:Ljava/lang/String;

    const/4 v2, 0x1

    mul-long/2addr v4, p1

    div-long/2addr v4, p3

    invoke-interface {v0, v1, v2, v4, v5}, Lcom/samsung/scloud/response/ProgressListener;->onUpdate(Ljava/lang/String;IJ)V

    .line 2270
    :cond_0
    return-void
.end method

.method public toContinue()Z
    .locals 3

    .prologue
    .line 2274
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI$2;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "toContinue() isContinue = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI$2;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    # invokes: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->isToContinue()Z
    invoke-static {v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->access$100(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->dd(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->access$000(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;Ljava/lang/String;)V

    .line 2276
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI$2;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;

    # invokes: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->isToContinue()Z
    invoke-static {v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;->access$100(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAPI;)Z

    move-result v0

    return v0
.end method
