.class Lcom/sec/android/app/myfiles/fragment/InputNameFragment$3;
.super Ljava/lang/Object;
.source "InputNameFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->setupInputEditText(Landroid/widget/EditText;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/InputNameFragment;)V
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 208
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 204
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 5
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x1

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mOkButton:Landroid/widget/Button;

    if-eqz v0, :cond_2

    .line 185
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result p4

    .line 186
    if-lt p4, v3, :cond_1

    if-lez p4, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mFile:Ljava/io/File;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/InputNameFragment;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->cur_operation:I

    if-eq v0, v4, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->cur_operation:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mFile:Ljava/io/File;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/InputNameFragment;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/storage/emulated/0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mInputEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    const v2, 0x7f0b003e

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 189
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mOkButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 197
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->cur_operation:I

    if-ne v0, v4, :cond_3

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mInputName:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->access$002(Lcom/sec/android/app/myfiles/fragment/InputNameFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 200
    :cond_3
    return-void

    .line 193
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mOkButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method
