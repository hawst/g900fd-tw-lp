.class Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$3;
.super Ljava/lang/Object;
.source "SearchSetLocationFragment.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;)V
    .locals 0

    .prologue
    .line 362
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public deviceAdded(Lcom/samsung/android/allshare/Device;)V
    .locals 3
    .param p1, "device"    # Lcom/samsung/android/allshare/Device;

    .prologue
    .line 376
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mDevicesList:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    .line 378
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mDevicesList:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    # invokes: Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->isRegisteredDevice(Lcom/samsung/android/allshare/Device;)Z
    invoke-static {v0, p1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;Lcom/samsung/android/allshare/Device;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 380
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mDevicesList:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 381
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->availStorage:Ljava/util/ArrayList;

    # invokes: Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->refreshLocationList(Ljava/util/ArrayList;)V
    invoke-static {v0, v2}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;Ljava/util/ArrayList;)V

    .line 382
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mListArrayAdapter:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;->notifyDataSetChanged()V

    .line 391
    :cond_0
    :goto_0
    monitor-exit v1

    .line 392
    return-void

    .line 384
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mDevicesList:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 386
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mDevicesList:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 387
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->availStorage:Ljava/util/ArrayList;

    # invokes: Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->refreshLocationList(Ljava/util/ArrayList;)V
    invoke-static {v0, v2}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;Ljava/util/ArrayList;)V

    .line 388
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mListArrayAdapter:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 391
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public deviceRemoved(Lcom/samsung/android/allshare/Device;)V
    .locals 3
    .param p1, "device"    # Lcom/samsung/android/allshare/Device;

    .prologue
    .line 365
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mDevicesList:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;)Ljava/util/List;

    move-result-object v1

    monitor-enter v1

    .line 367
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mDevicesList:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 368
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->availStorage:Ljava/util/ArrayList;

    # invokes: Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->refreshLocationList(Ljava/util/ArrayList;)V
    invoke-static {v0, v2}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;Ljava/util/ArrayList;)V

    .line 369
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mListArrayAdapter:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;->notifyDataSetChanged()V

    .line 371
    monitor-exit v1

    .line 372
    return-void

    .line 371
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
