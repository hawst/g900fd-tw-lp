.class Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;
.super Ljava/lang/Object;
.source "RemoteShareFragment.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$FolderInfoReceiverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFolderInfoReceived(ILandroid/database/Cursor;)V
    .locals 18
    .param p1, "token"    # I
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 129
    if-eqz p2, :cond_0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v14, v14, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-nez v14, :cond_1

    .line 223
    :cond_0
    :goto_0
    return-void

    .line 132
    :cond_1
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v14

    if-eqz v14, :cond_2

    .line 133
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v14, v14, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    .line 134
    .local v2, "actionBar":Landroid/app/ActionBar;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v14, v14, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    .line 135
    .local v13, "res":Landroid/content/res/Resources;
    packed-switch p1, :pswitch_data_0

    .line 222
    .end local v2    # "actionBar":Landroid/app/ActionBar;
    .end local v13    # "res":Landroid/content/res/Resources;
    :cond_2
    :goto_1
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 137
    .restart local v2    # "actionBar":Landroid/app/ActionBar;
    .restart local v13    # "res":Landroid/content/res/Resources;
    :pswitch_0
    const-string v14, "recipient_ids"

    move-object/from16 v0, p2

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p2

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 138
    .local v3, "contactIds":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v14, v14, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v14, v3}, Lcom/sec/android/app/myfiles/utils/Utils;->getContactNames(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 139
    .local v10, "folderName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v14, v14, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    instance-of v14, v14, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    if-eqz v14, :cond_3

    if-eqz v13, :cond_3

    .line 140
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v14, v14, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v14, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->getMessageFolder()I

    move-result v14

    const/16 v15, 0x27

    if-ne v14, v15, :cond_9

    .line 141
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v16, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const v16, 0x7f0b000a

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    # setter for: Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mRemoteShareRoot:Ljava/lang/String;
    invoke-static {v14, v15}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->access$002(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 147
    :cond_3
    :goto_2
    if-nez v10, :cond_a

    .line 148
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mRemoteShareRoot:Ljava/lang/String;
    invoke-static {v14}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)Ljava/lang/String;

    move-result-object v12

    .line 152
    .local v12, "newPathIndicator":Ljava/lang/String;
    :goto_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mRemoteSharePathIndicator:Ljava/lang/String;
    invoke-static {v14}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_4

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mRemoteSharePathIndicator:Ljava/lang/String;
    invoke-static {v14}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_5

    .line 153
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    # setter for: Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mRemoteSharePathIndicator:Ljava/lang/String;
    invoke-static {v14, v12}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->access$102(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 154
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v14, v14, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mRemoteSharePathIndicator:Ljava/lang/String;
    invoke-static {v15}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 156
    :cond_5
    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->isAdded()Z

    move-result v14

    if-eqz v14, :cond_6

    .line 157
    invoke-virtual {v2, v10}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 160
    :cond_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v14, v14, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v14, :cond_2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v14, v14, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    instance-of v14, v14, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;

    if-eqz v14, :cond_2

    .line 162
    const-string v14, "media_box"

    move-object/from16 v0, p2

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p2

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 163
    .local v11, "mediaBox":I
    const/4 v14, 0x1

    if-eq v11, v14, :cond_7

    const/4 v14, 0x3

    if-ne v11, v14, :cond_8

    .line 167
    :cond_7
    const-string v14, "expire_date"

    move-object/from16 v0, p2

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p2

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 168
    .local v4, "expireDate":Ljava/lang/Long;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v14, v14, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v14, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->setFolderExpiredDate(J)V

    .line 169
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v14, v14, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v14, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;

    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->getFolderExpiredDate()Ljava/lang/String;

    move-result-object v14

    # setter for: Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mCurrentFolderExpireDate:Ljava/lang/String;
    invoke-static {v15, v14}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->access$202(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 170
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v14, v14, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->notifyDataSetChanged()V

    .line 173
    .end local v4    # "expireDate":Ljava/lang/Long;
    :cond_8
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v14, v14, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_2

    .line 174
    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 175
    .local v8, "folderId":Ljava/lang/String;
    if-eqz v8, :cond_2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v14, v14, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v8, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 176
    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->isAdded()Z

    move-result v14

    if-eqz v14, :cond_2

    .line 178
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget v14, v14, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mViewMode:I

    const/4 v15, 0x2

    if-ne v14, v15, :cond_b

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mCurrentFolderExpireDate:Ljava/lang/String;
    invoke-static {v14}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_b

    .line 180
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mActionBarSubtitleText:Ljava/lang/String;
    invoke-static {v15}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " | "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v15, v15, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v15}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0b0011

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mCurrentFolderExpireDate:Ljava/lang/String;
    invoke-static {v15}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2, v14}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 143
    .end local v8    # "folderId":Ljava/lang/String;
    .end local v11    # "mediaBox":I
    .end local v12    # "newPathIndicator":Ljava/lang/String;
    :cond_9
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v16, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const v16, 0x7f0b0009

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    # setter for: Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mRemoteShareRoot:Ljava/lang/String;
    invoke-static {v14, v15}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->access$002(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_2

    .line 150
    :cond_a
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mRemoteShareRoot:Ljava/lang/String;
    invoke-static {v15}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    sget-object v15, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .restart local v12    # "newPathIndicator":Ljava/lang/String;
    goto/16 :goto_3

    .line 186
    .restart local v8    # "folderId":Ljava/lang/String;
    .restart local v11    # "mediaBox":I
    :cond_b
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mActionBarSubtitleText:Ljava/lang/String;
    invoke-static {v14}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2, v14}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 194
    .end local v3    # "contactIds":Ljava/lang/String;
    .end local v8    # "folderId":Ljava/lang/String;
    .end local v10    # "folderName":Ljava/lang/String;
    .end local v11    # "mediaBox":I
    .end local v12    # "newPathIndicator":Ljava/lang/String;
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v14, v14, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v14

    if-nez v14, :cond_d

    .line 195
    new-instance v9, Landroid/util/SparseArray;

    invoke-direct {v9}, Landroid/util/SparseArray;-><init>()V

    .line 197
    .local v9, "folderInfoArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter$RemoteShareFolderInfo;>;"
    :cond_c
    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 198
    .local v8, "folderId":I
    const/4 v14, 0x1

    move-object/from16 v0, p2

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 199
    .local v5, "filesCount":I
    const/4 v14, 0x2

    move-object/from16 v0, p2

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 200
    .local v6, "filesSize":J
    new-instance v14, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter$RemoteShareFolderInfo;

    invoke-direct {v14, v5, v6, v7}, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter$RemoteShareFolderInfo;-><init>(IJ)V

    invoke-virtual {v9, v8, v14}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 201
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v14

    if-nez v14, :cond_c

    .line 202
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v14, v14, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    instance-of v14, v14, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;

    if-eqz v14, :cond_2

    .line 203
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v14, v14, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v14, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;

    invoke-virtual {v14, v9}, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->setFolderInfoArray(Landroid/util/SparseArray;)V

    goto/16 :goto_1

    .line 206
    .end local v5    # "filesCount":I
    .end local v6    # "filesSize":J
    .end local v8    # "folderId":I
    .end local v9    # "folderInfoArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter$RemoteShareFolderInfo;>;"
    :cond_d
    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 207
    .local v8, "folderId":Ljava/lang/String;
    if-eqz v8, :cond_2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v14, v14, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v8, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 208
    const/4 v14, 0x1

    move-object/from16 v0, p2

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 209
    .restart local v5    # "filesCount":I
    const/4 v14, 0x2

    move-object/from16 v0, p2

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 210
    .restart local v6    # "filesSize":J
    if-eqz v13, :cond_2

    .line 211
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const v16, 0x7f0b0070

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " | "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v0, v6, v7}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    # setter for: Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mActionBarSubtitleText:Ljava/lang/String;
    invoke-static {v14, v15}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->access$302(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 213
    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->isAdded()Z

    move-result v14

    if-eqz v14, :cond_2

    .line 214
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mActionBarSubtitleText:Ljava/lang/String;
    invoke-static {v14}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2, v14}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 135
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
