.class final Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;
.super Ljava/lang/Object;
.source "SelectorActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "TabInfo"
.end annotation


# instance fields
.field private final args:Landroid/os/Bundle;

.field private final clss:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private final tag:Ljava/lang/String;

.field final synthetic this$1:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V
    .locals 0
    .param p2, "_tag"    # Ljava/lang/String;
    .param p4, "_args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 825
    .local p3, "_class":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    iput-object p1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;->this$1:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 827
    iput-object p2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;->tag:Ljava/lang/String;

    .line 829
    iput-object p3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;->clss:Ljava/lang/Class;

    .line 831
    iput-object p4, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;->args:Landroid/os/Bundle;

    .line 832
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;

    .prologue
    .line 817
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;->tag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;)Ljava/lang/Class;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;

    .prologue
    .line 817
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;->clss:Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;

    .prologue
    .line 817
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$TabInfo;->args:Landroid/os/Bundle;

    return-object v0
.end method
