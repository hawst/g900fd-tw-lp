.class Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$21;
.super Ljava/lang/Object;
.source "CategoryHomeFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->onPrepareDialog(ILandroid/app/Dialog;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

.field final synthetic val$checkDoNotShow:Landroid/widget/CheckBox;

.field final synthetic val$dialogId:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;ILandroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 2119
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$21;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iput p2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$21;->val$dialogId:I

    iput-object p3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$21;->val$checkDoNotShow:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2124
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$21;->val$dialogId:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$21;->val$dialogId:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 2126
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$21;->val$checkDoNotShow:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2127
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$21;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # setter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->bDoNotCheckDropBox:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$1702(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;Z)Z

    .line 2138
    :cond_1
    :goto_0
    return-void

    .line 2129
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$21;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # setter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->bDoNotCheckDropBox:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$1702(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;Z)Z

    goto :goto_0

    .line 2131
    :cond_3
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$21;->val$dialogId:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 2132
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$21;->val$checkDoNotShow:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2133
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$21;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # setter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->bDoNotCheckFTP:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$1802(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;Z)Z

    goto :goto_0

    .line 2135
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$21;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # setter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->bDoNotCheckFTP:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$1802(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;Z)Z

    goto :goto_0
.end method
