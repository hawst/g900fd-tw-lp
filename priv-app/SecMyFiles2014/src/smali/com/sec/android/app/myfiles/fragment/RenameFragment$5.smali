.class Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;
.super Ljava/lang/Object;
.source "RenameFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/RenameFragment;->clickOK()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

.field final synthetic val$currentPath1:Ljava/lang/String;

.field final synthetic val$dstFile:Ljava/io/File;

.field final synthetic val$dstName1:Ljava/lang/String;

.field final synthetic val$srcFile:Ljava/io/File;

.field final synthetic val$srcName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/RenameFragment;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;Ljava/lang/String;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 358
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    iput-object p2, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$currentPath1:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$dstName1:Ljava/lang/String;

    iput-object p4, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$dstFile:Ljava/io/File;

    iput-object p5, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$srcName:Ljava/lang/String;

    iput-object p6, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$srcFile:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    .line 363
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSourceFragmentId:I
    invoke-static {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/RenameFragment;)I

    move-result v11

    const/16 v12, 0x8

    if-eq v11, v12, :cond_0

    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSourceFragmentId:I
    invoke-static {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/RenameFragment;)I

    move-result v11

    const/16 v12, 0x12

    if-ne v11, v12, :cond_7

    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mPath:Ljava/lang/String;
    invoke-static {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/RenameFragment;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "/storage"

    invoke-virtual {v11, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_7

    .line 365
    :cond_0
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    move-result-object v5

    .line 366
    .local v5, "mDBAPIHelper":Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$currentPath1:Ljava/lang/String;

    iget-object v12, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$dstName1:Ljava/lang/String;

    invoke-virtual {v5, v11, v12}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->renameFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 368
    new-instance v1, Ljava/lang/String;

    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$currentPath1:Ljava/lang/String;

    invoke-direct {v1, v11}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 370
    .local v1, "dropboxPath":Ljava/lang/String;
    sget-char v11, Ljava/io/File;->separatorChar:C

    invoke-virtual {v1, v11}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    invoke-virtual {v1, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 372
    .local v8, "shortcutName":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v11

    if-nez v11, :cond_1

    .line 374
    const-string v1, "Dropbox/"

    .line 376
    :cond_1
    const-string v11, "Dropbox/"

    invoke-virtual {v1, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_2

    .line 378
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Dropbox/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 381
    :cond_2
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    invoke-static {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/RenameFragment;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v11

    invoke-virtual {v11, v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSetHoemScreenPath(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 383
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    invoke-static {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/RenameFragment;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v11

    invoke-virtual {v11, v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->deleteSetHomescreenPath(Ljava/lang/String;)V

    .line 384
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v11

    invoke-virtual {v11}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    invoke-static {v11, v1, v8}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteShortcutFromHome(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    :cond_3
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v11

    invoke-virtual {v11}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    const/16 v12, 0x8

    invoke-static {v11, v1, v12}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteShortcut(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v7

    .line 388
    .local v7, "rowsDeleted":I
    const/4 v11, 0x1

    if-ge v7, v11, :cond_4

    .line 389
    const/4 v11, 0x0

    const-string v12, "RenameFragment"

    const-string v13, "Shortcut does not exist"

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 392
    :cond_4
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->access$500(Lcom/sec/android/app/myfiles/fragment/RenameFragment;)Landroid/os/Handler;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 594
    .end local v1    # "dropboxPath":Ljava/lang/String;
    .end local v5    # "mDBAPIHelper":Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;
    .end local v7    # "rowsDeleted":I
    .end local v8    # "shortcutName":Ljava/lang/String;
    :cond_5
    :goto_0
    return-void

    .line 395
    .restart local v5    # "mDBAPIHelper":Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;
    :cond_6
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    const v12, 0x7f0b0075

    # invokes: Lcom/sec/android/app/myfiles/fragment/RenameFragment;->showToast(I)V
    invoke-static {v11, v12}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->access$600(Lcom/sec/android/app/myfiles/fragment/RenameFragment;I)V

    goto :goto_0

    .line 397
    .end local v5    # "mDBAPIHelper":Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;
    :cond_7
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSourceFragmentId:I
    invoke-static {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/RenameFragment;)I

    move-result v11

    const/16 v12, 0x1f

    if-ne v11, v12, :cond_a

    .line 400
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v11

    if-eqz v11, :cond_5

    .line 402
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$currentPath1:Ljava/lang/String;

    invoke-static {v11, v12}, Lcom/sec/android/app/myfiles/utils/Utils;->isDirectoryOfDropbox(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    .line 404
    .local v3, "isDirectory":Z
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    move-result-object v0

    .line 405
    .local v0, "baiduAPIHelper":Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$currentPath1:Ljava/lang/String;

    iget-object v12, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$dstName1:Ljava/lang/String;

    const/4 v13, 0x0

    invoke-virtual {v0, v11, v12, v13, v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->rename(Ljava/lang/String;Ljava/lang/String;ZZ)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 407
    const/4 v11, 0x1

    sput-boolean v11, Lcom/sec/android/app/myfiles/utils/Utils;->needUpdate:Z

    .line 408
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->access$500(Lcom/sec/android/app/myfiles/fragment/RenameFragment;)Landroid/os/Handler;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 411
    :cond_8
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->getErrorCode()I

    move-result v11

    const/16 v12, 0x7955

    if-ne v11, v12, :cond_9

    .line 412
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    const v12, 0x7f0b012a

    # invokes: Lcom/sec/android/app/myfiles/fragment/RenameFragment;->showToast(I)V
    invoke-static {v11, v12}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->access$600(Lcom/sec/android/app/myfiles/fragment/RenameFragment;I)V

    goto :goto_0

    .line 414
    :cond_9
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    const v12, 0x7f0b0075

    # invokes: Lcom/sec/android/app/myfiles/fragment/RenameFragment;->showToast(I)V
    invoke-static {v11, v12}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->access$600(Lcom/sec/android/app/myfiles/fragment/RenameFragment;I)V

    goto :goto_0

    .line 420
    .end local v0    # "baiduAPIHelper":Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;
    .end local v3    # "isDirectory":Z
    :cond_a
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$dstFile:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_13

    .line 422
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$srcName:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$dstName1:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_12

    .line 424
    new-instance v10, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$srcFile:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const/16 v12, 0x5f

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 426
    .local v10, "tempFile":Ljava/io/File;
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$srcFile:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->isFile()Z

    move-result v4

    .line 428
    .local v4, "isFile":Z
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$srcFile:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->isDirectory()Z

    move-result v3

    .line 430
    .restart local v3    # "isDirectory":Z
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$srcFile:Ljava/io/File;

    invoke-virtual {v11, v10}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v11

    if-eqz v11, :cond_11

    .line 432
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$dstFile:Ljava/io/File;

    invoke-virtual {v10, v11}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 435
    :try_start_0
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    iget-object v12, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$srcFile:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->deleteInDatabase(Ljava/lang/String;)Z

    .line 437
    if-eqz v4, :cond_f

    .line 439
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$srcFile:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    .line 441
    .local v9, "srcFilePath":Ljava/lang/String;
    if-eqz v9, :cond_b

    .line 443
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    invoke-static {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/RenameFragment;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v11

    invoke-virtual {v11, v9}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSetHoemScreenPath(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_b

    .line 446
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v11

    invoke-virtual {v11}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    invoke-static {v11, v9}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteFileShortcutFromHome(Landroid/content/Context;Ljava/lang/String;)V

    .line 471
    .end local v9    # "srcFilePath":Ljava/lang/String;
    :cond_b
    :goto_1
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$srcFile:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->removeThumbnailCache(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 479
    :goto_2
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$dstFile:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->isDirectory()Z

    move-result v11

    if-nez v11, :cond_c

    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$dstFile:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    const-string v12, "#"

    invoke-virtual {v11, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_c

    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$dstFile:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    const-string v12, "%"

    invoke-virtual {v11, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_c

    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$dstFile:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v11

    const-string v12, "."

    invoke-virtual {v11, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_10

    .line 480
    :cond_c
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getDialogInstance()Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    move-result-object v11

    if-eqz v11, :cond_d

    .line 481
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getDialogInstance()Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->showWaitProgressDialog()V

    .line 483
    :cond_d
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v11

    invoke-virtual {v11}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$dstFile:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/app/myfiles/utils/Utils;->sendScan(Landroid/content/Context;Ljava/lang/String;)V

    .line 488
    :goto_3
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v11

    instance-of v11, v11, Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    if-eqz v11, :cond_e

    .line 489
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->access$500(Lcom/sec/android/app/myfiles/fragment/RenameFragment;)Landroid/os/Handler;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 492
    :cond_e
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 494
    .local v6, "rename_intent":Landroid/content/Intent;
    const-string v11, "rename_done"

    invoke-virtual {v6, v11}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 496
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v11

    invoke-virtual {v11}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    if-eqz v11, :cond_5

    .line 497
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v11

    invoke-virtual {v11}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    invoke-virtual {v11, v6}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 451
    .end local v6    # "rename_intent":Landroid/content/Intent;
    :cond_f
    if-eqz v3, :cond_b

    .line 453
    :try_start_1
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$srcFile:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    .line 455
    .restart local v9    # "srcFilePath":Ljava/lang/String;
    if-eqz v9, :cond_b

    .line 463
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v11

    invoke-virtual {v11}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    const/16 v12, 0x1a

    invoke-static {v11, v9, v12}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteShortcut(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v7

    .line 464
    .restart local v7    # "rowsDeleted":I
    const/4 v11, 0x1

    if-ge v7, v11, :cond_b

    .line 465
    const/4 v11, 0x0

    const-string v12, "RenameFragment"

    const-string v13, "Shortcut does not exist"

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 473
    .end local v7    # "rowsDeleted":I
    .end local v9    # "srcFilePath":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 476
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 486
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_10
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v11

    invoke-virtual {v11}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$dstFile:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/app/myfiles/utils/Utils;->sendScan(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_3

    .line 503
    :cond_11
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    const v12, 0x7f0b0075

    # invokes: Lcom/sec/android/app/myfiles/fragment/RenameFragment;->showToast(I)V
    invoke-static {v11, v12}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->access$600(Lcom/sec/android/app/myfiles/fragment/RenameFragment;I)V

    goto/16 :goto_0

    .line 509
    .end local v3    # "isDirectory":Z
    .end local v4    # "isFile":Z
    .end local v10    # "tempFile":Ljava/io/File;
    :cond_12
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    const v12, 0x7f0b012a

    # invokes: Lcom/sec/android/app/myfiles/fragment/RenameFragment;->showToast(I)V
    invoke-static {v11, v12}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->access$600(Lcom/sec/android/app/myfiles/fragment/RenameFragment;I)V

    goto/16 :goto_0

    .line 515
    :cond_13
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$srcFile:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->isFile()Z

    move-result v4

    .line 517
    .restart local v4    # "isFile":Z
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$srcFile:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->isDirectory()Z

    move-result v3

    .line 519
    .restart local v3    # "isDirectory":Z
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$srcFile:Ljava/io/File;

    iget-object v12, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$dstFile:Ljava/io/File;

    invoke-virtual {v11, v12}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v11

    if-eqz v11, :cond_19

    .line 522
    :try_start_2
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    iget-object v12, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$srcFile:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->deleteInDatabase(Ljava/lang/String;)Z

    .line 524
    if-eqz v4, :cond_16

    .line 526
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$srcFile:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    .line 528
    .restart local v9    # "srcFilePath":Ljava/lang/String;
    if-eqz v9, :cond_14

    .line 530
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    invoke-static {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/RenameFragment;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v11

    invoke-virtual {v11, v9}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSetHoemScreenPath(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_14

    .line 532
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    invoke-static {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/RenameFragment;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v11

    invoke-virtual {v11, v9}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->deleteSetHomescreenPath(Ljava/lang/String;)V

    .line 533
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v11

    invoke-virtual {v11}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    invoke-static {v11, v9}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteFileShortcutFromHome(Landroid/content/Context;Ljava/lang/String;)V

    .line 558
    .end local v9    # "srcFilePath":Ljava/lang/String;
    :cond_14
    :goto_4
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$srcFile:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->removeThumbnailCache(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 566
    :goto_5
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$dstFile:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->isDirectory()Z

    move-result v11

    if-nez v11, :cond_15

    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$dstFile:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    const-string v12, "#"

    invoke-virtual {v11, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_15

    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$dstFile:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    const-string v12, "%"

    invoke-virtual {v11, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_15

    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$dstFile:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v11

    const-string v12, "."

    invoke-virtual {v11, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_18

    .line 570
    :cond_15
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v11

    invoke-virtual {v11}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$dstFile:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/app/myfiles/utils/Utils;->sendScan(Landroid/content/Context;Ljava/lang/String;)V

    .line 576
    :goto_6
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->access$500(Lcom/sec/android/app/myfiles/fragment/RenameFragment;)Landroid/os/Handler;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 579
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 581
    .restart local v6    # "rename_intent":Landroid/content/Intent;
    const-string v11, "rename_done"

    invoke-virtual {v6, v11}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 583
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v11

    invoke-virtual {v11}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    if-eqz v11, :cond_5

    .line 584
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v11

    invoke-virtual {v11}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    invoke-virtual {v11, v6}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 537
    .end local v6    # "rename_intent":Landroid/content/Intent;
    :cond_16
    if-eqz v3, :cond_14

    .line 539
    :try_start_3
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$srcFile:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    .line 541
    .restart local v9    # "srcFilePath":Ljava/lang/String;
    if-eqz v9, :cond_14

    .line 543
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    invoke-static {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/RenameFragment;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v11

    invoke-virtual {v11, v9}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSetHoemScreenPath(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_17

    .line 545
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    invoke-static {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/RenameFragment;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v11

    invoke-virtual {v11, v9}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->deleteSetHomescreenPath(Ljava/lang/String;)V

    .line 546
    sget-char v11, Ljava/io/File;->separatorChar:C

    invoke-virtual {v9, v11}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    invoke-virtual {v9, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 547
    .restart local v8    # "shortcutName":Ljava/lang/String;
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v11

    invoke-virtual {v11}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    invoke-static {v11, v9, v8}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteShortcutFromHome(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    .end local v8    # "shortcutName":Ljava/lang/String;
    :cond_17
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v11

    invoke-virtual {v11}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    const/16 v12, 0x1a

    invoke-static {v11, v9, v12}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteShortcut(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v7

    .line 551
    .restart local v7    # "rowsDeleted":I
    const/4 v11, 0x1

    if-ge v7, v11, :cond_14

    .line 552
    const/4 v11, 0x0

    const-string v12, "RenameFragment"

    const-string v13, "Shortcut does not exist"

    invoke-static {v11, v12, v13}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_4

    .line 560
    .end local v7    # "rowsDeleted":I
    .end local v9    # "srcFilePath":Ljava/lang/String;
    :catch_1
    move-exception v2

    .line 563
    .restart local v2    # "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_5

    .line 573
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_18
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v11

    invoke-virtual {v11}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->val$dstFile:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/app/myfiles/utils/Utils;->sendScan(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 589
    :cond_19
    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    const v12, 0x7f0b0075

    # invokes: Lcom/sec/android/app/myfiles/fragment/RenameFragment;->showToast(I)V
    invoke-static {v11, v12}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->access$600(Lcom/sec/android/app/myfiles/fragment/RenameFragment;I)V

    goto/16 :goto_0
.end method
