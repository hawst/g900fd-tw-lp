.class public Lcom/sec/android/app/myfiles/adapter/WatchAdapter;
.super Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;
.source "WatchAdapter.java"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/ShortcutItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    .local p2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/ShortcutItem;>;"
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 35
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/WatchAdapter;->mContext:Landroid/content/Context;

    .line 37
    return-void
.end method


# virtual methods
.method protected loadShortcuts()Ljava/util/ArrayList;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/ShortcutItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v1, 0x21

    const/4 v4, 0x0

    .line 42
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 46
    .local v14, "userShortcutList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/ShortcutItem;>;"
    new-instance v12, Ljava/io/File;

    sget-object v2, Lcom/sec/android/app/myfiles/utils/Constant;->WATCH_FOLDER_IMAGE_AND_VIDEO:Ljava/lang/String;

    invoke-direct {v12, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 48
    .local v12, "mImageAndVideoFolder":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 50
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/WatchAdapter;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/WatchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 52
    new-instance v0, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/WatchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0003

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/myfiles/utils/Constant;->WATCH_FOLDER_IMAGE_AND_VIDEO:Ljava/lang/String;

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/element/ShortcutItem;-><init>(ILjava/lang/String;Ljava/lang/String;ZI)V

    .line 54
    .local v0, "itemOfImage":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 56
    new-instance v5, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/WatchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0004

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/myfiles/utils/Constant;->WATCH_FOLDER_IMAGE_AND_VIDEO:Ljava/lang/String;

    const/4 v10, 0x1

    move v6, v1

    move v9, v4

    invoke-direct/range {v5 .. v10}, Lcom/sec/android/app/myfiles/element/ShortcutItem;-><init>(ILjava/lang/String;Ljava/lang/String;ZI)V

    .line 58
    .local v5, "itemOfVideo":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v14, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 64
    .end local v0    # "itemOfImage":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    .end local v5    # "itemOfVideo":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :cond_0
    new-instance v13, Ljava/io/File;

    sget-object v2, Lcom/sec/android/app/myfiles/utils/Constant;->WATCH_FOLDER_VOICE:Ljava/lang/String;

    invoke-direct {v13, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 66
    .local v13, "mVoiceFolder":Ljava/io/File;
    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 68
    new-instance v6, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    const-string v8, "Voice"

    sget-object v9, Lcom/sec/android/app/myfiles/utils/Constant;->WATCH_FOLDER_VOICE:Ljava/lang/String;

    const/4 v11, 0x2

    move v7, v1

    move v10, v4

    invoke-direct/range {v6 .. v11}, Lcom/sec/android/app/myfiles/element/ShortcutItem;-><init>(ILjava/lang/String;Ljava/lang/String;ZI)V

    .line 70
    .local v6, "itemOfVoice":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v14, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    .end local v6    # "itemOfVoice":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :cond_1
    return-object v14
.end method
