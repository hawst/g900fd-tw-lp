.class Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$11;
.super Landroid/content/BroadcastReceiver;
.source "FileBrowserFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->setExternalStorageReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)V
    .locals 0

    .prologue
    .line 1716
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1721
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1722
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 1724
    .local v2, "uri":Landroid/net/Uri;
    const/4 v3, 0x2

    const-string v4, "FileBrowserFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Broadcast receive : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " , uri : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1726
    const-string v3, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1729
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isMountedStateCheck(Landroid/content/Context;)V

    .line 1731
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v3, :cond_3

    .line 1733
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1735
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v1

    .line 1737
    .local v1, "path":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    instance-of v3, v3, Lcom/sec/android/app/myfiles/SelectorActivity;

    if-eqz v3, :cond_5

    .line 1740
    if-eqz v1, :cond_1

    .line 1743
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNeedToRefresh:Z

    .line 1744
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->finish()V

    .line 1759
    .end local v1    # "path":Ljava/lang/String;
    :cond_1
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->refreshNavigation()V

    .line 1761
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v3

    const-string v4, "/storage"

    if-eq v3, v4, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    if-ne v3, v4, :cond_3

    .line 1763
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->isSelectMode()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1765
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->finishSelectMode()V

    .line 1771
    :cond_3
    const-string v3, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1772
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->refreshNavigation()V

    .line 1774
    :cond_4
    return-void

    .line 1747
    .restart local v1    # "path":Ljava/lang/String;
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    instance-of v3, v3, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v3, :cond_1

    .line 1749
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1751
    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1753
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView()V

    goto :goto_0
.end method
