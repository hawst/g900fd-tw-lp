.class public final enum Lcom/sec/android/app/myfiles/utils/ImageCacheManager$CacheType;
.super Ljava/lang/Enum;
.source "ImageCacheManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/utils/ImageCacheManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CacheType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/myfiles/utils/ImageCacheManager$CacheType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/myfiles/utils/ImageCacheManager$CacheType;

.field public static final enum DISK:Lcom/sec/android/app/myfiles/utils/ImageCacheManager$CacheType;

.field public static final enum MEMORY:Lcom/sec/android/app/myfiles/utils/ImageCacheManager$CacheType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 30
    new-instance v0, Lcom/sec/android/app/myfiles/utils/ImageCacheManager$CacheType;

    const-string v1, "DISK"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/ImageCacheManager$CacheType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/ImageCacheManager$CacheType;->DISK:Lcom/sec/android/app/myfiles/utils/ImageCacheManager$CacheType;

    new-instance v0, Lcom/sec/android/app/myfiles/utils/ImageCacheManager$CacheType;

    const-string v1, "MEMORY"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/myfiles/utils/ImageCacheManager$CacheType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/ImageCacheManager$CacheType;->MEMORY:Lcom/sec/android/app/myfiles/utils/ImageCacheManager$CacheType;

    .line 29
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/myfiles/utils/ImageCacheManager$CacheType;

    sget-object v1, Lcom/sec/android/app/myfiles/utils/ImageCacheManager$CacheType;->DISK:Lcom/sec/android/app/myfiles/utils/ImageCacheManager$CacheType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/myfiles/utils/ImageCacheManager$CacheType;->MEMORY:Lcom/sec/android/app/myfiles/utils/ImageCacheManager$CacheType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/myfiles/utils/ImageCacheManager$CacheType;->$VALUES:[Lcom/sec/android/app/myfiles/utils/ImageCacheManager$CacheType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/myfiles/utils/ImageCacheManager$CacheType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 29
    const-class v0, Lcom/sec/android/app/myfiles/utils/ImageCacheManager$CacheType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/utils/ImageCacheManager$CacheType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/myfiles/utils/ImageCacheManager$CacheType;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ImageCacheManager$CacheType;->$VALUES:[Lcom/sec/android/app/myfiles/utils/ImageCacheManager$CacheType;

    invoke-virtual {v0}, [Lcom/sec/android/app/myfiles/utils/ImageCacheManager$CacheType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/myfiles/utils/ImageCacheManager$CacheType;

    return-object v0
.end method
