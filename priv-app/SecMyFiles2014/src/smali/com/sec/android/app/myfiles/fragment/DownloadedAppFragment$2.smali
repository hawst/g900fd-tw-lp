.class Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$2;
.super Ljava/lang/Object;
.source "DownloadedAppFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnTwMultiSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field dragSelectEnable:Z

.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)V
    .locals 1

    .prologue
    .line 292
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 294
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$2;->dragSelectEnable:Z

    return-void
.end method


# virtual methods
.method public OnTwMultiSelectStart(II)V
    .locals 4
    .param p1, "startX"    # I
    .param p2, "startY"    # I

    .prologue
    const/4 v3, 0x1

    .line 322
    const/4 v0, 0x0

    const-string v1, "DownloadedAppFragment"

    const-string v2, "MultiSelect Start = "

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 324
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->getSelectionType()I

    move-result v0

    if-nez v0, :cond_0

    .line 333
    :goto_0
    return-void

    .line 327
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->setEnableDragBlock(Z)V

    .line 329
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setEnableDragBlock(Z)V

    .line 331
    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$2;->dragSelectEnable:Z

    goto :goto_0
.end method

.method public OnTwMultiSelectStop(II)V
    .locals 6
    .param p1, "endX"    # I
    .param p2, "endY"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 338
    const-string v2, "DownloadedAppFragment"

    const-string v3, "MultiSelect end = "

    invoke-static {v4, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 340
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$2;->dragSelectEnable:Z

    if-eqz v2, :cond_3

    .line 342
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->isSelectMode()Z

    move-result v2

    if-nez v2, :cond_0

    .line 344
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    const/4 v3, -0x1

    invoke-virtual {v2, v5, v3, v4}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->startSelectMode(III)V

    .line 348
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mTwDragSelectedItemMap:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 349
    .local v0, "i":I
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mTwDragSelectedItemMap:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Ljava/util/HashMap;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v5, :cond_1

    .line 350
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->selectItem(I)V

    goto :goto_0

    .line 353
    .end local v0    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mTwDragSelectedItemMap:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 355
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->notifyDataSetChanged()V

    .line 356
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$2;->dragSelectEnable:Z

    .line 359
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_3
    return-void
.end method

.method public onTwMultiSelected(Landroid/widget/AdapterView;Landroid/view/View;IJZZZ)V
    .locals 5
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .param p6, "isShiftpress"    # Z
    .param p7, "isCtrlpress"    # Z
    .param p8, "isPentpress"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJZZZ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 298
    const-string v0, "DownloadedAppFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTwMultiSelected call position = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  PenPress is = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 300
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;

    if-eqz v0, :cond_1

    .line 302
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v0, Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/SelectorActivity;->getmSelectionType()I

    move-result v0

    if-nez v0, :cond_0

    .line 317
    :goto_0
    return-void

    .line 307
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    const/4 v1, -0x1

    invoke-virtual {v0, v4, v1, v3}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->startSelectMode(III)V

    .line 312
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mTwDragSelectedItemMap:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 313
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mTwDragSelectedItemMap:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Ljava/util/HashMap;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mTwDragSelectedItemMap:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 315
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mTwDragSelectedItemMap:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
