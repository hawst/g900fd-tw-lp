.class Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$6;
.super Ljava/lang/Object;
.source "FileBrowserFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->onCreateItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)V
    .locals 0

    .prologue
    .line 1524
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 10
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1529
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v4, v4, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v4, :cond_0

    .line 1530
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v4, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v4, v4, Lcom/sec/android/app/myfiles/MainActivity;->mIsSearchMode:Z

    if-eqz v4, :cond_0

    move v4, v5

    .line 1594
    :goto_0
    return v4

    .line 1535
    :cond_0
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 1537
    .local v0, "cursor":Landroid/database/Cursor;
    const/4 v3, 0x0

    .line 1539
    .local v3, "path":Ljava/lang/String;
    if-eqz v0, :cond_7

    .line 1541
    const-string v4, "_data"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1543
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1545
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_2

    .line 1547
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v7, 0x7f0b005a

    invoke-static {v4, v7, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 1549
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1551
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 1554
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)Landroid/os/Handler;

    move-result-object v4

    const-wide/16 v8, 0x1f4

    invoke-virtual {v4, v5, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1557
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->isSelectMode()Z

    move-result v4

    if-nez v4, :cond_4

    .line 1559
    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isInternalRootFolder(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_7

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isExternalRootFolder(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_7

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isUsbStorageRootFolder(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_7

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxRootFolder(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 1562
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v4, v6, p3, v5}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->startSelectMode(III)V

    .line 1566
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1568
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mCurrentListableView:Lcom/sec/android/app/myfiles/view/IDirectDragable;

    invoke-interface {v4, v6}, Lcom/sec/android/app/myfiles/view/IDirectDragable;->setDirectDragMode(Z)V

    :goto_1
    move v4, v6

    .line 1577
    goto/16 :goto_0

    .line 1574
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mCurrentListableView:Lcom/sec/android/app/myfiles/view/IDirectDragable;

    invoke-interface {v4, v5}, Lcom/sec/android/app/myfiles/view/IDirectDragable;->setDirectDragMode(Z)V

    goto :goto_1

    .line 1582
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemPosition()Ljava/util/ArrayList;

    move-result-object v4

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1584
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->isShowTreeView()Z

    move-result v4

    if-nez v4, :cond_6

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v4

    if-nez v4, :cond_6

    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mSideSyncControl:Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v4}, Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;->isKMSRunning(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_6

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mSideSyncControl:Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;

    invoke-static {}, Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;->isPSSRunning()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1586
    :cond_6
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1587
    .local v2, "mBundle":Landroid/os/Bundle;
    const-string v4, "ITEM_INITIATING_DRAG_PATH"

    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1588
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v4, p2, v2}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->startDragMode(Landroid/view/View;Landroid/os/Bundle;)V

    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "mBundle":Landroid/os/Bundle;
    :cond_7
    move v4, v5

    .line 1594
    goto/16 :goto_0
.end method
