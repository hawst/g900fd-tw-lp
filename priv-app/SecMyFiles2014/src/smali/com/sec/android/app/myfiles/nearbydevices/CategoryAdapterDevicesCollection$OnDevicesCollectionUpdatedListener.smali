.class public interface abstract Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;
.super Ljava/lang/Object;
.source "CategoryAdapterDevicesCollection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnDevicesCollectionUpdatedListener"
.end annotation


# virtual methods
.method public abstract notifyDataSetChanged()V
.end method

.method public abstract removeItem(Lcom/sec/android/app/myfiles/element/ShortcutItem;)V
.end method

.method public abstract setDevicesCollection(Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;)V
.end method

.method public abstract setNearbyDevicesProvider(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)V
.end method

.method public abstract showDialog(I)V
.end method

.method public abstract showToastForScanningNearbyDevices(Z)V
.end method
