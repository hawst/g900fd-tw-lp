.class public Lcom/sec/android/app/myfiles/utils/Constant;
.super Ljava/lang/Object;
.source "Constant.java"


# static fields
.field public static final ACTIONBAR_LOCK_BIG_FILE:I = 0x3

.field public static final ACTIONBAR_LOCK_SMALL_FILE:I = 0x4

.field public static final ACTIONBAR_UNLOCK_BIG_FILE:I = 0x7

.field public static final ACTIONBAR_UNLOCK_SMALL_FILE:I = 0x8

.field public static final ACTION_BAR_ID_ALL_FILE_BROWSER:I = 0x3

.field public static final ACTION_BAR_ID_ALL_FILE_BROWSER_SELECT:I = 0x4

.field public static final ACTION_BAR_ID_CATEGORY_BROWSER:I = 0x5

.field public static final ACTION_BAR_ID_CATEGORY_BROWSER_SELECT:I = 0x6

.field public static final ACTION_BAR_ID_CATEGORY_FRAGMENT:I = 0x8

.field public static final ACTION_BAR_ID_CATEGORY_HOME:I = 0x1

.field public static final ACTION_BAR_ID_CATEGORY_HOME_DELETE:I = 0x2

.field public static final ACTION_BAR_ID_DEFAULT:I = 0x0

.field public static final ACTION_BAR_ID_SEARCH:I = 0x9

.field public static final ACTION_BAR_ID_SELECT_MODE:I = 0x7

.field public static final ACTION_EASY_MODE_CHANGE:Ljava/lang/String; = "com.android.launcher.action.EASY_MODE_CHANGE"

.field public static final ACTION_FTP_BACKGROUND_BROWSE_DONE:Ljava/lang/String; = "com.sec.android.app.myfiles.ftp.BACKGROUND_BROWSE_DONE"

.field public static final ACTION_KNOX_CONTAINERAGENT_TO_KLMS_CREATED_B2C:Ljava/lang/String; = "com.sec.knox.containeragent.klms.created.b2c"

.field public static final ACTION_KNOX_CONTAINERAGENT_TO_KLMS_REMOVED_B2C:Ljava/lang/String; = "com.sec.knox.containeragent.klms.removed.b2c"

.field public static final ACTION_KNOX_FILE_RELAY_CANCEL:Ljava/lang/String; = "com.sec.knox.container.FileRelayCancel"

.field public static final ACTION_KNOX_FILE_RELAY_COMPLETE:Ljava/lang/String; = "com.sec.knox.container.FileRelayComplete"

.field public static final ACTION_KNOX_FILE_RELAY_DONE:Ljava/lang/String; = "com.sec.knox.container.FileRelayDone"

.field public static final ACTION_KNOX_FILE_RELAY_ERROR:Ljava/lang/String; = "com.sec.knox.container.FileRelayError"

.field public static final ACTION_KNOX_FILE_RELAY_EXIST:Ljava/lang/String; = "com.sec.knox.container.FileRelayExist"

.field public static final ACTION_KNOX_FILE_RELAY_FAIL:Ljava/lang/String; = "com.sec.knox.container.FileRelayFail"

.field public static final ACTION_KNOX_FILE_RELAY_PROGRESS:Ljava/lang/String; = "com.sec.knox.container.FileRelayProgress"

.field public static final ACTION_KNOX_FILE_RELAY_REQUEST:Ljava/lang/String; = "com.sec.knox.container.FileRelayRequest"

.field public static final ACTION_MEDIA_SCAN:Ljava/lang/String; = "android.intent.action.MEDIA_SCAN"

.field public static final ACTION_MULTIWINDOW_NOTIFY_MULTIWINDOW_STATUS:Ljava/lang/String; = "com.sec.android.action.NOTIFY_MULTIWINDOW_STATUS"

.field public static final ACTION_MYFILES_FILE_OPERATION_DONE:Ljava/lang/String; = "com.sec.android.action.FILE_OPERATION_DONE"

.field public static final ACTION_MYFILES_RUN_FROM_SHORTCUT:Ljava/lang/String; = "com.sec.android.app.myfiles.RUN_FROM_SHORTCUT"

.field public static final ACTION_MYFILES_SELECT_FINISHED:Ljava/lang/String; = "com.sec.android.app.myfiles.PICK_DATA_FINISHED"

.field public static final ACTION_MYFILES_SHORTCUT_ACTION:Ljava/lang/String; = "com.sec.android.action.SHORTCUT_ACTION"

.field public static final ACTION_MYFILES_SORTBY_CHANGED:Ljava/lang/String; = "com.sec.android.app.myfiles.SORTBY_CHANGED"

.field public static final ACTION_RENAME_DONE:Ljava/lang/String; = "rename_done"

.field public static final ADD_FTPS_TAG:Ljava/lang/String; = "add_ftps"

.field public static final ADD_FTP_TAG:Ljava/lang/String; = "add_ftp"

.field public static final ADD_SFTP_TAG:Ljava/lang/String; = "add_sftp"

.field public static final ADD_SHORTCUT_ON_HOMESCREEN_TAG:Ljava/lang/String; = "add_shortcut_on_homescreen"

.field public static final ADD_SHORTCUT_RESULT_TAG:Ljava/lang/String; = "add_shortcut_tag"

.field public static final ADD_TO_HOME:I = 0x1

.field public static final ADD_TO_MY_FILES:I = 0x0

.field public static final ALL_FILES:I = 0x201

.field public static final ALL_FILES_TAG:Ljava/lang/String; = "all_files"

.field public static final ASC:I = 0x0

.field public static final AddFTP:I = 0xd

.field public static final AddFTPS:I = 0xe

.field public static final AddSFTP:I = 0xf

.field public static final BACK_KEY_INTENT:Ljava/lang/String; = "back_key_intent"

.field public static final BAIDU:I = 0x1f

.field public static final BAIDU_CACHE_FOLDER:Ljava/lang/String; = "/storage/sdcard0/Android/data/com.baidu.netdisk/files/scratch"

.field public static final BAIDU_TAG:Ljava/lang/String; = "baidu"

.field public static final BAIDU_THUMBNAIL_FOLDER:Ljava/lang/String; = "/storage/sdcard0/Android/data/com.baidu.netdisk/cache/thumbs"

.field public static final BIG_FILE_SIZE:J = 0x1312d00L

.field public static final CASH_DOWNLOAD:Ljava/lang/String; = "cacheDownload"

.field public static final CATEGORY_HOME:I = 0x0

.field public static final CATEGORY_HOME_TAG:Ljava/lang/String; = "category_home"

.field public static final CATEGORY_SIZE_GIGABYTE:I = 0x38400021

.field public static final CATEGORY_SIZE_KILOBYTE:I = 0x386

.field public static final CATEGORY_SIZE_MEGABYTE:I = 0xe1002

.field public static final CATEGORY_SIZE_SINGLE_BYTE:I = 0x1

.field public static final CATEGORY_SIZE_SINGLE_GIGABYTE_END:I = 0x4051ebc0

.field public static final CATEGORY_SIZE_SINGLE_GIGABYTE_START:I = 0x3fae145f

.field public static final CATEGORY_SIZE_SINGLE_KILOBYTE_END:I = 0x406

.field public static final CATEGORY_SIZE_SINGLE_KILOBYTE_START:I = 0x3fa

.field public static final CATEGORY_SIZE_SINGLE_MEGABYTE_END:I = 0x10147b

.field public static final CATEGORY_SIZE_SINGLE_MEGABYTE_START:I = 0xfeb85

.field public static final CATEGORY_TYPE:Ljava/lang/String; = "category_type"

.field public static final CLEAR_HISTORICALPATH:Ljava/lang/String; = "CLEAR_HISTORICALPATH"

.field public static final COMPRESS_FILE_OPERATION:Ljava/lang/String; = "compress"

.field public static final CONTEXT_LOCK_BIG_FILE:I = 0x1

.field public static final CONTEXT_LOCK_SMALL_FILE:I = 0x2

.field public static final CONTEXT_UNLOCK_BIG_FILE:I = 0x5

.field public static final CONTEXT_UNLOCK_SMALL_FILE:I = 0x6

.field public static final COPY:I = 0x0

.field public static final COPY_BUFFER_SIZE:I = 0x2000

.field public static final COPY_MOVE_IN_PRIVATE:I = 0xf

.field public static final CREATE_FOLDER:I = 0x8

.field public static final CREATE_FOLDER_OPERATION:Ljava/lang/String; = "createFolder"

.field public static final CURRENT_FOLDER:Ljava/lang/String; = "current_folder"

.field public static final CURRENT_ITEM:Ljava/lang/String; = "current_item"

.field public static final CURRENT_OPERATION:Ljava/lang/String; = "kind_of_operation"

.field public static final DEBUG:Z = true

.field public static final DEFAULT_DOWNLOAD_DIR:Ljava/lang/String;

.field public static final DEFAULT_TEMP_DOWNLOAD_DIR:Ljava/lang/String;

.field public static final DEFAULT_TEMP_UPLOAD_DIR:Ljava/lang/String;

.field public static final DELETE:I = 0x2

.field public static final DELETE_FILE_OPERATION:Ljava/lang/String; = "delete"

.field public static final DELETE_OLD_THUMBNAIL_COUNT:I = 0x32

.field public static final DESC:I = 0x1

.field public static final DEST_FRAGMENT_ID:Ljava/lang/String; = "dest_fragment_id"

.field public static final DETAILS:I = 0xa

.field public static final DETAIL_FILES_COUNT:Ljava/lang/String; = "detail_files_count"

.field public static final DETAIL_FOLDERS_COUNT:Ljava/lang/String; = "detail_folder_count"

.field public static final DETAIL_ITEMS_SIZE:Ljava/lang/String; = "detail_items_size"

.field public static final DETAIL_ITEM_FORMAT:Ljava/lang/String; = "detail_item_format"

.field public static final DETAIL_ITEM_INDEX:Ljava/lang/String; = "detail_item_index"

.field public static final DETAIL_ITEM_PATH:Ljava/lang/String; = "detail_item_path"

.field public static final DIALOG_ADD_SHORTCUT:I = 0x8

.field public static final DIALOG_CLEAR_HISTORY:I = 0xd

.field public static final DIALOG_CONFIRM_DELETE:I = 0x2

.field public static final DIALOG_FTP_MODE:I = 0xc

.field public static final DIALOG_MOVE_TO_KNOX:I = 0x6

.field public static final DIALOG_REMOVE_FROM_KNOX:I = 0x7

.field public static final DIALOG_SORT_BY:I = 0x1

.field public static final DIALOG_VIEW_BY:I = 0x0

.field public static final DIALOG_WARNING_MOBILE_DATA_BAIDU:I = 0x5

.field public static final DIALOG_WARNING_MOBILE_DATA_BAIDU_WLANON:I = 0xb

.field public static final DIALOG_WARNING_MOBILE_DATA_DROPBOX:I = 0x3

.field public static final DIALOG_WARNING_MOBILE_DATA_FTP:I = 0x4

.field public static final DIALOG_WARNING_MOBILE_DATA_FTP_WLANON:I = 0xa

.field public static final DIALOG_WARNING_MOBILE_DATA_NEARBYDEVICES_WLANON:I = 0x9

.field public static final DOCUMENTS:I = 0x5

.field public static final DOCUMENTS_TAG:Ljava/lang/String; = "documents"

.field public static final DONE_FILE_LIST:Ljava/lang/String; = "done_file_list"

.field public static final DONE_FOLDER_LIST:Ljava/lang/String; = "done_folder_list"

.field public static final DOWNLOAD:I = 0xc

.field public static final DOWNLOADED_APPS:I = 0x6

.field public static final DOWNLOADED_APPS_TAG:Ljava/lang/String; = "downloaded_apps"

.field public static final DRAG_DROP_Cloud:Ljava/lang/String; = "selectedCloudUri"

.field public static final DRAG_DROP_FTP:Ljava/lang/String; = "selectedFTPUri"

.field public static final DRAG_DROP_GALLERY_LABEL:Ljava/lang/String; = "galleryURI"

.field public static final DRAG_DROP_LABEL:Ljava/lang/String; = "selectedUri"

.field public static final DRAG_SUCCESS:Ljava/lang/String; = "success"

.field public static final DROPBOX:I = 0x8

.field public static final DROPBOX_CACHE_FOLDER:Ljava/lang/String; = "/storage/sdcard0/Android/data/com.dropbox.android/files/scratch"

.field public static final DROPBOX_FOLDER:Ljava/lang/String; = "Dropbox/"

.field public static final DROPBOX_MAX_SYNC_CNT_PER_ONCE:I = 0xfa0

.field public static final DROPBOX_SUBMIT_CNT_PER_ONCE:I = 0x7d0

.field public static final DROPBOX_TAG:Ljava/lang/String; = "dropbox"

.field public static final DROPBOX_THUMBNAIL_FILE_NAME:Ljava/lang/String; = "large.jpg"

.field public static final DROPBOX_THUMBNAIL_FOLDER:Ljava/lang/String; = "/storage/sdcard0/Android/data/com.dropbox.android/cache/thumbs"

.field public static final DST_FTP_PARAMS:Ljava/lang/String; = "dst-ftp-param"

.field public static final EMPTY_SHORTCUT:I = 0x1b

.field public static final ERR_FOLDER_NOT_EXIST:I = 0x0

.field public static final ERR_NONE:I = -0x1

.field public static final ERR_OK:I = 0x0

.field public static final EXTERNAL_SD:I = 0x202

.field public static final EXTERNAL_SECRETBOX:I = 0x9

.field public static final EXTERNAL_STORAGE_ID:I = 0x20001

.field public static final EXTERNAL_USB_DRIVE_A:I = 0x603

.field public static final EXTERNAL_USB_DRIVE_B:I = 0x604

.field public static final EXTERNAL_USB_DRIVE_C:I = 0x605

.field public static final EXTERNAL_USB_DRIVE_D:I = 0x606

.field public static final EXTERNAL_USB_DRIVE_E:I = 0x607

.field public static final EXTERNAL_USB_DRIVE_F:I = 0x608

.field public static final EXTRACT:I = 0x4

.field public static final EXTRACT_FILE_OPERATION:Ljava/lang/String; = "extract"

.field public static final EXTRACT_HERE:I = 0xb

.field public static final FEATURE_INSERT_LOG_ADD_FTP:Ljava/lang/String; = "FTPA"

.field public static final FEATURE_INSERT_LOG_CONNECTION_FTP:Ljava/lang/String; = "FTPC"

.field public static final FEATURE_INSERT_LOG_DROPBOX:Ljava/lang/String; = "DROP"

.field public static final FEATURE_INSERT_LOG_HOVER_DIALOG_OPTION:Ljava/lang/String; = "HOPT"

.field public static final FEATURE_INSERT_LOG_MOVE_TO_PRIVATE:Ljava/lang/String; = "PRIV"

.field public static final FEATURE_INSERT_LOG_NEARBYDEVICE:Ljava/lang/String; = "NEAR"

.field public static final FEATURE_INSERT_LOG_THUMBNAILVIEW:Ljava/lang/String; = "THUM"

.field public static final FILE:I = 0x1

.field public static final FILE_ALREADY_EXISTS:I = 0x7955

.field public static final FILE_NAME:Ljava/lang/String; = "file_name"

.field public static final FILE_OPERATION:Ljava/lang/String; = "FILE_OPERATION"

.field public static final FILE_OPERATION_PRIVATE:Ljava/lang/String; = "FILE_OPERATION_PRIVATE"

.field public static final FOLDER:I = 0x0

.field public static final FOLDER_AND_FILE:I = 0x2

.field public static final FOLDER_FORMAT:I = 0x3001

.field public static final FRAGMENT_ID:Ljava/lang/String; = "id"

.field public static final FROM_ADD_SHORTCUT:Ljava/lang/String; = "from_add_shortcut"

.field public static final FROM_BACK_PRESS:Ljava/lang/String; = "from_back"

.field public static final FROM_FINDO:Ljava/lang/String; = "from_findo_file_position"

.field public static final FROM_FINDO_IS_FOLDER:Ljava/lang/String; = "from_findo_is_folder"

.field public static final FROM_HOME_SHORTCUT:Ljava/lang/String; = "from_home_shortcut"

.field public static final FROM_PERSONAL_WIDGET:Ljava/lang/String; = "from_personal_widget"

.field public static final FROM_WATCH_FOLDER:Ljava/lang/String; = "FROM_WATCHFOLDER"

.field public static final FTP:I = 0xa

.field public static final FTPS:I = 0xb

.field public static final FTPS_TAG:Ljava/lang/String; = "ftps"

.field public static final FTP_CACHE_FOLDER:Ljava/lang/String; = "/storage/sdcard0/Android/data/com.sec.android.app.myfiles/ftp/cache"

.field public static final FTP_LIST:I = 0x11

.field public static final FTP_LIST_TAG:Ljava/lang/String; = "ftp_list"

.field public static final FTP_NO_TIMESTAMP:Ljava/lang/String; = "no_tstmp"

.field public static final FTP_SHORTCUT:I = 0x1c

.field public static final FTP_SHORTCUTS:I = 0x10

.field public static final FTP_TAG:Ljava/lang/String; = "ftp"

.field public static final FTP_TYPE:Ljava/lang/String; = "ftp_type"

.field public static final GET_THUMBNAIL_TIME:I = 0xe4e1c0

.field public static final GOLF_TEMP_VIDEO_PATH:Ljava/lang/String;

.field public static final HIDE_STORAGE:Ljava/lang/String; = "hide_storage"

.field public static final HISTORY:I = 0x28

.field public static final HISTORY_TAG:Ljava/lang/String; = "history"

.field public static final IMAGES:I = 0x2

.field public static final IMAGES_TAG:Ljava/lang/String; = "images"

.field public static final INDEX:Ljava/lang/String; = "index"

.field public static final INPUT_DIALOG_FRAGMENT_RESULT:Ljava/lang/String; = "result_value"

.field public static final INTERNAL:I = 0x101

.field public static final INTERNAL_STORAGE_ID:I = 0x10001

.field public static final INVALID_CHAR:[Ljava/lang/String;

.field public static final IS_FROM_SEARCH_INTENT_KEY:Ljava/lang/String; = "is_from_search_intent_key"

.field public static final IS_SELECTOR:Ljava/lang/String; = "ISSELECTOR"

.field public static final IS_SHORTCUT_FOLDER:Ljava/lang/String; = "IS_SHORTCUT_FOLDER"

.field public static final ITEM_INITIATING_DRAG_PATH:Ljava/lang/String; = "ITEM_INITIATING_DRAG_PATH"

.field public static final LANDSCAPE:I = 0x1

.field public static final LANDSCAPE_MULTIWINDOW:I = 0x3

.field public static final LEFT_PANEL_FIRST:I = 0x0

.field public static final LEFT_PANEL_LAST:I = 0x9

.field public static final LIMIT_HISTORICAL_PATH:I = 0x14

.field public static final LIMIT_RECENTLY_FILES:I = 0xa

.field public static final LIST:I = 0x0

.field public static final LIST_AND_DETAILS:I = 0x1

.field public static final LOCK:I = 0x17

.field public static final MASK_EXTERNAL_STORAGE:I = 0x200

.field public static final MASK_ID:I = 0xff

.field public static final MASK_INTERNAL_STORAGE:I = 0x100

.field public static final MASK_USB_OTG_STORAGE:I = 0x400

.field public static final MASK_USING_NAVIGATION:I = 0x200

.field public static final MASK_USING_NETWORK:I = 0x100

.field public static final MAX_FOLDER_NAME_LENGTH:I = 0x32

.field public static final MAX_PATH_LENGTH:I = 0xff

.field public static final MAX_SHORTCUTS_COUNT:I = 0x32

.field public static final MAX_THUMBNAIL_COUNT:I = 0x96

.field public static final MICRO_HEIGHT:I = 0x8c

.field public static final MICRO_WIDTH:I = 0xbe

.field public static final MIN_CLICK_INTERVAL:J = 0x2eeL

.field public static final MOVE:I = 0x1

.field public static final MOVE_TO_PRIVATE:I = 0xd

.field public static final MOVE_TO_PRIVATE_MAX_PATH_LENGTH:I = 0xb0

.field public static final MOVE_TO_SECRET:I = 0x5

.field public static final MSG_CONVERT_DONE:I = 0x6

.field public static final MSG_DISMISS_PROGRESS_DIALOG:I = 0x1

.field public static final MSG_REFRESH_FRAGMENT:I = 0x2

.field public static final MSG_REFRESH_HOME_FRAGMENT:I = 0x3

.field public static final MSG_REFRESH_SEARCH:I = 0x4

.field public static final MSG_REFRESH_TREEVIEW:I = 0x5

.field public static final MSG_SHOW_PROGRESS_DIALOG:I = 0x0

.field public static final MSG_UNREGISTER_RECEIVER:I = 0x7

.field public static final MUSIC:I = 0x4

.field public static final MUSIC_TAG:Ljava/lang/String; = "music"

.field public static final MYFILES_ACTION_PICK_DATA:Ljava/lang/String; = "com.sec.android.app.myfiles.PICK_DATA"

.field public static final MYFILES_ACTION_PICK_DATA_DOWNLOAD:Ljava/lang/String; = "com.sec.android.app.myfiles.PICK_DATA_DOWNLOAD"

.field public static final MYFILES_ACTION_PICK_DATA_MULTIPLE:Ljava/lang/String; = "com.sec.android.app.myfiles.PICK_DATA_MULTIPLE"

.field public static final MYFILES_ACTION_PICK_SELECT_PATH:Ljava/lang/String; = "com.sec.android.app.myfiles.PICK_SELECT_PATH"

.field public static final MYFILES_PACKAGE:Ljava/lang/String; = "com.sec.android.app.myfiles"

.field public static final NAME:I = 0x2

.field public static final NEARBY_DEVICES:I = 0x9

.field public static final NEARBY_DEVICES_PROVIDER_ID:Ljava/lang/String; = "provider_id"

.field public static final NEARBY_DEVICES_PROVIDER_NAME:Ljava/lang/String; = "provider_name"

.field public static final NEARBY_DEVICES_SERVICE_MEDIA:Ljava/lang/String; = "com.samsung.android.allshare.media"

.field public static final NEARBY_DEVICES_TAG:Ljava/lang/String; = "nearby_devices"

.field public static final NONE:I = -0x1

.field public static final NONE_RESOURCE:I = -0x1

.field public static final NO_NETWORK:I = 0x13

.field public static final NO_NETWORK_TAG:Ljava/lang/String; = "no_network"

.field public static final OPERATION_RESULT:Ljava/lang/String; = "OPERATION_RESULT"

.field public static final PERMISSION_FILE_OPERATION_DONE:Ljava/lang/String; = "com.sec.android.app.myfiles.permission.FILE_OPERATION_DONE"

.field public static final PORTRAIT:I = 0x0

.field public static final PORTRAIT_MULTIWINDOW:I = 0x2

.field public static final RECENTLY_FILES:I = 0x7

.field public static final RECENTLY_FILES_TAG:Ljava/lang/String; = "recently_files"

.field public static final REMOTE_DEVICE_FILE_AUDIO_TYPE:I = 0x2

.field public static final REMOTE_DEVICE_FILE_IMAGE_TYPE:I = 0x3

.field public static final REMOTE_DEVICE_FILE_UNKNOWN_TYPE:I = 0x0

.field public static final REMOTE_DEVICE_FILE_VIDEO_TYPE:I = 0x4

.field public static final REMOTE_DEVICE_FOLDER_TYPE:I = 0x1

.field public static final REMOTE_SHARE:I = 0x25

.field public static final REMOTE_SHARE_DIR:Ljava/lang/String; = "REMOTE_SHARE_DIR"

.field public static final REMOTE_SHARE_FOLDER_ID:Ljava/lang/String; = "REMOTE_SHARE_ID"

.field public static final REMOTE_SHARE_INBOX:I = 0x26

.field public static final REMOTE_SHARE_OUTBOX:I = 0x27

.field public static final REMOTE_SHARE_SERVICE_ID:I = 0x2

.field public static final REMOTE_SHARE_TAG:Ljava/lang/String; = "remote_share"

.field public static final REMOTE_SHARE_VIEWER:Ljava/lang/String; = "remote_share_viewer"

.field public static final REMOVE_FROM_PRIVATE:I = 0xe

.field public static final REMOVE_FROM_SECRET:I = 0x6

.field public static final RENAME:I = 0x7

.field public static final RENAME_SHORTCUT_REQUEST_INTENT_KEY:Ljava/lang/String; = "rename_shortcut_request_intent_key"

.field public static final REQUEST_ADD_FTP:I = 0x8

.field public static final REQUEST_ADD_FTPS:I = 0x9

.field public static final REQUEST_ADD_SFTP:I = 0xa

.field public static final REQUEST_ADD_SHORTCUT:I = 0x3

.field public static final REQUEST_ADD_SHORTCUT_ON_HOMESCREEN:I = 0x14

.field public static final REQUEST_CREATE_FOLDER:I = 0x4

.field public static final REQUEST_DROPBOX_SHAREVIA:I = 0x11

.field public static final REQUEST_DROP_FILE_OPERATION:I = 0x12

.field public static final REQUEST_EXTRACT:I = 0x6

.field public static final REQUEST_FILES_DOWNLOAD_DROPBOX_SHARE:I = 0xf

.field public static final REQUEST_FILES_UPLOAD:I = 0xc

.field public static final REQUEST_FILES_UPLOAD_WITH_CLEAN:I = 0xe

.field public static final REQUEST_FILE_DOWNLOAD:I = 0xb

.field public static final REQUEST_FILE_OPERATION:I = 0x2

.field public static final REQUEST_FROM_TAG:Ljava/lang/String; = "from"

.field public static final REQUEST_MOVE_TO_KNOX:I = 0x15

.field public static final REQUEST_REMOTE_SHARE_DOWNLOAD:I = 0x1b

.field public static final REQUEST_REMOVE_FROM_KNOX:I = 0x16

.field public static final REQUEST_RENAME_ITEM:I = 0x13

.field public static final REQUEST_SELECT_PATH_FOR_COPY:I = 0x0

.field public static final REQUEST_SELECT_PATH_FOR_MOVE:I = 0x1

.field public static final REQUEST_SETTING:I = 0xd

.field public static final REQUEST_SHOW_ADD_SHORTCUT_DIALOG:I = 0x7

.field public static final REQUEST_TO_TAG:Ljava/lang/String; = "to"

.field public static final REQUEST_USER_SHORTCUT_RENAME:I = 0x10

.field public static final REQUEST_ZIP:I = 0x5

.field public static final RETURN_PATH:Ljava/lang/String; = "RETURNPATH"

.field public static final RSHARE_CONTENT_URI:Landroid/net/Uri;

.field public static final RSHARE_DOWNLOAD_DIR:Ljava/lang/String;

.field public static final RSHARE_DOWNLOAD_DIR_URI:Ljava/lang/String;

.field public static final RUN_FROM:Ljava/lang/String; = "run_from"

.field public static final SDCARD:I = 0x1d

.field public static final SEARCH:I = 0x11

.field public static final SEARCH_ADVANCE:I = 0x22

.field public static final SEARCH_ADVANCE_EXTENTION_INTENT_KEY:Ljava/lang/String; = "search_advance_extention_intent_key"

.field public static final SEARCH_ADVANCE_FRAGMENT_TAG:Ljava/lang/String; = "SearchAdvanceFragment"

.field public static final SEARCH_ADVANCE_FROM_EXTENSION:I = 0x1

.field public static final SEARCH_ADVANCE_FROM_NEARBY:I = 0x2

.field public static final SEARCH_ADVANCE_FROM_NONE:I = -0x1

.field public static final SEARCH_ADVANCE_INTENT_KEY:Ljava/lang/String; = "search_advance_intent_key"

.field public static final SEARCH_ADVANCE_IS_DOWNLOADED_APP_ENABLE:Ljava/lang/String; = "search_advance_is_downloaded_app_enable"

.field public static final SEARCH_ADVANCE_NAME_INTENT_KEY:Ljava/lang/String; = "search_advance_name_intent_key"

.field public static final SEARCH_BAIDU_FRAGMENT:I = 0x20

.field public static final SEARCH_COPY_MOVE_DESTINATION:I = 0x33

.field public static final SEARCH_DATE_CHANGED:Ljava/lang/String; = "SEARCH_DATE_CHANGED"

.field public static final SEARCH_DROPBOX_FRAGMENT:I = 0x14

.field public static final SEARCH_EXTENSION_CHANGED:Ljava/lang/String; = "SEARCH_EXTENSION_CHANGED"

.field public static final SEARCH_EXTENSION_FOCUS:Ljava/lang/String; = "SEARCH_EXTENSION_FOCUS"

.field public static final SEARCH_FILETYPE_CHANGED:Ljava/lang/String; = "SEARCH_FILETYPE_CHANGED"

.field public static final SEARCH_FILE_TYPE_INDEX_INTENT_KEY:Ljava/lang/String; = "search_file_type_index_intent_key"

.field public static final SEARCH_FILE_TYPE_NAME_INTENT_KEY:Ljava/lang/String; = "search_file_type_name_intent_key"

.field public static final SEARCH_FRAGMENT:I = 0x12

.field public static final SEARCH_FROM_DATE_INTENT_KEY:Ljava/lang/String; = "search_from_date_intent_key"

.field public static final SEARCH_KEYWORD_INTENT_KEY:Ljava/lang/String; = "search_keyword_intent_key"

.field public static final SEARCH_LOCATION_CHANGED:Ljava/lang/String; = "SEARCH_LOCATION_CHANGED"

.field public static final SEARCH_NEARBY_DEVICES_FRAGMENT:I = 0x13

.field public static final SEARCH_OPTIONS_ITEM_PATH:Ljava/lang/String; = "search_options_item_path"

.field public static final SEARCH_OPTION_TYPE:Ljava/lang/String; = "search_option_type"

.field public static final SEARCH_OPTION_TYPE_ADAVANCED:I = 0x6

.field public static final SEARCH_OPTION_TYPE_ALL:I = 0x1

.field public static final SEARCH_OPTION_TYPE_DATE:I = 0x4

.field public static final SEARCH_OPTION_TYPE_EXTENSION:I = 0x5

.field public static final SEARCH_OPTION_TYPE_FILE_TYPE:I = 0x3

.field public static final SEARCH_OPTION_TYPE_LOCATION:I = 0x2

.field public static final SEARCH_SET_LOCATION_INTENT_KEY:Ljava/lang/String; = "search_set_location_intent_key"

.field public static final SEARCH_SET_LOCATION_IS_NEARR_BY_DEVICE_INDEX:Ljava/lang/String; = "search_set_location_is_near_by_device_index"

.field public static final SEARCH_SET_LOCATION_IS_NEARR_BY_DEVICE_INTENT_KEY:Ljava/lang/String; = "search_set_location_is_near_by_device_intent_key"

.field public static final SEARCH_TAG:Ljava/lang/String; = "search"

.field public static final SEARCH_TO_DATE_INTENT_KEY:Ljava/lang/String; = "search_to_date_intent_key"

.field public static final SECRETBOX:I = 0x1f

.field public static final SECRETBOX_ACTION_MODE_OFF:Ljava/lang/String; = "com.samsung.android.intent.action.PRIVATE_MODE_OFF"

.field public static final SECRETBOX_ACTION_MODE_ON:Ljava/lang/String; = "com.samsung.android.intent.action.PRIVATE_MODE_ON"

.field public static final SECRETBOX_STORAGE_ID:I = 0x30001

.field public static final SELECTOR:I = 0x15

.field public static final SELECTOR_ALL_FILE:I = 0x16

.field public static final SELECTOR_CATEGORY_TYPE:Ljava/lang/String; = "SELECTOR_CATEGORY_TYPE"

.field public static final SELECTOR_DROPBOX:I = 0x17

.field public static final SELECTOR_FREQUENTLY_USED_FOLDER:I = 0x19

.field public static final SELECTOR_FTP_MOVE:Ljava/lang/String; = "SELECTOR_FTP_MOVE"

.field public static final SELECTOR_RECENTLY_USED_FOLDER:I = 0x18

.field public static final SELECTOR_SHOW_FTP:Ljava/lang/String; = "SHOW_FTP"

.field public static final SELECTOR_SOURCE_PATHS:Ljava/lang/String; = "SELECTOR_SOURCE_PATH"

.field public static final SELECTOR_SOURCE_TYPE:Ljava/lang/String; = "SELECTOR_SOURCE_TYPE"

.field public static final SELECTOR_SRC_FTP_PARAMS:Ljava/lang/String; = "SELECTOR_SAME_FTP"

.field public static final SELECTOR_TARGET_PATHS:Ljava/lang/String; = "SELECTOR_TARGET_PATH"

.field public static final SELECT_CHANGE_PIN:I = 0x18

.field public static final SELECT_CONTENT_EXTENSION:Ljava/lang/String; = "CONTENT_EXTENSION"

.field public static final SELECT_CONTENT_TYPE:Ljava/lang/String; = "CONTENT_TYPE"

.field public static final SELECT_EXCEPT_CONTENT_EXTENSION:Ljava/lang/String; = "EXCEPT_CONTENT_EXTENSION"

.field public static final SELECT_EXCEPT_CONTENT_TYPE:Ljava/lang/String; = "EXCEPT_CONTENT_TYPE"

.field public static final SELECT_JUST_SELECT_MODE:Ljava/lang/String; = "JUST_SELECT_MODE"

.field public static final SELECT_LIMIT_COUNT:I = 0x64

.field public static final SELECT_MODE_FROM_AIRBUTTON:I = 0x2

.field public static final SELECT_MODE_FROM_NORMAL:I = 0x0

.field public static final SELECT_MODE_FROM_NORMAL_DELETE:I = 0x5

.field public static final SELECT_MODE_FROM_SELECTOR:I = 0x1

.field public static final SELECT_MODE_FROM_SHORTCUT:I = 0x3

.field public static final SELECT_MODE_FROM_SHORTCUT_DELETE:I = 0x4

.field public static final SELECT_MODE_FROM_SHORTCUT_NORMAL_DELETE:I = 0x6

.field public static final SELECT_MODE_NONE:I = -0x1

.field public static final SELECT_PATH_MODE:Ljava/lang/String; = "SELECT_PATH_MODE"

.field public static final SELECT_REPLY_BY_BROADCAST:Ljava/lang/String; = "reply_by_broadcast"

.field public static final SELECT_RESULT_CODE:Ljava/lang/String; = "RESULT_CODE"

.field public static final SELECT_RESULT_PATH:Ljava/lang/String; = "FILE"

.field public static final SELECT_RESULT_PATH_DOWNLOAD:Ljava/lang/String; = "uri"

.field public static final SELECT_RESULT_PATH_LIST:Ljava/lang/String; = "FILE"

.field public static final SELECT_RESULT_URI:Ljava/lang/String; = "FILE_URI"

.field public static final SELECT_SELECTION_TYPE:Ljava/lang/String; = "selection_type"

.field public static final SELECT_SHOW_FOLDER_FILE_TYPE:Ljava/lang/String; = "show_folder_file_type"

.field public static final SELECT_TYPE_MULTIPLE:I = 0x1

.field public static final SELECT_TYPE_NONE:I = -0x1

.field public static final SELECT_TYPE_PATH:I = 0x2

.field public static final SELECT_TYPE_PATH_DOWNLOAD:I = 0x3

.field public static final SELECT_TYPE_SINGLE:I = 0x0

.field public static final SETTINGS_SHOW_FILE_EXTENSION_KEY:Ljava/lang/String; = "show_file_extension"

.field public static final SETTINGS_SHOW_HIDDEN_FILE_KEY:Ljava/lang/String; = "show_hidden_files"

.field public static final SETTING_SHOW_CHANGE_PIN:Ljava/lang/String; = "change_pin"

.field public static final SET_CHANGE_PIN:I = 0x1a

.field public static final SFTP:I = 0xc

.field public static final SFTP_TAG:Ljava/lang/String; = "sftp"

.field public static final SHARE:I = 0x9

.field public static final SHARED_PREF_DO_NOT_SHOW_WARNING_DROPBOX:Ljava/lang/String; = "dropbox_warning"

.field public static final SHARED_PREF_DO_NOT_SHOW_WARNING_FTP:Ljava/lang/String; = "ftp_warning"

.field public static final SHARED_PREF_DO_NOT_SHOW_WARNING_WLANON:Ljava/lang/String; = "wlanon_warning"

.field public static final SHARED_PREF_KEY_CURRENT_ORDER_BY:Ljava/lang/String; = "current_order_by"

.field public static final SHARED_PREF_KEY_CURRENT_SORT_BY:Ljava/lang/String; = "current_sort_by"

.field public static final SHARED_PREF_KEY_CURRENT_VIEW:Ljava/lang/String; = "current_view"

.field public static final SHARED_PREF_KEY_DOCUMENTS_SIZE:Ljava/lang/String; = "documentssize"

.field public static final SHARED_PREF_KEY_DOWNLOADED_APP_SIZE:Ljava/lang/String; = "downloadedappsize"

.field public static final SHARED_PREF_KEY_DROPBOX_AUTHTOKEN_KEY:Ljava/lang/String; = "dropbox_auth_key"

.field public static final SHARED_PREF_KEY_DROPBOX_AUTHTOKEN_SECRET:Ljava/lang/String; = "dropbox_auth_secret"

.field public static final SHARED_PREF_KEY_EXPANDED_VIEW_ID_IN_SPLITVIEW:Ljava/lang/String; = "expanded_view"

.field public static final SHARED_PREF_KEY_FTP_SHORTCUT_LIST:Ljava/lang/String; = "ftp_shortcut_list"

.field public static final SHARED_PREF_KEY_HOME_FOLDER:Ljava/lang/String; = "home_folder"

.field public static final SHARED_PREF_KEY_LOCK_FILES_DIALOG:Ljava/lang/String; = "lock_files_dialog"

.field public static final SHARED_PREF_KEY_MANAGED_BROWSER_NORMAL_STATE:Ljava/lang/String; = "managed_browser_normal_state"

.field public static final SHARED_PREF_KEY_SEARCH_FILE_TYPE:Ljava/lang/String; = "search_file_type"

.field public static final SHARED_PREF_KEY_SEARCH_FILE_TYPE_ALL:Ljava/lang/String; = "search_file_type_all"

.field public static final SHARED_PREF_KEY_SEARCH_FILE_TYPE_DOCUMENT:Ljava/lang/String; = "search_file_type_doc"

.field public static final SHARED_PREF_KEY_SEARCH_FILE_TYPE_DOWNLOADED_APPS:Ljava/lang/String; = "search_file_type_down"

.field public static final SHARED_PREF_KEY_SEARCH_FILE_TYPE_IMAGES:Ljava/lang/String; = "search_file_type_images"

.field public static final SHARED_PREF_KEY_SEARCH_FILE_TYPE_INDEX:Ljava/lang/String; = "search_file_type_index"

.field public static final SHARED_PREF_KEY_SEARCH_FILE_TYPE_MUSIC:Ljava/lang/String; = "search_file_type_music"

.field public static final SHARED_PREF_KEY_SEARCH_FILE_TYPE_VIDEOS:Ljava/lang/String; = "search_file_type_videos"

.field public static final SHARED_PREF_KEY_SEARCH_FROM_DATE:Ljava/lang/String; = "search_from_date"

.field public static final SHARED_PREF_KEY_SEARCH_LOCATION_BAIDU:Ljava/lang/String; = "search_location_baidu"

.field public static final SHARED_PREF_KEY_SEARCH_LOCATION_DEVICE:Ljava/lang/String; = "search_location_device"

.field public static final SHARED_PREF_KEY_SEARCH_LOCATION_DROPBOX:Ljava/lang/String; = "search_location_dropbox"

.field public static final SHARED_PREF_KEY_SEARCH_LOCATION_NEARBY:Ljava/lang/String; = "search_location_nearby"

.field public static final SHARED_PREF_KEY_SEARCH_LOCATION_PRIVATE:Ljava/lang/String; = "search_location_private"

.field public static final SHARED_PREF_KEY_SEARCH_LOCATION_SDCARD:Ljava/lang/String; = "search_location_sdcard"

.field public static final SHARED_PREF_KEY_SEARCH_LOCATION_USB_A:Ljava/lang/String; = "search_location_usb_a"

.field public static final SHARED_PREF_KEY_SEARCH_LOCATION_USB_B:Ljava/lang/String; = "search_location_usb_b"

.field public static final SHARED_PREF_KEY_SEARCH_LOCATION_USB_C:Ljava/lang/String; = "search_location_usb_c"

.field public static final SHARED_PREF_KEY_SEARCH_LOCATION_USB_D:Ljava/lang/String; = "search_location_usb_d"

.field public static final SHARED_PREF_KEY_SEARCH_LOCATION_USB_E:Ljava/lang/String; = "search_location_usb_e"

.field public static final SHARED_PREF_KEY_SEARCH_LOCATION_USB_F:Ljava/lang/String; = "search_location_usb_f"

.field public static final SHARED_PREF_KEY_SEARCH_TO_DATE:Ljava/lang/String; = "search_to_date"

.field public static final SHARED_PREF_KEY_SET_HOMESCREEN_SHORTCUT_PATH:Ljava/lang/String; = "homescreen_shortcut_path"

.field public static final SHARED_PREF_KEY_SHOW_CATEGORY:Ljava/lang/String; = "show_category"

.field public static final SHARED_PREF_KEY_SHOW_FILE_EXTENSION:Ljava/lang/String; = "show_file_extension"

.field public static final SHARED_PREF_KEY_SHOW_HIDDEN_FILES:Ljava/lang/String; = "show_hidden_files"

.field public static final SHARED_PREF_KEY_SPLITVIEW_POSITION:Ljava/lang/String; = "splitview_position"

.field public static final SHARED_PREF_KEY_USER_SHORTCUT_LIST:Ljava/lang/String; = "user_shortcut_list"

.field public static final SHARED_PREF_NAME:Ljava/lang/String; = "MyFiles"

.field public static final SHORTCUT_CURRENT_POSITION:Ljava/lang/String; = "SHORTCUT_CURRENT_POSITION"

.field public static final SHORTCUT_INDEX_USE_NEXT_AVAILABLE:I = -0x1

.field public static final SHORTCUT_ROOT_FOLDER:Ljava/lang/String; = "SHORTCUT_ROOT_FOLDER"

.field public static final SHORTCUT_SELECTED_MODE_ENABLE_INTENT_KEY:Ljava/lang/String; = "shortcut_mode_enable_intent_key"

.field public static final SHORTCUT_TYPE:Ljava/lang/String; = "shortcut_type"

.field public static final SHORTCUT_WORKING_INDEX_INTENT_KEY:Ljava/lang/String; = "shortcut_working_index_intent_key"

.field public static final SIDESYNC_ACTION_DRAG_DROP_COMPLETED:Ljava/lang/String; = "com.sec.android.sidesync.source.DROP_DRAG"

.field public static final SIDESYNC_ACTION_KMS_DRAG_START:Ljava/lang/String; = "sidesync.app.action.KMS_FILETRANSFER_DRAG_FILEINFO"

.field public static final SIDESYNC_ACTION_PSS_DRAG_START:Ljava/lang/String; = "com.sec.android.sidesync.source.START_DRAG"

.field public static final SIDESYNC_ACTION_SINK_CONNECTED:Ljava/lang/String; = "com.sec.android.sidesync.sink.SIDESYNC_CONNECTED"

.field public static final SIDESYNC_ACTION_SINK_DISCONNECTED:Ljava/lang/String; = "com.sec.android.sidesync.sink.SIDESYNC_DISCONNECT"

.field public static final SIDESYNC_ACTION_SOURCE_CONNECTED:Ljava/lang/String; = "com.sec.android.sidesync.source.SIDESYNC_CONNECTED"

.field public static final SIDESYNC_ACTION_SOURCE_DISCONNECTED:Ljava/lang/String; = "com.sec.android.sidesync.source.SIDESYNC_DISCONNECT"

.field public static final SIDESYNC_CONNNECTED:I = 0x1

.field public static final SIDESYNC_DISCONNNECTED:I = 0x0

.field public static final SIDESYNC_KMS_SINK_SERVICE_CLASS:Ljava/lang/String; = "com.sec.android.sidesync.kms.sink.service.SideSyncServerService"

.field public static final SIDESYNC_KMS_SOURCE_SERVICE_CLASS:Ljava/lang/String; = "com.sec.android.sidesync.kms.source.service.SideSyncService"

.field public static final SIDESYNC_SINK_API:Ljava/lang/String; = "com.sec.android.sidesync.sink.action.SIDESYNC_SINK_API"

.field public static final SIDESYNC_SOURCE_API:Ljava/lang/String; = "com.sec.android.sidesync.source.action.SIDESYNC_SOURCE_API"

.field public static final SIZE:I = 0x3

.field public static final SPLIT_VIEW_INVALID_ID:I = -0x1

.field public static final SPLIT_VIEW_SHORTCUT_ID:I = 0x1

.field public static final SPLIT_VIEW_TREEVIEW_ID:I = 0x0

.field public static final SPLIT_VIEW_WATCH_ID:I = 0x2

.field public static final SRC_FRAGMENT_ID:Ljava/lang/String; = "src_fragment_id"

.field public static final SRC_FTP_PARAMS:Ljava/lang/String; = "src-ftp-param"

.field public static final START_FOLDER:Ljava/lang/String; = "START_FOLDER"

.field public static final START_PATH:Ljava/lang/String; = "FOLDERPATH"

.field public static final SW_VERIFICATION_LOG:Ljava/lang/String; = "VerificationLog"

.field public static final TAG:Ljava/lang/String; = "MyFiles"

.field public static final TARGET_FOLDER:Ljava/lang/String; = "target_folder"

.field public static final THUMBNAIL:I = 0x2

.field public static final TIME:I = 0x0

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final TOTAL:I = 0x0

.field public static final TREE_VIEW_ITEM_CLICKED:Ljava/lang/String; = "TREE_VIEW_ITEM_CLICKED"

.field public static final TYPE:I = 0x1

.field public static final TYPE_NONE:I = -0x1

.field public static final UNLOCK:I = 0x19

.field public static final USB_STORAGE:I = 0x1e

.field public static final USER_BROWSER:I = 0x24

.field public static final USER_SHORTCUT:I = 0x1a

.field public static final USER_SHORTCUT_LIST:I = 0x23

.field public static final VIDEOS:I = 0x3

.field public static final VIDEOS_TAG:Ljava/lang/String; = "videos"

.field public static final WATCH_FLODER:I = 0x21

.field public static final WATCH_FOLDER_IMAGE_AND_VIDEO:Ljava/lang/String;

.field public static final WATCH_FOLDER_VOICE:Ljava/lang/String;

.field public static final ZIP:I = 0x3


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 131
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "\\"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "/"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, ":"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "*"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "?"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "\""

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "<"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, ">"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "|"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "\n"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Constant;->INVALID_CHAR:[Ljava/lang/String;

    .line 984
    sget-object v0, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-static {v0}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Constant;->DEFAULT_DOWNLOAD_DIR:Ljava/lang/String;

    .line 987
    sget-object v0, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-static {v0}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Constant;->DEFAULT_TEMP_DOWNLOAD_DIR:Ljava/lang/String;

    .line 992
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/myfiles/utils/Constant;->DEFAULT_TEMP_DOWNLOAD_DIR:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".myFiles_temp"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Constant;->DEFAULT_TEMP_UPLOAD_DIR:Ljava/lang/String;

    .line 1043
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "file:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Download"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Constant;->RSHARE_DOWNLOAD_DIR_URI:Ljava/lang/String;

    .line 1045
    const-string v0, "content://com.sec.rshare/content"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Constant;->RSHARE_CONTENT_URI:Landroid/net/Uri;

    .line 1046
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Download"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Constant;->RSHARE_DOWNLOAD_DIR:Ljava/lang/String;

    .line 1074
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/DCIM/Camera/Galaxy_Gear"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Constant;->WATCH_FOLDER_IMAGE_AND_VIDEO:Ljava/lang/String;

    .line 1076
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Sounds/Galaxy_Gear"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Constant;->WATCH_FOLDER_VOICE:Ljava/lang/String;

    .line 1091
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/RW_LIB/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Constant;->GOLF_TEMP_VIDEO_PATH:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
