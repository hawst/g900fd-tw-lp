.class Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;
.super Ljava/lang/Object;
.source "DownLoadOperationTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UploadableFile"
.end annotation


# instance fields
.field public mLocalFile:Ljava/io/File;

.field public mNewFileName:Ljava/lang/String;

.field public mServerPath:Ljava/lang/String;

.field public mSize:J

.field public mStarted:Z

.field public mUpload:Lcom/samsung/scloud/DropboxAPI$CancelableUpload;

.field public mUploadPercent:J


# direct methods
.method public constructor <init>(Ljava/io/File;Ljava/lang/String;JLjava/lang/String;)V
    .locals 5
    .param p1, "localFile"    # Ljava/io/File;
    .param p2, "newName"    # Ljava/lang/String;
    .param p3, "currentSize"    # J
    .param p5, "serverPath"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 196
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mLocalFile:Ljava/io/File;

    .line 198
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mServerPath:Ljava/lang/String;

    .line 200
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mNewFileName:Ljava/lang/String;

    .line 202
    iput-wide v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mSize:J

    .line 204
    iput-wide v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mUploadPercent:J

    .line 206
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mUpload:Lcom/samsung/scloud/DropboxAPI$CancelableUpload;

    .line 208
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mStarted:Z

    .line 212
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mLocalFile:Ljava/io/File;

    .line 213
    iput-object p2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mNewFileName:Ljava/lang/String;

    .line 214
    iput-wide p3, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mSize:J

    .line 215
    iput-object p5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mServerPath:Ljava/lang/String;

    .line 216
    iput-wide v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mUploadPercent:J

    .line 217
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mUpload:Lcom/samsung/scloud/DropboxAPI$CancelableUpload;

    .line 218
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mStarted:Z

    .line 219
    return-void
.end method
