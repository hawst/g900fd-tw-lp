.class Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$3;
.super Ljava/lang/Object;
.source "FolderHoverPopup.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)V
    .locals 0

    .prologue
    .line 461
    iput-object p1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$3;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x0

    .line 464
    const-string v6, "FolderHoverPopup"

    const-string v7, "onTouch"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    .line 466
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    if-nez v6, :cond_2

    .line 468
    check-cast p1, Landroid/widget/GridView;

    .end local p1    # "view":Landroid/view/View;
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    float-to-int v7, v7

    invoke-virtual {p1, v6, v7}, Landroid/widget/GridView;->pointToPosition(II)I

    move-result v4

    .line 470
    .local v4, "position":I
    if-gez v4, :cond_0

    .line 511
    .end local v4    # "position":I
    :goto_0
    return v5

    .line 473
    .restart local v4    # "position":I
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$3;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    # getter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->folderHoverPopupAdapter:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;
    invoke-static {v6}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$400(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;->getItem(I)Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;

    move-result-object v2

    .line 475
    .local v2, "item":Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->getFileFormat()I

    move-result v6

    const/16 v7, 0x3001

    if-ne v6, v7, :cond_4

    .line 478
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$3;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v6, v6, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v6, :cond_3

    .line 480
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$3;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverFolderNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->getFilePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v6, v7, v5}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z

    .line 482
    iget-object v5, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$3;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v5, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/MainActivity;->fragmentRefresh()V

    .line 507
    :cond_1
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$3;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->dissmissPopup()V

    .line 508
    iget-object v5, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$3;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->removeDelayedMessage()V

    .line 511
    .end local v2    # "item":Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;
    .end local v4    # "position":I
    :cond_2
    const/4 v5, 0x1

    goto :goto_0

    .line 484
    .restart local v2    # "item":Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;
    .restart local v4    # "position":I
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$3;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v5, v5, Lcom/sec/android/app/myfiles/SearchActivity;

    if-eqz v5, :cond_1

    .line 486
    iget-object v5, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$3;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v5, Lcom/sec/android/app/myfiles/SearchActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->getFilePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/myfiles/SearchActivity;->moveToFolderInResult(Ljava/lang/String;)V

    goto :goto_1

    .line 490
    :cond_4
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->getFilePath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v1

    .line 491
    .local v1, "fileTypeInt":I
    new-instance v0, Ljava/io/File;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->getFilePath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 493
    .local v0, "file":Ljava/io/File;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/MediaFile;->isArchiveFileType(I)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 495
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$3;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v6, v6, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v6, :cond_1

    .line 496
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->getFilePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->getFilePath()Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x2f

    invoke-virtual {v7, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v7

    invoke-virtual {v6, v5, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 497
    .local v3, "parentPath":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$3;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverFolderNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v6, v3, v5}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z

    .line 498
    iget-object v5, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$3;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v5, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->getFilePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/myfiles/MainActivity;->fragmentPerformExtract(Ljava/lang/String;)V

    goto :goto_1

    .line 502
    .end local v3    # "parentPath":Ljava/lang/String;
    :cond_5
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$3;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    iget v6, v6, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mCategoryType:I

    iget-object v7, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$3;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$3;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    iget-object v8, v8, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentSortBy()I

    move-result v8

    invoke-static {v6, v0, v7, v5, v8}, Lcom/sec/android/app/myfiles/utils/FileUtils;->openFile(ILjava/io/File;Landroid/app/Activity;ZI)V

    goto/16 :goto_1
.end method
