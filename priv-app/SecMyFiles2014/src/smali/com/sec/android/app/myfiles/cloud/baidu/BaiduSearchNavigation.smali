.class public Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchNavigation;
.super Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;
.source "BaiduSearchNavigation.java"


# instance fields
.field private mPathStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;-><init>(Landroid/content/Context;I)V

    .line 28
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchNavigation;->mPathStack:Ljava/util/Stack;

    .line 32
    return-void
.end method


# virtual methods
.method public getEmptyCourso()Landroid/database/Cursor;
    .locals 4

    .prologue
    .line 36
    new-instance v0, Landroid/database/MatrixCursor;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 38
    .local v0, "cursor":Landroid/database/MatrixCursor;
    return-object v0
.end method

.method public getPathStack()Ljava/util/Stack;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchNavigation;->mPathStack:Ljava/util/Stack;

    return-object v0
.end method

.method public goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z
    .locals 1
    .param p1, "folderPath"    # Ljava/lang/String;
    .param p2, "isBack"    # Ljava/lang/Boolean;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchNavigation;->mPathStack:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 44
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z

    move-result v0

    return v0
.end method
