.class public Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;
.source "SearchSetLocationFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;,
        Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;
    }
.end annotation


# static fields
.field private static _instance:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;


# instance fields
.field availStorage:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field hashtable:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/samsung/android/allshare/Device;",
            ">;"
        }
    .end annotation
.end field

.field intent:Landroid/content/Intent;

.field isfromAdvance:Z

.field mActivity:Landroid/app/Activity;

.field private mChosenDevice:Lcom/samsung/android/allshare/Device;

.field mData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;",
            ">;"
        }
    .end annotation
.end field

.field private mDevicesList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/allshare/Device;",
            ">;"
        }
    .end annotation
.end field

.field mDialog:Landroid/app/AlertDialog;

.field mIsNearbyCheck:Z

.field mIsOtherCheck:Z

.field mListArrayAdapter:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;

.field mLocationImage:Landroid/widget/LinearLayout;

.field private mNearbyDevicesListener:Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;

.field private mNearbyPosition:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field selectedLocation:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->_instance:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;-><init>()V

    .line 52
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->hashtable:Ljava/util/Hashtable;

    .line 53
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->intent:Landroid/content/Intent;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->availStorage:Ljava/util/ArrayList;

    .line 55
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mListArrayAdapter:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;

    .line 56
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->selectedLocation:Ljava/util/ArrayList;

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mData:Ljava/util/ArrayList;

    .line 63
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mIsNearbyCheck:Z

    .line 64
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mIsOtherCheck:Z

    .line 355
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mDevicesList:Ljava/util/List;

    .line 358
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mChosenDevice:Lcom/samsung/android/allshare/Device;

    .line 359
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mNearbyPosition:Ljava/util/ArrayList;

    .line 361
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$3;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mNearbyDevicesListener:Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;

    .line 610
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->clickOK()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mDevicesList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->refreshLocationList(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;Lcom/samsung/android/allshare/Device;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;
    .param p1, "x1"    # Lcom/samsung/android/allshare/Device;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->isRegisteredDevice(Lcom/samsung/android/allshare/Device;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mNearbyPosition:Ljava/util/ArrayList;

    return-object v0
.end method

.method private clickOK()V
    .locals 6

    .prologue
    const/4 v5, -0x1

    .line 284
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->selectedLocation:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFileLocation(Ljava/util/ArrayList;)V

    .line 285
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;

    .line 286
    .local v1, "item":Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f0b003e

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 287
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->isChecked()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationDevice(Z)V

    goto :goto_0

    .line 288
    :cond_1
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f0b003f

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 289
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->isChecked()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationSDcard(Z)V

    goto :goto_0

    .line 290
    :cond_2
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f0b0042

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 291
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->isChecked()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationUSBa(Z)V

    goto :goto_0

    .line 292
    :cond_3
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f0b0043

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 293
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->isChecked()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationUSBb(Z)V

    goto/16 :goto_0

    .line 294
    :cond_4
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f0b0044

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 295
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->isChecked()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationUSBc(Z)V

    goto/16 :goto_0

    .line 296
    :cond_5
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f0b0045

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 297
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->isChecked()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationUSBd(Z)V

    goto/16 :goto_0

    .line 298
    :cond_6
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f0b0046

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 299
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->isChecked()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationUSBe(Z)V

    goto/16 :goto_0

    .line 300
    :cond_7
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f0b0047

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 301
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->isChecked()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationUSBf(Z)V

    goto/16 :goto_0

    .line 302
    :cond_8
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f0b0008

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 303
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->isChecked()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationDropbox(Z)V

    goto/16 :goto_0

    .line 304
    :cond_9
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f0b00fa

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 305
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->isChecked()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationBaidu(Z)V

    goto/16 :goto_0

    .line 306
    :cond_a
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f0b0151

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 307
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->isChecked()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationPrivate(Z)V

    goto/16 :goto_0

    .line 309
    :cond_b
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mIsNearbyCheck:Z

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationNearby(Z)V

    .line 310
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 311
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setmCheckedNearby(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 316
    .end local v1    # "item":Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;
    :cond_c
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mChosenDevice:Lcom/samsung/android/allshare/Device;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->intent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "search_set_location_is_near_by_device_intent_key"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 318
    invoke-static {}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->getInstance()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mChosenDevice:Lcom/samsung/android/allshare/Device;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->registerDevice(Lcom/samsung/android/allshare/Device;)V

    .line 319
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->intent:Landroid/content/Intent;

    const-string v3, "provider_id"

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mChosenDevice:Lcom/samsung/android/allshare/Device;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 320
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->intent:Landroid/content/Intent;

    const-string v3, "provider_name"

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mChosenDevice:Lcom/samsung/android/allshare/Device;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 322
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->intent:Landroid/content/Intent;

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "search_set_location_is_near_by_device_index"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mChosenDevice:Lcom/samsung/android/allshare/Device;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationDeviceID(ILjava/lang/String;)V

    .line 324
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->isfromAdvance:Z

    if-eqz v2, :cond_d

    .line 325
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getTargetRequestCode()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->intent:Landroid/content/Intent;

    invoke-virtual {v2, v3, v5, v4}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 336
    :goto_1
    return-void

    .line 327
    :cond_d
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getTargetRequestCode()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->intent:Landroid/content/Intent;

    invoke-virtual {p0, v2, v5, v3}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->onClickSearchCallback(IILandroid/content/Intent;)V

    goto :goto_1

    .line 331
    :cond_e
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->isfromAdvance:Z

    if-eqz v2, :cond_f

    .line 332
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getTargetRequestCode()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->intent:Landroid/content/Intent;

    invoke-virtual {v2, v3, v5, v4}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_1

    .line 334
    :cond_f
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getTargetRequestCode()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->intent:Landroid/content/Intent;

    invoke-virtual {p0, v2, v5, v3}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->onClickSearchCallback(IILandroid/content/Intent;)V

    goto :goto_1
.end method

.method public static getInstance()Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;
    .locals 1

    .prologue
    .line 69
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->_instance:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    if-nez v0, :cond_0

    .line 70
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->_instance:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    .line 73
    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->_instance:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    return-object v0
.end method

.method private isRegisteredDevice(Lcom/samsung/android/allshare/Device;)Z
    .locals 7
    .param p1, "device"    # Lcom/samsung/android/allshare/Device;

    .prologue
    const/4 v4, 0x0

    .line 397
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mDevicesList:Ljava/util/List;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mDevicesList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 399
    invoke-virtual {p1}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v5

    const-string v6, "+"

    invoke-virtual {v5, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 400
    .local v0, "deviceID":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mDevicesList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 402
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mDevicesList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/Device;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v5

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mDevicesList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/Device;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v3

    const-string v6, "+"

    invoke-virtual {v3, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v5, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 403
    .local v2, "savedDeviceID":Ljava/lang/String;
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 404
    const/4 v3, 0x1

    .line 408
    .end local v0    # "deviceID":Ljava/lang/String;
    .end local v1    # "i":I
    .end local v2    # "savedDeviceID":Ljava/lang/String;
    :goto_1
    return v3

    .line 400
    .restart local v0    # "deviceID":Ljava/lang/String;
    .restart local v1    # "i":I
    .restart local v2    # "savedDeviceID":Ljava/lang/String;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .end local v0    # "deviceID":Ljava/lang/String;
    .end local v1    # "i":I
    .end local v2    # "savedDeviceID":Ljava/lang/String;
    :cond_1
    move v3, v4

    .line 408
    goto :goto_1
.end method

.method private refreshLocationList(Ljava/util/ArrayList;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "availStorage":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 413
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_0

    .line 415
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 416
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->hashtable:Ljava/util/Hashtable;

    invoke-virtual {v9}, Ljava/util/Hashtable;->clear()V

    .line 418
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    const v12, 0x7f0b003e

    invoke-virtual {v9, v12}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 420
    sget-boolean v9, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v9, :cond_1

    .line 421
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    const v12, 0x7f0b003f

    invoke-virtual {v9, v12}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 423
    :cond_1
    sget-boolean v9, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-eqz v9, :cond_8

    .line 425
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getMountedUsbStorage()Ljava/util/ArrayList;

    move-result-object v3

    .line 427
    .local v3, "listUsbStorages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;

    .line 429
    .local v6, "usbStorage":Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->getStoragePath()Ljava/lang/String;

    move-result-object v7

    .line 430
    .local v7, "usbStoragePath":Ljava/lang/String;
    const-string v9, "/"

    invoke-virtual {v7, v9}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    invoke-virtual {v7, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 431
    .local v8, "usbname":Ljava/lang/String;
    const-string v9, "UsbDriveA"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 432
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    const v12, 0x7f0b0042

    invoke-virtual {v9, v12}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 433
    :cond_3
    const-string v9, "UsbDriveB"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 434
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    const v12, 0x7f0b0043

    invoke-virtual {v9, v12}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 435
    :cond_4
    const-string v9, "UsbDriveC"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 436
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    const v12, 0x7f0b0044

    invoke-virtual {v9, v12}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 437
    :cond_5
    const-string v9, "UsbDriveD"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 438
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    const v12, 0x7f0b0045

    invoke-virtual {v9, v12}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 439
    :cond_6
    const-string v9, "UsbDriveE"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 440
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    const v12, 0x7f0b0046

    invoke-virtual {v9, v12}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 441
    :cond_7
    const-string v9, "UsbDriveF"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 442
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    const v12, 0x7f0b0047

    invoke-virtual {v9, v12}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 447
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "listUsbStorages":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;>;"
    .end local v6    # "usbStorage":Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    .end local v7    # "usbStoragePath":Ljava/lang/String;
    .end local v8    # "usbname":Ljava/lang/String;
    :cond_8
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mNearbyPosition:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    .line 448
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mDevicesList:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .restart local v2    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/Device;

    .line 450
    .local v1, "i":Lcom/samsung/android/allshare/Device;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 451
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->hashtable:Ljava/util/Hashtable;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v9, v12, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 452
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mNearbyPosition:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v9, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 455
    .end local v1    # "i":Lcom/samsung/android/allshare/Device;
    :cond_9
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mDevicesList:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    if-nez v9, :cond_a

    .line 456
    iput-boolean v11, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mIsNearbyCheck:Z

    .line 460
    :cond_a
    new-instance v0, Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-direct {v0, v9}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    .line 461
    .local v0, "cacheDB":Lcom/sec/android/app/myfiles/db/CacheDB;
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->isDropBoxCloudDataExist()I

    move-result v4

    .line 462
    .local v4, "noOfRows":I
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_b

    if-lez v4, :cond_b

    .line 464
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v9

    if-eqz v9, :cond_d

    .line 465
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    const v12, 0x7f0b00fa

    invoke-virtual {v9, v12}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 471
    :cond_b
    :goto_2
    sget-boolean v9, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    if-eqz v9, :cond_e

    .line 472
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    const v12, 0x7f0b0151

    invoke-virtual {v9, v12}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 479
    :goto_3
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_4
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->selectedLocation:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v1, v9, :cond_f

    .line 480
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->selectedLocation:Ljava/util/ArrayList;

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {p1, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_c

    .line 481
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->selectedLocation:Ljava/util/ArrayList;

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 479
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 467
    .end local v1    # "i":I
    :cond_d
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    const v12, 0x7f0b0008

    invoke-virtual {v9, v12}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 475
    :cond_e
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v9, v11}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationPrivate(Z)V

    goto :goto_3

    .line 485
    .restart local v1    # "i":I
    :cond_f
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mOkButton:Landroid/widget/Button;

    if-eqz v9, :cond_10

    .line 486
    iget-object v12, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mOkButton:Landroid/widget/Button;

    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->selectedLocation:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-eqz v9, :cond_11

    move v9, v10

    :goto_5
    invoke-virtual {v12, v9}, Landroid/widget/Button;->setEnabled(Z)V

    .line 489
    :cond_10
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->selectedLocation:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-eqz v9, :cond_12

    iget-boolean v9, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mIsNearbyCheck:Z

    if-nez v9, :cond_12

    .line 490
    iput-boolean v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mIsOtherCheck:Z

    .line 495
    :goto_6
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    .line 498
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_14

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 499
    .local v5, "str":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->selectedLocation:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_13

    .line 500
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mData:Ljava/util/ArrayList;

    new-instance v12, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;

    invoke-direct {v12, v5, v10}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v9, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .end local v5    # "str":Ljava/lang/String;
    :cond_11
    move v9, v11

    .line 486
    goto :goto_5

    .line 492
    :cond_12
    iput-boolean v11, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mIsOtherCheck:Z

    goto :goto_6

    .line 502
    .restart local v5    # "str":Ljava/lang/String;
    :cond_13
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mData:Ljava/util/ArrayList;

    new-instance v12, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;

    invoke-direct {v12, v5, v11}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v9, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 504
    .end local v5    # "str":Ljava/lang/String;
    :cond_14
    return-void
.end method

.method private registerNearbyDevicesListener()V
    .locals 2

    .prologue
    .line 344
    invoke-static {}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;->getInstance()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;

    move-result-object v0

    .line 345
    .local v0, "notifier":Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mNearbyDevicesListener:Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;->registerListenerToProviderAddNotification(Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;)Z

    .line 346
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mNearbyDevicesListener:Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;->registerListenerToProviderRemoveNotification(Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;)Z

    .line 347
    return-void
.end method

.method private unregisterNearbyDevicesListener()V
    .locals 2

    .prologue
    .line 350
    invoke-static {}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;->getInstance()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;

    move-result-object v0

    .line 351
    .local v0, "notifier":Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mNearbyDevicesListener:Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;->unregisterListenerFromProviderAddNotification(Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;)Z

    .line 352
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mNearbyDevicesListener:Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;->unregisterListenerFromProviderRemoveNotification(Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;)Z

    .line 353
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 192
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 194
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 683
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 684
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getTargetRequestCode()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 685
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mActivity:Landroid/app/Activity;

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v10

    const-string v11, "title"

    invoke-virtual {v10, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    .line 81
    .local v9, "titleResId":I
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    const v11, 0x7f0f00e1

    invoke-virtual {v10, v11}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout;

    iput-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mLocationImage:Landroid/widget/LinearLayout;

    .line 82
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v10

    const-string v11, "search_advance_intent_key"

    invoke-virtual {v10, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v10

    iput-boolean v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->isfromAdvance:Z

    .line 83
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v10}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    .line 84
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mDevicesList:Ljava/util/List;

    if-eqz v10, :cond_0

    .line 85
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mDevicesList:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->clear()V

    .line 87
    :cond_0
    new-instance v10, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;

    invoke-direct {v10, p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;)V

    iput-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mListArrayAdapter:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;

    .line 88
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->availStorage:Ljava/util/ArrayList;

    invoke-direct {p0, v10}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->refreshLocationList(Ljava/util/ArrayList;)V

    .line 90
    new-instance v8, Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    invoke-direct {v8, v10}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 91
    .local v8, "listView":Landroid/widget/ListView;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f09016f

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f090170

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f09016f

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    const/4 v13, 0x0

    invoke-virtual {v8, v10, v11, v12, v13}, Landroid/widget/ListView;->setPadding(IIII)V

    .line 92
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    invoke-direct {v0, v10}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 97
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->registerNearbyDevicesListener()V

    .line 99
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 100
    const/4 v3, 0x0

    .line 102
    .local v3, "i":I
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationDevice()Z

    move-result v10

    if-nez v10, :cond_1

    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationSDcard()Z

    move-result v10

    if-nez v10, :cond_1

    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBa()Z

    move-result v10

    if-nez v10, :cond_1

    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBb()Z

    move-result v10

    if-nez v10, :cond_1

    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBc()Z

    move-result v10

    if-nez v10, :cond_1

    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBd()Z

    move-result v10

    if-nez v10, :cond_1

    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBe()Z

    move-result v10

    if-nez v10, :cond_1

    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBf()Z

    move-result v10

    if-nez v10, :cond_1

    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationPrivate()Z

    move-result v10

    if-nez v10, :cond_1

    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationDropbox()Z

    move-result v10

    if-nez v10, :cond_1

    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationBaidu()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 108
    :cond_1
    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mIsOtherCheck:Z

    .line 112
    :goto_0
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->availStorage:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_11

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 115
    .local v7, "item":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    const v11, 0x7f0b003e

    invoke-virtual {v10, v11}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 116
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationDevice()Z

    move-result v6

    .line 149
    .local v6, "isChecked":Z
    :cond_2
    :goto_2
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mData:Ljava/util/ArrayList;

    new-instance v11, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;

    invoke-direct {v11, v7, v6}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mDevicesList:Ljava/util/List;

    if-eqz v10, :cond_10

    .line 152
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mDevicesList:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_10

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/Device;

    .line 153
    .local v2, "device":Lcom/samsung/android/allshare/Device;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 154
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mNearbyPosition:Ljava/util/ArrayList;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 110
    .end local v2    # "device":Lcom/samsung/android/allshare/Device;
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "isChecked":Z
    .end local v7    # "item":Ljava/lang/String;
    :cond_4
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mIsOtherCheck:Z

    goto :goto_0

    .line 117
    .restart local v7    # "item":Ljava/lang/String;
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    const v11, 0x7f0b003f

    invoke-virtual {v10, v11}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 118
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationSDcard()Z

    move-result v6

    .restart local v6    # "isChecked":Z
    goto :goto_2

    .line 119
    .end local v6    # "isChecked":Z
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    const v11, 0x7f0b0042

    invoke-virtual {v10, v11}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 120
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBa()Z

    move-result v6

    .restart local v6    # "isChecked":Z
    goto :goto_2

    .line 121
    .end local v6    # "isChecked":Z
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    const v11, 0x7f0b0043

    invoke-virtual {v10, v11}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 122
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBb()Z

    move-result v6

    .restart local v6    # "isChecked":Z
    goto :goto_2

    .line 123
    .end local v6    # "isChecked":Z
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    const v11, 0x7f0b0044

    invoke-virtual {v10, v11}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 124
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBc()Z

    move-result v6

    .restart local v6    # "isChecked":Z
    goto/16 :goto_2

    .line 125
    .end local v6    # "isChecked":Z
    :cond_9
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    const v11, 0x7f0b0045

    invoke-virtual {v10, v11}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 126
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBd()Z

    move-result v6

    .restart local v6    # "isChecked":Z
    goto/16 :goto_2

    .line 127
    .end local v6    # "isChecked":Z
    :cond_a
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    const v11, 0x7f0b0046

    invoke-virtual {v10, v11}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 128
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBe()Z

    move-result v6

    .restart local v6    # "isChecked":Z
    goto/16 :goto_2

    .line 129
    .end local v6    # "isChecked":Z
    :cond_b
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    const v11, 0x7f0b0047

    invoke-virtual {v10, v11}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 130
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationUSBf()Z

    move-result v6

    .restart local v6    # "isChecked":Z
    goto/16 :goto_2

    .line 131
    .end local v6    # "isChecked":Z
    :cond_c
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    const v11, 0x7f0b0008

    invoke-virtual {v10, v11}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_d

    .line 132
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationDropbox()Z

    move-result v6

    .restart local v6    # "isChecked":Z
    goto/16 :goto_2

    .line 133
    .end local v6    # "isChecked":Z
    :cond_d
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    const v11, 0x7f0b00fa

    invoke-virtual {v10, v11}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 134
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationBaidu()Z

    move-result v6

    .restart local v6    # "isChecked":Z
    goto/16 :goto_2

    .line 135
    .end local v6    # "isChecked":Z
    :cond_e
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    const v11, 0x7f0b0151

    invoke-virtual {v10, v11}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_f

    .line 136
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationPrivate()Z

    move-result v6

    .restart local v6    # "isChecked":Z
    goto/16 :goto_2

    .line 138
    .end local v6    # "isChecked":Z
    :cond_f
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchLocationNearby()Z

    move-result v6

    .line 139
    .restart local v6    # "isChecked":Z
    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mIsNearbyCheck:Z

    .line 140
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-static {}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getmCheckedNearby()Ljava/lang/String;

    move-result-object v1

    .line 141
    .local v1, "checkedNearbyName":Ljava/lang/String;
    if-eqz v6, :cond_2

    .line 142
    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 143
    const/4 v6, 0x0

    goto/16 :goto_2

    .line 159
    .end local v1    # "checkedNearbyName":Ljava/lang/String;
    :cond_10
    add-int/lit8 v3, v3, 0x1

    .line 161
    goto/16 :goto_1

    .line 168
    .end local v6    # "isChecked":Z
    .end local v7    # "item":Ljava/lang/String;
    :cond_11
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mListArrayAdapter:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;

    invoke-virtual {v8, v10}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 169
    invoke-virtual {v8, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 170
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getSearchFileLocation()Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_12

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 171
    .restart local v7    # "item":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->selectedLocation:Ljava/util/ArrayList;

    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 173
    .end local v7    # "item":Ljava/lang/String;
    :cond_12
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->intent:Landroid/content/Intent;

    const-string v11, "search_set_location_intent_key"

    iget-object v12, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->selectedLocation:Ljava/util/ArrayList;

    invoke-virtual {v10, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 174
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    invoke-virtual {v10, v9}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 175
    invoke-virtual {v0, v8}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 176
    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->setupDialog(Landroid/app/AlertDialog$Builder;)V

    .line 177
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mDialog:Landroid/app/AlertDialog;

    .line 178
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mDialog:Landroid/app/AlertDialog;

    return-object v10
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 227
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->onDestroy()V

    .line 232
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->unregisterNearbyDevicesListener()V

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mLocationImage:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mLocationImage:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 239
    :cond_0
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 519
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v3, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->isChecked()Z

    move-result v2

    .line 520
    .local v2, "isCheked":Z
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v3, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;

    if-nez v2, :cond_1

    move v4, v5

    :goto_0
    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->setChecked(Z)V

    .line 522
    if-nez v2, :cond_5

    .line 523
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mNearbyPosition:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 524
    .local v0, "i":I
    if-ne p3, v0, :cond_0

    .line 525
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mIsNearbyCheck:Z

    .line 526
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->hashtable:Ljava/util/Hashtable;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/Device;

    iput-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mChosenDevice:Lcom/samsung/android/allshare/Device;

    .line 529
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->intent:Landroid/content/Intent;

    const-string v4, "search_set_location_is_near_by_device_intent_key"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 530
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->intent:Landroid/content/Intent;

    const-string v4, "search_set_location_is_near_by_device_index"

    invoke-virtual {v3, v4, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .end local v0    # "i":I
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    move v4, v6

    .line 520
    goto :goto_0

    .line 533
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->selectedLocation:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v3, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 545
    :goto_2
    sget-boolean v3, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    if-nez v3, :cond_3

    .line 546
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->selectedLocation:Ljava/util/ArrayList;

    const-string v4, "Private"

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 549
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->selectedLocation:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eqz v3, :cond_8

    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mIsNearbyCheck:Z

    if-nez v3, :cond_8

    .line 550
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mIsOtherCheck:Z

    .line 551
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->intent:Landroid/content/Intent;

    const-string v4, "search_set_location_is_near_by_device_intent_key"

    invoke-virtual {v3, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 556
    :goto_3
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mOkButton:Landroid/widget/Button;

    if-eqz v3, :cond_4

    .line 557
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mOkButton:Landroid/widget/Button;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->selectedLocation:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eqz v4, :cond_9

    :goto_4
    invoke-virtual {v3, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 561
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mListArrayAdapter:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;->notifyDataSetChanged()V

    .line 562
    return-void

    .line 535
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mNearbyPosition:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_6
    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 536
    .restart local v0    # "i":I
    if-ne p3, v0, :cond_6

    .line 537
    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mIsNearbyCheck:Z

    .line 538
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->intent:Landroid/content/Intent;

    const-string v4, "search_set_location_is_near_by_device_intent_key"

    invoke-virtual {v3, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_5

    .line 542
    .end local v0    # "i":I
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->selectedLocation:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v3, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_2

    .line 553
    :cond_8
    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mIsOtherCheck:Z

    goto :goto_3

    :cond_9
    move v5, v6

    .line 557
    goto :goto_4
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 184
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mOkButton:Landroid/widget/Button;

    .line 185
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mCancelButton:Landroid/widget/Button;

    .line 187
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->onResume()V

    .line 188
    return-void
.end method

.method public refreshAdapter()V
    .locals 1

    .prologue
    .line 508
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mListArrayAdapter:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;

    if-eqz v0, :cond_0

    .line 510
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->availStorage:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->refreshLocationList(Ljava/util/ArrayList;)V

    .line 511
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mListArrayAdapter:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;->notifyDataSetChanged()V

    .line 514
    :cond_0
    return-void
.end method

.method protected setupDialog(Landroid/app/AlertDialog$Builder;)V
    .locals 2
    .param p1, "builder"    # Landroid/app/AlertDialog$Builder;

    .prologue
    .line 244
    const v0, 0x7f0b0015

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 260
    const v0, 0x7f0b0017

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$2;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 279
    return-void
.end method

.method protected setupInputEditText(Landroid/widget/EditText;)V
    .locals 0
    .param p1, "editText"    # Landroid/widget/EditText;

    .prologue
    .line 341
    return-void
.end method
