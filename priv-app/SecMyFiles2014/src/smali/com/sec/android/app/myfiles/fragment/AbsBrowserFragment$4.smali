.class Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$4;
.super Ljava/lang/Object;
.source "AbsBrowserFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->setSelectModeActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V
    .locals 0

    .prologue
    .line 1339
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1344
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isSelected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1346
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 1348
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->selectAllItem()V

    .line 1358
    :goto_0
    return-void

    .line 1352
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 1354
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->unselectAllItem()V

    goto :goto_0
.end method
