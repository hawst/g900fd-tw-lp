.class Lcom/sec/android/app/myfiles/SelectorActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "SelectorActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/SelectorActivity;->setExternalStorageReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/SelectorActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/SelectorActivity;)V
    .locals 0

    .prologue
    .line 1554
    iput-object p1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$1;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x2

    .line 1559
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1561
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1564
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$1;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isMountedStateCheck(Landroid/content/Context;)V

    .line 1566
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$1;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$1300(Lcom/sec/android/app/myfiles/SelectorActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1568
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$1;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$1300(Lcom/sec/android/app/myfiles/SelectorActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1570
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$1;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$1300(Lcom/sec/android/app/myfiles/SelectorActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1572
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$1;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$1300(Lcom/sec/android/app/myfiles/SelectorActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1575
    :cond_1
    return-void
.end method
