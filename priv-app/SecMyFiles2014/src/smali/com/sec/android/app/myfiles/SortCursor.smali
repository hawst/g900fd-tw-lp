.class public Lcom/sec/android/app/myfiles/SortCursor;
.super Landroid/database/CursorWrapper;
.source "SortCursor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/SortCursor$FileComparator;,
        Lcom/sec/android/app/myfiles/SortCursor$SortEntry;
    }
.end annotation


# static fields
.field private static final MODULE:Ljava/lang/String; = "SortCursor"

.field private static inOrder:I

.field private static sort_type:I


# instance fields
.field private isSearch:Z

.field private position:I

.field private sortedList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/SortCursor$SortEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/database/Cursor;II)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "sort_type"    # I
    .param p3, "inOrder"    # I

    .prologue
    .line 47
    invoke-direct {p0, p1}, Landroid/database/CursorWrapper;-><init>(Landroid/database/Cursor;)V

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/SortCursor;->position:I

    .line 49
    sput p2, Lcom/sec/android/app/myfiles/SortCursor;->sort_type:I

    .line 50
    sput p3, Lcom/sec/android/app/myfiles/SortCursor;->inOrder:I

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/SortCursor;->isSearch:Z

    .line 53
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SortCursor;->initSortList()V

    .line 54
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SortCursor;->executeSortList()V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;IIZ)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "sort_type"    # I
    .param p3, "inOrder"    # I
    .param p4, "isSearch"    # Z

    .prologue
    .line 57
    invoke-direct {p0, p1}, Landroid/database/CursorWrapper;-><init>(Landroid/database/Cursor;)V

    .line 58
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/SortCursor;->position:I

    .line 59
    sput p2, Lcom/sec/android/app/myfiles/SortCursor;->sort_type:I

    .line 60
    sput p3, Lcom/sec/android/app/myfiles/SortCursor;->inOrder:I

    .line 61
    iput-boolean p4, p0, Lcom/sec/android/app/myfiles/SortCursor;->isSearch:Z

    .line 63
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SortCursor;->initSortList()V

    .line 64
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SortCursor;->executeSortList()V

    .line 65
    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 37
    sget v0, Lcom/sec/android/app/myfiles/SortCursor;->sort_type:I

    return v0
.end method

.method static synthetic access$100()I
    .locals 1

    .prologue
    .line 37
    sget v0, Lcom/sec/android/app/myfiles/SortCursor;->inOrder:I

    return v0
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 266
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SortCursor;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 267
    return-void
.end method

.method protected executeSortList()V
    .locals 5

    .prologue
    .line 211
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SortCursor;->sortedList:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/SortCursor;->sortedList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ge v2, v3, :cond_1

    .line 224
    :cond_0
    :goto_0
    return-void

    .line 214
    :cond_1
    new-instance v0, Lcom/sec/android/app/myfiles/SortCursor$FileComparator;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/SortCursor$FileComparator;-><init>()V

    .line 218
    .local v0, "comparator":Lcom/sec/android/app/myfiles/SortCursor$FileComparator;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SortCursor;->sortedList:Ljava/util/List;

    invoke-static {v2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 220
    :catch_0
    move-exception v1

    .line 222
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    const/4 v2, 0x0

    const-string v3, "SortCursor"

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 262
    iget v0, p0, Lcom/sec/android/app/myfiles/SortCursor;->position:I

    return v0
.end method

.method protected getRelativePosition(I)I
    .locals 4
    .param p1, "position"    # I

    .prologue
    const/4 v2, 0x0

    .line 228
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SortCursor;->sortedList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;

    iget v1, v1, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->order:I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 236
    :goto_0
    return v1

    .line 229
    :catch_0
    move-exception v0

    .line 234
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    const-string v1, "SortCursor"

    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    move v1, v2

    .line 236
    goto :goto_0
.end method

.method protected initSortList()V
    .locals 12

    .prologue
    .line 168
    const/4 v3, 0x0

    .line 170
    .local v3, "i":I
    iget-object v9, p0, Lcom/sec/android/app/myfiles/SortCursor;->sortedList:Ljava/util/List;

    if-eqz v9, :cond_0

    .line 171
    iget-object v9, p0, Lcom/sec/android/app/myfiles/SortCursor;->sortedList:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->clear()V

    .line 176
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SortCursor;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 178
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_2

    move v4, v3

    .line 182
    .end local v3    # "i":I
    .local v4, "i":I
    :goto_1
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 184
    new-instance v8, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;

    invoke-direct {v8}, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;-><init>()V

    .line 185
    .local v8, "sortKey":Lcom/sec/android/app/myfiles/SortCursor$SortEntry;
    const-string v9, "_data"

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 186
    .local v5, "indx_data":I
    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 187
    .local v7, "path":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 188
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->name:Ljava/lang/String;

    .line 189
    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    move-result-wide v10

    iput-wide v10, v8, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->time:J
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 190
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "i":I
    .restart local v3    # "i":I
    :try_start_1
    iput v4, v8, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->order:I

    .line 191
    invoke-static {v7}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v9

    iput v9, v8, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->type:I

    .line 192
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v9

    iput-boolean v9, v8, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->isDirectory:Z

    .line 194
    iget-boolean v9, p0, Lcom/sec/android/app/myfiles/SortCursor;->isSearch:Z

    if-nez v9, :cond_1

    .line 195
    const-string v9, "_size"

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 196
    .local v6, "indx_size":I
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    iput-wide v10, v8, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->size:J

    .line 200
    .end local v6    # "indx_size":I
    :goto_2
    iget-object v9, p0, Lcom/sec/android/app/myfiles/SortCursor;->sortedList:Ljava/util/List;

    invoke-interface {v9, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    move v4, v3

    .line 201
    .end local v3    # "i":I
    .restart local v4    # "i":I
    goto :goto_1

    .line 173
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v2    # "file":Ljava/io/File;
    .end local v4    # "i":I
    .end local v5    # "indx_data":I
    .end local v7    # "path":Ljava/lang/String;
    .end local v8    # "sortKey":Lcom/sec/android/app/myfiles/SortCursor$SortEntry;
    .restart local v3    # "i":I
    :cond_0
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, p0, Lcom/sec/android/app/myfiles/SortCursor;->sortedList:Ljava/util/List;

    goto :goto_0

    .line 198
    .restart local v0    # "cursor":Landroid/database/Cursor;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v5    # "indx_data":I
    .restart local v7    # "path":Ljava/lang/String;
    .restart local v8    # "sortKey":Lcom/sec/android/app/myfiles/SortCursor$SortEntry;
    :cond_1
    :try_start_2
    iget-boolean v9, v8, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->isDirectory:Z

    if-eqz v9, :cond_3

    const-wide/16 v10, 0x0

    :goto_3
    iput-wide v10, v8, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->size:J
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 203
    :catch_0
    move-exception v1

    .line 205
    .end local v2    # "file":Ljava/io/File;
    .end local v5    # "indx_data":I
    .end local v7    # "path":Ljava/lang/String;
    .end local v8    # "sortKey":Lcom/sec/android/app/myfiles/SortCursor$SortEntry;
    .local v1, "e":Ljava/lang/IllegalStateException;
    :goto_4
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 208
    .end local v1    # "e":Ljava/lang/IllegalStateException;
    :cond_2
    :goto_5
    return-void

    .line 198
    .restart local v2    # "file":Ljava/io/File;
    .restart local v5    # "indx_data":I
    .restart local v7    # "path":Ljava/lang/String;
    .restart local v8    # "sortKey":Lcom/sec/android/app/myfiles/SortCursor$SortEntry;
    :cond_3
    :try_start_3
    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->getTotalSize(Ljava/io/File;)J
    :try_end_3
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_0

    move-result-wide v10

    goto :goto_3

    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "i":I
    .end local v5    # "indx_data":I
    .end local v7    # "path":Ljava/lang/String;
    .end local v8    # "sortKey":Lcom/sec/android/app/myfiles/SortCursor$SortEntry;
    .restart local v4    # "i":I
    :cond_4
    move v3, v4

    .line 206
    .end local v4    # "i":I
    .restart local v3    # "i":I
    goto :goto_5

    .line 203
    .end local v3    # "i":I
    .restart local v4    # "i":I
    :catch_1
    move-exception v1

    move v3, v4

    .end local v4    # "i":I
    .restart local v3    # "i":I
    goto :goto_4
.end method

.method public moveToFirst()Z
    .locals 1

    .prologue
    .line 259
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/SortCursor;->moveToPosition(I)Z

    move-result v0

    return v0
.end method

.method public moveToNext()Z
    .locals 1

    .prologue
    .line 256
    iget v0, p0, Lcom/sec/android/app/myfiles/SortCursor;->position:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/SortCursor;->moveToPosition(I)Z

    move-result v0

    return v0
.end method

.method public moveToPosition(I)Z
    .locals 5
    .param p1, "position"    # I

    .prologue
    const/4 v3, 0x0

    .line 239
    iput p1, p0, Lcom/sec/android/app/myfiles/SortCursor;->position:I

    .line 240
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/SortCursor;->getRelativePosition(I)I

    move-result v2

    .line 241
    .local v2, "order":I
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SortCursor;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 242
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_0

    .line 244
    :try_start_0
    invoke-interface {v0, v2}, Landroid/database/Cursor;->moveToPosition(I)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 250
    :cond_0
    :goto_0
    return v3

    .line 245
    :catch_0
    move-exception v1

    .line 246
    .local v1, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method
