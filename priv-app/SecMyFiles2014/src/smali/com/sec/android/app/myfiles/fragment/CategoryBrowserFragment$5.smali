.class Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5;
.super Ljava/lang/Object;
.source "CategoryBrowserFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;)V
    .locals 0

    .prologue
    .line 701
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 12
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 706
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v7

    invoke-interface {v7, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 707
    .local v0, "cursor":Landroid/database/Cursor;
    const-string v7, "_id"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 708
    .local v2, "cursorId":J
    const-string v7, "_data"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 709
    .local v5, "path":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->isSelectMode()Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v7

    if-eqz v7, :cond_6

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "run_from"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    const/16 v8, 0x15

    if-ne v7, v8, :cond_6

    .line 712
    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 713
    .local v1, "file":Ljava/io/File;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isClickValid()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 715
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 717
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getIsAudioSelector()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 718
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->getSelectionType()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_2

    .line 719
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget v7, v7, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mCategoryType:I

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->getCurrentSortBy()I

    move-result v10

    invoke-static {v7, v1, v8, v9, v10}, Lcom/sec/android/app/myfiles/utils/FileUtils;->openFile(ILjava/io/File;Landroid/app/Activity;ZI)V

    .line 800
    .end local v1    # "file":Ljava/io/File;
    :cond_1
    :goto_0
    return-void

    .line 722
    .restart local v1    # "file":Ljava/io/File;
    :cond_2
    const v7, 0x7f0f0038

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 723
    .local v6, "selectionButton":Landroid/view/View;
    if-eqz v6, :cond_1

    .line 724
    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    .line 725
    invoke-virtual {v6}, Landroid/view/View;->performClick()Z

    goto :goto_0

    .line 731
    .end local v6    # "selectionButton":Landroid/view/View;
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget v7, v7, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mCategoryType:I

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->getCurrentSortBy()I

    move-result v10

    invoke-static {v7, v1, v8, v9, v10}, Lcom/sec/android/app/myfiles/utils/FileUtils;->openFile(ILjava/io/File;Landroid/app/Activity;ZI)V

    .line 734
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->initFileObserver()V

    .line 736
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v4

    .line 738
    .local v4, "parentFile":Ljava/io/File;
    if-eqz v4, :cond_1

    .line 743
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    new-instance v8, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5$1;

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, p0, v9}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5$1;-><init>(Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5;Ljava/lang/String;)V

    iput-object v8, v7, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mFileObserver:Landroid/os/FileObserver;

    .line 780
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mFileObserver:Landroid/os/FileObserver;

    invoke-virtual {v7}, Landroid/os/FileObserver;->startWatching()V

    goto :goto_0

    .line 785
    .end local v4    # "parentFile":Ljava/io/File;
    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v8, 0x7f0b005a

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    .line 787
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;)Landroid/os/Handler;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 789
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;)Landroid/os/Handler;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 792
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;)Landroid/os/Handler;

    move-result-object v7

    const/4 v8, 0x0

    const-wide/16 v10, 0x1f4

    invoke-virtual {v7, v8, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 797
    .end local v1    # "file":Ljava/io/File;
    :cond_6
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v7, v2, v3, v5}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->multipleSelect(JLjava/lang/String;)V

    goto :goto_0
.end method
