.class Lcom/sec/android/app/myfiles/fragment/CloudFragment$4;
.super Ljava/lang/Object;
.source "CloudFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/CloudFragment;->onCreateItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/CloudFragment;)V
    .locals 0

    .prologue
    .line 1238
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 8
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1243
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v5, v5, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v5, :cond_0

    .line 1244
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v5, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mIsSearchMode:Z

    if-eqz v5, :cond_0

    move v5, v6

    .line 1314
    :goto_0
    return v5

    .line 1249
    :cond_0
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/Cursor;

    .line 1251
    .local v1, "cursor":Landroid/database/Cursor;
    const/4 v4, 0x0

    .line 1252
    .local v4, "path":Ljava/lang/String;
    const/4 v2, 0x0

    .line 1255
    .local v2, "format":I
    if-eqz v1, :cond_6

    .line 1257
    const-string v5, "_data"

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1259
    const-string v5, "format"

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 1261
    .local v3, "index":I
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 1264
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->isSelectMode()Z

    move-result v5

    if-nez v5, :cond_3

    .line 1266
    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->isInternalRootFolder(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_6

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->isExternalRootFolder(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_6

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxRootFolder(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 1268
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    invoke-virtual {v5, v7, p3, v6}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->startSelectMode(III)V

    .line 1272
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1274
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mCurrentListableView:Lcom/sec/android/app/myfiles/view/IDirectDragable;

    invoke-interface {v5, v7}, Lcom/sec/android/app/myfiles/view/IDirectDragable;->setDirectDragMode(Z)V

    :goto_1
    move v5, v7

    .line 1283
    goto :goto_0

    .line 1280
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mCurrentListableView:Lcom/sec/android/app/myfiles/view/IDirectDragable;

    invoke-interface {v5, v6}, Lcom/sec/android/app/myfiles/view/IDirectDragable;->setDirectDragMode(Z)V

    goto :goto_1

    .line 1288
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemPosition()Ljava/util/ArrayList;

    move-result-object v5

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1290
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 1292
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->isShowTreeView()Z

    move-result v5

    if-nez v5, :cond_5

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v5

    if-nez v5, :cond_5

    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mSideSyncControl:Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;->isKMSRunning(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_5

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mSideSyncControl:Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;

    invoke-static {}, Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;->isPSSRunning()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1295
    :cond_5
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1297
    .local v0, "arg":Landroid/os/Bundle;
    const-string v5, "dropbox"

    invoke-virtual {v0, v5, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1298
    const-string v5, "ITEM_INITIATING_DRAG_PATH"

    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1299
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    invoke-virtual {v5, p2, v0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->startDragMode(Landroid/view/View;Landroid/os/Bundle;)V

    .end local v0    # "arg":Landroid/os/Bundle;
    .end local v3    # "index":I
    :cond_6
    :goto_2
    move v5, v6

    .line 1314
    goto/16 :goto_0

    .line 1304
    .restart local v3    # "index":I
    :cond_7
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1306
    .restart local v0    # "arg":Landroid/os/Bundle;
    const-string v5, "dropbox"

    invoke-virtual {v0, v5, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1308
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    invoke-virtual {v5, p2, v0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->startDragMode(Landroid/view/View;Landroid/os/Bundle;)V

    goto :goto_2
.end method
