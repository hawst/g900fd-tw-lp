.class Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;
.super Ljava/lang/Thread;
.source "FolderHoverPopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ChangeListThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)V
    .locals 0

    .prologue
    .line 590
    iput-object p1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/16 v5, 0x10

    .line 595
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    iget-boolean v2, v2, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mDismissFolderHover:Z

    if-nez v2, :cond_5

    .line 597
    const/4 v1, 0x0

    .line 599
    .local v1, "i":I
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    # getter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mArrayList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$100(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    # getter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mArrayList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$100(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v2, v5, :cond_0

    .line 603
    const-wide/16 v2, 0xbb8

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 610
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    # getter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mDisplayList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$500(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    # getter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mDisplayList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$500(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_4

    .line 612
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    # getter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mDisplayList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$500(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 619
    :cond_1
    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    # getter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mDisplayFileCount:I
    invoke-static {v2}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$600(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    # getter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mArrayList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$100(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    .line 621
    if-ge v1, v5, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    # getter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mDisplayFileCount:I
    invoke-static {v2}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$600(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    # getter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mArrayList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$100(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v2, v3, :cond_6

    .line 630
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    # getter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mDisplayFileCount:I
    invoke-static {v2}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$600(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    # getter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mArrayList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$100(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v2, v3, :cond_3

    .line 631
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    const/4 v3, 0x0

    # setter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mDisplayFileCount:I
    invoke-static {v2, v3}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$602(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;I)I

    .line 634
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->sendListChangeMessage()V

    goto :goto_0

    .line 605
    :catch_0
    move-exception v0

    .line 607
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 614
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    # getter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mDisplayList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$500(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)Ljava/util/ArrayList;

    move-result-object v2

    if-nez v2, :cond_1

    .line 638
    .end local v1    # "i":I
    :cond_5
    return-void

    .line 625
    .restart local v1    # "i":I
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    # getter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mDisplayList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$500(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    # getter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mArrayList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$100(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    # getter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mDisplayFileCount:I
    invoke-static {v4}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$600(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 626
    add-int/lit8 v1, v1, 0x1

    .line 627
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    # operator++ for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mDisplayFileCount:I
    invoke-static {v2}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$608(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)I

    goto :goto_2
.end method
