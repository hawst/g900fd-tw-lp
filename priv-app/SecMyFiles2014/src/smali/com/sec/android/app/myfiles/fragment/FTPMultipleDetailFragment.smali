.class public Lcom/sec/android/app/myfiles/fragment/FTPMultipleDetailFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;
.source "FTPMultipleDetailFragment.java"


# static fields
.field private static final CONTAINS_POSITION:I = 0x1

.field private static final NUM_OF_ITEMS:I = 0x2

.field private static final SIZE_POSITION:I


# instance fields
.field private mContains:Ljava/lang/String;

.field private mSize:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected fillListView(Landroid/widget/ListView;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "listView"    # Landroid/widget/ListView;
    .param p2, "arg"    # Landroid/os/Bundle;

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPMultipleDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "detail_items_size"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/sec/android/app/myfiles/fragment/FTPMultipleDetailFragment;->getSizeString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPMultipleDetailFragment;->mSize:Ljava/lang/String;

    .line 29
    const-string v0, "detail_files_count"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const-string v1, "detail_folder_count"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/myfiles/fragment/FTPMultipleDetailFragment;->getContainsString(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPMultipleDetailFragment;->mContains:Ljava/lang/String;

    .line 33
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPMultipleDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPMultipleDetailFragment;->mDetailsAdapter:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;

    .line 34
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPMultipleDetailFragment;->mDetailsAdapter:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 35
    return-void
.end method

.method protected getContainsString(II)Ljava/lang/String;
    .locals 7
    .param p1, "filesSelected"    # I
    .param p2, "foldersSelected"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 79
    .local v0, "sb":Ljava/lang/StringBuilder;
    if-lez p1, :cond_0

    .line 81
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPMultipleDetailFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f0c0000

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, p1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    :cond_0
    if-lez p2, :cond_2

    .line 86
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 88
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPMultipleDetailFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0001

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, p2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected getInitValue(I)Ljava/lang/String;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 51
    packed-switch p1, :pswitch_data_0

    .line 57
    const-string v0, ""

    :goto_0
    return-object v0

    .line 53
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPMultipleDetailFragment;->mSize:Ljava/lang/String;

    goto :goto_0

    .line 55
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPMultipleDetailFragment;->mContains:Ljava/lang/String;

    goto :goto_0

    .line 51
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected getItemCount()I
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x2

    return v0
.end method

.method protected getLabel(I)Ljava/lang/String;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 39
    packed-switch p1, :pswitch_data_0

    .line 45
    const-string v0, ""

    :goto_0
    return-object v0

    .line 41
    :pswitch_0
    const v0, 0x7f0b00aa

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/FTPMultipleDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 43
    :pswitch_1
    const v0, 0x7f0b00ab

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/FTPMultipleDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 39
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected getSizeTextViewPosition()I
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    return v0
.end method

.method protected setSize(Ljava/lang/String;)V
    .locals 0
    .param p1, "size"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/FTPMultipleDetailFragment;->mSize:Ljava/lang/String;

    .line 64
    return-void
.end method
