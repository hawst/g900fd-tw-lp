.class Lcom/sec/android/app/myfiles/view/PathIndicator$4;
.super Ljava/lang/Object;
.source "PathIndicator.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/view/PathIndicator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/view/PathIndicator;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/view/PathIndicator;)V
    .locals 0

    .prologue
    .line 544
    iput-object p1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator$4;->this$0:Lcom/sec/android/app/myfiles/view/PathIndicator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 549
    const/4 v1, -0x1

    .line 551
    .local v1, "index":I
    const/4 v3, 0x0

    .line 553
    .local v3, "vh":Lcom/sec/android/app/myfiles/view/PathIndicator$ViewHolder;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/view/PathIndicator$4;->this$0:Lcom/sec/android/app/myfiles/view/PathIndicator;

    # getter for: Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonViewHolderList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/view/PathIndicator;->access$100(Lcom/sec/android/app/myfiles/view/PathIndicator;)Ljava/util/ArrayList;

    move-result-object v4

    if-nez v4, :cond_1

    .line 592
    :cond_0
    :goto_0
    return-void

    .line 557
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/myfiles/view/PathIndicator$4;->this$0:Lcom/sec/android/app/myfiles/view/PathIndicator;

    # getter for: Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonViewHolderList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/view/PathIndicator;->access$100(Lcom/sec/android/app/myfiles/view/PathIndicator;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 559
    iget-object v4, p0, Lcom/sec/android/app/myfiles/view/PathIndicator$4;->this$0:Lcom/sec/android/app/myfiles/view/PathIndicator;

    # getter for: Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderButtonViewHolderList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/view/PathIndicator;->access$100(Lcom/sec/android/app/myfiles/view/PathIndicator;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "vh":Lcom/sec/android/app/myfiles/view/PathIndicator$ViewHolder;
    check-cast v3, Lcom/sec/android/app/myfiles/view/PathIndicator$ViewHolder;

    .line 561
    .restart local v3    # "vh":Lcom/sec/android/app/myfiles/view/PathIndicator$ViewHolder;
    iget-object v4, v3, Lcom/sec/android/app/myfiles/view/PathIndicator$ViewHolder;->mPathIndicatorButton:Landroid/widget/TextView;

    invoke-virtual {p1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 563
    move v1, v0

    .line 569
    :cond_2
    const/4 v4, -0x1

    if-eq v1, v4, :cond_0

    .line 571
    iget-object v4, p0, Lcom/sec/android/app/myfiles/view/PathIndicator$4;->this$0:Lcom/sec/android/app/myfiles/view/PathIndicator;

    # getter for: Lcom/sec/android/app/myfiles/view/PathIndicator;->mPathChangeListener:Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/view/PathIndicator;->access$200(Lcom/sec/android/app/myfiles/view/PathIndicator;)Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 577
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 579
    .local v2, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    :goto_2
    if-gt v0, v1, :cond_5

    .line 581
    iget-object v4, p0, Lcom/sec/android/app/myfiles/view/PathIndicator$4;->this$0:Lcom/sec/android/app/myfiles/view/PathIndicator;

    # getter for: Lcom/sec/android/app/myfiles/view/PathIndicator;->mFolderList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/view/PathIndicator;->access$300(Lcom/sec/android/app/myfiles/view/PathIndicator;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 583
    if-ge v0, v1, :cond_3

    .line 585
    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 579
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 557
    .end local v2    # "sb":Ljava/lang/StringBuilder;
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 589
    .restart local v2    # "sb":Ljava/lang/StringBuilder;
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/myfiles/view/PathIndicator$4;->this$0:Lcom/sec/android/app/myfiles/view/PathIndicator;

    # getter for: Lcom/sec/android/app/myfiles/view/PathIndicator;->mPathChangeListener:Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/view/PathIndicator;->access$200(Lcom/sec/android/app/myfiles/view/PathIndicator;)Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;->onPathSelected(ILjava/lang/String;)V

    goto :goto_0
.end method
