.class public Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;
.super Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
.source "DownloadedAppAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;,
        Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$SizeInfo;
    }
.end annotation


# static fields
.field static final REMOVE_DIACRITICALS_PATTERN:Ljava/util/regex/Pattern;

.field private static final SIZE_INVALID:I = -0x2

.field protected static mEntriesMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAppNameComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mAppSizeComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mAppTimeComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mApplications:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ApplicationInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mCurId:J

.field private mDetailContainer:Landroid/view/ViewGroup;

.field private mDownloadAppHoverPopup:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

.field private mIsSelectMode:Z

.field private mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private mSelectedItem:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

.field protected mSelectionType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    const-string v0, "\\p{InCombiningDiacriticalMarks}+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->REMOVE_DIACRITICALS_PATTERN:Ljava/util/regex/Pattern;

    .line 89
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mEntriesMap:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;-><init>()V

    .line 87
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mItems:Ljava/util/ArrayList;

    .line 91
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mCurId:J

    .line 104
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mDownloadAppHoverPopup:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 637
    new-instance v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$4;-><init>(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mAppNameComparator:Ljava/util/Comparator;

    .line 674
    new-instance v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$5;-><init>(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mAppSizeComparator:Ljava/util/Comparator;

    .line 691
    new-instance v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$6;-><init>(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mAppTimeComparator:Ljava/util/Comparator;

    .line 110
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mContext:Landroid/content/Context;

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mContext:Landroid/content/Context;

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-static {v0, v1}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    new-instance v0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mDownloadAppHoverPopup:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 121
    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mEntriesMap:Ljava/util/HashMap;

    if-nez v0, :cond_1

    .line 123
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mEntriesMap:Ljava/util/HashMap;

    .line 150
    :goto_0
    return-void

    .line 127
    :cond_1
    sget-object v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mEntriesMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;)Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mSelectedItem:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;)Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;
    .param p1, "x1"    # Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mSelectedItem:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mDownloadAppHoverPopup:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    return-object v0
.end method

.method private getEntryLocked(Landroid/content/pm/ApplicationInfo;)Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    .locals 6
    .param p1, "info"    # Landroid/content/pm/ApplicationInfo;

    .prologue
    .line 200
    sget-object v1, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mEntriesMap:Ljava/util/HashMap;

    iget-object v2, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .line 202
    .local v0, "entry":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    if-nez v0, :cond_1

    .line 204
    new-instance v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .end local v0    # "entry":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mContext:Landroid/content/Context;

    iget-wide v2, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mCurId:J

    const-wide/16 v4, 0x1

    add-long/2addr v4, v2

    iput-wide v4, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mCurId:J

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;-><init>(Landroid/content/Context;Landroid/content/pm/ApplicationInfo;J)V

    .line 206
    .restart local v0    # "entry":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    sget-object v1, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mEntriesMap:Ljava/util/HashMap;

    iget-object v2, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    :cond_0
    :goto_0
    return-object v0

    .line 210
    :cond_1
    iget-object v1, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->info:Landroid/content/pm/ApplicationInfo;

    if-eq v1, p1, :cond_0

    .line 212
    iput-object p1, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->info:Landroid/content/pm/ApplicationInfo;

    goto :goto_0
.end method

.method private loadInstalledApps()V
    .locals 8

    .prologue
    .line 157
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mPackageManager:Landroid/content/pm/PackageManager;

    const/16 v7, 0x2200

    invoke-virtual {v6, v7}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mApplications:Ljava/util/List;

    .line 164
    new-instance v0, Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mApplications:Ljava/util/List;

    invoke-direct {v0, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 166
    .local v0, "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 168
    .local v3, "filteredApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    if-ge v4, v6, :cond_1

    .line 170
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/ApplicationInfo;

    .line 172
    .local v5, "info":Landroid/content/pm/ApplicationInfo;
    iget v6, v5, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v6, v6, 0x1

    if-nez v6, :cond_0

    iget v6, v5, Landroid/content/pm/ApplicationInfo;->flags:I

    const/high16 v7, 0x800000

    and-int/2addr v6, v7

    if-eqz v6, :cond_0

    .line 176
    iget-object v6, v5, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    if-eqz v6, :cond_0

    .line 178
    invoke-direct {p0, v5}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->getEntryLocked(Landroid/content/pm/ApplicationInfo;)Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    move-result-object v2

    .line 180
    .local v2, "entry":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->ensureLabel(Landroid/content/Context;)V

    .line 182
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168
    .end local v2    # "entry":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 189
    .end local v5    # "info":Landroid/content/pm/ApplicationInfo;
    :cond_1
    iput-object v3, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mItems:Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 195
    .end local v0    # "apps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    .end local v3    # "filteredApps":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;>;"
    .end local v4    # "i":I
    :goto_1
    return-void

    .line 190
    :catch_0
    move-exception v1

    .line 192
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public static normalize(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 806
    sget-object v1, Ljava/text/Normalizer$Form;->NFD:Ljava/text/Normalizer$Form;

    invoke-static {p0, v1}, Ljava/text/Normalizer;->normalize(Ljava/lang/CharSequence;Ljava/text/Normalizer$Form;)Ljava/lang/String;

    move-result-object v0

    .line 808
    .local v0, "tmp":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->REMOVE_DIACRITICALS_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public finishSelectMode()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 555
    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mIsSelectMode:Z

    .line 557
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mSelectedItem:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .line 559
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .line 561
    .local v1, "item":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    invoke-virtual {v1, v3}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->setSelected(Z)V

    goto :goto_0

    .line 564
    .end local v1    # "item":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->notifyDataSetChanged()V

    .line 565
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mItems:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 238
    const/4 v0, 0x0

    .line 240
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 246
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 252
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    iget-wide v0, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->id:J

    return-wide v0
.end method

.method public getSelectedItem()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 744
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 746
    .local v2, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .line 748
    .local v1, "item":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 750
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 754
    .end local v1    # "item":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    :cond_1
    return-object v2
.end method

.method public getSelectedItemCount()I
    .locals 4

    .prologue
    .line 761
    const/4 v2, 0x0

    .line 763
    .local v2, "result":I
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .line 765
    .local v0, "entry":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 767
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 771
    .end local v0    # "entry":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    :cond_1
    return v2
.end method

.method public getSelectedItemPosition()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 730
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 732
    .local v0, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v2

    .line 734
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mItems:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mSelectedItem:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 735
    monitor-exit v2

    .line 737
    return-object v0

    .line 735
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 18
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 258
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mContext:Landroid/content/Context;

    const-string v15, "layout_inflater"

    invoke-virtual {v14, v15}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    .line 260
    .local v6, "inflater":Landroid/view/LayoutInflater;
    if-nez p2, :cond_9

    .line 262
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mViewMode:I

    const/4 v15, 0x2

    if-ne v14, v15, :cond_7

    .line 264
    const v14, 0x7f04000f

    const/4 v15, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v6, v14, v0, v15}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 306
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mViewMode:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->setId(I)V

    .line 308
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mItems:Ljava/util/ArrayList;

    move/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .line 310
    .local v7, "item":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    const/4 v12, 0x0

    .line 312
    .local v12, "selectRadio":Landroid/view/View;
    const/4 v11, 0x0

    .line 314
    .local v11, "selectCheckBox":Landroid/view/View;
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mSelectionType:I

    if-nez v14, :cond_d

    .line 316
    const v14, 0x7f0f0038

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    .line 318
    new-instance v14, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$1;

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v7}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$1;-><init>(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;)V

    invoke-virtual {v12, v14}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 341
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mIsSelectMode:Z

    if-eqz v14, :cond_c

    .line 343
    const/4 v14, 0x0

    invoke-virtual {v12, v14}, Landroid/view/View;->setVisibility(I)V

    move-object v14, v12

    .line 345
    check-cast v14, Landroid/widget/CompoundButton;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->isSelected()Z

    move-result v15

    invoke-virtual {v14, v15}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 381
    :goto_1
    const v14, 0x7f0f0067

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mDetailContainer:Landroid/view/ViewGroup;

    .line 383
    const v14, 0x7f0f0032

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 385
    .local v3, "appName":Landroid/widget/TextView;
    const v14, 0x7f0f0031

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 387
    .local v2, "appIcon":Landroid/widget/ImageView;
    const v14, 0x7f0f005a

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 389
    .local v10, "mSizeOrItems":Landroid/widget/TextView;
    const v14, 0x7f0f0069

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 391
    .local v9, "mDate":Landroid/widget/TextView;
    const v14, 0x7f0f0068

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    .line 393
    .local v13, "status":Landroid/widget/TextView;
    const v14, 0x7f0f006a

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 395
    .local v4, "divider":Landroid/widget/ImageView;
    if-eqz v4, :cond_1

    .line 397
    const/16 v14, 0x8

    invoke-virtual {v4, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 400
    :cond_1
    if-eqz v13, :cond_2

    .line 402
    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 404
    :cond_2
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mViewMode:I

    if-nez v14, :cond_f

    .line 406
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mDetailContainer:Landroid/view/ViewGroup;

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 417
    :cond_3
    :goto_2
    iget-object v14, v7, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->label:Ljava/lang/String;

    invoke-virtual {v3, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 419
    if-eqz v2, :cond_5

    .line 420
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mViewMode:I

    const/4 v15, 0x2

    if-eq v14, v15, :cond_4

    .line 421
    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    .line 422
    .local v8, "lp":Landroid/view/ViewGroup$LayoutParams;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f090018

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v14

    float-to-int v14, v14

    iput v14, v8, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 423
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f090017

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v14

    float-to-int v14, v14

    iput v14, v8, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 424
    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 426
    .end local v8    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :cond_4
    sget-object v14, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v14}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 427
    iget-object v14, v7, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->icon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v14}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 431
    :cond_5
    new-instance v5, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$3;

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v7}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$3;-><init>(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;)V

    .line 500
    .local v5, "hoverListener":Landroid/view/View$OnHoverListener;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mContext:Landroid/content/Context;

    const-string v15, "com.sec.feature.hovering_ui"

    invoke-static {v14, v15}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 502
    if-eqz v2, :cond_6

    .line 503
    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 507
    :cond_6
    return-object p2

    .line 268
    .end local v2    # "appIcon":Landroid/widget/ImageView;
    .end local v3    # "appName":Landroid/widget/TextView;
    .end local v4    # "divider":Landroid/widget/ImageView;
    .end local v5    # "hoverListener":Landroid/view/View$OnHoverListener;
    .end local v7    # "item":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    .end local v9    # "mDate":Landroid/widget/TextView;
    .end local v10    # "mSizeOrItems":Landroid/widget/TextView;
    .end local v11    # "selectCheckBox":Landroid/view/View;
    .end local v12    # "selectRadio":Landroid/view/View;
    .end local v13    # "status":Landroid/widget/TextView;
    :cond_7
    const v14, 0x7f040015

    const/4 v15, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v6, v14, v0, v15}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 270
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mViewMode:I

    const/4 v15, 0x1

    if-ne v14, v15, :cond_8

    .line 272
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f090014

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v14

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->setMinimumHeight(I)V

    goto/16 :goto_0

    .line 276
    :cond_8
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f090013

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v14

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->setMinimumHeight(I)V

    goto/16 :goto_0

    .line 282
    :cond_9
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getId()I

    move-result v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mViewMode:I

    if-eq v14, v15, :cond_0

    .line 284
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mViewMode:I

    const/4 v15, 0x2

    if-ne v14, v15, :cond_a

    .line 286
    const v14, 0x7f04000f

    const/4 v15, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v6, v14, v0, v15}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    goto/16 :goto_0

    .line 290
    :cond_a
    const v14, 0x7f040015

    const/4 v15, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v6, v14, v0, v15}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 292
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mViewMode:I

    const/4 v15, 0x1

    if-ne v14, v15, :cond_b

    .line 294
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f090014

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v14

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->setMinimumHeight(I)V

    goto/16 :goto_0

    .line 298
    :cond_b
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f090013

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v14

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->setMinimumHeight(I)V

    goto/16 :goto_0

    .line 349
    .restart local v7    # "item":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    .restart local v11    # "selectCheckBox":Landroid/view/View;
    .restart local v12    # "selectRadio":Landroid/view/View;
    :cond_c
    const/16 v14, 0x8

    invoke-virtual {v12, v14}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 354
    :cond_d
    const v14, 0x7f0f0037

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .line 356
    new-instance v14, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$2;

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v7}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$2;-><init>(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;)V

    invoke-virtual {v11, v14}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 368
    const/4 v14, 0x0

    invoke-virtual {v11, v14}, Landroid/view/View;->setClickable(Z)V

    .line 370
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mIsSelectMode:Z

    if-eqz v14, :cond_e

    .line 371
    const/4 v14, 0x0

    invoke-virtual {v11, v14}, Landroid/view/View;->setVisibility(I)V

    move-object v14, v11

    .line 372
    check-cast v14, Landroid/widget/CheckBox;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->isSelected()Z

    move-result v15

    invoke-virtual {v14, v15}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_1

    .line 376
    :cond_e
    const/16 v14, 0x8

    invoke-virtual {v11, v14}, Landroid/view/View;->setVisibility(I)V

    .line 377
    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/view/View;->setBackgroundColor(I)V

    goto/16 :goto_1

    .line 408
    .restart local v2    # "appIcon":Landroid/widget/ImageView;
    .restart local v3    # "appName":Landroid/widget/TextView;
    .restart local v4    # "divider":Landroid/widget/ImageView;
    .restart local v9    # "mDate":Landroid/widget/TextView;
    .restart local v10    # "mSizeOrItems":Landroid/widget/TextView;
    .restart local v13    # "status":Landroid/widget/TextView;
    :cond_f
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mViewMode:I

    const/4 v15, 0x1

    if-ne v14, v15, :cond_3

    .line 410
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mDetailContainer:Landroid/view/ViewGroup;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 412
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mContext:Landroid/content/Context;

    iget-wide v0, v7, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->size:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-static {v14, v0, v1}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v10, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 414
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mContext:Landroid/content/Context;

    iget-wide v0, v7, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->date:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-static {v14, v0, v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v9, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2
.end method

.method public isSelectMode()Z
    .locals 1

    .prologue
    .line 571
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mIsSelectMode:Z

    return v0
.end method

.method public multipleSelect(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 1080
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .line 1081
    .local v0, "item":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->isSelected()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->setSelected(Z)V

    .line 1083
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->notifyToSelectedItemChangeListener()V

    .line 1085
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->notifyDataSetChanged()V

    .line 1086
    return-void

    .line 1081
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected notifyToSelectedItemChangeListener()V
    .locals 3

    .prologue
    .line 712
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mSelectedItemChangeListener:Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;

    if-eqz v0, :cond_0

    .line 714
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mSelectionType:I

    packed-switch v0, :pswitch_data_0

    .line 724
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 720
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mSelectedItemChangeListener:Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->getSelectedItemCount()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;->multipleSelectionChanged(II)V

    goto :goto_0

    .line 714
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onRefresh()V
    .locals 2

    .prologue
    .line 514
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->loadInstalledApps()V

    .line 516
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mItems:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 518
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mCurrentSortBy:I

    packed-switch v0, :pswitch_data_0

    .line 535
    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;->onRefresh()V

    .line 536
    return-void

    .line 521
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mItems:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mAppTimeComparator:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_0

    .line 525
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mItems:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mAppNameComparator:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_0

    .line 529
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mItems:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mAppSizeComparator:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_0

    .line 518
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public selectAllItem()V
    .locals 3

    .prologue
    .line 610
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .line 612
    .local v1, "item":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->setSelected(Z)V

    goto :goto_0

    .line 615
    .end local v1    # "item":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->notifyToSelectedItemChangeListener()V

    .line 617
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->notifyDataSetChanged()V

    .line 618
    return-void
.end method

.method public selectItem(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 578
    const/4 v1, -0x1

    if-eq p1, v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_1

    .line 580
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v2

    .line 582
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .line 584
    .local v0, "item":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->isSelected()Z

    move-result v1

    if-nez v1, :cond_2

    .line 586
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->setSelected(Z)V

    .line 593
    :goto_0
    iget v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mSelectionType:I

    if-nez v1, :cond_0

    .line 595
    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mSelectedItem:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .line 597
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 599
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->notifyToSelectedItemChangeListener()V

    .line 601
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->notifyDataSetChanged()V

    .line 604
    .end local v0    # "item":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    :cond_1
    return-void

    .line 590
    .restart local v0    # "item":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    :cond_2
    const/4 v1, 0x0

    :try_start_1
    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->setSelected(Z)V

    goto :goto_0

    .line 597
    .end local v0    # "item":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public setViewMode(I)V
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 777
    iput p1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mViewMode:I

    .line 779
    return-void
.end method

.method public startSelectMode(II)V
    .locals 1
    .param p1, "selectionType"    # I
    .param p2, "from"    # I

    .prologue
    .line 542
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mIsSelectMode:Z

    .line 544
    iput p1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mSelectionType:I

    .line 546
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->notifyDataSetChanged()V

    .line 548
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->notifyToSelectedItemChangeListener()V

    .line 549
    return-void
.end method

.method public unselectAllItem()V
    .locals 3

    .prologue
    .line 624
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mSelectedItem:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .line 626
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .line 628
    .local v1, "item":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->setSelected(Z)V

    goto :goto_0

    .line 631
    .end local v1    # "item":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->notifyToSelectedItemChangeListener()V

    .line 633
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->notifyDataSetChanged()V

    .line 634
    return-void
.end method
