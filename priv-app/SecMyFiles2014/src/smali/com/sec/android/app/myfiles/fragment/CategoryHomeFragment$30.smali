.class Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$30;
.super Ljava/lang/Object;
.source "CategoryHomeFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->remoteShareClickListener()Landroid/widget/AdapterView$OnItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V
    .locals 0

    .prologue
    .line 3329
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$30;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 3334
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p2}, Landroid/view/View;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_1

    .line 3362
    :cond_0
    :goto_0
    return-void

    .line 3337
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$30;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mRemoteShareListAdapter:Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$600(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;

    move-result-object v3

    invoke-virtual {v3, p3}, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 3339
    .local v2, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    long-to-int v1, p4

    .line 3340
    .local v1, "categoryType":I
    const/4 v0, 0x0

    .line 3342
    .local v0, "argument":Landroid/os/Bundle;
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 3345
    :pswitch_0
    const/16 v1, 0x25

    .line 3346
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "argument":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 3347
    .restart local v0    # "argument":Landroid/os/Bundle;
    const-string v3, "REMOTE_SHARE_DIR"

    const/16 v4, 0x26

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3358
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$30;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v3, v3, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v3, :cond_0

    .line 3359
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$30;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setStartBrowserState(Z)V

    .line 3360
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$30;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v3, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v3, v1, v0}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 3350
    :pswitch_1
    const/16 v1, 0x25

    .line 3351
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "argument":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 3352
    .restart local v0    # "argument":Landroid/os/Bundle;
    const-string v3, "REMOTE_SHARE_DIR"

    const/16 v4, 0x27

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_1

    .line 3342
    :pswitch_data_0
    .packed-switch 0x26
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
