.class public abstract Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsFragment;
.source "AbsAddFTPFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment$4;
    }
.end annotation


# static fields
.field private static final SESSION_NAME_ALPHANUM_ONLY:Z


# instance fields
.field private mBackFromOK:Z

.field protected mDoneButton:Landroid/view/MenuItem;

.field private mFTPSessionListener:Lcom/sec/android/app/myfiles/ftp/IFTPSessionListener;

.field protected mOldParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

.field protected mShortcutIndex:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mBackFromOK:Z

    .line 590
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mShortcutIndex:I

    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mBackFromOK:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;Lcom/sec/android/app/myfiles/ftp/FTPParams;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;
    .param p1, "x1"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->storeFTPToDataStore(Lcom/sec/android/app/myfiles/ftp/FTPParams;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;)Lcom/sec/android/app/myfiles/ftp/IFTPSessionListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mFTPSessionListener:Lcom/sec/android/app/myfiles/ftp/IFTPSessionListener;

    return-object v0
.end method

.method private changeViewAfterKeyboardHidden()V
    .locals 1

    .prologue
    .line 295
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/Utils;->USE_SPLIT:Z

    if-nez v0, :cond_0

    .line 297
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->onSoftKeyboardHidden()V

    .line 309
    :cond_0
    return-void
.end method

.method protected static checkPassword(Ljava/lang/String;)Z
    .locals 2
    .param p0, "password"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 389
    if-eqz p0, :cond_0

    .line 390
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    .line 392
    :cond_0
    return v0
.end method

.method protected static checkPort(Ljava/lang/String;)Z
    .locals 6
    .param p0, "portString"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 347
    if-eqz p0, :cond_0

    .line 348
    const/4 v1, 0x0

    .line 350
    .local v1, "port":I
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 355
    if-lez v1, :cond_0

    const/high16 v3, 0x10000

    if-gt v1, v3, :cond_0

    const/4 v2, 0x1

    .line 357
    .end local v1    # "port":I
    :cond_0
    :goto_0
    return v2

    .line 351
    .restart local v1    # "port":I
    :catch_0
    move-exception v0

    .line 352
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v3, "MyFiles"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "AbsAddFTPFragment.checkPort: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", NumberFormatException."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected static checkServer(Ljava/lang/String;)Z
    .locals 2
    .param p0, "server"    # Ljava/lang/String;

    .prologue
    .line 339
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    .line 340
    const/4 v0, 0x1

    .line 342
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static checkSessionName(Ljava/lang/String;)Z
    .locals 4
    .param p0, "sessionName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 362
    const/4 v0, 0x1

    .line 363
    .local v0, "charsOk":Z
    if-eqz p0, :cond_2

    .line 372
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 377
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v2

    .line 372
    goto :goto_0

    .line 375
    :cond_2
    const/4 v0, 0x0

    .line 377
    if-eqz v0, :cond_3

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-gtz v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method protected static checkUsername(Ljava/lang/String;)Z
    .locals 2
    .param p0, "username"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 381
    if-eqz p0, :cond_0

    .line 382
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    .line 384
    :cond_0
    return v0
.end method

.method private onSoftKeyboardHidden()V
    .locals 3

    .prologue
    .line 279
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 280
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "shortcut_working_index_intent_key"

    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mShortcutIndex:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 281
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mBackFromOK:Z

    if-eqz v1, :cond_0

    .line 282
    const-string v1, "add_ftp"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 284
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView(Landroid/os/Bundle;)V

    .line 285
    return-void
.end method

.method private storeFTPToDataStore(Lcom/sec/android/app/myfiles/ftp/FTPParams;)V
    .locals 7
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .prologue
    .line 576
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mOldParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    if-eqz v0, :cond_0

    .line 578
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mShortcutIndex:I

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getSessionName()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Security;->getInstance()Lcom/sec/android/app/myfiles/utils/Security;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/utils/Security;->encrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/Utils;->updateShortcut(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)I

    .line 586
    :goto_0
    return-void

    .line 583
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getSessionName()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xa

    iget v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mShortcutIndex:I

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/myfiles/utils/Utils;->addShortCut(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/drawable/Drawable;Z)V

    goto :goto_0
.end method


# virtual methods
.method public AsyncOpenCompleted()V
    .locals 0

    .prologue
    .line 336
    return-void
.end method

.method public changeActionbar()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 672
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-nez v1, :cond_0

    .line 713
    :goto_0
    return-void

    .line 675
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 676
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 677
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 678
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 679
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040008

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 681
    .local v0, "customview":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 682
    const v1, 0x7f0f0008

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mActionbar_cancel:Landroid/widget/TextView;

    .line 683
    const v1, 0x7f0f0009

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mActionbar_ok:Landroid/widget/TextView;

    .line 684
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mActionbar_ok:Landroid/widget/TextView;

    const v2, 0x7f0b0016

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 685
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mActionbar_cancel:Landroid/widget/TextView;

    new-instance v2, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment$2;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 692
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mActionbar_ok:Landroid/widget/TextView;

    new-instance v2, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment$3;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 712
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->validate()V

    goto :goto_0
.end method

.method protected abstract checkData()Z
.end method

.method protected checkTextViewsTextSize(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/widget/TextView;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 502
    .local p1, "txtViews":Ljava/util/List;, "Ljava/util/List<Landroid/widget/TextView;>;"
    if-eqz p1, :cond_1

    .line 504
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 506
    .local v1, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0901b4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    iget v5, v1, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v2, v4

    .line 508
    .local v2, "txtSize":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 510
    .local v3, "txtView":Landroid/widget/TextView;
    if-eqz v3, :cond_0

    .line 512
    invoke-virtual {v3}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment$1;

    invoke-direct {v5, p0, v3, v2}, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;Landroid/widget/TextView;I)V

    invoke-virtual {v4, v5}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0

    .line 533
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "metrics":Landroid/util/DisplayMetrics;
    .end local v2    # "txtSize":I
    .end local v3    # "txtView":Landroid/widget/TextView;
    :cond_1
    return-void
.end method

.method protected dataHasChanged()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 551
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mOldParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    if-eqz v2, :cond_0

    .line 553
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->gatherParameters()Lcom/sec/android/app/myfiles/ftp/FTPParams;

    move-result-object v0

    .line 555
    .local v0, "newParams":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    if-eqz v0, :cond_0

    .line 558
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mOldParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getSessionName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getSessionName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mOldParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mOldParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getPort()I

    move-result v2

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getPort()I

    move-result v3

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mOldParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getMode()Lcom/sec/android/app/myfiles/ftp/FTPMode;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getMode()Lcom/sec/android/app/myfiles/ftp/FTPMode;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/ftp/FTPMode;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mOldParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getUsername()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getUsername()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mOldParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getPassword()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getPassword()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mOldParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getAnonymous()Z

    move-result v2

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getAnonymous()Z

    move-result v3

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mOldParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getEncoding()Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getEncoding()Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 571
    .end local v0    # "newParams":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    :cond_0
    :goto_0
    return v1

    .line 558
    .restart local v0    # "newParams":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public finishDragMode()V
    .locals 0

    .prologue
    .line 235
    return-void
.end method

.method protected abstract gatherParameters()Lcom/sec/android/app/myfiles/ftp/FTPParams;
.end method

.method protected getEncoding(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPEncoding;
    .locals 1
    .param p1, "encodingName"    # Ljava/lang/String;

    .prologue
    .line 432
    invoke-static {p1}, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->getValueOf(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    move-result-object v0

    return-object v0
.end method

.method protected getEncodingPosition(Lcom/sec/android/app/myfiles/ftp/FTPEncoding;)I
    .locals 1
    .param p1, "encoding"    # Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    .prologue
    .line 449
    const/4 v0, 0x0

    return v0
.end method

.method protected getEncryption(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPEncryption;
    .locals 1
    .param p1, "encryptionName"    # Ljava/lang/String;

    .prologue
    .line 453
    const v0, 0x7f0b00c2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 454
    sget-object v0, Lcom/sec/android/app/myfiles/ftp/FTPEncryption;->IMPLICIT:Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    .line 456
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/ftp/FTPEncryption;->EXPLICIT:Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    goto :goto_0
.end method

.method protected getEncryptionPosition(Lcom/sec/android/app/myfiles/ftp/FTPEncryption;)I
    .locals 3
    .param p1, "encryption"    # Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    .prologue
    const/4 v0, 0x0

    .line 461
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment$4;->$SwitchMap$com$sec$android$app$myfiles$ftp$FTPEncryption:[I

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPEncryption;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 467
    :goto_0
    :pswitch_0
    return v0

    .line 465
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 461
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected getMode(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPMode;
    .locals 1
    .param p1, "modeName"    # Ljava/lang/String;

    .prologue
    .line 413
    const v0, 0x7f0b00bb

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 414
    sget-object v0, Lcom/sec/android/app/myfiles/ftp/FTPMode;->PASSIVE:Lcom/sec/android/app/myfiles/ftp/FTPMode;

    .line 416
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/ftp/FTPMode;->ACTIVE:Lcom/sec/android/app/myfiles/ftp/FTPMode;

    goto :goto_0
.end method

.method protected getModePosition(Lcom/sec/android/app/myfiles/ftp/FTPMode;)I
    .locals 3
    .param p1, "mode"    # Lcom/sec/android/app/myfiles/ftp/FTPMode;

    .prologue
    const/4 v0, 0x1

    .line 421
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment$4;->$SwitchMap$com$sec$android$app$myfiles$ftp$FTPMode:[I

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPMode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 427
    :goto_0
    :pswitch_0
    return v0

    .line 423
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 421
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getOptionsMenuResId(Z)I
    .locals 1
    .param p1, "isSelectMode"    # Z

    .prologue
    .line 331
    const/4 v0, -0x1

    return v0
.end method

.method protected getPassword(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 409
    return-object p1
.end method

.method protected getPort(Ljava/lang/String;)I
    .locals 2
    .param p1, "portString"    # Ljava/lang/String;

    .prologue
    .line 397
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 398
    .local v0, "port":I
    if-lez v0, :cond_0

    const/high16 v1, 0x10000

    if-le v0, v1, :cond_1

    .line 399
    :cond_0
    const/16 v0, 0x15

    .line 401
    :cond_1
    return v0
.end method

.method protected getUsername(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p1, "username"    # Ljava/lang/String;

    .prologue
    .line 405
    return-object p1
.end method

.method public onBackPressed()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 313
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v3, :cond_0

    .line 314
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v4, "input_method"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 316
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->getView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    move-result v1

    .line 318
    .local v1, "isSoftKeyboardHiding":Z
    if-nez v1, :cond_1

    .line 319
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->onSoftKeyboardHidden()V

    .line 323
    :goto_0
    const/4 v2, 0x1

    .line 326
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    .end local v1    # "isSoftKeyboardHiding":Z
    :cond_0
    return v2

    .line 321
    .restart local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    .restart local v1    # "isSoftKeyboardHiding":Z
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->changeViewAfterKeyboardHidden()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 98
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 101
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->changeActionbar()V

    .line 102
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 57
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 59
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 61
    .local v0, "arg":Landroid/os/Bundle;
    if-eqz v0, :cond_1

    .line 63
    const-string v2, "FOLDERPATH"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 65
    .local v1, "paramString":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 69
    const-string v2, "FOLDERPATH"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 71
    new-instance v2, Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-direct {v2, v1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mOldParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .line 75
    :cond_0
    const-string v2, "shortcut_working_index_intent_key"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mShortcutIndex:I

    .line 92
    .end local v1    # "paramString":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->changeActionbar()V

    .line 93
    return-void
.end method

.method protected onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 243
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onCreateItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;
    .locals 1

    .prologue
    .line 248
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onCreateSelectedItemChangeListener()Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;
    .locals 1

    .prologue
    .line 253
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    .line 263
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onDestroyView()V

    .line 265
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 267
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 269
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 271
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    .line 275
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 159
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 160
    const/4 v0, 0x1

    return v0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 207
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 227
    return-void
.end method

.method public onRefresh()V
    .locals 0

    .prologue
    .line 258
    return-void
.end method

.method protected abstract restoreState(Lcom/sec/android/app/myfiles/ftp/FTPParams;)V
.end method

.method protected runRestore()V
    .locals 5

    .prologue
    .line 472
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 473
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 474
    const-string v3, "FOLDERPATH"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 475
    .local v2, "paramString":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 477
    :try_start_0
    new-instance v3, Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-direct {v3, v2}, Lcom/sec/android/app/myfiles/ftp/FTPParams;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mOldParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .line 478
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mOldParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->restoreState(Lcom/sec/android/app/myfiles/ftp/FTPParams;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 484
    .end local v2    # "paramString":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 479
    .restart local v2    # "paramString":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 480
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    const-string v3, "MyFiles"

    const-string v4, "AddFTPFragment: Cannot recognize the string."

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setFTPSessionListener(Lcom/sec/android/app/myfiles/ftp/IFTPSessionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPSessionListener;

    .prologue
    .line 497
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mFTPSessionListener:Lcom/sec/android/app/myfiles/ftp/IFTPSessionListener;

    .line 498
    return-void
.end method

.method public setSelectModeActionBar()V
    .locals 0

    .prologue
    .line 239
    return-void
.end method

.method public startDragMode(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "arg"    # Landroid/os/Bundle;

    .prologue
    .line 231
    return-void
.end method

.method protected validate()V
    .locals 2

    .prologue
    .line 487
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mActionbar_ok:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 488
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->checkData()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->dataHasChanged()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 489
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mActionbar_ok:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 494
    :cond_0
    :goto_0
    return-void

    .line 491
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mActionbar_ok:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0
.end method
