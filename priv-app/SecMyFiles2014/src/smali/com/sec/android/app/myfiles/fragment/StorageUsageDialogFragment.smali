.class public Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;
.super Landroid/app/DialogFragment;
.source "StorageUsageDialogFragment.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mStorageTypes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method private calculateStorageUsage(Lcom/sec/android/app/myfiles/view/StorageUsageView;I)V
    .locals 28
    .param p1, "storageUsageView"    # Lcom/sec/android/app/myfiles/view/StorageUsageView;
    .param p2, "storageType"    # I

    .prologue
    .line 209
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->clear()V

    .line 213
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, p2

    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getStorageCapacity(Landroid/content/Context;I)J

    move-result-wide v16

    .line 215
    .local v16, "totalSpace":J
    invoke-static/range {p2 .. p2}, Lcom/sec/android/app/myfiles/utils/Utils;->getStorageFreeSpace(I)J

    move-result-wide v6

    .line 217
    .local v6, "totalAvailableSpace":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v16

    invoke-static {v0, v1, v2}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->setTotalSpace(Ljava/lang/String;)V

    .line 219
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    sub-long v22, v16, v6

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-static {v0, v1, v2}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->setTotalAvailableSpace(Ljava/lang/String;)V

    .line 223
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, p2

    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getImageFilesSize(Landroid/content/Context;I)J

    move-result-wide v10

    .line 225
    .local v10, "totalImageSize":J
    const v20, 0x7f0b0003

    const v21, 0x7f020063

    const v22, 0x7f0200eb

    long-to-double v0, v10

    move-wide/from16 v24, v0

    move-wide/from16 v0, v16

    long-to-double v0, v0

    move-wide/from16 v26, v0

    div-double v24, v24, v26

    move-wide/from16 v0, v24

    double-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->addCategoryEntry(IIIF)V

    .line 232
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, p2

    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getVideoFilesSize(Landroid/content/Context;I)J

    move-result-wide v18

    .line 234
    .local v18, "totalVideoSize":J
    const v20, 0x7f0b0004

    const v21, 0x7f020066

    const v22, 0x7f0200ee

    move-wide/from16 v0, v18

    long-to-double v0, v0

    move-wide/from16 v24, v0

    move-wide/from16 v0, v16

    long-to-double v0, v0

    move-wide/from16 v26, v0

    div-double v24, v24, v26

    move-wide/from16 v0, v24

    double-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->addCategoryEntry(IIIF)V

    .line 241
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, p2

    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getMusicFilesSize(Landroid/content/Context;I)J

    move-result-wide v12

    .line 243
    .local v12, "totalMusicSize":J
    const v20, 0x7f0b0147

    const v21, 0x7f020064

    const v22, 0x7f0200ec

    long-to-double v0, v12

    move-wide/from16 v24, v0

    move-wide/from16 v0, v16

    long-to-double v0, v0

    move-wide/from16 v26, v0

    div-double v24, v24, v26

    move-wide/from16 v0, v24

    double-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->addCategoryEntry(IIIF)V

    .line 250
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, p2

    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getDocumentFilesSize(Landroid/content/Context;I)J

    move-result-wide v8

    .line 252
    .local v8, "totalDocumentSize":J
    const v20, 0x7f0b0006

    const v21, 0x7f020062

    const v22, 0x7f0200ea

    long-to-double v0, v8

    move-wide/from16 v24, v0

    move-wide/from16 v0, v16

    long-to-double v0, v0

    move-wide/from16 v26, v0

    div-double v24, v24, v26

    move-wide/from16 v0, v24

    double-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->addCategoryEntry(IIIF)V

    .line 259
    sub-long v20, v16, v6

    sub-long v20, v20, v10

    sub-long v20, v20, v18

    sub-long v20, v20, v12

    sub-long v14, v20, v8

    .line 261
    .local v14, "totalOthersSize":J
    const v20, 0x7f0b004b

    const v21, 0x7f020065

    const v22, 0x7f0200ed

    long-to-double v0, v14

    move-wide/from16 v24, v0

    move-wide/from16 v0, v16

    long-to-double v0, v0

    move-wide/from16 v26, v0

    div-double v24, v24, v26

    move-wide/from16 v0, v24

    double-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->addCategoryEntry(IIIF)V

    .line 266
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->commit()V

    .line 267
    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 23
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 53
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;->mContext:Landroid/content/Context;

    .line 55
    new-instance v5, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-direct {v5, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 57
    .local v5, "builder":Landroid/app/AlertDialog$Builder;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v9

    .line 59
    .local v9, "inflater":Landroid/view/LayoutInflater;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/utils/Utils;->getScreenState(Landroid/content/Context;)I

    move-result v13

    .line 61
    .local v13, "screenState":I
    const/4 v11, 0x1

    .line 65
    .local v11, "mCreateStorageUsageCount":I
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;->mStorageTypes:Ljava/util/ArrayList;

    .line 67
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;->mStorageTypes:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    const/16 v20, 0x101

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 69
    sget-boolean v19, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v19, :cond_0

    .line 71
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;->mStorageTypes:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    const/16 v20, 0x202

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    :cond_0
    sget-boolean v19, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-eqz v19, :cond_2

    .line 83
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getMountedUsbStorage()Ljava/util/ArrayList;

    move-result-object v12

    .line 85
    .local v12, "mountedUsb":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;>;"
    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;

    .line 87
    .local v18, "usb":Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->isMounted()Ljava/lang/Boolean;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19

    if-eqz v19, :cond_1

    .line 89
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;->mStorageTypes:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->getStorageType()I

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 94
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v12    # "mountedUsb":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;>;"
    .end local v18    # "usb":Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;->mStorageTypes:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v19

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_3

    .line 96
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;->mStorageTypes:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const/16 v21, 0x0

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 101
    :cond_3
    new-instance v14, Landroid/widget/ScrollView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-direct {v14, v0}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 102
    .local v14, "scrollview":Landroid/widget/ScrollView;
    new-instance v6, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-direct {v6, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 104
    .local v6, "container":Landroid/widget/LinearLayout;
    new-instance v10, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0900e9

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v19

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0900ea

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v20

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-direct {v10, v0, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 107
    .local v10, "lp":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {v6, v10}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 109
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 111
    const/16 v16, 0x0

    .line 113
    .local v16, "storageUsage":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;->mStorageTypes:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .restart local v8    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_a

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Integer;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v15

    .line 115
    .local v15, "storageType":I
    const v19, 0x7f04004a

    const/16 v20, 0x0

    move/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v9, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v16

    .line 117
    new-instance v10, Landroid/widget/LinearLayout$LayoutParams;

    .end local v10    # "lp":Landroid/view/ViewGroup$LayoutParams;
    const/16 v19, -0x2

    const/16 v20, -0x2

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-direct {v10, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 119
    .restart local v10    # "lp":Landroid/view/ViewGroup$LayoutParams;
    const v19, 0x7f0f010b

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Lcom/sec/android/app/myfiles/view/StorageUsageView;

    .line 122
    .local v17, "storageUsageView":Lcom/sec/android/app/myfiles/view/StorageUsageView;
    if-eqz v13, :cond_4

    const/16 v19, 0x2

    move/from16 v0, v19

    if-ne v13, v0, :cond_7

    .line 124
    :cond_4
    const/4 v13, 0x0

    .line 131
    :goto_2
    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->setScreenState(I)V

    .line 133
    const/16 v19, 0x1

    move/from16 v0, v19

    if-ne v11, v0, :cond_5

    .line 135
    const/16 v19, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0900eb

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v20

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    move/from16 v4, v22

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->setPadding(IIII)V

    .line 138
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;->mStorageTypes:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v19

    move/from16 v0, v19

    if-ne v11, v0, :cond_8

    .line 139
    const/16 v19, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v15, v1}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->setStorageType(IZ)V

    .line 144
    :goto_3
    if-nez v15, :cond_9

    .line 154
    :cond_6
    :goto_4
    move-object/from16 v0, v16

    invoke-virtual {v6, v0, v10}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 156
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v15}, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;->calculateStorageUsage(Lcom/sec/android/app/myfiles/view/StorageUsageView;I)V

    .line 158
    add-int/lit8 v11, v11, 0x1

    .line 159
    goto/16 :goto_1

    .line 128
    :cond_7
    const/4 v13, 0x1

    goto :goto_2

    .line 141
    :cond_8
    const/16 v19, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v15, v1}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->setStorageType(IZ)V

    goto :goto_3

    .line 146
    :cond_9
    const/16 v19, 0x101

    move/from16 v0, v19

    if-eq v15, v0, :cond_6

    .line 148
    const/16 v19, 0x202

    move/from16 v0, v19

    if-eq v15, v0, :cond_6

    .line 150
    invoke-static {v15}, Lcom/sec/android/app/myfiles/utils/Utils;->isUSBStorageType(I)Z

    move-result v19

    if-eqz v19, :cond_6

    goto :goto_4

    .line 160
    .end local v15    # "storageType":I
    .end local v17    # "storageUsageView":Lcom/sec/android/app/myfiles/view/StorageUsageView;
    :cond_a
    invoke-virtual {v14, v6, v10}, Landroid/widget/ScrollView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 162
    const v19, 0x7f0b00d6

    move/from16 v0, v19

    invoke-virtual {v5, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 164
    invoke-virtual {v5, v14}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 166
    const v19, 0x7f0b0015

    new-instance v20, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment$1;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;)V

    move/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v5, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 175
    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    .line 177
    .local v7, "dialog":Landroid/app/Dialog;
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v7, v0}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 185
    return-object v7
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 200
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/utils/Utils;->setStorageUsageIsShowing(Landroid/content/Context;Z)V

    .line 202
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 203
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 192
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/StorageUsageDialogFragment;->mContext:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/utils/Utils;->setStorageUsageIsShowing(Landroid/content/Context;Z)V

    .line 194
    return-void
.end method
