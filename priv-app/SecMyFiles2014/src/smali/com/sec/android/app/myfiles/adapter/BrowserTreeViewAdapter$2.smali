.class Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;
.super Ljava/lang/Object;
.source "BrowserTreeViewAdapter.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

.field final synthetic val$vh:Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;)V
    .locals 0

    .prologue
    .line 859
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    iput-object p2, p0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->val$vh:Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 24
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    .line 864
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;

    iget-object v0, v3, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mItem:Lcom/sec/android/app/myfiles/element/TreeViewItem;

    move-object/from16 v16, v0

    .line 866
    .local v16, "item":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getAction()I

    move-result v12

    .line 868
    .local v12, "action":I
    const/4 v3, 0x0

    const-string v4, "BrowserTreeViewAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onDrag() receive : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 870
    packed-switch v12, :pswitch_data_0

    .line 1092
    :cond_0
    :goto_0
    const/4 v3, 0x1

    :goto_1
    return v3

    .line 874
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->startDrag()V

    .line 876
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 882
    :pswitch_1
    move-object/from16 v0, v16

    iget v3, v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    if-nez v3, :cond_3

    .line 884
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    move-object/from16 v0, p1

    # setter for: Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mDragView:Landroid/view/View;
    invoke-static {v3, v0}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->access$002(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;Landroid/view/View;)Landroid/view/View;

    .line 886
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->access$100(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object/from16 v0, v16

    iget-boolean v3, v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mCurrentSelected:Z

    if-nez v3, :cond_2

    .line 888
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mSelectedItemBackground:I
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->access$200(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;)I

    move-result v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 890
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->val$vh:Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;

    # setter for: Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mDragViewHolder:Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;
    invoke-static {v3, v4}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->access$302(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;)Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;

    .line 892
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->val$vh:Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mSelectedItemTextColor:I
    invoke-static {v4}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->access$400(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 895
    :cond_2
    const/4 v2, 0x0

    .line 897
    .local v2, "anim":Landroid/view/animation/ScaleAnimation;
    new-instance v2, Landroid/view/animation/ScaleAnimation;

    .end local v2    # "anim":Landroid/view/animation/ScaleAnimation;
    const/high16 v3, 0x3f800000    # 1.0f

    const v4, 0x3f99999a    # 1.2f

    const/high16 v5, 0x3f800000    # 1.0f

    const v6, 0x3f99999a    # 1.2f

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/high16 v10, 0x3f000000    # 0.5f

    invoke-direct/range {v2 .. v10}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 901
    .restart local v2    # "anim":Landroid/view/animation/ScaleAnimation;
    const-wide/16 v4, 0x12c

    invoke-virtual {v2, v4, v5}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 903
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V

    .line 905
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v3

    if-nez v3, :cond_0

    .line 907
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_0

    .line 910
    .end local v2    # "anim":Landroid/view/animation/ScaleAnimation;
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->access$100(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_4

    move-object/from16 v0, v16

    iget-boolean v3, v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mCurrentSelected:Z

    if-nez v3, :cond_0

    .line 914
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->val$vh:Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const v4, 0x3ecccccd    # 0.4f

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setAlpha(F)V

    goto/16 :goto_0

    .line 922
    :pswitch_2
    move-object/from16 v0, v16

    iget v3, v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    if-nez v3, :cond_7

    .line 924
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->access$100(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_5

    move-object/from16 v0, v16

    iget-boolean v3, v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mCurrentSelected:Z

    if-nez v3, :cond_6

    .line 926
    :cond_5
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 928
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->val$vh:Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mItemOriginalTextColor:I
    invoke-static {v4}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->access$500(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 931
    :cond_6
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->clearAnimation()V

    goto/16 :goto_0

    .line 935
    :cond_7
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 937
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->val$vh:Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setAlpha(F)V

    goto/16 :goto_0

    .line 945
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->access$100(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_8

    move-object/from16 v0, v16

    iget-boolean v3, v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mCurrentSelected:Z

    if-nez v3, :cond_9

    .line 947
    :cond_8
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 949
    move-object/from16 v0, v16

    iget v3, v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    if-nez v3, :cond_9

    .line 951
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->val$vh:Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mItemOriginalTextColor:I
    invoke-static {v4}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->access$500(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 955
    :cond_9
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->clearAnimation()V

    .line 957
    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mPath:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isAbsoluteRootFolder(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 959
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->access$100(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0b00c9

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 964
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mDropListener:Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;

    if-eqz v3, :cond_15

    .line 966
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getClipData()Landroid/content/ClipData;

    move-result-object v13

    .line 968
    .local v13, "clipData":Landroid/content/ClipData;
    if-eqz v13, :cond_14

    invoke-virtual {v13}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v3

    if-eqz v3, :cond_14

    .line 970
    invoke-virtual {v13}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ClipDescription;->getLabel()Ljava/lang/CharSequence;

    move-result-object v19

    .line 972
    .local v19, "label":Ljava/lang/CharSequence;
    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_b

    const-string v3, "selectedUri"

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    :cond_b
    const-string v3, "selectedFTPUri"

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string v3, "selectedCloudUri"

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string v4, "galleryURI"

    move-object/from16 v3, v19

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 979
    :cond_c
    invoke-virtual {v13}, Landroid/content/ClipData;->getItemCount()I

    move-result v17

    .line 981
    .local v17, "itemCount":I
    const/16 v21, 0x0

    .line 983
    .local v21, "selectedFilePath":Ljava/lang/String;
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    .line 985
    .local v20, "selectedFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_2
    move/from16 v0, v17

    if-ge v15, v0, :cond_f

    .line 987
    invoke-virtual {v13, v15}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v18

    .line 989
    .local v18, "itemUri":Landroid/content/ClipData$Item;
    if-eqz v18, :cond_d

    .line 991
    invoke-virtual/range {v18 .. v18}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v22

    .line 993
    .local v22, "uri":Landroid/net/Uri;
    const-string v3, "selectedFTPUri"

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 995
    invoke-static/range {v22 .. v22}, Lcom/sec/android/app/myfiles/ftp/FTPUtils;->getItemPathFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v21

    .line 1003
    :goto_3
    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_d

    .line 1005
    invoke-virtual/range {v20 .. v21}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 985
    .end local v22    # "uri":Landroid/net/Uri;
    :cond_d
    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    .line 999
    .restart local v22    # "uri":Landroid/net/Uri;
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->access$100(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;)Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-static {v3, v0}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getFilePathFromUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v21

    goto :goto_3

    .line 1010
    .end local v18    # "itemUri":Landroid/content/ClipData$Item;
    .end local v22    # "uri":Landroid/net/Uri;
    :cond_f
    const/4 v14, 0x0

    .line 1012
    .local v14, "dstFolder":Ljava/lang/String;
    move-object/from16 v0, v16

    iget v3, v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    if-nez v3, :cond_11

    .line 1014
    move-object/from16 v0, v16

    iget-object v14, v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mPath:Ljava/lang/String;

    .line 1023
    :cond_10
    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mDropListener:Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;

    move-object/from16 v0, v20

    invoke-interface {v3, v14, v0}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;->onDrop(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 1016
    :cond_11
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->access$100(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_12

    move-object/from16 v0, v16

    iget-boolean v3, v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mCurrentSelected:Z

    if-nez v3, :cond_10

    .line 1018
    :cond_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->val$vh:Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setAlpha(F)V

    goto :goto_4

    .line 1028
    .end local v14    # "dstFolder":Ljava/lang/String;
    .end local v15    # "i":I
    .end local v17    # "itemCount":I
    .end local v20    # "selectedFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v21    # "selectedFilePath":Ljava/lang/String;
    :cond_13
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 1034
    .end local v19    # "label":Ljava/lang/CharSequence;
    :cond_14
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 1040
    .end local v13    # "clipData":Landroid/content/ClipData;
    :cond_15
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 1048
    :pswitch_4
    move-object/from16 v0, v16

    iget v3, v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    if-eqz v3, :cond_16

    .line 1050
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1052
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->val$vh:Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const v4, 0x7f0200af

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1056
    :cond_16
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->val$vh:Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    move-object/from16 v0, v16

    iget-boolean v4, v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mEnabled:Z

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1058
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    iget-boolean v3, v3, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mIsDragMode:Z

    if-nez v3, :cond_17

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->access$100(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->getScreenState(Landroid/content/Context;)I

    move-result v3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_17

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->access$100(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->getScreenState(Landroid/content/Context;)I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    .line 1061
    :cond_17
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mDragView:Landroid/view/View;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_19

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mDragViewHolder:Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->access$300(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;)Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;

    move-result-object v3

    if-eqz v3, :cond_19

    .line 1063
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mDragView:Landroid/view/View;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->clearAnimation()V

    .line 1065
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->access$100(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_18

    move-object/from16 v0, v16

    iget-boolean v3, v0, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mCurrentSelected:Z

    if-nez v3, :cond_19

    .line 1067
    :cond_18
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mDragView:Landroid/view/View;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1069
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mDragViewHolder:Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->access$300(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;)Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mItemOriginalTextColor:I
    invoke-static {v4}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->access$500(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1073
    :cond_19
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->finishDrag()V

    .line 1075
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0

    .line 1081
    :pswitch_5
    const/4 v3, 0x2

    new-array v11, v3, [I

    .line 1082
    .local v11, "a":[I
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/view/View;->getLocationInWindow([I)V

    .line 1083
    const/4 v3, 0x1

    aget v23, v11, v3

    .line 1084
    .local v23, "y":I
    const/16 v3, 0xdc

    move/from16 v0, v23

    if-ge v0, v3, :cond_1a

    .line 1085
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    const/16 v4, -0x20

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->scrollListBy(I)V

    goto/16 :goto_0

    .line 1086
    :cond_1a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int v4, v4, v23

    int-to-float v4, v4

    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getY()F

    move-result v5

    add-float/2addr v4, v5

    cmpg-float v3, v3, v4

    if-gez v3, :cond_0

    .line 1087
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->scrollListBy(I)V

    goto/16 :goto_0

    .line 870
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_5
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
