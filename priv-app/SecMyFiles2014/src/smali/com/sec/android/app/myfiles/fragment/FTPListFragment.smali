.class public Lcom/sec/android/app/myfiles/fragment/FTPListFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;
.source "FTPListFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/fragment/FTPListFragment$3;
    }
.end annotation


# instance fields
.field private mSelector:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mSelector:Z

    return-void
.end method

.method private static runAddFTP(Lcom/sec/android/app/myfiles/ftp/FTPType;Landroid/app/Activity;I)V
    .locals 4
    .param p0, "ftp"    # Lcom/sec/android/app/myfiles/ftp/FTPType;
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "shortcutIndex"    # I

    .prologue
    .line 266
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 267
    .local v0, "argument":Landroid/os/Bundle;
    const-string v2, "shortcut_working_index_intent_key"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 268
    const-class v2, Lcom/sec/android/app/myfiles/ftp/FTPType;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPType;->ID:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 269
    const/16 v1, 0xd

    .line 270
    .local v1, "fragmentId":I
    sget-object v2, Lcom/sec/android/app/myfiles/fragment/FTPListFragment$3;->$SwitchMap$com$sec$android$app$myfiles$ftp$FTPType:[I

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 281
    :goto_0
    instance-of v2, p1, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v2, :cond_0

    .line 282
    check-cast p1, Lcom/sec/android/app/myfiles/MainActivity;

    .end local p1    # "activity":Landroid/app/Activity;
    invoke-virtual {p1, v1, v0}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    .line 284
    :cond_0
    return-void

    .line 272
    .restart local p1    # "activity":Landroid/app/Activity;
    :pswitch_0
    const/16 v1, 0xd

    .line 273
    goto :goto_0

    .line 275
    :pswitch_1
    const/16 v1, 0xe

    .line 276
    goto :goto_0

    .line 278
    :pswitch_2
    const/16 v1, 0xf

    goto :goto_0

    .line 270
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public AsyncOpenCompleted()V
    .locals 0

    .prologue
    .line 205
    return-void
.end method

.method protected changeViewMode(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    const/4 v2, 0x0

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setVisibility(I)V

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->setVisibility(I)V

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->getAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 180
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->setBrowserAdapterViewMode(I)V

    .line 182
    return-void
.end method

.method protected deleteSelectedItem()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 305
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v2

    .line 307
    .local v2, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v3, v2}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteFTPShortcuts(Landroid/content/Context;Ljava/util/ArrayList;)I

    move-result v1

    .line 309
    .local v1, "deletedRows":I
    if-lez v1, :cond_0

    .line 310
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v4, 0x7f0b00d8

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 316
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v0

    .line 317
    .local v0, "c":Landroid/database/Cursor;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->finishSelectMode()V

    .line 318
    if-nez v0, :cond_1

    .line 319
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v3, :cond_1

    .line 320
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView()V

    .line 325
    :goto_1
    return-void

    .line 312
    .end local v0    # "c":Landroid/database/Cursor;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v4, 0x7f0b0063

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 324
    .restart local v0    # "c":Landroid/database/Cursor;
    :cond_1
    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->updateAdapter(Landroid/database/Cursor;)V

    goto :goto_1
.end method

.method public getOptionsMenuResId(Z)I
    .locals 2
    .param p1, "isSelectMode"    # Z

    .prologue
    const v0, 0x7f0e0018

    .line 187
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mSelector:Z

    if-eqz v1, :cond_1

    .line 197
    :cond_0
    :goto_0
    return v0

    .line 190
    :cond_1
    if-eqz p1, :cond_2

    .line 191
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->getSelectModeFrom()I

    move-result v1

    if-nez v1, :cond_0

    .line 192
    const v0, 0x7f0e000f

    goto :goto_0

    .line 197
    :cond_2
    const v0, 0x7f0e000e

    goto :goto_0
.end method

.method public onBackPressed()Z
    .locals 1

    .prologue
    .line 147
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mSelector:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 148
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->finishSelectMode()V

    .line 154
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 150
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 54
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onCreate(Landroid/os/Bundle;)V

    .line 55
    return-void
.end method

.method protected onCreateDetailFragmentListener()Landroid/app/DialogFragment;
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onCreateFileOperationListener()Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 112
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/FTPListFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/FTPListFragment;)V

    return-object v0
.end method

.method protected onCreateItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;
    .locals 1

    .prologue
    .line 159
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/FTPListFragment$2;-><init>(Lcom/sec/android/app/myfiles/fragment/FTPListFragment;)V

    return-object v0
.end method

.method protected onCreateMultipleDetailFragmentListener()Landroid/app/DialogFragment;
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 72
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 74
    .local v0, "view":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setOnPathChangeListener(Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;)V

    .line 75
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    const-string v2, "FFTP"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 76
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .line 77
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 79
    return-object v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 13
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 209
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 210
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 262
    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 213
    :sswitch_0
    sget-object v0, Lcom/sec/android/app/myfiles/ftp/FTPType;->FTP:Lcom/sec/android/app/myfiles/ftp/FTPType;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->runAddFTP(Lcom/sec/android/app/myfiles/ftp/FTPType;Landroid/app/Activity;I)V

    .line 215
    const/4 v0, 0x1

    goto :goto_0

    .line 218
    :sswitch_1
    const/4 v0, 0x1

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->startSelectMode(III)V

    .line 220
    const/4 v0, 0x1

    goto :goto_0

    .line 223
    :sswitch_2
    const/4 v0, 0x1

    const/4 v1, -0x1

    const/4 v2, 0x5

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->startSelectMode(III)V

    .line 225
    const/4 v0, 0x1

    goto :goto_0

    .line 228
    :sswitch_3
    const-string v9, ""

    .line 229
    .local v9, "index":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v12

    .line 230
    .local v12, "selectedItems":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    const/4 v0, 0x0

    invoke-interface {v12, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .line 231
    .local v11, "selectItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    const-string v3, "_data = ?"

    .line 232
    .local v3, "where":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v1, v11, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    aput-object v1, v4, v0

    .line 233
    .local v4, "whereArgs":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Shortcut;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 235
    .local v7, "cursor":Landroid/database/Cursor;
    if-nez v7, :cond_1

    .line 236
    const/4 v0, 0x0

    goto :goto_0

    .line 237
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 239
    :cond_2
    const-string v0, "shortcut_index"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 242
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_2

    .line 244
    :cond_3
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 245
    .local v6, "argument":Landroid/os/Bundle;
    const-string v0, "FOLDERPATH"

    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Security;->getInstance()Lcom/sec/android/app/myfiles/utils/Security;

    move-result-object v1

    iget-object v2, v11, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/utils/Security;->decrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    const/4 v10, 0x0

    .line 248
    .local v10, "indexInt":I
    :try_start_0
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v10

    .line 253
    :goto_1
    const-string v0, "shortcut_working_index_intent_key"

    invoke-virtual {v6, v0, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 254
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v0, :cond_4

    .line 255
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    const/16 v1, 0xd

    invoke-virtual {v0, v1, v6}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    .line 256
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/MainActivity;->closeActionmode()V

    .line 258
    :cond_4
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 259
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 249
    :catch_0
    move-exception v8

    .line 250
    .local v8, "ex":Ljava/lang/NumberFormatException;
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 251
    invoke-virtual {v8}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_1

    .line 210
    :sswitch_data_0
    .sparse-switch
        0x7f0f0132 -> :sswitch_3
        0x7f0f0137 -> :sswitch_1
        0x7f0f0138 -> :sswitch_2
        0x7f0f0140 -> :sswitch_0
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v2, 0x7f0f0140

    .line 84
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->getFTPShortCutCount(Landroid/content/Context;)I

    move-result v0

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    .line 88
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 89
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 93
    :cond_0
    return-void
.end method

.method public onRefresh()V
    .locals 0

    .prologue
    .line 143
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 65
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onResume()V

    .line 66
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->showPathIndicator(Z)V

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    const-string v1, "13"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->setCategoryItemListSelection(Ljava/lang/String;)I

    .line 68
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 60
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onStart()V

    .line 61
    return-void
.end method

.method protected selectAllItem()V
    .locals 1

    .prologue
    .line 288
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->selectAllItem()V

    .line 289
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->selectAllItem()V

    .line 292
    :cond_0
    return-void
.end method

.method protected unselectAllItem()V
    .locals 1

    .prologue
    .line 296
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->unselectAllItem()V

    .line 297
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->unselectAllItem()V

    .line 300
    :cond_0
    return-void
.end method
