.class public Lcom/sec/android/app/myfiles/element/CategoryItem;
.super Ljava/lang/Object;
.source "CategoryItem.java"


# instance fields
.field private mId:I

.field private mPath:Ljava/lang/String;

.field private mSelected:Z

.field private mTitle:Ljava/lang/String;

.field private mTotalSize:J

.field private mType:I


# direct methods
.method public constructor <init>(ILjava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "id"    # I
    .param p4, "path"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/element/CategoryItem;->mSelected:Z

    .line 47
    iput p1, p0, Lcom/sec/android/app/myfiles/element/CategoryItem;->mType:I

    .line 49
    iput-object p2, p0, Lcom/sec/android/app/myfiles/element/CategoryItem;->mTitle:Ljava/lang/String;

    .line 51
    iput p3, p0, Lcom/sec/android/app/myfiles/element/CategoryItem;->mId:I

    .line 53
    iput-object p4, p0, Lcom/sec/android/app/myfiles/element/CategoryItem;->mPath:Ljava/lang/String;

    .line 54
    return-void
.end method


# virtual methods
.method public declared-synchronized getId()I
    .locals 1

    .prologue
    .line 71
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/sec/android/app/myfiles/element/CategoryItem;->mId:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/element/CategoryItem;->mPath:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getSelected()Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/element/CategoryItem;->mSelected:Z

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/myfiles/element/CategoryItem;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized getTotalSize()J
    .locals 2

    .prologue
    .line 108
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/element/CategoryItem;->mTotalSize:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/sec/android/app/myfiles/element/CategoryItem;->mType:I

    return v0
.end method

.method public declared-synchronized setIcon(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 78
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/sec/android/app/myfiles/element/CategoryItem;->mId:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 79
    monitor-exit p0

    return-void

    .line 78
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setPath(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 90
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/sec/android/app/myfiles/element/CategoryItem;->mPath:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91
    monitor-exit p0

    return-void

    .line 90
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setSelected(Z)V
    .locals 0
    .param p1, "selected"    # Z

    .prologue
    .line 102
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/element/CategoryItem;->mSelected:Z

    .line 103
    return-void
.end method

.method public declared-synchronized setTotalSize(J)V
    .locals 1
    .param p1, "totalSize"    # J

    .prologue
    .line 114
    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lcom/sec/android/app/myfiles/element/CategoryItem;->mTotalSize:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    monitor-exit p0

    return-void

    .line 114
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
