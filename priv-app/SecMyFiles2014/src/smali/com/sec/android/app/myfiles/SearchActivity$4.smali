.class Lcom/sec/android/app/myfiles/SearchActivity$4;
.super Ljava/lang/Object;
.source "SearchActivity.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/SearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/SearchActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/SearchActivity;)V
    .locals 0

    .prologue
    .line 682
    iput-object p1, p0, Lcom/sec/android/app/myfiles/SearchActivity$4;->this$0:Lcom/sec/android/app/myfiles/SearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v0, 0x1

    .line 686
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 701
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 690
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SearchActivity$4;->this$0:Lcom/sec/android/app/myfiles/SearchActivity;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onRefresh()V

    goto :goto_0

    .line 696
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SearchActivity$4;->this$0:Lcom/sec/android/app/myfiles/SearchActivity;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/SearchActivity$4;->this$0:Lcom/sec/android/app/myfiles/SearchActivity;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v2}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/SearchActivity;->onQueryTextSubmit(Ljava/lang/String;)Z

    goto :goto_0

    .line 686
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
