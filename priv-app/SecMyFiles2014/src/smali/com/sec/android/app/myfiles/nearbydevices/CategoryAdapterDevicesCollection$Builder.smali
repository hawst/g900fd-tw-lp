.class public Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;
.super Ljava/lang/Object;
.source "CategoryAdapterDevicesCollection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDevicesCollectionUpdatedListener:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;

.field private final mShortcutAdapter:Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesShortcutAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "devicesCollectionUpdatedListener"    # Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ShortcutViewAdapter:",
            "Ljava/lang/Object;",
            "mShortcutAdapter:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/content/Context;",
            "Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 49
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 50
    :cond_1
    iput-object p1, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;->mContext:Landroid/content/Context;

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;->mShortcutAdapter:Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesShortcutAdapter;

    .line 53
    iput-object p2, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;->mDevicesCollectionUpdatedListener:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;

    .line 54
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;)Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;->mDevicesCollectionUpdatedListener:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;

    return-object v0
.end method


# virtual methods
.method public build()Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;
    .locals 2

    .prologue
    .line 61
    new-instance v0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;-><init>(Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$1;)V

    return-object v0
.end method

.method public refreshView(Ljava/lang/Runnable;)Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;
    .locals 0
    .param p1, "refreshView"    # Ljava/lang/Runnable;

    .prologue
    .line 57
    return-object p0
.end method
