.class public Lcom/sec/android/app/myfiles/utils/FileUtils;
.super Ljava/lang/Object;
.source "FileUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/utils/FileUtils$FileOperation;
    }
.end annotation


# static fields
.field private static final MODULE:Ljava/lang/String; = "FileUtils"

.field private static mConvertThread:Ljava/lang/Thread;

.field private static mIsExtractSuccessRefresh:Z

.field public static mediaList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/FileUtils;->mediaList:Ljava/util/HashMap;

    .line 72
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/myfiles/utils/FileUtils;->mConvertThread:Ljava/lang/Thread;

    .line 73
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/FileUtils;->mIsExtractSuccessRefresh:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    return-void
.end method

.method public static InitMediaList()V
    .locals 1

    .prologue
    .line 1266
    sget-object v0, Lcom/sec/android/app/myfiles/utils/FileUtils;->mediaList:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 1268
    sget-object v0, Lcom/sec/android/app/myfiles/utils/FileUtils;->mediaList:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1270
    :cond_0
    return-void
.end method

.method public static OpenFileDialog(Landroid/app/Activity;)V
    .locals 6
    .param p0, "context"    # Landroid/app/Activity;

    .prologue
    .line 1875
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1877
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f040038

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 1879
    .local v2, "openfileAlter":Landroid/view/View;
    const v3, 0x7f0f00cf

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1881
    .local v1, "openfile":Landroid/widget/TextView;
    const v3, 0x7f0b00ee

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 1883
    const v3, 0x7f0b0064

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1885
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 1887
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    .line 1889
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 1890
    return-void
.end method

.method public static addPostfix(Ljava/io/File;I)Ljava/lang/String;
    .locals 8
    .param p0, "f"    # Ljava/io/File;
    .param p1, "postfixNum"    # I

    .prologue
    const/16 v7, 0x29

    .line 1539
    const/4 v4, 0x0

    .line 1541
    .local v4, "resultStr":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1543
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1545
    .local v5, "sb":Ljava/lang/StringBuffer;
    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1547
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1578
    :goto_0
    return-object v4

    .line 1551
    .end local v5    # "sb":Ljava/lang/StringBuffer;
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 1553
    .local v1, "fullName":Ljava/lang/String;
    const/16 v6, 0x2e

    invoke-virtual {v1, v6}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    .line 1556
    .local v2, "lastDot":I
    const/4 v6, -0x1

    if-eq v2, v6, :cond_1

    .line 1558
    const/4 v6, 0x0

    invoke-virtual {v1, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 1560
    .local v3, "name":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v1, v2, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1562
    .local v0, "ext":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1564
    .restart local v5    # "sb":Ljava/lang/StringBuffer;
    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1566
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1568
    goto :goto_0

    .line 1570
    .end local v0    # "ext":Ljava/lang/String;
    .end local v3    # "name":Ljava/lang/String;
    .end local v5    # "sb":Ljava/lang/StringBuffer;
    :cond_1
    new-instance v5, Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1572
    .restart local v5    # "sb":Ljava/lang/StringBuffer;
    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 1574
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public static changeToImageFilePath(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 7
    .param p0, "folder"    # Ljava/lang/String;
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "allFrame"    # Z

    .prologue
    .line 1730
    :try_start_0
    const-string v6, ".golf"

    invoke-virtual {p1, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    .line 1731
    .local v3, "indexEnd":I
    const-string v6, "/"

    invoke-virtual {p1, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    .line 1732
    .local v2, "indexBegin":I
    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 1734
    .local v1, "fileName":Ljava/lang/String;
    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    .line 1736
    if-nez p2, :cond_0

    .line 1738
    const/4 v6, 0x1

    invoke-virtual {p1, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 1741
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1742
    .local v4, "strBuild":Ljava/lang/StringBuilder;
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1743
    const-string v6, "_"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1744
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1745
    .local v5, "tempPath":Ljava/lang/String;
    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1746
    const-string v6, ".jpg"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1747
    invoke-static {v5, p1, p2}, Lcom/sec/android/app/myfiles/utils/FileUtils;->generateGolfFile(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1748
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 1752
    .end local v1    # "fileName":Ljava/lang/String;
    .end local v2    # "indexBegin":I
    .end local v3    # "indexEnd":I
    .end local v4    # "strBuild":Ljava/lang/StringBuilder;
    .end local v5    # "tempPath":Ljava/lang/String;
    :goto_0
    return-object v6

    .line 1749
    :catch_0
    move-exception v0

    .line 1750
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 1752
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public static convertShare(Landroid/content/Context;Lcom/quramsoft/qdio/QdioJNI$QdioJPEGData;Ljava/lang/String;Landroid/os/Handler;)Z
    .locals 17
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "qdioData"    # Lcom/quramsoft/qdio/QdioJNI$QdioJPEGData;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "ProgressHandler"    # Landroid/os/Handler;

    .prologue
    .line 1583
    new-instance v4, Ljava/io/File;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "/RW_LIB"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v4, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1585
    .local v4, "folder":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v15

    if-nez v15, :cond_0

    .line 1586
    invoke-virtual {v4}, Ljava/io/File;->mkdir()Z

    .line 1588
    :cond_0
    move-object/from16 v1, p2

    .line 1589
    .local v1, "drcpath":Ljava/lang/String;
    const/16 v15, 0x2e

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v5

    .line 1590
    .local v5, "index":I
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v1, v0, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ".mp4"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1592
    new-instance v15, Ljava/io/File;

    invoke-direct {v15, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_1

    .line 1594
    const/4 v15, 0x6

    move-object/from16 v0, p3

    invoke-static {v0, v15, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v8

    .line 1595
    .local v8, "msg":Landroid/os/Message;
    move-object/from16 v0, p3

    invoke-virtual {v0, v8}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1596
    const/4 v15, 0x0

    .line 1646
    .end local v8    # "msg":Landroid/os/Message;
    :goto_0
    return v15

    .line 1599
    :cond_1
    const/16 v13, 0x96

    .line 1600
    .local v13, "outputSize":I
    const/4 v11, 0x5

    .line 1602
    .local v11, "outFileResolution":I
    new-instance v9, Lcom/samsung/app/share/via/external/NativeAccess;

    invoke-direct {v9}, Lcom/samsung/app/share/via/external/NativeAccess;-><init>()V

    .line 1603
    .local v9, "nativeAccess":Lcom/samsung/app/share/via/external/NativeAccess;
    new-instance v10, Lcom/samsung/app/share/via/external/ShareviaObj;

    invoke-direct {v10}, Lcom/samsung/app/share/via/external/ShareviaObj;-><init>()V

    .line 1604
    .local v10, "obj":Lcom/samsung/app/share/via/external/ShareviaObj;
    const/4 v14, 0x0

    .line 1605
    .local v14, "start_offset":I
    const/4 v6, 0x0

    .line 1609
    .local v6, "length":I
    const/4 v15, 0x0

    :try_start_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/quramsoft/qdio/QdioJNI$QdioJPEGData;->getStartOffset(I)I

    move-result v14

    .line 1610
    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/quramsoft/qdio/QdioJNI$QdioJPEGData;->getLength(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    .line 1616
    :goto_1
    move v7, v13

    .line 1617
    .local v7, "maxOutFileSize":I
    move v3, v11

    .line 1618
    .local v3, "expectedResolution":I
    const/4 v12, 0x0

    .line 1619
    .local v12, "outputFileTimePossible":I
    const/4 v15, 0x5

    move-object/from16 v0, p2

    invoke-virtual {v9, v0, v7, v3, v15}, Lcom/samsung/app/share/via/external/NativeAccess;->getInputParamerterAnalysis(Ljava/lang/String;III)I

    move-result v12

    .line 1621
    move-object/from16 v0, p2

    invoke-virtual {v10, v0}, Lcom/samsung/app/share/via/external/ShareviaObj;->setShareViaInputFilename(Ljava/lang/String;)V

    .line 1622
    invoke-virtual {v10, v1}, Lcom/samsung/app/share/via/external/ShareviaObj;->setShareViaOutputFilename(Ljava/lang/String;)V

    .line 1624
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v15

    invoke-virtual {v10, v15}, Lcom/samsung/app/share/via/external/ShareviaObj;->setShareViaAssetmngr(Landroid/content/res/AssetManager;)V

    .line 1625
    const-string v15, "gallery_detail_ic_sound_scene.png"

    invoke-virtual {v10, v15}, Lcom/samsung/app/share/via/external/ShareviaObj;->setShareViaIconFileName(Ljava/lang/String;)V

    .line 1627
    invoke-virtual {v10, v11}, Lcom/samsung/app/share/via/external/ShareviaObj;->setShareViaOutputFileResolution(I)V

    .line 1628
    const/4 v15, 0x0

    invoke-virtual {v10, v15}, Lcom/samsung/app/share/via/external/ShareviaObj;->setShareViaStartTime(I)V

    .line 1629
    add-int/lit16 v15, v12, 0x3a98

    invoke-virtual {v10, v15}, Lcom/samsung/app/share/via/external/ShareviaObj;->setShareViaEndTime(I)V

    .line 1631
    const v15, 0xf4240

    invoke-virtual {v10, v15}, Lcom/samsung/app/share/via/external/ShareviaObj;->setShareViamaxDuration(I)V

    .line 1632
    invoke-virtual {v10, v7}, Lcom/samsung/app/share/via/external/ShareviaObj;->setShareViamaxSize(I)V

    .line 1633
    const/4 v15, 0x2

    invoke-virtual {v10, v15}, Lcom/samsung/app/share/via/external/ShareviaObj;->setShareViaVideoCodec(I)V

    .line 1634
    const/4 v15, 0x5

    invoke-virtual {v10, v15}, Lcom/samsung/app/share/via/external/ShareviaObj;->setShareViaAudioCodec(I)V

    .line 1637
    invoke-virtual {v10, v14}, Lcom/samsung/app/share/via/external/ShareviaObj;->setShareViaAudioOffset(I)V

    .line 1638
    invoke-virtual {v10, v6}, Lcom/samsung/app/share/via/external/ShareviaObj;->setShareViaAudioLength(I)V

    .line 1640
    const/4 v15, 0x1

    invoke-virtual {v10, v15}, Lcom/samsung/app/share/via/external/ShareviaObj;->setShareViaTranscodeMode(I)V

    .line 1642
    new-instance v15, Lcom/samsung/app/share/via/external/ConvertThread;

    move-object/from16 v0, p3

    invoke-direct {v15, v9, v10, v0}, Lcom/samsung/app/share/via/external/ConvertThread;-><init>(Lcom/samsung/app/share/via/external/NativeAccess;Lcom/samsung/app/share/via/external/ShareviaObj;Landroid/os/Handler;)V

    sput-object v15, Lcom/sec/android/app/myfiles/utils/FileUtils;->mConvertThread:Ljava/lang/Thread;

    .line 1644
    sget-object v15, Lcom/sec/android/app/myfiles/utils/FileUtils;->mConvertThread:Ljava/lang/Thread;

    invoke-virtual {v15}, Ljava/lang/Thread;->start()V

    .line 1646
    const/4 v15, 0x1

    goto :goto_0

    .line 1612
    .end local v3    # "expectedResolution":I
    .end local v7    # "maxOutFileSize":I
    .end local v12    # "outputFileTimePossible":I
    :catch_0
    move-exception v2

    .line 1613
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public static convertShare(Landroid/content/Context;Ljava/lang/String;Landroid/os/Handler;)Z
    .locals 15
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "ProgressHandler"    # Landroid/os/Handler;

    .prologue
    .line 1652
    new-instance v3, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v14

    invoke-virtual {v14}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/RW_LIB"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v3, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1654
    .local v3, "folder":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v13

    if-nez v13, :cond_0

    .line 1655
    invoke-virtual {v3}, Ljava/io/File;->mkdir()Z

    .line 1657
    :cond_0
    move-object/from16 v1, p1

    .line 1658
    .local v1, "drcpath":Ljava/lang/String;
    const/16 v13, 0x2e

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    .line 1659
    .local v4, "index":I
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v14, 0x0

    invoke-virtual {v1, v14, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ".mp4"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1661
    new-instance v13, Ljava/io/File;

    invoke-direct {v13, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v13

    if-eqz v13, :cond_1

    .line 1663
    const/4 v13, 0x6

    move-object/from16 v0, p2

    invoke-static {v0, v13, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    .line 1664
    .local v6, "msg":Landroid/os/Message;
    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1665
    const/4 v13, 0x0

    .line 1713
    .end local v6    # "msg":Landroid/os/Message;
    :goto_0
    return v13

    .line 1668
    :cond_1
    const/16 v11, 0x96

    .line 1669
    .local v11, "outputSize":I
    const/4 v9, 0x5

    .line 1671
    .local v9, "outFileResolution":I
    new-instance v7, Lcom/samsung/app/share/via/external/NativeAccess;

    invoke-direct {v7}, Lcom/samsung/app/share/via/external/NativeAccess;-><init>()V

    .line 1672
    .local v7, "nativeAccess":Lcom/samsung/app/share/via/external/NativeAccess;
    new-instance v8, Lcom/samsung/app/share/via/external/ShareviaObj;

    invoke-direct {v8}, Lcom/samsung/app/share/via/external/ShareviaObj;-><init>()V

    .line 1674
    .local v8, "obj":Lcom/samsung/app/share/via/external/ShareviaObj;
    move v5, v11

    .line 1675
    .local v5, "maxOutFileSize":I
    move v2, v9

    .line 1676
    .local v2, "expectedResolution":I
    const/4 v10, 0x0

    .line 1677
    .local v10, "outputFileTimePossible":I
    const/4 v13, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v7, v0, v5, v2, v13}, Lcom/samsung/app/share/via/external/NativeAccess;->getInputParamerterAnalysis(Ljava/lang/String;III)I

    move-result v10

    .line 1679
    new-instance v12, Ljava/io/File;

    sget-object v13, Lcom/sec/android/app/myfiles/utils/Constant;->GOLF_TEMP_VIDEO_PATH:Ljava/lang/String;

    invoke-direct {v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1681
    .local v12, "tmpVideoDir":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v13

    if-nez v13, :cond_2

    .line 1683
    invoke-virtual {v12}, Ljava/io/File;->mkdirs()Z

    .line 1686
    :cond_2
    sget-object v13, Lcom/sec/android/app/myfiles/utils/Constant;->GOLF_TEMP_VIDEO_PATH:Ljava/lang/String;

    const/4 v14, 0x1

    move-object/from16 v0, p1

    invoke-static {v13, v0, v14}, Lcom/sec/android/app/myfiles/utils/FileUtils;->changeToImageFilePath(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    .line 1689
    sget-object v13, Lcom/sec/android/app/myfiles/utils/Constant;->GOLF_TEMP_VIDEO_PATH:Ljava/lang/String;

    invoke-virtual {v8, v13}, Lcom/samsung/app/share/via/external/ShareviaObj;->setShareViaInputFilename(Ljava/lang/String;)V

    .line 1692
    invoke-virtual {v8, v1}, Lcom/samsung/app/share/via/external/ShareviaObj;->setShareViaOutputFilename(Ljava/lang/String;)V

    .line 1694
    const/4 v13, 0x0

    invoke-virtual {v8, v13}, Lcom/samsung/app/share/via/external/ShareviaObj;->setShareViaStartTime(I)V

    .line 1695
    add-int/lit16 v13, v10, 0xfa0

    invoke-virtual {v8, v13}, Lcom/samsung/app/share/via/external/ShareviaObj;->setShareViaEndTime(I)V

    .line 1698
    const v13, 0xf4240

    invoke-virtual {v8, v13}, Lcom/samsung/app/share/via/external/ShareviaObj;->setShareViamaxDuration(I)V

    .line 1699
    invoke-virtual {v8, v5}, Lcom/samsung/app/share/via/external/ShareviaObj;->setShareViamaxSize(I)V

    .line 1701
    const/4 v13, 0x2

    invoke-virtual {v8, v13}, Lcom/samsung/app/share/via/external/ShareviaObj;->setShareViaVideoCodec(I)V

    .line 1702
    const/4 v13, 0x5

    invoke-virtual {v8, v13}, Lcom/samsung/app/share/via/external/ShareviaObj;->setShareViaAudioCodec(I)V

    .line 1704
    invoke-virtual {v8, v9}, Lcom/samsung/app/share/via/external/ShareviaObj;->setShareViaOutputFileResolution(I)V

    .line 1705
    const-string v13, "gallery_detail_ic_golf.png"

    invoke-virtual {v8, v13}, Lcom/samsung/app/share/via/external/ShareviaObj;->setShareViaIconFileName(Ljava/lang/String;)V

    .line 1707
    const/4 v13, 0x2

    invoke-virtual {v8, v13}, Lcom/samsung/app/share/via/external/ShareviaObj;->setShareViaTranscodeMode(I)V

    .line 1709
    new-instance v13, Lcom/samsung/app/share/via/external/ConvertThread;

    move-object/from16 v0, p2

    invoke-direct {v13, v7, v8, v0}, Lcom/samsung/app/share/via/external/ConvertThread;-><init>(Lcom/samsung/app/share/via/external/NativeAccess;Lcom/samsung/app/share/via/external/ShareviaObj;Landroid/os/Handler;)V

    sput-object v13, Lcom/sec/android/app/myfiles/utils/FileUtils;->mConvertThread:Ljava/lang/Thread;

    .line 1711
    sget-object v13, Lcom/sec/android/app/myfiles/utils/FileUtils;->mConvertThread:Ljava/lang/Thread;

    invoke-virtual {v13}, Ljava/lang/Thread;->start()V

    .line 1713
    const/4 v13, 0x1

    goto :goto_0
.end method

.method public static deleteFolderRowFromMediaDB(ILandroid/net/Uri;Landroid/app/Activity;)V
    .locals 6
    .param p0, "mediaType"    # I
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "context"    # Landroid/app/Activity;

    .prologue
    const/4 v4, 0x1

    .line 1134
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->isImageFileType(I)Z

    move-result v2

    .line 1136
    .local v2, "isImage":Z
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->isVideoFileType(I)Z

    move-result v3

    .line 1138
    .local v3, "isVideo":Z
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->isAudioFileType(I)Z

    move-result v1

    .line 1140
    .local v1, "isAudio":Z
    if-eq v2, v4, :cond_0

    if-eq v3, v4, :cond_0

    if-ne v1, v4, :cond_1

    .line 1142
    :cond_0
    invoke-virtual {p2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1144
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz v0, :cond_1

    .line 1145
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_data like \""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v0, p1, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1147
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :cond_1
    return-void
.end method

.method public static deleteRowFromMediaDB(ILandroid/net/Uri;Landroid/app/Activity;)V
    .locals 6
    .param p0, "mediaType"    # I
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "context"    # Landroid/app/Activity;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1114
    if-nez p2, :cond_1

    .line 1129
    :cond_0
    :goto_0
    return-void

    .line 1117
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->isImageFileType(I)Z

    move-result v2

    .line 1119
    .local v2, "isImage":Z
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->isVideoFileType(I)Z

    move-result v3

    .line 1121
    .local v3, "isVideo":Z
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->isAudioFileType(I)Z

    move-result v1

    .line 1123
    .local v1, "isAudio":Z
    if-eq v2, v4, :cond_2

    if-eq v3, v4, :cond_2

    if-ne v1, v4, :cond_0

    .line 1125
    :cond_2
    invoke-virtual {p2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1127
    .local v0, "cr":Landroid/content/ContentResolver;
    invoke-virtual {v0, p1, v5, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static deleteRowFromMediaDB(Landroid/net/Uri;Landroid/app/Activity;)V
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "context"    # Landroid/app/Activity;

    .prologue
    const/4 v1, 0x0

    .line 1151
    if-nez p1, :cond_1

    .line 1161
    :cond_0
    :goto_0
    return-void

    .line 1154
    :cond_1
    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1156
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz p0, :cond_0

    .line 1158
    if-eqz v0, :cond_0

    .line 1159
    invoke-virtual {v0, p0, v1, v1}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static generateGolfFile(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 29
    .param p0, "tempFilePath"    # Ljava/lang/String;
    .param p1, "FilePath"    # Ljava/lang/String;
    .param p2, "allFrame"    # Z

    .prologue
    .line 1781
    const/4 v15, 0x0

    .line 1783
    .local v15, "inStream":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v9, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1784
    .local v9, "f":Ljava/io/File;
    new-instance v16, Ljava/io/FileInputStream;

    move-object/from16 v0, v16

    invoke-direct {v0, v9}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1785
    .end local v15    # "inStream":Ljava/io/FileInputStream;
    .local v16, "inStream":Ljava/io/FileInputStream;
    if-eqz v16, :cond_8

    .line 1786
    :try_start_1
    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v26

    move-wide/from16 v0, v26

    long-to-int v0, v0

    move/from16 v17, v0

    .line 1787
    .local v17, "len":I
    move/from16 v0, v17

    new-array v4, v0, [B

    .line 1788
    .local v4, "byteArray":[B
    const/16 v26, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v26

    move/from16 v2, v17

    invoke-virtual {v0, v4, v1, v2}, Ljava/io/FileInputStream;->read([BII)I

    .line 1789
    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1790
    .local v5, "byteBuf":Ljava/nio/ByteBuffer;
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 1791
    const/16 v26, 0x0

    move/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1792
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v13

    .line 1793
    .local v13, "ib":Ljava/nio/IntBuffer;
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1795
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    move-result v24

    .line 1796
    .local v24, "ver_major":I
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v25

    .line 1797
    .local v25, "ver_minor":I
    const/16 v26, 0x1

    move/from16 v0, v24

    move/from16 v1, v26

    if-lt v0, v1, :cond_0

    const/16 v26, 0x4

    move/from16 v0, v25

    move/from16 v1, v26

    if-ge v0, v1, :cond_3

    .line 1862
    :cond_0
    if-eqz v16, :cond_1

    .line 1864
    :try_start_2
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    :cond_1
    :goto_0
    move-object/from16 v15, v16

    .line 1871
    .end local v4    # "byteArray":[B
    .end local v5    # "byteBuf":Ljava/nio/ByteBuffer;
    .end local v9    # "f":Ljava/io/File;
    .end local v13    # "ib":Ljava/nio/IntBuffer;
    .end local v16    # "inStream":Ljava/io/FileInputStream;
    .end local v17    # "len":I
    .end local v24    # "ver_major":I
    .end local v25    # "ver_minor":I
    .restart local v15    # "inStream":Ljava/io/FileInputStream;
    :cond_2
    :goto_1
    return-void

    .line 1865
    .end local v15    # "inStream":Ljava/io/FileInputStream;
    .restart local v4    # "byteArray":[B
    .restart local v5    # "byteBuf":Ljava/nio/ByteBuffer;
    .restart local v9    # "f":Ljava/io/File;
    .restart local v13    # "ib":Ljava/nio/IntBuffer;
    .restart local v16    # "inStream":Ljava/io/FileInputStream;
    .restart local v17    # "len":I
    .restart local v24    # "ver_major":I
    .restart local v25    # "ver_minor":I
    :catch_0
    move-exception v23

    .line 1866
    .local v23, "t":Ljava/lang/Throwable;
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    .line 1800
    .end local v23    # "t":Ljava/lang/Throwable;
    :cond_3
    :try_start_3
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    move-result v11

    .line 1801
    .local v11, "header_length":I
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    move-result v10

    .line 1802
    .local v10, "format":I
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    move-result v18

    .line 1805
    .local v18, "nFrames":I
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1806
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1807
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1808
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1810
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1811
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1812
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1813
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1814
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1815
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1816
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1817
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1818
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1819
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1820
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1821
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1822
    const/16 v19, 0x0

    .line 1824
    .local v19, "num_video_seq":I
    if-eqz p2, :cond_5

    .line 1825
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v19

    .line 1826
    if-gtz v19, :cond_5

    .line 1862
    if-eqz v16, :cond_4

    .line 1864
    :try_start_4
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    :cond_4
    :goto_2
    move-object/from16 v15, v16

    .line 1867
    .end local v16    # "inStream":Ljava/io/FileInputStream;
    .restart local v15    # "inStream":Ljava/io/FileInputStream;
    goto :goto_1

    .line 1865
    .end local v15    # "inStream":Ljava/io/FileInputStream;
    .restart local v16    # "inStream":Ljava/io/FileInputStream;
    :catch_1
    move-exception v23

    .line 1866
    .restart local v23    # "t":Ljava/lang/Throwable;
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_2

    .line 1829
    .end local v23    # "t":Ljava/lang/Throwable;
    :cond_5
    :try_start_5
    invoke-virtual {v5, v11}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1831
    const/16 v26, 0x1

    move/from16 v0, v24

    move/from16 v1, v26

    if-lt v0, v1, :cond_8

    const/16 v26, 0x2

    move/from16 v0, v25

    move/from16 v1, v26

    if-lt v0, v1, :cond_8

    .line 1833
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v14

    .line 1837
    .local v14, "ib1":Ljava/nio/IntBuffer;
    invoke-virtual {v14}, Ljava/nio/IntBuffer;->remaining()I

    move-result v26

    sub-int v26, v26, v18

    sub-int v26, v26, v19

    add-int/lit8 v21, v26, -0x1

    .line 1838
    .local v21, "offset_start_in_ib":I
    add-int/lit8 v26, v19, 0x1

    add-int v26, v26, v18

    move/from16 v0, v26

    new-array v0, v0, [I

    move-object/from16 v20, v0

    .line 1839
    .local v20, "offsetArray":[I
    move/from16 v0, v21

    invoke-virtual {v14, v0}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;

    .line 1841
    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Ljava/nio/IntBuffer;->get([I)Ljava/nio/IntBuffer;

    .line 1842
    if-eqz p2, :cond_6

    add-int/lit8 v26, v19, 0x1

    add-int v6, v26, v18

    .line 1843
    .local v6, "count":I
    :goto_3
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_4
    if-ge v12, v6, :cond_8

    .line 1844
    const/16 v22, 0x0

    .line 1845
    .local v22, "size":I
    add-int v26, v19, v18

    move/from16 v0, v26

    if-ne v6, v0, :cond_7

    .line 1846
    aget v26, v20, v12
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    sub-int v22, v21, v26

    .line 1850
    :goto_5
    :try_start_6
    move/from16 v0, v22

    new-array v7, v0, [B

    .line 1851
    .local v7, "data":[B
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v26

    aget v27, v20, v12

    const/16 v28, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v22

    invoke-static {v0, v1, v7, v2, v3}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 1852
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v22

    invoke-static {v0, v10, v1, v7}, Lcom/sec/android/app/myfiles/utils/FileUtils;->saveBufferToFile(Ljava/lang/String;II[B)V
    :try_end_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1843
    .end local v7    # "data":[B
    :goto_6
    add-int/lit8 v12, v12, 0x1

    goto :goto_4

    .line 1842
    .end local v6    # "count":I
    .end local v12    # "i":I
    .end local v22    # "size":I
    :cond_6
    const/4 v6, 0x1

    goto :goto_3

    .line 1848
    .restart local v6    # "count":I
    .restart local v12    # "i":I
    .restart local v22    # "size":I
    :cond_7
    add-int/lit8 v26, v12, 0x1

    :try_start_7
    aget v26, v20, v26

    aget v27, v20, v12

    sub-int v22, v26, v27

    goto :goto_5

    .line 1853
    :catch_2
    move-exception v8

    .line 1854
    .local v8, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v8}, Ljava/lang/OutOfMemoryError;->printStackTrace()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_6

    .line 1859
    .end local v4    # "byteArray":[B
    .end local v5    # "byteBuf":Ljava/nio/ByteBuffer;
    .end local v6    # "count":I
    .end local v8    # "e":Ljava/lang/OutOfMemoryError;
    .end local v10    # "format":I
    .end local v11    # "header_length":I
    .end local v12    # "i":I
    .end local v13    # "ib":Ljava/nio/IntBuffer;
    .end local v14    # "ib1":Ljava/nio/IntBuffer;
    .end local v17    # "len":I
    .end local v18    # "nFrames":I
    .end local v19    # "num_video_seq":I
    .end local v20    # "offsetArray":[I
    .end local v21    # "offset_start_in_ib":I
    .end local v22    # "size":I
    .end local v24    # "ver_major":I
    .end local v25    # "ver_minor":I
    :catch_3
    move-exception v8

    move-object/from16 v15, v16

    .line 1860
    .end local v9    # "f":Ljava/io/File;
    .end local v16    # "inStream":Ljava/io/FileInputStream;
    .local v8, "e":Ljava/lang/Exception;
    .restart local v15    # "inStream":Ljava/io/FileInputStream;
    :goto_7
    :try_start_8
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1862
    if-eqz v15, :cond_2

    .line 1864
    :try_start_9
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_4

    goto/16 :goto_1

    .line 1865
    :catch_4
    move-exception v23

    .line 1866
    .restart local v23    # "t":Ljava/lang/Throwable;
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_1

    .line 1862
    .end local v8    # "e":Ljava/lang/Exception;
    .end local v15    # "inStream":Ljava/io/FileInputStream;
    .end local v23    # "t":Ljava/lang/Throwable;
    .restart local v9    # "f":Ljava/io/File;
    .restart local v16    # "inStream":Ljava/io/FileInputStream;
    :cond_8
    if-eqz v16, :cond_a

    .line 1864
    :try_start_a
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_5

    move-object/from16 v15, v16

    .line 1867
    .end local v16    # "inStream":Ljava/io/FileInputStream;
    .restart local v15    # "inStream":Ljava/io/FileInputStream;
    goto/16 :goto_1

    .line 1865
    .end local v15    # "inStream":Ljava/io/FileInputStream;
    .restart local v16    # "inStream":Ljava/io/FileInputStream;
    :catch_5
    move-exception v23

    .line 1866
    .restart local v23    # "t":Ljava/lang/Throwable;
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Throwable;->printStackTrace()V

    move-object/from16 v15, v16

    .line 1867
    .end local v16    # "inStream":Ljava/io/FileInputStream;
    .restart local v15    # "inStream":Ljava/io/FileInputStream;
    goto/16 :goto_1

    .line 1862
    .end local v9    # "f":Ljava/io/File;
    .end local v23    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v26

    :goto_8
    if-eqz v15, :cond_9

    .line 1864
    :try_start_b
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_6

    .line 1867
    :cond_9
    :goto_9
    throw v26

    .line 1865
    :catch_6
    move-exception v23

    .line 1866
    .restart local v23    # "t":Ljava/lang/Throwable;
    invoke-virtual/range {v23 .. v23}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_9

    .line 1862
    .end local v15    # "inStream":Ljava/io/FileInputStream;
    .end local v23    # "t":Ljava/lang/Throwable;
    .restart local v9    # "f":Ljava/io/File;
    .restart local v16    # "inStream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v26

    move-object/from16 v15, v16

    .end local v16    # "inStream":Ljava/io/FileInputStream;
    .restart local v15    # "inStream":Ljava/io/FileInputStream;
    goto :goto_8

    .line 1859
    .end local v9    # "f":Ljava/io/File;
    :catch_7
    move-exception v8

    goto :goto_7

    .end local v15    # "inStream":Ljava/io/FileInputStream;
    .restart local v9    # "f":Ljava/io/File;
    .restart local v16    # "inStream":Ljava/io/FileInputStream;
    :cond_a
    move-object/from16 v15, v16

    .end local v16    # "inStream":Ljava/io/FileInputStream;
    .restart local v15    # "inStream":Ljava/io/FileInputStream;
    goto/16 :goto_1
.end method

.method public static getExtractSuccess()Z
    .locals 1

    .prologue
    .line 99
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/FileUtils;->mIsExtractSuccessRefresh:Z

    return v0
.end method

.method public static getFileNameInPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 1526
    const/4 v0, 0x0

    .line 1528
    .local v0, "fileName":Ljava/lang/String;
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1530
    sget-char v1, Ljava/io/File;->separatorChar:C

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 1533
    :cond_0
    return-object v0
.end method

.method public static getFilePathFromUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 24
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 964
    const/16 v17, 0x0

    .line 966
    .local v17, "filePath":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v23

    .line 968
    .local v23, "scheme":Ljava/lang/String;
    const-string v2, "content"

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 970
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 972
    .local v9, "cursor":Landroid/database/Cursor;
    if-eqz v9, :cond_1

    .line 974
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 976
    const-string v2, "_data"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 978
    .local v10, "dataColIndex":I
    const/4 v2, -0x1

    if-eq v10, v2, :cond_2

    .line 980
    invoke-interface {v9, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 1046
    :cond_0
    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .end local v9    # "cursor":Landroid/database/Cursor;
    .end local v10    # "dataColIndex":I
    :cond_1
    :goto_1
    move-object/from16 v2, v17

    .line 1054
    :goto_2
    return-object v2

    .line 986
    .restart local v9    # "cursor":Landroid/database/Cursor;
    .restart local v10    # "dataColIndex":I
    :cond_2
    :try_start_0
    const-string v2, "_display_name"

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 988
    .local v11, "dispNameColIndex":I
    const/4 v2, -0x1

    if-eq v11, v2, :cond_0

    .line 990
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v19

    .line 992
    .local v19, "input":Ljava/io/InputStream;
    if-nez v19, :cond_3

    .line 994
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 996
    const/4 v2, 0x0

    goto :goto_2

    .line 999
    :cond_3
    invoke-interface {v9, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 1001
    .local v16, "fileName":Ljava/lang/String;
    const-string v2, "."

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v12

    .line 1003
    .local v12, "dotIndex":I
    const-string v15, ""

    .line 1005
    .local v15, "fileExt":Ljava/lang/String;
    const/4 v2, -0x1

    if-eq v12, v2, :cond_4

    .line 1007
    add-int/lit8 v2, v12, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v15

    .line 1009
    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v16

    .line 1012
    :cond_4
    const/4 v14, 0x0

    .line 1013
    .local v14, "file":Ljava/io/File;
    const/16 v18, -0x1

    .line 1015
    .local v18, "i":I
    :cond_5
    if-ltz v18, :cond_6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    .line 1016
    .local v20, "newFileName":Ljava/lang/String;
    :goto_3
    new-instance v14, Ljava/io/File;

    .end local v14    # "file":Ljava/io/File;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/android/app/myfiles/utils/Constant;->DEFAULT_DOWNLOAD_DIR:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v14, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1017
    .restart local v14    # "file":Ljava/io/File;
    add-int/lit8 v18, v18, 0x1

    .line 1018
    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_5

    .line 1020
    new-instance v21, Ljava/io/FileOutputStream;

    move-object/from16 v0, v21

    invoke-direct {v0, v14}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1023
    .local v21, "output":Ljava/io/OutputStream;
    const/16 v2, 0x400

    :try_start_1
    new-array v8, v2, [B

    .line 1025
    .local v8, "buffer":[B
    :goto_4
    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/io/InputStream;->read([B)I

    move-result v22

    .local v22, "read":I
    const/4 v2, -0x1

    move/from16 v0, v22

    if-eq v0, v2, :cond_7

    .line 1026
    const/4 v2, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v0, v8, v2, v1}, Ljava/io/OutputStream;->write([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    .line 1033
    .end local v8    # "buffer":[B
    .end local v22    # "read":I
    :catchall_0
    move-exception v2

    :try_start_2
    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStream;->close()V

    .line 1034
    invoke-virtual/range {v21 .. v21}, Ljava/io/OutputStream;->close()V

    throw v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1036
    :catch_0
    move-exception v13

    .line 1037
    .local v13, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 1041
    .end local v11    # "dispNameColIndex":I
    .end local v12    # "dotIndex":I
    .end local v13    # "e":Ljava/lang/Exception;
    .end local v14    # "file":Ljava/io/File;
    .end local v15    # "fileExt":Ljava/lang/String;
    .end local v16    # "fileName":Ljava/lang/String;
    .end local v18    # "i":I
    .end local v19    # "input":Ljava/io/InputStream;
    .end local v20    # "newFileName":Ljava/lang/String;
    .end local v21    # "output":Ljava/io/OutputStream;
    :catch_1
    move-exception v13

    .line 1042
    .local v13, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v13}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto/16 :goto_0

    .line 1015
    .end local v13    # "e":Ljava/io/FileNotFoundException;
    .restart local v11    # "dispNameColIndex":I
    .restart local v12    # "dotIndex":I
    .restart local v14    # "file":Ljava/io/File;
    .restart local v15    # "fileExt":Ljava/lang/String;
    .restart local v16    # "fileName":Ljava/lang/String;
    .restart local v18    # "i":I
    .restart local v19    # "input":Ljava/io/InputStream;
    :cond_6
    :try_start_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1

    move-result-object v20

    goto :goto_3

    .line 1028
    .restart local v8    # "buffer":[B
    .restart local v20    # "newFileName":Ljava/lang/String;
    .restart local v21    # "output":Ljava/io/OutputStream;
    .restart local v22    # "read":I
    :cond_7
    :try_start_5
    invoke-virtual/range {v21 .. v21}, Ljava/io/OutputStream;->flush()V

    .line 1029
    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v17

    .line 1033
    :try_start_6
    invoke-virtual/range {v19 .. v19}, Ljava/io/InputStream;->close()V

    .line 1034
    invoke-virtual/range {v21 .. v21}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_0

    .line 1049
    .end local v8    # "buffer":[B
    .end local v9    # "cursor":Landroid/database/Cursor;
    .end local v10    # "dataColIndex":I
    .end local v11    # "dispNameColIndex":I
    .end local v12    # "dotIndex":I
    .end local v14    # "file":Ljava/io/File;
    .end local v15    # "fileExt":Ljava/lang/String;
    .end local v16    # "fileName":Ljava/lang/String;
    .end local v18    # "i":I
    .end local v19    # "input":Ljava/io/InputStream;
    .end local v20    # "newFileName":Ljava/lang/String;
    .end local v21    # "output":Ljava/io/OutputStream;
    .end local v22    # "read":I
    :cond_8
    const-string v2, "file"

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1051
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_1
.end method

.method public static getIDfromUri(Ljava/lang/String;Ljava/lang/String;Landroid/app/Activity;)I
    .locals 13
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "mimetype"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/app/Activity;

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v12, 0x0

    .line 569
    invoke-virtual {p2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 571
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v11, 0x0

    .line 585
    .local v11, "rowId":I
    invoke-static {p1}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeForMimeType(Ljava/lang/String;)I

    move-result v10

    .line 589
    .local v10, "mediaType":I
    invoke-static {v10}, Lcom/sec/android/app/myfiles/MediaFile;->isImageFileType(I)Z

    move-result v8

    .line 591
    .local v8, "isImage":Z
    invoke-static {v10}, Lcom/sec/android/app/myfiles/MediaFile;->isVideoFileType(I)Z

    move-result v9

    .line 593
    .local v9, "isVideo":Z
    invoke-static {v10}, Lcom/sec/android/app/myfiles/MediaFile;->isAudioFileType(I)Z

    move-result v7

    .line 595
    .local v7, "isAudio":Z
    if-eqz v8, :cond_3

    .line 597
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const-string v3, "_data= ? COLLATE LOCALIZED"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p0, v4, v12

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 603
    .local v6, "c":Landroid/database/Cursor;
    if-nez v6, :cond_1

    .line 605
    const-string v1, "FileUtils"

    const-string v2, "getIDfromUri : c is null"

    invoke-static {v12, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 668
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_0
    :goto_0
    return v11

    .line 609
    .restart local v6    # "c":Landroid/database/Cursor;
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 611
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 613
    const-string v1, "_id"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 616
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 619
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_3
    if-eqz v9, :cond_6

    .line 621
    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const-string v3, "_data= ? COLLATE LOCALIZED"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p0, v4, v12

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 627
    .restart local v6    # "c":Landroid/database/Cursor;
    if-nez v6, :cond_4

    .line 629
    const-string v1, "FileUtils"

    const-string v2, "getIDfromUri : c is null"

    invoke-static {v12, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 633
    :cond_4
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_5

    .line 635
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 637
    const-string v1, "_id"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 640
    :cond_5
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 643
    .end local v6    # "c":Landroid/database/Cursor;
    :cond_6
    if-eqz v7, :cond_0

    .line 645
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const-string v3, "_data= ? COLLATE LOCALIZED"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p0, v4, v12

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 651
    .restart local v6    # "c":Landroid/database/Cursor;
    if-nez v6, :cond_7

    .line 653
    const-string v1, "FileUtils"

    const-string v2, "getIDfromUri : c is null"

    invoke-static {v12, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 657
    :cond_7
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_8

    .line 659
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 661
    const-string v1, "_id"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 664
    :cond_8
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public static getItemToMediaList(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "realPath"    # Ljava/lang/String;

    .prologue
    .line 1342
    sget-object v0, Lcom/sec/android/app/myfiles/utils/FileUtils;->mediaList:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 1344
    sget-object v0, Lcom/sec/android/app/myfiles/utils/FileUtils;->mediaList:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1347
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getParentFolderPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 1508
    const/4 v0, 0x0

    .line 1510
    .local v0, "parent":Ljava/lang/String;
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_1

    .line 1512
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    sget-char v2, Ljava/io/File;->separatorChar:C

    if-ne v1, v2, :cond_0

    .line 1514
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {p0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 1517
    :cond_0
    sget-char v1, Ljava/io/File;->separatorChar:C

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    invoke-virtual {p0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1520
    :cond_1
    return-object v0
.end method

.method public static getThumbUri(Ljava/lang/String;Landroid/app/Activity;)Ljava/lang/String;
    .locals 3
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/app/Activity;

    .prologue
    const/4 v1, 0x0

    .line 674
    sget-object v0, Lcom/sec/android/app/myfiles/utils/FileUtils;->mediaList:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 676
    sget-object v0, Lcom/sec/android/app/myfiles/utils/FileUtils;->mediaList:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 678
    new-instance v2, Ljava/io/File;

    sget-object v0, Lcom/sec/android/app/myfiles/utils/FileUtils;->mediaList:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 686
    :goto_0
    return-object v0

    .line 683
    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/FileUtils;->mediaList:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 686
    goto :goto_0
.end method

.method private static hideCurrentIm(Landroid/app/Activity;)V
    .locals 2
    .param p0, "context"    # Landroid/app/Activity;

    .prologue
    .line 558
    if-eqz p0, :cond_0

    .line 559
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 561
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 562
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    .line 565
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    return-void
.end method

.method public static insertItemToMediaList(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "realPath"    # Ljava/lang/String;
    .param p1, "thumbPath"    # Ljava/lang/String;

    .prologue
    .line 1353
    sget-object v0, Lcom/sec/android/app/myfiles/utils/FileUtils;->mediaList:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 1355
    sget-object v0, Lcom/sec/android/app/myfiles/utils/FileUtils;->mediaList:Ljava/util/HashMap;

    invoke-virtual {v0, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1357
    :cond_0
    return-void
.end method

.method public static isRunningConvertThread()Z
    .locals 1

    .prologue
    .line 1719
    sget-object v0, Lcom/sec/android/app/myfiles/utils/FileUtils;->mConvertThread:Ljava/lang/Thread;

    if-nez v0, :cond_0

    .line 1721
    const/4 v0, 0x0

    .line 1724
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/FileUtils;->mConvertThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    goto :goto_0
.end method

.method public static makeListFromMediaDB(Landroid/app/Activity;)V
    .locals 9
    .param p0, "context"    # Landroid/app/Activity;

    .prologue
    const/4 v2, 0x0

    .line 1275
    sget-object v1, Lcom/sec/android/app/myfiles/utils/FileUtils;->mediaList:Ljava/util/HashMap;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/android/app/myfiles/utils/FileUtils;->mediaList:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    if-gtz v1, :cond_1

    .line 1277
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 1279
    .local v8, "realPath":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1281
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1287
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1289
    :cond_0
    if-eqz v6, :cond_1

    .line 1291
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1337
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v6    # "c":Landroid/database/Cursor;
    .end local v8    # "realPath":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_1
    :goto_0
    return-void

    .line 1297
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    .restart local v6    # "c":Landroid/database/Cursor;
    .restart local v8    # "realPath":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_2
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-ge v7, v1, :cond_3

    .line 1299
    const-string v1, "_id"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "_data"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1302
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    .line 1297
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 1305
    :cond_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1307
    const-string v1, "external"

    invoke-static {v1}, Landroid/provider/MediaStore$Images$Thumbnails;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1313
    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_5

    .line 1315
    :cond_4
    if-eqz v6, :cond_1

    .line 1317
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1323
    :cond_5
    const/4 v7, 0x0

    :goto_2
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-ge v7, v1, :cond_6

    .line 1325
    sget-object v1, Lcom/sec/android/app/myfiles/utils/FileUtils;->mediaList:Ljava/util/HashMap;

    const-string v2, "image_id"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    const-string v3, "_data"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1328
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    .line 1323
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 1331
    :cond_6
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1333
    invoke-virtual {v8}, Ljava/util/HashMap;->clear()V

    goto/16 :goto_0
.end method

.method public static openFile(ILjava/io/File;Landroid/app/Activity;ZI)V
    .locals 11
    .param p0, "categoryType"    # I
    .param p1, "file"    # Ljava/io/File;
    .param p2, "context"    # Landroid/app/Activity;
    .param p3, "isInSelectList"    # Z
    .param p4, "sortOrder"    # I

    .prologue
    .line 113
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 340
    :cond_0
    :goto_0
    return-void

    .line 117
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v3

    .line 121
    .local v3, "mediaType":I
    const/4 v0, 0x0

    .line 124
    .local v0, "activityNotFound":Z
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {p2, v7}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isOMADrmFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 126
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {p2, v7}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->getRealMimeTypeOfDRM(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 128
    .local v4, "mimetype":Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeForMimeType(Ljava/lang/String;)I

    move-result v3

    .line 140
    :goto_1
    invoke-static {v3}, Lcom/sec/android/app/myfiles/MediaFile;->isInstallFileType(I)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 142
    new-instance v2, Landroid/content/Intent;

    const-string v7, "android.intent.action.VIEW"

    invoke-direct {v2, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 146
    .local v2, "intent":Landroid/content/Intent;
    const/16 v7, 0x64

    if-ne v7, v3, :cond_5

    .line 148
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v7

    const-string v8, "application/vnd.android.package-archive"

    invoke-virtual {v2, v7, v8}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 155
    :cond_2
    :goto_2
    const-string v7, "from-myfiles"

    const/4 v8, 0x1

    invoke-virtual {v2, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 157
    invoke-static {p2}, Lcom/sec/android/app/myfiles/utils/FileUtils;->hideCurrentIm(Landroid/app/Activity;)V

    .line 161
    :try_start_0
    invoke-virtual {p2, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 175
    if-nez v0, :cond_0

    .line 177
    invoke-static {p2, p1}, Lcom/sec/android/app/myfiles/utils/FileUtils;->updateOpendFileInfo(Landroid/app/Activity;Ljava/io/File;)V

    goto :goto_0

    .line 130
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v4    # "mimetype":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/myfiles/MediaFile;->needToCheckMimeType(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 132
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, p2}, Lcom/sec/android/app/myfiles/MediaFile;->getMimeTypeFromMediaStore(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "mimetype":Ljava/lang/String;
    goto :goto_1

    .line 136
    .end local v4    # "mimetype":Ljava/lang/String;
    :cond_4
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/myfiles/MediaFile;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "mimetype":Ljava/lang/String;
    goto :goto_1

    .line 150
    .restart local v2    # "intent":Landroid/content/Intent;
    :cond_5
    const/16 v7, 0x65

    if-ne v7, v3, :cond_2

    .line 152
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v7

    const-string v8, "application/vnd.samsung.widget"

    invoke-virtual {v2, v7, v8}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2

    .line 163
    :catch_0
    move-exception v1

    .line 165
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    const/4 v0, 0x1

    .line 167
    :try_start_1
    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 169
    invoke-virtual {p2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0064

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {p2, v7}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 175
    if-nez v0, :cond_0

    .line 177
    invoke-static {p2, p1}, Lcom/sec/android/app/myfiles/utils/FileUtils;->updateOpendFileInfo(Landroid/app/Activity;Ljava/io/File;)V

    goto/16 :goto_0

    .line 175
    .end local v1    # "e":Landroid/content/ActivityNotFoundException;
    :catchall_0
    move-exception v7

    if-nez v0, :cond_6

    .line 177
    invoke-static {p2, p1}, Lcom/sec/android/app/myfiles/utils/FileUtils;->updateOpendFileInfo(Landroid/app/Activity;Ljava/io/File;)V

    :cond_6
    throw v7

    .line 182
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_7
    invoke-static {v3}, Lcom/sec/android/app/myfiles/MediaFile;->isImageFileType(I)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 184
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->removeThumbnailCache(Ljava/lang/String;)V

    .line 186
    const/4 v2, 0x0

    .line 188
    .restart local v2    # "intent":Landroid/content/Intent;
    const/4 v7, 0x2

    if-ne p0, v7, :cond_9

    .line 190
    const/4 v7, 0x2

    invoke-static {p2, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesDB;->getIntentForQueryInformation(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v2

    .line 197
    :goto_3
    const-string v7, "android.intent.action.VIEW"

    invoke-virtual {v2, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 199
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {p2, v7}, Lcom/sec/android/app/myfiles/utils/FileUtils;->pathToUri(Landroid/app/Activity;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 201
    .local v5, "myUri":Landroid/net/Uri;
    if-nez v5, :cond_8

    .line 203
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    .line 206
    :cond_8
    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 208
    const-string v7, "android.intent.extra.STREAM"

    invoke-virtual {v2, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 210
    const-string v7, "from-myfiles"

    const/4 v8, 0x1

    invoke-virtual {v2, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 212
    const/4 v7, 0x0

    const-string v8, "FileUtils"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Clicked image file. MIME type = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 213
    const/4 v7, 0x2

    const-string v8, "FileUtils"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "                     Uri = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 214
    invoke-static {p2}, Lcom/sec/android/app/myfiles/utils/FileUtils;->hideCurrentIm(Landroid/app/Activity;)V

    .line 218
    const/high16 v7, 0x10000000

    :try_start_2
    invoke-virtual {v2, v7}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 219
    invoke-virtual {p2, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 233
    if-nez v0, :cond_0

    .line 235
    invoke-static {p2, p1}, Lcom/sec/android/app/myfiles/utils/FileUtils;->updateOpendFileInfo(Landroid/app/Activity;Ljava/io/File;)V

    goto/16 :goto_0

    .line 194
    .end local v5    # "myUri":Landroid/net/Uri;
    :cond_9
    new-instance v2, Landroid/content/Intent;

    .end local v2    # "intent":Landroid/content/Intent;
    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .restart local v2    # "intent":Landroid/content/Intent;
    goto :goto_3

    .line 221
    .restart local v5    # "myUri":Landroid/net/Uri;
    :catch_1
    move-exception v1

    .line 223
    .restart local v1    # "e":Landroid/content/ActivityNotFoundException;
    const/4 v0, 0x1

    .line 225
    :try_start_3
    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 227
    invoke-static {p2}, Lcom/sec/android/app/myfiles/utils/FileUtils;->OpenFileDialog(Landroid/app/Activity;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 233
    if-nez v0, :cond_0

    .line 235
    invoke-static {p2, p1}, Lcom/sec/android/app/myfiles/utils/FileUtils;->updateOpendFileInfo(Landroid/app/Activity;Ljava/io/File;)V

    goto/16 :goto_0

    .line 233
    .end local v1    # "e":Landroid/content/ActivityNotFoundException;
    :catchall_1
    move-exception v7

    if-nez v0, :cond_a

    .line 235
    invoke-static {p2, p1}, Lcom/sec/android/app/myfiles/utils/FileUtils;->updateOpendFileInfo(Landroid/app/Activity;Ljava/io/File;)V

    :cond_a
    throw v7

    .line 243
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v5    # "myUri":Landroid/net/Uri;
    :cond_b
    const/4 v7, 0x3

    if-eq p0, v7, :cond_c

    const/4 v7, 0x4

    if-eq p0, v7, :cond_c

    const/4 v7, 0x5

    if-ne p0, v7, :cond_d

    .line 246
    :cond_c
    invoke-static {p2, p0}, Lcom/sec/android/app/myfiles/utils/MyFilesDB;->getIntentForQueryInformation(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v2

    .line 253
    .restart local v2    # "intent":Landroid/content/Intent;
    :goto_4
    const-string v7, "key_filename"

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 255
    if-nez p3, :cond_e

    .line 257
    const-string v7, "preview"

    const/4 v8, 0x0

    invoke-virtual {v2, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 259
    const-string v7, "sort_order"

    invoke-virtual {v2, v7, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 275
    :goto_5
    if-eqz v4, :cond_f

    const-string v7, "application/zip"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 277
    const/4 v0, 0x1

    .line 278
    invoke-static {p2}, Lcom/sec/android/app/myfiles/utils/FileUtils;->OpenFileDialog(Landroid/app/Activity;)V

    goto/16 :goto_0

    .line 250
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_d
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .restart local v2    # "intent":Landroid/content/Intent;
    goto :goto_4

    .line 263
    :cond_e
    const-string v7, "preview"

    const/4 v8, 0x1

    invoke-virtual {v2, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 265
    const-string v7, "sort_order"

    const/4 v8, -0x1

    invoke-virtual {v2, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_5

    .line 280
    :cond_f
    if-eqz v4, :cond_10

    const-string v7, "application/vnd.samsung.scc.LifeTimes"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    .line 281
    const/high16 v7, 0x20000000

    invoke-virtual {v2, v7}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 284
    :cond_10
    const-string v7, "android.intent.action.VIEW"

    invoke-virtual {v2, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 286
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v6

    .line 288
    .local v6, "uri":Landroid/net/Uri;
    invoke-virtual {v2, v6, v4}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 290
    const-string v7, "from-myfiles"

    const/4 v8, 0x1

    invoke-virtual {v2, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 292
    const-string v7, "android.intent.extra.STREAM"

    invoke-virtual {v2, v7, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 294
    const/4 v7, 0x0

    const-string v8, "FileUtils"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Clicked file. MIME type = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 295
    const/4 v7, 0x2

    const-string v8, "FileUtils"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Clicked Uri => "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " MIME type = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 296
    invoke-static {p2}, Lcom/sec/android/app/myfiles/utils/FileUtils;->hideCurrentIm(Landroid/app/Activity;)V

    .line 299
    :try_start_4
    invoke-virtual {p2, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 301
    const/4 v7, 0x0

    const-string v8, "FileUtils"

    const-string v9, "start activity"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Landroid/content/ActivityNotFoundException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 329
    if-nez v0, :cond_0

    .line 331
    invoke-static {p2, p1}, Lcom/sec/android/app/myfiles/utils/FileUtils;->updateOpendFileInfo(Landroid/app/Activity;Ljava/io/File;)V

    goto/16 :goto_0

    .line 303
    :catch_2
    move-exception v1

    .line 305
    .restart local v1    # "e":Landroid/content/ActivityNotFoundException;
    const/4 v0, 0x1

    .line 307
    :try_start_5
    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 309
    const/4 v7, 0x0

    const-string v8, "FileUtils"

    const-string v9, "activity not found"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 311
    invoke-virtual {p2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0064

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {p2, v7}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 329
    if-nez v0, :cond_0

    .line 331
    invoke-static {p2, p1}, Lcom/sec/android/app/myfiles/utils/FileUtils;->updateOpendFileInfo(Landroid/app/Activity;Ljava/io/File;)V

    goto/16 :goto_0

    .line 314
    .end local v1    # "e":Landroid/content/ActivityNotFoundException;
    :catch_3
    move-exception v1

    .line 316
    .local v1, "e":Ljava/lang/SecurityException;
    const/4 v0, 0x1

    .line 318
    :try_start_6
    invoke-virtual {v1}, Ljava/lang/SecurityException;->printStackTrace()V

    .line 320
    const/4 v7, 0x0

    const-string v8, "FileUtils"

    const-string v9, "activity not found"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 325
    invoke-static {p2}, Lcom/sec/android/app/myfiles/utils/FileUtils;->OpenFileDialog(Landroid/app/Activity;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 329
    if-nez v0, :cond_0

    .line 331
    invoke-static {p2, p1}, Lcom/sec/android/app/myfiles/utils/FileUtils;->updateOpendFileInfo(Landroid/app/Activity;Ljava/io/File;)V

    goto/16 :goto_0

    .line 329
    .end local v1    # "e":Ljava/lang/SecurityException;
    :catchall_2
    move-exception v7

    if-nez v0, :cond_11

    .line 331
    invoke-static {p2, p1}, Lcom/sec/android/app/myfiles/utils/FileUtils;->updateOpendFileInfo(Landroid/app/Activity;Ljava/io/File;)V

    :cond_11
    throw v7
.end method

.method public static openFile(ILjava/io/File;Landroid/app/Activity;ZILjava/lang/String;)V
    .locals 12
    .param p0, "categoryType"    # I
    .param p1, "file"    # Ljava/io/File;
    .param p2, "context"    # Landroid/app/Activity;
    .param p3, "isInSelectList"    # Z
    .param p4, "sortOrder"    # I
    .param p5, "searchKey"    # Ljava/lang/String;

    .prologue
    .line 354
    if-nez p2, :cond_1

    .line 554
    :cond_0
    :goto_0
    return-void

    .line 357
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v4

    .line 361
    .local v4, "mediaType":I
    const/4 v1, 0x0

    .line 364
    .local v1, "activityNotFound":Z
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-static {p2, v8}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isOMADrmFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 366
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-static {p2, v8}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->getRealMimeTypeOfDRM(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 368
    .local v5, "mimetype":Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeForMimeType(Ljava/lang/String;)I

    move-result v4

    .line 380
    :goto_1
    invoke-static {v4}, Lcom/sec/android/app/myfiles/MediaFile;->isInstallFileType(I)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 382
    new-instance v3, Landroid/content/Intent;

    const-string v8, "android.intent.action.VIEW"

    invoke-direct {v3, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 386
    .local v3, "intent":Landroid/content/Intent;
    const/16 v8, 0x64

    if-ne v8, v4, :cond_5

    .line 388
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v8

    const-string v9, "application/vnd.android.package-archive"

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 395
    :cond_2
    :goto_2
    const-string v8, "from-myfiles"

    const/4 v9, 0x1

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 397
    invoke-static {p2}, Lcom/sec/android/app/myfiles/utils/FileUtils;->hideCurrentIm(Landroid/app/Activity;)V

    .line 401
    :try_start_0
    invoke-virtual {p2, v3}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 415
    if-nez v1, :cond_0

    .line 417
    invoke-static {p2, p1}, Lcom/sec/android/app/myfiles/utils/FileUtils;->updateOpendFileInfo(Landroid/app/Activity;Ljava/io/File;)V

    goto :goto_0

    .line 370
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v5    # "mimetype":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/myfiles/MediaFile;->needToCheckMimeType(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 372
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, p2}, Lcom/sec/android/app/myfiles/MediaFile;->getMimeTypeFromMediaStore(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .restart local v5    # "mimetype":Ljava/lang/String;
    goto :goto_1

    .line 376
    .end local v5    # "mimetype":Ljava/lang/String;
    :cond_4
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/myfiles/MediaFile;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .restart local v5    # "mimetype":Ljava/lang/String;
    goto :goto_1

    .line 390
    .restart local v3    # "intent":Landroid/content/Intent;
    :cond_5
    const/16 v8, 0x65

    if-ne v8, v4, :cond_2

    .line 392
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v8

    const-string v9, "application/vnd.samsung.widget"

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2

    .line 403
    :catch_0
    move-exception v2

    .line 405
    .local v2, "e":Landroid/content/ActivityNotFoundException;
    const/4 v1, 0x1

    .line 407
    :try_start_1
    invoke-virtual {v2}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 409
    invoke-virtual {p2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b0064

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v8

    invoke-virtual {p2, v8}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 415
    if-nez v1, :cond_0

    .line 417
    invoke-static {p2, p1}, Lcom/sec/android/app/myfiles/utils/FileUtils;->updateOpendFileInfo(Landroid/app/Activity;Ljava/io/File;)V

    goto/16 :goto_0

    .line 415
    .end local v2    # "e":Landroid/content/ActivityNotFoundException;
    :catchall_0
    move-exception v8

    if-nez v1, :cond_6

    .line 417
    invoke-static {p2, p1}, Lcom/sec/android/app/myfiles/utils/FileUtils;->updateOpendFileInfo(Landroid/app/Activity;Ljava/io/File;)V

    :cond_6
    throw v8

    .line 422
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_7
    invoke-static {v4}, Lcom/sec/android/app/myfiles/MediaFile;->isImageFileType(I)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 424
    const/4 v3, 0x0

    .line 426
    .restart local v3    # "intent":Landroid/content/Intent;
    const/4 v8, 0x2

    if-ne p0, v8, :cond_9

    .line 428
    const/4 v8, 0x2

    invoke-static {p2, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesDB;->getIntentForQueryInformation(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v3

    .line 435
    :goto_3
    const-string v8, "android.intent.action.VIEW"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 437
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-static {p2, v8}, Lcom/sec/android/app/myfiles/utils/FileUtils;->pathToUri(Landroid/app/Activity;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 439
    .local v6, "myUri":Landroid/net/Uri;
    if-nez v6, :cond_8

    .line 441
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v6

    .line 444
    :cond_8
    invoke-virtual {v3, v6, v5}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 446
    const-string v8, "android.intent.extra.STREAM"

    invoke-virtual {v3, v8, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 448
    const-string v8, "from-myfiles"

    const/4 v9, 0x1

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 450
    const/4 v8, 0x0

    const-string v9, "FileUtils"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Clicked image file. MIME type = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 451
    const/4 v8, 0x2

    const-string v9, "FileUtils"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "                     Uri = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 452
    invoke-static {p2}, Lcom/sec/android/app/myfiles/utils/FileUtils;->hideCurrentIm(Landroid/app/Activity;)V

    .line 455
    :try_start_2
    invoke-virtual {p2, v3}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 467
    if-nez v1, :cond_0

    .line 469
    invoke-static {p2, p1}, Lcom/sec/android/app/myfiles/utils/FileUtils;->updateOpendFileInfo(Landroid/app/Activity;Ljava/io/File;)V

    goto/16 :goto_0

    .line 432
    .end local v6    # "myUri":Landroid/net/Uri;
    :cond_9
    new-instance v3, Landroid/content/Intent;

    .end local v3    # "intent":Landroid/content/Intent;
    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .restart local v3    # "intent":Landroid/content/Intent;
    goto :goto_3

    .line 457
    .restart local v6    # "myUri":Landroid/net/Uri;
    :catch_1
    move-exception v2

    .line 459
    .restart local v2    # "e":Landroid/content/ActivityNotFoundException;
    const/4 v1, 0x1

    .line 461
    :try_start_3
    invoke-virtual {v2}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 463
    invoke-virtual {p2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b0064

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v8

    invoke-virtual {p2, v8}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 467
    if-nez v1, :cond_0

    .line 469
    invoke-static {p2, p1}, Lcom/sec/android/app/myfiles/utils/FileUtils;->updateOpendFileInfo(Landroid/app/Activity;Ljava/io/File;)V

    goto/16 :goto_0

    .line 467
    .end local v2    # "e":Landroid/content/ActivityNotFoundException;
    :catchall_1
    move-exception v8

    if-nez v1, :cond_a

    .line 469
    invoke-static {p2, p1}, Lcom/sec/android/app/myfiles/utils/FileUtils;->updateOpendFileInfo(Landroid/app/Activity;Ljava/io/File;)V

    :cond_a
    throw v8

    .line 477
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v6    # "myUri":Landroid/net/Uri;
    :cond_b
    const/4 v8, 0x3

    if-eq p0, v8, :cond_c

    const/4 v8, 0x4

    if-eq p0, v8, :cond_c

    const/4 v8, 0x5

    if-ne p0, v8, :cond_e

    .line 480
    :cond_c
    invoke-static {p2, p0}, Lcom/sec/android/app/myfiles/utils/MyFilesDB;->getIntentForQueryInformation(Landroid/content/Context;I)Landroid/content/Intent;

    move-result-object v3

    .line 487
    .restart local v3    # "intent":Landroid/content/Intent;
    :goto_4
    const-string v8, "key_filename"

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 489
    if-nez p3, :cond_f

    .line 491
    const-string v8, "preview"

    const/4 v9, 0x0

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 493
    const-string v8, "sort_order"

    move/from16 v0, p4

    invoke-virtual {v3, v8, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 502
    :goto_5
    invoke-static {v4}, Lcom/sec/android/app/myfiles/MediaFile;->isDocumentFileType(I)Z

    move-result v8

    if-eqz v8, :cond_10

    .line 504
    const/high16 v8, 0x10000000

    invoke-virtual {v3, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 510
    :cond_d
    :goto_6
    const-string v8, "android.intent.action.VIEW"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 512
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v7

    .line 514
    .local v7, "uri":Landroid/net/Uri;
    invoke-virtual {v3, v7, v5}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 516
    const-string v8, "from-myfiles"

    const/4 v9, 0x1

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 518
    const-string v8, "android.intent.extra.STREAM"

    invoke-virtual {v3, v8, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 520
    const-string v8, "search-key"

    move-object/from16 v0, p5

    invoke-virtual {v3, v8, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 522
    const/4 v8, 0x0

    const-string v9, "FileUtils"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Clicked file. MIME type = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 523
    const/4 v8, 0x2

    const-string v9, "FileUtils"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Clicked Uri => "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " MIME type = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " search-key="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p5

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 524
    invoke-static {p2}, Lcom/sec/android/app/myfiles/utils/FileUtils;->hideCurrentIm(Landroid/app/Activity;)V

    .line 527
    :try_start_4
    invoke-virtual {p2, v3}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 529
    const/4 v8, 0x0

    const-string v9, "FileUtils"

    const-string v10, "start activity"

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Landroid/content/ActivityNotFoundException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 543
    if-nez v1, :cond_0

    .line 545
    invoke-static {p2, p1}, Lcom/sec/android/app/myfiles/utils/FileUtils;->updateOpendFileInfo(Landroid/app/Activity;Ljava/io/File;)V

    goto/16 :goto_0

    .line 484
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v7    # "uri":Landroid/net/Uri;
    :cond_e
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .restart local v3    # "intent":Landroid/content/Intent;
    goto/16 :goto_4

    .line 497
    :cond_f
    const-string v8, "preview"

    const/4 v9, 0x1

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 499
    const-string v8, "sort_order"

    const/4 v9, -0x1

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_5

    .line 506
    :cond_10
    if-eqz v5, :cond_d

    const-string v8, "application/vnd.samsung.scc.LifeTimes"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_d

    .line 507
    const/high16 v8, 0x20000000

    invoke-virtual {v3, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto/16 :goto_6

    .line 531
    .restart local v7    # "uri":Landroid/net/Uri;
    :catch_2
    move-exception v2

    .line 533
    .restart local v2    # "e":Landroid/content/ActivityNotFoundException;
    const/4 v1, 0x1

    .line 535
    :try_start_5
    invoke-virtual {v2}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 537
    const/4 v8, 0x0

    const-string v9, "FileUtils"

    const-string v10, "activity not found"

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 539
    invoke-virtual {p2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b0064

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v8}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v8

    invoke-virtual {p2, v8}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 543
    if-nez v1, :cond_0

    .line 545
    invoke-static {p2, p1}, Lcom/sec/android/app/myfiles/utils/FileUtils;->updateOpendFileInfo(Landroid/app/Activity;Ljava/io/File;)V

    goto/16 :goto_0

    .line 543
    .end local v2    # "e":Landroid/content/ActivityNotFoundException;
    :catchall_2
    move-exception v8

    if-nez v1, :cond_11

    .line 545
    invoke-static {p2, p1}, Lcom/sec/android/app/myfiles/utils/FileUtils;->updateOpendFileInfo(Landroid/app/Activity;Ljava/io/File;)V

    :cond_11
    throw v8
.end method

.method public static pathToUri(Landroid/app/Activity;ILjava/lang/String;)Landroid/net/Uri;
    .locals 5
    .param p0, "context"    # Landroid/app/Activity;
    .param p1, "contentType"    # I
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 947
    const/4 v1, 0x2

    const-string v2, "FileUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PATH = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 949
    const/16 v1, 0x8

    if-eq p1, v1, :cond_0

    const/16 v1, 0x1f

    if-ne p1, v1, :cond_1

    .line 951
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 953
    .local v0, "f":Ljava/io/File;
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 957
    .end local v0    # "f":Ljava/io/File;
    :goto_0
    return-object v1

    :cond_1
    invoke-static {p0, p2}, Lcom/sec/android/app/myfiles/utils/FileUtils;->pathToUri(Landroid/app/Activity;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0
.end method

.method public static pathToUri(Landroid/app/Activity;Ljava/lang/String;)Landroid/net/Uri;
    .locals 19
    .param p0, "context"    # Landroid/app/Activity;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 800
    const/4 v3, 0x2

    const-string v4, "FileUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "PATH = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 802
    const/16 v18, 0x0

    .line 804
    .local v18, "uri":Landroid/net/Uri;
    new-instance v10, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v10, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 806
    .local v10, "f":Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 808
    const/4 v3, 0x0

    const-string v4, "FileUtils"

    const-string v5, "Uri :  is not existed"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 810
    const/4 v3, 0x0

    .line 941
    :goto_0
    return-object v3

    .line 814
    :cond_0
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isMediaScannerScannig()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 816
    invoke-static {v10}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v18

    .line 818
    const/4 v3, 0x0

    const-string v4, "FileUtils"

    const-string v5, "PathToUri - Media scanner is running"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    move-object/from16 v3, v18

    .line 820
    goto :goto_0

    .line 826
    :cond_1
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 828
    .local v2, "cr":Landroid/content/ContentResolver;
    const-wide/16 v16, 0x0

    .line 832
    .local v16, "rowId":J
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/MediaFile;->getShareMimeType(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    .line 834
    .local v15, "mimetype":Ljava/lang/String;
    invoke-static {v15}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeForMimeType(Ljava/lang/String;)I

    move-result v14

    .line 838
    .local v14, "mediaType":I
    invoke-static {v14}, Lcom/sec/android/app/myfiles/MediaFile;->isImageFileType(I)Z

    move-result v12

    .line 840
    .local v12, "isImage":Z
    invoke-static {v14}, Lcom/sec/android/app/myfiles/MediaFile;->isVideoFileType(I)Z

    move-result v13

    .line 842
    .local v13, "isVideo":Z
    invoke-static {v14}, Lcom/sec/android/app/myfiles/MediaFile;->isAudioFileType(I)Z

    move-result v11

    .line 844
    .local v11, "isAudio":Z
    if-eqz v12, :cond_5

    .line 846
    sget-object v3, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const-string v5, "_data= ? COLLATE LOCALIZED"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 852
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_4

    .line 854
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_2

    .line 856
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 858
    const-string v3, "_id"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 860
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "content://media/external/images/media/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v18

    .line 863
    :cond_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 932
    .end local v2    # "cr":Landroid/content/ContentResolver;
    .end local v8    # "c":Landroid/database/Cursor;
    .end local v11    # "isAudio":Z
    .end local v12    # "isImage":Z
    .end local v13    # "isVideo":Z
    .end local v14    # "mediaType":I
    .end local v15    # "mimetype":Ljava/lang/String;
    .end local v16    # "rowId":J
    :goto_1
    if-nez v18, :cond_3

    .line 934
    const/4 v3, 0x0

    const-string v4, "FileUtils"

    const-string v5, "pathToUri - URI is null"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 936
    invoke-static {v10}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v18

    .line 939
    :cond_3
    const/4 v3, 0x2

    const-string v4, "FileUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Uri : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v18 .. v18}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    move-object/from16 v3, v18

    .line 941
    goto/16 :goto_0

    .line 867
    .restart local v2    # "cr":Landroid/content/ContentResolver;
    .restart local v8    # "c":Landroid/database/Cursor;
    .restart local v11    # "isAudio":Z
    .restart local v12    # "isImage":Z
    .restart local v13    # "isVideo":Z
    .restart local v14    # "mediaType":I
    .restart local v15    # "mimetype":Ljava/lang/String;
    .restart local v16    # "rowId":J
    :cond_4
    const/4 v3, 0x0

    :try_start_1
    const-string v4, "FileUtils"

    const-string v5, "pathToUri - image c is null"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 927
    .end local v2    # "cr":Landroid/content/ContentResolver;
    .end local v8    # "c":Landroid/database/Cursor;
    .end local v11    # "isAudio":Z
    .end local v12    # "isImage":Z
    .end local v13    # "isVideo":Z
    .end local v14    # "mediaType":I
    .end local v15    # "mimetype":Ljava/lang/String;
    .end local v16    # "rowId":J
    :catch_0
    move-exception v9

    .line 929
    .local v9, "e":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 870
    .end local v9    # "e":Ljava/lang/Exception;
    .restart local v2    # "cr":Landroid/content/ContentResolver;
    .restart local v11    # "isAudio":Z
    .restart local v12    # "isImage":Z
    .restart local v13    # "isVideo":Z
    .restart local v14    # "mediaType":I
    .restart local v15    # "mimetype":Ljava/lang/String;
    .restart local v16    # "rowId":J
    :cond_5
    if-eqz v13, :cond_8

    .line 872
    :try_start_2
    sget-object v3, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const-string v5, "_data= ? COLLATE LOCALIZED"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 878
    .restart local v8    # "c":Landroid/database/Cursor;
    if-eqz v8, :cond_7

    .line 880
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_6

    .line 882
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 884
    const-string v3, "_id"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 886
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "content://media/external/video/media/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v18

    .line 889
    :cond_6
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 893
    :cond_7
    const/4 v3, 0x0

    const-string v4, "FileUtils"

    const-string v5, "pathToUri - video c is null"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 896
    .end local v8    # "c":Landroid/database/Cursor;
    :cond_8
    if-eqz v11, :cond_b

    .line 898
    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const-string v5, "_data= ? COLLATE LOCALIZED"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p1, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 904
    .restart local v8    # "c":Landroid/database/Cursor;
    if-eqz v8, :cond_a

    .line 906
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_9

    .line 908
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 910
    const-string v3, "_id"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 912
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "content://media/external/audio/media/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v18

    .line 915
    :cond_9
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 919
    :cond_a
    const/4 v3, 0x0

    const-string v4, "FileUtils"

    const-string v5, "pathToUri - audio c is null"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 924
    .end local v8    # "c":Landroid/database/Cursor;
    :cond_b
    invoke-static {v10}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v18

    goto/16 :goto_1
.end method

.method public static removeItemFromMediaList(Ljava/lang/String;)V
    .locals 2
    .param p0, "realPath"    # Ljava/lang/String;

    .prologue
    .line 1362
    sget-object v1, Lcom/sec/android/app/myfiles/utils/FileUtils;->mediaList:Ljava/util/HashMap;

    if-eqz v1, :cond_1

    .line 1364
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getItemToMediaList(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1366
    new-instance v0, Ljava/io/File;

    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getItemToMediaList(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1368
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 1371
    .end local v0    # "f":Ljava/io/File;
    :cond_0
    sget-object v1, Lcom/sec/android/app/myfiles/utils/FileUtils;->mediaList:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1373
    :cond_1
    return-void
.end method

.method public static saveBufferToFile(Ljava/lang/String;II[B)V
    .locals 7
    .param p0, "fileName"    # Ljava/lang/String;
    .param p1, "format"    # I
    .param p2, "size"    # I
    .param p3, "buffer"    # [B

    .prologue
    .line 1757
    const-string v1, ".jpg"

    .line 1758
    .local v1, "ext":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1759
    .local v2, "fl":Ljava/io/File;
    const/4 v3, 0x0

    .line 1761
    .local v3, "stream":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1762
    .end local v3    # "stream":Ljava/io/FileOutputStream;
    .local v4, "stream":Ljava/io/FileOutputStream;
    const/4 v6, 0x0

    :try_start_1
    invoke-virtual {v4, p3, v6, p2}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1768
    if-eqz v4, :cond_2

    .line 1770
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->flush()V

    .line 1771
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    move-object v3, v4

    .line 1777
    .end local v4    # "stream":Ljava/io/FileOutputStream;
    .restart local v3    # "stream":Ljava/io/FileOutputStream;
    :cond_0
    :goto_0
    return-void

    .line 1772
    .end local v3    # "stream":Ljava/io/FileOutputStream;
    .restart local v4    # "stream":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v5

    .line 1773
    .local v5, "t":Ljava/lang/Throwable;
    invoke-virtual {v5}, Ljava/lang/Throwable;->printStackTrace()V

    move-object v3, v4

    .line 1774
    .end local v4    # "stream":Ljava/io/FileOutputStream;
    .restart local v3    # "stream":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 1763
    .end local v5    # "t":Ljava/lang/Throwable;
    :catch_1
    move-exception v0

    .line 1764
    .local v0, "e":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1768
    if-eqz v3, :cond_0

    .line 1770
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V

    .line 1771
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 1772
    :catch_2
    move-exception v5

    .line 1773
    .restart local v5    # "t":Ljava/lang/Throwable;
    invoke-virtual {v5}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    .line 1765
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    .end local v5    # "t":Ljava/lang/Throwable;
    :catch_3
    move-exception v0

    .line 1766
    .local v0, "e":Ljava/io/IOException;
    :goto_2
    :try_start_5
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1768
    if-eqz v3, :cond_0

    .line 1770
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V

    .line 1771
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 1772
    :catch_4
    move-exception v5

    .line 1773
    .restart local v5    # "t":Ljava/lang/Throwable;
    invoke-virtual {v5}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    .line 1768
    .end local v0    # "e":Ljava/io/IOException;
    .end local v5    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v6

    :goto_3
    if-eqz v3, :cond_1

    .line 1770
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V

    .line 1771
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_5

    .line 1774
    :cond_1
    :goto_4
    throw v6

    .line 1772
    :catch_5
    move-exception v5

    .line 1773
    .restart local v5    # "t":Ljava/lang/Throwable;
    invoke-virtual {v5}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_4

    .line 1768
    .end local v3    # "stream":Ljava/io/FileOutputStream;
    .end local v5    # "t":Ljava/lang/Throwable;
    .restart local v4    # "stream":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "stream":Ljava/io/FileOutputStream;
    .restart local v3    # "stream":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 1765
    .end local v3    # "stream":Ljava/io/FileOutputStream;
    .restart local v4    # "stream":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v0

    move-object v3, v4

    .end local v4    # "stream":Ljava/io/FileOutputStream;
    .restart local v3    # "stream":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 1763
    .end local v3    # "stream":Ljava/io/FileOutputStream;
    .restart local v4    # "stream":Ljava/io/FileOutputStream;
    :catch_7
    move-exception v0

    move-object v3, v4

    .end local v4    # "stream":Ljava/io/FileOutputStream;
    .restart local v3    # "stream":Ljava/io/FileOutputStream;
    goto :goto_1

    .end local v3    # "stream":Ljava/io/FileOutputStream;
    .restart local v4    # "stream":Ljava/io/FileOutputStream;
    :cond_2
    move-object v3, v4

    .end local v4    # "stream":Ljava/io/FileOutputStream;
    .restart local v3    # "stream":Ljava/io/FileOutputStream;
    goto :goto_0
.end method

.method public static saveThumbtoDB(Ljava/lang/String;Landroid/app/Activity;Landroid/graphics/Bitmap;)Ljava/lang/String;
    .locals 19
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/app/Activity;
    .param p2, "thumb"    # Landroid/graphics/Bitmap;

    .prologue
    .line 692
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v15, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 694
    .local v15, "sb":Ljava/lang/StringBuilder;
    const-string v3, "/DCIM/.thumbnails"

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 696
    new-instance v9, Ljava/io/File;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v9, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 698
    .local v9, "defaultDir":Ljava/io/File;
    const/16 v16, 0x0

    .line 700
    .local v16, "temp":Ljava/io/File;
    const/4 v12, 0x0

    .line 704
    .local v12, "out":Ljava/io/FileOutputStream;
    :try_start_0
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 706
    invoke-virtual {v9}, Ljava/io/File;->mkdir()Z

    .line 709
    :cond_0
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_5

    .line 711
    new-instance v10, Ljava/io/File;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v10, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 713
    .end local v9    # "defaultDir":Ljava/io/File;
    .local v10, "defaultDir":Ljava/io/File;
    :try_start_1
    const-string v3, "thumb"

    const-string v4, ".jpg"

    invoke-static {v3, v4, v10}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v16

    move-object v9, v10

    .line 720
    .end local v10    # "defaultDir":Ljava/io/File;
    .restart local v9    # "defaultDir":Ljava/io/File;
    :goto_0
    :try_start_2
    new-instance v13, Ljava/io/FileOutputStream;

    move-object/from16 v0, v16

    invoke-direct {v13, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 722
    .end local v12    # "out":Ljava/io/FileOutputStream;
    .local v13, "out":Ljava/io/FileOutputStream;
    :try_start_3
    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x3c

    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v4, v13}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 734
    if-eqz v13, :cond_1

    .line 736
    :try_start_4
    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    :cond_1
    move-object v12, v13

    .line 745
    .end local v13    # "out":Ljava/io/FileOutputStream;
    .restart local v12    # "out":Ljava/io/FileOutputStream;
    :cond_2
    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 747
    .local v2, "cr":Landroid/content/ContentResolver;
    const/4 v14, 0x0

    .line 749
    .local v14, "rowId":I
    sget-object v3, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const-string v5, "_data= ? COLLATE LOCALIZED"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p0, v6, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 756
    .local v8, "c":Landroid/database/Cursor;
    if-eqz v8, :cond_4

    .line 758
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 760
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_3

    if-eqz v16, :cond_3

    .line 762
    const-string v3, "_id"

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v8, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 764
    const-string v3, "external"

    invoke-static {v3}, Landroid/provider/MediaStore$Images$Thumbnails;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v17

    .line 766
    .local v17, "uri":Landroid/net/Uri;
    new-instance v18, Landroid/content/ContentValues;

    invoke-direct/range {v18 .. v18}, Landroid/content/ContentValues;-><init>()V

    .line 768
    .local v18, "values":Landroid/content/ContentValues;
    const-string v3, "_data"

    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 770
    const-string v3, "image_id"

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 775
    :try_start_5
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_5
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_5 .. :try_end_5} :catch_4

    .line 788
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/sec/android/app/myfiles/utils/FileUtils;->insertItemToMediaList(Ljava/lang/String;Ljava/lang/String;)V

    .line 791
    .end local v17    # "uri":Landroid/net/Uri;
    .end local v18    # "values":Landroid/content/ContentValues;
    :cond_3
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 794
    :cond_4
    const/4 v3, 0x0

    :goto_2
    return-object v3

    .line 717
    .end local v2    # "cr":Landroid/content/ContentResolver;
    .end local v8    # "c":Landroid/database/Cursor;
    .end local v14    # "rowId":I
    :cond_5
    :try_start_6
    const-string v3, "thumb"

    const-string v4, ".jpg"

    invoke-static {v3, v4, v9}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v16

    goto :goto_0

    .line 739
    .end local v12    # "out":Ljava/io/FileOutputStream;
    .restart local v13    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v11

    .line 741
    .local v11, "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    move-object v12, v13

    .line 743
    .end local v13    # "out":Ljava/io/FileOutputStream;
    .restart local v12    # "out":Ljava/io/FileOutputStream;
    goto :goto_1

    .line 726
    .end local v11    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v11

    .line 728
    .local v11, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_7
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 734
    if-eqz v12, :cond_2

    .line 736
    :try_start_8
    invoke-virtual {v12}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    goto :goto_1

    .line 739
    :catch_2
    move-exception v11

    .line 741
    .local v11, "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 732
    .end local v11    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    .line 734
    :goto_4
    if-eqz v12, :cond_6

    .line 736
    :try_start_9
    invoke-virtual {v12}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    .line 742
    :cond_6
    :goto_5
    throw v3

    .line 739
    :catch_3
    move-exception v11

    .line 741
    .restart local v11    # "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 777
    .end local v11    # "e":Ljava/io/IOException;
    .restart local v2    # "cr":Landroid/content/ContentResolver;
    .restart local v8    # "c":Landroid/database/Cursor;
    .restart local v14    # "rowId":I
    .restart local v17    # "uri":Landroid/net/Uri;
    .restart local v18    # "values":Landroid/content/ContentValues;
    :catch_4
    move-exception v11

    .line 779
    .local v11, "e":Landroid/database/sqlite/SQLiteFullException;
    const v3, 0x7f0b0068

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 781
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 783
    const/4 v3, 0x0

    const-string v4, "FileUtils"

    const-string v5, "saveThumbtoDB : SQLiteFullException is occured."

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 785
    const/4 v3, 0x0

    goto :goto_2

    .line 732
    .end local v2    # "cr":Landroid/content/ContentResolver;
    .end local v8    # "c":Landroid/database/Cursor;
    .end local v9    # "defaultDir":Ljava/io/File;
    .end local v11    # "e":Landroid/database/sqlite/SQLiteFullException;
    .end local v14    # "rowId":I
    .end local v17    # "uri":Landroid/net/Uri;
    .end local v18    # "values":Landroid/content/ContentValues;
    .restart local v10    # "defaultDir":Ljava/io/File;
    :catchall_1
    move-exception v3

    move-object v9, v10

    .end local v10    # "defaultDir":Ljava/io/File;
    .restart local v9    # "defaultDir":Ljava/io/File;
    goto :goto_4

    .end local v12    # "out":Ljava/io/FileOutputStream;
    .restart local v13    # "out":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v3

    move-object v12, v13

    .end local v13    # "out":Ljava/io/FileOutputStream;
    .restart local v12    # "out":Ljava/io/FileOutputStream;
    goto :goto_4

    .line 726
    .end local v9    # "defaultDir":Ljava/io/File;
    .restart local v10    # "defaultDir":Ljava/io/File;
    :catch_5
    move-exception v11

    move-object v9, v10

    .end local v10    # "defaultDir":Ljava/io/File;
    .restart local v9    # "defaultDir":Ljava/io/File;
    goto :goto_3

    .end local v12    # "out":Ljava/io/FileOutputStream;
    .restart local v13    # "out":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v11

    move-object v12, v13

    .end local v13    # "out":Ljava/io/FileOutputStream;
    .restart local v12    # "out":Ljava/io/FileOutputStream;
    goto :goto_3
.end method

.method public static saveUritoMediaDB(Landroid/net/Uri;Ljava/lang/String;Landroid/app/Activity;)V
    .locals 6
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "changedPath"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/app/Activity;

    .prologue
    const/4 v5, 0x0

    .line 1060
    invoke-virtual {p2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1062
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1064
    .local v1, "cv":Landroid/content/ContentValues;
    const-string v3, "_data"

    invoke-virtual {v1, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1068
    const/4 v3, 0x0

    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {v0, p0, v1, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1076
    :goto_0
    return-void

    .line 1070
    :catch_0
    move-exception v2

    .line 1072
    .local v2, "e":Landroid/database/sqlite/SQLiteFullException;
    const v3, 0x7f0b0068

    invoke-static {p2, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 1074
    const-string v3, "FileUtils"

    const-string v4, "saveUritoMediaDB - SQLiteFullException is occured."

    invoke-static {v5, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static setExtractSuccess(Z)V
    .locals 0
    .param p0, "extractSuccess"    # Z

    .prologue
    .line 94
    sput-boolean p0, Lcom/sec/android/app/myfiles/utils/FileUtils;->mIsExtractSuccessRefresh:Z

    .line 95
    return-void
.end method

.method public static updateMediaDB(Ljava/lang/String;Ljava/io/File;Landroid/app/Activity;)V
    .locals 13
    .param p0, "selectedFile"    # Ljava/lang/String;
    .param p1, "destFile"    # Ljava/io/File;
    .param p2, "context"    # Landroid/app/Activity;

    .prologue
    const/4 v12, 0x0

    .line 1168
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->needToCheckMimeType(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1170
    invoke-static {p0, p2}, Lcom/sec/android/app/myfiles/MediaFile;->getMimeTypeFromMediaStore(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 1181
    .local v5, "mimetype":Ljava/lang/String;
    :goto_0
    invoke-static {v5}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeForMimeType(Ljava/lang/String;)I

    move-result v4

    .line 1183
    .local v4, "mediaType":I
    invoke-static {v4}, Lcom/sec/android/app/myfiles/MediaFile;->isImageFileType(I)Z

    move-result v2

    .line 1185
    .local v2, "isImage":Z
    invoke-static {v4}, Lcom/sec/android/app/myfiles/MediaFile;->isVideoFileType(I)Z

    move-result v3

    .line 1187
    .local v3, "isVideo":Z
    invoke-static {v4}, Lcom/sec/android/app/myfiles/MediaFile;->isAudioFileType(I)Z

    move-result v1

    .line 1192
    .local v1, "isAudio":Z
    if-eqz v1, :cond_0

    .line 1194
    :try_start_0
    const-string v8, "content://media/external/audio/media/"

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-static {p0, v5, p2}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getIDfromUri(Ljava/lang/String;Ljava/lang/String;Landroid/app/Activity;)I

    move-result v9

    int-to-long v10, v9

    invoke-static {v8, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    .line 1197
    .local v6, "uri":Landroid/net/Uri;
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 1203
    .local v7, "values":Landroid/content/ContentValues;
    const-string v8, "_data"

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1205
    const-string v8, "date_modified"

    const-string v9, "0"

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1207
    invoke-virtual {p2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v8, v6, v7, v9, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1209
    new-instance v8, Landroid/content/Intent;

    const-string v9, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "file://"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p2, v8}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1213
    .end local v6    # "uri":Landroid/net/Uri;
    .end local v7    # "values":Landroid/content/ContentValues;
    :cond_0
    if-eqz v3, :cond_1

    .line 1214
    const-string v8, "content://media/external/video/media/"

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-static {p0, v5, p2}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getIDfromUri(Ljava/lang/String;Ljava/lang/String;Landroid/app/Activity;)I

    move-result v9

    int-to-long v10, v9

    invoke-static {v8, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    .line 1217
    .restart local v6    # "uri":Landroid/net/Uri;
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 1223
    .restart local v7    # "values":Landroid/content/ContentValues;
    const-string v8, "_data"

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1225
    const-string v8, "date_modified"

    const-string v9, "0"

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1227
    invoke-virtual {p2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v8, v6, v7, v9, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1229
    new-instance v8, Landroid/content/Intent;

    const-string v9, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "file://"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p2, v8}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1233
    .end local v6    # "uri":Landroid/net/Uri;
    .end local v7    # "values":Landroid/content/ContentValues;
    :cond_1
    if-eqz v2, :cond_2

    .line 1234
    const-string v8, "content://media/external/images/media/"

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-static {p0, v5, p2}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getIDfromUri(Ljava/lang/String;Ljava/lang/String;Landroid/app/Activity;)I

    move-result v9

    int-to-long v10, v9

    invoke-static {v8, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    .line 1237
    .restart local v6    # "uri":Landroid/net/Uri;
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 1241
    .restart local v7    # "values":Landroid/content/ContentValues;
    const-string v8, "_data"

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1243
    const-string v8, "date_modified"

    const-string v9, "0"

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1249
    invoke-virtual {p2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v8, v6, v7, v9, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1251
    new-instance v8, Landroid/content/Intent;

    const-string v9, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "file://"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p2, v8}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteFullException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1261
    .end local v6    # "uri":Landroid/net/Uri;
    .end local v7    # "values":Landroid/content/ContentValues;
    :cond_2
    :goto_1
    return-void

    .line 1172
    .end local v1    # "isAudio":Z
    .end local v2    # "isImage":Z
    .end local v3    # "isVideo":Z
    .end local v4    # "mediaType":I
    .end local v5    # "mimetype":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-static {p2, v8}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isOMADrmFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1174
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-static {p2, v8}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->getRealMimeTypeOfDRM(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .restart local v5    # "mimetype":Ljava/lang/String;
    goto/16 :goto_0

    .line 1178
    .end local v5    # "mimetype":Ljava/lang/String;
    :cond_4
    invoke-static {p0}, Lcom/sec/android/app/myfiles/MediaFile;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .restart local v5    # "mimetype":Ljava/lang/String;
    goto/16 :goto_0

    .line 1255
    .restart local v1    # "isAudio":Z
    .restart local v2    # "isImage":Z
    .restart local v3    # "isVideo":Z
    .restart local v4    # "mediaType":I
    :catch_0
    move-exception v0

    .line 1257
    .local v0, "e":Landroid/database/sqlite/SQLiteFullException;
    const v8, 0x7f0b0068

    invoke-static {p2, v8, v12}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    .line 1259
    const-string v8, "FileUtils"

    const-string v9, "updateMediaDB - SQLiteFullException is occured."

    invoke-static {v12, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private static updateOpendFileInfo(Landroid/app/Activity;Ljava/io/File;)V
    .locals 23
    .param p0, "context"    # Landroid/app/Activity;
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 1378
    if-nez p0, :cond_1

    .line 1483
    :cond_0
    :goto_0
    return-void

    .line 1381
    :cond_1
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 1389
    .local v2, "cr":Landroid/content/ContentResolver;
    new-instance v22, Landroid/content/ContentValues;

    invoke-direct/range {v22 .. v22}, Landroid/content/ContentValues;-><init>()V

    .line 1391
    .local v22, "values":Landroid/content/ContentValues;
    const/4 v3, 0x3

    new-array v4, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "_id"

    aput-object v5, v4, v3

    const/4 v3, 0x1

    const-string v5, "_size"

    aput-object v5, v4, v3

    const/4 v3, 0x2

    const-string v5, "date_modified"

    aput-object v5, v4, v3

    .line 1397
    .local v4, "projection":[Ljava/lang/String;
    const-string v3, "external"

    invoke-static {v3}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string v5, "_data = ?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const-string v7, "datetaken DESC limit 1"

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 1403
    .local v12, "fileCursor":Landroid/database/Cursor;
    if-eqz v12, :cond_0

    .line 1405
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v3

    const/4 v5, 0x1

    if-ne v3, v5, :cond_3

    .line 1408
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1410
    const-string v3, "_id"

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 1412
    .local v14, "fileId":J
    const-string v3, "_size"

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 1414
    .local v16, "fileSize":J
    const-string v3, "date_modified"

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v12, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 1418
    .local v20, "modifiedDate":J
    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v6

    const/4 v7, 0x0

    const-string v8, "_data LIKE ?"

    const/4 v3, 0x1

    new-array v9, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v9, v3

    const/4 v10, 0x0

    move-object v5, v2

    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 1424
    .local v11, "c":Landroid/database/Cursor;
    if-eqz v11, :cond_3

    .line 1426
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v3

    const/4 v5, 0x1

    if-ne v3, v5, :cond_4

    .line 1428
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1430
    const-string v3, "_id"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 1432
    .local v18, "id":J
    const-string v3, "count_opened"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 1434
    .local v13, "openCount":I
    const-string v3, "_size"

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1436
    const-string v3, "date_modified"

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1438
    const-string v3, "date_used"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1440
    const-string v3, "count_opened"

    add-int/lit8 v5, v13, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1442
    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Files;->getContentUri(J)Landroid/net/Uri;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v2, v3, v0, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_2

    .line 1444
    const/4 v3, 0x0

    const-string v5, "FileUtils"

    const-string v6, "update file open count"

    invoke-static {v3, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1477
    .end local v13    # "openCount":I
    .end local v18    # "id":J
    :cond_2
    :goto_1
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1481
    .end local v11    # "c":Landroid/database/Cursor;
    .end local v14    # "fileId":J
    .end local v16    # "fileSize":J
    .end local v20    # "modifiedDate":J
    :cond_3
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 1447
    .restart local v11    # "c":Landroid/database/Cursor;
    .restart local v14    # "fileId":J
    .restart local v16    # "fileSize":J
    .restart local v20    # "modifiedDate":J
    :cond_4
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-nez v3, :cond_2

    .line 1449
    if-eqz v12, :cond_2

    .line 1451
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v3

    const/4 v5, 0x1

    if-ne v3, v5, :cond_5

    .line 1453
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1455
    const-string v3, "_data"

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1457
    const-string v3, "file_id"

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1459
    const-string v3, "_size"

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1461
    const-string v3, "date_modified"

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1463
    const-string v3, "date_used"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1465
    const-string v3, "count_opened"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1467
    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 1469
    const/4 v3, 0x0

    const-string v5, "FileUtils"

    const-string v6, "new file is inserted to table"

    invoke-static {v3, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1473
    :cond_5
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    goto :goto_1
.end method
