.class public final enum Lcom/sec/android/app/myfiles/utils/FileType;
.super Ljava/lang/Enum;
.source "FileType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/myfiles/utils/FileType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/myfiles/utils/FileType;

.field public static final enum AUDIO:Lcom/sec/android/app/myfiles/utils/FileType;

.field public static final enum DOCUMENT:Lcom/sec/android/app/myfiles/utils/FileType;

.field public static final enum DOWNLOADEDAPPS:Lcom/sec/android/app/myfiles/utils/FileType;

.field public static final enum IMAGE:Lcom/sec/android/app/myfiles/utils/FileType;

.field public static final enum VIDEO:Lcom/sec/android/app/myfiles/utils/FileType;


# instance fields
.field private final mId:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 20
    new-instance v0, Lcom/sec/android/app/myfiles/utils/FileType;

    const-string v1, "IMAGE"

    invoke-direct {v0, v1, v7, v3}, Lcom/sec/android/app/myfiles/utils/FileType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/FileType;->IMAGE:Lcom/sec/android/app/myfiles/utils/FileType;

    .line 21
    new-instance v0, Lcom/sec/android/app/myfiles/utils/FileType;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v3, v4}, Lcom/sec/android/app/myfiles/utils/FileType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/FileType;->VIDEO:Lcom/sec/android/app/myfiles/utils/FileType;

    .line 22
    new-instance v0, Lcom/sec/android/app/myfiles/utils/FileType;

    const-string v1, "AUDIO"

    invoke-direct {v0, v1, v4, v5}, Lcom/sec/android/app/myfiles/utils/FileType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/FileType;->AUDIO:Lcom/sec/android/app/myfiles/utils/FileType;

    .line 23
    new-instance v0, Lcom/sec/android/app/myfiles/utils/FileType;

    const-string v1, "DOCUMENT"

    invoke-direct {v0, v1, v5, v6}, Lcom/sec/android/app/myfiles/utils/FileType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/FileType;->DOCUMENT:Lcom/sec/android/app/myfiles/utils/FileType;

    .line 24
    new-instance v0, Lcom/sec/android/app/myfiles/utils/FileType;

    const-string v1, "DOWNLOADEDAPPS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/android/app/myfiles/utils/FileType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/FileType;->DOWNLOADEDAPPS:Lcom/sec/android/app/myfiles/utils/FileType;

    .line 19
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/app/myfiles/utils/FileType;

    sget-object v1, Lcom/sec/android/app/myfiles/utils/FileType;->IMAGE:Lcom/sec/android/app/myfiles/utils/FileType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/android/app/myfiles/utils/FileType;->VIDEO:Lcom/sec/android/app/myfiles/utils/FileType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/myfiles/utils/FileType;->AUDIO:Lcom/sec/android/app/myfiles/utils/FileType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/myfiles/utils/FileType;->DOCUMENT:Lcom/sec/android/app/myfiles/utils/FileType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/myfiles/utils/FileType;->DOWNLOADEDAPPS:Lcom/sec/android/app/myfiles/utils/FileType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/app/myfiles/utils/FileType;->$VALUES:[Lcom/sec/android/app/myfiles/utils/FileType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "id"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 27
    iput p3, p0, Lcom/sec/android/app/myfiles/utils/FileType;->mId:I

    .line 28
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/myfiles/utils/FileType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 19
    const-class v0, Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/utils/FileType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/myfiles/utils/FileType;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/app/myfiles/utils/FileType;->$VALUES:[Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-virtual {v0}, [Lcom/sec/android/app/myfiles/utils/FileType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/myfiles/utils/FileType;

    return-object v0
.end method


# virtual methods
.method public getId()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/sec/android/app/myfiles/utils/FileType;->mId:I

    return v0
.end method
