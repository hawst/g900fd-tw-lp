.class Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$2;
.super Ljava/lang/Object;
.source "BaiduAdapter.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;

.field final synthetic val$filePath:Ljava/lang/String;

.field final synthetic val$fileType:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 282
    iput-object p1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$2;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;

    iput p2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$2;->val$fileType:I

    iput-object p3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$2;->val$filePath:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x0

    .line 287
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$2;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->isSelectMode()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$2;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->access$000(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, p2}, Lcom/sec/android/app/myfiles/utils/Utils;->isHoverSettingDisabled(Landroid/content/Context;Landroid/view/MotionEvent;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$2;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->access$100(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 290
    const/4 v0, 0x0

    .line 291
    .local v0, "bSupportHover":Z
    const/4 v2, 0x0

    .line 293
    .local v2, "hoverMgr":Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 294
    .local v3, "hoverRect":Landroid/graphics/Rect;
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    check-cast v4, Landroid/view/View;

    invoke-virtual {v4, v3}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 296
    iget v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$2;->val$fileType:I

    invoke-static {v4}, Lcom/sec/android/app/myfiles/MediaFile;->isImageFileType(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 298
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$2;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->access$100(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v2

    .line 300
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$2;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->access$100(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v5

    const/16 v6, 0x1f

    iget-object v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$2;->val$filePath:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$2;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mThumbnailCache:Landroid/util/LruCache;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->access$200(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;)Landroid/util/LruCache;

    move-result-object v4

    iget-object v8, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$2;->val$filePath:Ljava/lang/String;

    invoke-virtual {v4, v8}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    invoke-virtual {v5, v6, v3, v7, v4}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->setParam(ILandroid/graphics/Rect;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 302
    const/4 v0, 0x1

    .line 309
    :goto_0
    if-eqz v0, :cond_0

    .line 311
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 359
    .end local v0    # "bSupportHover":Z
    .end local v2    # "hoverMgr":Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    .end local v3    # "hoverRect":Landroid/graphics/Rect;
    :cond_0
    :goto_1
    :pswitch_0
    return v9

    .line 306
    .restart local v0    # "bSupportHover":Z
    .restart local v2    # "hoverMgr":Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    .restart local v3    # "hoverRect":Landroid/graphics/Rect;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 317
    :pswitch_1
    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {p2, v4}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v4

    if-ne v4, v10, :cond_2

    .line 319
    const/16 v4, 0xa

    const/4 v5, -0x1

    invoke-static {v4, v5}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 327
    :cond_2
    :goto_2
    const/16 v4, 0x1f4

    invoke-virtual {v2, v4}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->sendDelayedMessage(I)V

    goto :goto_1

    .line 322
    :catch_0
    move-exception v1

    .line 324
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 332
    .end local v1    # "e":Landroid/os/RemoteException;
    :pswitch_2
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->removeDelayedMessage()V

    .line 336
    const/4 v4, 0x0

    :try_start_1
    invoke-virtual {p2, v4}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v4

    if-ne v4, v10, :cond_0

    .line 338
    const/4 v4, 0x1

    const/4 v5, -0x1

    invoke-static {v4, v5}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 341
    :catch_1
    move-exception v1

    .line 343
    .restart local v1    # "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 311
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
