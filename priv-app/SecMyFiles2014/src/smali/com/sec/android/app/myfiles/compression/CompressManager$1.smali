.class Lcom/sec/android/app/myfiles/compression/CompressManager$1;
.super Ljava/lang/Object;
.source "CompressManager.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/compression/AbsCompressor$OnOperationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/compression/CompressManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/compression/CompressManager;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/compression/CompressManager;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/sec/android/app/myfiles/compression/CompressManager$1;->this$0:Lcom/sec/android/app/myfiles/compression/CompressManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancelled(II)V
    .locals 1
    .param p1, "operation"    # I
    .param p2, "resultCode"    # I

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/myfiles/compression/CompressManager$1;->this$0:Lcom/sec/android/app/myfiles/compression/CompressManager;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/compression/CompressManager;->mCompressorListener:Lcom/sec/android/app/myfiles/compression/CompressManager$OnCompressorListener;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/myfiles/compression/CompressManager$1;->this$0:Lcom/sec/android/app/myfiles/compression/CompressManager;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/compression/CompressManager;->mCompressorListener:Lcom/sec/android/app/myfiles/compression/CompressManager$OnCompressorListener;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/myfiles/compression/CompressManager$OnCompressorListener;->onOperationCancelled(II)V

    .line 122
    :cond_0
    return-void
.end method

.method public onCompleted(I)V
    .locals 1
    .param p1, "operation"    # I

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/app/myfiles/compression/CompressManager$1;->this$0:Lcom/sec/android/app/myfiles/compression/CompressManager;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/compression/CompressManager;->mCompressorListener:Lcom/sec/android/app/myfiles/compression/CompressManager$OnCompressorListener;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/myfiles/compression/CompressManager$1;->this$0:Lcom/sec/android/app/myfiles/compression/CompressManager;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/compression/CompressManager;->mCompressorListener:Lcom/sec/android/app/myfiles/compression/CompressManager$OnCompressorListener;

    invoke-interface {v0, p1}, Lcom/sec/android/app/myfiles/compression/CompressManager$OnCompressorListener;->onOperationCompleted(I)V

    .line 113
    :cond_0
    return-void
.end method
