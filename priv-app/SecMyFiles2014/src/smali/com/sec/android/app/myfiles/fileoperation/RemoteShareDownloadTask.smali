.class public Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;
.super Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;
.source "RemoteShareDownloadTask.java"


# static fields
.field private static final ACTION_REMOTE_SHARE_CANCEL:Ljava/lang/String; = "com.sec.rshare.intent.ACTION_CANCEL"

.field private static final ACTION_REMOTE_SHARE_DOWNLOAD:Ljava/lang/String; = "com.sec.rshare.intent.ACTION_DOWNLOAD"

.field private static final CONTENT_URI:Landroid/net/Uri;

.field private static final EXTRA_CLIENT_CALLBACK:Ljava/lang/String; = "extra_client_callback"

.field private static final EXTRA_CONTENT_ID_LIST:Ljava/lang/String; = "extra_content_id_list"

.field private static final EXTRA_MEDIA_URI:Ljava/lang/String; = "extra_media_uri"

.field private static final EXTRA_PROGRESS_BYTE:Ljava/lang/String; = "extra_progress_byte"

.field private static final EXTRA_PROGRESS_FILE_NAME:Ljava/lang/String; = "extra_progress_file_name"

.field private static final EXTRA_PROGRESS_TOTAL_FILE_LENGTH:Ljava/lang/String; = "extra_progress_total_file_length"

.field private static final MODULE:Ljava/lang/String; = "RemoteShareDownloadTask"

.field private static final RESULT_PROGRESS:I = 0x2

.field private static final RESULT_START:I = 0x1

.field private static final RESULT_SUCCESS:I = 0x0

.field private static final TOKEN_REQ_DOWNLOAD:I = 0x7d1


# instance fields
.field private mClientMessenger:Landroid/os/Messenger;

.field private mDownloadHandler:Landroid/os/Handler;

.field private mDownloadableId:Ljava/lang/String;

.field private final mDownloadedCondition:Ljava/util/concurrent/locks/Condition;

.field private mErrorCode:I

.field private final mLock:Ljava/util/concurrent/locks/Lock;

.field private mMediaId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-string v0, "content://com.sec.rshare/media"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/view/View;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contentType"    # I
    .param p3, "progressView"    # Landroid/view/View;
    .param p4, "operation"    # I

    .prologue
    .line 77
    invoke-direct {p0, p1, p4, p2, p3}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;-><init>(Landroid/content/Context;IILandroid/view/View;)V

    .line 71
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mErrorCode:I

    .line 72
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mLock:Ljava/util/concurrent/locks/Lock;

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mDownloadedCondition:Ljava/util/concurrent/locks/Condition;

    .line 78
    iput p2, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mSourceFragmentId:I

    .line 79
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->createHandler()Landroid/os/Handler;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mDownloadHandler:Landroid/os/Handler;

    .line 80
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mDownloadHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mClientMessenger:Landroid/os/Messenger;

    .line 81
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;[Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->publishProgress([Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;)Ljava/util/concurrent/locks/Lock;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mLock:Ljava/util/concurrent/locks/Lock;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;)Ljava/util/concurrent/locks/Condition;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mDownloadedCondition:Ljava/util/concurrent/locks/Condition;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;[Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->publishProgress([Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$402(Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;
    .param p1, "x1"    # I

    .prologue
    .line 41
    iput p1, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mErrorCode:I

    return p1
.end method

.method private createHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 132
    new-instance v0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask$1;-><init>(Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;)V

    return-object v0
.end method

.method private sendCancelIntent()V
    .locals 4

    .prologue
    .line 122
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mContext:Landroid/content/Context;

    if-nez v2, :cond_0

    .line 129
    :goto_0
    return-void

    .line 125
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.rshare.intent.ACTION_CANCEL"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 126
    .local v0, "intent":Landroid/content/Intent;
    sget-object v2, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->CONTENT_URI:Landroid/net/Uri;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mMediaId:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 127
    .local v1, "uri":Landroid/net/Uri;
    const-string v2, "extra_media_uri"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 128
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method


# virtual methods
.method protected cancelOperation()V
    .locals 1

    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    :goto_0
    return-void

    .line 214
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->sendCancelIntent()V

    .line 215
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->cancelOperation()V

    goto :goto_0
.end method

.method protected varargs doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;
    .locals 9
    .param p1, "params"    # [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 86
    aget-object v3, p1, v7

    .line 87
    .local v3, "param":Lcom/sec/android/app/myfiles/element/FileOperationParam;
    if-eqz v3, :cond_0

    iget-object v5, v3, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    if-eqz v5, :cond_0

    iget-object v5, v3, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x2

    if-ge v5, v6, :cond_1

    .line 118
    :cond_0
    :goto_0
    return-object v8

    .line 91
    :cond_1
    iput v7, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mErrorCode:I

    .line 92
    iget-object v5, v3, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iput-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mDownloadableId:Ljava/lang/String;

    .line 93
    iget-object v5, v3, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iput-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mMediaId:Ljava/lang/String;

    .line 94
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mDownloadableId:Ljava/lang/String;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mDownloadableId:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mMediaId:Ljava/lang/String;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mMediaId:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_0

    .line 97
    sget-object v5, Lcom/sec/android/app/myfiles/utils/Constant;->RSHARE_DOWNLOAD_DIR_URI:Ljava/lang/String;

    iput-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mTargetFolder:Ljava/lang/String;

    .line 99
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 100
    .local v4, "selItemsInt":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mDownloadableId:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 102
    sget-object v5, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->CONTENT_URI:Landroid/net/Uri;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mMediaId:Ljava/lang/String;

    invoke-static {v5, v6}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 104
    .local v2, "mediaUri":Landroid/net/Uri;
    new-instance v1, Landroid/content/Intent;

    const-string v5, "com.sec.rshare.intent.ACTION_DOWNLOAD"

    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 105
    .local v1, "intent":Landroid/content/Intent;
    const-string v5, "extra_client_callback"

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mClientMessenger:Landroid/os/Messenger;

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 106
    const-string v5, "extra_media_uri"

    invoke-virtual {v1, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 107
    const-string v5, "extra_content_id_list"

    invoke-virtual {v1, v5, v4}, Landroid/content/Intent;->putIntegerArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 108
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 109
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 112
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mDownloadedCondition:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v5}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 113
    :catch_0
    move-exception v0

    .line 114
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 116
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v5

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v6}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v5
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 41
    check-cast p1, [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;

    move-result-object v0

    return-object v0
.end method

.method public isError()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mErrorCode:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onCancelled()V
    .locals 0

    .prologue
    .line 221
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->onCancelled()V

    .line 222
    return-void
.end method

.method protected onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V
    .locals 0
    .param p1, "result"    # Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .prologue
    .line 207
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V

    .line 208
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 41
    check-cast p1, Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Lcom/sec/android/app/myfiles/element/FileOperationProgress;)V
    .locals 4
    .param p1, "values"    # [Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    .prologue
    .line 197
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 198
    const/4 v1, 0x0

    aget-object v1, p1, v1

    iget v0, v1, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mCurrentTargetPercentage:I

    .line 199
    .local v0, "percent":I
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 200
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mPercentText:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 201
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mPercentText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 203
    :cond_0
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 41
    check-cast p1, [Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->onProgressUpdate([Lcom/sec/android/app/myfiles/element/FileOperationProgress;)V

    return-void
.end method

.method protected setupProgressView()V
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 192
    return-void
.end method

.method protected startRename(Ljava/lang/String;Z)V
    .locals 0
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "isFile"    # Z

    .prologue
    .line 227
    return-void
.end method
