.class final Lcom/sec/android/app/myfiles/utils/Utils$4;
.super Ljava/lang/Object;
.source "Utils.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/utils/Utils;->getEditTextFilterDigit(Landroid/content/Context;I)[Landroid/text/InputFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$ctx:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 3111
    iput-object p1, p0, Lcom/sec/android/app/myfiles/utils/Utils$4;->val$ctx:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 4
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .prologue
    const v3, 0x7f0b00a2

    .line 3116
    move v0, p2

    .local v0, "i":I
    :goto_0
    if-ge v0, p3, :cond_2

    .line 3118
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isDigit(C)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3120
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$100()Landroid/widget/Toast;

    move-result-object v1

    if-nez v1, :cond_0

    .line 3122
    iget-object v1, p0, Lcom/sec/android/app/myfiles/utils/Utils$4;->val$ctx:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    # setter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->access$102(Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 3130
    :goto_1
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$100()Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 3132
    const-string v1, ""

    .line 3138
    :goto_2
    return-object v1

    .line 3126
    :cond_0
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$100()Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/Toast;->setText(I)V

    goto :goto_1

    .line 3116
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3138
    :cond_2
    const/4 v1, 0x0

    goto :goto_2
.end method
