.class public interface abstract Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$MediaColumns;
.super Ljava/lang/Object;
.source "RemoteShareNavigation.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "MediaColumns"
.end annotation


# static fields
.field public static final DATE:Ljava/lang/String; = "date"

.field public static final EXPIRE_DATE:Ljava/lang/String; = "expire_date"

.field public static final MEDIA_BOX:Ljava/lang/String; = "media_box"

.field public static final RECIPIENT_IDS:Ljava/lang/String; = "recipient_ids"

.field public static final STATUS:Ljava/lang/String; = "status"

.field public static final TITLE:Ljava/lang/String; = "title"
