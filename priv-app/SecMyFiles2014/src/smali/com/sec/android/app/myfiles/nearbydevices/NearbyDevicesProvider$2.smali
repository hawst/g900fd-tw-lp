.class Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$2;
.super Ljava/lang/Object;
.source "NearbyDevicesProvider.java"

# interfaces
.implements Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->connect()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$2;->this$0:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreated(Lcom/samsung/android/allshare/ServiceProvider;Lcom/samsung/android/allshare/ServiceConnector$ServiceState;)V
    .locals 2
    .param p1, "provider"    # Lcom/samsung/android/allshare/ServiceProvider;
    .param p2, "state"    # Lcom/samsung/android/allshare/ServiceConnector$ServiceState;

    .prologue
    .line 165
    const-string v0, "MyFiles[NearbyDevices]"

    const-string v1, "ServiceConnector onCreated"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$2;->this$0:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    check-cast p1, Lcom/samsung/android/allshare/media/MediaServiceProvider;

    .end local p1    # "provider":Lcom/samsung/android/allshare/ServiceProvider;
    # setter for: Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;
    invoke-static {v0, p1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->access$102(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;Lcom/samsung/android/allshare/media/MediaServiceProvider;)Lcom/samsung/android/allshare/media/MediaServiceProvider;

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$2;->this$0:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    # getter for: Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->access$100(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)Lcom/samsung/android/allshare/media/MediaServiceProvider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/media/MediaServiceProvider;->getDownloader()Lcom/samsung/android/allshare/extension/SECDownloader;

    move-result-object v0

    # setter for: Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mDownloader:Lcom/samsung/android/allshare/extension/SECDownloader;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->access$502(Lcom/samsung/android/allshare/extension/SECDownloader;)Lcom/samsung/android/allshare/extension/SECDownloader;

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$2;->this$0:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mConnected:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->access$202(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;Z)Z

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$2;->this$0:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    # invokes: Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->fillDevices()V
    invoke-static {v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->access$600(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)V

    .line 170
    return-void
.end method

.method public onDeleted(Lcom/samsung/android/allshare/ServiceProvider;)V
    .locals 2
    .param p1, "provider"    # Lcom/samsung/android/allshare/ServiceProvider;

    .prologue
    .line 152
    const-string v0, "MyFiles[NearbyDevices]"

    const-string v1, "ServiceConnector onDeleted"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$2;->this$0:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->access$102(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;Lcom/samsung/android/allshare/media/MediaServiceProvider;)Lcom/samsung/android/allshare/media/MediaServiceProvider;

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$2;->this$0:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mConnected:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->access$202(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;Z)Z

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$2;->this$0:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    # getter for: Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mDevicesCollection:Ljava/util/Collection;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->access$300(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$2;->this$0:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    # getter for: Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mSaveCurDevices:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->access$400(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$2;->this$0:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    # getter for: Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mSaveCurDevices:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->access$400(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider$2;->this$0:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    # getter for: Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->mSaveCurDevices:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->access$400(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 161
    :cond_0
    return-void
.end method
