.class Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;
.super Ljava/lang/Object;
.source "BaiduDownLoadOperationTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UploadableFile"
.end annotation


# instance fields
.field public mLocalFile:Ljava/io/File;

.field public mNewFileName:Ljava/lang/String;

.field public mServerPath:Ljava/lang/String;

.field public mSize:J

.field public mStarted:Z

.field public mUploadPercent:J


# direct methods
.method public constructor <init>(Ljava/io/File;Ljava/lang/String;JLjava/lang/String;)V
    .locals 5
    .param p1, "localFile"    # Ljava/io/File;
    .param p2, "newName"    # Ljava/lang/String;
    .param p3, "currentSize"    # J
    .param p5, "serverPath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;->mLocalFile:Ljava/io/File;

    .line 177
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;->mServerPath:Ljava/lang/String;

    .line 179
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;->mNewFileName:Ljava/lang/String;

    .line 181
    iput-wide v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;->mSize:J

    .line 183
    iput-wide v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;->mUploadPercent:J

    .line 185
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;->mStarted:Z

    .line 189
    iput-object p1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;->mLocalFile:Ljava/io/File;

    .line 190
    iput-object p2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;->mNewFileName:Ljava/lang/String;

    .line 191
    iput-wide p3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;->mSize:J

    .line 192
    iput-object p5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;->mServerPath:Ljava/lang/String;

    .line 193
    iput-wide v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;->mUploadPercent:J

    .line 194
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;->mStarted:Z

    .line 195
    return-void
.end method
