.class public abstract Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;
.super Landroid/app/DialogFragment;
.source "AsyncOpenOperationUiFragment.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationFunction;


# static fields
.field private static final HW_BACK_BUTTON_ENABLED:Z

.field private static final TAG:Ljava/lang/String;


# instance fields
.field protected mCancelButtonActive:Z

.field protected mCancelOperation:Ljava/lang/Runnable;

.field mOperationType:I

.field protected mParentFrgment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

.field mProgressView:Landroid/view/View;

.field private final mSync:Ljava/lang/Object;

.field protected mWaitOnCancel:Z

.field protected mWifiDisconnect:Z

.field private mbCompleteCondition:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 58
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 54
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mbCompleteCondition:Z

    .line 56
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mSync:Ljava/lang/Object;

    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mCancelButtonActive:Z

    .line 60
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mWaitOnCancel:Z

    .line 61
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsFragment;)V
    .locals 2
    .param p1, "ParentFrgment"    # Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .prologue
    const/4 v1, 0x0

    .line 63
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 54
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mbCompleteCondition:Z

    .line 56
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mSync:Ljava/lang/Object;

    .line 64
    iput-object p1, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mParentFrgment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .line 65
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mCancelButtonActive:Z

    .line 66
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mWaitOnCancel:Z

    .line 67
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsFragment;Ljava/lang/Runnable;)V
    .locals 2
    .param p1, "parentFragment"    # Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    .param p2, "cancelOperation"    # Ljava/lang/Runnable;

    .prologue
    const/4 v1, 0x0

    .line 75
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 54
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mbCompleteCondition:Z

    .line 56
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mSync:Ljava/lang/Object;

    .line 76
    iput-object p1, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mParentFrgment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .line 77
    iput-object p2, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mCancelOperation:Ljava/lang/Runnable;

    .line 78
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mCancelButtonActive:Z

    .line 79
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mWaitOnCancel:Z

    .line 80
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsFragment;Ljava/lang/Runnable;Z)V
    .locals 2
    .param p1, "parentFragment"    # Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    .param p2, "cancelOperation"    # Ljava/lang/Runnable;
    .param p3, "cancelButtonActive"    # Z

    .prologue
    const/4 v1, 0x0

    .line 82
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 54
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mbCompleteCondition:Z

    .line 56
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mSync:Ljava/lang/Object;

    .line 83
    iput-object p1, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mParentFrgment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .line 84
    iput-object p2, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mCancelOperation:Ljava/lang/Runnable;

    .line 85
    iput-boolean p3, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mCancelButtonActive:Z

    .line 86
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mWaitOnCancel:Z

    .line 87
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsFragment;Ljava/lang/Runnable;ZZ)V
    .locals 1
    .param p1, "parentFragment"    # Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    .param p2, "cancelOperation"    # Ljava/lang/Runnable;
    .param p3, "cancelButtonActive"    # Z
    .param p4, "waitOnCancel"    # Z

    .prologue
    .line 90
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mbCompleteCondition:Z

    .line 56
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mSync:Ljava/lang/Object;

    .line 91
    iput-object p1, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mParentFrgment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .line 92
    iput-object p2, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mCancelOperation:Ljava/lang/Runnable;

    .line 93
    iput-boolean p3, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mCancelButtonActive:Z

    .line 94
    iput-boolean p4, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mWaitOnCancel:Z

    .line 95
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsFragment;Z)V
    .locals 1
    .param p1, "ParentFrgment"    # Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    .param p2, "waitOnCancel"    # Z

    .prologue
    .line 69
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mbCompleteCondition:Z

    .line 56
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mSync:Ljava/lang/Object;

    .line 70
    iput-object p1, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mParentFrgment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mCancelButtonActive:Z

    .line 72
    iput-boolean p2, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mWaitOnCancel:Z

    .line 73
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;Landroid/widget/Button;Landroid/app/Dialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;
    .param p1, "x1"    # Landroid/widget/Button;
    .param p2, "x2"    # Landroid/app/Dialog;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->setUiToCancelling(Landroid/widget/Button;Landroid/app/Dialog;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mSync:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private setUiToCancelling(Landroid/widget/Button;Landroid/app/Dialog;)V
    .locals 2
    .param p1, "negativeButton"    # Landroid/widget/Button;
    .param p2, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 190
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mParentFrgment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 192
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b009b

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 194
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 198
    :cond_0
    return-void
.end method


# virtual methods
.method public getCompletedCondition()Z
    .locals 1

    .prologue
    .line 131
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mbCompleteCondition:Z

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 284
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 286
    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->setCompletedCondition(Z)V

    .line 288
    new-instance v1, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment$4;-><init>(Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;)V

    .line 320
    .local v1, "task":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/Void;Ljava/lang/Void;Ljava/lang/Void;>;"
    :try_start_0
    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v1, v2, v3}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 324
    :goto_0
    return-void

    .line 321
    :catch_0
    move-exception v0

    .line 322
    .local v0, "e":Ljava/util/concurrent/RejectedExecutionException;
    invoke-virtual {v0}, Ljava/util/concurrent/RejectedExecutionException;->printStackTrace()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 142
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 144
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->setRetainInstance(Z)V

    .line 146
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 203
    iget-object v3, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mParentFrgment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mParentFrgment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v3, v3, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    if-eqz v3, :cond_0

    .line 266
    :goto_0
    return-object v2

    .line 208
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 209
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v3, 0x7f0b0065

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 210
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 212
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 214
    .local v1, "factory":Landroid/view/LayoutInflater;
    const v3, 0x7f040039

    invoke-virtual {v1, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mProgressView:Landroid/view/View;

    .line 216
    iget-object v2, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mProgressView:Landroid/view/View;

    const v3, 0x7f0f00d1

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 218
    iget-object v2, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mProgressView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 219
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 220
    new-instance v2, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment$2;-><init>(Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 247
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mCancelButtonActive:Z

    if-eqz v2, :cond_1

    .line 248
    const v2, 0x7f0b0017

    new-instance v3, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment$3;-><init>(Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 266
    :cond_1
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 272
    invoke-super {p0, p1, p2, p3}, Landroid/app/DialogFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 273
    .local v1, "view":Landroid/view/View;
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mCancelButtonActive:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mParentFrgment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mParentFrgment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v2, v2, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    if-eqz v2, :cond_1

    .line 274
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 275
    .local v0, "dialog":Landroid/app/Dialog;
    if-eqz v0, :cond_1

    .line 276
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 279
    .end local v0    # "dialog":Landroid/app/Dialog;
    :cond_1
    return-object v1
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 151
    invoke-super {p0}, Landroid/app/DialogFragment;->onStart()V

    .line 153
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mWaitOnCancel:Z

    if-eqz v2, :cond_0

    .line 155
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    .line 157
    .local v0, "dialog":Landroid/app/AlertDialog;
    if-eqz v0, :cond_0

    .line 159
    const/4 v2, -0x2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    .line 161
    .local v1, "negativeButton":Landroid/widget/Button;
    if-eqz v1, :cond_0

    .line 163
    new-instance v2, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment$1;

    invoke-direct {v2, p0, v1, v0}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment$1;-><init>(Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;Landroid/widget/Button;Landroid/app/AlertDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 186
    .end local v0    # "dialog":Landroid/app/AlertDialog;
    .end local v1    # "negativeButton":Landroid/widget/Button;
    :cond_0
    return-void
.end method

.method public setCancelButtonActive(Z)V
    .locals 0
    .param p1, "cancelButtonActive"    # Z

    .prologue
    .line 111
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mCancelButtonActive:Z

    .line 113
    return-void
.end method

.method public setCancelOperation(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "cancelOperation"    # Ljava/lang/Runnable;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mCancelOperation:Ljava/lang/Runnable;

    .line 107
    return-void
.end method

.method public setCompletedCondition(Z)V
    .locals 2
    .param p1, "bCompleted"    # Z

    .prologue
    .line 122
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mbCompleteCondition:Z

    .line 123
    if-eqz p1, :cond_0

    .line 124
    iget-object v1, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mSync:Ljava/lang/Object;

    monitor-enter v1

    .line 125
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mSync:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 126
    monitor-exit v1

    .line 128
    :cond_0
    return-void

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setParentFragment(Lcom/sec/android/app/myfiles/fragment/AbsFragment;)V
    .locals 0
    .param p1, "fragment"    # Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mParentFrgment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .line 101
    return-void
.end method

.method public setWaitOnCancel(Z)V
    .locals 0
    .param p1, "waitOnCancel"    # Z

    .prologue
    .line 117
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mWaitOnCancel:Z

    .line 119
    return-void
.end method

.method public setmWifiDisconnect(Z)V
    .locals 0
    .param p1, "mWifiDisconnect"    # Z

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mWifiDisconnect:Z

    .line 137
    return-void
.end method
