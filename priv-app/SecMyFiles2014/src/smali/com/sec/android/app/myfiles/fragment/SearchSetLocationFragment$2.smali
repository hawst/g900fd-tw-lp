.class Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$2;
.super Ljava/lang/Object;
.source "SearchSetLocationFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->setupDialog(Landroid/app/AlertDialog$Builder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;)V
    .locals 0

    .prologue
    .line 260
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 266
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->dismissAllowingStateLoss()V

    .line 267
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    iget-boolean v1, v1, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->isfromAdvance:Z

    if-eqz v1, :cond_0

    .line 268
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getTargetRequestCode()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 277
    :goto_0
    return-void

    .line 270
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getTargetRequestCode()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->onClickSearchCallback(IILandroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 272
    :catch_0
    move-exception v0

    .line 275
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
