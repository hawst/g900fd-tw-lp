.class public abstract Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
.super Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;
.source "AbsBrowserAdapter.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/adapter/IContentsFilter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;,
        Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$IdPath;,
        Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;
    }
.end annotation


# static fields
.field private static final MODULE:Ljava/lang/String; = "AbsBrowserAdapter"


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mHasThumbnail:Z

.field protected mLayoutInflater:Landroid/view/LayoutInflater;

.field private mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

.field private mRowLayoutResId:I

.field protected mSelectModeFrom:I

.field protected mSelectedItemMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mSelectionType:I

.field protected mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

.field protected mShowFolderFileType:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "c"    # Landroid/database/Cursor;
    .param p4, "flags"    # I
    .param p5, "navigation"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .prologue
    const/4 v3, 0x0

    .line 95
    invoke-direct {p0, p1, p3, p4}, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 88
    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mHasThumbnail:Z

    .line 99
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader;->sThumbnailLoader:Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailLoaderThread;

    if-nez v0, :cond_0

    .line 101
    const-string v0, "AbsBrowserAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sThumbnailLoader Thread state :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader;->sThumbnailLoader:Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailLoaderThread;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 105
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mContext:Landroid/content/Context;

    .line 107
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    .line 111
    iput-object p5, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .line 113
    iput p2, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mRowLayoutResId:I

    .line 115
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mShowFolderFileType:I

    .line 117
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    .line 143
    return-void
.end method


# virtual methods
.method public addExceptFilterExtension(Ljava/lang/String;)V
    .locals 1
    .param p1, "extension"    # Ljava/lang/String;

    .prologue
    .line 1160
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v0, :cond_0

    .line 1162
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->addExceptFilterExtension(Ljava/lang/String;)V

    .line 1164
    :cond_0
    return-void
.end method

.method public addExceptFilterMimeType(Ljava/lang/String;)V
    .locals 1
    .param p1, "mimetype"    # Ljava/lang/String;

    .prologue
    .line 1150
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v0, :cond_0

    .line 1152
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->addExceptFilterMimeType(Ljava/lang/String;)V

    .line 1154
    :cond_0
    return-void
.end method

.method public addFilterExtension(Ljava/lang/String;)V
    .locals 1
    .param p1, "extension"    # Ljava/lang/String;

    .prologue
    .line 1140
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v0, :cond_0

    .line 1142
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->addFilterExtension(Ljava/lang/String;)V

    .line 1144
    :cond_0
    return-void
.end method

.method public addFilterMimeType(Ljava/lang/String;)V
    .locals 1
    .param p1, "mimetype"    # Ljava/lang/String;

    .prologue
    .line 1130
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v0, :cond_0

    .line 1132
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->addFilterMimeType(Ljava/lang/String;)V

    .line 1134
    :cond_0
    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 10
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 213
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;

    .line 215
    .local v6, "vh":Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;
    if-eqz p3, :cond_0

    invoke-interface {p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 437
    :cond_0
    :goto_0
    return-void

    .line 220
    :cond_1
    const-string v7, "_id"

    invoke-interface {p3, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {p3, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 222
    .local v4, "id":J
    const-string v7, "_data"

    invoke-interface {p3, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {p3, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 224
    .local v1, "data":Ljava/lang/String;
    iget-boolean v7, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->isFirst:Z

    if-eqz v7, :cond_2

    .line 225
    iget-object v0, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    .line 226
    .local v0, "chb":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-virtual {v7, v8}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    const-wide/16 v8, 0x190

    invoke-virtual {v7, v8, v9}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    new-instance v8, Landroid/view/animation/interpolator/SineInOut70;

    invoke-direct {v8}, Landroid/view/animation/interpolator/SineInOut70;-><init>()V

    invoke-virtual {v7, v8}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 235
    .end local v0    # "chb":Landroid/view/View;
    :cond_2
    const v7, 0x7f0f0036

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDocExtnOverlay:Landroid/widget/ImageView;

    .line 237
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDocExtnOverlay:Landroid/widget/ImageView;

    if-eqz v7, :cond_3

    .line 238
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDocExtnOverlay:Landroid/widget/ImageView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 240
    :cond_3
    iget v7, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mViewMode:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_5

    .line 242
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f080017

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 244
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 248
    if-eqz v1, :cond_5

    .line 249
    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 250
    .local v2, "extension":Ljava/lang/String;
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDocExtnOverlay:Landroid/widget/ImageView;

    if-eqz v7, :cond_5

    .line 251
    if-eqz v2, :cond_10

    .line 252
    const-string v7, ".docx"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    const-string v7, ".doc"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    const-string v7, ".docm"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    const-string v7, ".dotm"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    const-string v7, ".dotx"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    const-string v7, ".dot"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 258
    :cond_4
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDocExtnOverlay:Landroid/widget/ImageView;

    const v8, 0x7f0200d1

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 302
    .end local v2    # "extension":Ljava/lang/String;
    :cond_5
    :goto_1
    iget v7, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectionType:I

    if-nez v7, :cond_11

    .line 304
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectRadioButton:Landroid/widget/RadioButton;

    new-instance v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$1;

    invoke-direct {v8, p0, v4, v5, v1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$1;-><init>(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;JLjava/lang/String;)V

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 349
    :goto_2
    iget-boolean v7, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mIsSelectMode:Z

    if-eqz v7, :cond_16

    .line 351
    iget v7, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectionType:I

    packed-switch v7, :pswitch_data_0

    .line 407
    :cond_6
    :goto_3
    iget v7, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mViewMode:I

    if-nez v7, :cond_17

    .line 409
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDetailContainer:Landroid/view/ViewGroup;

    if-nez v7, :cond_7

    .line 411
    const v7, 0x7f0f0067

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    iput-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDetailContainer:Landroid/view/ViewGroup;

    .line 412
    const v7, 0x7f0f005a

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    .line 413
    const v7, 0x7f0f0069

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    .line 416
    :cond_7
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDetailContainer:Landroid/view/ViewGroup;

    if-eqz v7, :cond_8

    .line 417
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDetailContainer:Landroid/view/ViewGroup;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 432
    :cond_8
    :goto_4
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    .line 433
    .local v3, "hover":Landroid/widget/HoverPopupWindow;
    if-eqz v3, :cond_0

    .line 434
    const/4 v7, 0x0

    invoke-virtual {v3, v7}, Landroid/widget/HoverPopupWindow;->setFHAnimationEnabled(Z)V

    goto/16 :goto_0

    .line 260
    .end local v3    # "hover":Landroid/widget/HoverPopupWindow;
    .restart local v2    # "extension":Ljava/lang/String;
    :cond_9
    const-string v7, ".ppt"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_a

    const-string v7, ".pptx"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_a

    const-string v7, ".potm"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_a

    const-string v7, ".potx"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_a

    const-string v7, ".ppam"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_a

    const-string v7, ".ppsm"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_a

    const-string v7, ".ppsx"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_a

    const-string v7, ".pptm"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_a

    const-string v7, ".sldm"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_a

    const-string v7, ".sldx"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_a

    const-string v7, ".pot"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_a

    const-string v7, ".pps"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 272
    :cond_a
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDocExtnOverlay:Landroid/widget/ImageView;

    const v8, 0x7f0200d6

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 274
    :cond_b
    const-string v7, ".pdf"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 275
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDocExtnOverlay:Landroid/widget/ImageView;

    const v8, 0x7f0200d4

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 277
    :cond_c
    const-string v7, ".xls"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_d

    const-string v7, ".xlsx"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_d

    const-string v7, ".xlsm"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_d

    const-string v7, ".xltm"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_d

    const-string v7, ".xltx"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_d

    const-string v7, ".xlm"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_d

    const-string v7, ".xlt"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 284
    :cond_d
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDocExtnOverlay:Landroid/widget/ImageView;

    const v8, 0x7f0200d8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 286
    :cond_e
    const-string v7, ".txt"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 287
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDocExtnOverlay:Landroid/widget/ImageView;

    const v8, 0x7f0200d7

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 290
    :cond_f
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDocExtnOverlay:Landroid/widget/ImageView;

    if-eqz v7, :cond_5

    .line 291
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDocExtnOverlay:Landroid/widget/ImageView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    .line 294
    :cond_10
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDocExtnOverlay:Landroid/widget/ImageView;

    if-eqz v7, :cond_5

    .line 295
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDocExtnOverlay:Landroid/widget/ImageView;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    .line 321
    .end local v2    # "extension":Ljava/lang/String;
    :cond_11
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    new-instance v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$2;

    invoke-direct {v8, p0, v4, v5, v1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$2;-><init>(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;JLjava/lang/String;)V

    invoke-virtual {v7, v8}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 340
    iget v7, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectModeFrom:I

    if-eqz v7, :cond_12

    .line 341
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/CheckBox;->setClickable(Z)V

    goto/16 :goto_2

    .line 343
    :cond_12
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/CheckBox;->setClickable(Z)V

    goto/16 :goto_2

    .line 355
    :pswitch_0
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v7}, Landroid/widget/RadioButton;->getVisibility()I

    move-result v7

    const/16 v8, 0x8

    if-ne v7, v8, :cond_13

    .line 357
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectRadioButton:Landroid/widget/RadioButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 360
    :cond_13
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 362
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectRadioButton:Landroid/widget/RadioButton;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto/16 :goto_3

    .line 369
    :pswitch_1
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v7}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v7

    const/16 v8, 0x8

    if-ne v7, v8, :cond_14

    .line 371
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 374
    :cond_14
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectRadioButton:Landroid/widget/RadioButton;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 375
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_3

    .line 381
    :pswitch_2
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v7}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v7

    if-nez v7, :cond_15

    .line 383
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 386
    :cond_15
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v7}, Landroid/widget/RadioButton;->getVisibility()I

    move-result v7

    if-nez v7, :cond_6

    .line 388
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectRadioButton:Landroid/widget/RadioButton;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setVisibility(I)V

    goto/16 :goto_3

    .line 396
    :cond_16
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectRadioButton:Landroid/widget/RadioButton;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 397
    const/4 v7, 0x0

    invoke-virtual {p1, v7}, Landroid/view/View;->setBackgroundColor(I)V

    .line 401
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 402
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto/16 :goto_3

    .line 419
    :cond_17
    iget v7, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mViewMode:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_8

    .line 421
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDetailContainer:Landroid/view/ViewGroup;

    if-nez v7, :cond_18

    .line 423
    const v7, 0x7f0f0067

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    iput-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDetailContainer:Landroid/view/ViewGroup;

    .line 424
    const v7, 0x7f0f005a

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    .line 425
    const v7, 0x7f0f0069

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    .line 428
    :cond_18
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDetailContainer:Landroid/view/ViewGroup;

    if-eqz v7, :cond_8

    .line 429
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDetailContainer:Landroid/view/ViewGroup;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_4

    .line 351
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public changeCursor(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 1021
    monitor-enter p0

    .line 1023
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mIsSelectMode:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 1025
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1028
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 1030
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->notifyToSelectedItemChangeListener()V

    .line 1031
    monitor-exit p0

    .line 1032
    return-void

    .line 1031
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public clearFilterMimeType()V
    .locals 1

    .prologue
    .line 1180
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v0, :cond_0

    .line 1182
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->clearFilterMimeType()V

    .line 1184
    :cond_0
    return-void
.end method

.method public finishSelectMode()V
    .locals 1

    .prologue
    .line 458
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mIsSelectMode:Z

    .line 460
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 462
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->notifyDataSetChanged()V

    .line 463
    return-void
.end method

.method protected abstract getContentType()I
.end method

.method protected getIconResourceForListView(Ljava/lang/String;Landroid/content/Context;I)I
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "fileType"    # I

    .prologue
    .line 1197
    invoke-static {p1, p2, p3}, Lcom/sec/android/app/myfiles/MediaFile;->getSmallIcon(Ljava/lang/String;Landroid/content/Context;I)I

    move-result v0

    return v0
.end method

.method protected getIconResourceForThumbnailView(Ljava/lang/String;Landroid/content/Context;I)I
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "fileType"    # I

    .prologue
    .line 1193
    invoke-static {p1, p2, p3}, Lcom/sec/android/app/myfiles/MediaFile;->getLargeIcon(Ljava/lang/String;Landroid/content/Context;I)I

    move-result v0

    return v0
.end method

.method public getItemSize(Ljava/lang/String;)J
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 917
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method protected getItemType(Ljava/lang/String;)I
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 911
    const/4 v0, -0x1

    return v0
.end method

.method public getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    .locals 1

    .prologue
    .line 1189
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    return-object v0
.end method

.method public getSelectedItem()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 855
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 857
    .local v4, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    monitor-enter p0

    .line 859
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 861
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Long;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 863
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 865
    .local v0, "id":Ljava/lang/Long;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 867
    .local v3, "path":Ljava/lang/String;
    new-instance v2, Lcom/sec/android/app/myfiles/element/BrowserItem;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getContentType()I

    move-result v5

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-direct {v2, v5, v6, v7, v3}, Lcom/sec/android/app/myfiles/element/BrowserItem;-><init>(IJLjava/lang/String;)V

    .line 869
    .local v2, "mBrowserItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getContentType()I

    move-result v5

    const/16 v6, 0x8

    if-ne v5, v6, :cond_0

    .line 871
    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getItemType(Ljava/lang/String;)I

    move-result v5

    iput v5, v2, Lcom/sec/android/app/myfiles/element/BrowserItem;->mFormat:I

    .line 874
    :cond_0
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 904
    .end local v0    # "id":Ljava/lang/Long;
    .end local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Long;>;"
    .end local v2    # "mBrowserItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    .end local v3    # "path":Ljava/lang/String;
    :catchall_0
    move-exception v5

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .restart local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Long;>;"
    :cond_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 906
    return-object v4
.end method

.method public getSelectedItemCount()I
    .locals 1

    .prologue
    .line 1014
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    return v0
.end method

.method public getSelectedItemPosition()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 814
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 816
    .local v4, "selectedItemPos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v5

    if-nez v5, :cond_0

    .line 848
    :goto_0
    return-object v4

    .line 820
    :cond_0
    const/4 v3, 0x0

    .line 822
    .local v3, "index":I
    monitor-enter p0

    .line 824
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 826
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v5

    if-nez v5, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 832
    :cond_1
    const-string v5, "_id"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 834
    .local v2, "id":Ljava/lang/Long;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 836
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 839
    :cond_2
    add-int/lit8 v3, v3, 0x1

    .line 841
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Landroid/database/StaleDataException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-nez v5, :cond_1

    .line 846
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v2    # "id":Ljava/lang/Long;
    :cond_3
    :goto_1
    :try_start_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v5

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5

    .line 843
    :catch_0
    move-exception v1

    .line 844
    .local v1, "e":Landroid/database/StaleDataException;
    :try_start_2
    invoke-virtual {v1}, Landroid/database/StaleDataException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public isDrmFiltering()Z
    .locals 1

    .prologue
    .line 1495
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v0, :cond_0

    .line 1497
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->isDrmFiltering()Z

    move-result v0

    .line 1501
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEnableFiltering()Z
    .locals 1

    .prologue
    .line 1116
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v0, :cond_0

    .line 1118
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->isEnableFiltering()Z

    move-result v0

    .line 1122
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSelectMode()Z
    .locals 1

    .prologue
    .line 469
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mIsSelectMode:Z

    return v0
.end method

.method protected loadThumbnail(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;Ljava/lang/String;)V
    .locals 1
    .param p1, "vh"    # Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;
    .param p2, "filePath"    # Ljava/lang/String;

    .prologue
    .line 1201
    const/4 v0, -0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->loadThumbnail(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;Ljava/lang/String;I)V

    .line 1202
    return-void
.end method

.method protected loadThumbnail(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;Ljava/lang/String;I)V
    .locals 9
    .param p1, "vh"    # Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "categoryType"    # I

    .prologue
    const/16 v7, 0x8

    const/4 v8, 0x2

    .line 1208
    :try_start_0
    invoke-static {p2}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->getThumbnailDrawableWithoutMakeCache(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 1211
    .local v5, "thumbnail":Landroid/graphics/drawable/Drawable;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-static {p2, v6}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;Landroid/content/Context;)I

    move-result v2

    .line 1213
    .local v2, "fileType":I
    invoke-static {v2}, Lcom/sec/android/app/myfiles/MediaFile;->isDocumentFileType(I)Z

    move-result v6

    if-eqz v6, :cond_0

    iget v6, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mViewMode:I

    if-eq v6, v8, :cond_0

    .line 1215
    const/4 v5, 0x0

    .line 1218
    :cond_0
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getVisibility()I

    move-result v6

    if-ne v6, v7, :cond_1

    .line 1220
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1223
    :cond_1
    if-nez v5, :cond_b

    .line 1227
    iget v6, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mViewMode:I

    if-ne v6, v8, :cond_9

    .line 1229
    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p0, p2, v6, v2}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getIconResourceForThumbnailView(Ljava/lang/String;Landroid/content/Context;I)I

    move-result v0

    .line 1236
    .local v0, "defaultIconResId":I
    :goto_0
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    if-eqz v6, :cond_2

    .line 1238
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1241
    :cond_2
    invoke-static {v2}, Lcom/sec/android/app/myfiles/MediaFile;->isDocumentFileType(I)Z

    move-result v6

    if-eqz v6, :cond_3

    iget v6, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mViewMode:I

    if-ne v6, v8, :cond_7

    .line 1243
    :cond_3
    sget-object v6, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader;->sThumbnailLoader:Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailLoaderThread;

    iget-object v3, v6, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailLoaderThread;->mBackThreadHandler:Landroid/os/Handler;

    .line 1245
    .local v3, "h":Landroid/os/Handler;
    new-instance v4, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;

    invoke-direct {v4}, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;-><init>()V

    .line 1247
    .local v4, "info":Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v6, :cond_4

    .line 1249
    const/4 v6, -0x1

    if-ne p3, v6, :cond_a

    .line 1250
    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCategoryType()I

    move-result v6

    iput v6, v4, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mCategoryType:I

    .line 1256
    :cond_4
    :goto_1
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    iput-object v6, v4, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mTitleTextView:Landroid/widget/TextView;

    .line 1258
    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mContext:Landroid/content/Context;

    iput-object v6, v4, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mThumbnailInfoContext:Landroid/content/Context;

    .line 1260
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    if-eqz v6, :cond_5

    .line 1262
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    iput-object v6, v4, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mIconImageView:Landroid/widget/ImageView;

    .line 1264
    iget-object v6, v4, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mIconImageView:Landroid/widget/ImageView;

    invoke-virtual {v6, p2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 1267
    :cond_5
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    iput-object v6, v4, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mThumbnailImageView:Landroid/widget/ImageView;

    .line 1269
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    if-eqz v6, :cond_6

    .line 1271
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    iput-object v6, v4, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mOverlayIconImageView:Landroid/widget/ImageView;

    .line 1274
    :cond_6
    iput v2, v4, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mFileType:I

    .line 1276
    iput-object p2, v4, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mPath:Ljava/lang/String;

    .line 1278
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getListView()Lcom/sec/android/app/myfiles/view/MyFilesListView;

    move-result-object v6

    iget v6, v6, Lcom/sec/android/app/myfiles/view/MyFilesListView;->mScrollState:I

    iput v6, v4, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mScrollState:I

    .line 1280
    iget v6, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mViewMode:I

    iput v6, v4, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mViewMode:I

    .line 1282
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDocExtnOverlay:Landroid/widget/ImageView;

    iput-object v6, v4, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mDocExtnOverlay:Landroid/widget/ImageView;

    .line 1284
    if-eqz v3, :cond_7

    .line 1285
    const/4 v6, 0x0

    invoke-virtual {v3, v6, v4}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 1289
    .end local v3    # "h":Landroid/os/Handler;
    .end local v4    # "info":Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;
    :cond_7
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mHasThumbnail:Z

    .line 1362
    .end local v0    # "defaultIconResId":I
    .end local v2    # "fileType":I
    .end local v5    # "thumbnail":Landroid/graphics/drawable/Drawable;
    :cond_8
    :goto_2
    return-void

    .line 1233
    .restart local v2    # "fileType":I
    .restart local v5    # "thumbnail":Landroid/graphics/drawable/Drawable;
    :cond_9
    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p0, p2, v6, v2}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getIconResourceForListView(Ljava/lang/String;Landroid/content/Context;I)I

    move-result v0

    .restart local v0    # "defaultIconResId":I
    goto :goto_0

    .line 1252
    .restart local v3    # "h":Landroid/os/Handler;
    .restart local v4    # "info":Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;
    :cond_a
    iput p3, v4, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mCategoryType:I
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1357
    .end local v0    # "defaultIconResId":I
    .end local v2    # "fileType":I
    .end local v3    # "h":Landroid/os/Handler;
    .end local v4    # "info":Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;
    .end local v5    # "thumbnail":Landroid/graphics/drawable/Drawable;
    :catch_0
    move-exception v1

    .line 1359
    .local v1, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_2

    .line 1293
    .end local v1    # "e":Ljava/lang/OutOfMemoryError;
    .restart local v2    # "fileType":I
    .restart local v5    # "thumbnail":Landroid/graphics/drawable/Drawable;
    :cond_b
    :try_start_1
    invoke-static {v2}, Lcom/sec/android/app/myfiles/MediaFile;->isDocumentFileType(I)Z

    move-result v6

    if-eqz v6, :cond_c

    iget v6, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mViewMode:I

    if-ne v6, v8, :cond_8

    .line 1298
    :cond_c
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    if-eqz v6, :cond_d

    .line 1300
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v6, p2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 1303
    :cond_d
    iget v6, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mViewMode:I

    if-ne v6, v8, :cond_13

    .line 1305
    invoke-static {v2}, Lcom/sec/android/app/myfiles/MediaFile;->isInstallFileType(I)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 1307
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    if-eqz v6, :cond_e

    .line 1309
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v6, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1332
    :cond_e
    :goto_3
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDocExtnOverlay:Landroid/widget/ImageView;

    if-eqz v6, :cond_f

    .line 1333
    sget-object v6, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;

    invoke-static {p2}, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 1334
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDocExtnOverlay:Landroid/widget/ImageView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1348
    :cond_f
    :goto_4
    invoke-static {v2}, Lcom/sec/android/app/myfiles/MediaFile;->isVideoFileType(I)Z

    move-result v6

    if-eqz v6, :cond_8

    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    if-eqz v6, :cond_8

    .line 1350
    iget v6, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mViewMode:I

    if-eq v6, v8, :cond_14

    .line 1351
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    const v7, 0x7f0200b8

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 1314
    :cond_10
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v6, :cond_11

    .line 1315
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1316
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v6, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1318
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    if-eqz v6, :cond_11

    .line 1320
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1321
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1326
    :cond_11
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1328
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mHasThumbnail:Z

    goto :goto_3

    .line 1337
    :cond_12
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDocExtnOverlay:Landroid/widget/ImageView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_4

    .line 1342
    :cond_13
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    if-eqz v6, :cond_f

    .line 1344
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v6, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_4

    .line 1353
    :cond_14
    iget-object v6, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    const v7, 0x7f0200d5

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2
.end method

.method public multipleSelect(JLjava/lang/String;)V
    .locals 3
    .param p1, "id"    # J
    .param p3, "data"    # Ljava/lang/String;

    .prologue
    .line 1506
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1508
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1515
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->notifyToSelectedItemChangeListener()V

    .line 1517
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->notifyDataSetChanged()V

    .line 1518
    return-void

    .line 1512
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 149
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    iget v3, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mRowLayoutResId:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 151
    .local v1, "view":Landroid/view/View;
    new-instance v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;-><init>()V

    .line 153
    .local v0, "vh":Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;
    const v2, 0x7f0f0037

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    .line 155
    const v2, 0x7f0f0038

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectRadioButton:Landroid/widget/RadioButton;

    .line 157
    const v2, 0x7f0f0031

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    .line 159
    const v2, 0x7f0f0033

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    .line 161
    const v2, 0x7f0f0035

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    .line 163
    const v2, 0x7f0f0032

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    .line 165
    const v2, 0x7f0f0063

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitleSearchHeader:Landroid/widget/TextView;

    .line 167
    const v2, 0x7f0f006a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDivider:Landroid/widget/ImageView;

    .line 171
    iget v2, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mViewMode:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    .line 173
    const v2, 0x7f0f0067

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDetailContainer:Landroid/view/ViewGroup;

    .line 175
    const v2, 0x7f0f005a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    .line 178
    const v2, 0x7f0f0068

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mHighlight:Landroid/widget/TextView;

    .line 180
    iget-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mHighlight:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 182
    const v2, 0x7f0f0069

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    .line 184
    const v2, 0x7f0f0030

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIconContainer:Landroid/widget/RelativeLayout;

    .line 187
    :cond_0
    const v2, 0x7f0f006b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/view/ListPlayerProgressView;

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mProgressBar:Lcom/sec/android/app/myfiles/view/ListPlayerProgressView;

    .line 189
    const v2, 0x7f0f0039

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mPersonalContentsIcon:Landroid/widget/ImageView;

    .line 191
    const v2, 0x7f0f0066

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDescriptionTextView:Landroid/widget/TextView;

    .line 193
    const v2, 0x7f0f0065

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDescriptionViewGroup:Landroid/view/ViewGroup;

    .line 195
    iget v2, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mViewMode:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 197
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090014

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setMinimumHeight(I)V

    .line 204
    :cond_1
    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 206
    return-object v1

    .line 199
    :cond_2
    iget v2, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mViewMode:I

    if-nez v2, :cond_1

    .line 201
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090013

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setMinimumHeight(I)V

    goto :goto_0
.end method

.method protected notifyToSelectedItemChangeListener()V
    .locals 5

    .prologue
    .line 1044
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemChangeListener:Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;

    if-eqz v2, :cond_0

    .line 1046
    iget v2, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectionType:I

    packed-switch v2, :pswitch_data_0

    .line 1077
    :cond_0
    :goto_0
    return-void

    .line 1052
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemPosition()Ljava/util/ArrayList;

    move-result-object v1

    .line 1054
    .local v1, "selectedPosition":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 1056
    const/4 v0, -0x1

    .line 1063
    .local v0, "position":I
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemChangeListener:Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;

    invoke-interface {v2, v0}, Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;->singleSelectionChanged(I)V

    goto :goto_0

    .line 1060
    .end local v0    # "position":I
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .restart local v0    # "position":I
    goto :goto_1

    .line 1069
    .end local v0    # "position":I
    .end local v1    # "selectedPosition":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1070
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemChangeListener:Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v3

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    invoke-interface {v2, v3, v4}, Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;->multipleSelectionChanged(II)V

    goto :goto_0

    .line 1046
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public penRangeSelect(IIZ)V
    .locals 10
    .param p1, "startPosition"    # I
    .param p2, "endPosition"    # I
    .param p3, "skipFolders"    # Z

    .prologue
    .line 570
    iget-boolean v8, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mIsSelectMode:Z

    if-nez v8, :cond_1

    .line 618
    :cond_0
    return-void

    .line 574
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 576
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v8

    if-nez v8, :cond_0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 581
    move v4, p1

    .local v4, "i":I
    :goto_0
    if-gt v4, p2, :cond_0

    .line 583
    if-eqz p3, :cond_2

    .line 587
    :try_start_0
    const-string v8, "format"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 588
    .local v5, "index":I
    const/4 v8, -0x1

    if-eq v5, v8, :cond_2

    .line 590
    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 591
    .local v3, "format":I
    const/16 v8, 0x3001

    if-ne v3, v8, :cond_2

    .line 592
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 581
    .end local v3    # "format":I
    .end local v5    # "index":I
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 597
    :catch_0
    move-exception v2

    .line 599
    .local v2, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v2}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 603
    .end local v2    # "e":Ljava/lang/IllegalStateException;
    :cond_2
    const-string v8, "_id"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 605
    .local v6, "id":J
    const-string v8, "_data"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 607
    .local v1, "data":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 609
    iget-object v8, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 616
    :goto_2
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_1

    .line 613
    :cond_3
    iget-object v8, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v8, v9, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2
.end method

.method public penRangeSelect(Ljava/util/TreeSet;Z)V
    .locals 11
    .param p2, "skipFolders"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/TreeSet",
            "<",
            "Ljava/lang/Integer;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 509
    .local p1, "selectedPosition":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/lang/Integer;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/TreeSet;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 566
    :cond_0
    :goto_0
    return-void

    .line 513
    :cond_1
    iget-boolean v9, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mIsSelectMode:Z

    if-eqz v9, :cond_0

    .line 517
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 519
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v9

    if-nez v9, :cond_0

    .line 524
    invoke-virtual {p1}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 526
    .local v8, "pos":I
    invoke-interface {v0, v8}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 529
    if-eqz p2, :cond_3

    .line 533
    :try_start_0
    const-string v9, "format"

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 534
    .local v5, "index":I
    const/4 v9, -0x1

    if-eq v5, v9, :cond_3

    .line 536
    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 537
    .local v3, "format":I
    const/16 v9, 0x3001

    if-ne v3, v9, :cond_3

    .line 538
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 543
    .end local v3    # "format":I
    .end local v5    # "index":I
    :catch_0
    move-exception v2

    .line 545
    .local v2, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v2}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 549
    .end local v2    # "e":Ljava/lang/IllegalStateException;
    :cond_3
    const-string v9, "_id"

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 551
    .local v6, "id":J
    const-string v9, "_data"

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 553
    .local v1, "data":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 555
    iget-object v9, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 559
    :cond_4
    iget-object v9, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v9, v10, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 563
    .end local v1    # "data":Ljava/lang/String;
    .end local v6    # "id":J
    .end local v8    # "pos":I
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->notifyToSelectedItemChangeListener()V

    .line 565
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0
.end method

.method public penSelectItem(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 622
    iget-boolean v4, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mIsSelectMode:Z

    if-eqz v4, :cond_0

    .line 624
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 626
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 651
    .end local v0    # "cursor":Landroid/database/Cursor;
    :cond_0
    :goto_0
    return-void

    .line 631
    .restart local v0    # "cursor":Landroid/database/Cursor;
    :cond_1
    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 633
    const-string v4, "_id"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 635
    .local v2, "id":J
    const-string v4, "_data"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 637
    .local v1, "data":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 639
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 646
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->notifyToSelectedItemChangeListener()V

    .line 648
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 643
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public refreshSelection()V
    .locals 5

    .prologue
    .line 955
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    if-gtz v4, :cond_1

    .line 971
    :cond_0
    :goto_0
    return-void

    .line 957
    :cond_1
    monitor-enter p0

    .line 958
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 959
    .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Long;>;"
    const/4 v0, 0x0

    .line 960
    .local v0, "file":Ljava/io/File;
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 961
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 962
    .local v1, "id":Ljava/lang/Long;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 963
    .local v3, "path":Ljava/lang/String;
    if-eqz v3, :cond_2

    .line 964
    new-instance v0, Ljava/io/File;

    .end local v0    # "file":Ljava/io/File;
    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 965
    .restart local v0    # "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_2

    .line 966
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 970
    .end local v0    # "file":Ljava/io/File;
    .end local v1    # "id":Ljava/lang/Long;
    .end local v2    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Long;>;"
    .end local v3    # "path":Ljava/lang/String;
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .restart local v0    # "file":Ljava/io/File;
    .restart local v2    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Long;>;"
    :cond_3
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public removeFilterMimeType(Ljava/lang/String;)V
    .locals 1
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 1170
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v0, :cond_0

    .line 1172
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->removeFilterMimeType(Ljava/lang/String;)V

    .line 1174
    :cond_0
    return-void
.end method

.method public selectAllFileItem()V
    .locals 7

    .prologue
    .line 745
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mIsSelectMode:Z

    if-eqz v3, :cond_0

    .line 747
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 749
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 785
    .end local v0    # "cursor":Landroid/database/Cursor;
    :cond_0
    :goto_0
    return-void

    .line 754
    .restart local v0    # "cursor":Landroid/database/Cursor;
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 762
    :cond_2
    const-string v3, "_id"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 764
    .local v4, "id":J
    const-string v3, "_data"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 766
    .local v1, "data":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 767
    .local v2, "file":Ljava/io/File;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    instance-of v3, v3, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;

    if-eqz v3, :cond_4

    .line 768
    const-string v3, "format"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/4 v6, 0x1

    if-ne v3, v6, :cond_3

    .line 769
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v3, v6, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 777
    :cond_3
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 780
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->notifyToSelectedItemChangeListener()V

    .line 782
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 772
    :cond_4
    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 773
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v3, v6, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public selectAllItem()V
    .locals 6

    .prologue
    .line 711
    iget-boolean v4, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mIsSelectMode:Z

    if-eqz v4, :cond_0

    .line 713
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 715
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 742
    .end local v0    # "cursor":Landroid/database/Cursor;
    :cond_0
    :goto_0
    return-void

    .line 720
    .restart local v0    # "cursor":Landroid/database/Cursor;
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 728
    :cond_2
    const-string v4, "_id"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 730
    .local v2, "id":J
    const-string v4, "_data"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 732
    .local v1, "data":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 734
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_2

    .line 737
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->notifyToSelectedItemChangeListener()V

    .line 739
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public selectItem(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 484
    iget-boolean v4, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mIsSelectMode:Z

    if-eqz v4, :cond_0

    .line 486
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 488
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 506
    .end local v0    # "cursor":Landroid/database/Cursor;
    :cond_0
    :goto_0
    return-void

    .line 493
    .restart local v0    # "cursor":Landroid/database/Cursor;
    :cond_1
    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 495
    const-string v4, "_id"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 497
    .local v2, "id":J
    const-string v4, "_data"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 499
    .local v1, "data":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 501
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->notifyToSelectedItemChangeListener()V

    .line 503
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public setDrmFiltering(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 1488
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v0, :cond_0

    .line 1490
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->setDrmFiltering(Z)V

    .line 1492
    :cond_0
    return-void
.end method

.method public setEnableFiltering(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 1106
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v0, :cond_0

    .line 1108
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->setEnableFiltering(Z)V

    .line 1110
    :cond_0
    return-void
.end method

.method protected setHoverLeft(IIZ)I
    .locals 2
    .param p1, "rectLeft"    # I
    .param p2, "eventX"    # I
    .param p3, "isThumbMode"    # Z

    .prologue
    .line 1372
    const/4 v0, 0x0

    .line 1374
    .local v0, "x":I
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1376
    if-eqz p3, :cond_0

    .line 1377
    add-int v0, p1, p2

    .line 1386
    :goto_0
    return v0

    .line 1379
    :cond_0
    div-int/lit8 v1, p2, 0x2

    add-int v0, p1, v1

    goto :goto_0

    .line 1383
    :cond_1
    div-int/lit8 v1, p2, 0x2

    add-int v0, p1, v1

    goto :goto_0
.end method

.method protected setHoverTop(II)I
    .locals 2
    .param p1, "rectTop"    # I
    .param p2, "eventY"    # I

    .prologue
    .line 1397
    const/4 v0, 0x0

    .line 1399
    .local v0, "y":I
    div-int/lit8 v1, p2, 0x2

    add-int v0, p1, v1

    .line 1401
    return v0
.end method

.method public setSelectedItemChangeListener(Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;

    .prologue
    .line 1038
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemChangeListener:Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;

    .line 1039
    return-void
.end method

.method public setShowFolderFileType(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    const/4 v1, 0x2

    .line 1083
    if-ltz p1, :cond_0

    if-le p1, v1, :cond_2

    .line 1085
    :cond_0
    iput v1, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mShowFolderFileType:I

    .line 1092
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v1, :cond_1

    .line 1094
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->setShowFolderFileType(I)V

    .line 1096
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v0

    .line 1098
    .local v0, "cursor":Landroid/database/Cursor;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 1100
    .end local v0    # "cursor":Landroid/database/Cursor;
    :cond_1
    return-void

    .line 1089
    :cond_2
    iput p1, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mShowFolderFileType:I

    goto :goto_0
.end method

.method public setSortBy(II)V
    .locals 1
    .param p1, "sortBy"    # I
    .param p2, "inOrder"    # I

    .prologue
    .line 802
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->setSortBy(II)V

    .line 804
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v0, :cond_0

    .line 806
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->setSortBy(II)V

    .line 808
    :cond_0
    return-void
.end method

.method public setViewMode(II)V
    .locals 0
    .param p1, "mode"    # I
    .param p2, "layout"    # I

    .prologue
    .line 475
    iput p1, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mViewMode:I

    .line 477
    iput p2, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mRowLayoutResId:I

    .line 478
    return-void
.end method

.method public startSelectMode(II)V
    .locals 1
    .param p1, "selectionType"    # I
    .param p2, "from"    # I

    .prologue
    .line 443
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mIsSelectMode:Z

    .line 445
    iput p1, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectionType:I

    .line 447
    iput p2, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectModeFrom:I

    .line 449
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->notifyToSelectedItemChangeListener()V

    .line 451
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->notifyDataSetChanged()V

    .line 452
    return-void
.end method

.method public unselectAllItem()V
    .locals 1

    .prologue
    .line 789
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mIsSelectMode:Z

    if-eqz v0, :cond_0

    .line 791
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 793
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->notifyToSelectedItemChangeListener()V

    .line 795
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->notifyDataSetChanged()V

    .line 797
    :cond_0
    return-void
.end method

.method public validateSelectedItems()Z
    .locals 6

    .prologue
    .line 922
    const/4 v4, 0x1

    .line 924
    .local v4, "result":Z
    monitor-enter p0

    .line 926
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 934
    .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Long;>;"
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 936
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 938
    .local v1, "id":Ljava/lang/Long;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-virtual {v5, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 940
    .local v3, "path":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 942
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    .line 944
    const/4 v4, 0x0

    .line 949
    .end local v0    # "f":Ljava/io/File;
    .end local v1    # "id":Ljava/lang/Long;
    .end local v3    # "path":Ljava/lang/String;
    :cond_1
    monitor-exit p0

    .line 951
    return v4

    .line 949
    .end local v2    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Long;>;"
    :catchall_0
    move-exception v5

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method
