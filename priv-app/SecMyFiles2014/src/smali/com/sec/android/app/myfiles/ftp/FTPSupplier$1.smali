.class Lcom/sec/android/app/myfiles/ftp/FTPSupplier$1;
.super Ljava/lang/Object;
.source "FTPSupplier.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/ftp/FTPAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->createDirectory(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/ftp/FTPSupplier;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier$1;->this$0:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getWorkElement()Ljava/lang/String;
    .locals 1

    .prologue
    .line 183
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDone(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "result"    # Landroid/os/Bundle;

    .prologue
    .line 177
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier$1;->this$0:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    invoke-interface {v0}, Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;->onFolderCreated()V

    .line 178
    return-void
.end method

.method public work(Landroid/os/Bundle;)Z
    .locals 2
    .param p1, "arg"    # Landroid/os/Bundle;

    .prologue
    .line 164
    if-eqz p1, :cond_0

    .line 165
    const-string v1, "create_dir"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 166
    .local v0, "target":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 168
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier$1;->this$0:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    # getter for: Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->access$000(Lcom/sec/android/app/myfiles/ftp/FTPSupplier;)Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncCreateFolder(Ljava/lang/String;)Z

    move-result v1

    .line 172
    .end local v0    # "target":Ljava/lang/String;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
