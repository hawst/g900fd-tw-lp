.class Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$MediaBulkDeleter;
.super Ljava/lang/Object;
.source "AbsFileOperationTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MediaBulkDeleter"
.end annotation


# instance fields
.field deletedNumber:I

.field mBaseUri:Landroid/net/Uri;

.field path:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

.field whereArgs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field whereClause:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;)V
    .locals 2

    .prologue
    .line 1111
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$MediaBulkDeleter;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$MediaBulkDeleter;->whereClause:Ljava/lang/StringBuilder;

    .line 1105
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$MediaBulkDeleter;->whereArgs:Ljava/util/ArrayList;

    .line 1114
    const-string v0, "external"

    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$MediaBulkDeleter;->mBaseUri:Landroid/net/Uri;

    .line 1115
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$MediaBulkDeleter;->deletedNumber:I

    .line 1117
    return-void
.end method


# virtual methods
.method public delete(J)V
    .locals 3
    .param p1, "id"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1123
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$MediaBulkDeleter;->whereClause:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1124
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$MediaBulkDeleter;->whereClause:Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1126
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$MediaBulkDeleter;->whereClause:Ljava/lang/StringBuilder;

    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1127
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$MediaBulkDeleter;->whereArgs:Ljava/util/ArrayList;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1128
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$MediaBulkDeleter;->whereArgs:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v1, 0x64

    if-le v0, v1, :cond_1

    .line 1129
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$MediaBulkDeleter;->flush()V

    .line 1131
    :cond_1
    return-void
.end method

.method public flush()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1134
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$MediaBulkDeleter;->whereArgs:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1135
    .local v2, "size":I
    if-lez v2, :cond_0

    .line 1136
    new-array v0, v2, [Ljava/lang/String;

    .line 1137
    .local v0, "foo":[Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$MediaBulkDeleter;->whereArgs:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "foo":[Ljava/lang/String;
    check-cast v0, [Ljava/lang/String;

    .line 1138
    .restart local v0    # "foo":[Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$MediaBulkDeleter;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$MediaBulkDeleter;->mBaseUri:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_id IN ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$MediaBulkDeleter;->whereClause:Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 1140
    .local v1, "numrows":I
    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$MediaBulkDeleter;->deletedNumber:I

    add-int/2addr v3, v1

    iput v3, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$MediaBulkDeleter;->deletedNumber:I

    .line 1142
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$MediaBulkDeleter;->whereClause:Ljava/lang/StringBuilder;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1143
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$MediaBulkDeleter;->whereArgs:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 1145
    .end local v0    # "foo":[Ljava/lang/String;
    .end local v1    # "numrows":I
    :cond_0
    return-void
.end method

.method public getTotalDeletedNumber()I
    .locals 1

    .prologue
    .line 1119
    iget v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$MediaBulkDeleter;->deletedNumber:I

    return v0
.end method
