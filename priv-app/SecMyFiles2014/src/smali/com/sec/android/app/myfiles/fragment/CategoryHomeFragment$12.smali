.class Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;
.super Ljava/lang/Object;
.source "CategoryHomeFragment.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V
    .locals 0

    .prologue
    .line 1656
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 11
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v10, 0x0

    .line 1661
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 1736
    :cond_0
    :goto_0
    return v10

    .line 1665
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1666
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->notifyDataSetChanged()V

    .line 1668
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1669
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->onRefresh()V

    .line 1671
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mRemoteShareListAdapter:Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$600(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1672
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mRemoteShareListAdapter:Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$600(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->onRefresh()V

    .line 1674
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->RemoteShareListRefresh()V

    .line 1675
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v1, :cond_4

    .line 1676
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iput-boolean v10, v1, Lcom/sec/android/app/myfiles/AbsMainActivity;->mIsWorkingUpdateCategorySize:Z

    .line 1677
    :cond_4
    const/4 v1, 0x2

    const-string v6, "CategoryHomeFragment"

    const-string v7, "updateCategorySize done!!"

    invoke-static {v1, v6, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1682
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    move-result-object v1

    const/4 v6, 0x6

    invoke-virtual {v1, v6}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->getItemByType(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/element/CategoryItem;

    .line 1684
    .local v0, "downloadedAppItem":Lcom/sec/android/app/myfiles/element/CategoryItem;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    # += operator for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDownloadedAppTotalSize:J
    invoke-static {v6, v8, v9}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$1114(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;J)J

    .line 1686
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # operator++ for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSizeLoadedAppCount:I
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$1208(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)I

    .line 1688
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSizeLoadedAppCount:I
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$1200(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)I

    move-result v1

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDownloadedAppCount:I
    invoke-static {v6}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$1300(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)I

    move-result v6

    if-ne v1, v6, :cond_0

    .line 1690
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDownloadedAppTotalSize()J

    move-result-wide v2

    .line 1692
    .local v2, "savedTotalSize":J
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # setter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSizeLoadedAppCount:I
    invoke-static {v1, v10}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$1202(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;I)I

    .line 1694
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # setter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDownloadedAppCount:I
    invoke-static {v1, v10}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$1302(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;I)I

    .line 1696
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDownloadedAppTotalSize:J
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$1100(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)J

    move-result-wide v6

    cmp-long v1, v6, v2

    if-eqz v1, :cond_5

    .line 1698
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDownloadedAppTotalSize:J
    invoke-static {v6}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$1100(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setDownloadedAppTotalSize(J)V

    .line 1700
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDownloadedAppTotalSize:J
    invoke-static {v6}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$1100(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSavedDownloadedAppTotalSize(J)V

    .line 1702
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->commitDownAppSize()Z

    .line 1705
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDownloadedAppTotalSize:J
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$1100(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Lcom/sec/android/app/myfiles/element/CategoryItem;->setTotalSize(J)V

    .line 1707
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->notifyDataSetChanged()V

    .line 1710
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v1, :cond_0

    .line 1712
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iput-boolean v10, v1, Lcom/sec/android/app/myfiles/AbsMainActivity;->mIsWorkingUpdateCategorySize:Z

    goto/16 :goto_0

    .line 1717
    .end local v0    # "downloadedAppItem":Lcom/sec/android/app/myfiles/element/CategoryItem;
    .end local v2    # "savedTotalSize":J
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0

    .line 1721
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1, v10}, Lcom/sec/android/app/myfiles/utils/Utils;->getDocumentFilesSize(Landroid/content/Context;I)J

    move-result-wide v4

    .line 1723
    .local v4, "totalDocumentSize":J
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getSavedDocumentsTotalSize()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-eqz v1, :cond_0

    .line 1725
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v1, v4, v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setDocumentsTotalSize(J)V

    .line 1726
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->commitDocumentsSize()Z

    .line 1727
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    move-result-object v1

    const/4 v6, 0x5

    invoke-virtual {v1, v6}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->getItemByType(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/CategoryItem;

    invoke-virtual {v1, v4, v5}, Lcom/sec/android/app/myfiles/element/CategoryItem;->setTotalSize(J)V

    .line 1728
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0

    .line 1661
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
