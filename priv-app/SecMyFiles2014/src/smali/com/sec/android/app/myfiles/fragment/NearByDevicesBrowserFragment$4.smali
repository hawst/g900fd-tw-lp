.class Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$4;
.super Ljava/lang/Object;
.source "NearByDevicesBrowserFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->onCreateItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;)V
    .locals 0

    .prologue
    .line 740
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 6
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .local p1, "parentView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 745
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mDevicesListShowing:Z
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 773
    :cond_0
    :goto_0
    return v3

    .line 749
    :cond_1
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v5

    invoke-interface {v5, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 751
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 753
    const/4 v2, -0x1

    .line 757
    .local v2, "mediaType":I
    :try_start_0
    const-string v5, "MediaType"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 764
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->isSelectMode()Z

    move-result v5

    if-nez v5, :cond_0

    if-eq v2, v4, :cond_0

    .line 766
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    invoke-virtual {v5, v4, p3, v3}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->startSelectMode(III)V

    move v3, v4

    .line 769
    goto :goto_0

    .line 759
    :catch_0
    move-exception v1

    .line 761
    .local v1, "e":Landroid/database/CursorIndexOutOfBoundsException;
    invoke-virtual {v1}, Landroid/database/CursorIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_1
.end method
