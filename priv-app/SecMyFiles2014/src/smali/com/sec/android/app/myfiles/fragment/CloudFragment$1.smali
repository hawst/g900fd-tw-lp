.class Lcom/sec/android/app/myfiles/fragment/CloudFragment$1;
.super Ljava/lang/Object;
.source "CloudFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/CloudFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/CloudFragment;)V
    .locals 0

    .prologue
    .line 233
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 237
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->getCurrentItemIndex()I

    move-result v0

    .line 239
    .local v0, "index":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->getFirstVisiblePosition()I

    move-result v1

    if-lt v0, v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->getLastVisiblePosition()I

    move-result v1

    if-le v0, v1, :cond_2

    .line 241
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->getCurrentItemIndex()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->setTreeViewYPos(I)V

    .line 243
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->restoreTreeYPosition()V

    .line 245
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v1, :cond_2

    .line 246
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->notifyUpdate(Z)V

    .line 248
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxPathIndicator:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CloudFragment;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    .line 249
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxRoot:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/CloudFragment;)Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxPathIndicator:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->access$002(Lcom/sec/android/app/myfiles/fragment/CloudFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 253
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxPathIndicator:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CloudFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 256
    :cond_2
    return-void

    .line 251
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxRoot:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/CloudFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxPathIndicator:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->access$002(Lcom/sec/android/app/myfiles/fragment/CloudFragment;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0
.end method
