.class Lcom/sec/android/app/myfiles/fragment/RenameFragment$1;
.super Ljava/lang/Object;
.source "RenameFragment.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/RenameFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/RenameFragment;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x7

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 71
    iget v1, p1, Landroid/os/Message;->what:I

    if-nez v1, :cond_1

    .line 73
    const-string v1, "RenameFragment"

    const-string v2, "mHandler : MESSAGE_RENAME_SUCCESS"

    invoke-static {v3, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 74
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->dismiss()V

    .line 75
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 76
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "FILE_OPERATION"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 77
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getTargetRequestCode()I

    move-result v2

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 87
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return v4

    .line 79
    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    if-ne v1, v4, :cond_0

    .line 81
    const-string v1, "RenameFragment"

    const-string v2, "mHandler : MESSAGE_RENAME_ERROR"

    invoke-static {v3, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 82
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->dismiss()V

    .line 83
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 84
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "FILE_OPERATION"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 85
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->getTargetRequestCode()I

    move-result v2

    invoke-virtual {v1, v2, v4, v0}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method
