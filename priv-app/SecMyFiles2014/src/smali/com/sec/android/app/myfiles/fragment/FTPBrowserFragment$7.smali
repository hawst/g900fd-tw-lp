.class Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$7;
.super Ljava/lang/Object;
.source "FTPBrowserFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)V
    .locals 0

    .prologue
    .line 1561
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 16
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1566
    .local p1, "parentView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->isSelectMode()Z

    move-result v11

    if-eqz v11, :cond_1

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSelector:Z
    invoke-static {v11}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->access$500(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)Z

    move-result v11

    if-nez v11, :cond_1

    .line 1568
    invoke-virtual/range {p1 .. p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v11

    move/from16 v0, p3

    invoke-interface {v11, v0}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/database/Cursor;

    .line 1569
    .local v3, "cursor":Landroid/database/Cursor;
    const-string v11, "_id"

    invoke-interface {v3, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {v3, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 1570
    .local v4, "cursorId":J
    const-string v11, "_data"

    invoke-interface {v3, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {v3, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 1571
    .local v9, "path":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v11, v11, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v11, v4, v5, v9}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->multipleSelect(JLjava/lang/String;)V

    .line 1628
    .end local v3    # "cursor":Landroid/database/Cursor;
    .end local v4    # "cursorId":J
    .end local v9    # "path":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 1573
    :cond_1
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isClickValid()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1575
    invoke-virtual/range {p1 .. p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v11

    move/from16 v0, p3

    invoke-interface {v11, v0}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/database/Cursor;

    .line 1577
    .restart local v3    # "cursor":Landroid/database/Cursor;
    const-string v11, "_data"

    invoke-interface {v3, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {v3, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 1579
    .restart local v9    # "path":Ljava/lang/String;
    const-string v11, "is_directory"

    invoke-interface {v3, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {v3, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 1581
    .local v7, "isDir":I
    const-string v11, "file_size"

    invoke-interface {v3, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {v3, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 1583
    .local v12, "size":J
    const-string v11, "title"

    invoke-interface {v3, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    invoke-interface {v3, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1585
    .local v8, "name":Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getParentFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1587
    .local v6, "directory":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v11, v11, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v11}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1589
    const/4 v11, 0x1

    if-eq v7, v11, :cond_2

    const/4 v11, 0x2

    if-ne v7, v11, :cond_4

    .line 1592
    :cond_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->isSelectMode()Z

    move-result v11

    if-eqz v11, :cond_3

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v11, v11, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v11, :cond_3

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v11, v11, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v11

    if-lez v11, :cond_3

    .line 1594
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v11, v11, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->unselectAllItem()V

    .line 1596
    :cond_3
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v11, v11, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v10

    .line 1597
    .local v10, "previousPath":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    const/4 v14, 0x0

    # setter for: Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSwitchOnBack:Z
    invoke-static {v11, v14}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->access$602(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;Z)Z

    .line 1598
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v11, v11, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    const/4 v14, 0x0

    invoke-static {v14}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v14

    invoke-virtual {v11, v9, v14}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z

    .line 1599
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrefix:Ljava/lang/String;
    invoke-static {v15}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    # setter for: Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPreviousPath:Ljava/lang/String;
    invoke-static {v11, v14}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->access$002(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 1600
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v11, v11, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrefix:Ljava/lang/String;
    invoke-static {v15}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v14}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1603
    .end local v10    # "previousPath":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v11

    if-eqz v11, :cond_5

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v11

    const-string v14, "run_from"

    invoke-virtual {v11, v14}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v11

    const/16 v14, 0x15

    if-ne v11, v14, :cond_5

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getSelectionType()I

    move-result v11

    const/4 v14, 0x1

    if-ne v11, v14, :cond_5

    .line 1605
    const v11, 0x7f0f0037

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1607
    .local v2, "checkBoxView":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 1609
    const/4 v11, 0x0

    invoke-virtual {v2, v11}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    .line 1611
    invoke-virtual {v2}, Landroid/view/View;->performClick()Z

    goto/16 :goto_0

    .line 1618
    .end local v2    # "checkBoxView":Landroid/view/View;
    :cond_5
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v14

    if-eqz v14, :cond_6

    const-string v6, "/"

    .end local v6    # "directory":Ljava/lang/String;
    :cond_6
    invoke-static {v8, v7, v6, v12, v13}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getItem(Ljava/lang/String;ILjava/lang/String;J)Lcom/sec/android/app/myfiles/ftp/FTPItem;

    move-result-object v14

    # invokes: Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->downloadAndOpen(Lcom/sec/android/app/myfiles/ftp/FTPItem;)V
    invoke-static {v11, v14}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->access$700(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;Lcom/sec/android/app/myfiles/ftp/FTPItem;)V

    goto/16 :goto_0

    .line 1624
    .restart local v6    # "directory":Ljava/lang/String;
    :cond_7
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v11, v11, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v14, v14, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v15, 0x7f0b00c7

    invoke-virtual {v14, v15}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x1

    invoke-static {v11, v14, v15}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0
.end method
