.class public Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;
.source "SearchSetDateFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static _instance:Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;


# instance fields
.field cal:Ljava/util/Calendar;

.field isfromAdvance:Z

.field mDateImage:Landroid/widget/LinearLayout;

.field private mFromDate:J

.field private mIntent:Landroid/content/Intent;

.field private mSearchFromDateButton:Landroid/widget/TextView;

.field private mSearchToDateButton:Landroid/widget/TextView;

.field private mToDate:J

.field sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->_instance:Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->clickOK()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;

    .prologue
    .line 45
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mFromDate:J

    return-wide v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;
    .param p1, "x1"    # J

    .prologue
    .line 45
    iput-wide p1, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mFromDate:J

    return-wide p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mSearchFromDateButton:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;

    .prologue
    .line 45
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mToDate:J

    return-wide v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;
    .param p1, "x1"    # J

    .prologue
    .line 45
    iput-wide p1, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mToDate:J

    return-wide p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mSearchToDateButton:Landroid/widget/TextView;

    return-object v0
.end method

.method private clickOK()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x3e8

    const/4 v4, -0x1

    .line 175
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mToDate:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 177
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mToDate:J

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    iget-wide v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mFromDate:J

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFromDate(J)V

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    iget-wide v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mToDate:J

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchToDate(J)V

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mIntent:Landroid/content/Intent;

    const-string v1, "search_from_date_intent_key"

    iget-wide v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mFromDate:J

    div-long/2addr v2, v6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mIntent:Landroid/content/Intent;

    const-string v1, "search_to_date_intent_key"

    iget-wide v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mToDate:J

    div-long/2addr v2, v6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 185
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->isfromAdvance:Z

    if-eqz v0, :cond_1

    .line 186
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->getTargetRequestCode()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1, v4, v2}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 189
    :goto_0
    return-void

    .line 188
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->getTargetRequestCode()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mIntent:Landroid/content/Intent;

    invoke-virtual {p0, v0, v4, v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->onClickSearchCallback(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public static getInstance()Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->_instance:Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;

    if-nez v0, :cond_0

    .line 64
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->_instance:Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;

    .line 67
    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->_instance:Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;

    return-object v0
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 273
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 274
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->getTargetRequestCode()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 275
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 11
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    .line 198
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->isClickable()Z

    move-result v2

    if-ne v2, v3, :cond_0

    .line 199
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 270
    :cond_0
    :goto_0
    return-void

    .line 202
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->cal:Ljava/util/Calendar;

    iget-wide v4, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mFromDate:J

    invoke-virtual {v2, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 205
    :try_start_0
    new-instance v0, Landroid/app/DatePickerDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment$3;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;)V

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->cal:Ljava/util/Calendar;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->cal:Ljava/util/Calendar;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->cal:Ljava/util/Calendar;

    const/4 v6, 0x5

    invoke-virtual {v5, v6}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    .line 216
    .local v0, "id":Landroid/app/DatePickerDialog;
    invoke-virtual {v0}, Landroid/app/DatePickerDialog;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v7

    .line 217
    .local v7, "dp":Landroid/widget/DatePicker;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->cal:Ljava/util/Calendar;

    const/16 v3, 0x7b2

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5}, Ljava/util/Calendar;->set(III)V

    .line 218
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->cal:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v7, v2, v3}, Landroid/widget/DatePicker;->setMinDate(J)V

    .line 219
    iget-wide v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mToDate:J

    invoke-virtual {v7, v2, v3}, Landroid/widget/DatePicker;->setMaxDate(J)V

    .line 220
    const v2, 0x7f0b0092

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/DatePickerDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 221
    new-instance v2, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment$4;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;)V

    invoke-virtual {v0, v2}, Landroid/app/DatePickerDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 227
    invoke-virtual {v0}, Landroid/app/DatePickerDialog;->show()V

    .line 228
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mSearchFromDateButton:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setClickable(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 230
    .end local v0    # "id":Landroid/app/DatePickerDialog;
    .end local v7    # "dp":Landroid/widget/DatePicker;
    :catch_0
    move-exception v9

    .line 232
    .local v9, "e":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 236
    .end local v9    # "e":Ljava/lang/Exception;
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->cal:Ljava/util/Calendar;

    iget-wide v4, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mToDate:J

    invoke-virtual {v2, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 239
    :try_start_1
    new-instance v1, Landroid/app/DatePickerDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment$5;

    invoke-direct {v3, p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment$5;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;)V

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->cal:Ljava/util/Calendar;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->cal:Ljava/util/Calendar;

    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Ljava/util/Calendar;->get(I)I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->cal:Ljava/util/Calendar;

    const/4 v10, 0x5

    invoke-virtual {v6, v10}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-direct/range {v1 .. v6}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    .line 248
    .local v1, "id1":Landroid/app/DatePickerDialog;
    new-instance v2, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment$6;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment$6;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;)V

    invoke-virtual {v1, v2}, Landroid/app/DatePickerDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 254
    invoke-virtual {v1}, Landroid/app/DatePickerDialog;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v8

    .line 255
    .local v8, "dp1":Landroid/widget/DatePicker;
    iget-wide v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mFromDate:J

    invoke-virtual {v8, v2, v3}, Landroid/widget/DatePicker;->setMinDate(J)V

    .line 256
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v8, v2, v3}, Landroid/widget/DatePicker;->setMaxDate(J)V

    .line 257
    const v2, 0x7f0b0093

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/DatePickerDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 258
    invoke-virtual {v1}, Landroid/app/DatePickerDialog;->show()V

    .line 259
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mSearchToDateButton:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setClickable(Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 260
    .end local v1    # "id1":Landroid/app/DatePickerDialog;
    .end local v8    # "dp1":Landroid/widget/DatePicker;
    :catch_1
    move-exception v9

    .line 263
    .restart local v9    # "e":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 199
    :pswitch_data_0
    .packed-switch 0x7f0f00fd
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const-wide/16 v10, 0x0

    const/4 v8, 0x0

    .line 71
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v5}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    .line 72
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->cal:Ljava/util/Calendar;

    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    .line 75
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getSearchFromDate()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mFromDate:J

    .line 76
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getSearchToDate()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mToDate:J

    .line 78
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "title"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 79
    .local v3, "titleResId":I
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const v6, 0x7f0f00e9

    invoke-virtual {v5, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mDateImage:Landroid/widget/LinearLayout;

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "search_advance_intent_key"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->isfromAdvance:Z

    .line 82
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v0, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 84
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    new-instance v2, Landroid/widget/ScrollView;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v2, v5}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 86
    .local v2, "scrollview":Landroid/widget/ScrollView;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 88
    .local v1, "customTitleView":Landroid/view/LayoutInflater;
    const v5, 0x7f040042

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 90
    .local v4, "v":Landroid/view/View;
    invoke-virtual {v2, v4}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    .line 91
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mIntent:Landroid/content/Intent;

    .line 93
    iget-wide v6, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mFromDate:J

    cmp-long v5, v6, v10

    if-nez v5, :cond_0

    iget-wide v6, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mToDate:J

    cmp-long v5, v6, v10

    if-nez v5, :cond_0

    .line 95
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->cal:Ljava/util/Calendar;

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mToDate:J

    .line 97
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->cal:Ljava/util/Calendar;

    const/4 v6, 0x6

    const/4 v7, -0x7

    invoke-virtual {v5, v6, v7}, Ljava/util/Calendar;->add(II)V

    .line 98
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->cal:Ljava/util/Calendar;

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mFromDate:J

    .line 108
    :goto_0
    const v5, 0x7f0f00fd

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mSearchFromDateButton:Landroid/widget/TextView;

    .line 110
    const v5, 0x7f0f00fe

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mSearchToDateButton:Landroid/widget/TextView;

    .line 116
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mSearchFromDateButton:Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setWritingBuddyEnabled(Z)V

    .line 118
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mSearchToDateButton:Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setWritingBuddyEnabled(Z)V

    .line 120
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mSearchFromDateButton:Landroid/widget/TextView;

    invoke-virtual {v5, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mSearchToDateButton:Landroid/widget/TextView;

    invoke-virtual {v5, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mSearchToDateButton:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    iget-wide v8, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mToDate:J

    invoke-static {v6, v8, v9}, Lcom/sec/android/app/myfiles/utils/Utils;->getSmallFormatedDateFromLong(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mSearchFromDateButton:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    iget-wide v8, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mFromDate:J

    invoke-static {v6, v8, v9}, Lcom/sec/android/app/myfiles/utils/Utils;->getSmallFormatedDateFromLong(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 130
    const v5, 0x7f0b00ea

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 132
    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->setupDialog(Landroid/app/AlertDialog$Builder;)V

    .line 134
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    return-object v5

    .line 104
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getSearchToDate()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mToDate:J

    .line 105
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getSearchFromDate()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mFromDate:J

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 139
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->onDestroy()V

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mDateImage:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->mDateImage:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 146
    :cond_0
    return-void
.end method

.method protected setupDialog(Landroid/app/AlertDialog$Builder;)V
    .locals 2
    .param p1, "builder"    # Landroid/app/AlertDialog$Builder;

    .prologue
    .line 151
    const v0, 0x7f0b0015

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 160
    const v0, 0x7f0b0017

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment$2;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 171
    return-void
.end method

.method protected setupInputEditText(Landroid/widget/EditText;)V
    .locals 0
    .param p1, "editText"    # Landroid/widget/EditText;

    .prologue
    .line 194
    return-void
.end method
