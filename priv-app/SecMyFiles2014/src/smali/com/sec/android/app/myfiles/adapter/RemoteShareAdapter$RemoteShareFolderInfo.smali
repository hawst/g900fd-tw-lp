.class public Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter$RemoteShareFolderInfo;
.super Ljava/lang/Object;
.source "RemoteShareAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RemoteShareFolderInfo"
.end annotation


# instance fields
.field private mFilesCount:I

.field private mFilesSummarySize:J


# direct methods
.method public constructor <init>(IJ)V
    .locals 2
    .param p1, "filesCount"    # I
    .param p2, "filesSummarySize"    # J

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter$RemoteShareFolderInfo;->mFilesCount:I

    .line 61
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter$RemoteShareFolderInfo;->mFilesSummarySize:J

    .line 63
    iput p1, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter$RemoteShareFolderInfo;->mFilesCount:I

    .line 64
    iput-wide p2, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter$RemoteShareFolderInfo;->mFilesSummarySize:J

    .line 65
    return-void
.end method


# virtual methods
.method public getFilesCount()I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter$RemoteShareFolderInfo;->mFilesCount:I

    return v0
.end method

.method public getFilesSummarySize()J
    .locals 2

    .prologue
    .line 70
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter$RemoteShareFolderInfo;->mFilesSummarySize:J

    return-wide v0
.end method
