.class public Lcom/sec/android/app/myfiles/utils/MediaLoader$GolfDecoder;
.super Ljava/lang/Object;
.source "MediaLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/utils/MediaLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GolfDecoder"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1457
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static generateGolfFile(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 28
    .param p0, "tempFilePath"    # Ljava/lang/String;
    .param p1, "FilePath"    # Ljava/lang/String;
    .param p2, "allFrame"    # Z

    .prologue
    .line 1482
    const/4 v15, 0x0

    .line 1484
    .local v15, "inStream":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v9, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1485
    .local v9, "f":Ljava/io/File;
    new-instance v16, Ljava/io/FileInputStream;

    move-object/from16 v0, v16

    invoke-direct {v0, v9}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1486
    .end local v15    # "inStream":Ljava/io/FileInputStream;
    .local v16, "inStream":Ljava/io/FileInputStream;
    if-eqz v16, :cond_9

    .line 1487
    :try_start_1
    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v26

    move-wide/from16 v0, v26

    long-to-int v0, v0

    move/from16 v17, v0

    .line 1488
    .local v17, "len":I
    move/from16 v0, v17

    new-array v4, v0, [B

    .line 1489
    .local v4, "byteArray":[B
    const/16 v25, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v25

    move/from16 v2, v17

    invoke-virtual {v0, v4, v1, v2}, Ljava/io/FileInputStream;->read([BII)I

    .line 1490
    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1491
    .local v5, "byteBuf":Ljava/nio/ByteBuffer;
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 1492
    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1493
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v13

    .line 1494
    .local v13, "ib":Ljava/nio/IntBuffer;
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1496
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    move-result v23

    .line 1497
    .local v23, "ver_major":I
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    move-result v24

    .line 1498
    .local v24, "ver_minor":I
    const/16 v25, 0x1

    move/from16 v0, v23

    move/from16 v1, v25

    if-lt v0, v1, :cond_0

    const/16 v25, 0x4

    move/from16 v0, v24

    move/from16 v1, v25

    if-ge v0, v1, :cond_4

    .line 1499
    :cond_0
    if-eqz v16, :cond_1

    .line 1500
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1567
    :cond_1
    if-eqz v16, :cond_2

    .line 1569
    :try_start_2
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_2
    :goto_0
    move-object/from16 v15, v16

    .line 1576
    .end local v4    # "byteArray":[B
    .end local v5    # "byteBuf":Ljava/nio/ByteBuffer;
    .end local v9    # "f":Ljava/io/File;
    .end local v13    # "ib":Ljava/nio/IntBuffer;
    .end local v16    # "inStream":Ljava/io/FileInputStream;
    .end local v17    # "len":I
    .end local v23    # "ver_major":I
    .end local v24    # "ver_minor":I
    .restart local v15    # "inStream":Ljava/io/FileInputStream;
    :cond_3
    :goto_1
    return-void

    .line 1570
    .end local v15    # "inStream":Ljava/io/FileInputStream;
    .restart local v4    # "byteArray":[B
    .restart local v5    # "byteBuf":Ljava/nio/ByteBuffer;
    .restart local v9    # "f":Ljava/io/File;
    .restart local v13    # "ib":Ljava/nio/IntBuffer;
    .restart local v16    # "inStream":Ljava/io/FileInputStream;
    .restart local v17    # "len":I
    .restart local v23    # "ver_major":I
    .restart local v24    # "ver_minor":I
    :catch_0
    move-exception v8

    .line 1572
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1505
    .end local v8    # "e":Ljava/io/IOException;
    :cond_4
    :try_start_3
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    move-result v11

    .line 1506
    .local v11, "header_length":I
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    move-result v10

    .line 1507
    .local v10, "format":I
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    move-result v18

    .line 1510
    .local v18, "nFrames":I
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1511
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1512
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1513
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1515
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1516
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1517
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1518
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1519
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1520
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1521
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1522
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1523
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1524
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1525
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1526
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I

    .line 1527
    const/16 v19, 0x0

    .line 1529
    .local v19, "num_video_seq":I
    if-eqz p2, :cond_6

    .line 1530
    invoke-virtual {v13}, Ljava/nio/IntBuffer;->get()I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v19

    .line 1531
    if-gtz v19, :cond_6

    .line 1567
    if-eqz v16, :cond_5

    .line 1569
    :try_start_4
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_5
    :goto_2
    move-object/from16 v15, v16

    .line 1573
    .end local v16    # "inStream":Ljava/io/FileInputStream;
    .restart local v15    # "inStream":Ljava/io/FileInputStream;
    goto :goto_1

    .line 1570
    .end local v15    # "inStream":Ljava/io/FileInputStream;
    .restart local v16    # "inStream":Ljava/io/FileInputStream;
    :catch_1
    move-exception v8

    .line 1572
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 1534
    .end local v8    # "e":Ljava/io/IOException;
    :cond_6
    :try_start_5
    invoke-virtual {v5, v11}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1536
    const/16 v25, 0x1

    move/from16 v0, v23

    move/from16 v1, v25

    if-lt v0, v1, :cond_9

    const/16 v25, 0x2

    move/from16 v0, v24

    move/from16 v1, v25

    if-lt v0, v1, :cond_9

    .line 1538
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v14

    .line 1542
    .local v14, "ib1":Ljava/nio/IntBuffer;
    invoke-virtual {v14}, Ljava/nio/IntBuffer;->remaining()I

    move-result v25

    sub-int v25, v25, v18

    sub-int v25, v25, v19

    add-int/lit8 v21, v25, -0x1

    .line 1543
    .local v21, "offset_start_in_ib":I
    add-int/lit8 v25, v19, 0x1

    add-int v25, v25, v18

    move/from16 v0, v25

    new-array v0, v0, [I

    move-object/from16 v20, v0

    .line 1544
    .local v20, "offsetArray":[I
    move/from16 v0, v21

    invoke-virtual {v14, v0}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;

    .line 1546
    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Ljava/nio/IntBuffer;->get([I)Ljava/nio/IntBuffer;

    .line 1547
    if-eqz p2, :cond_7

    add-int/lit8 v25, v19, 0x1

    add-int v6, v25, v18

    .line 1548
    .local v6, "count":I
    :goto_3
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_4
    if-ge v12, v6, :cond_9

    .line 1549
    const/16 v22, 0x0

    .line 1550
    .local v22, "size":I
    add-int v25, v19, v18

    move/from16 v0, v25

    if-ne v6, v0, :cond_8

    .line 1551
    aget v25, v20, v12
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    sub-int v22, v21, v25

    .line 1555
    :goto_5
    :try_start_6
    move/from16 v0, v22

    new-array v7, v0, [B

    .line 1556
    .local v7, "data":[B
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v25

    aget v26, v20, v12

    const/16 v27, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v27

    move/from16 v3, v22

    invoke-static {v0, v1, v7, v2, v3}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 1557
    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v22

    invoke-static {v0, v10, v1, v7}, Lcom/sec/android/app/myfiles/utils/MediaLoader$GolfDecoder;->saveBufferToFile(Ljava/lang/String;II[B)V
    :try_end_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1548
    .end local v7    # "data":[B
    :goto_6
    add-int/lit8 v12, v12, 0x1

    goto :goto_4

    .line 1547
    .end local v6    # "count":I
    .end local v12    # "i":I
    .end local v22    # "size":I
    :cond_7
    const/4 v6, 0x1

    goto :goto_3

    .line 1553
    .restart local v6    # "count":I
    .restart local v12    # "i":I
    .restart local v22    # "size":I
    :cond_8
    add-int/lit8 v25, v12, 0x1

    :try_start_7
    aget v25, v20, v25

    aget v26, v20, v12

    sub-int v22, v25, v26

    goto :goto_5

    .line 1558
    :catch_2
    move-exception v8

    .line 1559
    .local v8, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v8}, Ljava/lang/OutOfMemoryError;->printStackTrace()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_6

    .line 1564
    .end local v4    # "byteArray":[B
    .end local v5    # "byteBuf":Ljava/nio/ByteBuffer;
    .end local v6    # "count":I
    .end local v8    # "e":Ljava/lang/OutOfMemoryError;
    .end local v10    # "format":I
    .end local v11    # "header_length":I
    .end local v12    # "i":I
    .end local v13    # "ib":Ljava/nio/IntBuffer;
    .end local v14    # "ib1":Ljava/nio/IntBuffer;
    .end local v17    # "len":I
    .end local v18    # "nFrames":I
    .end local v19    # "num_video_seq":I
    .end local v20    # "offsetArray":[I
    .end local v21    # "offset_start_in_ib":I
    .end local v22    # "size":I
    .end local v23    # "ver_major":I
    .end local v24    # "ver_minor":I
    :catch_3
    move-exception v8

    move-object/from16 v15, v16

    .line 1565
    .end local v9    # "f":Ljava/io/File;
    .end local v16    # "inStream":Ljava/io/FileInputStream;
    .local v8, "e":Ljava/lang/Exception;
    .restart local v15    # "inStream":Ljava/io/FileInputStream;
    :goto_7
    :try_start_8
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1567
    if-eqz v15, :cond_3

    .line 1569
    :try_start_9
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    goto/16 :goto_1

    .line 1570
    :catch_4
    move-exception v8

    .line 1572
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 1567
    .end local v8    # "e":Ljava/io/IOException;
    .end local v15    # "inStream":Ljava/io/FileInputStream;
    .restart local v9    # "f":Ljava/io/File;
    .restart local v16    # "inStream":Ljava/io/FileInputStream;
    :cond_9
    if-eqz v16, :cond_b

    .line 1569
    :try_start_a
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    move-object/from16 v15, v16

    .line 1573
    .end local v16    # "inStream":Ljava/io/FileInputStream;
    .restart local v15    # "inStream":Ljava/io/FileInputStream;
    goto/16 :goto_1

    .line 1570
    .end local v15    # "inStream":Ljava/io/FileInputStream;
    .restart local v16    # "inStream":Ljava/io/FileInputStream;
    :catch_5
    move-exception v8

    .line 1572
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    move-object/from16 v15, v16

    .line 1573
    .end local v16    # "inStream":Ljava/io/FileInputStream;
    .restart local v15    # "inStream":Ljava/io/FileInputStream;
    goto/16 :goto_1

    .line 1567
    .end local v8    # "e":Ljava/io/IOException;
    .end local v9    # "f":Ljava/io/File;
    :catchall_0
    move-exception v25

    :goto_8
    if-eqz v15, :cond_a

    .line 1569
    :try_start_b
    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6

    .line 1573
    :cond_a
    :goto_9
    throw v25

    .line 1570
    :catch_6
    move-exception v8

    .line 1572
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 1567
    .end local v8    # "e":Ljava/io/IOException;
    .end local v15    # "inStream":Ljava/io/FileInputStream;
    .restart local v9    # "f":Ljava/io/File;
    .restart local v16    # "inStream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v25

    move-object/from16 v15, v16

    .end local v16    # "inStream":Ljava/io/FileInputStream;
    .restart local v15    # "inStream":Ljava/io/FileInputStream;
    goto :goto_8

    .line 1564
    .end local v9    # "f":Ljava/io/File;
    :catch_7
    move-exception v8

    goto :goto_7

    .end local v15    # "inStream":Ljava/io/FileInputStream;
    .restart local v9    # "f":Ljava/io/File;
    .restart local v16    # "inStream":Ljava/io/FileInputStream;
    :cond_b
    move-object/from16 v15, v16

    .end local v16    # "inStream":Ljava/io/FileInputStream;
    .restart local v15    # "inStream":Ljava/io/FileInputStream;
    goto/16 :goto_1
.end method

.method private static saveBufferToFile(Ljava/lang/String;II[B)V
    .locals 6
    .param p0, "fileName"    # Ljava/lang/String;
    .param p1, "format"    # I
    .param p2, "size"    # I
    .param p3, "buffer"    # [B

    .prologue
    .line 1459
    const-string v1, ".jpg"

    .line 1460
    .local v1, "ext":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1461
    .local v2, "fl":Ljava/io/File;
    const/4 v3, 0x0

    .line 1463
    .local v3, "stream":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1464
    .end local v3    # "stream":Ljava/io/FileOutputStream;
    .local v4, "stream":Ljava/io/FileOutputStream;
    const/4 v5, 0x0

    :try_start_1
    invoke-virtual {v4, p3, v5, p2}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1470
    if-eqz v4, :cond_2

    .line 1472
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v3, v4

    .line 1479
    .end local v4    # "stream":Ljava/io/FileOutputStream;
    .restart local v3    # "stream":Ljava/io/FileOutputStream;
    :cond_0
    :goto_0
    return-void

    .line 1473
    .end local v3    # "stream":Ljava/io/FileOutputStream;
    .restart local v4    # "stream":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v0

    .line 1475
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v3, v4

    .line 1476
    .end local v4    # "stream":Ljava/io/FileOutputStream;
    .restart local v3    # "stream":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 1465
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 1466
    .local v0, "e":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_3
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1470
    if-eqz v3, :cond_0

    .line 1472
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 1473
    :catch_2
    move-exception v0

    .line 1475
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1467
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 1468
    .restart local v0    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_5
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1470
    if-eqz v3, :cond_0

    .line 1472
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 1473
    :catch_4
    move-exception v0

    .line 1475
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1470
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    :goto_3
    if-eqz v3, :cond_1

    .line 1472
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 1476
    :cond_1
    :goto_4
    throw v5

    .line 1473
    :catch_5
    move-exception v0

    .line 1475
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 1470
    .end local v0    # "e":Ljava/io/IOException;
    .end local v3    # "stream":Ljava/io/FileOutputStream;
    .restart local v4    # "stream":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v5

    move-object v3, v4

    .end local v4    # "stream":Ljava/io/FileOutputStream;
    .restart local v3    # "stream":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 1467
    .end local v3    # "stream":Ljava/io/FileOutputStream;
    .restart local v4    # "stream":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v0

    move-object v3, v4

    .end local v4    # "stream":Ljava/io/FileOutputStream;
    .restart local v3    # "stream":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 1465
    .end local v3    # "stream":Ljava/io/FileOutputStream;
    .restart local v4    # "stream":Ljava/io/FileOutputStream;
    :catch_7
    move-exception v0

    move-object v3, v4

    .end local v4    # "stream":Ljava/io/FileOutputStream;
    .restart local v3    # "stream":Ljava/io/FileOutputStream;
    goto :goto_1

    .end local v3    # "stream":Ljava/io/FileOutputStream;
    .restart local v4    # "stream":Ljava/io/FileOutputStream;
    :cond_2
    move-object v3, v4

    .end local v4    # "stream":Ljava/io/FileOutputStream;
    .restart local v3    # "stream":Ljava/io/FileOutputStream;
    goto :goto_0
.end method
