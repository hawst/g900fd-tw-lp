.class public Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;
.super Ljava/lang/Object;
.source "CategoryAdapterDevicesCollection.java"

# interfaces
.implements Ljava/util/Collection;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$1;,
        Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;,
        Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Collection",
        "<",
        "Lcom/samsung/android/allshare/Device;",
        ">;"
    }
.end annotation


# static fields
.field private static final MODULE:Ljava/lang/String; = "CategoryAdapterDevicesCollection"

.field public static final NEARBY_DEVICES_DEFAULT_ICON:I = 0x7f020001


# instance fields
.field private final mContext:Landroid/content/Context;

.field mDevicesCollectionUpdatedListener:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;

.field private final mDevicesMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/samsung/android/allshare/Device;",
            "Lcom/sec/android/app/myfiles/element/ShortcutItem;",
            ">;"
        }
    .end annotation
.end field

.field private final mNearbyDrawableProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;

.field private mShortcutAdapter:Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesShortcutAdapter;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;)V
    .locals 2
    .param p1, "builder"    # Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    # getter for: Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;->access$100(Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mContext:Landroid/content/Context;

    .line 73
    # getter for: Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;->mDevicesCollectionUpdatedListener:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;
    invoke-static {p1}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;->access$200(Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;)Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mDevicesCollectionUpdatedListener:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mDevicesMap:Ljava/util/HashMap;

    .line 75
    new-instance v0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;

    # getter for: Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;->access$100(Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mNearbyDrawableProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;

    .line 76
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;
    .param p2, "x1"    # Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$1;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;-><init>(Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$Builder;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private getDevicePath(Lcom/samsung/android/allshare/Device;)Ljava/lang/String;
    .locals 1
    .param p1, "device"    # Lcom/samsung/android/allshare/Device;

    .prologue
    .line 282
    invoke-virtual {p1}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getShortcutItem(Lcom/samsung/android/allshare/Device;)Lcom/sec/android/app/myfiles/element/ShortcutItem;
    .locals 7
    .param p1, "device"    # Lcom/samsung/android/allshare/Device;

    .prologue
    const/4 v4, 0x0

    .line 220
    iget-object v1, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mDevicesMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 221
    iget-object v1, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mDevicesMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 278
    :goto_0
    return-object v1

    .line 223
    :cond_0
    new-instance v0, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    const/16 v1, 0x9

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->getDevicePath(Lcom/samsung/android/allshare/Device;)Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mContext:Landroid/content/Context;

    invoke-static {v5, v4}, Lcom/sec/android/app/myfiles/utils/Utils;->getFirstUnsuedIndexFromShottut(Landroid/content/Context;Z)I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/element/ShortcutItem;-><init>(ILjava/lang/String;Ljava/lang/String;ZI)V

    .line 229
    .local v0, "shortcutItem":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/Device;->getIcon()Landroid/net/Uri;

    move-result-object v1

    if-nez v1, :cond_1

    .line 232
    iget-object v1, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 275
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mNearbyDrawableProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Device;->getIcon()Landroid/net/Uri;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$1UpdateCategoryItemIcon;

    invoke-direct {v3, p0, v0}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$1UpdateCategoryItemIcon;-><init>(Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;Lcom/sec/android/app/myfiles/element/ShortcutItem;)V

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;->setDrawable(Landroid/net/Uri;Lcom/sec/android/app/myfiles/nearbydevices/DrawableUpdatable;)V

    move-object v1, v0

    .line 278
    goto :goto_0

    .line 237
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Device;->getIcon()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v1

    const-string v2, ""

    invoke-static {v1, v2}, Landroid/graphics/drawable/Drawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->setIcon(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 238
    :catch_0
    move-exception v6

    .line 241
    .local v6, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v6}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method private refreshView()V
    .locals 0

    .prologue
    .line 216
    return-void
.end method


# virtual methods
.method public declared-synchronized add(Lcom/samsung/android/allshare/Device;)Z
    .locals 9
    .param p1, "device"    # Lcom/samsung/android/allshare/Device;

    .prologue
    const/4 v7, 0x0

    .line 91
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 92
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->getShortcutItem(Lcom/samsung/android/allshare/Device;)Lcom/sec/android/app/myfiles/element/ShortcutItem;

    move-result-object v8

    .line 93
    .local v8, "shortcutItem":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getTitle()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x9

    const/4 v4, -0x1

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/myfiles/utils/Utils;->addShortCut(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/drawable/Drawable;Z)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mDevicesCollectionUpdatedListener:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;

    invoke-interface {v0}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;->notifyDataSetChanged()V

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mDevicesMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v7, 0x1

    .line 109
    .end local v8    # "shortcutItem":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :cond_0
    monitor-exit p0

    return v7

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public bridge synthetic add(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 43
    check-cast p1, Lcom/samsung/android/allshare/Device;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->add(Lcom/samsung/android/allshare/Device;)Z

    move-result v0

    return v0
.end method

.method public addAll(Ljava/util/Collection;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lcom/samsung/android/allshare/Device;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 148
    .local p1, "devices":Ljava/util/Collection;, "Ljava/util/Collection<+Lcom/samsung/android/allshare/Device;>;"
    const/4 v2, 0x1

    .line 149
    .local v2, "retVal":Z
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/Device;

    .line 150
    .local v0, "device":Lcom/samsung/android/allshare/Device;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->add(Lcom/samsung/android/allshare/Device;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 151
    const/4 v2, 0x0

    goto :goto_0

    .line 154
    .end local v0    # "device":Lcom/samsung/android/allshare/Device;
    :cond_1
    return v2
.end method

.method public declared-synchronized clear()V
    .locals 3

    .prologue
    .line 134
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mDevicesMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 136
    .local v1, "shortcutItem":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mDevicesCollectionUpdatedListener:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;

    invoke-interface {v2, v1}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;->removeItem(Lcom/sec/android/app/myfiles/element/ShortcutItem;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 134
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "shortcutItem":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 139
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mDevicesMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 141
    iget-object v2, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mDevicesCollectionUpdatedListener:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;

    invoke-interface {v2}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;->notifyDataSetChanged()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 144
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized contains(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 159
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mDevicesMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized containsAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 164
    .local p1, "objects":Ljava/util/Collection;, "Ljava/util/Collection<*>;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mDevicesMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getDevice(Ljava/lang/String;)Lcom/samsung/android/allshare/Device;
    .locals 3
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 79
    if-eqz p1, :cond_1

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/Device;

    .line 81
    .local v0, "device":Lcom/samsung/android/allshare/Device;
    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->getDevicePath(Lcom/samsung/android/allshare/Device;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 86
    .end local v0    # "device":Lcom/samsung/android/allshare/Device;
    .end local v1    # "i$":Ljava/util/Iterator;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized isEmpty()Z
    .locals 1

    .prologue
    .line 169
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mDevicesMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/samsung/android/allshare/Device;",
            ">;"
        }
    .end annotation

    .prologue
    .line 174
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mDevicesMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized remove(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x0

    .line 115
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mDevicesMap:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 116
    iget-object v3, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mDevicesMap:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 118
    .local v2, "shortcutItem":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteShortcut(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v1

    .line 119
    .local v1, "rowsDeleted":I
    if-lez v1, :cond_0

    .line 120
    const/4 v3, 0x0

    const-string v4, "CategoryAdapterDevicesCollection"

    const-string v5, "NearbyDevice\'s shortcut deleted"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 125
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mDevicesCollectionUpdatedListener:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;

    invoke-interface {v3}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;->notifyDataSetChanged()V

    .line 126
    iget-object v3, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mDevicesMap:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v0, 0x1

    .line 129
    .end local v1    # "rowsDeleted":I
    .end local v2    # "shortcutItem":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :cond_1
    monitor-exit p0

    return v0

    .line 115
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public removeAll(Ljava/util/Collection;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 179
    .local p1, "objects":Ljava/util/Collection;, "Ljava/util/Collection<*>;"
    const/4 v2, 0x1

    .line 180
    .local v2, "retVal":Z
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 181
    .local v1, "object":Ljava/lang/Object;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->remove(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 182
    const/4 v2, 0x0

    goto :goto_0

    .line 185
    .end local v1    # "object":Ljava/lang/Object;
    :cond_1
    return v2
.end method

.method public retainAll(Ljava/util/Collection;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 190
    .local p1, "objects":Ljava/util/Collection;, "Ljava/util/Collection<*>;"
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->toArray()[Ljava/lang/Object;

    move-result-object v0

    .line 191
    .local v0, "arr":[Ljava/lang/Object;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 192
    aget-object v2, v0, v1

    invoke-interface {p1, v2}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 193
    aget-object v2, v0, v1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->remove(Ljava/lang/Object;)Z

    .line 191
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 196
    :cond_1
    const/4 v2, 0x0

    return v2
.end method

.method public setDevicesCollectionUpdatedListener(Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;)V
    .locals 0
    .param p1, "devicesCollectionUpdatedListener"    # Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;

    .prologue
    .line 296
    iput-object p1, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mDevicesCollectionUpdatedListener:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;

    .line 297
    return-void
.end method

.method public setShortcutAdapter(Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesShortcutAdapter;)V
    .locals 0
    .param p1, "shortcutViewAdapter"    # Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesShortcutAdapter;

    .prologue
    .line 290
    iput-object p1, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mShortcutAdapter:Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesShortcutAdapter;

    .line 291
    return-void
.end method

.method public declared-synchronized size()I
    .locals 1

    .prologue
    .line 201
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mDevicesMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized toArray()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 206
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mDevicesMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;)[TT;"
        }
    .end annotation

    .prologue
    .line 211
    .local p1, "array":[Ljava/lang/Object;, "[TT;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mDevicesMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
