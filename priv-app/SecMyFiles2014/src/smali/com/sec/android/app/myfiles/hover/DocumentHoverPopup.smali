.class public Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;
.super Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
.source "DocumentHoverPopup.java"


# static fields
.field private static final MODULE:Ljava/lang/String; = "DocumentHoverPopup"


# instance fields
.field private layout:Landroid/widget/RelativeLayout;

.field private mHoverImg:Landroid/graphics/Bitmap;

.field private mHoverOperationContainer:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 66
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;-><init>(Landroid/content/Context;)V

    .line 58
    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->mHoverImg:Landroid/graphics/Bitmap;

    .line 60
    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->layout:Landroid/widget/RelativeLayout;

    .line 67
    return-void
.end method

.method private computeSampleSize(Landroid/graphics/BitmapFactory$Options;I)I
    .locals 7
    .param p1, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p2, "target"    # I

    .prologue
    const/4 v5, 0x1

    .line 84
    iget v4, p1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 85
    .local v4, "w":I
    iget v3, p1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 86
    .local v3, "h":I
    div-int v2, v4, p2

    .line 87
    .local v2, "candidateW":I
    div-int v1, v3, p2

    .line 88
    .local v1, "candidateH":I
    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 90
    .local v0, "candidate":I
    if-nez v0, :cond_0

    .line 107
    :goto_0
    return v5

    .line 94
    :cond_0
    if-le v0, v5, :cond_1

    .line 95
    if-le v4, p2, :cond_1

    div-int v6, v4, v0

    if-ge v6, p2, :cond_1

    .line 96
    add-int/lit8 v0, v0, -0x1

    .line 99
    :cond_1
    if-le v0, v5, :cond_2

    .line 101
    if-le v3, p2, :cond_2

    div-int v5, v3, v0

    if-ge v5, p2, :cond_2

    .line 103
    add-int/lit8 v0, v0, -0x1

    :cond_2
    move v5, v0

    .line 107
    goto :goto_0
.end method

.method private getExifOrientation(Ljava/lang/String;)I
    .locals 9
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    const/4 v8, -0x1

    const/4 v5, 0x0

    .line 166
    if-nez p1, :cond_1

    .line 167
    const-string v6, "DocumentHoverPopup"

    const-string v7, "exif is null"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    move v0, v5

    .line 213
    :cond_0
    :goto_0
    return v0

    .line 173
    :cond_1
    const/4 v0, 0x0

    .line 175
    .local v0, "degree":I
    const/4 v2, 0x0

    .line 179
    .local v2, "exif":Landroid/media/ExifInterface;
    :try_start_0
    new-instance v3, Landroid/media/ExifInterface;

    invoke-direct {v3, p1}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "exif":Landroid/media/ExifInterface;
    .local v3, "exif":Landroid/media/ExifInterface;
    move-object v2, v3

    .line 186
    .end local v3    # "exif":Landroid/media/ExifInterface;
    .restart local v2    # "exif":Landroid/media/ExifInterface;
    :goto_1
    if-eqz v2, :cond_0

    .line 188
    const-string v5, "Orientation"

    invoke-virtual {v2, v5, v8}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v4

    .line 191
    .local v4, "orientation":I
    if-eq v4, v8, :cond_0

    .line 193
    packed-switch v4, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 200
    :pswitch_1
    const/16 v0, 0xb4

    .line 201
    goto :goto_0

    .line 181
    .end local v4    # "orientation":I
    :catch_0
    move-exception v1

    .line 183
    .local v1, "ex":Ljava/io/IOException;
    const-string v6, "DocumentHoverPopup"

    const-string v7, "cannot read exif"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 196
    .end local v1    # "ex":Ljava/io/IOException;
    .restart local v4    # "orientation":I
    :pswitch_2
    const/16 v0, 0x5a

    .line 197
    goto :goto_0

    .line 204
    :pswitch_3
    const/16 v0, 0x10e

    .line 205
    goto :goto_0

    .line 193
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private getSampleSizeBitmap(ILjava/lang/String;)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "targetWidthHeight"    # I
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    const/16 v6, 0x258

    const/4 v5, 0x1

    .line 112
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 115
    .local v1, "options":Landroid/graphics/BitmapFactory$Options;
    iput-boolean v5, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 116
    invoke-static {p2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 118
    .local v0, "bm":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 119
    const/4 v2, 0x0

    .line 161
    :goto_0
    return-object v2

    .line 120
    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 122
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Bitmap Width Hight ...... "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Height :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Bitmap Width Hight ...... "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Height :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-le v2, v6, :cond_1

    .line 129
    const/16 v2, 0x1f4

    invoke-static {v0, v6, v2, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 132
    :cond_1
    const/4 v2, -0x1

    if-eq p1, v2, :cond_2

    .line 134
    invoke-direct {p0, v1, p1}, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->computeSampleSize(Landroid/graphics/BitmapFactory$Options;I)I

    move-result v2

    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 137
    :cond_2
    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Bitmap Width Hight after ...... "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Height :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v0

    .line 161
    goto/16 :goto_0
.end method

.method private loadFullBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 71
    const/4 v0, 0x0

    .line 73
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz p1, :cond_0

    .line 74
    const/16 v1, 0x12c

    invoke-direct {p0, v1, p1}, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->getSampleSizeBitmap(ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 79
    :goto_0
    return-object v0

    .line 76
    :cond_0
    const/4 v1, 0x0

    const-string v2, "DocumentHoverPopup"

    const-string v3, "null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "b"    # Landroid/graphics/Bitmap;
    .param p2, "degrees"    # I

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/4 v9, 0x0

    .line 218
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    .line 220
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 222
    .local v5, "m":Landroid/graphics/Matrix;
    int-to-float v0, p2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-virtual {v5, v0, v1, v2}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 227
    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 230
    .local v7, "b2":Landroid/graphics/Bitmap;
    invoke-virtual {p1, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 232
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 233
    move-object p1, v7

    .line 243
    .end local v5    # "m":Landroid/graphics/Matrix;
    .end local v7    # "b2":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-object p1

    .line 236
    .restart local v5    # "m":Landroid/graphics/Matrix;
    :catch_0
    move-exception v8

    .line 238
    .local v8, "ex":Ljava/lang/OutOfMemoryError;
    const-string v0, "DocumentHoverPopup"

    const-string v1, "Out of memory"

    invoke-static {v9, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private scaledBitmapForSmallImage(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/16 v2, 0x190

    .line 248
    if-eqz p1, :cond_0

    .line 250
    const/4 v1, 0x1

    invoke-static {p1, v2, v2, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 255
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected changeList()V
    .locals 0

    .prologue
    .line 550
    return-void
.end method

.method protected changeListThreadStart()V
    .locals 0

    .prologue
    .line 556
    return-void
.end method

.method protected dissmissPopup()V
    .locals 4

    .prologue
    .line 514
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->isShow()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 517
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 518
    const/4 v1, 0x0

    const-string v2, "DocumentHoverPopup"

    const-string v3, "ImageHoverDlg.dismiss(); "

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 527
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    .line 529
    :cond_0
    return-void

    .line 521
    :catch_0
    move-exception v0

    .line 523
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method protected initViews()Z
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v2, 0x0

    .line 261
    const/4 v9, 0x0

    .line 263
    .local v9, "noThumbnail":Landroid/graphics/Bitmap;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->mFilePath:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->bFromNearByDevice:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->bFromDropBox:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->mCategoryType:I

    const/16 v3, 0xa

    if-eq v0, v3, :cond_0

    move v0, v11

    .line 504
    :goto_0
    return v0

    .line 267
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->mFilePath:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->bFromDropBox:Z

    if-nez v0, :cond_4

    .line 269
    const-string v0, "content://com.samsung.docthumbnail.thumbnailprovider/thumbnailpath"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 271
    .local v1, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->mFilePath:Ljava/lang/String;

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 273
    .local v10, "thumbnailCursor":Landroid/database/Cursor;
    const-string v6, ""

    .line 274
    .local v6, "filePath":Ljava/lang/String;
    if-eqz v10, :cond_2

    .line 275
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 276
    const-string v0, "thumbnail_path"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    .line 281
    const-string v0, "thumbnail_path"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 285
    :cond_1
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 288
    :cond_2
    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->loadFullBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->mHoverImg:Landroid/graphics/Bitmap;

    .line 326
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v6    # "filePath":Ljava/lang/String;
    .end local v10    # "thumbnailCursor":Landroid/database/Cursor;
    :cond_3
    :goto_1
    new-instance v0, Landroid/app/Dialog;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-direct {v0, v3}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    .line 327
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v3, 0x7f04002d

    invoke-static {v0, v3, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->layout:Landroid/widget/RelativeLayout;

    .line 329
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->layout:Landroid/widget/RelativeLayout;

    const v2, 0x7f0f00be

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 331
    .local v8, "iv":Landroid/widget/ImageView;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->layout:Landroid/widget/RelativeLayout;

    const v2, 0x7f0f00bf

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->mHoverOperationContainer:Landroid/view/View;

    .line 335
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->mHoverOperationContainer:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->mHoverImg:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_5

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->mHoverImg:Landroid/graphics/Bitmap;

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 354
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->setDialogProperties()V

    .line 355
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->setDialogPosition()V

    .line 357
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 359
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->layout:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->initHoverOperationView(Landroid/view/View;)V

    .line 361
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->layout:Landroid/widget/RelativeLayout;

    new-instance v2, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup$1;-><init>(Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;)V

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 421
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->layout:Landroid/widget/RelativeLayout;

    new-instance v2, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup$2;-><init>(Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;)V

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 504
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 290
    .end local v8    # "iv":Landroid/widget/ImageView;
    :cond_4
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->bFromNearByDevice:Z

    if-nez v0, :cond_3

    .line 302
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->bFromDropBox:Z

    if-nez v0, :cond_3

    .line 323
    iput-object v2, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->mHoverImg:Landroid/graphics/Bitmap;

    goto :goto_1

    .line 342
    .restart local v8    # "iv":Landroid/widget/ImageView;
    :cond_5
    const-string v0, "DocumentHoverPopup"

    const-string v2, "mHoverImg is empty ~ !!! "

    invoke-static {v11, v0, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 345
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->mFilePath:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->mFilePath:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v3, v4}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;Landroid/content/Context;)I

    move-result v3

    invoke-static {v0, v2, v3}, Lcom/sec/android/app/myfiles/MediaFile;->getLargeIcon(Ljava/lang/String;Landroid/content/Context;I)I

    move-result v7

    .line 348
    .local v7, "iconId":I
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v7}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->scaledBitmapForSmallImage(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 351
    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_2
.end method

.method protected setDialogPosition()V
    .locals 4

    .prologue
    .line 533
    const/4 v1, 0x0

    const-string v2, "DocumentHoverPopup"

    const-string v3, "setPosition"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    .line 535
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 539
    .local v0, "params":Landroid/view/WindowManager$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->mHoverRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v1, v1, -0x32

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 540
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->mHoverRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    add-int/lit8 v1, v1, -0x64

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 541
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 542
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x33

    invoke-virtual {v1, v2}, Landroid/view/Window;->setGravity(I)V

    .line 544
    return-void
.end method

.method protected showContent()V
    .locals 0

    .prologue
    .line 510
    return-void
.end method
