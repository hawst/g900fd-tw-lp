.class public Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;
.super Ljava/lang/Object;
.source "FTPConnectionHandler.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler$1;
    }
.end annotation


# static fields
.field private static final ANONYMOUS_LOGIN:Ljava/lang/String; = "anonymous"

.field private static final ANONYMOUS_PASSWORD:Ljava/lang/String; = ""

.field private static final BUFFER_SIZE:I = 0x10000

.field private static final FILE_PACKAGE_SIZE:I = 0x19

.field private static final MDTM:Ljava/lang/String; = "MdTm"

.field private static final MODULE:Ljava/lang/String; = "FTPConnectionHandler"

.field public static success:Z


# instance fields
.field private volatile mAbortTransferring:Z

.field private volatile mBrowsing:Z

.field private mClient:Lorg/apache/commons/net/ftp/FTPClient;

.field private mClientOperationListener:Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;

.field private mDownloadIs:Ljava/io/InputStream;

.field private mDownloadOs:Ljava/io/OutputStream;

.field private mLastErrorCode:I

.field private final mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

.field private volatile mSupportsMdtm:Ljava/lang/Boolean;

.field private mSync:Ljava/lang/Object;

.field private volatile mTransferring:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->success:Z

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/myfiles/ftp/FTPParams;)V
    .locals 2
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1229
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mSync:Ljava/lang/Object;

    .line 1230
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mTransferring:Z

    .line 1231
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mAbortTransferring:Z

    .line 1232
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mBrowsing:Z

    .line 1233
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mLastErrorCode:I

    .line 53
    if-nez p1, :cond_0

    .line 54
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "FTPParams cannot be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 56
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .line 57
    invoke-static {p1}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->getFTPClient(Lcom/sec/android/app/myfiles/ftp/FTPParams;)Lorg/apache/commons/net/ftp/FTPClient;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    .line 58
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;
    .param p1, "x1"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mTransferring:Z

    return p1
.end method

.method static synthetic access$102(Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;
    .param p1, "x1"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mAbortTransferring:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;)Lorg/apache/commons/net/ftp/FTPClient;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;)Ljava/io/InputStream;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadIs:Ljava/io/InputStream;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;)Ljava/io/OutputStream;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadOs:Ljava/io/OutputStream;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->disconnect()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->recreateFtpClient()V

    return-void
.end method

.method private disconnect()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1154
    const/4 v1, 0x2

    const-string v2, "FTPConnectionHandler"

    const-string v3, "disconnect"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    .line 1158
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v1}, Lorg/apache/commons/net/ftp/FTPClient;->disconnect()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1172
    :goto_0
    return-void

    .line 1160
    :catch_0
    move-exception v0

    .line 1162
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "FTPConnectionHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "disconnect IOException: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1164
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 1166
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "FTPConnectionHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "EXCEPTION: disconnect failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 1168
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->recreateFtpClient()V

    goto :goto_0
.end method

.method private fillList(Lorg/apache/commons/net/ftp/FTPListParseEngine;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 10
    .param p1, "engine"    # Lorg/apache/commons/net/ftp/FTPListParseEngine;
    .param p3, "workingDir"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/commons/net/ftp/FTPListParseEngine;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    const/4 v9, 0x2

    .line 851
    const/4 v5, 0x2

    :try_start_0
    const-string v6, "FTPConnectionHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "fillList start engine size : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lorg/apache/commons/net/ftp/FTPListParseEngine;->getFiles()[Lorg/apache/commons/net/ftp/FTPFile;

    move-result-object v8

    array-length v8, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 856
    :goto_0
    const-string v5, "FTPConnectionHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "fillList start mBrowsing : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mBrowsing:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v9, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 857
    :cond_0
    :goto_1
    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mBrowsing:Z

    if-eqz v5, :cond_3

    invoke-virtual {p1}, Lorg/apache/commons/net/ftp/FTPListParseEngine;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 859
    const/16 v5, 0x19

    invoke-virtual {p1, v5}, Lorg/apache/commons/net/ftp/FTPListParseEngine;->getNext(I)[Lorg/apache/commons/net/ftp/FTPFile;

    move-result-object v0

    .local v0, "arr$":[Lorg/apache/commons/net/ftp/FTPFile;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_2
    if-ge v3, v4, :cond_0

    aget-object v2, v0, v3

    .line 861
    .local v2, "f":Lorg/apache/commons/net/ftp/FTPFile;
    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mBrowsing:Z

    if-nez v5, :cond_1

    .line 862
    const-string v5, "FTPConnectionHandler"

    const-string v6, "fillList mBrowsing : false"

    invoke-static {v9, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 852
    .end local v0    # "arr$":[Lorg/apache/commons/net/ftp/FTPFile;
    .end local v2    # "f":Lorg/apache/commons/net/ftp/FTPFile;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :catch_0
    move-exception v1

    .line 854
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 866
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v0    # "arr$":[Lorg/apache/commons/net/ftp/FTPFile;
    .restart local v2    # "f":Lorg/apache/commons/net/ftp/FTPFile;
    .restart local v3    # "i$":I
    .restart local v4    # "len$":I
    :cond_1
    if-eqz v2, :cond_2

    .line 868
    const-string v5, "."

    invoke-virtual {v2}, Lorg/apache/commons/net/ftp/FTPFile;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, ".."

    invoke-virtual {v2}, Lorg/apache/commons/net/ftp/FTPFile;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 870
    invoke-static {v2, p3}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getItem(Lorg/apache/commons/net/ftp/FTPFile;Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPItem;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 859
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 879
    .end local v0    # "arr$":[Lorg/apache/commons/net/ftp/FTPFile;
    .end local v2    # "f":Lorg/apache/commons/net/ftp/FTPFile;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :cond_3
    const-string v5, "FTPConnectionHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "fillList end mBrowsing : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mBrowsing:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v9, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 880
    const-string v5, "FTPConnectionHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "fillList end items.size : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v9, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 882
    return-void
.end method

.method private static getFTPClient(Lcom/sec/android/app/myfiles/ftp/FTPParams;)Lorg/apache/commons/net/ftp/FTPClient;
    .locals 3
    .param p0, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .prologue
    .line 1200
    const/4 v0, 0x0

    .line 1202
    .local v0, "ftp":Lorg/apache/commons/net/ftp/FTPClient;
    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler$1;->$SwitchMap$com$sec$android$app$myfiles$ftp$FTPType:[I

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getType()Lcom/sec/android/app/myfiles/ftp/FTPType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1209
    new-instance v0, Lorg/apache/commons/net/ftp/FTPClient;

    .end local v0    # "ftp":Lorg/apache/commons/net/ftp/FTPClient;
    invoke-direct {v0}, Lorg/apache/commons/net/ftp/FTPClient;-><init>()V

    .line 1212
    .restart local v0    # "ftp":Lorg/apache/commons/net/ftp/FTPClient;
    :goto_0
    return-object v0

    .line 1204
    :pswitch_0
    new-instance v0, Lorg/apache/commons/net/ftp/FTPSClient;

    .end local v0    # "ftp":Lorg/apache/commons/net/ftp/FTPClient;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getEncryption()Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/myfiles/ftp/FTPEncryption;->IMPLICIT:Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-direct {v0, v1}, Lorg/apache/commons/net/ftp/FTPSClient;-><init>(Z)V

    .line 1206
    .restart local v0    # "ftp":Lorg/apache/commons/net/ftp/FTPClient;
    goto :goto_0

    .line 1204
    .end local v0    # "ftp":Lorg/apache/commons/net/ftp/FTPClient;
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 1202
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private static makeDirectoryOnLocal(Ljava/lang/String;)Z
    .locals 5
    .param p0, "targetDir"    # Ljava/lang/String;

    .prologue
    .line 1187
    const/4 v1, 0x2

    const-string v2, "FTPConnectionHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "makeDirectoryOnLocal targetDir: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1188
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1189
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1190
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1191
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    .line 1196
    .end local v0    # "dir":Ljava/io/File;
    :goto_0
    return v1

    .line 1193
    .restart local v0    # "dir":Ljava/io/File;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0

    .line 1196
    .end local v0    # "dir":Ljava/io/File;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private recreateFtpClient()V
    .locals 1

    .prologue
    .line 1176
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->getFTPClient(Lcom/sec/android/app/myfiles/ftp/FTPParams;)Lorg/apache/commons/net/ftp/FTPClient;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    .line 1178
    return-void
.end method


# virtual methods
.method public abortBrowse()V
    .locals 1

    .prologue
    .line 937
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mBrowsing:Z

    .line 938
    return-void
.end method

.method public abortTransfer()V
    .locals 3

    .prologue
    .line 929
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler$1AbortTransfer;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler$1AbortTransfer;-><init>(Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;)V

    .line 931
    .local v0, "at":Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler$1AbortTransfer;
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler$1AbortTransfer;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 933
    return-void
.end method

.method public getFtpParams()Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .locals 1

    .prologue
    .line 1217
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    return-object v0
.end method

.method public getLastErrorCode()I
    .locals 1

    .prologue
    .line 1149
    iget v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mLastErrorCode:I

    return v0
.end method

.method public getStat()I
    .locals 8

    .prologue
    const/4 v2, -0x1

    .line 943
    const/4 v1, -0x1

    .line 945
    .local v1, "statInfo":I
    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mSync:Ljava/lang/Object;

    monitor-enter v3

    .line 949
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v4}, Lorg/apache/commons/net/ftp/FTPClient;->noop()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 967
    :try_start_1
    monitor-exit v3

    move v2, v1

    .line 969
    :goto_0
    return v2

    .line 951
    :catch_0
    move-exception v0

    .line 953
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->disconnect()V

    .line 955
    monitor-exit v3

    goto :goto_0

    .line 967
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 957
    :catch_1
    move-exception v0

    .line 959
    .local v0, "e":Ljava/lang/Exception;
    const/4 v4, 0x0

    :try_start_2
    const-string v5, "FTPConnectionHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "EXCEPTION: getStat failed: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 961
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->recreateFtpClient()V

    .line 963
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public getTimestamp(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 1002
    sget-boolean v2, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mAbortBrowsing:Z

    if-eqz v2, :cond_0

    .line 1003
    const/4 v2, 0x2

    const-string v3, "ftp"

    const-string v4, "mAbortBrowsing ture getTimestamp RETURN !!!"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1029
    :goto_0
    return-object v1

    .line 1009
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mSync:Ljava/lang/Object;

    monitor-enter v3

    .line 1013
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v2, p1}, Lorg/apache/commons/net/ftp/FTPClient;->getModificationTime(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1015
    .local v1, "modTime":Ljava/lang/String;
    :try_start_1
    monitor-exit v3

    goto :goto_0

    .line 1033
    .end local v1    # "modTime":Ljava/lang/String;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 1017
    :catch_0
    move-exception v0

    .line 1019
    .local v0, "e":Ljava/io/IOException;
    :try_start_2
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->disconnect()V

    .line 1021
    monitor-exit v3

    goto :goto_0

    .line 1023
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 1025
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    const-string v4, "FTPConnectionHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "EXCEPTION: getTimestamp failed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 1027
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->recreateFtpClient()V

    .line 1029
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public isConnected()Z
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v0}, Lorg/apache/commons/net/ftp/FTPClient;->isConnected()Z

    move-result v0

    .line 303
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isConnectedAndLoggedIn()Z
    .locals 1

    .prologue
    .line 294
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->isLoggedIn()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLoggedIn()Z
    .locals 2

    .prologue
    .line 310
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->getStat()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isReady()Z
    .locals 1

    .prologue
    .line 316
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->isConnectedAndLoggedIn()Z

    move-result v0

    return v0
.end method

.method public setFtpClientOperationListener(Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;

    .prologue
    .line 1182
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClientOperationListener:Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;

    .line 1184
    return-void
.end method

.method public syncBrowseForItemContent(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)Ljava/util/List;
    .locals 10
    .param p1, "item"    # Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v8, 0x2

    .line 763
    const-string v5, "FTPConnectionHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "syncBrowseForItemContent: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v8, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 764
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 766
    .local v2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    iget-object v5, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mSync:Ljava/lang/Object;

    monitor-enter v5

    .line 767
    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->isReady()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 768
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mBrowsing:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 773
    :try_start_1
    invoke-interface {p1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isRoot()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 775
    const-string v3, "/"

    .line 782
    .local v3, "workingDir":Ljava/lang/String;
    :goto_0
    const/4 v6, 0x2

    const-string v7, "FTPConnectionHandler"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "syncBrowseForItemContent workingDir: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 783
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 790
    iget-object v6, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v6, v3}, Lorg/apache/commons/net/ftp/FTPClient;->changeWorkingDirectory(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 791
    const/4 v6, 0x2

    const-string v7, "FTPConnectionHandler"

    const-string v8, "syncBrowseForItemContent changeWorkingDirectory : true"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 792
    iget-object v6, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v6}, Lorg/apache/commons/net/ftp/FTPClient;->initiateListParsing()Lorg/apache/commons/net/ftp/FTPListParseEngine;

    move-result-object v1

    .line 794
    .local v1, "engine":Lorg/apache/commons/net/ftp/FTPListParseEngine;
    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->fillList(Lorg/apache/commons/net/ftp/FTPListParseEngine;Ljava/util/ArrayList;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 841
    .end local v1    # "engine":Lorg/apache/commons/net/ftp/FTPListParseEngine;
    .end local v3    # "workingDir":Ljava/lang/String;
    :goto_1
    const/4 v4, 0x0

    :try_start_2
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mBrowsing:Z

    .line 843
    :cond_0
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 845
    .end local v2    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    :goto_2
    return-object v2

    .line 779
    .restart local v2    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    :cond_1
    :try_start_3
    invoke-interface {p1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "workingDir":Ljava/lang/String;
    goto :goto_0

    .line 798
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v6}, Lorg/apache/commons/net/ftp/FTPClient;->getReplyCode()I

    move-result v6

    const/16 v7, 0x226

    if-ne v6, v7, :cond_3

    .line 799
    const/4 v6, 0x2

    const-string v7, "FTPConnectionHandler"

    const-string v8, "Target folder does not exist."

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 800
    const/4 v6, 0x0

    iput v6, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mLastErrorCode:I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 801
    :try_start_4
    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object v2, v4

    goto :goto_2

    .line 804
    :cond_3
    const/4 v6, 0x2

    :try_start_5
    const-string v7, "FTPConnectionHandler"

    const-string v8, "syncBrowseForItemContent changeWorkingDirectory : false"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 805
    iget-object v6, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v6, v3}, Lorg/apache/commons/net/ftp/FTPClient;->initiateListParsing(Ljava/lang/String;)Lorg/apache/commons/net/ftp/FTPListParseEngine;

    move-result-object v1

    .line 807
    .restart local v1    # "engine":Lorg/apache/commons/net/ftp/FTPListParseEngine;
    invoke-direct {p0, v1, v2, v3}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->fillList(Lorg/apache/commons/net/ftp/FTPListParseEngine;Ljava/util/ArrayList;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 830
    .end local v1    # "engine":Lorg/apache/commons/net/ftp/FTPListParseEngine;
    .end local v3    # "workingDir":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 831
    .local v0, "e":Ljava/io/IOException;
    const/4 v6, 0x0

    :try_start_6
    const-string v7, "FTPConnectionHandler"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "syncBrowseForItemContent IOException: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 832
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->disconnect()V

    .line 833
    monitor-exit v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-object v2, v4

    goto :goto_2

    .line 827
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v3    # "workingDir":Ljava/lang/String;
    :cond_4
    const/4 v6, 0x2

    :try_start_7
    const-string v7, "FTPConnectionHandler"

    const-string v8, "syncBrowseForItemContent working directory is empty - a server error."

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 828
    :try_start_8
    monitor-exit v5

    move-object v2, v4

    goto :goto_2

    .line 834
    .end local v3    # "workingDir":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 836
    .local v0, "e":Ljava/lang/Exception;
    const/4 v4, 0x0

    const-string v6, "FTPConnectionHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "EXCEPTION: syncBrowseForItemContent failed: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v6, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 838
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->recreateFtpClient()V

    goto/16 :goto_1

    .line 843
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    throw v4
.end method

.method public syncConnect()Z
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x0

    .line 63
    sget-boolean v3, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mAbortBrowsing:Z

    if-eqz v3, :cond_0

    .line 64
    const-string v3, "ftp"

    const-string v4, "mAbortBrowsing ture syncConnect RETURN !!!"

    invoke-static {v5, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 127
    :goto_0
    return v2

    .line 69
    :cond_0
    const-string v3, "FTPConnectionHandler"

    const-string v4, "syncConnect"

    invoke-static {v5, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 72
    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mSync:Ljava/lang/Object;

    monitor-enter v3

    .line 74
    :try_start_0
    sget-boolean v4, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mAbortBrowsing:Z

    if-eqz v4, :cond_1

    .line 75
    const/4 v4, 0x2

    const-string v5, "ftp"

    const-string v6, "mAbortBrowsing ture mSync syncConnect RETURN !!!"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 76
    monitor-exit v3

    goto :goto_0

    .line 125
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 79
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->isConnected()Z

    move-result v2

    if-nez v2, :cond_3

    .line 83
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    const/16 v4, 0x1770

    invoke-virtual {v2, v4}, Lorg/apache/commons/net/ftp/FTPClient;->setConnectTimeout(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 88
    :try_start_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getEncoding()Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    move-result-object v2

    sget-object v4, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->AUTO:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    if-eq v2, v4, :cond_2

    .line 89
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getEncoding()Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lorg/apache/commons/net/ftp/FTPClient;->setControlEncoding(Ljava/lang/String;)V

    .line 91
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getPort()I

    move-result v2

    if-lez v2, :cond_4

    .line 92
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getHost()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getPort()I

    move-result v5

    invoke-virtual {v2, v4, v5}, Lorg/apache/commons/net/ftp/FTPClient;->connect(Ljava/lang/String;I)V

    .line 96
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getMode()Lcom/sec/android/app/myfiles/ftp/FTPMode;

    move-result-object v2

    sget-object v4, Lcom/sec/android/app/myfiles/ftp/FTPMode;->PASSIVE:Lcom/sec/android/app/myfiles/ftp/FTPMode;

    if-ne v2, v4, :cond_5

    .line 97
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v2}, Lorg/apache/commons/net/ftp/FTPClient;->enterLocalPassiveMode()V

    .line 101
    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v2}, Lorg/apache/commons/net/ftp/FTPClient;->getReplyCode()I

    move-result v1

    .line 102
    .local v1, "reply":I
    invoke-static {v1}, Lorg/apache/commons/net/ftp/FTPReply;->isPositiveCompletion(I)Z

    move-result v2

    if-nez v2, :cond_6

    .line 103
    const/4 v2, 0x0

    const-string v4, "FTPConnectionHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Connect failed reply: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 104
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v2}, Lorg/apache/commons/net/ftp/FTPClient;->disconnect()V

    .line 105
    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->success:Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 125
    .end local v1    # "reply":I
    :cond_3
    :goto_3
    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 127
    sget-boolean v2, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->success:Z

    goto/16 :goto_0

    .line 94
    :cond_4
    :try_start_4
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getHost()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lorg/apache/commons/net/ftp/FTPClient;->connect(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 109
    :catch_0
    move-exception v0

    .line 110
    .local v0, "e":Ljava/io/IOException;
    const/4 v2, 0x0

    :try_start_5
    const-string v4, "FTPConnectionHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Connect failed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 111
    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->success:Z

    .line 112
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v2}, Lorg/apache/commons/net/ftp/FTPClient;->isConnected()Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v2

    if-eqz v2, :cond_3

    .line 114
    :try_start_6
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v2}, Lorg/apache/commons/net/ftp/FTPClient;->disconnect()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_3

    .line 115
    :catch_1
    move-exception v2

    goto :goto_3

    .line 99
    .end local v0    # "e":Ljava/io/IOException;
    :cond_5
    :try_start_7
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v2}, Lorg/apache/commons/net/ftp/FTPClient;->enterLocalActiveMode()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_2

    .line 117
    :catch_2
    move-exception v0

    .line 119
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    :try_start_8
    const-string v4, "FTPConnectionHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "EXCEPTION: syncConnect failed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 121
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->recreateFtpClient()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_3

    .line 107
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "reply":I
    :cond_6
    const/4 v2, 0x1

    :try_start_9
    sput-boolean v2, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->success:Z
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_3
.end method

.method public syncConnectAndLogIn()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 178
    sget-boolean v1, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mAbortBrowsing:Z

    if-eqz v1, :cond_1

    .line 179
    const/4 v1, 0x2

    const-string v2, "ftp"

    const-string v3, "mAbortBrowsing ture syncConnectAndLogIn  RETURN "

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 186
    :cond_0
    :goto_0
    return v0

    .line 183
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->syncConnect()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 184
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->syncLogIn()Z

    move-result v0

    goto :goto_0
.end method

.method public syncCreateFolder(Ljava/lang/String;)Z
    .locals 7
    .param p1, "newDirPath"    # Ljava/lang/String;

    .prologue
    .line 548
    const/4 v2, 0x2

    const-string v3, "FTPConnectionHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "syncCreateFolder dir: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 549
    const/4 v1, 0x0

    .line 551
    .local v1, "success":Z
    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mSync:Ljava/lang/Object;

    monitor-enter v3

    .line 552
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->isReady()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 554
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v2, p1}, Lorg/apache/commons/net/ftp/FTPClient;->makeDirectory(Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    .line 567
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 569
    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClientOperationListener:Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;

    if-eqz v2, :cond_1

    .line 571
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClientOperationListener:Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-interface {v2, v3, p1}, Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;->notifyCreateFolder(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;)V

    .line 575
    :cond_1
    return v1

    .line 555
    :catch_0
    move-exception v0

    .line 556
    .local v0, "e":Ljava/io/IOException;
    const/4 v2, 0x0

    :try_start_3
    const-string v4, "FTPConnectionHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "syncCreateFolder IOException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 557
    const/4 v1, 0x0

    .line 558
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->disconnect()V

    goto :goto_0

    .line 567
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2

    .line 559
    :catch_1
    move-exception v0

    .line 561
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    :try_start_4
    const-string v4, "FTPConnectionHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "EXCEPTION: syncCreateFolder failed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 563
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->recreateFtpClient()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public syncDeleteFile(Ljava/lang/String;)Z
    .locals 7
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 664
    const/4 v2, 0x2

    const-string v3, "FTPConnectionHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "syncDeleteFile: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 665
    const/4 v1, 0x0

    .line 667
    .local v1, "success":Z
    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mSync:Ljava/lang/Object;

    monitor-enter v3

    .line 668
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->isReady()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 670
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v2, p1}, Lorg/apache/commons/net/ftp/FTPClient;->deleteFile(Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    .line 683
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 685
    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClientOperationListener:Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;

    if-eqz v2, :cond_1

    .line 687
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClientOperationListener:Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-interface {v2, v3, p1}, Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;->notifyDeleteFile(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;)V

    .line 691
    :cond_1
    return v1

    .line 671
    :catch_0
    move-exception v0

    .line 672
    .local v0, "e":Ljava/io/IOException;
    const/4 v2, 0x0

    :try_start_3
    const-string v4, "FTPConnectionHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "syncDeleteFile IOException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 673
    const/4 v1, 0x0

    .line 674
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->disconnect()V

    goto :goto_0

    .line 683
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2

    .line 675
    :catch_1
    move-exception v0

    .line 677
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    :try_start_4
    const-string v4, "FTPConnectionHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "EXCEPTION: syncDeleteFile failed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 679
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->recreateFtpClient()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public syncDeleteFiles([Ljava/lang/String;)I
    .locals 12
    .param p1, "paths"    # [Ljava/lang/String;

    .prologue
    const/4 v9, 0x2

    .line 580
    const-string v7, "FTPConnectionHandler"

    const-string v8, "syncDeleteFiles"

    invoke-static {v9, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 581
    const/4 v5, 0x0

    .line 583
    .local v5, "succeeded":I
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 585
    .local v6, "succeededPaths":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    iget-object v8, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mSync:Ljava/lang/Object;

    monitor-enter v8

    .line 586
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->isReady()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    if-eqz v7, :cond_1

    .line 588
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    :try_start_1
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    .line 589
    .local v4, "path":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v7, v4}, Lorg/apache/commons/net/ftp/FTPClient;->deleteFile(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 590
    invoke-virtual {v6, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 591
    add-int/lit8 v5, v5, 0x1

    .line 588
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 593
    :cond_0
    const/4 v7, 0x2

    const-string v9, "FTPConnectionHandler"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "syncDeleteFiles failed deletion of: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v9, v10}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 597
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "path":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 598
    .local v1, "e":Ljava/io/IOException;
    const/4 v7, 0x0

    :try_start_2
    const-string v9, "FTPConnectionHandler"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "syncDeleteFiles IOException: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v9, v10}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 599
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->disconnect()V

    .line 608
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "e":Ljava/io/IOException;
    :cond_1
    :goto_2
    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 610
    iget-object v7, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClientOperationListener:Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;

    if-eqz v7, :cond_2

    .line 612
    iget-object v7, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClientOperationListener:Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-interface {v7, v8, v6}, Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;->notifyDeleteFiles(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/util/Collection;)V

    .line 616
    :cond_2
    return v5

    .line 600
    .restart local v0    # "arr$":[Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 602
    .local v1, "e":Ljava/lang/Exception;
    const/4 v7, 0x0

    :try_start_3
    const-string v9, "FTPConnectionHandler"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "EXCEPTION: syncDeleteFiles failed: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v9, v10}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 604
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->recreateFtpClient()V

    goto :goto_2

    .line 608
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v7
.end method

.method public syncDeleteFolder(Ljava/lang/String;)Z
    .locals 7
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 696
    const/4 v2, 0x2

    const-string v3, "FTPConnectionHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "syncDeleteFolder: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 697
    const/4 v1, 0x0

    .line 699
    .local v1, "success":Z
    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mSync:Ljava/lang/Object;

    monitor-enter v3

    .line 700
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->isReady()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 702
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v2, p1}, Lorg/apache/commons/net/ftp/FTPClient;->removeDirectory(Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    .line 715
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 717
    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClientOperationListener:Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;

    if-eqz v2, :cond_1

    .line 719
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClientOperationListener:Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-interface {v2, v3, p1}, Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;->notifyDeleteFolder(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;)V

    .line 723
    :cond_1
    return v1

    .line 703
    :catch_0
    move-exception v0

    .line 704
    .local v0, "e":Ljava/io/IOException;
    const/4 v2, 0x0

    :try_start_3
    const-string v4, "FTPConnectionHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "syncDeleteFolder IOException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 705
    const/4 v1, 0x0

    .line 706
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->disconnect()V

    goto :goto_0

    .line 715
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2

    .line 707
    :catch_1
    move-exception v0

    .line 709
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    :try_start_4
    const-string v4, "FTPConnectionHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "EXCEPTION: syncDeleteFolder failed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 711
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->recreateFtpClient()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public syncDeleteFolders([Ljava/lang/String;)I
    .locals 12
    .param p1, "paths"    # [Ljava/lang/String;

    .prologue
    const/4 v9, 0x2

    .line 622
    const-string v7, "FTPConnectionHandler"

    const-string v8, "syncDeleteFolders"

    invoke-static {v9, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 623
    const/4 v5, 0x0

    .line 625
    .local v5, "succeeded":I
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 627
    .local v6, "succeededPaths":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    iget-object v8, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mSync:Ljava/lang/Object;

    monitor-enter v8

    .line 628
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->isReady()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    if-eqz v7, :cond_1

    .line 630
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    :try_start_1
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    .line 631
    .local v4, "path":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v7, v4}, Lorg/apache/commons/net/ftp/FTPClient;->removeDirectory(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 632
    invoke-virtual {v6, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 633
    add-int/lit8 v5, v5, 0x1

    .line 630
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 635
    :cond_0
    const/4 v7, 0x2

    const-string v9, "FTPConnectionHandler"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "syncDeleteFolders failed deletion of: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v9, v10}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 639
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "path":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 640
    .local v1, "e":Ljava/io/IOException;
    const/4 v7, 0x0

    :try_start_2
    const-string v9, "FTPConnectionHandler"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "syncDeleteFolders IOException: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v9, v10}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 641
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->disconnect()V

    .line 650
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "e":Ljava/io/IOException;
    :cond_1
    :goto_2
    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 652
    iget-object v7, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClientOperationListener:Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;

    if-eqz v7, :cond_2

    .line 654
    iget-object v7, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClientOperationListener:Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-interface {v7, v8, v6}, Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;->notifyDeleteFolders(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/util/Collection;)V

    .line 658
    :cond_2
    return v5

    .line 642
    .restart local v0    # "arr$":[Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 644
    .local v1, "e":Ljava/lang/Exception;
    const/4 v7, 0x0

    :try_start_3
    const-string v9, "FTPConnectionHandler"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "EXCEPTION: syncDeleteFolders failed: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v9, v10}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 646
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->recreateFtpClient()V

    goto :goto_2

    .line 650
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v7
.end method

.method public syncDisconnect()Z
    .locals 7

    .prologue
    const/4 v4, 0x2

    .line 230
    sget-boolean v2, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mAbortBrowsing:Z

    if-eqz v2, :cond_0

    .line 231
    const-string v2, "ftp"

    const-string v3, "mAbortBrowsing ture syncDisconnect !!!"

    invoke-static {v4, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 233
    :cond_0
    const-string v2, "FTPConnectionHandler"

    const-string v3, "syncDisconnect"

    invoke-static {v4, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 234
    const/4 v1, 0x1

    .line 236
    .local v1, "success":Z
    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mSync:Ljava/lang/Object;

    monitor-enter v3

    .line 238
    :try_start_0
    sget-boolean v2, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mAbortBrowsing:Z

    if-eqz v2, :cond_1

    .line 239
    const/4 v2, 0x2

    const-string v4, "ftp"

    const-string v5, "mAbortBrowsing ture mSync syncDisconnect !!!"

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 243
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v2}, Lorg/apache/commons/net/ftp/FTPClient;->isConnected()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_2

    .line 245
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v2}, Lorg/apache/commons/net/ftp/FTPClient;->disconnect()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 258
    :cond_2
    :goto_0
    :try_start_2
    monitor-exit v3

    .line 260
    return v1

    .line 246
    :catch_0
    move-exception v0

    .line 247
    .local v0, "e":Ljava/io/IOException;
    const/4 v2, 0x0

    const-string v4, "FTPConnectionHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Disconnect failed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 248
    const/4 v1, 0x0

    .line 249
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->disconnect()V

    goto :goto_0

    .line 258
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 250
    :catch_1
    move-exception v0

    .line 252
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    :try_start_3
    const-string v4, "FTPConnectionHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "EXCEPTION: syncDisconnect failed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 254
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->recreateFtpClient()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public syncDownloadFileToLocal(Lcom/sec/android/app/myfiles/ftp/IFTPItem;Ljava/lang/String;Lcom/sec/android/app/myfiles/openoperation/IProgressUpdatable;)Z
    .locals 20
    .param p1, "file"    # Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .param p2, "targetDirectory"    # Ljava/lang/String;
    .param p3, "update"    # Lcom/sec/android/app/myfiles/openoperation/IProgressUpdatable;

    .prologue
    .line 452
    const/4 v13, 0x2

    const-string v14, "FTPConnectionHandler"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "syncDownloadFileToLocal targetDirectory: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 453
    const/4 v10, 0x0

    .line 455
    .local v10, "success":Z
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mSync:Ljava/lang/Object;

    monitor-enter v14

    .line 456
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->isReady()Z

    move-result v13

    if-eqz v13, :cond_c

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mAbortTransferring:Z

    if-nez v13, :cond_c

    .line 457
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mTransferring:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 458
    const/4 v2, 0x0

    .line 460
    .local v2, "abort":Z
    :try_start_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    const/4 v15, 0x2

    invoke-virtual {v13, v15}, Lorg/apache/commons/net/ftp/FTPClient;->setFileType(I)Z

    .line 461
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    const/4 v15, 0x1

    invoke-virtual {v13, v15}, Lorg/apache/commons/net/ftp/FTPClient;->setKeepAlive(Z)V

    .line 462
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    const-wide/16 v16, 0x3c

    move-wide/from16 v0, v16

    invoke-virtual {v13, v0, v1}, Lorg/apache/commons/net/ftp/FTPClient;->setControlKeepAliveTimeout(J)V

    .line 463
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    const/high16 v15, 0x10000

    invoke-virtual {v13, v15}, Lorg/apache/commons/net/ftp/FTPClient;->setBufferSize(I)V

    .line 464
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-interface/range {p1 .. p1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v13, v15}, Lorg/apache/commons/net/ftp/FTPClient;->retrieveFileStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadIs:Ljava/io/InputStream;

    .line 465
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadIs:Ljava/io/InputStream;

    if-nez v13, :cond_4

    .line 466
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mAbortTransferring:Z

    if-eqz v13, :cond_0

    .line 467
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mAbortTransferring:Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 469
    :cond_0
    const/4 v13, 0x0

    .line 505
    :try_start_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadOs:Ljava/io/OutputStream;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v15, :cond_1

    .line 507
    :try_start_3
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadOs:Ljava/io/OutputStream;

    invoke-virtual {v15}, Ljava/io/OutputStream;->flush()V

    .line 508
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadOs:Ljava/io/OutputStream;

    invoke-virtual {v15}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 513
    :cond_1
    :goto_0
    :try_start_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadIs:Ljava/io/InputStream;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v15, :cond_2

    .line 515
    :try_start_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadIs:Ljava/io/InputStream;

    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 520
    :cond_2
    :goto_1
    if-eqz v2, :cond_3

    .line 521
    :try_start_6
    new-instance v15, Ljava/io/File;

    move-object/from16 v0, p2

    invoke-direct {v15, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15}, Ljava/io/File;->delete()Z

    :cond_3
    monitor-exit v14

    .line 543
    .end local v2    # "abort":Z
    :goto_2
    return v13

    .line 509
    .restart local v2    # "abort":Z
    :catch_0
    move-exception v5

    .line 510
    .local v5, "e":Ljava/io/IOException;
    const/4 v15, 0x0

    const-string v16, "FTPConnectionHandler"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "syncDownloadFileToLocal IOException: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 539
    .end local v2    # "abort":Z
    .end local v5    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v13

    monitor-exit v14
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v13

    .line 516
    .restart local v2    # "abort":Z
    :catch_1
    move-exception v5

    .line 517
    .restart local v5    # "e":Ljava/io/IOException;
    const/4 v15, 0x0

    :try_start_7
    const-string v16, "FTPConnectionHandler"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "syncDownloadFileToLocal IOException: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    .line 471
    .end local v5    # "e":Ljava/io/IOException;
    :cond_4
    :try_start_8
    new-instance v11, Ljava/io/File;

    move-object/from16 v0, p2

    invoke-direct {v11, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 472
    .local v11, "target":Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v6

    .line 473
    .local v6, "parent":Ljava/io/File;
    if-eqz v6, :cond_5

    .line 474
    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->makeDirectoryOnLocal(Ljava/lang/String;)Z

    .line 476
    :cond_5
    new-instance v13, Ljava/io/BufferedOutputStream;

    new-instance v15, Ljava/io/FileOutputStream;

    move-object/from16 v0, p2

    invoke-direct {v15, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v13, v15}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadOs:Ljava/io/OutputStream;

    .line 477
    const/high16 v13, 0x10000

    new-array v3, v13, [B

    .line 478
    .local v3, "buff":[B
    const-wide/16 v8, 0x0

    .line 479
    .local v8, "readTotal":J
    const/4 v4, -0x1

    .local v4, "bytesRead":I
    const/4 v7, -0x1

    .local v7, "previousUpdate":I
    const/4 v12, -0x1

    .line 480
    .local v12, "toUpdate":I
    :cond_6
    :goto_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadIs:Ljava/io/InputStream;

    invoke-virtual {v13, v3}, Ljava/io/InputStream;->read([B)I

    move-result v4

    const/4 v13, -0x1

    if-eq v4, v13, :cond_7

    .line 481
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mTransferring:Z
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_7
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    if-nez v13, :cond_e

    .line 482
    const/4 v2, 0x1

    .line 505
    :cond_7
    :try_start_9
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadOs:Ljava/io/OutputStream;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    if-eqz v13, :cond_8

    .line 507
    :try_start_a
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadOs:Ljava/io/OutputStream;

    invoke-virtual {v13}, Ljava/io/OutputStream;->flush()V

    .line 508
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadOs:Ljava/io/OutputStream;

    invoke-virtual {v13}, Ljava/io/OutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 513
    :cond_8
    :goto_4
    :try_start_b
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadIs:Ljava/io/InputStream;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    if-eqz v13, :cond_9

    .line 515
    :try_start_c
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadIs:Ljava/io/InputStream;

    invoke-virtual {v13}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_4
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 520
    :cond_9
    :goto_5
    if-eqz v2, :cond_a

    .line 521
    :try_start_d
    new-instance v13, Ljava/io/File;

    move-object/from16 v0, p2

    invoke-direct {v13, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Ljava/io/File;->delete()Z
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 525
    .end local v3    # "buff":[B
    .end local v4    # "bytesRead":I
    .end local v6    # "parent":Ljava/io/File;
    .end local v7    # "previousUpdate":I
    .end local v8    # "readTotal":J
    .end local v11    # "target":Ljava/io/File;
    .end local v12    # "toUpdate":I
    :cond_a
    :goto_6
    :try_start_e
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v13}, Lorg/apache/commons/net/ftp/FTPClient;->completePendingCommand()Z
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_c
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_d
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    move-result v10

    .line 536
    :goto_7
    if-eqz v2, :cond_b

    const/4 v10, 0x0

    .line 537
    :cond_b
    const/4 v13, 0x0

    :try_start_f
    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mTransferring:Z

    .line 539
    .end local v2    # "abort":Z
    :cond_c
    monitor-exit v14
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 540
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mAbortTransferring:Z

    if-eqz v13, :cond_d

    .line 541
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mAbortTransferring:Z

    :cond_d
    move v13, v10

    .line 543
    goto/16 :goto_2

    .line 485
    .restart local v2    # "abort":Z
    .restart local v3    # "buff":[B
    .restart local v4    # "bytesRead":I
    .restart local v6    # "parent":Ljava/io/File;
    .restart local v7    # "previousUpdate":I
    .restart local v8    # "readTotal":J
    .restart local v11    # "target":Ljava/io/File;
    .restart local v12    # "toUpdate":I
    :cond_e
    :try_start_10
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadOs:Ljava/io/OutputStream;

    const/4 v15, 0x0

    invoke-virtual {v13, v3, v15, v4}, Ljava/io/OutputStream;->write([BII)V

    .line 486
    int-to-long v0, v4

    move-wide/from16 v16, v0

    add-long v8, v8, v16

    .line 487
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getSize()J

    move-result-wide v16

    const-wide/16 v18, 0x0

    cmp-long v13, v16, v18

    if-lez v13, :cond_f

    .line 488
    const-wide/16 v16, 0x64

    mul-long v16, v16, v8

    invoke-interface/range {p1 .. p1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getSize()J

    move-result-wide v18

    div-long v16, v16, v18

    move-wide/from16 v0, v16

    long-to-int v12, v0

    .line 490
    :cond_f
    if-le v12, v7, :cond_6

    .line 491
    move v7, v12

    .line 492
    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Lcom/sec/android/app/myfiles/openoperation/IProgressUpdatable;->updateProgress(I)V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_2
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_7
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    goto/16 :goto_3

    .line 495
    .end local v3    # "buff":[B
    .end local v4    # "bytesRead":I
    .end local v6    # "parent":Ljava/io/File;
    .end local v7    # "previousUpdate":I
    .end local v8    # "readTotal":J
    .end local v11    # "target":Ljava/io/File;
    .end local v12    # "toUpdate":I
    :catch_2
    move-exception v5

    .line 496
    .restart local v5    # "e":Ljava/io/IOException;
    const/4 v13, 0x0

    :try_start_11
    const-string v15, "FTPConnectionHandler"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "syncDownloadFileToLocal download failed: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v13, v15, v0}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    .line 497
    const/4 v10, 0x0

    .line 505
    :try_start_12
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadOs:Ljava/io/OutputStream;
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    if-eqz v13, :cond_10

    .line 507
    :try_start_13
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadOs:Ljava/io/OutputStream;

    invoke-virtual {v13}, Ljava/io/OutputStream;->flush()V

    .line 508
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadOs:Ljava/io/OutputStream;

    invoke-virtual {v13}, Ljava/io/OutputStream;->close()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_5
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    .line 513
    :cond_10
    :goto_8
    :try_start_14
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadIs:Ljava/io/InputStream;
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    if-eqz v13, :cond_11

    .line 515
    :try_start_15
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadIs:Ljava/io/InputStream;

    invoke-virtual {v13}, Ljava/io/InputStream;->close()V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_6
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    .line 520
    :cond_11
    :goto_9
    if-eqz v2, :cond_a

    .line 521
    :try_start_16
    new-instance v13, Ljava/io/File;

    move-object/from16 v0, p2

    invoke-direct {v13, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Ljava/io/File;->delete()Z

    goto/16 :goto_6

    .line 509
    .end local v5    # "e":Ljava/io/IOException;
    .restart local v3    # "buff":[B
    .restart local v4    # "bytesRead":I
    .restart local v6    # "parent":Ljava/io/File;
    .restart local v7    # "previousUpdate":I
    .restart local v8    # "readTotal":J
    .restart local v11    # "target":Ljava/io/File;
    .restart local v12    # "toUpdate":I
    :catch_3
    move-exception v5

    .line 510
    .restart local v5    # "e":Ljava/io/IOException;
    const/4 v13, 0x0

    const-string v15, "FTPConnectionHandler"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "syncDownloadFileToLocal IOException: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v13, v15, v0}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 516
    .end local v5    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v5

    .line 517
    .restart local v5    # "e":Ljava/io/IOException;
    const/4 v13, 0x0

    const-string v15, "FTPConnectionHandler"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "syncDownloadFileToLocal IOException: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v13, v15, v0}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 509
    .end local v3    # "buff":[B
    .end local v4    # "bytesRead":I
    .end local v6    # "parent":Ljava/io/File;
    .end local v7    # "previousUpdate":I
    .end local v8    # "readTotal":J
    .end local v11    # "target":Ljava/io/File;
    .end local v12    # "toUpdate":I
    :catch_5
    move-exception v5

    .line 510
    const/4 v13, 0x0

    const-string v15, "FTPConnectionHandler"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "syncDownloadFileToLocal IOException: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v13, v15, v0}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    .line 516
    :catch_6
    move-exception v5

    .line 517
    const/4 v13, 0x0

    const-string v15, "FTPConnectionHandler"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "syncDownloadFileToLocal IOException: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v13, v15, v0}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    goto/16 :goto_9

    .line 498
    .end local v5    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v5

    .line 500
    .local v5, "e":Ljava/lang/Exception;
    const/4 v13, 0x0

    :try_start_17
    const-string v15, "FTPConnectionHandler"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "EXCEPTION: syncDownloadFileToLocal failed: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v13, v15, v0}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 502
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->recreateFtpClient()V
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_1

    .line 505
    :try_start_18
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadOs:Ljava/io/OutputStream;
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_0

    if-eqz v13, :cond_12

    .line 507
    :try_start_19
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadOs:Ljava/io/OutputStream;

    invoke-virtual {v13}, Ljava/io/OutputStream;->flush()V

    .line 508
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadOs:Ljava/io/OutputStream;

    invoke-virtual {v13}, Ljava/io/OutputStream;->close()V
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_19} :catch_8
    .catchall {:try_start_19 .. :try_end_19} :catchall_0

    .line 513
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_12
    :goto_a
    :try_start_1a
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadIs:Ljava/io/InputStream;
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_0

    if-eqz v13, :cond_13

    .line 515
    :try_start_1b
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadIs:Ljava/io/InputStream;

    invoke-virtual {v13}, Ljava/io/InputStream;->close()V
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1b} :catch_9
    .catchall {:try_start_1b .. :try_end_1b} :catchall_0

    .line 520
    :cond_13
    :goto_b
    if-eqz v2, :cond_a

    .line 521
    :try_start_1c
    new-instance v13, Ljava/io/File;

    move-object/from16 v0, p2

    invoke-direct {v13, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Ljava/io/File;->delete()Z

    goto/16 :goto_6

    .line 509
    .restart local v5    # "e":Ljava/lang/Exception;
    :catch_8
    move-exception v5

    .line 510
    .local v5, "e":Ljava/io/IOException;
    const/4 v13, 0x0

    const-string v15, "FTPConnectionHandler"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "syncDownloadFileToLocal IOException: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v13, v15, v0}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_a

    .line 516
    .end local v5    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v5

    .line 517
    .restart local v5    # "e":Ljava/io/IOException;
    const/4 v13, 0x0

    const-string v15, "FTPConnectionHandler"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "syncDownloadFileToLocal IOException: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v13, v15, v0}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_b

    .line 505
    .end local v5    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v13

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadOs:Ljava/io/OutputStream;
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_0

    if-eqz v15, :cond_14

    .line 507
    :try_start_1d
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadOs:Ljava/io/OutputStream;

    invoke-virtual {v15}, Ljava/io/OutputStream;->flush()V

    .line 508
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadOs:Ljava/io/OutputStream;

    invoke-virtual {v15}, Ljava/io/OutputStream;->close()V
    :try_end_1d
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_1d} :catch_a
    .catchall {:try_start_1d .. :try_end_1d} :catchall_0

    .line 513
    :cond_14
    :goto_c
    :try_start_1e
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadIs:Ljava/io/InputStream;
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_0

    if-eqz v15, :cond_15

    .line 515
    :try_start_1f
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadIs:Ljava/io/InputStream;

    invoke-virtual {v15}, Ljava/io/InputStream;->close()V
    :try_end_1f
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_1f} :catch_b
    .catchall {:try_start_1f .. :try_end_1f} :catchall_0

    .line 520
    :cond_15
    :goto_d
    if-eqz v2, :cond_16

    .line 521
    :try_start_20
    new-instance v15, Ljava/io/File;

    move-object/from16 v0, p2

    invoke-direct {v15, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15}, Ljava/io/File;->delete()Z

    :cond_16
    throw v13

    .line 509
    :catch_a
    move-exception v5

    .line 510
    .restart local v5    # "e":Ljava/io/IOException;
    const/4 v15, 0x0

    const-string v16, "FTPConnectionHandler"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "syncDownloadFileToLocal IOException: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_c

    .line 516
    .end local v5    # "e":Ljava/io/IOException;
    :catch_b
    move-exception v5

    .line 517
    .restart local v5    # "e":Ljava/io/IOException;
    const/4 v15, 0x0

    const-string v16, "FTPConnectionHandler"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "syncDownloadFileToLocal IOException: "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_d

    .line 526
    .end local v5    # "e":Ljava/io/IOException;
    :catch_c
    move-exception v5

    .line 527
    .restart local v5    # "e":Ljava/io/IOException;
    const/4 v13, 0x0

    const-string v15, "FTPConnectionHandler"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "syncDownloadFileToLocal IOException: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v5}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v13, v15, v0}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 528
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->disconnect()V

    goto/16 :goto_7

    .line 529
    .end local v5    # "e":Ljava/io/IOException;
    :catch_d
    move-exception v5

    .line 531
    .local v5, "e":Ljava/lang/Exception;
    const/4 v13, 0x0

    const-string v15, "FTPConnectionHandler"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "EXCEPTION: syncDownloadFileToLocal failed: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v5}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v13, v15, v0}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 533
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->recreateFtpClient()V
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_0

    goto/16 :goto_7
.end method

.method public syncFileExists(Ljava/lang/String;)Z
    .locals 9
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1087
    const-string v5, "FTPConnectionHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "syncFileExists: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 1089
    sget-boolean v5, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mAbortBrowsing:Z

    if-eqz v5, :cond_1

    .line 1091
    const/4 v4, 0x2

    const-string v5, "ftp"

    const-string v6, "mAbortBrowsing true syncFileExists RETURN !!!"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1139
    :cond_0
    :goto_0
    return v3

    .line 1097
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1099
    iget-object v5, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mSync:Ljava/lang/Object;

    monitor-enter v5

    .line 1103
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v6, p1}, Lorg/apache/commons/net/ftp/FTPClient;->getStatus(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1105
    .local v2, "status":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v6}, Lorg/apache/commons/net/ftp/FTPClient;->getReplyCode()I

    move-result v1

    .line 1109
    .local v1, "reply":I
    invoke-static {v1}, Lorg/apache/commons/net/ftp/FTPReply;->isPositiveCompletion(I)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1111
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    if-nez v6, :cond_2

    move v3, v4

    :cond_2
    :try_start_1
    monitor-exit v5

    goto :goto_0

    .line 1135
    .end local v1    # "reply":I
    .end local v2    # "status":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 1115
    .restart local v1    # "reply":I
    .restart local v2    # "status":Ljava/lang/String;
    :cond_3
    :try_start_2
    monitor-exit v5

    move v3, v4

    goto :goto_0

    .line 1119
    .end local v1    # "reply":I
    .end local v2    # "status":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1121
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->disconnect()V

    .line 1123
    monitor-exit v5

    goto :goto_0

    .line 1125
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 1127
    .local v0, "e":Ljava/lang/Exception;
    const/4 v4, 0x0

    const-string v6, "FTPConnectionHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "EXCEPTION: syncFileExists failed: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v6, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 1129
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->recreateFtpClient()V

    .line 1131
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public syncLogIn()Z
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x0

    .line 133
    sget-boolean v3, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mAbortBrowsing:Z

    if-eqz v3, :cond_0

    .line 134
    const-string v3, "ftp"

    const-string v4, "mAbortBrowsing ture syncLogIn RETURN!!! "

    invoke-static {v5, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    move v1, v2

    .line 172
    :goto_0
    return v1

    .line 139
    :cond_0
    const-string v3, "FTPConnectionHandler"

    const-string v4, "syncLogIn params"

    invoke-static {v5, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 140
    const/4 v1, 0x0

    .line 142
    .local v1, "success":Z
    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mSync:Ljava/lang/Object;

    monitor-enter v3

    .line 144
    :try_start_0
    sget-boolean v4, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mAbortBrowsing:Z

    if-eqz v4, :cond_1

    .line 145
    const/4 v4, 0x2

    const-string v5, "ftp"

    const-string v6, "mAbortBrowsing ture mSync syncLogIn  RETURN !!!"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 146
    monitor-exit v3

    move v1, v2

    goto :goto_0

    .line 151
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->isConnected()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_2

    .line 153
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getAnonymous()Z

    move-result v2

    if-nez v2, :cond_3

    .line 154
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getUsername()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getPassword()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lorg/apache/commons/net/ftp/FTPClient;->login(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    .line 170
    :cond_2
    :goto_1
    :try_start_2
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 156
    :cond_3
    :try_start_3
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    const-string v4, "anonymous"

    const-string v5, ""

    invoke-virtual {v2, v4, v5}, Lorg/apache/commons/net/ftp/FTPClient;->login(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v1

    goto :goto_1

    .line 158
    :catch_0
    move-exception v0

    .line 159
    .local v0, "e":Ljava/io/IOException;
    const/4 v2, 0x0

    :try_start_4
    const-string v4, "FTPConnectionHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "LogIn failed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 160
    const/4 v1, 0x0

    .line 161
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->disconnect()V

    goto :goto_1

    .line 162
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 164
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    const-string v4, "FTPConnectionHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "EXCEPTION: syncLogIn failed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 166
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->recreateFtpClient()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method public syncLogOut()Z
    .locals 7

    .prologue
    const/4 v4, 0x2

    .line 193
    sget-boolean v2, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mAbortBrowsing:Z

    if-eqz v2, :cond_0

    .line 194
    const-string v2, "ftp"

    const-string v3, "mAbortBrowsing ture syncLogOut !!!"

    invoke-static {v4, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 196
    :cond_0
    const-string v2, "FTPConnectionHandler"

    const-string v3, "syncLogOut"

    invoke-static {v4, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 197
    const/4 v1, 0x0

    .line 199
    .local v1, "success":Z
    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mSync:Ljava/lang/Object;

    monitor-enter v3

    .line 201
    :try_start_0
    sget-boolean v2, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mAbortBrowsing:Z

    if-eqz v2, :cond_1

    .line 202
    const/4 v2, 0x2

    const-string v4, "ftp"

    const-string v5, "mAbortBrowsing ture mSync syncLogOut !!!"

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 207
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->isConnectedAndLoggedIn()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_2

    .line 209
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v2}, Lorg/apache/commons/net/ftp/FTPClient;->logout()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    .line 222
    :cond_2
    :goto_0
    :try_start_2
    monitor-exit v3

    .line 224
    return v1

    .line 210
    :catch_0
    move-exception v0

    .line 211
    .local v0, "e":Ljava/io/IOException;
    const/4 v2, 0x0

    const-string v4, "FTPConnectionHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "LogOut failed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 212
    const/4 v1, 0x0

    .line 213
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->disconnect()V

    goto :goto_0

    .line 222
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 214
    :catch_1
    move-exception v0

    .line 216
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    :try_start_3
    const-string v4, "FTPConnectionHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "EXCEPTION: syncLogOut failed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 218
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->recreateFtpClient()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public syncLogOutAndDisconnect()Z
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 266
    sget-boolean v0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mAbortBrowsing:Z

    if-eqz v0, :cond_0

    .line 267
    const-string v0, "ftp"

    const-string v1, "mAbortBrowsing ture syncLogOutAndDisconnect !!!"

    invoke-static {v2, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 269
    :cond_0
    const-string v0, "FTPConnectionHandler"

    const-string v1, "syncLogOutAndDisconnect"

    invoke-static {v2, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 270
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->syncLogOut()Z

    .line 271
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->syncDisconnect()Z

    move-result v0

    return v0
.end method

.method public syncPrepareConnection()Z
    .locals 1

    .prologue
    .line 976
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->isReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 978
    const/4 v0, 0x1

    .line 990
    :goto_0
    return v0

    .line 982
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->syncDisconnect()Z

    .line 984
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->syncConnect()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 986
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->syncLogIn()Z

    move-result v0

    goto :goto_0

    .line 990
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public syncReconnect()Z
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 277
    sget-boolean v0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mAbortBrowsing:Z

    if-eqz v0, :cond_0

    .line 278
    const-string v0, "ftp"

    const-string v1, "mAbortBrowsing ture syncReconnect RETURN "

    invoke-static {v2, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 279
    const/4 v0, 0x0

    .line 287
    :goto_0
    return v0

    .line 283
    :cond_0
    const-string v0, "FTPConnectionHandler"

    const-string v1, "syncReconnect"

    invoke-static {v2, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 284
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 285
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->syncLogOutAndDisconnect()Z

    .line 287
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->syncConnectAndLogIn()Z

    move-result v0

    goto :goto_0
.end method

.method public syncRenameFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "from"    # Ljava/lang/String;
    .param p2, "to"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x2

    .line 728
    const-string v2, "FTPConnectionHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "syncRenameFile from: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", to: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v7, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 729
    const/4 v1, 0x0

    .line 731
    .local v1, "success":Z
    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mSync:Ljava/lang/Object;

    monitor-enter v3

    .line 732
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->isReady()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 733
    const/4 v2, 0x2

    const-string v4, "FTPConnectionHandler"

    const-string v5, "syncRenameFile Ready"

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 735
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    invoke-virtual {v2, p1, p2}, Lorg/apache/commons/net/ftp/FTPClient;->rename(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    .line 750
    :goto_0
    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 752
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClientOperationListener:Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;

    if-eqz v2, :cond_0

    .line 754
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClientOperationListener:Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-interface {v2, v3, p1, p2}, Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;->notifyRenameItem(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Ljava/lang/String;)V

    .line 757
    :cond_0
    const-string v2, "FTPConnectionHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "syncRenameFile success : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v7, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 758
    return v1

    .line 736
    :catch_0
    move-exception v0

    .line 737
    .local v0, "e":Ljava/io/IOException;
    const/4 v2, 0x0

    :try_start_3
    const-string v4, "FTPConnectionHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "syncRenameFile IOException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 738
    const/4 v1, 0x0

    .line 739
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->disconnect()V

    goto :goto_0

    .line 750
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2

    .line 740
    :catch_1
    move-exception v0

    .line 742
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    :try_start_4
    const-string v4, "FTPConnectionHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "EXCEPTION: syncRenameFile failed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 744
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->recreateFtpClient()V

    goto :goto_0

    .line 748
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const/4 v2, 0x2

    const-string v4, "FTPConnectionHandler"

    const-string v5, "syncRenameFile not Ready"

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public syncSupportsMdtm()Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1040
    sget-boolean v2, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mAbortBrowsing:Z

    if-eqz v2, :cond_1

    .line 1041
    const/4 v2, 0x2

    const-string v3, "ftp"

    const-string v4, "mAbortBrowsing ture syncSupportsMdtm RETURN !!!"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1080
    :cond_0
    :goto_0
    return v1

    .line 1046
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mSupportsMdtm:Ljava/lang/Boolean;

    if-nez v2, :cond_2

    .line 1048
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->syncPrepareConnection()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1050
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mSync:Ljava/lang/Object;

    monitor-enter v2

    .line 1054
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    const-string v4, "MdTm"

    invoke-virtual {v3, v4}, Lorg/apache/commons/net/ftp/FTPClient;->hasFeature(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mSupportsMdtm:Ljava/lang/Boolean;

    .line 1056
    const/4 v3, 0x0

    const-string v4, "FTPConnectionHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "FTP server supports MDTM?: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mSupportsMdtm:Ljava/lang/Boolean;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1074
    :goto_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1080
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mSupportsMdtm:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mSupportsMdtm:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_0

    .line 1058
    :catch_0
    move-exception v0

    .line 1060
    .local v0, "e":Ljava/io/IOException;
    const/4 v3, 0x0

    :try_start_2
    const-string v4, "FTPConnectionHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "syncSypportsMdTm failed IOException: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 1062
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mSupportsMdtm:Ljava/lang/Boolean;

    .line 1064
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->disconnect()V

    goto :goto_1

    .line 1074
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 1066
    :catch_1
    move-exception v0

    .line 1068
    .local v0, "e":Ljava/lang/Exception;
    const/4 v3, 0x0

    :try_start_3
    const-string v4, "FTPConnectionHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "EXCEPTION: syncSupportsMdtm failed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 1070
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->recreateFtpClient()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public syncUploadFileFromLocal(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/openoperation/IProgressUpdatable;)Z
    .locals 26
    .param p1, "from"    # Ljava/lang/String;
    .param p2, "to"    # Ljava/lang/String;
    .param p3, "update"    # Lcom/sec/android/app/myfiles/openoperation/IProgressUpdatable;

    .prologue
    .line 321
    const/16 v20, 0x2

    const-string v21, "FTPConnectionHandler"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "syncUploadFileFromLocal from: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ", to: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 322
    const/16 v18, 0x0

    .line 323
    .local v18, "success":Z
    const/4 v7, 0x0

    .line 325
    .local v7, "deleteTarget":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mSync:Ljava/lang/Object;

    move-object/from16 v21, v0

    monitor-enter v21

    .line 326
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->isReady()Z

    move-result v20

    if-eqz v20, :cond_7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mAbortTransferring:Z

    move/from16 v20, v0

    if-nez v20, :cond_7

    .line 327
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mTransferring:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 328
    const/4 v9, 0x0

    .line 329
    .local v9, "is":Ljava/io/FileInputStream;
    const/4 v11, 0x0

    .line 330
    .local v11, "os":Ljava/io/OutputStream;
    const/4 v4, 0x0

    .line 332
    .local v4, "abort":Z
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    move-object/from16 v20, v0

    const/16 v22, 0x2

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/FTPClient;->setFileType(I)Z

    .line 333
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    move-object/from16 v20, v0

    const/16 v22, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/FTPClient;->setKeepAlive(Z)V

    .line 334
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    move-object/from16 v20, v0

    const-wide/16 v22, 0x3c

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lorg/apache/commons/net/ftp/FTPClient;->setControlKeepAliveTimeout(J)V

    .line 335
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    move-object/from16 v20, v0

    const/high16 v22, 0x10000

    move-object/from16 v0, v20

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/FTPClient;->setBufferSize(I)V

    .line 336
    const/high16 v20, 0x10000

    move/from16 v0, v20

    new-array v5, v0, [B

    .line 337
    .local v5, "buff":[B
    const-wide/16 v14, 0x0

    .line 338
    .local v14, "readTotal":J
    const/4 v6, -0x1

    .local v6, "bytesRead":I
    const/4 v12, -0x1

    .local v12, "previousUpdate":I
    const/16 v19, -0x1

    .line 339
    .local v19, "toUpdate":I
    new-instance v13, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v13, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 340
    .local v13, "srcFile":Ljava/io/File;
    invoke-virtual {v13}, Ljava/io/File;->length()J

    move-result-wide v16

    .line 341
    .local v16, "size":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/FTPClient;->storeFileStream(Ljava/lang/String;)Ljava/io/OutputStream;

    move-result-object v11

    .line 342
    if-eqz v11, :cond_2

    .line 343
    new-instance v10, Ljava/io/FileInputStream;

    invoke-direct {v10, v13}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_12
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 344
    .end local v9    # "is":Ljava/io/FileInputStream;
    .local v10, "is":Ljava/io/FileInputStream;
    :cond_0
    :goto_0
    :try_start_2
    invoke-virtual {v10, v5}, Ljava/io/FileInputStream;->read([B)I

    move-result v6

    const/16 v20, -0x1

    move/from16 v0, v20

    if-eq v6, v0, :cond_12

    .line 345
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mTransferring:Z

    move/from16 v20, v0
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_11
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_10
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    if-eqz v20, :cond_1

    if-nez v11, :cond_b

    .line 346
    :cond_1
    const/4 v4, 0x1

    move-object v9, v10

    .line 378
    .end local v10    # "is":Ljava/io/FileInputStream;
    .restart local v9    # "is":Ljava/io/FileInputStream;
    :cond_2
    :goto_1
    if-eqz v9, :cond_3

    .line 380
    :try_start_3
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 386
    :cond_3
    :goto_2
    if-eqz v11, :cond_4

    .line 388
    :try_start_4
    invoke-virtual {v11}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 395
    .end local v5    # "buff":[B
    .end local v6    # "bytesRead":I
    .end local v12    # "previousUpdate":I
    .end local v13    # "srcFile":Ljava/io/File;
    .end local v14    # "readTotal":J
    .end local v16    # "size":J
    .end local v19    # "toUpdate":I
    :cond_4
    :goto_3
    if-eqz v4, :cond_5

    .line 397
    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lorg/apache/commons/net/ftp/FTPClient;->deleteFile(Ljava/lang/String;)Z
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_d
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 406
    :cond_5
    :goto_4
    if-eqz v11, :cond_11

    .line 407
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lorg/apache/commons/net/ftp/FTPClient;->completePendingCommand()Z
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_e
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_f
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result v18

    .line 422
    :goto_5
    if-eqz v4, :cond_6

    const/16 v18, 0x0

    .line 423
    :cond_6
    const/16 v20, 0x0

    :try_start_7
    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mTransferring:Z

    .line 425
    .end local v4    # "abort":Z
    .end local v9    # "is":Ljava/io/FileInputStream;
    .end local v11    # "os":Ljava/io/OutputStream;
    :cond_7
    monitor-exit v21
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 427
    if-eqz v7, :cond_8

    .line 429
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->syncPrepareConnection()Z

    move-result v20

    if-eqz v20, :cond_8

    .line 431
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->syncDeleteFile(Ljava/lang/String;)Z

    .line 437
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mAbortTransferring:Z

    move/from16 v20, v0

    if-eqz v20, :cond_9

    .line 438
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mAbortTransferring:Z

    .line 441
    :cond_9
    if-eqz v18, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClientOperationListener:Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;

    move-object/from16 v20, v0

    if-eqz v20, :cond_a

    .line 443
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClientOperationListener:Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;->notifyUploadFile(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    :cond_a
    return v18

    .line 349
    .restart local v4    # "abort":Z
    .restart local v5    # "buff":[B
    .restart local v6    # "bytesRead":I
    .restart local v10    # "is":Ljava/io/FileInputStream;
    .restart local v11    # "os":Ljava/io/OutputStream;
    .restart local v12    # "previousUpdate":I
    .restart local v13    # "srcFile":Ljava/io/File;
    .restart local v14    # "readTotal":J
    .restart local v16    # "size":J
    .restart local v19    # "toUpdate":I
    :cond_b
    const/16 v20, 0x0

    :try_start_8
    move/from16 v0, v20

    invoke-virtual {v11, v5, v0, v6}, Ljava/io/OutputStream;->write([BII)V

    .line 350
    int-to-long v0, v6

    move-wide/from16 v22, v0

    add-long v14, v14, v22

    .line 351
    const-wide/16 v22, 0x64

    mul-long v22, v22, v14

    div-long v22, v22, v16

    move-wide/from16 v0, v22

    long-to-int v0, v0

    move/from16 v19, v0

    .line 352
    move/from16 v0, v19

    if-le v0, v12, :cond_0

    .line 353
    move/from16 v12, v19

    .line 354
    move-object/from16 v0, p3

    move/from16 v1, v19

    invoke-interface {v0, v1}, Lcom/sec/android/app/myfiles/openoperation/IProgressUpdatable;->updateProgress(I)V
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_11
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_10
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto/16 :goto_0

    .line 358
    :catch_0
    move-exception v8

    move-object v9, v10

    .line 359
    .end local v5    # "buff":[B
    .end local v6    # "bytesRead":I
    .end local v10    # "is":Ljava/io/FileInputStream;
    .end local v12    # "previousUpdate":I
    .end local v13    # "srcFile":Ljava/io/File;
    .end local v14    # "readTotal":J
    .end local v16    # "size":J
    .end local v19    # "toUpdate":I
    .local v8, "e":Ljava/io/FileNotFoundException;
    .restart local v9    # "is":Ljava/io/FileInputStream;
    :goto_6
    const/16 v20, 0x0

    :try_start_9
    const-string v22, "FTPConnectionHandler"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "syncUploadFileFromLocal FileNotFound: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v8}, Ljava/io/FileNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move/from16 v0, v20

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 360
    const/16 v18, 0x0

    .line 361
    invoke-virtual {v8}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 378
    if-eqz v9, :cond_c

    .line 380
    :try_start_a
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 386
    .end local v8    # "e":Ljava/io/FileNotFoundException;
    :cond_c
    :goto_7
    if-eqz v11, :cond_4

    .line 388
    :try_start_b
    invoke-virtual {v11}, Ljava/io/OutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_3

    .line 389
    :catch_1
    move-exception v8

    .line 390
    .local v8, "e":Ljava/io/IOException;
    const/16 v20, 0x0

    :try_start_c
    const-string v22, "FTPConnectionHandler"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "syncUploadFileFromLocal IOException: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move/from16 v0, v20

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 391
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 425
    .end local v4    # "abort":Z
    .end local v8    # "e":Ljava/io/IOException;
    .end local v9    # "is":Ljava/io/FileInputStream;
    .end local v11    # "os":Ljava/io/OutputStream;
    :catchall_0
    move-exception v20

    monitor-exit v21
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    throw v20

    .line 381
    .restart local v4    # "abort":Z
    .restart local v5    # "buff":[B
    .restart local v6    # "bytesRead":I
    .restart local v9    # "is":Ljava/io/FileInputStream;
    .restart local v11    # "os":Ljava/io/OutputStream;
    .restart local v12    # "previousUpdate":I
    .restart local v13    # "srcFile":Ljava/io/File;
    .restart local v14    # "readTotal":J
    .restart local v16    # "size":J
    .restart local v19    # "toUpdate":I
    :catch_2
    move-exception v8

    .line 382
    .restart local v8    # "e":Ljava/io/IOException;
    const/16 v20, 0x0

    :try_start_d
    const-string v22, "FTPConnectionHandler"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "syncUploadFileFromLocal IOException: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move/from16 v0, v20

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 383
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 389
    .end local v8    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v8

    .line 390
    .restart local v8    # "e":Ljava/io/IOException;
    const/16 v20, 0x0

    const-string v22, "FTPConnectionHandler"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "syncUploadFileFromLocal IOException: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move/from16 v0, v20

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 391
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 381
    .end local v5    # "buff":[B
    .end local v6    # "bytesRead":I
    .end local v12    # "previousUpdate":I
    .end local v13    # "srcFile":Ljava/io/File;
    .end local v14    # "readTotal":J
    .end local v16    # "size":J
    .end local v19    # "toUpdate":I
    .local v8, "e":Ljava/io/FileNotFoundException;
    :catch_4
    move-exception v8

    .line 382
    .local v8, "e":Ljava/io/IOException;
    const/16 v20, 0x0

    const-string v22, "FTPConnectionHandler"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "syncUploadFileFromLocal IOException: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move/from16 v0, v20

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 383
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto/16 :goto_7

    .line 362
    .end local v8    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v8

    .line 363
    .restart local v8    # "e":Ljava/io/IOException;
    :goto_8
    const/4 v7, 0x1

    .line 364
    const/16 v20, 0x0

    :try_start_e
    const-string v22, "FTPConnectionHandler"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "syncUploadFileFromLocal IOException: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move/from16 v0, v20

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 365
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 378
    if-eqz v9, :cond_d

    .line 380
    :try_start_f
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_7
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 386
    :cond_d
    :goto_9
    if-eqz v11, :cond_4

    .line 388
    :try_start_10
    invoke-virtual {v11}, Ljava/io/OutputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_6
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    goto/16 :goto_3

    .line 389
    :catch_6
    move-exception v8

    .line 390
    const/16 v20, 0x0

    :try_start_11
    const-string v22, "FTPConnectionHandler"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "syncUploadFileFromLocal IOException: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move/from16 v0, v20

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 391
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 381
    :catch_7
    move-exception v8

    .line 382
    const/16 v20, 0x0

    const-string v22, "FTPConnectionHandler"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "syncUploadFileFromLocal IOException: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move/from16 v0, v20

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 383
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    goto :goto_9

    .line 366
    .end local v8    # "e":Ljava/io/IOException;
    :catch_8
    move-exception v8

    .line 368
    .local v8, "e":Ljava/lang/Exception;
    :goto_a
    const/4 v7, 0x1

    .line 370
    const/16 v20, 0x0

    :try_start_12
    const-string v22, "FTPConnectionHandler"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "EXCEPTION: syncUploadFileFromLocal failed: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move/from16 v0, v20

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 372
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->recreateFtpClient()V

    .line 374
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    .line 378
    if-eqz v9, :cond_e

    .line 380
    :try_start_13
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_a
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    .line 386
    .end local v8    # "e":Ljava/lang/Exception;
    :cond_e
    :goto_b
    if-eqz v11, :cond_4

    .line 388
    :try_start_14
    invoke-virtual {v11}, Ljava/io/OutputStream;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_9
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    goto/16 :goto_3

    .line 389
    :catch_9
    move-exception v8

    .line 390
    .local v8, "e":Ljava/io/IOException;
    const/16 v20, 0x0

    :try_start_15
    const-string v22, "FTPConnectionHandler"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "syncUploadFileFromLocal IOException: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move/from16 v0, v20

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 391
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 381
    .local v8, "e":Ljava/lang/Exception;
    :catch_a
    move-exception v8

    .line 382
    .local v8, "e":Ljava/io/IOException;
    const/16 v20, 0x0

    const-string v22, "FTPConnectionHandler"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "syncUploadFileFromLocal IOException: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move/from16 v0, v20

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 383
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    goto :goto_b

    .line 378
    .end local v8    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v20

    :goto_c
    if-eqz v9, :cond_f

    .line 380
    :try_start_16
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_b
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    .line 386
    :cond_f
    :goto_d
    if-eqz v11, :cond_10

    .line 388
    :try_start_17
    invoke-virtual {v11}, Ljava/io/OutputStream;->close()V
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_c
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    .line 392
    :cond_10
    :goto_e
    :try_start_18
    throw v20

    .line 381
    :catch_b
    move-exception v8

    .line 382
    .restart local v8    # "e":Ljava/io/IOException;
    const/16 v22, 0x0

    const-string v23, "FTPConnectionHandler"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "syncUploadFileFromLocal IOException: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v22 .. v24}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 383
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_d

    .line 389
    .end local v8    # "e":Ljava/io/IOException;
    :catch_c
    move-exception v8

    .line 390
    .restart local v8    # "e":Ljava/io/IOException;
    const/16 v22, 0x0

    const-string v23, "FTPConnectionHandler"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "syncUploadFileFromLocal IOException: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v22 .. v24}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 391
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_e

    .line 398
    .end local v8    # "e":Ljava/io/IOException;
    :catch_d
    move-exception v8

    .line 399
    .restart local v8    # "e":Ljava/io/IOException;
    const/16 v20, 0x0

    const-string v22, "FTPConnectionHandler"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "syncUploadFileFromLocal IOException: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move/from16 v0, v20

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 400
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_4

    .line 409
    .end local v8    # "e":Ljava/io/IOException;
    :cond_11
    const/16 v18, 0x0

    goto/16 :goto_5

    .line 411
    :catch_e
    move-exception v8

    .line 412
    .restart local v8    # "e":Ljava/io/IOException;
    const/16 v20, 0x0

    const-string v22, "FTPConnectionHandler"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "syncUploadFileFromLocal IOException: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move/from16 v0, v20

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 413
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->disconnect()V

    .line 414
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_5

    .line 415
    .end local v8    # "e":Ljava/io/IOException;
    :catch_f
    move-exception v8

    .line 417
    .local v8, "e":Ljava/lang/Exception;
    const/16 v20, 0x0

    const-string v22, "FTPConnectionHandler"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "EXCEPTION: syncUploadFileFromLocal on completePendingCommand failed: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move/from16 v0, v20

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 418
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->recreateFtpClient()V

    .line 419
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_0

    goto/16 :goto_5

    .line 378
    .end local v8    # "e":Ljava/lang/Exception;
    .end local v9    # "is":Ljava/io/FileInputStream;
    .restart local v5    # "buff":[B
    .restart local v6    # "bytesRead":I
    .restart local v10    # "is":Ljava/io/FileInputStream;
    .restart local v12    # "previousUpdate":I
    .restart local v13    # "srcFile":Ljava/io/File;
    .restart local v14    # "readTotal":J
    .restart local v16    # "size":J
    .restart local v19    # "toUpdate":I
    :catchall_2
    move-exception v20

    move-object v9, v10

    .end local v10    # "is":Ljava/io/FileInputStream;
    .restart local v9    # "is":Ljava/io/FileInputStream;
    goto/16 :goto_c

    .line 366
    .end local v9    # "is":Ljava/io/FileInputStream;
    .restart local v10    # "is":Ljava/io/FileInputStream;
    :catch_10
    move-exception v8

    move-object v9, v10

    .end local v10    # "is":Ljava/io/FileInputStream;
    .restart local v9    # "is":Ljava/io/FileInputStream;
    goto/16 :goto_a

    .line 362
    .end local v9    # "is":Ljava/io/FileInputStream;
    .restart local v10    # "is":Ljava/io/FileInputStream;
    :catch_11
    move-exception v8

    move-object v9, v10

    .end local v10    # "is":Ljava/io/FileInputStream;
    .restart local v9    # "is":Ljava/io/FileInputStream;
    goto/16 :goto_8

    .line 358
    .end local v5    # "buff":[B
    .end local v6    # "bytesRead":I
    .end local v12    # "previousUpdate":I
    .end local v13    # "srcFile":Ljava/io/File;
    .end local v14    # "readTotal":J
    .end local v16    # "size":J
    .end local v19    # "toUpdate":I
    :catch_12
    move-exception v8

    goto/16 :goto_6

    .end local v9    # "is":Ljava/io/FileInputStream;
    .restart local v5    # "buff":[B
    .restart local v6    # "bytesRead":I
    .restart local v10    # "is":Ljava/io/FileInputStream;
    .restart local v12    # "previousUpdate":I
    .restart local v13    # "srcFile":Ljava/io/File;
    .restart local v14    # "readTotal":J
    .restart local v16    # "size":J
    .restart local v19    # "toUpdate":I
    :cond_12
    move-object v9, v10

    .end local v10    # "is":Ljava/io/FileInputStream;
    .restart local v9    # "is":Ljava/io/FileInputStream;
    goto/16 :goto_1
.end method
