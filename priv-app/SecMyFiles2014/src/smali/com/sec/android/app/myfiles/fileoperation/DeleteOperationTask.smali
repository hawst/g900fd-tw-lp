.class public Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;
.super Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;
.source "DeleteOperationTask.java"


# static fields
.field private static final MODULE:Ljava/lang/String; = "DeleteOperationTask"


# instance fields
.field private mIsError:Z

.field private mSelectedItemType:I


# direct methods
.method public constructor <init>(Landroid/content/Context;IILandroid/view/View;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "operation"    # I
    .param p3, "contentType"    # I
    .param p4, "progressView"    # Landroid/view/View;

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;-><init>(Landroid/content/Context;IILandroid/view/View;)V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->mIsError:Z

    .line 43
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->mSelectedItemType:I

    .line 47
    return-void
.end method

.method private forcedDelete(Ljava/lang/String;)V
    .locals 5
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 156
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rm -rf "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v0

    .line 157
    .local v0, "chmod":Ljava/lang/Process;
    invoke-virtual {v0}, Ljava/lang/Process;->waitFor()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    .end local v0    # "chmod":Ljava/lang/Process;
    :goto_0
    return-void

    .line 158
    :catch_0
    move-exception v1

    .line 160
    .local v1, "e1":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;
    .locals 15
    .param p1, "params"    # [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .prologue
    .line 55
    const/4 v0, 0x0

    const-string v2, "DeleteOperationTask"

    const-string v3, "DeleteFiles start"

    invoke-static {v0, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 56
    const/4 v0, 0x0

    aget-object v14, p1, v0

    .line 57
    .local v14, "param":Lcom/sec/android/app/myfiles/element/FileOperationParam;
    iget-object v0, v14, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->mTargetFolder:Ljava/lang/String;

    .line 58
    iget-object v0, v14, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    .line 59
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    iput v0, v2, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mTotalTarget:I

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget v2, v14, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mStartIndex:I

    iput v2, v0, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTarget:I

    .line 61
    const-string v0, "external"

    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 62
    .local v1, "uri":Landroid/net/Uri;
    new-instance v7, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$MediaBulkDeleter;

    invoke-direct {v7, p0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$MediaBulkDeleter;-><init>(Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;)V

    .line 63
    .local v7, "deleter":Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$MediaBulkDeleter;
    const/4 v11, 0x0

    .line 64
    .local v11, "filesDeleted":Z
    const/4 v12, 0x0

    .line 65
    .local v12, "foldersDeleted":Z
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    if-eqz v0, :cond_5

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 68
    .local v10, "filePath":Ljava/lang/String;
    :try_start_0
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 69
    .local v9, "file":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->isFile()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 70
    const/4 v11, 0x1

    .line 74
    :goto_2
    invoke-virtual {p0, v9}, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->deleteInFileSystem(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 77
    iget v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->mContentType:I

    const/16 v2, 0x201

    if-eq v0, v2, :cond_1

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v2, 0x0

    const-string v3, "_data = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v10, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 79
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 80
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 81
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 82
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {v7, v2, v3}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$MediaBulkDeleter;->delete(J)V

    .line 84
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 87
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget v2, v0, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTarget:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTarget:I

    .line 88
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    aput-object v3, v0, v2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->publishProgress([Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/StackOverflowError; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 93
    .end local v9    # "file":Ljava/io/File;
    :catch_0
    move-exception v8

    .line 94
    .local v8, "e":Ljava/lang/Exception;
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    .line 95
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->cancel(Z)Z

    goto :goto_1

    .line 59
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v7    # "deleter":Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$MediaBulkDeleter;
    .end local v8    # "e":Ljava/lang/Exception;
    .end local v10    # "filePath":Ljava/lang/String;
    .end local v11    # "filesDeleted":Z
    .end local v12    # "foldersDeleted":Z
    .end local v13    # "i$":Ljava/util/Iterator;
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto/16 :goto_0

    .line 72
    .restart local v1    # "uri":Landroid/net/Uri;
    .restart local v7    # "deleter":Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$MediaBulkDeleter;
    .restart local v9    # "file":Ljava/io/File;
    .restart local v10    # "filePath":Ljava/lang/String;
    .restart local v11    # "filesDeleted":Z
    .restart local v12    # "foldersDeleted":Z
    .restart local v13    # "i$":Ljava/util/Iterator;
    :cond_3
    const/4 v12, 0x1

    goto :goto_2

    .line 90
    :cond_4
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->mIsError:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/StackOverflowError; {:try_start_1 .. :try_end_1} :catch_1

    .line 91
    const/4 v0, 0x0

    .line 111
    .end local v9    # "file":Ljava/io/File;
    .end local v10    # "filePath":Ljava/lang/String;
    .end local v13    # "i$":Ljava/util/Iterator;
    :goto_3
    return-object v0

    .line 96
    .restart local v10    # "filePath":Ljava/lang/String;
    .restart local v13    # "i$":Ljava/util/Iterator;
    :catch_1
    move-exception v8

    .line 97
    .local v8, "e":Ljava/lang/StackOverflowError;
    const/4 v0, 0x0

    const-string v2, "DeleteOperationTask"

    const-string v3, "doInBackground() - delete is failed StackOverflowError So forced to delete"

    invoke-static {v0, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 98
    invoke-direct {p0, v10}, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->forcedDelete(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 102
    .end local v8    # "e":Ljava/lang/StackOverflowError;
    .end local v10    # "filePath":Ljava/lang/String;
    .end local v13    # "i$":Ljava/util/Iterator;
    :cond_5
    if-eqz v11, :cond_7

    if-eqz v12, :cond_7

    const/4 v0, 0x2

    :goto_4
    iput v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->mSelectedItemType:I

    .line 104
    :try_start_2
    iget v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->mContentType:I

    const/16 v2, 0x201

    if-eq v0, v2, :cond_6

    .line 105
    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$MediaBulkDeleter;->flush()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    .line 110
    :cond_6
    :goto_5
    const/4 v0, 0x0

    const-string v2, "DeleteOperationTask"

    const-string v3, "DeleteFiles finish"

    invoke-static {v0, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 111
    const/4 v0, 0x0

    goto :goto_3

    .line 102
    :cond_7
    if-eqz v11, :cond_8

    const/4 v0, 0x1

    goto :goto_4

    :cond_8
    const/4 v0, 0x0

    goto :goto_4

    .line 107
    :catch_2
    move-exception v8

    .line 108
    .local v8, "e":Landroid/os/RemoteException;
    invoke-virtual {v8}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_5
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 40
    check-cast p1, [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V
    .locals 4
    .param p1, "result"    # Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .prologue
    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 125
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V

    .line 126
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->mIsError:Z

    if-nez v2, :cond_1

    iget v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->mSelectedItemType:I

    if-eq v2, v3, :cond_1

    .line 127
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    if-nez v2, :cond_0

    .line 129
    .local v1, "size":I
    :goto_0
    iget v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->mSelectedItemType:I

    packed-switch v2, :pswitch_data_0

    .line 148
    .end local v1    # "size":I
    :goto_1
    iput v3, p0, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->mSelectedItemType:I

    .line 149
    new-instance v0, Landroid/content/Intent;

    const-string v2, "Media_DB_Update_Finsished"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 150
    .local v0, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 151
    return-void

    .line 127
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto :goto_0

    .line 132
    .restart local v1    # "size":I
    :pswitch_0
    const v2, 0x7f0c000e

    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->showToastPrurals(II)V

    goto :goto_1

    .line 136
    :pswitch_1
    const v2, 0x7f0c000d

    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->showToastPrurals(II)V

    goto :goto_1

    .line 140
    :pswitch_2
    const v2, 0x7f0c000b

    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->showToastPrurals(II)V

    goto :goto_1

    .line 145
    .end local v1    # "size":I
    :cond_1
    const v2, 0x7f0b009f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->showToast(I)V

    .line 146
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->cancel(Z)Z

    goto :goto_1

    .line 129
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 40
    check-cast p1, Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Lcom/sec/android/app/myfiles/element/FileOperationProgress;)V
    .locals 4
    .param p1, "values"    # [Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    .prologue
    const/4 v3, 0x0

    .line 116
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->mCountText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->mCountText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v2, p1, v3

    iget v2, v2, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTarget:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v2, p1, v3

    iget v2, v2, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mTotalTarget:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->mPercentText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v2, p1, v3

    iget v2, v2, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTarget:I

    mul-int/lit8 v2, v2, 0x64

    aget-object v3, p1, v3

    iget v3, v3, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mTotalTarget:I

    div-int/2addr v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " %"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    :cond_0
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 40
    check-cast p1, [Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;->onProgressUpdate([Lcom/sec/android/app/myfiles/element/FileOperationProgress;)V

    return-void
.end method

.method protected setupProgressView()V
    .locals 0

    .prologue
    .line 51
    return-void
.end method
