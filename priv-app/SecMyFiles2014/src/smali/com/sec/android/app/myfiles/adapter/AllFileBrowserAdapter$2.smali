.class Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;
.super Ljava/lang/Object;
.source "AllFileBrowserAdapter.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

.field final synthetic val$bFolderFormat:Z

.field final synthetic val$filePath:Ljava/lang/String;

.field final synthetic val$filePosition:I

.field final synthetic val$fileType:I

.field final synthetic val$hoverFilePath:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;ZLjava/lang/String;IILjava/lang/String;)V
    .locals 0

    .prologue
    .line 488
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    iput-boolean p2, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->val$bFolderFormat:Z

    iput-object p3, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->val$hoverFilePath:Ljava/lang/String;

    iput p4, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->val$fileType:I

    iput p5, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->val$filePosition:I

    iput-object p6, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->val$filePath:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    const/16 v1, 0x201

    const/4 v11, 0x2

    const/4 v8, 0x0

    .line 493
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->isSelectMode()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$100(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/sec/android/app/myfiles/utils/Utils;->isHoverSettingDisabled(Landroid/content/Context;Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$200(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mVideoHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$300(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mAudioHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$400(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mFolderHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$500(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mDocumentHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$600(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 496
    const/4 v6, 0x0

    .line 498
    .local v6, "bSupportHover":Z
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 499
    .local v2, "hoverRect":Landroid/graphics/Rect;
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 501
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    iget v9, v2, Landroid/graphics/Rect;->left:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v10, v0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    iget v0, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mViewMode:I

    if-ne v0, v11, :cond_3

    move v0, v3

    :goto_0
    invoke-virtual {v4, v9, v10, v0}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->setHoverLeft(IIZ)I

    move-result v0

    iput v0, v2, Landroid/graphics/Rect;->left:I

    .line 502
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    iget v3, v2, Landroid/graphics/Rect;->top:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->setHoverTop(II)I

    move-result v0

    iput v0, v2, Landroid/graphics/Rect;->top:I

    .line 504
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->val$bFolderFormat:Z

    if-eqz v0, :cond_4

    .line 506
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v5

    .line 508
    .local v5, "hoverFolderNavi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mFolderHoverCursor:Landroid/database/Cursor;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$700(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_0

    .line 510
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->val$hoverFilePath:Ljava/lang/String;

    invoke-virtual {v5, v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInFolder(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    # setter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mFolderHoverCursor:Landroid/database/Cursor;
    invoke-static {v0, v3}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$702(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 513
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mFolderHoverCursor:Landroid/database/Cursor;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$700(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 515
    const-string v0, "AllFileBrowserAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "event X : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " envent Y : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v11, v0, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 518
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mFolderHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$500(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 520
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$100(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    iget v0, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mViewMode:I

    if-ne v0, v11, :cond_1

    .line 522
    iget v0, v2, Landroid/graphics/Rect;->left:I

    add-int/lit8 v0, v0, 0x32

    iput v0, v2, Landroid/graphics/Rect;->left:I

    .line 523
    iget v0, v2, Landroid/graphics/Rect;->top:I

    add-int/lit8 v0, v0, 0x32

    iput v0, v2, Landroid/graphics/Rect;->top:I

    .line 526
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mFolderHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$500(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->val$hoverFilePath:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mFolderHoverCursor:Landroid/database/Cursor;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$700(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Landroid/database/Cursor;

    move-result-object v4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->setParam(ILandroid/graphics/Rect;Ljava/lang/String;Landroid/database/Cursor;Lcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 533
    const/4 v6, 0x1

    .line 573
    .end local v5    # "hoverFolderNavi":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    :goto_1
    if-eqz v6, :cond_2

    .line 575
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 657
    .end local v2    # "hoverRect":Landroid/graphics/Rect;
    .end local v6    # "bSupportHover":Z
    :cond_2
    :goto_2
    :pswitch_0
    return v8

    .restart local v2    # "hoverRect":Landroid/graphics/Rect;
    .restart local v6    # "bSupportHover":Z
    :cond_3
    move v0, v8

    .line 501
    goto/16 :goto_0

    .line 535
    :cond_4
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->val$fileType:I

    invoke-static {v0}, Lcom/sec/android/app/myfiles/MediaFile;->isVideoFileType(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 537
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$100(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isOnCall(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 539
    const/4 v6, 0x0

    goto :goto_1

    .line 541
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mVideoHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$300(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 542
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mVideoHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$300(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->val$hoverFilePath:Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->val$filePosition:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->setParam(ILandroid/graphics/Rect;Ljava/lang/String;I)V

    .line 543
    const/4 v6, 0x1

    goto :goto_1

    .line 546
    :cond_6
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->val$fileType:I

    invoke-static {v0}, Lcom/sec/android/app/myfiles/MediaFile;->isAudioFileType(I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 548
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mAudioHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$400(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 549
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mAudioHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$400(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->val$hoverFilePath:Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->val$filePosition:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->setParam(ILandroid/graphics/Rect;Ljava/lang/String;I)V

    .line 550
    const/4 v6, 0x1

    goto :goto_1

    .line 552
    :cond_7
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->val$fileType:I

    invoke-static {v0}, Lcom/sec/android/app/myfiles/MediaFile;->isImageFileType(I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 554
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$200(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 555
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$200(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->val$hoverFilePath:Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->val$filePosition:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->setParam(ILandroid/graphics/Rect;Ljava/lang/String;I)V

    .line 556
    const/4 v6, 0x1

    goto :goto_1

    .line 558
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    iget v0, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mViewMode:I

    if-ne v0, v11, :cond_b

    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->val$fileType:I

    invoke-static {v0}, Lcom/sec/android/app/myfiles/MediaFile;->isDocumentFileType(I)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 559
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->val$fileType:I

    const/16 v1, 0x4c

    if-eq v0, v1, :cond_9

    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->val$fileType:I

    const/16 v1, 0x4b

    if-ne v0, v1, :cond_a

    .line 560
    :cond_9
    const/4 v6, 0x0

    goto/16 :goto_1

    .line 562
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mDocumentHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$600(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 563
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mDocumentHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$600(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v0

    const/4 v1, 0x5

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->val$filePath:Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->val$filePosition:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->setParam(ILandroid/graphics/Rect;Ljava/lang/String;I)V

    .line 564
    const/4 v6, 0x1

    goto/16 :goto_1

    .line 569
    :cond_b
    const/4 v6, 0x0

    goto/16 :goto_1

    .line 580
    :pswitch_1
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    if-ne v0, v11, :cond_c

    .line 582
    const/16 v0, 0xa

    const/4 v1, -0x1

    invoke-static {v0, v1}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 591
    :cond_c
    :goto_3
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->val$bFolderFormat:Z

    if-eqz v0, :cond_d

    .line 592
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    const/16 v1, 0x12c

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->sendDelayedMessage(I)V

    .line 597
    :goto_4
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v10

    # setter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mLastHoverEnterEventTime:J
    invoke-static {v0, v10, v11}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$802(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;J)J

    .line 598
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    invoke-static {p2}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v1

    # setter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mLastMotionEvent:Landroid/view/MotionEvent;
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$902(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    .line 599
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # setter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mLastHoverEnteredView:Landroid/view/View;
    invoke-static {v0, p1}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$1002(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;Landroid/view/View;)Landroid/view/View;

    goto/16 :goto_2

    .line 586
    :catch_0
    move-exception v7

    .line 588
    .local v7, "e":Landroid/os/RemoteException;
    invoke-virtual {v7}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_3

    .line 594
    .end local v7    # "e":Landroid/os/RemoteException;
    :cond_d
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    const/16 v1, 0x1f4

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->sendDelayedMessage(I)V

    goto :goto_4

    .line 605
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->removeDelayedMessage()V

    .line 609
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    if-ne v0, v11, :cond_e

    .line 611
    const/4 v0, 0x1

    const/4 v1, -0x1

    invoke-static {v0, v1}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 619
    :cond_e
    :goto_5
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mFolderHoverCursor:Landroid/database/Cursor;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$700(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mFolderHoverCursor:Landroid/database/Cursor;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$700(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_f

    .line 621
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mFolderHoverCursor:Landroid/database/Cursor;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$700(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 622
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mFolderHoverCursor:Landroid/database/Cursor;
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$702(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 628
    :cond_f
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mLastHoverEnterEventTime:J
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$800(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)J

    move-result-wide v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v10

    cmp-long v0, v0, v10

    if-nez v0, :cond_10

    .line 629
    const-string v0, "AllFileBrowserAdapter"

    const-string v1, "resending hover enter event"

    invoke-static {v8, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 631
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mLastHoverEnteredView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$1000(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mLastMotionEvent:Landroid/view/MotionEvent;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$900(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Landroid/view/MotionEvent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    .line 632
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mLastMotionEvent:Landroid/view/MotionEvent;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$900(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Landroid/view/MotionEvent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2

    .line 636
    :goto_6
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    const-wide/16 v10, -0x1

    # setter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mLastHoverEnterEventTime:J
    invoke-static {v0, v10, v11}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$802(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;J)J

    goto/16 :goto_2

    .line 614
    :catch_1
    move-exception v7

    .line 616
    .restart local v7    # "e":Landroid/os/RemoteException;
    invoke-virtual {v7}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_5

    .line 633
    .end local v7    # "e":Landroid/os/RemoteException;
    :catch_2
    move-exception v7

    .line 634
    .local v7, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v7}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_6

    .line 637
    .end local v7    # "e":Ljava/lang/RuntimeException;
    :cond_10
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mLastHoverEnterEventTime:J
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$800(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)J

    move-result-wide v0

    const-wide/16 v10, -0x1

    cmp-long v0, v0, v10

    if-eqz v0, :cond_2

    .line 639
    :try_start_3
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mLastMotionEvent:Landroid/view/MotionEvent;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->access$900(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Landroid/view/MotionEvent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_2

    .line 640
    :catch_3
    move-exception v7

    .line 641
    .restart local v7    # "e":Ljava/lang/RuntimeException;
    invoke-virtual {v7}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto/16 :goto_2

    .line 575
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
