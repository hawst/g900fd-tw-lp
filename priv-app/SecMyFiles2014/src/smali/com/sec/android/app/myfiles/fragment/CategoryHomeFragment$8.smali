.class Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$8;
.super Ljava/lang/Object;
.source "CategoryHomeFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setupViews(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V
    .locals 0

    .prologue
    .line 768
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 5
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 772
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v1, :cond_0

    .line 773
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v1, v1, Lcom/sec/android/app/myfiles/MainActivity;->mIsSearchMode:Z

    if-eqz v1, :cond_0

    move v1, v2

    .line 798
    :goto_0
    return v1

    .line 777
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 778
    .local v0, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v1

    const/16 v4, 0x8

    if-eq v1, v4, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v1

    const/16 v4, 0x26

    if-eq v1, v4, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v1

    const/16 v4, 0x27

    if-ne v1, v4, :cond_2

    :cond_1
    move v1, v2

    .line 779
    goto :goto_0

    .line 780
    :cond_2
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v1

    const/16 v4, 0x1f

    if-ne v1, v4, :cond_3

    .line 781
    const-string v1, "CategoryHomeFragment"

    const-string v3, "CategoryHomeFragment onItemLongClick - BAIDU"

    invoke-static {v2, v1, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    move v1, v2

    .line 782
    goto :goto_0

    .line 785
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->isSelectMode()Z

    move-result v1

    if-nez v1, :cond_4

    .line 786
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v1, v3, p3, v2}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->startSelectMode(III)V

    .line 789
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    move-result-object v1

    invoke-virtual {v1, v2, v2}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->startSelectMode(II)V

    .line 790
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->selectItem(I)V

    .line 791
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    move-result-object v1

    invoke-virtual {v1, v2, v2}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->startSelectMode(II)V

    .line 792
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    move-result-object v1

    invoke-virtual {v1, v2, v2}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->startSelectMode(II)V

    .line 793
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mStorageListAdapter:Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$500(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;

    move-result-object v1

    invoke-virtual {v1, v2, v2}, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->startSelectMode(II)V

    .line 794
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mRemoteShareListAdapter:Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$600(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;

    move-result-object v1

    invoke-virtual {v1, v2, v2}, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->startSelectMode(II)V

    .line 795
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setAllItemCount()V

    move v1, v3

    .line 796
    goto/16 :goto_0

    :cond_4
    move v1, v2

    .line 798
    goto/16 :goto_0
.end method
