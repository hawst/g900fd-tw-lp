.class Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$16;
.super Ljava/lang/Object;
.source "CategoryHomeFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

.field final synthetic val$dialogId:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;I)V
    .locals 0

    .prologue
    .line 1809
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$16;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iput p2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$16;->val$dialogId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v3, 0x1

    .line 1814
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 1815
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$16;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v0

    .line 1817
    .local v0, "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$16;->val$dialogId:I

    const/16 v2, 0x9

    if-ne v1, v2, :cond_1

    .line 1819
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$16;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->showToastForScanningNearbyDevices(Z)V

    .line 1820
    invoke-static {}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;->getInstance()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;

    move-result-object v1

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;
    invoke-static {}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$1500()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;->registerProvider(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)V

    .line 1821
    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;
    invoke-static {}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$1500()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->loadDevices()V

    .line 1842
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$16;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mDoNotCheckWLANOn:Z
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$1600(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setDoNotShowCheckWLANOn(Z)V

    .line 1843
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->commitShowDataWarning()Z

    .line 1844
    return-void

    .line 1823
    :cond_1
    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$16;->val$dialogId:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_2

    .line 1825
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$16;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->startBrowserFTPorDropBox()V

    goto :goto_0

    .line 1827
    :cond_2
    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$16;->val$dialogId:I

    const/16 v2, 0xb

    if-ne v1, v2, :cond_0

    .line 1828
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$16;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v1, :cond_0

    .line 1830
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$16;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setStartBrowserState(Z)V

    .line 1832
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$16;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-boolean v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->bDropBoxFromShortcut:Z

    if-eqz v1, :cond_3

    .line 1834
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$16;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->startBrowserFTPorDropBox()V

    goto :goto_0

    .line 1837
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$16;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$16;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget v2, v2, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mModeForWarningDialog:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto :goto_0
.end method
