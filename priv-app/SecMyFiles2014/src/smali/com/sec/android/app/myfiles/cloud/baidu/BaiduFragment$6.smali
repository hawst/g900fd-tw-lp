.class Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$6;
.super Ljava/lang/Object;
.source "BaiduFragment.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)V
    .locals 0

    .prologue
    .line 1338
    iput-object p1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$6;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPathSelected(ILjava/lang/String;)V
    .locals 9
    .param p1, "targetFolderID"    # I
    .param p2, "targetFolderPath"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1343
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$6;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->isSelectMode()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$6;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getSelectModeFrom()I

    move-result v3

    if-eq v3, v7, :cond_0

    .line 1345
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$6;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->finishSelectMode()V

    .line 1351
    :cond_0
    const-string v3, "Baidu"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1352
    const-string v3, "Baidu"

    const-string v4, ""

    invoke-virtual {p2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 1357
    .local v0, "BaiduTargetFolderPath":Ljava/lang/String;
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$6;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$5200(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v3

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1359
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$6;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$5400(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/view/PathIndicator;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$6;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduRoot:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$2300(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$6;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$5300(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 1360
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$6;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$5600(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$6;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mBaiduRoot:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$2300(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$6;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$5500(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setCurrentDirectory(Ljava/lang/String;)V

    .line 1362
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$6;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$5700(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v1

    .line 1364
    .local v1, "c":Landroid/database/Cursor;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$6;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # invokes: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->updateAdapter(Landroid/database/Cursor;)V
    invoke-static {v3, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$5800(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;Landroid/database/Cursor;)V

    .line 1366
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$6;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getSelectModeFrom()I

    move-result v3

    if-nez v3, :cond_5

    .line 1368
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$6;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->isSelectMode()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1370
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$6;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->finishSelectMode()V

    .line 1389
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$6;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getSelectionType()I

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$6;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getSelectionType()I

    move-result v3

    if-ne v3, v7, :cond_3

    .line 1391
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$6;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$6100(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;)Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->unselectAllItem()V

    .line 1398
    .end local v1    # "c":Landroid/database/Cursor;
    :cond_3
    :goto_2
    return-void

    .line 1354
    .end local v0    # "BaiduTargetFolderPath":Ljava/lang/String;
    :cond_4
    const/4 v3, 0x6

    invoke-virtual {p2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "BaiduTargetFolderPath":Ljava/lang/String;
    goto/16 :goto_0

    .line 1375
    .restart local v1    # "c":Landroid/database/Cursor;
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$6;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getSelectionType()I

    move-result v3

    if-eq v3, v8, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$6;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->getSelectionType()I

    move-result v3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    .line 1378
    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mOptionsMenu:Landroid/view/Menu;
    invoke-static {}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$5900()Landroid/view/Menu;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1380
    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->mOptionsMenu:Landroid/view/Menu;
    invoke-static {}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;->access$6000()Landroid/view/Menu;

    move-result-object v3

    const v4, 0x7f0f0148

    invoke-interface {v3, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 1382
    .local v2, "item":Landroid/view/MenuItem;
    if-eqz v2, :cond_1

    .line 1384
    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 1395
    .end local v1    # "c":Landroid/database/Cursor;
    .end local v2    # "item":Landroid/view/MenuItem;
    :cond_6
    const-string v3, "BaiduFragment"

    const-string v4, "Can\'t go to the folder"

    invoke-static {v6, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1396
    const-string v3, "BaiduFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "=> path = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v8, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
