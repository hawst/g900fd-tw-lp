.class Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$5;
.super Ljava/lang/Object;
.source "VideoHoverPopup.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->playVideo()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;)V
    .locals 0

    .prologue
    .line 515
    iput-object p1, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$5;->this$0:Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 3
    .param p1, "player"    # Landroid/media/MediaPlayer;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I

    .prologue
    const/4 v2, 0x2

    .line 519
    const-string v0, "VideoHoverPopup"

    const-string v1, "onError"

    invoke-static {v2, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    .line 520
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->reset()V

    .line 521
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$5;->this$0:Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->removeDelayedMessage()V

    .line 522
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup$5;->this$0:Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 523
    const/4 v0, 0x0

    return v0
.end method
