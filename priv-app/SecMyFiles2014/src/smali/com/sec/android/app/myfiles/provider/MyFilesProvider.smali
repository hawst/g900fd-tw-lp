.class public Lcom/sec/android/app/myfiles/provider/MyFilesProvider;
.super Landroid/content/ContentProvider;
.source "MyFilesProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/provider/MyFilesProvider$DatabaseHelper;
    }
.end annotation


# static fields
.field private static final DATABASE_NAME:Ljava/lang/String; = "myfiles.db"

.field private static final DATABASE_VERSION:I = 0x65

.field private static final FILES:I = 0x3

.field private static final FILES_ID:I = 0x4

.field private static final FILE_NAME_FIND_FILTER:Ljava/lang/String; = "\"abcdefghijklmnopqrstuvwxyz01234567890;[] .!@#$%^&()-_=+\""

.field private static final FINDO_ID:I = 0x5

.field private static final FOLDERS:I = 0x1

.field private static final FOLDERS_ID:I = 0x2

.field private static final MODULE:Ljava/lang/String; = "MyFilesProvider"

.field private static final SHORTCUT:I = 0x6

.field private static final SHORTCUT_ID:I = 0x7

.field private static final TABLE_FILES:Ljava/lang/String; = "files"

.field private static final TABLE_FOLDERS:Ljava/lang/String; = "folders"

.field private static final TABLE_SHORTCUT:Ljava/lang/String; = "shortcuts"

.field private static final URI_MATCHER:Landroid/content/UriMatcher;


# instance fields
.field private mDatabaseHelper:Lcom/sec/android/app/myfiles/provider/MyFilesProvider$DatabaseHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x5

    .line 58
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/sec/android/app/myfiles/provider/MyFilesProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    .line 669
    sget-object v0, Lcom/sec/android/app/myfiles/provider/MyFilesProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "myfiles"

    const-string v2, "folder"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 671
    sget-object v0, Lcom/sec/android/app/myfiles/provider/MyFilesProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "myfiles"

    const-string v2, "folder/#"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 673
    sget-object v0, Lcom/sec/android/app/myfiles/provider/MyFilesProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "myfiles"

    const-string v2, "file"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 675
    sget-object v0, Lcom/sec/android/app/myfiles/provider/MyFilesProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "myfiles"

    const-string v2, "file/#"

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 678
    sget-object v0, Lcom/sec/android/app/myfiles/provider/MyFilesProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "myfiles"

    const-string v2, "search_suggest_regex_query"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 679
    sget-object v0, Lcom/sec/android/app/myfiles/provider/MyFilesProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "myfiles"

    const-string v2, "search_suggest_regex_query/*"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 680
    sget-object v0, Lcom/sec/android/app/myfiles/provider/MyFilesProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "myfiles"

    const-string v2, "search_suggest_regex_query/#"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 683
    sget-object v0, Lcom/sec/android/app/myfiles/provider/MyFilesProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "myfiles"

    const-string v2, "shortcut"

    const/4 v3, 0x6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 684
    sget-object v0, Lcom/sec/android/app/myfiles/provider/MyFilesProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "myfiles"

    const-string v2, "shortcut/#"

    const/4 v3, 0x7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 685
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 946
    return-void
.end method

.method private getSearchCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 20
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 841
    new-instance v11, Landroid/database/MatrixCursor;

    const/16 v14, 0x9

    new-array v14, v14, [Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "suggest_icon_1"

    aput-object v16, v14, v15

    const/4 v15, 0x1

    const-string v16, "suggest_text_1"

    aput-object v16, v14, v15

    const/4 v15, 0x2

    const-string v16, "suggest_text_2"

    aput-object v16, v14, v15

    const/4 v15, 0x3

    const-string v16, "suggest_text_3"

    aput-object v16, v14, v15

    const/4 v15, 0x4

    const-string v16, "suggest_intent_data"

    aput-object v16, v14, v15

    const/4 v15, 0x5

    const-string v16, "suggest_uri"

    aput-object v16, v14, v15

    const/4 v15, 0x6

    const-string v16, "suggest_mime_type"

    aput-object v16, v14, v15

    const/4 v15, 0x7

    const-string v16, "suggest_target_type"

    aput-object v16, v14, v15

    const/16 v15, 0x8

    const-string v16, "highlight_content"

    aput-object v16, v14, v15

    invoke-direct {v11, v14}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 854
    .local v11, "result":Landroid/database/MatrixCursor;
    if-eqz p1, :cond_4

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v14

    if-eqz v14, :cond_4

    .line 856
    const/4 v6, 0x0

    .line 857
    .local v6, "filePath":Ljava/lang/String;
    const/4 v4, 0x0

    .line 858
    .local v4, "fileIconUri":Landroid/net/Uri;
    const/4 v5, 0x0

    .line 859
    .local v5, "fileName":Ljava/lang/String;
    const/4 v10, -0x1

    .line 860
    .local v10, "resId":I
    const/4 v3, 0x0

    .line 861
    .local v3, "date":Ljava/lang/String;
    const/4 v7, 0x0

    .line 862
    .local v7, "fileSize":Ljava/lang/String;
    const/4 v13, 0x0

    .line 863
    .local v13, "uri":Landroid/net/Uri;
    const/4 v9, 0x0

    .line 864
    .local v9, "mimeType":Ljava/lang/String;
    const/4 v12, 0x0

    .line 865
    .local v12, "targetType":Ljava/lang/String;
    const/4 v2, 0x0

    .line 869
    .local v2, "column":[Ljava/lang/Object;
    :cond_0
    const-string v14, "_data"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 871
    if-nez v6, :cond_1

    .line 872
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    .line 873
    invoke-virtual {v11}, Landroid/database/MatrixCursor;->close()V

    .line 874
    const/4 v11, 0x0

    .line 941
    .end local v2    # "column":[Ljava/lang/Object;
    .end local v3    # "date":Ljava/lang/String;
    .end local v4    # "fileIconUri":Landroid/net/Uri;
    .end local v5    # "fileName":Ljava/lang/String;
    .end local v6    # "filePath":Ljava/lang/String;
    .end local v7    # "fileSize":Ljava/lang/String;
    .end local v9    # "mimeType":Ljava/lang/String;
    .end local v10    # "resId":I
    .end local v11    # "result":Landroid/database/MatrixCursor;
    .end local v12    # "targetType":Ljava/lang/String;
    .end local v13    # "uri":Landroid/net/Uri;
    :goto_0
    return-object v11

    .line 877
    .restart local v2    # "column":[Ljava/lang/Object;
    .restart local v3    # "date":Ljava/lang/String;
    .restart local v4    # "fileIconUri":Landroid/net/Uri;
    .restart local v5    # "fileName":Ljava/lang/String;
    .restart local v6    # "filePath":Ljava/lang/String;
    .restart local v7    # "fileSize":Ljava/lang/String;
    .restart local v9    # "mimeType":Ljava/lang/String;
    .restart local v10    # "resId":I
    .restart local v11    # "result":Landroid/database/MatrixCursor;
    .restart local v12    # "targetType":Ljava/lang/String;
    .restart local v13    # "uri":Landroid/net/Uri;
    :cond_1
    const/16 v14, 0x2f

    invoke-virtual {v6, v14}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v14

    add-int/lit8 v14, v14, 0x1

    invoke-virtual {v6, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 879
    const/4 v4, 0x0

    .line 881
    const-string v14, "format"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 882
    .local v8, "format":I
    const/16 v14, 0x3001

    if-ne v8, v14, :cond_3

    .line 884
    const-string v12, "1"

    .line 885
    const-string v14, "android.resource://com.sec.android.app.myfiles/2130837679"

    invoke-static {v14}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 894
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/provider/MyFilesProvider;->getContext()Landroid/content/Context;

    move-result-object v14

    const-string v15, "date_modified"

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    const-wide/16 v18, 0x3e8

    mul-long v16, v16, v18

    move-wide/from16 v0, v16

    invoke-static {v14, v0, v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    .line 897
    const/4 v7, 0x0

    .line 898
    const/4 v13, 0x0

    .line 899
    const/4 v9, 0x0

    .line 901
    const/16 v14, 0x3001

    if-eq v8, v14, :cond_2

    .line 903
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/provider/MyFilesProvider;->getContext()Landroid/content/Context;

    move-result-object v14

    const-string v15, "_size"

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-static {v14, v0, v1}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v7

    .line 906
    new-instance v14, Ljava/io/File;

    invoke-direct {v14, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v14}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v13

    .line 908
    const-string v14, "mime_type"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 911
    if-nez v9, :cond_2

    .line 913
    invoke-static {v6}, Lcom/sec/android/app/myfiles/MediaFile;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 917
    :cond_2
    const/16 v14, 0x9

    new-array v2, v14, [Ljava/lang/Object;

    .end local v2    # "column":[Ljava/lang/Object;
    const/4 v14, 0x0

    aput-object v4, v2, v14

    const/4 v14, 0x1

    aput-object v5, v2, v14

    const/4 v14, 0x2

    aput-object v3, v2, v14

    const/4 v14, 0x3

    aput-object v7, v2, v14

    const/4 v14, 0x4

    aput-object v6, v2, v14

    const/4 v14, 0x5

    aput-object v13, v2, v14

    const/4 v14, 0x6

    aput-object v9, v2, v14

    const/4 v14, 0x7

    aput-object v12, v2, v14

    const/16 v14, 0x8

    const-string v15, ""

    aput-object v15, v2, v14

    .line 929
    .restart local v2    # "column":[Ljava/lang/Object;
    invoke-virtual {v11, v2}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 931
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v14

    if-nez v14, :cond_0

    .line 933
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    .line 934
    invoke-virtual {v11}, Landroid/database/MatrixCursor;->close()V

    goto/16 :goto_0

    .line 889
    :cond_3
    const-string v12, "0"

    .line 890
    invoke-static {v6}, Lcom/sec/android/app/myfiles/MediaFile;->getSmallIcon(Ljava/lang/String;)I

    move-result v10

    .line 891
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "android.resource://com.sec.android.app.myfiles/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    goto/16 :goto_1

    .line 938
    .end local v2    # "column":[Ljava/lang/Object;
    .end local v3    # "date":Ljava/lang/String;
    .end local v4    # "fileIconUri":Landroid/net/Uri;
    .end local v5    # "fileName":Ljava/lang/String;
    .end local v6    # "filePath":Ljava/lang/String;
    .end local v7    # "fileSize":Ljava/lang/String;
    .end local v8    # "format":I
    .end local v9    # "mimeType":Ljava/lang/String;
    .end local v10    # "resId":I
    .end local v12    # "targetType":Ljava/lang/String;
    .end local v13    # "uri":Landroid/net/Uri;
    :cond_4
    if-eqz p1, :cond_5

    .line 939
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->close()V

    .line 940
    :cond_5
    invoke-virtual {v11}, Landroid/database/MatrixCursor;->close()V

    .line 941
    const/4 v11, 0x0

    goto/16 :goto_0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 289
    iget-object v3, p0, Lcom/sec/android/app/myfiles/provider/MyFilesProvider;->mDatabaseHelper:Lcom/sec/android/app/myfiles/provider/MyFilesProvider$DatabaseHelper;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/provider/MyFilesProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 295
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    sget-object v3, Lcom/sec/android/app/myfiles/provider/MyFilesProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 361
    :pswitch_0
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 362
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "MyFilesProvider::delete() - Unknown URI"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 299
    :pswitch_1
    const-string v3, "folders"

    invoke-virtual {v1, v3, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 365
    .local v0, "count":I
    :goto_0
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 368
    return v0

    .line 306
    .end local v0    # "count":I
    :pswitch_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 308
    .local v2, "finalSelection":Ljava/lang/String;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 310
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 313
    :cond_0
    const-string v3, "folders"

    invoke-virtual {v1, v3, v2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 315
    .restart local v0    # "count":I
    goto :goto_0

    .line 320
    .end local v0    # "count":I
    .end local v2    # "finalSelection":Ljava/lang/String;
    :pswitch_3
    const-string v3, "files"

    invoke-virtual {v1, v3, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 322
    .restart local v0    # "count":I
    goto :goto_0

    .line 327
    .end local v0    # "count":I
    :pswitch_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 329
    .restart local v2    # "finalSelection":Ljava/lang/String;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 331
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 334
    :cond_1
    const-string v3, "files"

    invoke-virtual {v1, v3, v2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 336
    .restart local v0    # "count":I
    goto/16 :goto_0

    .line 340
    .end local v0    # "count":I
    .end local v2    # "finalSelection":Ljava/lang/String;
    :pswitch_5
    const-string v3, "shortcuts"

    invoke-virtual {v1, v3, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 342
    .restart local v0    # "count":I
    goto/16 :goto_0

    .line 347
    .end local v0    # "count":I
    :pswitch_6
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 349
    .restart local v2    # "finalSelection":Ljava/lang/String;
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 351
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 354
    :cond_2
    const-string v3, "shortcuts"

    invoke-virtual {v1, v3, v2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 356
    .restart local v0    # "count":I
    goto/16 :goto_0

    .line 295
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public getContentSearchCursor(Ljava/lang/String;JJ)Landroid/database/Cursor;
    .locals 38
    .param p1, "param"    # Ljava/lang/String;
    .param p2, "startTime"    # J
    .param p4, "endTime"    # J

    .prologue
    .line 691
    if-nez p1, :cond_0

    .line 692
    const/4 v12, 0x0

    .line 836
    :goto_0
    return-object v12

    .line 694
    :cond_0
    const-string v9, "modifiedfromTime"

    .line 695
    .local v9, "MODIFIED_FROM_TIME":Ljava/lang/String;
    const-string v10, "modifiedtoTime"

    .line 696
    .local v10, "MODIFIED_TO_TIME":Ljava/lang/String;
    const-string v8, "parent"

    .line 697
    .local v8, "KEY_PARENT":Ljava/lang/String;
    const-string v2, "content://com.samsung.index.searchprovider/galaxy_search"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 699
    .local v3, "CONTENT_URI":Landroid/net/Uri;
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 700
    .local v24, "keyList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v34, Ljava/util/ArrayList;

    invoke-direct/range {v34 .. v34}, Ljava/util/ArrayList;-><init>()V

    .line 702
    .local v34, "valueList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v2, "ContentSearch"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "create cursor for "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " startTime:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p2

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " endTime:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 704
    const-wide/16 v6, 0x0

    cmp-long v2, p2, v6

    if-lez v2, :cond_1

    .line 705
    const-string v2, "modifiedtoTime"

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 706
    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v34

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 708
    :cond_1
    const-wide/16 v6, 0x0

    cmp-long v2, p4, v6

    if-lez v2, :cond_2

    .line 709
    const-string v2, "modifiedfromTime"

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 710
    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v34

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 713
    :cond_2
    const/4 v12, 0x0

    .line 715
    .local v12, "contentCursor":Landroid/database/MatrixCursor;
    const-wide/16 v30, 0x0

    .local v30, "startSearchTime":J
    const-wide/16 v16, 0x0

    .line 718
    .local v16, "endSearchTime":J
    move-object/from16 v5, p1

    .line 719
    .local v5, "searchQuery":Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v30

    .line 720
    const-string v2, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "search Start Time : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v30

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 721
    const/4 v13, 0x0

    .line 725
    .local v13, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v33, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants;->ROOT_PATH:Ljava/lang/String;

    .line 727
    .local v33, "value":Ljava/lang/String;
    new-instance v27, Ljava/io/File;

    const-string v2, "/storage/extSdCard/"

    move-object/from16 v0, v27

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 728
    .local v27, "sd":Ljava/io/File;
    const/16 v28, 0x0

    .line 729
    .local v28, "sdFileCnt":I
    if-eqz v27, :cond_3

    invoke-virtual/range {v27 .. v27}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 730
    invoke-virtual/range {v27 .. v27}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v0, v2

    move/from16 v28, v0

    .line 731
    if-lez v28, :cond_3

    .line 732
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v33

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ":/storage/extSdCard/"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    .line 736
    :cond_3
    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    if-eqz v2, :cond_4

    sget-object v2, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 737
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v33

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ":"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v4, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    .line 740
    :cond_4
    const-string v2, "parent"

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 741
    move-object/from16 v0, v34

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 743
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/provider/MyFilesProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    invoke-virtual/range {v34 .. v34}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v6, v6, [Ljava/lang/String;

    move-object/from16 v0, v34

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v13

    .line 747
    if-nez v13, :cond_5

    .line 748
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 751
    .end local v27    # "sd":Ljava/io/File;
    .end local v28    # "sdFileCnt":I
    .end local v33    # "value":Ljava/lang/String;
    :catch_0
    move-exception v15

    .line 752
    .local v15, "e":Ljava/lang/Exception;
    invoke-virtual {v15}, Ljava/lang/Exception;->printStackTrace()V

    .line 755
    .end local v15    # "e":Ljava/lang/Exception;
    :cond_5
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 756
    new-instance v12, Landroid/database/MatrixCursor;

    .end local v12    # "contentCursor":Landroid/database/MatrixCursor;
    const/16 v2, 0x9

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v6, "suggest_icon_1"

    aput-object v6, v2, v4

    const/4 v4, 0x1

    const-string v6, "suggest_text_1"

    aput-object v6, v2, v4

    const/4 v4, 0x2

    const-string v6, "suggest_text_2"

    aput-object v6, v2, v4

    const/4 v4, 0x3

    const-string v6, "suggest_text_3"

    aput-object v6, v2, v4

    const/4 v4, 0x4

    const-string v6, "suggest_intent_data"

    aput-object v6, v2, v4

    const/4 v4, 0x5

    const-string v6, "suggest_uri"

    aput-object v6, v2, v4

    const/4 v4, 0x6

    const-string v6, "suggest_mime_type"

    aput-object v6, v2, v4

    const/4 v4, 0x7

    const-string v6, "suggest_target_type"

    aput-object v6, v2, v4

    const/16 v4, 0x8

    const-string v6, "highlight_content"

    aput-object v6, v2, v4

    invoke-direct {v12, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 769
    .restart local v12    # "contentCursor":Landroid/database/MatrixCursor;
    const/16 v21, 0x0

    .line 770
    .local v21, "filePath":Ljava/lang/String;
    const/16 v20, 0x0

    .line 771
    .local v20, "fileName":Ljava/lang/String;
    const/16 v18, 0x0

    .line 772
    .local v18, "f":Ljava/io/File;
    const/16 v19, 0x0

    .line 773
    .local v19, "fileIconUri":Landroid/net/Uri;
    const/16 v26, -0x1

    .line 774
    .local v26, "resId":I
    const/4 v14, 0x0

    .line 775
    .local v14, "date":Ljava/lang/String;
    const/16 v22, 0x0

    .line 776
    .local v22, "fileSize":Ljava/lang/String;
    const/16 v32, 0x0

    .line 777
    .local v32, "uri":Landroid/net/Uri;
    const/16 v25, 0x0

    .line 778
    .local v25, "mimeType":Ljava/lang/String;
    const-string v29, "2"

    .line 779
    .local v29, "targetType":Ljava/lang/String;
    const/4 v11, 0x0

    .line 784
    .local v11, "column":[Ljava/lang/Object;
    :cond_6
    const/4 v2, 0x1

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 785
    const/16 v2, 0x2f

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v20

    .line 787
    new-instance v18, Ljava/io/File;

    .end local v18    # "f":Ljava/io/File;
    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 789
    .restart local v18    # "f":Ljava/io/File;
    const/16 v19, 0x0

    .line 791
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/myfiles/MediaFile;->getSmallIcon(Ljava/lang/String;)I

    move-result v26

    .line 792
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "android.resource://com.sec.android.app.myfiles/"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v26

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v19

    .line 796
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/provider/MyFilesProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->lastModified()J

    move-result-wide v6

    const-wide/16 v36, 0x3e8

    mul-long v6, v6, v36

    invoke-static {v2, v6, v7}, Lcom/sec/android/app/myfiles/utils/Utils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v14

    .line 799
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/provider/MyFilesProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/myfiles/utils/Utils;->getTotalSize(Ljava/io/File;)J

    move-result-wide v6

    invoke-static {v2, v6, v7}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v22

    .line 802
    new-instance v2, Ljava/io/File;

    move-object/from16 v0, v21

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v32

    .line 804
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/myfiles/MediaFile;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 807
    const-string v23, ""

    .line 809
    .local v23, "highlightedContent":Ljava/lang/String;
    const-string v2, "highlight_content"

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    const/4 v4, -0x1

    if-eq v2, v4, :cond_7

    .line 810
    const-string v2, "highlight_content"

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 812
    :cond_7
    const/16 v2, 0x9

    new-array v11, v2, [Ljava/lang/Object;

    .end local v11    # "column":[Ljava/lang/Object;
    const/4 v2, 0x0

    aput-object v19, v11, v2

    const/4 v2, 0x1

    aput-object v20, v11, v2

    const/4 v2, 0x2

    aput-object v14, v11, v2

    const/4 v2, 0x3

    aput-object v22, v11, v2

    const/4 v2, 0x4

    aput-object v21, v11, v2

    const/4 v2, 0x5

    aput-object v32, v11, v2

    const/4 v2, 0x6

    aput-object v25, v11, v2

    const/4 v2, 0x7

    aput-object v29, v11, v2

    const/16 v2, 0x8

    aput-object v23, v11, v2

    .line 823
    .restart local v11    # "column":[Ljava/lang/Object;
    invoke-virtual {v12, v11}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 824
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_6

    .line 827
    .end local v11    # "column":[Ljava/lang/Object;
    .end local v14    # "date":Ljava/lang/String;
    .end local v18    # "f":Ljava/io/File;
    .end local v19    # "fileIconUri":Landroid/net/Uri;
    .end local v20    # "fileName":Ljava/lang/String;
    .end local v21    # "filePath":Ljava/lang/String;
    .end local v22    # "fileSize":Ljava/lang/String;
    .end local v23    # "highlightedContent":Ljava/lang/String;
    .end local v25    # "mimeType":Ljava/lang/String;
    .end local v26    # "resId":I
    .end local v29    # "targetType":Ljava/lang/String;
    .end local v32    # "uri":Landroid/net/Uri;
    :cond_8
    if-eqz v13, :cond_9

    .line 828
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 831
    :cond_9
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 832
    const-string v2, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "search Index End Time : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v16

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 833
    const-string v2, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "search Index timetaken : "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sub-long v6, v16, v30

    const-wide/16 v36, 0x3e8

    div-long v6, v6, v36

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 105
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 16
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 112
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/provider/MyFilesProvider;->mDatabaseHelper:Lcom/sec/android/app/myfiles/provider/MyFilesProvider$DatabaseHelper;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/provider/MyFilesProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 114
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v3, 0x0

    .line 116
    .local v3, "table":Ljava/lang/String;
    const/4 v13, 0x0

    .line 118
    .local v13, "newUri":Landroid/net/Uri;
    const-wide/16 v14, 0x0

    .line 121
    .local v14, "rowID":J
    sget-object v4, Lcom/sec/android/app/myfiles/provider/MyFilesProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 281
    :cond_0
    :goto_0
    :pswitch_0
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 282
    return-object v13

    .line 125
    :pswitch_1
    if-nez p2, :cond_1

    .line 127
    const/4 v4, 0x0

    const-string v5, "MyFilesProvider"

    const-string v6, "insert() - content value is null"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 131
    :cond_1
    const-string v4, "_data"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_2

    .line 133
    const/4 v4, 0x0

    const-string v5, "MyFilesProvider"

    const-string v6, "insert() - data is empty"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 137
    :cond_2
    const-string v4, "file_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    if-nez v4, :cond_3

    .line 139
    const/4 v4, 0x0

    const-string v5, "MyFilesProvider"

    const-string v6, "insert() - file id is empty"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 144
    :cond_3
    const-string v4, "count_opened"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 146
    const-string v4, "date_used"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 149
    const-string v3, "folders"

    .line 153
    const/4 v4, 0x0

    :try_start_0
    move-object/from16 v0, p2

    invoke-virtual {v2, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v14

    .line 161
    :goto_1
    const-wide/16 v4, 0x0

    cmp-long v4, v14, v4

    if-lez v4, :cond_0

    .line 163
    invoke-static {v14, v15}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Folders;->getContentUri(J)Landroid/net/Uri;

    move-result-object v13

    goto :goto_0

    .line 155
    :catch_0
    move-exception v11

    .line 157
    .local v11, "ex":Ljava/lang/IllegalStateException;
    invoke-virtual {v11}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1

    .line 172
    .end local v11    # "ex":Ljava/lang/IllegalStateException;
    :pswitch_2
    if-nez p2, :cond_4

    .line 174
    const/4 v4, 0x0

    const-string v5, "MyFilesProvider"

    const-string v6, "insert() - content value is null"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 178
    :cond_4
    const-string v4, "_data"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_5

    .line 180
    const/4 v4, 0x0

    const-string v5, "MyFilesProvider"

    const-string v6, "insert() - data is empty"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 184
    :cond_5
    const-string v4, "file_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    if-nez v4, :cond_6

    .line 186
    const/4 v4, 0x0

    const-string v5, "MyFilesProvider"

    const-string v6, "insert() - file id is empty"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 190
    :cond_6
    const-string v4, "date_used"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    if-nez v4, :cond_7

    .line 192
    const-string v4, "date_used"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 198
    :cond_7
    const-string v3, "files"

    .line 201
    const/4 v10, 0x0

    .line 203
    .local v10, "cursor":Landroid/database/Cursor;
    :try_start_1
    const-string v9, "date_used DESC LIMIT 10"

    .line 204
    .local v9, "orderBy":Ljava/lang/String;
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 205
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v4

    const/16 v5, 0xa

    if-ne v4, v5, :cond_8

    .line 206
    invoke-interface {v10}, Landroid/database/Cursor;->moveToLast()Z

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_8

    .line 207
    const/4 v4, 0x1

    new-array v12, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    invoke-interface {v10, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v10, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v12, v4

    .line 208
    .local v12, "fileId":[Ljava/lang/String;
    const-string v4, "_id=?"

    invoke-virtual {v2, v3, v4, v12}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 211
    .end local v12    # "fileId":[Ljava/lang/String;
    :cond_8
    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v2, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v14

    .line 212
    if-eqz v10, :cond_9

    .line 213
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    .line 220
    .end local v9    # "orderBy":Ljava/lang/String;
    :cond_9
    :goto_2
    const-wide/16 v4, 0x0

    cmp-long v4, v14, v4

    if-lez v4, :cond_0

    .line 222
    invoke-static {v14, v15}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Folders;->getContentUri(J)Landroid/net/Uri;

    move-result-object v13

    goto/16 :goto_0

    .line 214
    :catch_1
    move-exception v11

    .line 216
    .restart local v11    # "ex":Ljava/lang/IllegalStateException;
    invoke-virtual {v11}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_2

    .line 229
    .end local v10    # "cursor":Landroid/database/Cursor;
    .end local v11    # "ex":Ljava/lang/IllegalStateException;
    :pswitch_3
    if-nez p2, :cond_a

    .line 231
    const/4 v4, 0x0

    const-string v5, "MyFilesProvider"

    const-string v6, "insert() - content value is null"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 235
    :cond_a
    const-string v4, "shortcut_index"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    if-nez v4, :cond_b

    .line 237
    const/4 v4, 0x0

    const-string v5, "MyFilesProvider"

    const-string v6, "insert() - shortcut_index is empty"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 241
    :cond_b
    const-string v4, "_data"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_c

    .line 243
    const/4 v4, 0x0

    const-string v5, "MyFilesProvider"

    const-string v6, "insert() - shortcut_path is empty"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 246
    :cond_c
    const-string v4, "title"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_d

    .line 248
    const/4 v4, 0x0

    const-string v5, "MyFilesProvider"

    const-string v6, "insert() - shortcut_title is empty"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 251
    :cond_d
    const-string v4, "type"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    if-nez v4, :cond_e

    .line 253
    const/4 v4, 0x0

    const-string v5, "MyFilesProvider"

    const-string v6, "insert() - shortcut_type is empty"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 261
    :cond_e
    const-string v3, "shortcuts"

    .line 265
    const/4 v4, 0x0

    :try_start_2
    move-object/from16 v0, p2

    invoke-virtual {v2, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-wide v14

    .line 273
    :goto_3
    const-wide/16 v4, 0x0

    cmp-long v4, v14, v4

    if-lez v4, :cond_0

    .line 275
    invoke-static {v14, v15}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Folders;->getContentUri(J)Landroid/net/Uri;

    move-result-object v13

    goto/16 :goto_0

    .line 267
    :catch_2
    move-exception v11

    .line 269
    .restart local v11    # "ex":Ljava/lang/IllegalStateException;
    invoke-virtual {v11}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_3

    .line 121
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public onCreate()Z
    .locals 5

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/provider/MyFilesProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 97
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Lcom/sec/android/app/myfiles/provider/MyFilesProvider$DatabaseHelper;

    const-string v2, "myfiles.db"

    const/4 v3, 0x0

    const/16 v4, 0x65

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/sec/android/app/myfiles/provider/MyFilesProvider$DatabaseHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/provider/MyFilesProvider;->mDatabaseHelper:Lcom/sec/android/app/myfiles/provider/MyFilesProvider$DatabaseHelper;

    .line 98
    const/4 v1, 0x0

    return v1
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 37
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 455
    new-instance v6, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v6}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 457
    .local v6, "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/provider/MyFilesProvider;->mDatabaseHelper:Lcom/sec/android/app/myfiles/provider/MyFilesProvider$DatabaseHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/provider/MyFilesProvider$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    .line 459
    .local v7, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v11, 0x0

    .line 461
    .local v11, "groupBy":Ljava/lang/String;
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v31

    .line 463
    .local v31, "rowId":Ljava/lang/Long;
    sget-object v2, Lcom/sec/android/app/myfiles/provider/MyFilesProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 658
    :goto_0
    :pswitch_0
    const/16 v18, 0x0

    .line 660
    .local v18, "cursor":Landroid/database/Cursor;
    const/4 v12, 0x0

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move-object/from16 v10, p4

    move-object/from16 v13, p5

    :try_start_0
    invoke-virtual/range {v6 .. v13}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v18

    :goto_1
    move-object/from16 v36, v18

    .line 664
    .end local v6    # "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v7    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v18    # "cursor":Landroid/database/Cursor;
    :goto_2
    return-object v36

    .line 467
    .restart local v6    # "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v7    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :pswitch_1
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->decode(Ljava/lang/String;)Ljava/lang/Long;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v31

    .line 471
    :goto_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 474
    :pswitch_2
    const-string v2, "folders"

    invoke-virtual {v6, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 468
    :catch_0
    move-exception v24

    .line 469
    .local v24, "ex":Ljava/lang/NumberFormatException;
    invoke-virtual/range {v24 .. v24}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_3

    .line 479
    .end local v24    # "ex":Ljava/lang/NumberFormatException;
    :pswitch_3
    :try_start_2
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->decode(Ljava/lang/String;)Ljava/lang/Long;
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v31

    .line 483
    :goto_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 486
    :pswitch_4
    const-string v2, "files"

    invoke-virtual {v6, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 480
    :catch_1
    move-exception v24

    .line 481
    .restart local v24    # "ex":Ljava/lang/NumberFormatException;
    invoke-virtual/range {v24 .. v24}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_4

    .line 490
    .end local v24    # "ex":Ljava/lang/NumberFormatException;
    :pswitch_5
    :try_start_3
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->decode(Ljava/lang/String;)Ljava/lang/Long;
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_2

    move-result-object v31

    .line 494
    :goto_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 497
    :pswitch_6
    const-string v2, "shortcuts"

    invoke-virtual {v6, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 491
    :catch_2
    move-exception v24

    .line 492
    .restart local v24    # "ex":Ljava/lang/NumberFormatException;
    invoke-virtual/range {v24 .. v24}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_5

    .line 502
    .end local v24    # "ex":Ljava/lang/NumberFormatException;
    :pswitch_7
    const/4 v2, 0x0

    const-string v3, "MyFilesProvider"

    const-string v4, "FINDO_ID"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 504
    new-instance v28, Lcom/sec/android/app/myfiles/provider/QueryParser;

    invoke-direct/range {v28 .. v28}, Lcom/sec/android/app/myfiles/provider/QueryParser;-><init>()V

    .line 505
    .local v28, "qParser":Lcom/sec/android/app/myfiles/provider/QueryParser;
    const/16 v29, 0x0

    .line 507
    .local v29, "result":[Ljava/lang/String;
    if-eqz p4, :cond_0

    .line 509
    const/4 v2, 0x0

    aget-object v2, p4, v2

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Lcom/sec/android/app/myfiles/provider/QueryParser;->regexParser(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v29

    .line 512
    :cond_0
    const/4 v5, 0x0

    .line 513
    .local v5, "where":Ljava/lang/String;
    const-string v20, "(SUBSTR(LOWER(_data),LENGTH(RTRIM(LOWER(_data),\"abcdefghijklmnopqrstuvwxyz01234567890;[] .!@#$%^&()-_=+\"))) LIKE ? ) "

    .line 514
    .local v20, "dataPaser":Ljava/lang/String;
    const-string v27, "(SUBSTR(LOWER(_data),LENGTH(RTRIM(LOWER(_data),\"abcdefghijklmnopqrstuvwxyz01234567890;[] .!@#$%^&()-_=+\"))) LIKE \'/.%\')"

    .line 515
    .local v27, "noSearchHiddenFile":Ljava/lang/String;
    const-string v35, " OR (title LIKE ? )"

    .line 516
    .local v35, "titleData":Ljava/lang/String;
    const-string v21, " OR (_display_name LIKE ? )"

    .line 517
    .local v21, "displayNameData":Ljava/lang/String;
    new-instance v33, Ljava/util/ArrayList;

    invoke-direct/range {v33 .. v33}, Ljava/util/ArrayList;-><init>()V

    .line 519
    .local v33, "selectinos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/16 v26, 0x0

    .line 520
    .local v26, "isSearchedTime":Z
    const-string v2, "stime"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    .line 521
    .local v32, "sTime":Ljava/lang/String;
    const-string v2, "etime"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 522
    .local v22, "eTime":Ljava/lang/String;
    const-wide/16 v2, -0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v34

    .line 523
    .local v34, "startTime":Ljava/lang/Long;
    const-wide/16 v2, -0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    .line 525
    .local v23, "endTime":Ljava/lang/Long;
    if-eqz v32, :cond_1

    if-eqz v22, :cond_1

    .line 527
    invoke-static/range {v32 .. v32}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v8, 0x3e8

    div-long/2addr v2, v8

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v34

    .line 528
    invoke-static/range {v22 .. v22}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v8, 0x3e8

    div-long/2addr v2, v8

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    .line 531
    if-nez v29, :cond_4

    .line 533
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "( date_modified BETWEEN "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " )"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 542
    :cond_1
    :goto_6
    if-eqz v29, :cond_9

    .line 544
    if-nez v26, :cond_2

    .line 546
    if-eqz v5, :cond_5

    .line 547
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 552
    :cond_2
    :goto_7
    move-object/from16 v0, v29

    array-length v2, v0

    const/4 v3, 0x1

    if-le v2, v3, :cond_7

    .line 554
    const/4 v2, 0x0

    const-string v3, "MyFilesProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "result.length = "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v29

    array-length v8, v0

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 556
    if-eqz v5, :cond_6

    .line 557
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v35

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 561
    :goto_8
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v3, v29, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v33

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 562
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v3, v29, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v33

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 563
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v3, v29, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v33

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 565
    const/4 v2, 0x0

    const-string v3, "MyFilesProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "result[0] = "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v8, 0x0

    aget-object v8, v29, v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 566
    const/16 v25, 0x1

    .local v25, "i":I
    :goto_9
    move-object/from16 v0, v29

    array-length v2, v0

    move/from16 v0, v25

    if-ge v0, v2, :cond_8

    .line 568
    rem-int/lit8 v2, v25, 0x2

    if-nez v2, :cond_3

    .line 570
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v35

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 572
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v29, v25

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v33

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 573
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v29, v25

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v33

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 574
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v29, v25

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v33

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 576
    const/4 v2, 0x0

    const-string v3, "MyFilesProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "result[i] = "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v8, v29, v25

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 566
    :cond_3
    add-int/lit8 v25, v25, 0x1

    goto/16 :goto_9

    .line 537
    .end local v25    # "i":I
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "( date_modified BETWEEN "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v23

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ) AND ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 538
    const/16 v26, 0x1

    goto/16 :goto_6

    .line 549
    :cond_5
    const-string v5, "("

    goto/16 :goto_7

    .line 559
    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v35

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_8

    .line 582
    :cond_7
    if-eqz v5, :cond_f

    .line 583
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v35

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 587
    :goto_a
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v3, v29, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v33

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 588
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v3, v29, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v33

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 589
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v3, v29, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v33

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 592
    :cond_8
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 595
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/provider/MyFilesProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowHiddenFileStatus()Z

    move-result v2

    if-nez v2, :cond_a

    .line 597
    if-eqz v5, :cond_10

    .line 598
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND NOT "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 603
    :cond_a
    :goto_b
    const/16 v19, 0x0

    .line 604
    .local v19, "cursorForFindo":Landroid/database/Cursor;
    invoke-virtual/range {v33 .. v33}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_11

    .line 606
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/provider/MyFilesProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "external"

    invoke-static {v3}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual/range {v33 .. v33}, Ljava/util/ArrayList;->size()I

    move-result v8

    new-array v8, v8, [Ljava/lang/String;

    move-object/from16 v0, v33

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Ljava/lang/String;

    move-object v6, v8

    check-cast v6, [Ljava/lang/String;

    .end local v6    # "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    move-object/from16 v7, p5

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .end local v7    # "db":Landroid/database/sqlite/SQLiteDatabase;
    move-result-object v19

    .line 616
    :goto_c
    if-eqz v19, :cond_12

    .line 618
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/provider/MyFilesProvider;->getSearchCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v30

    check-cast v30, Landroid/database/MatrixCursor;

    .line 620
    .local v30, "resultCursor":Landroid/database/MatrixCursor;
    const/4 v15, 0x0

    .line 621
    .local v15, "contentCursor":Landroid/database/MatrixCursor;
    if-eqz p4, :cond_b

    move-object/from16 v0, p4

    array-length v2, v0

    if-lez v2, :cond_b

    .line 623
    const/4 v2, 0x0

    aget-object v7, p4, v2

    invoke-virtual/range {v34 .. v34}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    move-object/from16 v6, p0

    invoke-virtual/range {v6 .. v11}, Lcom/sec/android/app/myfiles/provider/MyFilesProvider;->getContentSearchCursor(Ljava/lang/String;JJ)Landroid/database/Cursor;

    move-result-object v15

    .end local v15    # "contentCursor":Landroid/database/MatrixCursor;
    check-cast v15, Landroid/database/MatrixCursor;

    .line 626
    .restart local v15    # "contentCursor":Landroid/database/MatrixCursor;
    :cond_b
    const/16 v16, 0x0

    .line 627
    .local v16, "count":I
    if-eqz v30, :cond_c

    invoke-virtual/range {v30 .. v30}, Landroid/database/MatrixCursor;->getCount()I

    move-result v2

    if-lez v2, :cond_c

    .line 628
    add-int/lit8 v16, v16, 0x1

    .line 630
    :cond_c
    if-eqz v15, :cond_d

    invoke-virtual {v15}, Landroid/database/MatrixCursor;->getCount()I

    move-result v2

    if-lez v2, :cond_d

    .line 631
    add-int/lit8 v16, v16, 0x1

    .line 633
    :cond_d
    move/from16 v0, v16

    new-array v14, v0, [Landroid/database/Cursor;

    .line 635
    .local v14, "arrCursor":[Landroid/database/Cursor;
    const/16 v16, 0x0

    .line 636
    if-eqz v30, :cond_14

    invoke-virtual/range {v30 .. v30}, Landroid/database/MatrixCursor;->getCount()I

    move-result v2

    if-lez v2, :cond_14

    .line 637
    add-int/lit8 v17, v16, 0x1

    .end local v16    # "count":I
    .local v17, "count":I
    aput-object v30, v14, v16

    .line 639
    :goto_d
    if-eqz v15, :cond_13

    invoke-virtual {v15}, Landroid/database/MatrixCursor;->getCount()I

    move-result v2

    if-lez v2, :cond_13

    .line 640
    add-int/lit8 v16, v17, 0x1

    .end local v17    # "count":I
    .restart local v16    # "count":I
    aput-object v15, v14, v17

    .line 643
    :goto_e
    const/16 v36, 0x0

    .line 645
    .local v36, "tmpCursor":Landroid/database/Cursor;
    if-lez v16, :cond_e

    .line 646
    new-instance v36, Landroid/database/MergeCursor;

    .end local v36    # "tmpCursor":Landroid/database/Cursor;
    move-object/from16 v0, v36

    invoke-direct {v0, v14}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .line 648
    .restart local v36    # "tmpCursor":Landroid/database/Cursor;
    :cond_e
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    .line 585
    .end local v14    # "arrCursor":[Landroid/database/Cursor;
    .end local v15    # "contentCursor":Landroid/database/MatrixCursor;
    .end local v16    # "count":I
    .end local v19    # "cursorForFindo":Landroid/database/Cursor;
    .end local v30    # "resultCursor":Landroid/database/MatrixCursor;
    .end local v36    # "tmpCursor":Landroid/database/Cursor;
    .restart local v6    # "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v7    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :cond_f
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v35

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_a

    .line 600
    :cond_10
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NOT "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v27

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_b

    .line 611
    .restart local v19    # "cursorForFindo":Landroid/database/Cursor;
    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/provider/MyFilesProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "external"

    invoke-static {v3}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object/from16 v7, p5

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .end local v6    # "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v7    # "db":Landroid/database/sqlite/SQLiteDatabase;
    move-result-object v19

    goto/16 :goto_c

    .line 655
    .end local v5    # "where":Ljava/lang/String;
    .end local v19    # "cursorForFindo":Landroid/database/Cursor;
    .end local v20    # "dataPaser":Ljava/lang/String;
    .end local v21    # "displayNameData":Ljava/lang/String;
    .end local v22    # "eTime":Ljava/lang/String;
    .end local v23    # "endTime":Ljava/lang/Long;
    .end local v26    # "isSearchedTime":Z
    .end local v27    # "noSearchHiddenFile":Ljava/lang/String;
    .end local v28    # "qParser":Lcom/sec/android/app/myfiles/provider/QueryParser;
    .end local v29    # "result":[Ljava/lang/String;
    .end local v32    # "sTime":Ljava/lang/String;
    .end local v33    # "selectinos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v34    # "startTime":Ljava/lang/Long;
    .end local v35    # "titleData":Ljava/lang/String;
    :cond_12
    :pswitch_8
    const/16 v36, 0x0

    goto/16 :goto_2

    .line 661
    .restart local v6    # "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v7    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .restart local v18    # "cursor":Landroid/database/Cursor;
    :catch_3
    move-exception v24

    .line 662
    .local v24, "ex":Ljava/lang/IllegalStateException;
    invoke-virtual/range {v24 .. v24}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto/16 :goto_1

    .end local v6    # "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v7    # "db":Landroid/database/sqlite/SQLiteDatabase;
    .end local v18    # "cursor":Landroid/database/Cursor;
    .end local v24    # "ex":Ljava/lang/IllegalStateException;
    .restart local v5    # "where":Ljava/lang/String;
    .restart local v14    # "arrCursor":[Landroid/database/Cursor;
    .restart local v15    # "contentCursor":Landroid/database/MatrixCursor;
    .restart local v17    # "count":I
    .restart local v19    # "cursorForFindo":Landroid/database/Cursor;
    .restart local v20    # "dataPaser":Ljava/lang/String;
    .restart local v21    # "displayNameData":Ljava/lang/String;
    .restart local v22    # "eTime":Ljava/lang/String;
    .restart local v23    # "endTime":Ljava/lang/Long;
    .restart local v26    # "isSearchedTime":Z
    .restart local v27    # "noSearchHiddenFile":Ljava/lang/String;
    .restart local v28    # "qParser":Lcom/sec/android/app/myfiles/provider/QueryParser;
    .restart local v29    # "result":[Ljava/lang/String;
    .restart local v30    # "resultCursor":Landroid/database/MatrixCursor;
    .restart local v32    # "sTime":Ljava/lang/String;
    .restart local v33    # "selectinos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v34    # "startTime":Ljava/lang/Long;
    .restart local v35    # "titleData":Ljava/lang/String;
    :cond_13
    move/from16 v16, v17

    .end local v17    # "count":I
    .restart local v16    # "count":I
    goto :goto_e

    :cond_14
    move/from16 v17, v16

    .end local v16    # "count":I
    .restart local v17    # "count":I
    goto :goto_d

    .line 463
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_8
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_7
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 375
    iget-object v3, p0, Lcom/sec/android/app/myfiles/provider/MyFilesProvider;->mDatabaseHelper:Lcom/sec/android/app/myfiles/provider/MyFilesProvider$DatabaseHelper;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/provider/MyFilesProvider$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 381
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    sget-object v3, Lcom/sec/android/app/myfiles/provider/MyFilesProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 445
    :pswitch_0
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "MyFilesProvider::update() - Unknown URI"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 385
    :pswitch_1
    const-string v3, "folders"

    invoke-virtual {v1, v3, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 447
    .local v0, "count":I
    :goto_0
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 448
    return v0

    .line 392
    .end local v0    # "count":I
    :pswitch_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 394
    .local v2, "finalSelection":Ljava/lang/String;
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 396
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 399
    :cond_0
    const-string v3, "folders"

    invoke-virtual {v1, v3, p2, v2, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 401
    .restart local v0    # "count":I
    goto :goto_0

    .line 406
    .end local v0    # "count":I
    .end local v2    # "finalSelection":Ljava/lang/String;
    :pswitch_3
    const-string v3, "files"

    invoke-virtual {v1, v3, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 408
    .restart local v0    # "count":I
    goto :goto_0

    .line 413
    .end local v0    # "count":I
    :pswitch_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 415
    .restart local v2    # "finalSelection":Ljava/lang/String;
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 417
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 420
    :cond_1
    const-string v3, "files"

    invoke-virtual {v1, v3, p2, v2, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 422
    .restart local v0    # "count":I
    goto/16 :goto_0

    .line 425
    .end local v0    # "count":I
    .end local v2    # "finalSelection":Ljava/lang/String;
    :pswitch_5
    const-string v3, "shortcuts"

    invoke-virtual {v1, v3, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 427
    .restart local v0    # "count":I
    goto/16 :goto_0

    .line 432
    .end local v0    # "count":I
    :pswitch_6
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 434
    .restart local v2    # "finalSelection":Ljava/lang/String;
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 436
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 439
    :cond_2
    const-string v3, "shortcuts"

    invoke-virtual {v1, v3, p2, v2, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 441
    .restart local v0    # "count":I
    goto/16 :goto_0

    .line 381
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
