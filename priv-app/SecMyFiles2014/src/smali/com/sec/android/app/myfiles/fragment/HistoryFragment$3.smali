.class Lcom/sec/android/app/myfiles/fragment/HistoryFragment$3;
.super Ljava/lang/Object;
.source "HistoryFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;)V
    .locals 0

    .prologue
    .line 597
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 10
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 602
    .local p1, "parentView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v1, :cond_0

    .line 604
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->isSearchMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 606
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/MainActivity;->finishSearchMode()V

    .line 612
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->isSelectMode()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 614
    const v1, 0x7f0f0037

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 616
    .local v7, "checkBoxView":Landroid/view/View;
    if-eqz v7, :cond_1

    .line 618
    const/4 v1, 0x0

    invoke-virtual {v7, v1}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    .line 620
    invoke-virtual {v7}, Landroid/view/View;->performClick()Z

    .line 672
    .end local v7    # "checkBoxView":Landroid/view/View;
    :cond_1
    :goto_0
    return-void

    .line 626
    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;

    iget-object v8, v1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mData:Landroid/os/Bundle;

    .line 628
    .local v8, "dataBundle":Landroid/os/Bundle;
    if-eqz v8, :cond_1

    .line 630
    const-string v1, "category"

    invoke-virtual {v8, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/app/myfiles/element/HistoryItem$Category;->getCategory(I)Lcom/sec/android/app/myfiles/element/HistoryItem$Category;

    move-result-object v0

    .line 632
    .local v0, "category":Lcom/sec/android/app/myfiles/element/HistoryItem$Category;
    const-string v1, "data"

    invoke-virtual {v8, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 634
    .local v5, "data":Ljava/lang/String;
    const-string v1, "name"

    invoke-virtual {v8, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 636
    .local v4, "name":Ljava/lang/String;
    const-string v1, "source"

    invoke-virtual {v8, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 638
    .local v6, "source":Ljava/lang/String;
    const-string v1, "external_id"

    invoke-virtual {v8, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 640
    .local v2, "externalId":J
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$8;->$SwitchMap$com$sec$android$app$myfiles$element$HistoryItem$Category:[I

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/element/HistoryItem$Category;->ordinal()I

    move-result v9

    aget v1, v1, v9

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 644
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;)Landroid/content/Context;

    move-result-object v1

    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;)Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    invoke-virtual {v9, v5}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v9

    # invokes: Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->startActivityForIntent(Landroid/content/Context;Landroid/content/Intent;)V
    invoke-static {v1, v9}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->access$200(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 654
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    # invokes: Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->openIfFileExists(Ljava/lang/String;)Z
    invoke-static {v1, v5}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 656
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    # invokes: Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->showDeleteOfRetryDownloadDialog(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static/range {v1 .. v6}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 640
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
