.class Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter$2;
.super Ljava/lang/Object;
.source "ShortcutAdapter.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;

.field final synthetic val$item:Lcom/sec/android/app/myfiles/element/ShortcutItem;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;Lcom/sec/android/app/myfiles/element/ShortcutItem;)V
    .locals 0

    .prologue
    .line 368
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;

    iput-object p2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter$2;->val$item:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 373
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter$2;->val$item:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->isSelectable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 375
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter$2;->val$item:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->setSelected(Z)V

    .line 377
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;)Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v0, :cond_0

    .line 379
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter$2;->this$0:Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/MainActivity;->invalidateOptionsMenu()V

    .line 382
    :cond_0
    return-void
.end method
