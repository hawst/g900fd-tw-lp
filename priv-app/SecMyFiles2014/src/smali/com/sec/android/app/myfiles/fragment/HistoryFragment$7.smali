.class Lcom/sec/android/app/myfiles/fragment/HistoryFragment$7;
.super Landroid/database/ContentObserver;
.source "HistoryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/HistoryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 945
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 4
    .param p1, "selfChange"    # Z

    .prologue
    .line 950
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 951
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->deletedCount:J
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->access$800(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 952
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    # operator-- for: Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->deletedCount:J
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->access$810(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;)J

    .line 956
    :cond_0
    :goto_0
    return-void

    .line 953
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->isSelectMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 954
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->AsyncOpen()V

    goto :goto_0
.end method
