.class Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$4;
.super Ljava/lang/Object;
.source "AllFileBrowserNavigation.java"

# interfaces
.implements Ljava/io/FileFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->searchAndroidFolder(Ljava/lang/String;III)Lcom/sec/android/app/myfiles/SortCursor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;)V
    .locals 0

    .prologue
    .line 1033
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$4;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public accept(Ljava/io/File;)Z
    .locals 8
    .param p1, "pathname"    # Ljava/io/File;

    .prologue
    const/4 v5, 0x0

    .line 1038
    if-nez p1, :cond_1

    .line 1114
    :cond_0
    :goto_0
    return v5

    .line 1043
    :cond_1
    const/4 v2, 0x0

    .line 1045
    .local v2, "result":Z
    const/4 v4, 0x0

    .line 1047
    .local v4, "showFileByMimeType":Z
    const/4 v3, 0x0

    .line 1049
    .local v3, "showFileByExtension":Z
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1051
    const/4 v2, 0x1

    .line 1114
    :cond_2
    :goto_1
    if-nez v2, :cond_3

    if-nez v3, :cond_3

    if-eqz v4, :cond_0

    :cond_3
    const/4 v5, 0x1

    goto :goto_0

    .line 1055
    :cond_4
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 1057
    .local v0, "extension":Ljava/lang/String;
    const/16 v6, 0x2e

    invoke-virtual {v0, v6}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    sget-object v7, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v6, v7}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 1059
    const-string v6, "SCC"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1061
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$4;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mContext:Landroid/content/Context;

    invoke-static {v6, v7}, Lcom/sec/android/app/myfiles/MediaFile;->getMimeTypeFromMediaStore(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 1063
    .local v1, "mimetype":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 1068
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$4;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mMimetypeForFilter:Ljava/util/HashSet;

    invoke-virtual {v6}, Ljava/util/HashSet;->size()I

    move-result v6

    if-lez v6, :cond_5

    .line 1070
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$4;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mMimetypeForFilter:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    .line 1073
    :cond_5
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$4;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v6}, Ljava/util/HashSet;->size()I

    move-result v6

    if-lez v6, :cond_6

    .line 1075
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$4;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v6, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    .line 1078
    :cond_6
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$4;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mExceptFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v6}, Ljava/util/HashSet;->size()I

    move-result v6

    if-lez v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$4;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mExceptFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v6, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1081
    const/4 v3, 0x0

    goto :goto_1

    .line 1086
    .end local v1    # "mimetype":Ljava/lang/String;
    :cond_7
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$4;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v6}, Ljava/util/HashSet;->size()I

    move-result v6

    if-lez v6, :cond_9

    .line 1088
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/myfiles/MediaFile;->needToCheckMimeType(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1090
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$4;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mContext:Landroid/content/Context;

    invoke-static {v6, v7}, Lcom/sec/android/app/myfiles/MediaFile;->isAudioInMediaStore(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1092
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "_A"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1096
    :cond_8
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$4;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v6, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    .line 1099
    :cond_9
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$4;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mMimetypeForFilter:Ljava/util/HashSet;

    invoke-virtual {v6}, Ljava/util/HashSet;->size()I

    move-result v6

    if-lez v6, :cond_a

    .line 1101
    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/myfiles/MediaFile;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1103
    .restart local v1    # "mimetype":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$4;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mMimetypeForFilter:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    .line 1106
    .end local v1    # "mimetype":Ljava/lang/String;
    :cond_a
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$4;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mExceptFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v6}, Ljava/util/HashSet;->size()I

    move-result v6

    if-lez v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$4;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mExceptFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v6, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1109
    const/4 v3, 0x0

    goto/16 :goto_1
.end method
