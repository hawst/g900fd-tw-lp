.class public final Lcom/sec/android/app/myfiles/utils/SearchParameter;
.super Ljava/lang/Object;
.source "SearchParameter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/utils/SearchParameter$1;,
        Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;
    }
.end annotation


# instance fields
.field private final mDateFrom:J

.field private final mDateTo:J

.field private final mExtentionString:Ljava/lang/String;

.field private final mFileTypes:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/sec/android/app/myfiles/utils/FileType;",
            ">;"
        }
    .end annotation
.end field

.field private final mKeyword:Ljava/lang/String;

.field private final mSearchType:I

.field private final mStorageType:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;)V
    .locals 2
    .param p1, "builder"    # Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iget-object v0, p1, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->mKeyword:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter;->mKeyword:Ljava/lang/String;

    .line 24
    iget v0, p1, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->mSearchType:I

    iput v0, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter;->mSearchType:I

    .line 25
    iget-object v0, p1, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->mStorageType:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter;->mStorageType:Ljava/util/ArrayList;

    .line 26
    iget-object v0, p1, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->mFileTypes:Ljava/util/EnumSet;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter;->mFileTypes:Ljava/util/EnumSet;

    .line 27
    iget-wide v0, p1, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->mDateFrom:J

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter;->mDateFrom:J

    .line 28
    iget-wide v0, p1, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->mDateTo:J

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter;->mDateTo:J

    .line 29
    iget-object v0, p1, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->mExtentionString:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter;->mExtentionString:Ljava/lang/String;

    .line 31
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;Lcom/sec/android/app/myfiles/utils/SearchParameter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;
    .param p2, "x1"    # Lcom/sec/android/app/myfiles/utils/SearchParameter$1;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;-><init>(Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;)V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/utils/SearchParameter;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/utils/SearchParameter;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter;->mKeyword:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/utils/SearchParameter;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/utils/SearchParameter;

    .prologue
    .line 21
    iget v0, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter;->mSearchType:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/utils/SearchParameter;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/utils/SearchParameter;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter;->mStorageType:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/utils/SearchParameter;)Ljava/util/EnumSet;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/utils/SearchParameter;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter;->mFileTypes:Ljava/util/EnumSet;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/utils/SearchParameter;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/utils/SearchParameter;

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter;->mDateFrom:J

    return-wide v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/myfiles/utils/SearchParameter;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/utils/SearchParameter;

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter;->mDateTo:J

    return-wide v0
.end method


# virtual methods
.method public getDateFrom()J
    .locals 2

    .prologue
    .line 46
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter;->mDateFrom:J

    return-wide v0
.end method

.method public getDateTo()J
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter;->mDateTo:J

    return-wide v0
.end method

.method public getExtentionString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter;->mExtentionString:Ljava/lang/String;

    return-object v0
.end method

.method public getFileTypes()Ljava/util/EnumSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/sec/android/app/myfiles/utils/FileType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter;->mFileTypes:Ljava/util/EnumSet;

    return-object v0
.end method

.method public getKeyword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter;->mKeyword:Ljava/lang/String;

    return-object v0
.end method

.method public getSearchType()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter;->mSearchType:I

    return v0
.end method

.method public getStorageType()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter;->mStorageType:Ljava/util/ArrayList;

    return-object v0
.end method
