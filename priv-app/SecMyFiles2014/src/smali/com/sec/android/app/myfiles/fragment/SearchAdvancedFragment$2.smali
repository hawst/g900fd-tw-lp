.class Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment$2;
.super Ljava/lang/Object;
.source "SearchAdvancedFragment.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;)V
    .locals 0

    .prologue
    .line 509
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 513
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 525
    :cond_0
    :goto_0
    return v0

    .line 515
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mNameSearchView:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 516
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->imm:Landroid/view/inputmethod/InputMethodManager;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mNameSearchView:Landroid/widget/EditText;

    invoke-virtual {v1, v2, v0}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto :goto_0

    .line 517
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mExtentionSearchView:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 518
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->imm:Landroid/view/inputmethod/InputMethodManager;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mExtentionSearchView:Landroid/widget/EditText;

    invoke-virtual {v1, v2, v0}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto :goto_0

    .line 521
    :pswitch_1
    const-string v2, "SearchAdvancedFragment"

    const-string v3, "kb hide "

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 522
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->imm:Landroid/view/inputmethod/InputMethodManager;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    goto :goto_0

    .line 513
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
