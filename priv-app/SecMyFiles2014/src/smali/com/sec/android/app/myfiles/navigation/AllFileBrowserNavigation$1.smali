.class Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$1;
.super Ljava/lang/Object;
.source "AllFileBrowserNavigation.java"

# interfaces
.implements Ljava/io/FileFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->getFilesInPath(Ljava/lang/String;III)Landroid/database/Cursor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;)V
    .locals 0

    .prologue
    .line 742
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public accept(Ljava/io/File;)Z
    .locals 2
    .param p1, "pathname"    # Ljava/io/File;

    .prologue
    .line 747
    if-nez p1, :cond_0

    .line 749
    const/4 v0, 0x0

    .line 756
    :goto_0
    return v0

    .line 751
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 753
    const/4 v0, 0x1

    goto :goto_0

    .line 756
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isForwardable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method
