.class public Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;
.super Ljava/lang/Object;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/utils/Utils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UsbStorage"
.end annotation


# instance fields
.field mMounted:Z

.field mStoragePath:Ljava/lang/String;

.field mStorageType:I


# direct methods
.method public constructor <init>(Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;)V
    .locals 1
    .param p1, "other"    # Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;

    .prologue
    .line 3515
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3517
    iget-object v0, p1, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->mStoragePath:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->mStoragePath:Ljava/lang/String;

    .line 3519
    iget-boolean v0, p1, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->mMounted:Z

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->mMounted:Z

    .line 3521
    iget v0, p1, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->mStorageType:I

    iput v0, p0, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->mStorageType:I

    .line 3522
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "mount"    # Z

    .prologue
    .line 3505
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3507
    iput-object p1, p0, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->mStoragePath:Ljava/lang/String;

    .line 3509
    iput-boolean p2, p0, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->mMounted:Z

    .line 3511
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    add-int/lit8 v0, v0, -0x41

    add-int/lit16 v0, v0, 0x603

    iput v0, p0, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->mStorageType:I

    .line 3512
    return-void
.end method


# virtual methods
.method public getStoragePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 3527
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->mStoragePath:Ljava/lang/String;

    return-object v0
.end method

.method public getStorageType()I
    .locals 1

    .prologue
    .line 3533
    iget v0, p0, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->mStorageType:I

    return v0
.end method

.method public isMounted()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 3539
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->mMounted:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public setMounted(Ljava/lang/Boolean;)V
    .locals 1
    .param p1, "mounted"    # Ljava/lang/Boolean;

    .prologue
    .line 3545
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/utils/Utils$UsbStorage;->mMounted:Z

    .line 3546
    return-void
.end method
