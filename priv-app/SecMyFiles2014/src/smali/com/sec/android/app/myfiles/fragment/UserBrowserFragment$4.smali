.class Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$4;
.super Ljava/lang/Object;
.source "UserBrowserFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->onCreateItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;)V
    .locals 0

    .prologue
    .line 1092
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 6
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1097
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 1099
    .local v0, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 1101
    .local v2, "path":Ljava/lang/String;
    if-eqz v0, :cond_4

    .line 1103
    const-string v5, "_data"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1105
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->isSelectMode()Z

    move-result v5

    if-nez v5, :cond_1

    .line 1107
    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isInternalRootFolder(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isExternalRootFolder(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isUsbStorageRootFolder(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxRootFolder(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 1110
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    invoke-virtual {v5, v3, p3, v4}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->startSelectMode(III)V

    .line 1114
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1116
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mCurrentListableView:Lcom/sec/android/app/myfiles/view/IDirectDragable;

    invoke-interface {v4, v3}, Lcom/sec/android/app/myfiles/view/IDirectDragable;->setDirectDragMode(Z)V

    .line 1142
    :goto_0
    return v3

    .line 1122
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mCurrentListableView:Lcom/sec/android/app/myfiles/view/IDirectDragable;

    invoke-interface {v5, v4}, Lcom/sec/android/app/myfiles/view/IDirectDragable;->setDirectDragMode(Z)V

    goto :goto_0

    .line 1130
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemPosition()Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1132
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->isShowTreeView()Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v3

    if-nez v3, :cond_3

    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mSideSyncControl:Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;->isKMSRunning(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mSideSyncControl:Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;

    invoke-static {}, Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;->isPSSRunning()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1134
    :cond_3
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1135
    .local v1, "mBundle":Landroid/os/Bundle;
    const-string v3, "ITEM_INITIATING_DRAG_PATH"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1136
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    invoke-virtual {v3, p2, v1}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->startDragMode(Landroid/view/View;Landroid/os/Bundle;)V

    .end local v1    # "mBundle":Landroid/os/Bundle;
    :cond_4
    move v3, v4

    .line 1142
    goto :goto_0
.end method
