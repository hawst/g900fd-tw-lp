.class public Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;
.super Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
.source "BaiduNavigation.java"


# static fields
.field private static final AUTHTOKEN_KEY:Ljava/lang/String; = "com.sec.android.cloudagent.baidu.credential.tokenkey"

.field private static final AUTHTOKEN_SECRET:Ljava/lang/String; = "com.sec.android.cloudagent.baidu.credential.name"

.field private static final CLOUD_ACTION_REQUEST_SIGNIN:Ljava/lang/String; = "com.sec.android.cloudagent.ACTION_REQUEST_SIGNIN"

.field private static final CLOUD_ACTION_RESPONSE_TOKEN:Ljava/lang/String; = "com.sec.android.cloudagent.ACTION_RESPONSE_TOKEN"

.field private static final CLOUD_ACTION_SIGNED_IN:Ljava/lang/String; = "com.sec.android.cloudagent.ACTION_ACCOUNT_SIGNED_IN"

.field private static final CLOUD_EXTRA_RESULT:Ljava/lang/String; = "result"

.field private static final FOLDER_DATA_INDEX:I = 0x1

.field private static final FOLDER_ID_INDEX:I = 0x0

.field private static final MODULE:Ljava/lang/String; = "BaiduNavigation"

.field private static final TAG:Ljava/lang/String; = "BaiduAPIHelper"

.field private static final mFilesProjection:[Ljava/lang/String;

.field private static final mFolderProjection:[Ljava/lang/String;


# instance fields
.field private CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

.field private Depth:I

.field private baiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

.field public cloudLoginReceiver:Landroid/content/BroadcastReceiver;

.field public mAsyncDBtask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mCurrentFolderID:I

.field private mFileUri:Landroid/net/Uri;

.field private mIsSyncing:Z

.field private mLoginReceiverIntentFilter:Landroid/content/IntentFilter;

.field private mLoginReceiverRegistered:Z

.field private mResigninRequested:Z

.field private mbAbort:Z

.field private mbGotoRoot:Z

.field private mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

.field private showDialog:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 106
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "_data"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mFolderProjection:[Ljava/lang/String;

    .line 111
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "_data"

    aput-object v1, v0, v3

    const-string v1, "date_modified"

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mFilesProjection:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 118
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;-><init>()V

    .line 74
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->Depth:I

    .line 93
    iput-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->baiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    .line 95
    iput-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->cloudLoginReceiver:Landroid/content/BroadcastReceiver;

    .line 97
    iput-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    .line 99
    iput-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mLoginReceiverIntentFilter:Landroid/content/IntentFilter;

    .line 101
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mLoginReceiverRegistered:Z

    .line 102
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->showDialog:Z

    .line 103
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mResigninRequested:Z

    .line 104
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mIsSyncing:Z

    .line 256
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mbGotoRoot:Z

    .line 119
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 122
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;-><init>(Landroid/content/Context;I)V

    .line 74
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->Depth:I

    .line 93
    iput-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->baiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    .line 95
    iput-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->cloudLoginReceiver:Landroid/content/BroadcastReceiver;

    .line 97
    iput-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    .line 99
    iput-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mLoginReceiverIntentFilter:Landroid/content/IntentFilter;

    .line 101
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mLoginReceiverRegistered:Z

    .line 102
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->showDialog:Z

    .line 103
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mResigninRequested:Z

    .line 104
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mIsSyncing:Z

    .line 256
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mbGotoRoot:Z

    .line 124
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->setAllAttachMode(Z)V

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mContentResolver:Landroid/content/ContentResolver;

    .line 128
    iput v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mCurrentFolderID:I

    .line 131
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mLoginReceiverIntentFilter:Landroid/content/IntentFilter;

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mLoginReceiverIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.cloudagent.ACTION_ACCOUNT_SIGNED_IN"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mLoginReceiverIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.cloudagent.ACTION_RESPONSE_TOKEN"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 135
    new-instance v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$1;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->cloudLoginReceiver:Landroid/content/BroadcastReceiver;

    .line 170
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->registerCloudLoginReceiver()V

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->baiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    .line 172
    return-void
.end method

.method private NewOpenAsyncTask()Landroid/os/AsyncTask;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 587
    new-instance v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$2;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;)V

    .line 609
    .local v0, "task":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/Void;Ljava/lang/Void;Ljava/lang/Void;>;"
    return-object v0
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;)Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->baiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->actionsOnSignIn()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mResigninRequested:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;
    .param p1, "x1"    # Z

    .prologue
    .line 57
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mResigninRequested:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;)Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;)Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    return-object v0
.end method

.method private actionsOnSignIn()V
    .locals 3

    .prologue
    .line 189
    const/4 v0, 0x2

    const-string v1, "BaiduNavigation"

    const-string v2, "actionsOnSignIn()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->baiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->requestGetToken(Landroid/content/Context;)V

    .line 191
    return-void
.end method

.method private createExceptFilteringWhere()Ljava/lang/String;
    .locals 4

    .prologue
    .line 983
    const-string v0, ""

    .line 985
    .local v0, "filteringWhere":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mExceptFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 988
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_data NOT LIKE ? AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 985
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 991
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 993
    const/4 v2, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1001
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " OR format = ? "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1003
    return-object v0

    .line 998
    :cond_1
    const-string v0, "_data NOT LIKE ?"

    goto :goto_1
.end method

.method private createExceptFilteringWhereArgs()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1008
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1010
    .local v1, "extensionResultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mExceptFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1013
    .local v0, "extension":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "%."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1016
    .end local v0    # "extension":Ljava/lang/String;
    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 1019
    const-string v3, "%."

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1022
    :cond_1
    const/16 v3, 0x3001

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1024
    return-object v1
.end method

.method private createFilteringWhere()Ljava/lang/String;
    .locals 4

    .prologue
    .line 937
    const-string v0, ""

    .line 939
    .local v0, "filteringWhere":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 942
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_data LIKE ? OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 939
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 945
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 947
    const/4 v2, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 955
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " OR format = ? "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 957
    return-object v0

    .line 952
    :cond_1
    const-string v0, "_data LIKE ?"

    goto :goto_1
.end method

.method private createFilteringWhereArgs()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 962
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 964
    .local v1, "extensionResultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 967
    .local v0, "extension":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "%."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 970
    .end local v0    # "extension":Ljava/lang/String;
    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 973
    const-string v3, "%."

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 976
    :cond_1
    const/16 v3, 0x3001

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 978
    return-object v1
.end method

.method private getDepthCount(Ljava/lang/String;)I
    .locals 3
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 565
    const/4 v0, 0x1

    .line 566
    .local v0, "Count":I
    if-eqz p1, :cond_0

    const-string v2, ""

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 567
    :cond_0
    const/4 v2, 0x1

    .line 579
    :goto_0
    return v2

    .line 572
    :cond_1
    const-string v2, "/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 574
    .local v1, "split":[Ljava/lang/String;
    array-length v2, v1

    if-gtz v2, :cond_2

    .line 575
    const/4 v0, 0x1

    :goto_1
    move v2, v0

    .line 579
    goto :goto_0

    .line 577
    :cond_2
    array-length v0, v1

    goto :goto_1
.end method

.method private getFilesInPath(Ljava/lang/String;III)Landroid/database/Cursor;
    .locals 16
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "sortBy"    # I
    .param p3, "inOrder"    # I
    .param p4, "target"    # I

    .prologue
    .line 378
    const/4 v7, 0x0

    .line 380
    .local v7, "where":Ljava/lang/String;
    const/4 v8, 0x0

    .line 382
    .local v8, "whereArgs":[Ljava/lang/String;
    const/4 v4, 0x0

    move/from16 v0, p2

    move/from16 v1, p3

    move/from16 v2, p4

    invoke-static {v0, v1, v2, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesDB;->getOrderBy(IIIZ)Ljava/lang/String;

    move-result-object v9

    .line 384
    .local v9, "orderBy":Ljava/lang/String;
    const-string v13, "format=12289"

    .line 386
    .local v13, "onlyFolder":Ljava/lang/String;
    const-string v12, "NOT format=12289"

    .line 389
    .local v12, "onlyFile":Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isAbsoluteRootFolder(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 391
    if-eqz p2, :cond_0

    if-eqz p2, :cond_3

    if-nez p4, :cond_3

    .line 393
    :cond_0
    new-instance v11, Landroid/database/MatrixCursor;

    sget-object v4, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mFilesProjection:[Ljava/lang/String;

    invoke-direct {v11, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 395
    .local v11, "mc":Landroid/database/MatrixCursor;
    new-instance v10, Ljava/io/File;

    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getRootFolder()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v10, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 397
    .local v10, "file":Ljava/io/File;
    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "0"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const/16 v6, 0x3001

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-virtual {v10}, Ljava/io/File;->lastModified()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v11, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 399
    sget-boolean v4, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v4, :cond_1

    .line 401
    new-instance v10, Ljava/io/File;

    .end local v10    # "file":Ljava/io/File;
    sget-object v4, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardStoragePath:Ljava/lang/String;

    invoke-direct {v10, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 403
    .restart local v10    # "file":Ljava/io/File;
    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "1"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "/storage/extSdCard"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const/16 v6, 0x3001

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-virtual {v10}, Ljava/io/File;->lastModified()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v11, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 447
    .end local v10    # "file":Ljava/io/File;
    .end local v11    # "mc":Landroid/database/MatrixCursor;
    :cond_1
    :goto_0
    return-object v11

    .line 409
    :cond_2
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isInternalRootFolder(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 411
    const-string v7, "_data LIKE ? AND parent=0"

    .line 413
    const/4 v4, 0x1

    new-array v8, v4, [Ljava/lang/String;

    .end local v8    # "whereArgs":[Ljava/lang/String;
    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v8, v4

    .line 433
    .restart local v8    # "whereArgs":[Ljava/lang/String;
    :cond_3
    :goto_1
    packed-switch p4, :pswitch_data_0

    .line 447
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "external"

    invoke-static {v5}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    goto :goto_0

    .line 415
    :cond_4
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isExternalRootFolder(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 417
    const-string v7, "_data LIKE ? AND parent=0"

    .line 419
    const/4 v4, 0x1

    new-array v8, v4, [Ljava/lang/String;

    .end local v8    # "whereArgs":[Ljava/lang/String;
    const/4 v4, 0x0

    const-string v5, "/storage/extSdCard%"

    aput-object v5, v8, v4

    .restart local v8    # "whereArgs":[Ljava/lang/String;
    goto :goto_1

    .line 421
    :cond_5
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxRootFolder(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 423
    const-string v7, "_data LIKE ? AND parent=0"

    .line 425
    const/4 v4, 0x1

    new-array v8, v4, [Ljava/lang/String;

    .end local v8    # "whereArgs":[Ljava/lang/String;
    const/4 v4, 0x0

    const-string v5, "/storage/SecretBox%"

    aput-object v5, v8, v4

    .restart local v8    # "whereArgs":[Ljava/lang/String;
    goto :goto_1

    .line 429
    :cond_6
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "parent="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mCurrentFolderID:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    .line 436
    :pswitch_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 437
    goto :goto_2

    .line 440
    :pswitch_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 441
    goto :goto_2

    .line 433
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getParentPathInBaidu()Ljava/lang/String;
    .locals 3

    .prologue
    .line 544
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 545
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mCurrentFolderPath:Ljava/lang/String;

    const/16 v2, 0x2f

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 546
    .local v0, "pos":I
    if-ltz v0, :cond_0

    .line 547
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mCurrentFolderPath:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 550
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 552
    .end local v0    # "pos":I
    :goto_1
    return-object v1

    .line 549
    .restart local v0    # "pos":I
    :cond_0
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mCurrentFolderPath:Ljava/lang/String;

    goto :goto_0

    .line 552
    .end local v0    # "pos":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mCurrentFolderPath:Ljava/lang/String;

    goto :goto_1
.end method

.method private declared-synchronized registerCloudLoginReceiver()V
    .locals 3

    .prologue
    .line 175
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mLoginReceiverIntentFilter:Landroid/content/IntentFilter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->cloudLoginReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mLoginReceiverRegistered:Z

    if-nez v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->cloudLoginReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mLoginReceiverIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 177
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mLoginReceiverRegistered:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 179
    :cond_0
    monitor-exit p0

    return-void

    .line 175
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized unregisterCloudLoginReceiver()V
    .locals 2

    .prologue
    .line 182
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->cloudLoginReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mLoginReceiverRegistered:Z

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->cloudLoginReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 184
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mLoginReceiverRegistered:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 186
    :cond_0
    monitor-exit p0

    return-void

    .line 182
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public AsyncOpen()V
    .locals 11

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x2

    .line 645
    new-instance v7, Lcom/sec/android/app/myfiles/db/CacheDB;

    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mContext:Landroid/content/Context;

    invoke-direct {v7, v0}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    .line 646
    .local v7, "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 647
    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/db/CacheDB;->getDropboxCursor()Ljava/lang/String;

    move-result-object v8

    .line 648
    .local v8, "delta":Ljava/lang/String;
    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 650
    const-string v0, "BaiduNavigation"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AsyncOpen needUpdate="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/Utils;->needUpdate:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 652
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/Utils;->needUpdate:Z

    if-eqz v0, :cond_3

    .line 653
    const-string v0, "BaiduNavigation"

    const-string v1, "it needs update"

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 655
    new-instance v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$5;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    new-instance v3, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$4;

    invoke-direct {v3, p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$4;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;)V

    move-object v1, p0

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$5;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;Lcom/sec/android/app/myfiles/fragment/AbsFragment;Ljava/lang/Runnable;ZZ)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    .line 674
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    if-eqz v0, :cond_2

    .line 675
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v10

    .line 676
    .local v10, "fManager":Landroid/app/FragmentManager;
    if-nez v10, :cond_0

    .line 677
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    .line 678
    .local v6, "activity":Landroid/app/Activity;
    if-eqz v6, :cond_0

    .line 679
    invoke-virtual {v6}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v10

    .line 682
    .end local v6    # "activity":Landroid/app/Activity;
    :cond_0
    if-eqz v10, :cond_1

    .line 683
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    const-string v1, "OpenOperation"

    invoke-virtual {v0, v10, v1}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 684
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->showDialog:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 756
    .end local v10    # "fManager":Landroid/app/FragmentManager;
    :cond_2
    :goto_0
    return-void

    .line 685
    .restart local v10    # "fManager":Landroid/app/FragmentManager;
    :catch_0
    move-exception v9

    .line 686
    .local v9, "ex":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    .line 687
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->showDialog:Z

    goto :goto_0

    .line 692
    .end local v9    # "ex":Ljava/lang/Exception;
    .end local v10    # "fManager":Landroid/app/FragmentManager;
    :cond_3
    if-nez v8, :cond_6

    .line 694
    const-string v0, "BaiduNavigation"

    const-string v1, "delta is null 1st time"

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 696
    new-instance v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$7;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    new-instance v3, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$6;

    invoke-direct {v3, p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$6;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;)V

    move-object v1, p0

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$7;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;Lcom/sec/android/app/myfiles/fragment/AbsFragment;Ljava/lang/Runnable;ZZ)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    .line 713
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    if-eqz v0, :cond_2

    .line 714
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v10

    .line 715
    .restart local v10    # "fManager":Landroid/app/FragmentManager;
    if-nez v10, :cond_4

    .line 716
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    .line 717
    .restart local v6    # "activity":Landroid/app/Activity;
    if-eqz v6, :cond_4

    .line 718
    invoke-virtual {v6}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v10

    .line 721
    .end local v6    # "activity":Landroid/app/Activity;
    :cond_4
    if-eqz v10, :cond_5

    .line 722
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    const-string v1, "OpenOperation"

    invoke-virtual {v0, v10, v1}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 723
    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->showDialog:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 724
    :catch_1
    move-exception v9

    .line 725
    .restart local v9    # "ex":Ljava/lang/Exception;
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    .line 726
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->showDialog:Z

    goto :goto_0

    .line 730
    .end local v9    # "ex":Ljava/lang/Exception;
    .end local v10    # "fManager":Landroid/app/FragmentManager;
    :cond_6
    const-string v0, "BaiduNavigation"

    const-string v1, "2nd time"

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 732
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    if-nez v0, :cond_7

    .line 734
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->NewOpenAsyncTask()Landroid/os/AsyncTask;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    .line 735
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    new-array v1, v5, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 736
    const-string v0, "BaiduNavigation"

    const-string v1, "mAsyncDBtask is null"

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 739
    :cond_7
    const-string v0, "BaiduNavigation"

    const-string v1, "mAsyncDBtask is not null"

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 740
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    invoke-virtual {v0}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_8

    .line 744
    const-string v0, "BaiduNavigation"

    const-string v1, "mAsyncDBtask Status.RUNNING"

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 746
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    invoke-virtual {v0}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_2

    .line 747
    const-string v0, "BaiduNavigation"

    const-string v1, "mAsyncDBtask Status.FINISHED"

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 749
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->NewOpenAsyncTask()Landroid/os/AsyncTask;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    .line 751
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v5, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0
.end method

.method protected AsyncOpenWithFullSync()V
    .locals 1

    .prologue
    .line 636
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mbAbort:Z

    .line 638
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->AsyncOpen()V

    .line 640
    return-void
.end method

.method public OnFragmentDestroy()V
    .locals 0

    .prologue
    .line 1028
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->unregisterCloudLoginReceiver()V

    .line 1029
    return-void
.end method

.method protected abortCurrentOperation()V
    .locals 1

    .prologue
    .line 631
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mbAbort:Z

    .line 632
    return-void
.end method

.method public getCurrentFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 540
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mCurrentFolderPath:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentFolderID()I
    .locals 3

    .prologue
    .line 364
    const/4 v0, 0x2

    const-string v1, "BaiduNavigation"

    const-string v2, "getCurrentFolderID()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 366
    iget v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mCurrentFolderID:I

    return v0
.end method

.method public getFilesInCurrentFolder()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->getFilesInFolder(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getFilesInFolder(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 458
    iget v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mShowFolderFileType:I

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->getFilesInFolder(Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getFilesInFolder(Ljava/lang/String;I)Landroid/database/Cursor;
    .locals 11
    .param p1, "folderPath"    # Ljava/lang/String;
    .param p2, "showFolderFileType"    # I

    .prologue
    const/4 v3, 0x2

    .line 470
    const-string v1, "BaiduNavigation"

    const-string v2, "getFilesInFolder()"

    invoke-static {v3, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 471
    new-instance v1, Lcom/sec/android/app/myfiles/db/CacheDB;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    .line 473
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 474
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->getDepthCount(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->Depth:I

    .line 475
    const/4 v8, 0x0

    .line 477
    .local v8, "c":Landroid/database/Cursor;
    const-string v6, ""

    .line 479
    .local v6, "where":Ljava/lang/String;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 481
    .local v10, "whereArgsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v7, 0x0

    .line 485
    .local v7, "whereArgs":[Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->isEnableFiltering()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 487
    const/4 v1, 0x2

    const-string v2, "BaiduNavigation"

    const-string v3, "isEnableFiltering is true"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 488
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->isAllAttachMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 490
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->createExceptFilteringWhere()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 492
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->createExceptFilteringWhereArgs()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 494
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, [Ljava/lang/String;

    move-object v7, v0

    .line 508
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    iget v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->Depth:I

    iget v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mCurrentSortBy:I

    iget v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mCurrentInOrder:I

    move-object v2, p1

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/app/myfiles/db/CacheDB;->getCloudDirListByFilter(Ljava/lang/String;IIILjava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 510
    if-nez v8, :cond_2

    .line 511
    new-instance v1, Lcom/samsung/scloud/exception/SCloudException;

    invoke-direct {v1}, Lcom/samsung/scloud/exception/SCloudException;-><init>()V

    throw v1
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 526
    :catch_0
    move-exception v9

    .line 528
    .local v9, "e":Lcom/samsung/scloud/exception/SCloudException;
    :try_start_1
    invoke-virtual {v9}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 530
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 533
    .end local v9    # "e":Lcom/samsung/scloud/exception/SCloudException;
    :goto_1
    return-object v8

    .line 500
    :cond_0
    :try_start_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->createFilteringWhere()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 502
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->createFilteringWhereArgs()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 504
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, [Ljava/lang/String;

    move-object v7, v0

    goto :goto_0

    .line 516
    :cond_1
    const/4 v1, 0x2

    const-string v2, "BaiduNavigation"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getCloudDirList folderPath="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 518
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    iget v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->Depth:I

    iget v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mCurrentSortBy:I

    iget v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mCurrentInOrder:I

    invoke-virtual {v1, p1, v2, v3, v4}, Lcom/sec/android/app/myfiles/db/CacheDB;->getCloudDirList(Ljava/lang/String;III)Landroid/database/Cursor;

    move-result-object v8

    .line 520
    if-nez v8, :cond_2

    .line 521
    new-instance v1, Lcom/samsung/scloud/exception/SCloudException;

    invoke-direct {v1}, Lcom/samsung/scloud/exception/SCloudException;-><init>()V

    throw v1
    :try_end_2
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 530
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    throw v1

    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    goto :goto_1
.end method

.method public getItemsInFolder(Ljava/lang/String;)I
    .locals 2
    .param p1, "dirpath"    # Ljava/lang/String;

    .prologue
    .line 559
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    iget v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->Depth:I

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->getCloudChildFileCount(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public goBack()Z
    .locals 6

    .prologue
    .line 321
    const/4 v3, 0x2

    const-string v4, "BaiduNavigation"

    const-string v5, "goBack()"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 323
    const/4 v1, 0x0

    .line 325
    .local v1, "result":Z
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mHistoricalPaths:Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;->pop()Ljava/lang/String;

    move-result-object v2

    .line 326
    .local v2, "upPath":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 327
    new-instance v0, Lcom/sec/android/app/myfiles/db/CacheDB;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    .line 328
    .local v0, "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 351
    invoke-virtual {v0, v2}, Lcom/sec/android/app/myfiles/db/CacheDB;->getIfDropboxFileExist(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 352
    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->getDepthCount(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->Depth:I

    .line 353
    iput-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 354
    const/4 v1, 0x1

    .line 357
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 360
    .end local v0    # "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    :cond_1
    return v1
.end method

.method public goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z
    .locals 3
    .param p1, "folderPath"    # Ljava/lang/String;
    .param p2, "isBack"    # Ljava/lang/Boolean;

    .prologue
    .line 243
    const/4 v0, 0x1

    .line 244
    .local v0, "result":Z
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->getDepthCount(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->Depth:I

    .line 246
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mHistoricalPaths:Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mCurrentFolderPath:Ljava/lang/String;

    if-nez v1, :cond_1

    const-string v1, "/"

    :goto_0
    invoke-virtual {v2, v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;->push(Ljava/lang/String;)V

    .line 247
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    if-eqz v1, :cond_0

    .line 248
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->setmIsSelector(Z)V

    .line 251
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 252
    return v0

    .line 246
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mCurrentFolderPath:Ljava/lang/String;

    goto :goto_0
.end method

.method public goToRoot(Ljava/lang/Boolean;)Z
    .locals 5
    .param p1, "isBack"    # Ljava/lang/Boolean;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x2

    .line 259
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->registerCloudLoginReceiver()V

    .line 260
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mResigninRequested:Z

    .line 261
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 262
    const-string v1, "BaiduNavigation"

    const-string v2, "goToRoot()"

    invoke-static {v3, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 263
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->baiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->isBaiduCloudAccountIsSignin()Z

    move-result v1

    if-nez v1, :cond_0

    .line 264
    const-string v1, "BaiduNavigation"

    const-string v2, "Baidu Accout SignIn is false"

    invoke-static {v3, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 265
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mResigninRequested:Z

    .line 266
    new-instance v0, Lcom/sec/android/app/myfiles/db/CacheDB;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    .line 267
    .local v0, "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 268
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->clearCloudData()J

    .line 269
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 270
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->baiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->requestSignIn(Landroid/content/Context;)V

    .line 282
    .end local v0    # "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    :goto_0
    return v4

    .line 273
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->baiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->isAuthKeySaved()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 275
    const-string v1, "BaiduNavigation"

    const-string v2, "goToRoot AsyncOpenWithFullSync"

    invoke-static {v3, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 276
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->AsyncOpenWithFullSync()V

    goto :goto_0

    .line 279
    :cond_1
    const-string v1, "BaiduNavigation"

    const-string v2, "goToRoot requestGetToken"

    invoke-static {v3, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 280
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->baiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->requestGetToken(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public goUp()Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    .line 288
    const-string v1, "BaiduNavigation"

    const-string v2, "goUp()"

    invoke-static {v3, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 290
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->Depth:I

    if-ne v1, v0, :cond_1

    .line 292
    :cond_0
    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mbAbort:Z

    .line 294
    const/4 v0, 0x0

    .line 314
    :goto_0
    return v0

    .line 300
    :cond_1
    iget v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->Depth:I

    if-ne v1, v3, :cond_2

    .line 302
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 304
    iput v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->Depth:I

    goto :goto_0

    .line 308
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->getParentPathInBaidu()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 310
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->getDepthCount(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->Depth:I

    goto :goto_0
.end method

.method public isCurrentFolderRoot()Z
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isRootFolder(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 195
    invoke-super {p0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->onPause()V

    .line 196
    const/4 v0, 0x2

    const-string v1, "BaiduNavigation"

    const-string v2, "onPause()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 197
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->unregisterCloudLoginReceiver()V

    .line 198
    return-void
.end method

.method public onResume()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 202
    invoke-super {p0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->onResume()V

    .line 203
    const-string v2, "BaiduNavigation"

    const-string v3, "onResume()"

    invoke-static {v6, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 204
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->showDialog:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    if-eqz v2, :cond_1

    .line 205
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->showDialog:Z

    .line 206
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 207
    .local v1, "fManager":Landroid/app/FragmentManager;
    if-nez v1, :cond_0

    .line 208
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 209
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 210
    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 212
    .end local v0    # "activity":Landroid/app/Activity;
    :cond_0
    if-eqz v1, :cond_1

    .line 213
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    const-string v3, "OpenOperation"

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 215
    .end local v1    # "fManager":Landroid/app/FragmentManager;
    :cond_1
    const-string v2, "BaiduNavigation"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mSignInRequested : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->baiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    sget-boolean v4, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->mSignInRequested:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 217
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->registerCloudLoginReceiver()V

    .line 218
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->baiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    sget-boolean v2, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->mSignInRequested:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->baiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->isBaiduCloudAccountIsSignin()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 219
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->baiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    sput-boolean v5, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->mSignInRequested:Z

    .line 220
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->actionsOnSignIn()V

    .line 222
    :cond_2
    return-void
.end method

.method public refreshNavigation()V
    .locals 0

    .prologue
    .line 229
    return-void
.end method

.method public setCurrentPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 464
    iput-object p1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 466
    return-void
.end method

.method public synclist(Z)V
    .locals 13
    .param p1, "bUsingTransaction"    # Z

    .prologue
    .line 765
    const/4 v9, 0x2

    :try_start_0
    const-string v10, "BaiduNavigation"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "SyncList Dropbox syncing bUsingTransaction = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 768
    iget-boolean v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mIsSyncing:Z

    if-eqz v9, :cond_0

    .line 769
    const/4 v9, 0x0

    const-string v10, "BaiduNavigation"

    const-string v11, "SyncList  Baidu syncing cancelling, syncing has already started"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 770
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mbAbort:Z

    .line 932
    :goto_0
    return-void

    .line 774
    :cond_0
    const/4 v9, 0x2

    const-string v10, "BaiduNavigation"

    const-string v11, "Baidu syncing has started"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 778
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mIsSyncing:Z

    .line 779
    const/4 v0, 0x0

    .line 780
    .local v0, "CloudDeltaCursor":Ljava/lang/String;
    new-instance v3, Lcom/sec/android/app/myfiles/db/CacheDB;

    iget-object v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mContext:Landroid/content/Context;

    invoke-direct {v3, v9}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    .line 781
    .local v3, "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 782
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->getDropboxCursor()Ljava/lang/String;

    move-result-object v0

    .line 783
    if-eqz v0, :cond_1

    const-string v9, ""

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 784
    :cond_1
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->clearCloudData()J

    .line 785
    :cond_2
    const/4 v7, 0x0

    .line 787
    .local v7, "nodes":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<Lcom/samsung/scloud/data/SCloudNode;>;"
    const/4 v1, 0x0

    .line 789
    .local v1, "SyncCount":I
    const/4 v2, 0x0

    .line 792
    .local v2, "TotalCount":I
    iget-boolean v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mbAbort:Z

    if-eqz v9, :cond_5

    .line 794
    const/4 v9, 0x2

    const-string v10, "BaiduNavigation"

    const-string v11, "SyncList Baidu syncing has End mbAbort is ture"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 795
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mIsSyncing:Z

    .line 797
    iget-object v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    if-eqz v9, :cond_3

    .line 798
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->showDialog:Z

    .line 799
    iget-object v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->setCompletedCondition(Z)V

    .line 801
    :cond_3
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mbAbort:Z

    .line 802
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 918
    .end local v0    # "CloudDeltaCursor":Ljava/lang/String;
    .end local v1    # "SyncCount":I
    .end local v2    # "TotalCount":I
    .end local v3    # "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    .end local v7    # "nodes":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<Lcom/samsung/scloud/data/SCloudNode;>;"
    :catch_0
    move-exception v4

    .line 921
    .local v4, "e":Ljava/lang/Exception;
    const/4 v9, 0x2

    const-string v10, "BaiduNavigation"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Baidu syncing exception "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 922
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 924
    iget-object v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    if-eqz v9, :cond_4

    .line 925
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->showDialog:Z

    .line 926
    iget-object v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->setCompletedCondition(Z)V

    .line 928
    :cond_4
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mIsSyncing:Z

    .line 929
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mbAbort:Z

    goto :goto_0

    .line 808
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v0    # "CloudDeltaCursor":Ljava/lang/String;
    .restart local v1    # "SyncCount":I
    .restart local v2    # "TotalCount":I
    .restart local v3    # "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    .restart local v7    # "nodes":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<Lcom/samsung/scloud/data/SCloudNode;>;"
    :cond_5
    move-object v8, v0

    .line 812
    .local v8, "workingDelta":Ljava/lang/String;
    :cond_6
    :try_start_1
    iget-boolean v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mbAbort:Z

    if-eqz v9, :cond_9

    .line 814
    const/4 v9, 0x2

    const-string v10, "BaiduNavigation"

    const-string v11, "Baidu mbAbort is ture break"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 815
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mbAbort:Z

    .line 906
    :cond_7
    :goto_1
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 907
    const/4 v9, 0x0

    const-string v10, "BaiduNavigation"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "synclist() needUpdate="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-boolean v12, Lcom/sec/android/app/myfiles/utils/Utils;->needUpdate:Z

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 909
    iget-object v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    if-eqz v9, :cond_8

    sget-boolean v9, Lcom/sec/android/app/myfiles/utils/Utils;->needUpdate:Z

    if-nez v9, :cond_8

    .line 910
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->showDialog:Z

    .line 911
    iget-object v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->setCompletedCondition(Z)V

    .line 915
    :cond_8
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mIsSyncing:Z

    .line 917
    const/4 v9, 0x2

    const-string v10, "BaiduNavigation"

    const-string v11, "Baidu syncing has End"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 819
    :cond_9
    iget-object v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->baiduAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    invoke-virtual {v9, v8}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->getDeltaList(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;

    move-result-object v7

    .line 821
    if-eqz v7, :cond_7

    .line 824
    invoke-virtual {v7}, Lcom/samsung/scloud/data/SCloudNodeList;->getCursor()Ljava/lang/String;

    move-result-object v8

    .line 825
    const/4 v9, 0x2

    const-string v10, "BaiduNavigation"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "SyncList getList - nodes :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v7}, Lcom/samsung/scloud/data/SCloudNodeList;->getNodes()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 827
    if-eqz p1, :cond_a

    .line 829
    invoke-virtual {v7}, Lcom/samsung/scloud/data/SCloudNodeList;->getNodes()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_a

    .line 830
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->beginTransaction()V

    .line 833
    :cond_a
    if-eqz v7, :cond_b

    .line 834
    invoke-virtual {v7}, Lcom/samsung/scloud/data/SCloudNodeList;->getNodes()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/scloud/data/SCloudNode;

    .line 836
    .local v6, "node":Lcom/samsung/scloud/data/SCloudNode;
    iget-boolean v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mbAbort:Z

    if-eqz v9, :cond_d

    .line 838
    const/4 v9, 0x2

    const-string v10, "BaiduNavigation"

    const-string v11, "SyncList Baidu mbAbort is ture break for loop"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 850
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "node":Lcom/samsung/scloud/data/SCloudNode;
    :cond_b
    const/4 v9, 0x2

    const-string v10, "BaiduNavigation"

    const-string v11, "SyncList db.insert done "

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 852
    if-eqz p1, :cond_c

    .line 854
    invoke-virtual {v7}, Lcom/samsung/scloud/data/SCloudNodeList;->getNodes()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v1

    .line 855
    invoke-virtual {v7}, Lcom/samsung/scloud/data/SCloudNodeList;->getNodes()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    add-int/2addr v2, v9

    .line 859
    if-lez v1, :cond_c

    .line 861
    const/4 v9, 0x2

    const-string v10, "BaiduNavigation"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "SyncList SyncCount Transcation : SyncCount = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 862
    const/4 v9, 0x2

    const-string v10, "BaiduNavigation"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "SyncList SyncCount Transcation : TotalCount = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 864
    const/4 v1, 0x0

    .line 866
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->setTransactionSuccessful()V

    .line 867
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->endTransaction()V

    .line 869
    const/4 v9, 0x2

    const-string v10, "BaiduNavigation"

    const-string v11, "SyncList SyncCount Transcation : endTransaction "

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 891
    :cond_c
    iget-boolean v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mbAbort:Z

    if-eqz v9, :cond_f

    .line 892
    const/4 v9, 0x2

    const-string v10, "BaiduNavigation"

    const-string v11, "SyncList Dropbox mbAbort is ture break ,do while loop"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 893
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mbAbort:Z

    goto/16 :goto_1

    .line 842
    .restart local v5    # "i$":Ljava/util/Iterator;
    .restart local v6    # "node":Lcom/samsung/scloud/data/SCloudNode;
    :cond_d
    invoke-virtual {v6}, Lcom/samsung/scloud/data/SCloudNode;->isDeleted()Z

    move-result v9

    if-eqz v9, :cond_e

    .line 843
    invoke-virtual {v3, v6}, Lcom/sec/android/app/myfiles/db/CacheDB;->deleteCloudData(Lcom/samsung/scloud/data/SCloudNode;)I

    goto/16 :goto_2

    .line 845
    :cond_e
    invoke-virtual {v3, v6, p1}, Lcom/sec/android/app/myfiles/db/CacheDB;->insertCloudData(Lcom/samsung/scloud/data/SCloudNode;Z)J

    goto/16 :goto_2

    .line 898
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "node":Lcom/samsung/scloud/data/SCloudNode;
    :cond_f
    move-object v0, v8

    .line 899
    invoke-virtual {v3, v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->setDropboxCursor(Ljava/lang/String;)V

    .line 903
    invoke-virtual {v7}, Lcom/samsung/scloud/data/SCloudNodeList;->getHasMore()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v9

    const/4 v10, 0x1

    if-eq v9, v10, :cond_6

    goto/16 :goto_1
.end method

.method public updateWait()V
    .locals 5

    .prologue
    .line 613
    new-instance v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$3;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;)V

    .line 621
    .local v1, "timerTask":Ljava/util/TimerTask;
    const/4 v2, 0x2

    const-string v3, "BaiduNavigation"

    const-string v4, "update..Wait.."

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 623
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 624
    .local v0, "timer":Ljava/util/Timer;
    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 625
    return-void
.end method
