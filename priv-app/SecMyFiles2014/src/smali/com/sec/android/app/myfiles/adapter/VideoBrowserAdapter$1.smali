.class Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter$1;
.super Ljava/lang/Object;
.source "VideoBrowserAdapter.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;

.field final synthetic val$filePosition:I

.field final synthetic val$fileType:I

.field final synthetic val$hoverFilePath:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;ILjava/lang/String;I)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;

    iput p2, p0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter$1;->val$fileType:I

    iput-object p3, p0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter$1;->val$hoverFilePath:Ljava/lang/String;

    iput p4, p0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter$1;->val$filePosition:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v6, 0xa

    const/4 v3, 0x1

    const/4 v9, 0x2

    const/4 v4, 0x0

    .line 121
    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;->isSelectMode()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-static {v5, p2}, Lcom/sec/android/app/myfiles/utils/Utils;->isHoverSettingDisabled(Landroid/content/Context;Landroid/view/MotionEvent;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    if-eqz v5, :cond_0

    .line 124
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    if-eq v5, v6, :cond_1

    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->isOnCall(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 199
    :cond_0
    :goto_0
    :pswitch_0
    return v4

    .line 128
    :cond_1
    const/4 v0, 0x0

    .line 130
    .local v0, "bSupportHover":Z
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 131
    .local v2, "hoverRect":Landroid/graphics/Rect;
    invoke-virtual {p1, v2}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 133
    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;

    iget v6, v2, Landroid/graphics/Rect;->left:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    float-to-int v7, v7

    iget-object v8, p0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;

    iget v8, v8, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;->mViewMode:I

    if-ne v8, v9, :cond_3

    :goto_1
    invoke-virtual {v5, v6, v7, v3}, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;->setHoverLeft(IIZ)I

    move-result v3

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 134
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;

    iget v5, v2, Landroid/graphics/Rect;->top:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {v3, v5, v6}, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;->setHoverTop(II)I

    move-result v3

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 136
    iget v3, p0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter$1;->val$fileType:I

    invoke-static {v3}, Lcom/sec/android/app/myfiles/MediaFile;->isVideoFileType(I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 138
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    const/4 v5, 0x3

    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter$1;->val$hoverFilePath:Ljava/lang/String;

    iget v7, p0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter$1;->val$filePosition:I

    invoke-virtual {v3, v5, v2, v6, v7}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->setParam(ILandroid/graphics/Rect;Ljava/lang/String;I)V

    .line 139
    const/4 v0, 0x1

    .line 146
    :goto_2
    if-eqz v0, :cond_0

    .line 148
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :pswitch_1
    goto :goto_0

    .line 152
    :pswitch_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;

    # setter for: Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;->previousListener:Landroid/view/View$OnHoverListener;
    invoke-static {v3, p0}, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;->access$002(Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;Landroid/view/View$OnHoverListener;)Landroid/view/View$OnHoverListener;

    .line 155
    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v3

    if-ne v3, v9, :cond_2

    .line 157
    const/16 v3, 0xa

    const/4 v5, -0x1

    invoke-static {v3, v5}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 165
    :cond_2
    :goto_3
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    const/16 v5, 0x1f4

    invoke-virtual {v3, v5}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->sendDelayedMessage(I)V

    goto :goto_0

    :cond_3
    move v3, v4

    .line 133
    goto :goto_1

    .line 143
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 160
    :catch_0
    move-exception v1

    .line 162
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_3

    .line 171
    .end local v1    # "e":Landroid/os/RemoteException;
    :pswitch_3
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;->previousListener:Landroid/view/View$OnHoverListener;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;)Landroid/view/View$OnHoverListener;

    move-result-object v3

    if-ne v3, p0, :cond_0

    .line 174
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v3

    if-ne v3, v9, :cond_5

    .line 176
    const/4 v3, 0x1

    const/4 v5, -0x1

    invoke-static {v3, v5}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 184
    :cond_5
    :goto_4
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->removeDelayedMessage()V

    goto/16 :goto_0

    .line 179
    :catch_1
    move-exception v1

    .line 181
    .restart local v1    # "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_4

    .line 148
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
