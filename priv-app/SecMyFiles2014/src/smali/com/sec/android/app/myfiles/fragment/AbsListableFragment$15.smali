.class Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$15;
.super Landroid/content/BroadcastReceiver;
.source "AbsListableFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;)V
    .locals 0

    .prologue
    .line 1052
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$15;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1057
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$15;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget v3, v3, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mRunFrom:I

    const/16 v4, 0x15

    if-eq v3, v4, :cond_0

    .line 1059
    sget-boolean v3, Lcom/sec/android/app/myfiles/utils/Utils;->USE_SPLIT:Z

    if-eqz v3, :cond_2

    .line 1060
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$15;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->getScreenState(Landroid/content/Context;)I

    move-result v0

    .line 1062
    .local v0, "screenState":I
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$15;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    if-ne v0, v1, :cond_1

    :goto_0
    invoke-virtual {v3, v1}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->showTreeView(Z)V

    .line 1067
    .end local v0    # "screenState":I
    :cond_0
    :goto_1
    return-void

    .restart local v0    # "screenState":I
    :cond_1
    move v1, v2

    .line 1062
    goto :goto_0

    .line 1064
    .end local v0    # "screenState":I
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$15;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->showTreeView(Z)V

    goto :goto_1
.end method
