.class public Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;
.super Landroid/app/DialogFragment;
.source "ConfirmOmaDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$ConfirmOmaViewHolder;
    }
.end annotation


# static fields
.field public static final ARG_OMA_ITEMS:Ljava/lang/String; = "arg_omas"

.field private static dialogInstance:Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;


# instance fields
.field private mConsumed:I

.field private mDownloadManager:Landroid/app/DownloadManager;

.field private mHistoryItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/HistoryItem;",
            ">;"
        }
    .end annotation
.end field

.field private mIsDialogShowing:Z

.field private mNegativeClickListener:Landroid/view/View$OnClickListener;

.field private mPositiveClickListener:Landroid/view/View$OnClickListener;

.field private mViewHolder:Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$ConfirmOmaViewHolder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mIsDialogShowing:Z

    .line 379
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$4;-><init>(Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mPositiveClickListener:Landroid/view/View$OnClickListener;

    .line 392
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$5;-><init>(Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mNegativeClickListener:Landroid/view/View$OnClickListener;

    .line 91
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mHistoryItems:Ljava/util/ArrayList;

    .line 92
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mIsDialogShowing:Z

    .line 93
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->rejectCurrentItem()V

    return-void
.end method

.method static synthetic access$108(Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mConsumed:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mConsumed:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->updateForNextItem(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->confirmCurrentItem()V

    return-void
.end method

.method private confirmCurrentItem()V
    .locals 3

    .prologue
    .line 259
    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mConsumed:I

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mHistoryItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 261
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mHistoryItems:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mConsumed:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/element/HistoryItem;

    .line 265
    .local v0, "item":Lcom/sec/android/app/myfiles/element/HistoryItem;
    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->requestDownload(Lcom/sec/android/app/myfiles/element/HistoryItem;)Z

    .line 269
    .end local v0    # "item":Lcom/sec/android/app/myfiles/element/HistoryItem;
    :cond_0
    return-void
.end method

.method private deleteFileIfExists(Ljava/lang/String;)V
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 355
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 357
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 358
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 365
    .end local v1    # "file":Ljava/io/File;
    :cond_0
    :goto_0
    return-void

    .line 361
    :catch_0
    move-exception v0

    .line 363
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static getDialog(Ljava/util/ArrayList;Landroid/app/Fragment;I)Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;
    .locals 4
    .param p1, "targetFragment"    # Landroid/app/Fragment;
    .param p2, "requestCode"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/HistoryItem;",
            ">;",
            "Landroid/app/Fragment;",
            "I)",
            "Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;"
        }
    .end annotation

    .prologue
    .line 72
    .local p0, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/HistoryItem;>;"
    sget-object v2, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->dialogInstance:Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->dialogInstance:Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->isDialogShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 73
    sget-object v2, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->dialogInstance:Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->addItemsToDisplayOmaDialog(Ljava/util/List;)V

    .line 74
    const/4 v2, 0x0

    .line 85
    :goto_0
    return-object v2

    .line 76
    :cond_0
    new-instance v1, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;

    invoke-direct {v1}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;-><init>()V

    .line 78
    .local v1, "dialog":Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 79
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "arg_omas"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 80
    invoke-virtual {v1, v0}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->setArguments(Landroid/os/Bundle;)V

    .line 81
    invoke-virtual {v1, p1, p2}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 82
    sput-object v1, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->dialogInstance:Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;

    .line 85
    sget-object v2, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->dialogInstance:Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;

    goto :goto_0
.end method

.method private getNextHistoryItem()Lcom/sec/android/app/myfiles/element/HistoryItem;
    .locals 2

    .prologue
    .line 228
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mConsumed:I

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mHistoryItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mHistoryItems:Ljava/util/ArrayList;

    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mConsumed:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/element/HistoryItem;

    .line 233
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private rejectCurrentItem()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 321
    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mConsumed:I

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mHistoryItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 323
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mHistoryItems:Ljava/util/ArrayList;

    iget v3, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mConsumed:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/element/HistoryItem;

    .line 325
    .local v0, "item":Lcom/sec/android/app/myfiles/element/HistoryItem;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 326
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "status"

    const/16 v3, 0x1ea

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 327
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "content://downloads/all_downloads"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getExternalId()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3, v1, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 329
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mDownloadManager:Landroid/app/DownloadManager;

    const/4 v3, 0x1

    new-array v3, v3, [J

    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getExternalId()J

    move-result-wide v6

    aput-wide v6, v3, v4

    invoke-virtual {v2, v3}, Landroid/app/DownloadManager;->remove([J)I

    .line 330
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getData()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->deleteFileIfExists(Ljava/lang/String;)V

    .line 333
    .end local v0    # "item":Lcom/sec/android/app/myfiles/element/HistoryItem;
    .end local v1    # "values":Landroid/content/ContentValues;
    :cond_0
    return-void
.end method

.method private requestDownload(Lcom/sec/android/app/myfiles/element/HistoryItem;)Z
    .locals 10
    .param p1, "item"    # Lcom/sec/android/app/myfiles/element/HistoryItem;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 288
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 289
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "status"

    const/16 v4, 0xb7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 290
    const-string v3, "current_bytes"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 291
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getSize()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-gtz v3, :cond_0

    .line 292
    const-string v3, "total_bytes"

    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 296
    :goto_0
    const-string v3, "_data"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 298
    const/4 v0, 0x0

    .line 300
    .local v0, "destFile":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getData()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x2f

    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 302
    .local v1, "index":I
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 304
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getData()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v8, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 310
    :goto_1
    const-string v3, "hint"

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "content://downloads/all_downloads"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getExternalId()J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4, v2, v9, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 313
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getData()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->deleteFileIfExists(Ljava/lang/String;)V

    .line 315
    return v8

    .line 294
    .end local v0    # "destFile":Ljava/lang/String;
    .end local v1    # "index":I
    :cond_0
    const-string v3, "total_bytes"

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getSize()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_0

    .line 308
    .restart local v0    # "destFile":Ljava/lang/String;
    .restart local v1    # "index":I
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getData()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v8, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getSource()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->chooseName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private updateForNextItem(Z)V
    .locals 5
    .param p1, "negative"    # Z

    .prologue
    .line 239
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->getNextHistoryItem()Lcom/sec/android/app/myfiles/element/HistoryItem;

    move-result-object v0

    .line 241
    .local v0, "nextItem":Lcom/sec/android/app/myfiles/element/HistoryItem;
    if-eqz v0, :cond_0

    .line 243
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mViewHolder:Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$ConfirmOmaViewHolder;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$ConfirmOmaViewHolder;->updateForItem(Lcom/sec/android/app/myfiles/element/HistoryItem;)V

    .line 255
    :goto_0
    return-void

    .line 248
    :cond_0
    if-eqz p1, :cond_1

    .line 249
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->getTargetRequestCode()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 251
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    goto :goto_0
.end method


# virtual methods
.method public addItemsToDisplayOmaDialog(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 96
    .local p1, "historyItems":Ljava/util/List;, "Ljava/util/List<Landroid/os/Parcelable;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    .line 97
    .local v0, "historyItem":Landroid/os/Parcelable;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mHistoryItems:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 98
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mHistoryItems:Ljava/util/ArrayList;

    check-cast v0, Lcom/sec/android/app/myfiles/element/HistoryItem;

    .end local v0    # "historyItem":Landroid/os/Parcelable;
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 101
    :cond_1
    return-void
.end method

.method public chooseName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 337
    const/16 v2, 0x2f

    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 338
    .local v0, "indexFrom":I
    add-int/lit8 v0, v0, 0x1

    .line 340
    if-gez v0, :cond_0

    .line 342
    const/4 v0, 0x0

    .line 345
    :cond_0
    const/16 v2, 0x2e

    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 346
    .local v1, "indexTo":I
    if-gez v1, :cond_1

    .line 348
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 350
    :cond_1
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 375
    invoke-super {p0}, Landroid/app/DialogFragment;->dismiss()V

    .line 376
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mIsDialogShowing:Z

    .line 377
    return-void
.end method

.method public isDialogShowing()Z
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mIsDialogShowing:Z

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 110
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 112
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "arg_omas"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 113
    .local v0, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    if-nez v0, :cond_0

    .line 122
    :goto_0
    return-void

    .line 116
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->addItemsToDisplayOmaDialog(Ljava/util/List;)V

    .line 118
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "download"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/DownloadManager;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mDownloadManager:Landroid/app/DownloadManager;

    .line 119
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mConsumed:I

    .line 120
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mIsDialogShowing:Z

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 143
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 144
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f040021

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 146
    .local v1, "view":Landroid/view/View;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$ConfirmOmaViewHolder;->fromView(Landroid/content/Context;Landroid/view/View;)Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$ConfirmOmaViewHolder;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mViewHolder:Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$ConfirmOmaViewHolder;

    .line 148
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0b016d

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0b016e

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$3;

    invoke-direct {v4, p0}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$3;-><init>(Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0b016f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$2;-><init>(Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$1;-><init>(Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;)V

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    return-object v2
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 128
    invoke-super {p0, p1, p2, p3}, Landroid/app/DialogFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 130
    .local v1, "view":Landroid/view/View;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 131
    .local v0, "dialog":Landroid/app/Dialog;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 134
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mViewHolder:Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$ConfirmOmaViewHolder;

    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->getNextHistoryItem()Lcom/sec/android/app/myfiles/element/HistoryItem;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$ConfirmOmaViewHolder;->updateForItem(Lcom/sec/android/app/myfiles/element/HistoryItem;)V

    .line 136
    return-object v1
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 369
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroyView()V

    .line 370
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mIsDialogShowing:Z

    .line 371
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 191
    invoke-super {p0}, Landroid/app/DialogFragment;->onStart()V

    .line 193
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    .line 195
    .local v0, "dialog":Landroid/app/AlertDialog;
    if-eqz v0, :cond_0

    .line 199
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mPositiveClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 202
    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->mNegativeClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 207
    :cond_0
    return-void
.end method

.method public showAllowingStateLoss(Landroid/app/FragmentManager;Ljava/lang/String;)V
    .locals 1
    .param p1, "fm"    # Landroid/app/FragmentManager;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    .line 216
    if-eqz p1, :cond_0

    .line 218
    invoke-virtual {p1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 219
    .local v0, "t":Landroid/app/FragmentTransaction;
    invoke-virtual {v0, p0, p2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 220
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 224
    .end local v0    # "t":Landroid/app/FragmentTransaction;
    :cond_0
    return-void
.end method
