.class public final enum Lcom/sec/android/app/myfiles/ftp/FTPErr;
.super Ljava/lang/Enum;
.source "FTPErr.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/myfiles/ftp/FTPErr;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/myfiles/ftp/FTPErr;

.field public static final enum CONNECT_FAILED:Lcom/sec/android/app/myfiles/ftp/FTPErr;

.field public static final enum CREATE_FOLDER_FAILED:Lcom/sec/android/app/myfiles/ftp/FTPErr;

.field public static final enum DELETE_FAILED:Lcom/sec/android/app/myfiles/ftp/FTPErr;

.field public static final enum LOGIN_FAILED:Lcom/sec/android/app/myfiles/ftp/FTPErr;

.field public static final enum NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

.field public static final enum PARTIALLY_DONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

.field public static final enum RENAME_ITEM_FAILED:Lcom/sec/android/app/myfiles/ftp/FTPErr;

.field public static final enum TARGET_SAME_AS_SOURCE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

.field public static final enum UNKNOWN:Lcom/sec/android/app/myfiles/ftp/FTPErr;

.field public static final enum WRONG_ARGUMENT:Lcom/sec/android/app/myfiles/ftp/FTPErr;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 21
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPErr;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/myfiles/ftp/FTPErr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 23
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPErr;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/myfiles/ftp/FTPErr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPErr;->UNKNOWN:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 25
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPErr;

    const-string v1, "WRONG_ARGUMENT"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/myfiles/ftp/FTPErr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPErr;->WRONG_ARGUMENT:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 27
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPErr;

    const-string v1, "CONNECT_FAILED"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/myfiles/ftp/FTPErr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPErr;->CONNECT_FAILED:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 29
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPErr;

    const-string v1, "LOGIN_FAILED"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/app/myfiles/ftp/FTPErr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPErr;->LOGIN_FAILED:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 31
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPErr;

    const-string v1, "PARTIALLY_DONE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/FTPErr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPErr;->PARTIALLY_DONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 33
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPErr;

    const-string v1, "CREATE_FOLDER_FAILED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/FTPErr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPErr;->CREATE_FOLDER_FAILED:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 35
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPErr;

    const-string v1, "RENAME_ITEM_FAILED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/FTPErr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPErr;->RENAME_ITEM_FAILED:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 37
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPErr;

    const-string v1, "DELETE_FAILED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/FTPErr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPErr;->DELETE_FAILED:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 39
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPErr;

    const-string v1, "TARGET_SAME_AS_SOURCE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/FTPErr;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPErr;->TARGET_SAME_AS_SOURCE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 19
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/sec/android/app/myfiles/ftp/FTPErr;

    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPErr;->UNKNOWN:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPErr;->WRONG_ARGUMENT:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPErr;->CONNECT_FAILED:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPErr;->LOGIN_FAILED:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/myfiles/ftp/FTPErr;->PARTIALLY_DONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/app/myfiles/ftp/FTPErr;->CREATE_FOLDER_FAILED:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/app/myfiles/ftp/FTPErr;->RENAME_ITEM_FAILED:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/app/myfiles/ftp/FTPErr;->DELETE_FAILED:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/app/myfiles/ftp/FTPErr;->TARGET_SAME_AS_SOURCE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPErr;->$VALUES:[Lcom/sec/android/app/myfiles/ftp/FTPErr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPErr;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 19
    const-class v0, Lcom/sec/android/app/myfiles/ftp/FTPErr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/ftp/FTPErr;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/myfiles/ftp/FTPErr;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/app/myfiles/ftp/FTPErr;->$VALUES:[Lcom/sec/android/app/myfiles/ftp/FTPErr;

    invoke-virtual {v0}, [Lcom/sec/android/app/myfiles/ftp/FTPErr;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/myfiles/ftp/FTPErr;

    return-object v0
.end method
