.class public Lcom/sec/android/app/myfiles/MainActivity;
.super Lcom/sec/android/app/myfiles/AbsMainActivity;
.source "MainActivity.java"

# interfaces
.implements Landroid/widget/SearchView$OnQueryTextListener;
.implements Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$onCopyMoveSelectedListener;


# static fields
.field public static FRAGMENT_LAYOUT_ID:I = 0x0

.field private static final MODULE:Ljava/lang/String; = "MainActivity"

.field private static final OPTION_SELECT_ALL:I = 0x0

.field private static final OPTION_UNSELECT_ALL:I = 0x1

.field private static final SEARCH_ADVANCE_FRAGMENT_TAG:Ljava/lang/String; = "SearchAdvanceFragment"

.field public static final THUMBNAIL_BROADCAST_ACTION:Ljava/lang/String; = "action.index.THUMBNAIL"

.field private static mSelectionLock:Ljava/lang/Object;


# instance fields
.field private final MAX_SEARCH_TEXT_LIMIT:I

.field public blockSendtoKNOX:Z

.field private deviceId:Ljava/lang/String;

.field private deviceName:Ljava/lang/String;

.field private fragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

.field private isSearchPeforming:Z

.field public mActionMode:Landroid/view/ActionMode;

.field public mActionModeCustom:Landroid/widget/LinearLayout;

.field public mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

.field public mActionbarSelectAllContainer:Landroid/view/View;

.field public mActionmodeAddShortcut:Landroid/view/MenuItem;

.field public mActionmodeAddShortcutHome:Landroid/view/MenuItem;

.field public mActionmodeCopy:Landroid/view/MenuItem;

.field public mActionmodeDelete:Landroid/view/MenuItem;

.field public mActionmodeDetail:Landroid/view/MenuItem;

.field public mActionmodeDownload:Landroid/view/MenuItem;

.field public mActionmodeExtract:Landroid/view/MenuItem;

.field public mActionmodeExtractHere:Landroid/view/MenuItem;

.field public mActionmodeFtpEdit:Landroid/view/MenuItem;

.field public mActionmodeLock:Landroid/view/MenuItem;

.field public mActionmodeMove:Landroid/view/MenuItem;

.field public mActionmodeMoveToKNOX:Landroid/view/MenuItem;

.field public mActionmodeRemoveFromKNOX:Landroid/view/MenuItem;

.field public mActionmodeRename:Landroid/view/MenuItem;

.field public mActionmodeShare:Landroid/view/MenuItem;

.field public mActionmodeUnlock:Landroid/view/MenuItem;

.field public mActionmodeZip:Landroid/view/MenuItem;

.field public mActionmoveMoveToPrivate:Landroid/view/MenuItem;

.field public mActionmoveRemoveFromPrivate:Landroid/view/MenuItem;

.field public mAddShortcutFromActionmode:Z

.field public mAdvancedSearchResult:Landroid/widget/TextView;

.field private mCalendar:Ljava/util/Calendar;

.field private mConnectionChangeReceiver:Landroid/content/BroadcastReceiver;

.field private mConnectionChangeReceiverFilter:Landroid/content/IntentFilter;

.field private mContext:Landroid/content/Context;

.field protected mCurrentFileTypes:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/sec/android/app/myfiles/utils/FileType;",
            ">;"
        }
    .end annotation
.end field

.field protected mCurrentFragmentSearch:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

.field protected mCurrentFromDate:J

.field protected mCurrentSearchType:I

.field protected mCurrentToDate:J

.field public mDisableMoveToPrivate:Z

.field private mEmptyTextView:Landroid/widget/TextView;

.field public mEnableMoveToPrivate:Z

.field protected mExtentionString:Ljava/lang/String;

.field private mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

.field private mExternalStorageReceiverFilter:Landroid/content/IntentFilter;

.field private mFinishNearByDevice:Z

.field private mFromHomeShortcut:Z

.field public mIsSearchMode:Z

.field public mIsSearchRetainForPen:Z

.field private mIsWindowFocused:Z

.field protected mParameter:Lcom/sec/android/app/myfiles/utils/SearchParameter;

.field public mQueryTextListener:Landroid/widget/SearchView$OnQueryTextListener;

.field private mRefreshHandler:Landroid/os/Handler;

.field protected mSearchFragmentManager:Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;

.field public mSearchFromAdvance:Z

.field public mSearchView:Landroid/widget/SearchView;

.field protected mSearchable:Lcom/sec/android/app/myfiles/fragment/ISearchable;

.field private mSecretReceiver:Landroid/content/BroadcastReceiver;

.field private mSecretReceiverFilter:Landroid/content/IntentFilter;

.field public mSelectAllButton:Landroid/widget/TextView;

.field mSelectAllClickListener:Landroid/view/View$OnClickListener;

.field public mSelectAllContainer:Landroid/widget/RelativeLayout;

.field private mSelectAllListPopupWindow:Landroid/widget/ListPopupWindow;

.field private mSelectOptions:[Ljava/lang/String;

.field private mSelectOptionsIds:[I

.field private mSelectionType:I

.field private mSelectionTypeChange:Z

.field protected mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

.field private mShowFolderFileType:I

.field private mSideSyncReceiver:Landroid/content/BroadcastReceiver;

.field private mSideSyncReceiverFilter:Landroid/content/IntentFilter;

.field private mStartFolder:Ljava/lang/String;

.field protected mStorageType:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mUnselectAllClickListener:Landroid/view/View$OnClickListener;

.field private preText:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 162
    const v0, 0x7f0f00c3

    sput v0, Lcom/sec/android/app/myfiles/MainActivity;->FRAGMENT_LAYOUT_ID:I

    .line 257
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectionLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 125
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/AbsMainActivity;-><init>()V

    .line 150
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mFinishNearByDevice:Z

    .line 152
    iput-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity;->mActionMode:Landroid/view/ActionMode;

    .line 186
    iput-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity;->preText:Ljava/lang/String;

    .line 188
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->MAX_SEARCH_TEXT_LIMIT:I

    .line 194
    iput-object p0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    .line 200
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectionTypeChange:Z

    .line 219
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->blockSendtoKNOX:Z

    .line 221
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mEnableMoveToPrivate:Z

    .line 223
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mDisableMoveToPrivate:Z

    .line 225
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mIsSearchMode:Z

    .line 227
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mIsSearchRetainForPen:Z

    .line 229
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mAddShortcutFromActionmode:Z

    .line 231
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchFromAdvance:Z

    .line 233
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mFromHomeShortcut:Z

    .line 259
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mIsWindowFocused:Z

    .line 1759
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/myfiles/MainActivity$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/MainActivity$5;-><init>(Lcom/sec/android/app/myfiles/MainActivity;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mRefreshHandler:Landroid/os/Handler;

    .line 2823
    new-instance v0, Lcom/sec/android/app/myfiles/MainActivity$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/MainActivity$8;-><init>(Lcom/sec/android/app/myfiles/MainActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mQueryTextListener:Landroid/widget/SearchView$OnQueryTextListener;

    return-void
.end method

.method private FragmentChangeCommit(ILcom/sec/android/app/myfiles/fragment/AbsFragment;)V
    .locals 13
    .param p1, "new_FragmentID"    # I
    .param p2, "new_fragment"    # Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .prologue
    const/16 v12, 0x12

    .line 1832
    iget-boolean v10, p0, Lcom/sec/android/app/myfiles/MainActivity;->mIsSearchMode:Z

    if-eqz v10, :cond_1

    if-eq p1, v12, :cond_1

    .line 1833
    if-nez p1, :cond_0

    iget-boolean v10, p0, Lcom/sec/android/app/myfiles/MainActivity;->mIsSearchRetainForPen:Z

    if-nez v10, :cond_1

    .line 1834
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->searchModeFinish()V

    .line 1837
    :cond_1
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/sec/android/app/myfiles/MainActivity;->mIsSearchRetainForPen:Z

    .line 1840
    const/4 v10, -0x1

    if-ne p1, v10, :cond_2

    .line 1966
    :goto_0
    return-void

    .line 1847
    :cond_2
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->getFragmentTagByID(I)Ljava/lang/String;

    move-result-object v5

    .line 1849
    .local v5, "Tag":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    .line 1851
    .local v7, "fm":Landroid/app/FragmentManager;
    invoke-virtual {v7}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v8

    .line 1854
    .local v8, "ft":Landroid/app/FragmentTransaction;
    const/4 v1, 0x0

    .line 1855
    .local v1, "CurrentTag":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    if-eqz v10, :cond_3

    .line 1856
    iget-object v10, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget v10, v10, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mId:I

    invoke-static {v10}, Lcom/sec/android/app/myfiles/utils/Utils;->getFragmentTagByID(I)Ljava/lang/String;

    move-result-object v1

    .line 1858
    :cond_3
    iget-object v10, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    if-eqz v10, :cond_a

    iget-object v10, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v10, :cond_a

    .line 1861
    iget-object v10, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->isVisible()Z

    move-result v10

    if-eqz v10, :cond_4

    if-eqz v1, :cond_4

    .line 1862
    iget-object v11, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    invoke-virtual {v7, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    iput-object v10, v11, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .line 1865
    :cond_4
    iget-object v10, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v10, :cond_7

    .line 1866
    iget-object v10, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v8, v10}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 1867
    iget-object v10, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v8, v10}, Landroid/app/FragmentTransaction;->detach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 1868
    if-eq p1, v12, :cond_5

    const/16 v10, 0x14

    if-eq p1, v10, :cond_5

    const/16 v10, 0x13

    if-eq p1, v10, :cond_5

    const/16 v10, 0x20

    if-ne p1, v10, :cond_6

    :cond_5
    iget-object v10, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget v10, v10, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mId:I

    if-eq v10, v12, :cond_6

    .line 1874
    iget-object v10, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    iget-object v10, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget v10, v10, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mId:I

    invoke-static {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setmBackFragment(I)V

    .line 1876
    :cond_6
    iget-object v10, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    const/4 v11, 0x0

    iput-object v11, v10, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .line 1936
    :cond_7
    :goto_1
    invoke-virtual {v7, v5}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .line 1938
    .local v9, "oldfg":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    if-eqz v9, :cond_8

    .line 1940
    invoke-virtual {v8, v9}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 1942
    invoke-virtual {v8, v9}, Landroid/app/FragmentTransaction;->detach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 1946
    :cond_8
    const v10, 0x7f0f00c3

    invoke-virtual {v8, v10, p2, v5}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 1948
    invoke-virtual {v8, p2}, Landroid/app/FragmentTransaction;->attach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 1949
    iget-object v10, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    if-eqz v10, :cond_9

    .line 1950
    iget-object v10, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iput p1, v10, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mId:I

    .line 1952
    iget-object v10, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iput-object p2, v10, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .line 1957
    :cond_9
    :try_start_0
    invoke-virtual {v8}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1959
    :catch_0
    move-exception v6

    .line 1961
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 1883
    .end local v6    # "e":Ljava/lang/Exception;
    .end local v9    # "oldfg":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    :cond_a
    if-eqz v1, :cond_c

    const-string v10, "SearchAdvanceFragment"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_c

    .line 1885
    invoke-virtual {v7, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .line 1887
    .local v2, "CurrntFragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    if-eqz v2, :cond_b

    .line 1890
    invoke-virtual {v8, v2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 1891
    invoke-virtual {v8, v2}, Landroid/app/FragmentTransaction;->detach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 1904
    .end local v2    # "CurrntFragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    :cond_b
    :goto_2
    iget-object v10, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    if-eqz v10, :cond_7

    if-eqz v1, :cond_7

    .line 1905
    iget v10, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSavedInstanceFragmentId:I

    iget-object v11, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget v11, v11, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mId:I

    if-eq v10, v11, :cond_7

    const-string v10, "SearchAdvanceFragment"

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_7

    .line 1907
    iget v10, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSavedInstanceFragmentId:I

    invoke-static {v10}, Lcom/sec/android/app/myfiles/utils/Utils;->getFragmentTagByID(I)Ljava/lang/String;

    move-result-object v4

    .line 1909
    .local v4, "SavedFrgTag":Ljava/lang/String;
    if-eqz v4, :cond_7

    .line 1910
    const-string v10, "SearchAdvanceFragment"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_d

    .line 1911
    invoke-virtual {v7, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .line 1913
    .local v3, "SavedFragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    if-eqz v3, :cond_7

    .line 1916
    invoke-virtual {v8, v3}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 1917
    invoke-virtual {v8, v3}, Landroid/app/FragmentTransaction;->detach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_1

    .line 1896
    .end local v3    # "SavedFragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    .end local v4    # "SavedFrgTag":Ljava/lang/String;
    :cond_c
    invoke-virtual {v7, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;

    .line 1897
    .local v0, "CurrentFragment":Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;
    if-eqz v0, :cond_b

    .line 1899
    invoke-virtual {v8, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 1900
    invoke-virtual {v8, v0}, Landroid/app/FragmentTransaction;->detach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_2

    .line 1921
    .end local v0    # "CurrentFragment":Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;
    .restart local v4    # "SavedFrgTag":Ljava/lang/String;
    :cond_d
    invoke-virtual {v7, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;

    .line 1923
    .local v3, "SavedFragment":Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;
    if-eqz v3, :cond_7

    .line 1925
    invoke-virtual {v8, v3}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 1926
    invoke-virtual {v8, v3}, Landroid/app/FragmentTransaction;->detach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto/16 :goto_1
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/MainActivity;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mRefreshHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/MainActivity;

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/MainActivity;->createSelectOptionClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/myfiles/MainActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/MainActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 125
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mFinishNearByDevice:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/myfiles/MainActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/MainActivity;

    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectionTypeChange:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/myfiles/MainActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/MainActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 125
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectionTypeChange:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/myfiles/MainActivity;)Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/MainActivity;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->fragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/myfiles/MainActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/MainActivity;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->preText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/myfiles/MainActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/MainActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/sec/android/app/myfiles/MainActivity;->preText:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/MainActivity;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mEmptyTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/MainActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/MainActivity;

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/MainActivity;->refreshFragmentsLocation()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/MainActivity;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/myfiles/MainActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/MainActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 125
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/MainActivity;->isSearchPeforming:Z

    return p1
.end method

.method static synthetic access$500()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 125
    sget-object v0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectionLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/myfiles/MainActivity;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/MainActivity;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectOptionsIds:[I

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/myfiles/MainActivity;)Ljava/util/Calendar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/MainActivity;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCalendar:Ljava/util/Calendar;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/myfiles/MainActivity;Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/MainActivity;
    .param p1, "x1"    # Ljava/util/Calendar;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCalendar:Ljava/util/Calendar;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/myfiles/MainActivity;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/MainActivity;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 125
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/MainActivity;->initSearchMenu(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/widget/ListPopupWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/MainActivity;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllListPopupWindow:Landroid/widget/ListPopupWindow;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/myfiles/MainActivity;Landroid/widget/ListPopupWindow;)Landroid/widget/ListPopupWindow;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/MainActivity;
    .param p1, "x1"    # Landroid/widget/ListPopupWindow;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllListPopupWindow:Landroid/widget/ListPopupWindow;

    return-object p1
.end method

.method private createSelectOptionClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 2142
    new-instance v0, Lcom/sec/android/app/myfiles/MainActivity$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/MainActivity$6;-><init>(Lcom/sec/android/app/myfiles/MainActivity;)V

    return-object v0
.end method

.method private deleteDownloadDescriptionFile(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 20
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 3271
    const/4 v4, 0x0

    .line 3272
    .local v4, "cursor":Landroid/database/Cursor;
    if-eqz p2, :cond_0

    .line 3273
    const/16 v16, 0x1

    const-string v17, "MainActivity"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "deleteDownloadDescriptionFile : intent = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v16 .. v18}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 3277
    :cond_0
    if-eqz p2, :cond_2

    .line 3279
    :try_start_0
    const-string v16, "extra_download_id"

    const-wide/16 v18, -0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    move-wide/from16 v2, v18

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v12

    .line 3281
    .local v12, "id":J
    const/16 v16, 0x1

    const-string v17, "MainActivity"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "deleteDownloadDescriptionFile : id = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v16 .. v18}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 3283
    const-wide/16 v16, -0x1

    cmp-long v16, v12, v16

    if-eqz v16, :cond_2

    .line 3285
    const-string v16, "download"

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/DownloadManager;

    .line 3287
    .local v7, "downloadManager":Landroid/app/DownloadManager;
    new-instance v11, Landroid/app/DownloadManager$Query;

    invoke-direct {v11}, Landroid/app/DownloadManager$Query;-><init>()V

    .line 3289
    .local v11, "query":Landroid/app/DownloadManager$Query;
    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [J

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-wide v12, v16, v17

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Landroid/app/DownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    .line 3291
    invoke-virtual {v7, v11}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v4

    .line 3293
    if-eqz v4, :cond_2

    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v16

    if-eqz v16, :cond_2

    .line 3295
    const-string v16, "status"

    move-object/from16 v0, v16

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 3297
    .local v15, "statusColumnIndex":I
    if-ltz v15, :cond_6

    .line 3299
    invoke-interface {v4, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 3301
    .local v14, "status":I
    const/16 v16, 0x1

    const-string v17, "MainActivity"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "deleteDownloadDescriptionFile : status = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v16 .. v18}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 3303
    const/16 v16, 0x8

    move/from16 v0, v16

    if-ne v14, v0, :cond_1

    .line 3305
    const-string v16, "local_filename"

    move-object/from16 v0, v16

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 3307
    .local v10, "filenameColumnIndex":I
    if-ltz v10, :cond_5

    .line 3309
    invoke-interface {v4, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 3311
    .local v9, "filePath":Ljava/lang/String;
    const-string v6, ".dd"

    .line 3313
    .local v6, "downloadDescriptionExtension":Ljava/lang/String;
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v17, 0x0

    const-string v18, "."

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v9, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ".dd"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 3315
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3317
    .local v5, "downloadDescrioptionFile":Ljava/io/File;
    if-eqz v5, :cond_3

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v16

    if-eqz v16, :cond_3

    .line 3319
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 3335
    .end local v5    # "downloadDescrioptionFile":Ljava/io/File;
    .end local v6    # "downloadDescriptionExtension":Ljava/lang/String;
    .end local v9    # "filePath":Ljava/lang/String;
    .end local v10    # "filenameColumnIndex":I
    .end local v14    # "status":I
    :cond_1
    :goto_0
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 3348
    .end local v7    # "downloadManager":Landroid/app/DownloadManager;
    .end local v11    # "query":Landroid/app/DownloadManager$Query;
    .end local v12    # "id":J
    .end local v15    # "statusColumnIndex":I
    :cond_2
    :goto_1
    return-void

    .line 3322
    .restart local v5    # "downloadDescrioptionFile":Ljava/io/File;
    .restart local v6    # "downloadDescriptionExtension":Ljava/lang/String;
    .restart local v7    # "downloadManager":Landroid/app/DownloadManager;
    .restart local v9    # "filePath":Ljava/lang/String;
    .restart local v10    # "filenameColumnIndex":I
    .restart local v11    # "query":Landroid/app/DownloadManager$Query;
    .restart local v12    # "id":J
    .restart local v14    # "status":I
    .restart local v15    # "statusColumnIndex":I
    :cond_3
    const/16 v16, 0x1

    const-string v17, "MainActivity"

    const-string v18, "deleteDownloadDescriptionFile : downloadDescrioptionFile == null or not exists"

    invoke-static/range {v16 .. v18}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 3339
    .end local v5    # "downloadDescrioptionFile":Ljava/io/File;
    .end local v6    # "downloadDescriptionExtension":Ljava/lang/String;
    .end local v7    # "downloadManager":Landroid/app/DownloadManager;
    .end local v9    # "filePath":Ljava/lang/String;
    .end local v10    # "filenameColumnIndex":I
    .end local v11    # "query":Landroid/app/DownloadManager$Query;
    .end local v12    # "id":J
    .end local v14    # "status":I
    .end local v15    # "statusColumnIndex":I
    :catch_0
    move-exception v8

    .line 3341
    .local v8, "e":Ljava/lang/Exception;
    if-eqz v4, :cond_4

    invoke-interface {v4}, Landroid/database/Cursor;->isClosed()Z

    move-result v16

    if-nez v16, :cond_4

    .line 3343
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 3346
    :cond_4
    const/16 v16, 0x0

    const-string v17, "MainActivity"

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v16 .. v18}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 3326
    .end local v8    # "e":Ljava/lang/Exception;
    .restart local v7    # "downloadManager":Landroid/app/DownloadManager;
    .restart local v10    # "filenameColumnIndex":I
    .restart local v11    # "query":Landroid/app/DownloadManager$Query;
    .restart local v12    # "id":J
    .restart local v14    # "status":I
    .restart local v15    # "statusColumnIndex":I
    :cond_5
    const/16 v16, 0x1

    :try_start_1
    const-string v17, "MainActivity"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "deleteDownloadDescriptionFile : filenameColumnIndex = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v16 .. v18}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3331
    .end local v10    # "filenameColumnIndex":I
    .end local v14    # "status":I
    :cond_6
    const/16 v16, 0x1

    const-string v17, "MainActivity"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "deleteDownloadDescriptionFile : statusColumnIndex = "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v16 .. v18}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private getFileType(Landroid/content/Intent;)Ljava/util/EnumSet;
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/sec/android/app/myfiles/utils/FileType;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2879
    const v2, 0x7f0b0001

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 2880
    .local v0, "all":Z
    if-eqz v0, :cond_0

    .line 2881
    sget-object v2, Lcom/sec/android/app/myfiles/utils/FileType;->IMAGE:Lcom/sec/android/app/myfiles/utils/FileType;

    sget-object v3, Lcom/sec/android/app/myfiles/utils/FileType;->DOWNLOADEDAPPS:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-static {v2, v3}, Ljava/util/EnumSet;->range(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v2

    .line 2900
    :goto_0
    return-object v2

    .line 2883
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2884
    .local v1, "fileTypes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/utils/FileType;>;"
    const v2, 0x7f0b0003

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2885
    sget-object v2, Lcom/sec/android/app/myfiles/utils/FileType;->IMAGE:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2887
    :cond_1
    const v2, 0x7f0b0147

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2888
    sget-object v2, Lcom/sec/android/app/myfiles/utils/FileType;->AUDIO:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2890
    :cond_2
    const v2, 0x7f0b0004

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2891
    sget-object v2, Lcom/sec/android/app/myfiles/utils/FileType;->VIDEO:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2893
    :cond_3
    const v2, 0x7f0b0006

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2894
    sget-object v2, Lcom/sec/android/app/myfiles/utils/FileType;->DOCUMENT:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2896
    :cond_4
    const v2, 0x7f0b0007

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2897
    sget-object v2, Lcom/sec/android/app/myfiles/utils/FileType;->DOWNLOADEDAPPS:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2899
    :cond_5
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x0

    goto :goto_0

    .line 2900
    :cond_6
    invoke-static {v1}, Ljava/util/EnumSet;->copyOf(Ljava/util/Collection;)Ljava/util/EnumSet;

    move-result-object v2

    goto :goto_0
.end method

.method private handleMenuItemAll()V
    .locals 2

    .prologue
    .line 2719
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentSearchType:I

    .line 2721
    const/16 v0, 0x12

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/myfiles/MainActivity;->setFragment(ILandroid/os/Bundle;)V

    .line 2723
    return-void
.end method

.method private initSearchMenu(Landroid/view/View;)V
    .locals 1
    .param p1, "customView"    # Landroid/view/View;

    .prologue
    .line 2592
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectionTypeChange:Z

    .line 2593
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/MainActivity;->handleMenuItemAll()V

    .line 2664
    return-void
.end method

.method private lruCacheClear()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2992
    const/4 v1, 0x0

    .line 2993
    .local v1, "curFragmentAdapter":Landroid/widget/BaseAdapter;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    if-eqz v3, :cond_0

    .line 2994
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget v4, v4, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mId:I

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getAdapter(I)Landroid/widget/BaseAdapter;

    move-result-object v1

    .line 2996
    :cond_0
    if-eqz v1, :cond_1

    .line 2998
    instance-of v3, v1, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    if-eqz v3, :cond_2

    move-object v3, v1

    .line 3000
    check-cast v3, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->getThumbnailCache()Landroid/util/LruCache;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 3002
    :try_start_0
    check-cast v1, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;

    .end local v1    # "curFragmentAdapter":Landroid/widget/BaseAdapter;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/DropboxAdapter;->getThumbnailCache()Landroid/util/LruCache;

    move-result-object v3

    invoke-virtual {v3}, Landroid/util/LruCache;->evictAll()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3032
    :cond_1
    :goto_0
    return-void

    .line 3003
    :catch_0
    move-exception v2

    .line 3004
    .local v2, "e":Ljava/lang/NullPointerException;
    const-string v3, "MainActivity"

    const-string v4, "onTrimMemory Dropbox ThumbnailCache cache is empty"

    invoke-static {v5, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3009
    .end local v2    # "e":Ljava/lang/NullPointerException;
    .restart local v1    # "curFragmentAdapter":Landroid/widget/BaseAdapter;
    :cond_2
    instance-of v3, v1, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    if-eqz v3, :cond_1

    move-object v3, v1

    .line 3011
    check-cast v3, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->getThumbnailCache()Landroid/util/LruCache;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 3013
    :try_start_1
    move-object v0, v1

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    move-object v3, v0

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->getThumbnailCache()Landroid/util/LruCache;

    move-result-object v3

    invoke-virtual {v3}, Landroid/util/LruCache;->evictAll()V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2

    :cond_3
    :goto_1
    move-object v3, v1

    .line 3020
    check-cast v3, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->getPath()Landroid/util/LruCache;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 3022
    :try_start_2
    check-cast v1, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    .end local v1    # "curFragmentAdapter":Landroid/widget/BaseAdapter;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->getPath()Landroid/util/LruCache;

    move-result-object v3

    invoke-virtual {v3}, Landroid/util/LruCache;->evictAll()V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 3023
    :catch_1
    move-exception v2

    .line 3024
    .restart local v2    # "e":Ljava/lang/NullPointerException;
    const-string v3, "MainActivity"

    const-string v4, "onTrimMemory NearByDevice Path cache is empty"

    invoke-static {v5, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3014
    .end local v2    # "e":Ljava/lang/NullPointerException;
    .restart local v1    # "curFragmentAdapter":Landroid/widget/BaseAdapter;
    :catch_2
    move-exception v2

    .line 3015
    .restart local v2    # "e":Ljava/lang/NullPointerException;
    const-string v3, "MainActivity"

    const-string v4, "onTrimMemory NearByDevice ThumbnailCache cache is empty"

    invoke-static {v5, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private refreshFragmentsLocation()V
    .locals 4

    .prologue
    .line 2666
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    .line 2667
    .local v1, "searchSetLocationFragment":Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;
    if-eqz v1, :cond_0

    .line 2668
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->refreshAdapter()V

    .line 2670
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "SearchAdvanceFragment"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;

    .line 2671
    .local v0, "advancedFragment":Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isResumed()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2672
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->refreshLocation()V

    .line 2673
    :cond_1
    return-void
.end method

.method private removeRecentlyPrivateContent()V
    .locals 8

    .prologue
    .line 1662
    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    .line 1664
    .local v2, "uri":Landroid/net/Uri;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1668
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    :try_start_0
    const-string v3, "_data LIKE ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "%"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1675
    :goto_0
    return-void

    .line 1670
    :catch_0
    move-exception v1

    .line 1672
    .local v1, "e":Landroid/database/sqlite/SQLiteException;
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_0
.end method

.method private setConnectionChangeReceiver()V
    .locals 2

    .prologue
    .line 1600
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mConnectionChangeReceiverFilter:Landroid/content/IntentFilter;

    .line 1602
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mConnectionChangeReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1604
    new-instance v0, Lcom/sec/android/app/myfiles/MainActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/MainActivity$3;-><init>(Lcom/sec/android/app/myfiles/MainActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mConnectionChangeReceiver:Landroid/content/BroadcastReceiver;

    .line 1614
    return-void
.end method

.method private setExternalStorageReceiver()V
    .locals 2

    .prologue
    .line 1448
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mExternalStorageReceiverFilter:Landroid/content/IntentFilter;

    .line 1451
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mExternalStorageReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1452
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mExternalStorageReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_SCAN"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1454
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mExternalStorageReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1457
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mExternalStorageReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1458
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mExternalStorageReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1461
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mExternalStorageReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1462
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mExternalStorageReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1464
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mExternalStorageReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 1466
    new-instance v0, Lcom/sec/android/app/myfiles/MainActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/MainActivity$1;-><init>(Lcom/sec/android/app/myfiles/MainActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

    .line 1567
    return-void
.end method

.method private setFragment(ILandroid/os/Bundle;)V
    .locals 6
    .param p1, "id"    # I
    .param p2, "arg"    # Landroid/os/Bundle;

    .prologue
    .line 2806
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchFragmentManager:Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;

    iget v2, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectionType:I

    iget v3, p0, Lcom/sec/android/app/myfiles/MainActivity;->mShowFolderFileType:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mStartFolder:Ljava/lang/String;

    move v1, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;->setFragment(IIILjava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->fragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .line 2809
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->fragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragmentSearch:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .line 2812
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->fragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v0, :cond_1

    .line 2813
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->fragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/ISearchable;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchable:Lcom/sec/android/app/myfiles/fragment/ISearchable;

    .line 2815
    const/16 v0, 0x13

    if-ne p1, v0, :cond_0

    .line 2816
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->fragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    const-string v1, "provider_id"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->changeProvider(Ljava/lang/String;)V

    .line 2822
    :cond_0
    :goto_0
    return-void

    .line 2819
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchable:Lcom/sec/android/app/myfiles/fragment/ISearchable;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2821
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private setSecretReceiver()V
    .locals 2

    .prologue
    .line 1618
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSecretReceiverFilter:Landroid/content/IntentFilter;

    .line 1620
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSecretReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1621
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSecretReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1622
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSecretReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1624
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSecretReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 1626
    new-instance v0, Lcom/sec/android/app/myfiles/MainActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/MainActivity$4;-><init>(Lcom/sec/android/app/myfiles/MainActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSecretReceiver:Landroid/content/BroadcastReceiver;

    .line 1657
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSecretReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSecretReceiverFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/myfiles/MainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1658
    return-void
.end method

.method private setSideSyncReceiver()V
    .locals 2

    .prologue
    .line 1572
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSideSyncReceiverFilter:Landroid/content/IntentFilter;

    .line 1574
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSideSyncReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.sidesync.source.DROP_DRAG"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1576
    new-instance v0, Lcom/sec/android/app/myfiles/MainActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/MainActivity$2;-><init>(Lcom/sec/android/app/myfiles/MainActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSideSyncReceiver:Landroid/content/BroadcastReceiver;

    .line 1596
    return-void
.end method


# virtual methods
.method public SearchFragmentCommit(Lcom/sec/android/app/myfiles/fragment/AbsFragment;)V
    .locals 1
    .param p1, "f"    # Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .prologue
    .line 1819
    const/16 v0, 0x12

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/myfiles/MainActivity;->FragmentChangeCommit(ILcom/sec/android/app/myfiles/fragment/AbsFragment;)V

    .line 1820
    return-void
.end method

.method public closeActionmode()V
    .locals 1

    .prologue
    .line 2870
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllListPopupWindow:Landroid/widget/ListPopupWindow;

    if-eqz v0, :cond_0

    .line 2871
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllListPopupWindow:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->dismiss()V

    .line 2873
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_1

    .line 2874
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 2875
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mActionMode:Landroid/view/ActionMode;

    .line 2877
    :cond_1
    return-void
.end method

.method public finishSearchMode()V
    .locals 0

    .prologue
    .line 2101
    return-void
.end method

.method public fragmentPerformExtract(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 1430
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v0, :cond_0

    .line 1432
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    if-eqz v0, :cond_0

    .line 1433
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->performExtract(Ljava/lang/String;)V

    .line 1436
    :cond_0
    return-void
.end method

.method public fragmentRefresh()V
    .locals 1

    .prologue
    .line 1423
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v0, :cond_0

    .line 1425
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onRefresh()V

    .line 1427
    :cond_0
    return-void
.end method

.method public fromSearchExtract(ILcom/sec/android/app/myfiles/fragment/AbsFragment;)V
    .locals 0
    .param p1, "new_FragmentID"    # I
    .param p2, "fragment"    # Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .prologue
    .line 3050
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/myfiles/MainActivity;->FragmentChangeCommit(ILcom/sec/android/app/myfiles/fragment/AbsFragment;)V

    .line 3051
    return-void
.end method

.method public getCurrentFragmentID()I
    .locals 1

    .prologue
    .line 2106
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mId:I

    return v0
.end method

.method protected getMainLayoutResId()I
    .locals 1

    .prologue
    .line 728
    const v0, 0x7f040030

    return v0
.end method

.method public getSelectAllPopupWindow()Landroid/widget/ListPopupWindow;
    .locals 1

    .prologue
    .line 3221
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllListPopupWindow:Landroid/widget/ListPopupWindow;

    return-object v0
.end method

.method public handleMenuItemAdvanced(I)V
    .locals 6
    .param p1, "searchFor"    # I

    .prologue
    .line 2776
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    .line 2777
    .local v2, "fragmentManager":Landroid/app/FragmentManager;
    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    .line 2778
    .local v3, "fragmentTransaction":Landroid/app/FragmentTransaction;
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;-><init>()V

    .line 2780
    .local v0, "advancedFragment":Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2781
    .local v1, "b":Landroid/os/Bundle;
    const-string v4, "SEARCH_EXTENSION_FOCUS"

    invoke-virtual {v1, v4, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2782
    const-string v4, "provider_id"

    iget-object v5, p0, Lcom/sec/android/app/myfiles/MainActivity;->deviceId:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2783
    const-string v4, "provider_name"

    iget-object v5, p0, Lcom/sec/android/app/myfiles/MainActivity;->deviceName:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2784
    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->setArguments(Landroid/os/Bundle;)V

    .line 2786
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v4, :cond_0

    .line 2787
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v3, v4}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 2788
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v3, v4}, Landroid/app/FragmentTransaction;->detach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 2789
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    const/4 v5, 0x0

    iput-object v5, v4, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .line 2791
    :cond_0
    const v4, 0x7f0f00c3

    const-string v5, "SearchAdvanceFragment"

    invoke-virtual {v3, v4, v0, v5}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 2793
    invoke-virtual {v3, v0}, Landroid/app/FragmentTransaction;->attach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 2795
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget v4, v4, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mId:I

    const/16 v5, 0x12

    if-eq v4, v5, :cond_1

    .line 2797
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget v4, v4, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mId:I

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setmBackFragment(I)V

    .line 2799
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    const/16 v5, 0x22

    iput v5, v4, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mId:I

    .line 2801
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->searchModeFinish()V

    .line 2802
    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 2804
    return-void
.end method

.method public initialActionBar()V
    .locals 1

    .prologue
    .line 735
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->getActionBarManager()Lcom/sec/android/app/myfiles/view/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/view/ActionBarManager;->initialize()V

    .line 736
    return-void
.end method

.method protected loadComponentFromView()V
    .locals 0

    .prologue
    .line 2951
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 16
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 1970
    invoke-super/range {p0 .. p3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1971
    const/4 v12, -0x1

    move/from16 v0, p2

    if-ne v0, v12, :cond_9

    if-eqz p3, :cond_9

    .line 1972
    const/4 v12, 0x2

    const-string v13, "MainActivity"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "onActivityResult() resultCode : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " requestCode : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1973
    packed-switch p1, :pswitch_data_0

    .line 2096
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1975
    :pswitch_1
    const/4 v12, 0x3

    move-object/from16 v0, p0

    iput v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentSearchType:I

    .line 1976
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/MainActivity;->getFileType(Landroid/content/Intent;)Ljava/util/EnumSet;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFileTypes:Ljava/util/EnumSet;

    .line 1978
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v12}, Landroid/widget/SearchView;->requestFocus()Z

    .line 1979
    const/16 v12, 0x12

    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lcom/sec/android/app/myfiles/MainActivity;->setFragment(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 1982
    :pswitch_2
    const/4 v12, 0x2

    move-object/from16 v0, p0

    iput v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentSearchType:I

    .line 1983
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "search_set_location_is_near_by_device_intent_key"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 1984
    const-string v12, "provider_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1985
    .local v6, "deviceId":Ljava/lang/String;
    const-string v12, "provider_name"

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1986
    .local v7, "deviceName":Ljava/lang/String;
    if-eqz v6, :cond_2

    if-eqz v7, :cond_2

    .line 1987
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1988
    .local v2, "arg":Landroid/os/Bundle;
    const-string v12, "provider_id"

    invoke-virtual {v2, v12, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1989
    const/16 v12, 0x13

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v2}, Lcom/sec/android/app/myfiles/MainActivity;->setFragment(ILandroid/os/Bundle;)V

    .line 1990
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchable:Lcom/sec/android/app/myfiles/fragment/ISearchable;

    if-eqz v12, :cond_0

    .line 1991
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchable:Lcom/sec/android/app/myfiles/fragment/ISearchable;

    invoke-interface {v12}, Lcom/sec/android/app/myfiles/fragment/ISearchable;->clean()V

    goto :goto_0

    .line 1998
    .end local v2    # "arg":Landroid/os/Bundle;
    .end local v6    # "deviceId":Ljava/lang/String;
    .end local v7    # "deviceName":Ljava/lang/String;
    :cond_1
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "search_set_location_intent_key"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mStorageType:Ljava/util/ArrayList;

    .line 2001
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mStorageType:Ljava/util/ArrayList;

    const v13, 0x7f0b00fa

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/myfiles/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 2002
    const/16 v12, 0x20

    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lcom/sec/android/app/myfiles/MainActivity;->setFragment(ILandroid/os/Bundle;)V

    .line 2014
    :cond_2
    :goto_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v12}, Landroid/widget/SearchView;->requestFocus()Z

    goto/16 :goto_0

    .line 2006
    :cond_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mStorageType:Ljava/util/ArrayList;

    const v13, 0x7f0b0008

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/myfiles/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 2007
    const/16 v12, 0x14

    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lcom/sec/android/app/myfiles/MainActivity;->setFragment(ILandroid/os/Bundle;)V

    goto :goto_1

    .line 2011
    :cond_4
    const/16 v12, 0x12

    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lcom/sec/android/app/myfiles/MainActivity;->setFragment(ILandroid/os/Bundle;)V

    goto :goto_1

    .line 2018
    :pswitch_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/MainActivity;->searchModeStart()V

    .line 2019
    const/4 v12, 0x6

    move-object/from16 v0, p0

    iput v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentSearchType:I

    .line 2020
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v13

    const-string v14, "search_advance_name_intent_key"

    invoke-virtual {v13, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 2021
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "search_from_date_intent_key"

    const-wide/16 v14, 0x0

    invoke-virtual {v12, v13, v14, v15}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v12

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFromDate:J

    .line 2022
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "search_to_date_intent_key"

    const-wide/16 v14, 0x0

    invoke-virtual {v12, v13, v14, v15}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v12

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentToDate:J

    .line 2023
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/MainActivity;->getFileType(Landroid/content/Intent;)Ljava/util/EnumSet;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFileTypes:Ljava/util/EnumSet;

    .line 2024
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "search_advance_extention_intent_key"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mExtentionString:Ljava/lang/String;

    .line 2026
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "SEARCH_LOCATION_CHANGED"

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v11

    .line 2027
    .local v11, "showLocation":Z
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "SEARCH_FILETYPE_CHANGED"

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    .line 2028
    .local v10, "showFileType":Z
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "SEARCH_DATE_CHANGED"

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    .line 2029
    .local v8, "showDate":Z
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "SEARCH_EXTENSION_CHANGED"

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    .line 2030
    .local v9, "showExtension":Z
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFileTypes:Ljava/util/EnumSet;

    if-nez v12, :cond_5

    .line 2031
    const/4 v10, 0x0

    .line 2033
    :cond_5
    const/4 v12, 0x4

    new-array v5, v12, [Z

    const/4 v12, 0x0

    aput-boolean v11, v5, v12

    const/4 v12, 0x1

    aput-boolean v10, v5, v12

    const/4 v12, 0x2

    aput-boolean v8, v5, v12

    const/4 v12, 0x3

    aput-boolean v9, v5, v12

    .line 2034
    .local v5, "changedItem":[Z
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setmChangedItemsForAdvanceSearch([Z)V

    .line 2035
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v12}, Landroid/widget/SearchView;->onActionViewCollapsed()V

    .line 2036
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Landroid/widget/SearchView;->setVisibility(I)V

    .line 2037
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "search_set_location_is_near_by_device_intent_key"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 2038
    const-string v12, "provider_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->deviceId:Ljava/lang/String;

    .line 2039
    const-string v12, "provider_name"

    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->deviceName:Ljava/lang/String;

    .line 2040
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->deviceId:Ljava/lang/String;

    if-eqz v12, :cond_0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->deviceName:Ljava/lang/String;

    if-eqz v12, :cond_0

    .line 2041
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2042
    .restart local v2    # "arg":Landroid/os/Bundle;
    const-string v12, "SEARCH_LOCATION_CHANGED"

    invoke-virtual {v2, v12, v5}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    .line 2043
    const-string v12, "provider_id"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/MainActivity;->deviceId:Ljava/lang/String;

    invoke-virtual {v2, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2044
    const/16 v12, 0x13

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v2}, Lcom/sec/android/app/myfiles/MainActivity;->setFragment(ILandroid/os/Bundle;)V

    .line 2045
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mQueryTextListener:Landroid/widget/SearchView$OnQueryTextListener;

    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v13

    const-string v14, "search_advance_name_intent_key"

    invoke-virtual {v13, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Landroid/widget/SearchView$OnQueryTextListener;->onQueryTextSubmit(Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 2047
    .end local v2    # "arg":Landroid/os/Bundle;
    :cond_6
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "search_set_location_intent_key"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mStorageType:Ljava/util/ArrayList;

    .line 2049
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mStorageType:Ljava/util/ArrayList;

    const v13, 0x7f0b00fa

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/myfiles/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 2050
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2051
    .local v4, "bundle":Landroid/os/Bundle;
    const-string v12, "search_option_type"

    const/4 v13, 0x6

    invoke-virtual {v4, v12, v13}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2052
    const-string v12, "search_keyword_intent_key"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v13}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v13

    invoke-interface {v13}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v4, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2053
    const-string v12, "SEARCH_LOCATION_CHANGED"

    invoke-virtual {v4, v12, v5}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    .line 2054
    const/16 v12, 0x12

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v4}, Lcom/sec/android/app/myfiles/MainActivity;->setFragment(ILandroid/os/Bundle;)V

    .line 2055
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mQueryTextListener:Landroid/widget/SearchView$OnQueryTextListener;

    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v13

    const-string v14, "search_advance_name_intent_key"

    invoke-virtual {v13, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Landroid/widget/SearchView$OnQueryTextListener;->onQueryTextSubmit(Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 2059
    .end local v4    # "bundle":Landroid/os/Bundle;
    :cond_7
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mStorageType:Ljava/util/ArrayList;

    const v13, 0x7f0b0008

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/sec/android/app/myfiles/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 2060
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2061
    .restart local v4    # "bundle":Landroid/os/Bundle;
    const-string v12, "search_option_type"

    const/4 v13, 0x6

    invoke-virtual {v4, v12, v13}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2062
    const-string v12, "search_keyword_intent_key"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v13}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v13

    invoke-interface {v13}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v4, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2063
    const-string v12, "SEARCH_LOCATION_CHANGED"

    invoke-virtual {v4, v12, v5}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    .line 2064
    const/16 v12, 0x12

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v4}, Lcom/sec/android/app/myfiles/MainActivity;->setFragment(ILandroid/os/Bundle;)V

    .line 2065
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mQueryTextListener:Landroid/widget/SearchView$OnQueryTextListener;

    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v13

    const-string v14, "search_advance_name_intent_key"

    invoke-virtual {v13, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Landroid/widget/SearchView$OnQueryTextListener;->onQueryTextSubmit(Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 2069
    .end local v4    # "bundle":Landroid/os/Bundle;
    :cond_8
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2070
    .local v3, "argument":Landroid/os/Bundle;
    const-string v12, "SEARCH_LOCATION_CHANGED"

    invoke-virtual {v3, v12, v5}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    .line 2071
    const/16 v12, 0x12

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v3}, Lcom/sec/android/app/myfiles/MainActivity;->setFragment(ILandroid/os/Bundle;)V

    .line 2072
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mQueryTextListener:Landroid/widget/SearchView$OnQueryTextListener;

    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v13

    const-string v14, "search_advance_name_intent_key"

    invoke-virtual {v13, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Landroid/widget/SearchView$OnQueryTextListener;->onQueryTextSubmit(Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 2079
    .end local v3    # "argument":Landroid/os/Bundle;
    .end local v5    # "changedItem":[Z
    .end local v8    # "showDate":Z
    .end local v9    # "showExtension":Z
    .end local v10    # "showFileType":Z
    .end local v11    # "showLocation":Z
    :pswitch_4
    const/4 v12, 0x4

    move-object/from16 v0, p0

    iput v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentSearchType:I

    .line 2080
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "search_from_date_intent_key"

    const-wide/16 v14, 0x0

    invoke-virtual {v12, v13, v14, v15}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v12

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFromDate:J

    .line 2081
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "search_to_date_intent_key"

    const-wide/16 v14, 0x0

    invoke-virtual {v12, v13, v14, v15}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v12

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentToDate:J

    .line 2083
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v12}, Landroid/widget/SearchView;->requestFocus()Z

    .line 2084
    const/16 v12, 0x12

    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lcom/sec/android/app/myfiles/MainActivity;->setFragment(ILandroid/os/Bundle;)V

    goto/16 :goto_0

    .line 2087
    :cond_9
    if-nez p2, :cond_0

    .line 2089
    const/4 v12, 0x6

    move/from16 v0, p1

    if-ne v0, v12, :cond_a

    .line 2090
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/MainActivity;->onBackPressed()V

    goto/16 :goto_0

    .line 2092
    :cond_a
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v12}, Landroid/widget/SearchView;->requestFocus()Z

    goto/16 :goto_0

    .line 1973
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 856
    const/4 v1, 0x0

    .line 857
    .local v1, "fromHomeBtn":Z
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/MainActivity;->mIsSearchMode:Z

    if-eqz v3, :cond_0

    .line 858
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchFromAdvance:Z

    if-eqz v3, :cond_4

    .line 859
    iget-object v3, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v3, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->ismIsHomeBtnSelected()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 860
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->searchModeFinish()V

    .line 861
    iget-object v3, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    const/4 v4, 0x0

    iput-object v4, v3, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .line 862
    const/4 v1, 0x1

    .line 877
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    if-eqz v3, :cond_1

    .line 878
    const-string v3, "MainActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onBackPressed() - mCurrentFragement.mId : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget v5, v5, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v7, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 879
    iget-object v3, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget v3, v3, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mId:I

    if-nez v3, :cond_6

    .line 881
    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/MainActivity;->mFinishNearByDevice:Z

    .line 882
    iget-object v3, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->isSelectMode()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 884
    iget-object v3, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->finishSelectMode()V

    .line 923
    :cond_1
    :goto_0
    return-void

    .line 864
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->searchModeFinish()V

    .line 865
    iget-object v3, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v3, v3, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;

    if-eqz v3, :cond_3

    .line 866
    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/MainActivity;->handleMenuItemAdvanced(I)V

    goto :goto_0

    .line 868
    :cond_3
    const/4 v3, -0x1

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/MainActivity;->handleMenuItemAdvanced(I)V

    goto :goto_0

    .line 872
    :cond_4
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/MainActivity;->isSearchPeforming:Z

    if-nez v3, :cond_0

    .line 873
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->searchModeFinish()V

    goto :goto_0

    .line 890
    :cond_5
    invoke-super {p0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onBackPressed()V

    goto :goto_0

    .line 896
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-nez v3, :cond_b

    .line 897
    iget-object v3, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-static {}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getmBackFragmentID()I

    move-result v2

    .line 898
    .local v2, "mBackFragment":I
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 899
    .local v0, "b":Landroid/os/Bundle;
    if-nez v2, :cond_8

    .line 900
    iget-object v3, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget v3, v3, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mId:I

    const/16 v4, 0x13

    if-ne v3, v4, :cond_7

    .line 901
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->finish()V

    goto :goto_0

    .line 904
    :cond_7
    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/MainActivity;->setDefaultView(Landroid/os/Bundle;)V

    .line 915
    :goto_1
    if-nez v1, :cond_1

    .line 916
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->searchModeStart()V

    goto :goto_0

    .line 906
    :cond_8
    const/16 v3, 0x201

    if-eq v2, v3, :cond_9

    const/16 v3, 0xa

    if-eq v2, v3, :cond_9

    const/16 v3, 0x9

    if-eq v2, v3, :cond_9

    const/16 v3, 0x8

    if-eq v2, v3, :cond_9

    const/16 v3, 0x1f

    if-ne v2, v3, :cond_a

    .line 908
    :cond_9
    const-string v3, "FOLDERPATH"

    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentDirectory()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 909
    const-string v3, "back_key_intent"

    invoke-virtual {v0, v3, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 912
    :cond_a
    invoke-virtual {p0, v2, v0}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto :goto_1

    .line 920
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v2    # "mBackFragment":I
    :cond_b
    iget-object v3, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onBackPressed()Z

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 929
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 931
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllListPopupWindow:Landroid/widget/ListPopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllListPopupWindow:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 933
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllListPopupWindow:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->dismiss()V

    .line 935
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllListPopupWindow:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->show()V

    .line 937
    :cond_0
    return-void
.end method

.method public onCopyMoveSelected(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 13
    .param p1, "argument"    # Landroid/os/Bundle;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    const/16 v12, 0x8

    const/4 v11, 0x1

    const/16 v10, 0x201

    const/4 v9, 0x0

    const/16 v8, 0xa

    .line 264
    const/4 v1, 0x0

    .line 266
    .local v1, "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    const/4 v6, -0x1

    .line 268
    .local v6, "srcFragmentId":I
    const/4 v0, -0x1

    .line 270
    .local v0, "dstFragmentId":I
    const-string v7, "src_fragment_id"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 272
    const-string v7, "src_fragment_id"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 275
    :cond_0
    const-string v7, "dest_fragment_id"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 277
    const-string v7, "dest_fragment_id"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 281
    :cond_1
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v7

    if-eqz v7, :cond_7

    const-string v7, "/"

    invoke-virtual {p2, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-static {p2}, Lcom/sec/android/app/myfiles/utils/Utils;->isBaiduFolder(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    :cond_2
    if-eq v6, v8, :cond_7

    if-eq v0, v8, :cond_7

    .line 284
    const-string v7, "RETURNPATH"

    invoke-virtual {p1, v7, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 285
    .local v5, "return_path":Ljava/lang/String;
    if-eqz v5, :cond_3

    .line 286
    const-string v7, "Baidu/"

    const-string v8, ""

    invoke-virtual {v5, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 287
    const-string v7, "RETURNPATH"

    invoke-virtual {p1, v7, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    :cond_3
    if-nez v1, :cond_4

    .line 291
    new-instance v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .end local v1    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    invoke-direct {v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;-><init>()V

    .line 292
    .restart local v1    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    const-string v7, "id"

    const/16 v8, 0x1f

    invoke-virtual {p1, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 293
    invoke-virtual {v1, p1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 295
    :cond_4
    const-string v7, "RETURNPATH"

    invoke-virtual {p1, v7, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_5

    .line 296
    const-string v7, "CLEAR_HISTORICALPATH"

    invoke-virtual {p1, v7, v11}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 298
    :cond_5
    const/16 v7, 0x1f

    invoke-direct {p0, v7, v1}, Lcom/sec/android/app/myfiles/MainActivity;->FragmentChangeCommit(ILcom/sec/android/app/myfiles/fragment/AbsFragment;)V

    .line 406
    .end local v5    # "return_path":Ljava/lang/String;
    :cond_6
    :goto_0
    return-void

    .line 302
    :cond_7
    const-string v7, "/"

    invoke-virtual {p2, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_8

    invoke-static {p2}, Lcom/sec/android/app/myfiles/utils/Utils;->isDropboxFolder(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_c

    :cond_8
    if-eq v6, v8, :cond_c

    if-eq v0, v8, :cond_c

    .line 303
    const-string v7, "RETURNPATH"

    invoke-virtual {p1, v7, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 304
    .restart local v5    # "return_path":Ljava/lang/String;
    if-eqz v5, :cond_9

    .line 305
    const-string v7, "Dropbox/"

    const-string v8, ""

    invoke-virtual {v5, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 306
    const-string v7, "RETURNPATH"

    invoke-virtual {p1, v7, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    :cond_9
    if-nez v1, :cond_a

    .line 310
    new-instance v1, Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    .end local v1    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    invoke-direct {v1}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;-><init>()V

    .line 311
    .restart local v1    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    const-string v7, "id"

    invoke-virtual {p1, v7, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 312
    invoke-virtual {v1, p1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 314
    :cond_a
    const-string v7, "RETURNPATH"

    invoke-virtual {p1, v7, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_b

    .line 315
    const-string v7, "CLEAR_HISTORICALPATH"

    invoke-virtual {p1, v7, v11}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 317
    :cond_b
    invoke-direct {p0, v12, v1}, Lcom/sec/android/app/myfiles/MainActivity;->FragmentChangeCommit(ILcom/sec/android/app/myfiles/fragment/AbsFragment;)V

    goto :goto_0

    .line 318
    .end local v5    # "return_path":Ljava/lang/String;
    :cond_c
    invoke-static {p2}, Lcom/sec/android/app/myfiles/utils/Utils;->isCategoryTag(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 320
    invoke-static {p2}, Lcom/sec/android/app/myfiles/utils/Utils;->getCategoryId(Ljava/lang/String;)I

    move-result v4

    .line 321
    .local v4, "id":I
    if-nez v1, :cond_d

    .line 322
    new-instance v1, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    .end local v1    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    invoke-direct {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;-><init>()V

    .line 323
    .restart local v1    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    const-string v7, "id"

    invoke-virtual {p1, v7, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 324
    const-string v7, "category_type"

    invoke-virtual {p1, v7, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 325
    invoke-virtual {v1, p1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 328
    :cond_d
    invoke-direct {p0, v4, v1}, Lcom/sec/android/app/myfiles/MainActivity;->FragmentChangeCommit(ILcom/sec/android/app/myfiles/fragment/AbsFragment;)V

    goto :goto_0

    .line 330
    .end local v4    # "id":I
    :cond_e
    invoke-static {p2}, Lcom/sec/android/app/myfiles/utils/Utils;->isInExternalSdStorage(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_f

    invoke-static {p2}, Lcom/sec/android/app/myfiles/utils/Utils;->isInUsbHostStorage(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_f

    invoke-static {p2}, Lcom/sec/android/app/myfiles/utils/Utils;->isInternalFolderPath(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_f

    invoke-static {p2}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxFolder(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_11

    :cond_f
    if-eq v0, v8, :cond_11

    .line 337
    if-nez v1, :cond_10

    .line 338
    new-instance v1, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    .end local v1    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    invoke-direct {v1}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;-><init>()V

    .line 339
    .restart local v1    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    const-string v7, "id"

    invoke-virtual {p1, v7, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 340
    invoke-virtual {v1, p1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 343
    :cond_10
    invoke-direct {p0, v10, v1}, Lcom/sec/android/app/myfiles/MainActivity;->FragmentChangeCommit(ILcom/sec/android/app/myfiles/fragment/AbsFragment;)V

    goto/16 :goto_0

    .line 347
    :cond_11
    const-string v7, "from_back"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_13

    const-string v7, "from_back"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 350
    .local v2, "fromBackPress":Z
    :goto_1
    if-eqz v2, :cond_14

    .line 352
    packed-switch v6, :pswitch_data_0

    goto/16 :goto_0

    .line 356
    :pswitch_0
    if-eq v0, v10, :cond_12

    if-ne v0, v8, :cond_6

    .line 358
    :cond_12
    new-instance v3, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    invoke-direct {v3}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;-><init>()V

    .line 360
    .local v3, "ftpFragment":Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;
    const-string v7, "id"

    invoke-virtual {p1, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 362
    invoke-virtual {v3, p1}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->setArguments(Landroid/os/Bundle;)V

    .line 364
    invoke-direct {p0, v8, v3}, Lcom/sec/android/app/myfiles/MainActivity;->FragmentChangeCommit(ILcom/sec/android/app/myfiles/fragment/AbsFragment;)V

    goto/16 :goto_0

    .line 347
    .end local v2    # "fromBackPress":Z
    .end local v3    # "ftpFragment":Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;
    :cond_13
    const/4 v2, 0x0

    goto :goto_1

    .line 376
    .restart local v2    # "fromBackPress":Z
    :cond_14
    sparse-switch v6, :sswitch_data_0

    goto/16 :goto_0

    .line 386
    :sswitch_0
    if-ne v0, v8, :cond_6

    .line 388
    new-instance v3, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    invoke-direct {v3}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;-><init>()V

    .line 390
    .restart local v3    # "ftpFragment":Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;
    const-string v7, "id"

    invoke-virtual {p1, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 392
    invoke-virtual {v3, p1}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->setArguments(Landroid/os/Bundle;)V

    .line 394
    invoke-direct {p0, v8, v3}, Lcom/sec/android/app/myfiles/MainActivity;->FragmentChangeCommit(ILcom/sec/android/app/myfiles/fragment/AbsFragment;)V

    goto/16 :goto_0

    .line 352
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch

    .line 376
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_0
        0x4 -> :sswitch_0
        0x5 -> :sswitch_0
        0xa -> :sswitch_0
        0x12 -> :sswitch_0
        0x201 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 20
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 432
    const/16 v17, 0x0

    const-string v18, "MainActivity"

    const-string v19, "MainActivity : onCreate"

    invoke-static/range {v17 .. v19}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 434
    if-eqz p1, :cond_0

    .line 436
    const-string v9, "android:fragments"

    .line 438
    .local v9, "fragmentKey":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 440
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 444
    .end local v9    # "fragmentKey":Ljava/lang/String;
    :cond_0
    invoke-super/range {p0 .. p1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onCreate(Landroid/os/Bundle;)V

    .line 446
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v12

    .line 448
    .local v12, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v0, v12, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    move/from16 v17, v0

    or-int/lit8 v17, v17, 0x2

    move/from16 v0, v17

    iput v0, v12, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 450
    iget v0, v12, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    move/from16 v17, v0

    or-int/lit8 v17, v17, 0x1

    move/from16 v0, v17

    iput v0, v12, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 452
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 454
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/myfiles/MainActivity;->setExternalStorageReceiver()V

    .line 456
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/myfiles/MainActivity;->setSideSyncReceiver()V

    .line 458
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/myfiles/MainActivity;->setConnectionChangeReceiver()V

    .line 460
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/myfiles/MainActivity;->setSecretReceiver()V

    .line 462
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/MainActivity;->registerReceiver()V

    .line 464
    const/16 v17, 0x0

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/myfiles/utils/Utils;->setMediaScannerScannig(Z)V

    .line 470
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v11

    .line 471
    .local v11, "intent":Landroid/content/Intent;
    if-eqz v11, :cond_1

    .line 473
    const-string v17, "from_home_shortcut"

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/MainActivity;->mFromHomeShortcut:Z

    .line 475
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mFromHomeShortcut:Z

    move/from16 v17, v0

    if-nez v17, :cond_1

    .line 477
    invoke-virtual {v11}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v10

    .line 478
    .local v10, "fromFindoSelectedFile":Ljava/lang/String;
    const-string v17, "FOLDERPATH"

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 479
    .local v16, "startPath":Ljava/lang/String;
    const-string v17, "START_FOLDER"

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 481
    .local v15, "startFolderPath":Ljava/lang/String;
    const-string v17, "remote_share_viewer"

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v14

    .line 483
    .local v14, "remoteShareViewer":Ljava/lang/Boolean;
    const-string v17, "android.intent.action.VIEW_DOWNLOADS"

    invoke-virtual {v11}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 485
    const/16 v17, 0x1

    const-string v18, "MainActivity"

    const-string v19, "onCreate ACTION_VIEW_DOWNLAODS"

    invoke-static/range {v17 .. v19}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 487
    const/16 v17, 0x28

    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    .line 535
    .end local v10    # "fromFindoSelectedFile":Ljava/lang/String;
    .end local v14    # "remoteShareViewer":Ljava/lang/Boolean;
    .end local v15    # "startFolderPath":Ljava/lang/String;
    .end local v16    # "startPath":Ljava/lang/String;
    :cond_1
    :goto_0
    const-string v17, "1.0"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/myfiles/utils/Utils;->getKNOXVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 536
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/myfiles/utils/Utils;->InitKNOXContainerInstallManager(Landroid/content/Context;)V

    .line 538
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/MainActivity;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    .line 545
    return-void

    .line 489
    .restart local v10    # "fromFindoSelectedFile":Ljava/lang/String;
    .restart local v14    # "remoteShareViewer":Ljava/lang/Boolean;
    .restart local v15    # "startFolderPath":Ljava/lang/String;
    .restart local v16    # "startPath":Ljava/lang/String;
    :cond_3
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_5

    .line 490
    const/16 v17, 0x0

    const-string v18, "MainActivity"

    const-string v19, "MainActivity start from Findo"

    invoke-static/range {v17 .. v19}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 493
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 494
    .local v7, "findoFile":Ljava/io/File;
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 496
    .local v3, "argument":Landroid/os/Bundle;
    invoke-virtual {v7}, Ljava/io/File;->isFile()Z

    move-result v17

    if-eqz v17, :cond_4

    .line 498
    const/16 v17, 0x0

    const-string v18, "/"

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v10, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    .line 499
    .local v13, "pathForFile":Ljava/lang/String;
    const-string v17, "FOLDERPATH"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    const-string v17, "from_findo_is_folder"

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 508
    .end local v13    # "pathForFile":Ljava/lang/String;
    :goto_1
    const-string v17, "from_findo_file_position"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    const/16 v17, 0x201

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 504
    :cond_4
    const-string v17, "FOLDERPATH"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    const-string v17, "from_findo_is_folder"

    const/16 v18, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_1

    .line 510
    .end local v3    # "argument":Landroid/os/Bundle;
    .end local v7    # "findoFile":Ljava/io/File;
    :cond_5
    if-eqz v16, :cond_6

    .line 511
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 512
    .restart local v3    # "argument":Landroid/os/Bundle;
    const-string v17, "FOLDERPATH"

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    new-instance v5, Ljava/io/File;

    move-object/from16 v0, v16

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 514
    .local v5, "file":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v17

    if-eqz v17, :cond_1

    .line 515
    const/16 v17, 0x201

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto/16 :goto_0

    .line 517
    .end local v3    # "argument":Landroid/os/Bundle;
    .end local v5    # "file":Ljava/io/File;
    :cond_6
    if-eqz v15, :cond_7

    .line 518
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 519
    .restart local v3    # "argument":Landroid/os/Bundle;
    const-string v17, "FOLDERPATH"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 521
    .local v6, "file2":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v17

    if-eqz v17, :cond_1

    .line 522
    const/16 v17, 0x201

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto/16 :goto_0

    .line 524
    .end local v3    # "argument":Landroid/os/Bundle;
    .end local v6    # "file2":Ljava/io/File;
    :cond_7
    invoke-virtual {v14}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v17

    if-eqz v17, :cond_1

    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isRemoteShareEnabled(Landroid/content/Context;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 525
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 526
    .restart local v3    # "argument":Landroid/os/Bundle;
    const-string v17, "REMOTE_SHARE_ID"

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 527
    .local v8, "folderId":Ljava/lang/String;
    const-string v17, "REMOTE_SHARE_DIR"

    const/16 v18, -0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 528
    .local v4, "dir":I
    const-string v17, "REMOTE_SHARE_ID"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 529
    const-string v17, "REMOTE_SHARE_DIR"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 530
    const/16 v17, 0x25

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 6

    .prologue
    const/4 v4, 0x2

    .line 814
    invoke-super {p0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onDestroy()V

    .line 816
    new-instance v1, Landroid/content/Intent;

    const-string v2, "action.index.THUMBNAIL"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 817
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/MainActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 818
    const-string v2, "MainActivity"

    const-string v3, "MainActivity : destroy my files"

    invoke-static {v4, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 820
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSecretReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_0

    .line 822
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSecretReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/MainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 828
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->unRegisterReceiver()V

    .line 832
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchLocationDeviceIDClear()V

    .line 833
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->commit()Z

    .line 835
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget v2, v2, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mId:I

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/MainActivity;->mFinishNearByDevice:Z

    if-eqz v2, :cond_1

    .line 837
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v2, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->nearByDevicesConnected()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 839
    const-string v2, "MainActivity"

    const-string v3, "MainActivity : onDestroy() - resetNearByDevicesList"

    invoke-static {v4, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 840
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v2, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->resetNearByDevicesList()V

    .line 842
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Shortcut;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    const-string v4, "type=9"

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 846
    :cond_1
    invoke-static {}, Lcom/sec/android/app/myfiles/db/CacheDB;->finalClose()V

    .line 847
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/myfiles/MainActivity;->mFinishNearByDevice:Z

    .line 848
    const-string v2, "1.0"

    iget-object v3, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->getKNOXVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 849
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->ClearKNOXContainerInstallManager()V

    .line 851
    :cond_2
    return-void

    .line 823
    :catch_0
    move-exception v0

    .line 824
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onKeyShortcut(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 3180
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v1

    and-int/lit16 v0, v1, -0x7001

    .line 3182
    .local v0, "filteredMetaState":I
    invoke-static {v0}, Landroid/view/KeyEvent;->metaStateHasNoModifiers(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3184
    packed-switch p1, :pswitch_data_0

    .line 3216
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onKeyShortcut(ILandroid/view/KeyEvent;)Z

    move-result v1

    :goto_1
    return v1

    .line 3188
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    if-eqz v1, :cond_1

    .line 3190
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->isSelectMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3192
    const/4 v1, 0x0

    goto :goto_1

    .line 3196
    :cond_1
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/android/app/myfiles/MainActivity$11;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/MainActivity$11;-><init>(Lcom/sec/android/app/myfiles/MainActivity;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 3184
    :pswitch_data_0
    .packed-switch 0x22
        :pswitch_0
    .end packed-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 3039
    const/16 v0, 0x54

    if-ne p1, v0, :cond_0

    .line 3041
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->searchModeStart()V

    .line 3042
    const/4 v0, 0x1

    .line 3045
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x1

    .line 1377
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 1415
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_0
    return v1

    .line 1380
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    if-eqz v1, :cond_2

    .line 1381
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    if-eqz v1, :cond_0

    .line 1382
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->setmIsHomeBtnSelected(Z)V

    .line 1384
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1386
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_1

    const-string v1, "android.intent.action.VIEW_DOWNLOADS"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    if-eqz v1, :cond_1

    .line 1391
    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1393
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->setDefaultView()V

    move v1, v2

    .line 1395
    goto :goto_0

    .line 1398
    :cond_1
    if-eqz v0, :cond_2

    const-string v1, "remote_share_viewer"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1400
    const-string v1, "remote_share_viewer"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 1403
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->onBackPressed()V

    move v1, v2

    .line 1405
    goto :goto_0

    .line 1409
    :sswitch_1
    iput-boolean v2, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectionTypeChange:Z

    .line 1410
    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/MainActivity;->handleMenuItemAdvanced(I)V

    move v1, v2

    .line 1411
    goto :goto_0

    .line 1377
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0f014c -> :sswitch_1
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 722
    invoke-super {p0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onPause()V

    .line 723
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v2, 0x0

    .line 3148
    const v1, 0x7f0f014c

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .local v0, "menuItem":Landroid/view/MenuItem;
    if-eqz v0, :cond_1

    .line 3149
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchFromAdvance:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3150
    :cond_0
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 3156
    :cond_1
    :goto_0
    const v1, 0x7f0f0142

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 3158
    sget-boolean v1, Lcom/sec/android/app/myfiles/utils/MyFilesFeatures;->US_MODEL:Z

    if-eqz v1, :cond_4

    .line 3160
    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 3167
    :cond_2
    :goto_1
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    return v1

    .line 3152
    :cond_3
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 3163
    :cond_4
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setShowAsAction(I)V

    goto :goto_1
.end method

.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 1
    .param p1, "arg0"    # Ljava/lang/String;

    .prologue
    .line 2956
    const/4 v0, 0x0

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 1
    .param p1, "arg0"    # Ljava/lang/String;

    .prologue
    .line 2962
    const/4 v0, 0x0

    return v0
.end method

.method protected onResume()V
    .locals 13

    .prologue
    const/16 v1, 0x8

    const/16 v12, 0x1f

    const/16 v11, 0x13

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 551
    invoke-super {p0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onResume()V

    .line 553
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    .line 554
    .local v8, "intent":Landroid/content/Intent;
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 556
    .local v6, "argument":Landroid/os/Bundle;
    if-eqz v8, :cond_3

    .line 558
    const-string v2, "from_home_shortcut"

    invoke-virtual {v8, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/myfiles/MainActivity;->mFromHomeShortcut:Z

    .line 560
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/MainActivity;->mFromHomeShortcut:Z

    if-eqz v2, :cond_3

    .line 562
    invoke-virtual {v8}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v3

    .line 563
    .local v3, "fromHomeShortcutSelectedFile":Ljava/lang/String;
    const-string v2, "FOLDERPATH"

    invoke-virtual {v6, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 564
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v10, "from_home_shortcut"

    invoke-virtual {v2, v10}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 565
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v10, "FOLDERPATH"

    invoke-virtual {v2, v10}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 567
    if-eqz v3, :cond_2

    .line 568
    const-string v2, "Dropbox/"

    invoke-virtual {v3, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 569
    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isDropboxFolder(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isDropboxAccountIsSignin(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 571
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 572
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isWifiOn(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckDropBox()Z

    move-result v2

    if-nez v2, :cond_1

    .line 575
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v2, v2, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    if-eqz v2, :cond_1

    .line 576
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v2, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iput-boolean v4, v2, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->bDropBoxFromShortcut:Z

    .line 577
    new-instance v0, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    const-string v2, ""

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/element/ShortcutItem;-><init>(ILjava/lang/String;Ljava/lang/String;ZI)V

    .line 579
    .local v0, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/4 v2, 0x3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v4, v0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->showDataWarningDialog(ILandroid/content/Context;Lcom/sec/android/app/myfiles/element/ShortcutItem;)V

    .line 700
    .end local v0    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    .end local v3    # "fromHomeShortcutSelectedFile":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 586
    .restart local v3    # "fromHomeShortcutSelectedFile":Ljava/lang/String;
    :cond_1
    const-string v2, "IS_SHORTCUT_FOLDER"

    invoke-virtual {v6, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 587
    const-string v2, "SHORTCUT_ROOT_FOLDER"

    invoke-virtual {v6, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    invoke-virtual {p0, v1, v6}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    .line 690
    :cond_2
    :goto_1
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/MainActivity;->mFromHomeShortcut:Z

    .line 691
    const-string v1, "from_home_shortcut"

    invoke-virtual {v8, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 695
    .end local v3    # "fromHomeShortcutSelectedFile":Ljava/lang/String;
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mId:I

    const/16 v2, 0x22

    if-ne v1, v2, :cond_0

    .line 697
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->setDefaultView()V

    goto :goto_0

    .line 591
    .restart local v3    # "fromHomeShortcutSelectedFile":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0, v11, v6}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto :goto_1

    .line 595
    :cond_5
    const-string v1, "Baidu/"

    invoke-virtual {v3, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 596
    const-string v1, "MainActivity"

    const-string v2, "MainActivity  oncreate Baidu/"

    invoke-static {v5, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 597
    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isBaiduFolder(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isBaiduCloudAccountIsSignin(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 599
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 600
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isWifiOn(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckDropBox()Z

    move-result v1

    if-nez v1, :cond_6

    .line 603
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    if-eqz v1, :cond_6

    .line 604
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iput-boolean v4, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->bDropBoxFromShortcut:Z

    .line 606
    new-instance v0, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    const-string v2, ""

    move v1, v12

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/element/ShortcutItem;-><init>(ILjava/lang/String;Ljava/lang/String;ZI)V

    .line 608
    .restart local v0    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/4 v2, 0x5

    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v4, v0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->showDataWarningDialog(ILandroid/content/Context;Lcom/sec/android/app/myfiles/element/ShortcutItem;)V

    goto/16 :goto_0

    .line 616
    .end local v0    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :cond_6
    sget-boolean v1, Lcom/sec/android/app/myfiles/utils/Utils;->mIsActiveWarningUsingWLAN:Z

    if-eqz v1, :cond_7

    const-string v1, "ChinaNalSecurity"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v10, "CscFeature_Common_ConfigLocalSecurityPolicy"

    invoke-virtual {v2, v10}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 618
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isWifiOn(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckWLANOn()Z

    move-result v1

    if-nez v1, :cond_7

    .line 619
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    if-eqz v1, :cond_7

    .line 620
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iput-boolean v4, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->bDropBoxFromShortcut:Z

    .line 621
    new-instance v0, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    const-string v2, ""

    move v1, v12

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/element/ShortcutItem;-><init>(ILjava/lang/String;Ljava/lang/String;ZI)V

    .line 622
    .restart local v0    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/16 v2, 0xb

    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v4, v0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->showDataWarningDialog(ILandroid/content/Context;Lcom/sec/android/app/myfiles/element/ShortcutItem;)V

    goto/16 :goto_0

    .line 628
    .end local v0    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :cond_7
    const-string v1, "IS_SHORTCUT_FOLDER"

    invoke-virtual {v6, v1, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 629
    const-string v1, "SHORTCUT_ROOT_FOLDER"

    invoke-virtual {v6, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 630
    invoke-virtual {p0, v12, v6}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto/16 :goto_1

    .line 632
    :cond_8
    invoke-virtual {p0, v11, v6}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto/16 :goto_1

    .line 635
    :cond_9
    const-string v1, "{\"port\""

    invoke-virtual {v3, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 636
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 637
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isWifiOn(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckFTP()Z

    move-result v1

    if-nez v1, :cond_a

    .line 640
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    if-eqz v1, :cond_a

    .line 641
    new-instance v0, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    const/16 v1, 0x1c

    const-string v2, ""

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/element/ShortcutItem;-><init>(ILjava/lang/String;Ljava/lang/String;ZI)V

    .line 643
    .restart local v0    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/4 v2, 0x4

    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v4, v0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->showDataWarningDialog(ILandroid/content/Context;Lcom/sec/android/app/myfiles/element/ShortcutItem;)V

    goto/16 :goto_0

    .line 652
    .end local v0    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :cond_a
    sget-boolean v1, Lcom/sec/android/app/myfiles/utils/Utils;->mIsActiveWarningUsingWLAN:Z

    if-eqz v1, :cond_b

    const-string v1, "ChinaNalSecurity"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v10, "CscFeature_Common_ConfigLocalSecurityPolicy"

    invoke-virtual {v2, v10}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 654
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isWifiOn(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckWLANOn()Z

    move-result v1

    if-nez v1, :cond_b

    .line 655
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    if-eqz v1, :cond_b

    .line 656
    new-instance v0, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    const/16 v1, 0x1c

    const-string v2, ""

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/element/ShortcutItem;-><init>(ILjava/lang/String;Ljava/lang/String;ZI)V

    .line 657
    .restart local v0    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/16 v2, 0xa

    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2, v4, v0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->showDataWarningDialog(ILandroid/content/Context;Lcom/sec/android/app/myfiles/element/ShortcutItem;)V

    goto/16 :goto_0

    .line 663
    .end local v0    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :cond_b
    const-string v1, "IS_SHORTCUT_FOLDER"

    invoke-virtual {v6, v1, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 664
    const-string v1, "SHORTCUT_ROOT_FOLDER"

    invoke-virtual {v6, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 665
    const/16 v1, 0xa

    invoke-virtual {p0, v1, v6}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto/16 :goto_1

    .line 667
    :cond_c
    invoke-virtual {p0, v11, v6}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto/16 :goto_1

    .line 672
    :cond_d
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 673
    .local v7, "file":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/storage/extSdCard"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    sget-boolean v1, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v1, :cond_f

    :cond_e
    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/storage/UsbDrive"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_10

    sget-boolean v1, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-nez v1, :cond_10

    :cond_f
    const-string v1, "file://"

    invoke-virtual {v3, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_10

    .line 678
    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    sget-char v4, Ljava/io/File;->separatorChar:C

    invoke-virtual {v2, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    .line 679
    .local v9, "shortcutName":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v1, v3, v9}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteShortcutFromHome(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->finish()V

    goto/16 :goto_1

    .line 684
    .end local v9    # "shortcutName":Ljava/lang/String;
    :cond_10
    const/16 v1, 0x201

    invoke-virtual {p0, v1, v6}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto/16 :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 414
    :try_start_0
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onSaveInstanceState(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 421
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    if-eqz v1, :cond_0

    .line 423
    const-string v1, "currentFragmentId"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget v2, v2, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mId:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 426
    :cond_0
    return-void

    .line 416
    :catch_0
    move-exception v0

    .line 418
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onTrimMemory(I)V
    .locals 4
    .param p1, "level"    # I

    .prologue
    .line 2968
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onTrimMemory(I)V

    .line 2969
    const/4 v0, 0x0

    const-string v1, "MainActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MainActivity : onTrimMemory : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 2970
    sparse-switch p1, :sswitch_data_0

    .line 2989
    :goto_0
    :sswitch_0
    return-void

    .line 2981
    :sswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/MainActivity;->lruCacheClear()V

    goto :goto_0

    .line 2970
    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0xa -> :sswitch_0
        0xf -> :sswitch_0
        0x14 -> :sswitch_0
        0x28 -> :sswitch_1
        0x3c -> :sswitch_1
        0x50 -> :sswitch_1
    .end sparse-switch
.end method

.method public onWindowFocusChanged(Z)V
    .locals 3
    .param p1, "hasFocus"    # Z

    .prologue
    const/16 v2, 0x22

    .line 707
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 709
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->getEditTextHasFocus()V

    .line 716
    :cond_0
    :goto_0
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mIsWindowFocused:Z

    .line 717
    return-void

    .line 710
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mId:I

    if-ne v0, v2, :cond_0

    if-eqz p1, :cond_0

    .line 711
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->getFragmentTagByID(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;

    if-eqz v0, :cond_0

    .line 712
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->getFragmentTagByID(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->showAdvancedSearchKeyboard()V

    goto :goto_0
.end method

.method protected refreshCategoryTree()V
    .locals 1

    .prologue
    .line 3172
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    if-eqz v0, :cond_0

    .line 3173
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->refreshCategoryTree()V

    .line 3175
    :cond_0
    return-void
.end method

.method public registerReceiver()V
    .locals 2

    .prologue
    .line 1680
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mExternalStorageReceiverFilter:Landroid/content/IntentFilter;

    if-nez v0, :cond_1

    .line 1682
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/MainActivity;->setExternalStorageReceiver()V

    .line 1685
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mExternalStorageReceiverFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/myfiles/MainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1687
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSideSyncReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSideSyncReceiverFilter:Landroid/content/IntentFilter;

    if-nez v0, :cond_3

    .line 1689
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/MainActivity;->setSideSyncReceiver()V

    .line 1692
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSideSyncReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSideSyncReceiverFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/myfiles/MainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1694
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mConnectionChangeReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mConnectionChangeReceiverFilter:Landroid/content/IntentFilter;

    if-eqz v0, :cond_4

    .line 1696
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mConnectionChangeReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mConnectionChangeReceiverFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/myfiles/MainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1698
    :cond_4
    return-void
.end method

.method public searchModeFinish()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3111
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mIsSearchMode:Z

    .line 3113
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    .line 3114
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->clearFocus()V

    .line 3116
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v0, :cond_1

    .line 3117
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->setMainActionBar()V

    .line 3118
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->invalidateOptionsMenu()V

    .line 3122
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mActionMode:Landroid/view/ActionMode;

    .line 3123
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mIsSearchMode:Z

    .line 3124
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchFromAdvance:Z

    .line 3125
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->isSearchPeforming:Z

    .line 3126
    return-void
.end method

.method public searchModeRestart()V
    .locals 3

    .prologue
    .line 3129
    const-string v0, ""

    .line 3131
    .local v0, "queryText":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    if-eqz v1, :cond_0

    .line 3132
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    iget-object v1, v1, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v1}, Landroid/widget/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3133
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->searchModeStart()V

    .line 3134
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    if-eqz v1, :cond_1

    .line 3135
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    iget-object v1, v1, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v1, v0}, Landroid/widget/SearchView$SearchAutoComplete;->setText(Ljava/lang/CharSequence;)V

    .line 3136
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    iget-object v1, v1, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/SearchView$SearchAutoComplete;->setSelection(I)V

    .line 3138
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mIsWindowFocused:Z

    if-nez v1, :cond_2

    .line 3139
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    if-eqz v1, :cond_2

    .line 3140
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->clearFocus()V

    .line 3143
    :cond_2
    return-void
.end method

.method public searchModeStart()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 3054
    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/MainActivity;->mIsSearchMode:Z

    .line 3055
    new-instance v4, Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    sget v7, Lcom/sec/android/app/myfiles/MainActivity;->FRAGMENT_LAYOUT_ID:I

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;-><init>(Landroid/app/FragmentManager;Landroid/content/Context;I)V

    iput-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchFragmentManager:Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;

    .line 3056
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->invalidateOptionsMenu()V

    .line 3057
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 3058
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 3059
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 3060
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 3062
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f040006

    invoke-virtual {v4, v5, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 3064
    .local v1, "customview":Landroid/view/View;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 3066
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f040007

    invoke-virtual {v4, v5, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 3069
    :cond_0
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/view/View;->setAlpha(F)V

    .line 3070
    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 3072
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v4, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    const-wide/16 v6, 0x190

    invoke-virtual {v4, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 3077
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 3079
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 3080
    const v4, 0x7f0f000e

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mAdvancedSearchResult:Landroid/widget/TextView;

    .line 3083
    :cond_1
    const v4, 0x7f0f000d

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/SearchView;

    iput-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    .line 3084
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    iget-object v4, v4, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    const-string v5, "disableEmoticonInput=true"

    invoke-virtual {v4, v5}, Landroid/widget/SearchView$SearchAutoComplete;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 3085
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v4}, Landroid/widget/SearchView;->onActionViewExpanded()V

    .line 3087
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    iget-object v4, v4, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-static {v5, v8}, Lcom/sec/android/app/myfiles/utils/Utils;->getEditTextFilter(Landroid/content/Context;I)[Landroid/text/InputFilter;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/SearchView$SearchAutoComplete;->setFilters([Landroid/text/InputFilter;)V

    .line 3088
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/MainActivity;->mQueryTextListener:Landroid/widget/SearchView$OnQueryTextListener;

    invoke-virtual {v4, v5}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    .line 3090
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v4}, Landroid/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const-string v5, "android:id/search_src_text"

    invoke-virtual {v4, v5, v10, v10}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 3091
    .local v3, "editTextId":I
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v4, v3}, Landroid/widget/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 3092
    .local v2, "editBoxView":Landroid/view/View;
    if-eqz v2, :cond_2

    instance-of v4, v2, Landroid/widget/EditText;

    if-eqz v4, :cond_2

    .line 3094
    check-cast v2, Landroid/widget/EditText;

    .end local v2    # "editBoxView":Landroid/view/View;
    invoke-virtual {v2, v9}, Landroid/widget/EditText;->getInputExtras(Z)Landroid/os/Bundle;

    move-result-object v0

    .line 3095
    .local v0, "b":Landroid/os/Bundle;
    const-string v4, "maxLength"

    const/16 v5, 0x32

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3097
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCalendar:Ljava/util/Calendar;

    .line 3098
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCalendar:Ljava/util/Calendar;

    const/4 v5, 0x6

    const/4 v6, -0x7

    invoke-virtual {v4, v5, v6}, Ljava/util/Calendar;->add(II)V

    .line 3106
    iput v9, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentSearchType:I

    .line 3107
    invoke-direct {p0, v1}, Lcom/sec/android/app/myfiles/MainActivity;->initSearchMenu(Landroid/view/View;)V

    .line 3108
    iput-boolean v8, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchFromAdvance:Z

    .line 3109
    return-void
.end method

.method public setDefaultView()V
    .locals 1

    .prologue
    .line 809
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/MainActivity;->setDefaultView(Landroid/os/Bundle;)V

    .line 810
    return-void
.end method

.method public setDefaultView(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "b"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 742
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 744
    .local v1, "fm":Landroid/app/FragmentManager;
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    .line 746
    .local v3, "ft":Landroid/app/FragmentTransaction;
    const/4 v2, 0x0

    .line 748
    .local v2, "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    const/4 v0, 0x0

    .line 755
    .local v0, "argument":Landroid/os/Bundle;
    if-eqz v2, :cond_1

    instance-of v4, v2, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    if-eqz v4, :cond_1

    move-object v4, v2

    .line 757
    check-cast v4, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getMlastOrientation()I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    if-eq v4, v5, :cond_0

    move-object v4, v2

    check-cast v4, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-boolean v4, v4, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->isResetNeeded:Z

    if-eqz v4, :cond_1

    .line 760
    :cond_0
    const-string v4, "MainActivity"

    const-string v5, "setDefaultView() : need to reload CategoryHomeFragment"

    invoke-static {v7, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 761
    const/4 v2, 0x0

    .line 765
    :cond_1
    if-nez v2, :cond_3

    .line 767
    const-string v4, "MainActivity"

    const-string v5, "fragment is null"

    invoke-static {v7, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 768
    new-instance v2, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .end local v2    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    invoke-direct {v2}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;-><init>()V

    .line 770
    .restart local v2    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "argument":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 772
    .restart local v0    # "argument":Landroid/os/Bundle;
    if-eqz p1, :cond_2

    .line 774
    const-string v4, "shortcut_working_index_intent_key"

    const-string v5, "shortcut_working_index_intent_key"

    invoke-virtual {p1, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 776
    const-string v4, "shortcut_mode_enable_intent_key"

    const-string v5, "shortcut_mode_enable_intent_key"

    invoke-virtual {p1, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 778
    const-string v4, "add_ftp"

    const-string v5, "add_ftp"

    invoke-virtual {p1, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 782
    :cond_2
    const-string v4, "id"

    invoke-virtual {v0, v4, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 784
    invoke-virtual {v2, v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 789
    :cond_3
    invoke-direct {p0, v6, v2}, Lcom/sec/android/app/myfiles/MainActivity;->FragmentChangeCommit(ILcom/sec/android/app/myfiles/fragment/AbsFragment;)V

    .line 801
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->invalidateOptionsMenu()V

    .line 803
    return-void
.end method

.method public setSelectAllClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "clickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 2111
    iput-object p1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllClickListener:Landroid/view/View$OnClickListener;

    .line 2112
    return-void
.end method

.method public setSelectOptionsVisibility(ZZ)V
    .locals 5
    .param p1, "selectAll"    # Z
    .param p2, "unselectAll"    # Z

    .prologue
    .line 2121
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllListPopupWindow:Landroid/widget/ListPopupWindow;

    if-nez v0, :cond_0

    .line 2139
    :goto_0
    return-void

    .line 2126
    :cond_0
    sget-object v1, Lcom/sec/android/app/myfiles/MainActivity;->mSelectionLock:Ljava/lang/Object;

    monitor-enter v1

    .line 2127
    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    .line 2128
    const/4 v0, 0x2

    :try_start_0
    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0035

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0037

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    iput-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectOptions:[Ljava/lang/String;

    .line 2129
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectOptionsIds:[I

    .line 2137
    :cond_1
    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2138
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllListPopupWindow:Landroid/widget/ListPopupWindow;

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    const v3, 0x7f040044

    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectOptions:[Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    .line 2130
    :cond_2
    if-eqz p1, :cond_3

    .line 2131
    const/4 v0, 0x1

    :try_start_1
    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0035

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    iput-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectOptions:[Ljava/lang/String;

    .line 2132
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput v3, v0, v2

    iput-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectOptionsIds:[I

    goto :goto_1

    .line 2137
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2133
    :cond_3
    if-eqz p2, :cond_1

    .line 2134
    const/4 v0, 0x1

    :try_start_2
    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0037

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    iput-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectOptions:[Ljava/lang/String;

    .line 2135
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v2, 0x0

    const/4 v3, 0x1

    aput v3, v0, v2

    iput-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectOptionsIds:[I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 2129
    nop

    :array_0
    .array-data 4
        0x0
        0x1
    .end array-data
.end method

.method public setSpinnerWidth(Ljava/lang/String;)V
    .locals 11
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    const v10, 0x7f0b0037

    const v9, 0x7f0b0035

    .line 3226
    iget-object v7, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllButton:Landroid/widget/TextView;

    if-eqz v7, :cond_0

    .line 3228
    iget-object v7, p0, Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 3230
    .local v4, "res":Landroid/content/res/Resources;
    iget-object v7, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllButton:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    .line 3232
    .local v3, "paint":Landroid/text/TextPaint;
    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-le v7, v8, :cond_1

    invoke-virtual {v4, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 3234
    .local v2, "opts":Ljava/lang/String;
    :goto_0
    invoke-virtual {v3, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v7

    float-to-int v7, v7

    const v8, 0x7f0901d8

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    add-int/2addr v7, v8

    const v8, 0x7f0901d7

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    add-int v0, v7, v8

    .line 3237
    .local v0, "contentWidth":I
    const v7, 0x7f0901cc

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    invoke-static {v0, v7}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 3239
    .local v1, "dropdownWidth":I
    if-nez p1, :cond_2

    .line 3241
    iget-object v7, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllListPopupWindow:Landroid/widget/ListPopupWindow;

    invoke-virtual {v7, v1}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    .line 3256
    .end local v0    # "contentWidth":I
    .end local v1    # "dropdownWidth":I
    .end local v2    # "opts":Ljava/lang/String;
    .end local v3    # "paint":Landroid/text/TextPaint;
    .end local v4    # "res":Landroid/content/res/Resources;
    :cond_0
    :goto_1
    return-void

    .line 3232
    .restart local v3    # "paint":Landroid/text/TextPaint;
    .restart local v4    # "res":Landroid/content/res/Resources;
    :cond_1
    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 3246
    .restart local v0    # "contentWidth":I
    .restart local v1    # "dropdownWidth":I
    .restart local v2    # "opts":Ljava/lang/String;
    :cond_2
    invoke-virtual {v3, p1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v7

    float-to-int v7, v7

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0901d0

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    add-int/2addr v7, v8

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0901d1

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    add-int v5, v7, v8

    .line 3249
    .local v5, "spinnerWidth":I
    invoke-static {v1, v5}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 3251
    .local v6, "width":I
    iget-object v7, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllButton:Landroid/widget/TextView;

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setWidth(I)V

    .line 3253
    iget-object v7, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllListPopupWindow:Landroid/widget/ListPopupWindow;

    invoke-virtual {v7, v6}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    goto :goto_1
.end method

.method public setUnselectAllClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "clickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 2116
    iput-object p1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mUnselectAllClickListener:Landroid/view/View$OnClickListener;

    .line 2117
    return-void
.end method

.method public startBrowser(ILandroid/os/Bundle;)V
    .locals 22
    .param p1, "type"    # I
    .param p2, "argument"    # Landroid/os/Bundle;

    .prologue
    .line 942
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    .line 944
    .local v6, "fm":Landroid/app/FragmentManager;
    invoke-virtual {v6}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v12

    .line 946
    .local v12, "ft":Landroid/app/FragmentTransaction;
    const/4 v7, 0x0

    .line 948
    .local v7, "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    const-string v10, "all_files"

    .line 950
    .local v10, "fragmentTag":Ljava/lang/String;
    move/from16 v8, p1

    .line 952
    .local v8, "fragmentId":I
    const/16 v18, 0x2

    const-string v19, "MainActivity"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "startBrowser() - fragmentId : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 954
    sparse-switch v8, :sswitch_data_0

    .line 1033
    :goto_0
    sparse-switch v8, :sswitch_data_1

    .line 1371
    :cond_0
    :goto_1
    :sswitch_0
    return-void

    .line 957
    :sswitch_1
    const-string v10, "all_files"

    .line 958
    goto :goto_0

    .line 961
    :sswitch_2
    const-string v10, "images"

    .line 962
    goto :goto_0

    .line 965
    :sswitch_3
    const-string v10, "videos"

    .line 966
    goto :goto_0

    .line 969
    :sswitch_4
    const-string v10, "music"

    .line 970
    goto :goto_0

    .line 973
    :sswitch_5
    const-string v10, "documents"

    .line 974
    goto :goto_0

    .line 977
    :sswitch_6
    const-string v10, "recently_files"

    .line 978
    goto :goto_0

    .line 981
    :sswitch_7
    const-string v10, "downloaded_apps"

    .line 982
    goto :goto_0

    .line 985
    :sswitch_8
    const-string v10, "dropbox"

    .line 986
    goto :goto_0

    .line 989
    :sswitch_9
    const-string v10, "remote_share"

    .line 990
    goto :goto_0

    .line 993
    :sswitch_a
    const-string v10, "nearby_devices"

    .line 994
    goto :goto_0

    .line 998
    :sswitch_b
    const-string v10, "ftp"

    .line 999
    goto :goto_0

    .line 1002
    :sswitch_c
    const-string v10, "sftp"

    .line 1003
    goto :goto_0

    .line 1006
    :sswitch_d
    const-string v10, "add_ftp"

    .line 1007
    goto :goto_0

    .line 1010
    :sswitch_e
    const-string v10, "add_ftps"

    .line 1011
    goto :goto_0

    .line 1014
    :sswitch_f
    const-string v10, "add_sftp"

    .line 1015
    goto :goto_0

    .line 1017
    :sswitch_10
    const-string v10, "ftp_list"

    .line 1018
    goto :goto_0

    .line 1021
    :sswitch_11
    const-string v10, "baidu"

    .line 1022
    goto :goto_0

    .line 1025
    :sswitch_12
    const-string v10, "no_network"

    .line 1026
    goto :goto_0

    .line 1029
    :sswitch_13
    const-string v10, "history"

    goto :goto_0

    .line 1039
    :sswitch_14
    if-nez v7, :cond_2

    .line 1041
    new-instance v7, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    .end local v7    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    invoke-direct {v7}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;-><init>()V

    .line 1043
    .restart local v7    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    if-nez p2, :cond_1

    .line 1045
    new-instance p2, Landroid/os/Bundle;

    .end local p2    # "argument":Landroid/os/Bundle;
    invoke-direct/range {p2 .. p2}, Landroid/os/Bundle;-><init>()V

    .line 1048
    .restart local p2    # "argument":Landroid/os/Bundle;
    :cond_1
    const-string v18, "id"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1050
    move-object/from16 v0, p2

    invoke-virtual {v7, v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 1364
    :cond_2
    :goto_2
    if-eqz v7, :cond_0

    const/16 v18, 0x13

    move/from16 v0, v18

    if-eq v8, v0, :cond_0

    .line 1365
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v7}, Lcom/sec/android/app/myfiles/MainActivity;->FragmentChangeCommit(ILcom/sec/android/app/myfiles/fragment/AbsFragment;)V

    goto :goto_1

    .line 1067
    :sswitch_15
    if-nez v7, :cond_2

    .line 1069
    new-instance v7, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    .end local v7    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    invoke-direct {v7}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;-><init>()V

    .line 1071
    .restart local v7    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    if-nez p2, :cond_3

    .line 1073
    new-instance p2, Landroid/os/Bundle;

    .end local p2    # "argument":Landroid/os/Bundle;
    invoke-direct/range {p2 .. p2}, Landroid/os/Bundle;-><init>()V

    .line 1076
    .restart local p2    # "argument":Landroid/os/Bundle;
    :cond_3
    const-string v18, "id"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1078
    const-string v18, "category_type"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1080
    move-object/from16 v0, p2

    invoke-virtual {v7, v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_2

    .line 1098
    :sswitch_16
    if-nez v7, :cond_2

    .line 1100
    new-instance v7, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    .end local v7    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    invoke-direct {v7}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;-><init>()V

    .line 1102
    .restart local v7    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    if-nez p2, :cond_4

    .line 1104
    new-instance p2, Landroid/os/Bundle;

    .end local p2    # "argument":Landroid/os/Bundle;
    invoke-direct/range {p2 .. p2}, Landroid/os/Bundle;-><init>()V

    .line 1107
    .restart local p2    # "argument":Landroid/os/Bundle;
    :cond_4
    const-string v18, "id"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1109
    const-string v18, "category_type"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1111
    move-object/from16 v0, p2

    invoke-virtual {v7, v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_2

    .line 1126
    :sswitch_17
    if-nez v7, :cond_2

    .line 1128
    new-instance v7, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    .end local v7    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    invoke-direct {v7}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;-><init>()V

    .line 1130
    .restart local v7    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    if-nez p2, :cond_5

    .line 1132
    new-instance p2, Landroid/os/Bundle;

    .end local p2    # "argument":Landroid/os/Bundle;
    invoke-direct/range {p2 .. p2}, Landroid/os/Bundle;-><init>()V

    .line 1136
    .restart local p2    # "argument":Landroid/os/Bundle;
    :cond_5
    const-string v18, "id"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1138
    const-string v18, "category_type"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1140
    move-object/from16 v0, p2

    invoke-virtual {v7, v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->setArguments(Landroid/os/Bundle;)V

    goto/16 :goto_2

    .line 1152
    :sswitch_18
    if-nez v7, :cond_2

    .line 1154
    new-instance v7, Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    .end local v7    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    invoke-direct {v7}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;-><init>()V

    .line 1156
    .restart local v7    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    if-nez p2, :cond_6

    .line 1158
    new-instance p2, Landroid/os/Bundle;

    .end local p2    # "argument":Landroid/os/Bundle;
    invoke-direct/range {p2 .. p2}, Landroid/os/Bundle;-><init>()V

    .line 1161
    .restart local p2    # "argument":Landroid/os/Bundle;
    :cond_6
    const-string v18, "id"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1163
    move-object/from16 v0, p2

    invoke-virtual {v7, v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->setArguments(Landroid/os/Bundle;)V

    goto/16 :goto_2

    .line 1172
    :sswitch_19
    if-nez v7, :cond_2

    .line 1174
    new-instance v7, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    .end local v7    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    invoke-direct {v7}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;-><init>()V

    .line 1176
    .restart local v7    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    if-nez p2, :cond_7

    .line 1178
    new-instance p2, Landroid/os/Bundle;

    .end local p2    # "argument":Landroid/os/Bundle;
    invoke-direct/range {p2 .. p2}, Landroid/os/Bundle;-><init>()V

    .line 1181
    .restart local p2    # "argument":Landroid/os/Bundle;
    :cond_7
    const-string v18, "id"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1183
    move-object/from16 v0, p2

    invoke-virtual {v7, v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->setArguments(Landroid/os/Bundle;)V

    goto/16 :goto_2

    .line 1191
    :sswitch_1a
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v18

    if-eqz v18, :cond_2

    .line 1192
    if-nez v7, :cond_2

    .line 1193
    new-instance v7, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    .end local v7    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    invoke-direct {v7}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;-><init>()V

    .line 1195
    .restart local v7    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    if-nez p2, :cond_8

    .line 1196
    new-instance p2, Landroid/os/Bundle;

    .end local p2    # "argument":Landroid/os/Bundle;
    invoke-direct/range {p2 .. p2}, Landroid/os/Bundle;-><init>()V

    .line 1199
    .restart local p2    # "argument":Landroid/os/Bundle;
    :cond_8
    const-string v18, "id"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1200
    move-object/from16 v0, p2

    invoke-virtual {v7, v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->setArguments(Landroid/os/Bundle;)V

    goto/16 :goto_2

    .line 1207
    :sswitch_1b
    const/4 v15, 0x0

    .line 1209
    .local v15, "ndFragment":Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;
    if-nez v15, :cond_a

    .line 1210
    new-instance v15, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    .end local v15    # "ndFragment":Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;
    invoke-direct {v15}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;-><init>()V

    .line 1211
    .restart local v15    # "ndFragment":Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;
    invoke-static {}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;->getInstance()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesNotifier;->registerListenerToProviderRemoveNotification(Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesProviderNotifiable;)Z

    .line 1213
    if-nez p2, :cond_9

    .line 1214
    new-instance p2, Landroid/os/Bundle;

    .end local p2    # "argument":Landroid/os/Bundle;
    invoke-direct/range {p2 .. p2}, Landroid/os/Bundle;-><init>()V

    .line 1217
    .restart local p2    # "argument":Landroid/os/Bundle;
    :cond_9
    const-string v18, "id"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1218
    move-object/from16 v0, p2

    invoke-virtual {v15, v0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->setArguments(Landroid/os/Bundle;)V

    .line 1221
    :cond_a
    move-object v7, v15

    .line 1223
    goto/16 :goto_2

    .line 1227
    .end local v15    # "ndFragment":Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;
    :sswitch_1c
    const/4 v13, 0x0

    .line 1229
    .local v13, "ftpFragment":Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;
    if-nez v13, :cond_b

    .line 1230
    new-instance v13, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    .end local v13    # "ftpFragment":Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;
    invoke-direct {v13}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;-><init>()V

    .line 1231
    .restart local v13    # "ftpFragment":Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;
    if-nez p2, :cond_b

    .line 1232
    new-instance p2, Landroid/os/Bundle;

    .end local p2    # "argument":Landroid/os/Bundle;
    invoke-direct/range {p2 .. p2}, Landroid/os/Bundle;-><init>()V

    .line 1236
    .restart local p2    # "argument":Landroid/os/Bundle;
    :cond_b
    const-string v18, "id"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1237
    const-string v18, "ftp_type"

    sget-object v19, Lcom/sec/android/app/myfiles/ftp/FTPType;->FTP:Lcom/sec/android/app/myfiles/ftp/FTPType;

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/ftp/FTPType;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1238
    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->setArguments(Landroid/os/Bundle;)V

    .line 1239
    move-object v7, v13

    .line 1241
    goto/16 :goto_2

    .line 1248
    .end local v13    # "ftpFragment":Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;
    :sswitch_1d
    const/4 v3, 0x0

    .line 1250
    .local v3, "addFtpFragment":Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;
    if-eqz v3, :cond_c

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->isResumed()Z

    move-result v18

    if-eqz v18, :cond_d

    .line 1252
    :cond_c
    new-instance v3, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    .end local v3    # "addFtpFragment":Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;
    invoke-direct {v3}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;-><init>()V

    .line 1255
    .restart local v3    # "addFtpFragment":Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;
    :cond_d
    move-object v7, v3

    .line 1257
    if-nez p2, :cond_e

    .line 1259
    new-instance p2, Landroid/os/Bundle;

    .end local p2    # "argument":Landroid/os/Bundle;
    invoke-direct/range {p2 .. p2}, Landroid/os/Bundle;-><init>()V

    .line 1262
    .restart local p2    # "argument":Landroid/os/Bundle;
    :cond_e
    const-string v18, "id"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1263
    const-string v18, "category_type"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1264
    move-object/from16 v0, p2

    invoke-virtual {v7, v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 1265
    const v18, 0x7f050006

    const v19, 0x7f050007

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v12, v0, v1}, Landroid/app/FragmentTransaction;->setCustomAnimations(II)Landroid/app/FragmentTransaction;

    goto/16 :goto_2

    .line 1270
    .end local v3    # "addFtpFragment":Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;
    :sswitch_1e
    const/4 v4, 0x0

    .line 1272
    .local v4, "addFtpsFragment":Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;
    if-nez v4, :cond_f

    .line 1274
    new-instance v4, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;

    .end local v4    # "addFtpsFragment":Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;
    invoke-direct {v4}, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;-><init>()V

    .line 1277
    .restart local v4    # "addFtpsFragment":Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;
    :cond_f
    move-object v7, v4

    .line 1279
    if-nez p2, :cond_10

    .line 1281
    new-instance p2, Landroid/os/Bundle;

    .end local p2    # "argument":Landroid/os/Bundle;
    invoke-direct/range {p2 .. p2}, Landroid/os/Bundle;-><init>()V

    .line 1284
    .restart local p2    # "argument":Landroid/os/Bundle;
    :cond_10
    const-string v18, "id"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1285
    const-string v18, "category_type"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1286
    move-object/from16 v0, p2

    invoke-virtual {v7, v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 1287
    const v18, 0x7f050006

    const v19, 0x7f050007

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v12, v0, v1}, Landroid/app/FragmentTransaction;->setCustomAnimations(II)Landroid/app/FragmentTransaction;

    goto/16 :goto_2

    .line 1292
    .end local v4    # "addFtpsFragment":Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;
    :sswitch_1f
    const/4 v5, 0x0

    .line 1294
    .local v5, "addSFtpFragment":Lcom/sec/android/app/myfiles/fragment/AddSFTPFragment;
    if-nez v5, :cond_11

    .line 1296
    new-instance v5, Lcom/sec/android/app/myfiles/fragment/AddSFTPFragment;

    .end local v5    # "addSFtpFragment":Lcom/sec/android/app/myfiles/fragment/AddSFTPFragment;
    invoke-direct {v5}, Lcom/sec/android/app/myfiles/fragment/AddSFTPFragment;-><init>()V

    .line 1299
    .restart local v5    # "addSFtpFragment":Lcom/sec/android/app/myfiles/fragment/AddSFTPFragment;
    :cond_11
    move-object v7, v5

    .line 1301
    if-nez p2, :cond_12

    .line 1303
    new-instance p2, Landroid/os/Bundle;

    .end local p2    # "argument":Landroid/os/Bundle;
    invoke-direct/range {p2 .. p2}, Landroid/os/Bundle;-><init>()V

    .line 1306
    .restart local p2    # "argument":Landroid/os/Bundle;
    :cond_12
    const-string v18, "id"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1307
    const-string v18, "category_type"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1308
    move-object/from16 v0, p2

    invoke-virtual {v7, v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 1309
    const v18, 0x7f050006

    const v19, 0x7f050007

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v12, v0, v1}, Landroid/app/FragmentTransaction;->setCustomAnimations(II)Landroid/app/FragmentTransaction;

    goto/16 :goto_2

    .line 1313
    .end local v5    # "addSFtpFragment":Lcom/sec/android/app/myfiles/fragment/AddSFTPFragment;
    :sswitch_20
    const/4 v14, 0x0

    .line 1315
    .local v14, "ftpListFragment":Lcom/sec/android/app/myfiles/fragment/FTPListFragment;
    if-nez v14, :cond_14

    .line 1317
    new-instance v14, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;

    .end local v14    # "ftpListFragment":Lcom/sec/android/app/myfiles/fragment/FTPListFragment;
    invoke-direct {v14}, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;-><init>()V

    .line 1318
    .restart local v14    # "ftpListFragment":Lcom/sec/android/app/myfiles/fragment/FTPListFragment;
    if-nez p2, :cond_13

    .line 1319
    new-instance p2, Landroid/os/Bundle;

    .end local p2    # "argument":Landroid/os/Bundle;
    invoke-direct/range {p2 .. p2}, Landroid/os/Bundle;-><init>()V

    .line 1322
    .restart local p2    # "argument":Landroid/os/Bundle;
    :cond_13
    const-string v18, "id"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1323
    const-string v18, "category_type"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1324
    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Lcom/sec/android/app/myfiles/fragment/FTPListFragment;->setArguments(Landroid/os/Bundle;)V

    .line 1326
    :cond_14
    move-object v7, v14

    .line 1327
    goto/16 :goto_2

    .line 1343
    .end local v14    # "ftpListFragment":Lcom/sec/android/app/myfiles/fragment/FTPListFragment;
    :sswitch_21
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v9

    .line 1344
    .local v9, "fragmentManager":Landroid/app/FragmentManager;
    invoke-virtual {v9}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v11

    .line 1345
    .local v11, "fragmentTransaction":Landroid/app/FragmentTransaction;
    new-instance v17, Lcom/sec/android/app/myfiles/fragment/NoNetworkFragment;

    invoke-direct/range {v17 .. v17}, Lcom/sec/android/app/myfiles/fragment/NoNetworkFragment;-><init>()V

    .line 1346
    .local v17, "nonetworkfragment":Lcom/sec/android/app/myfiles/fragment/NoNetworkFragment;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    move-object/from16 v18, v0

    if-eqz v18, :cond_15

    .line 1347
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 1348
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/app/FragmentTransaction;->detach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 1349
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .line 1351
    :cond_15
    const v18, 0x7f0f00c3

    const-string v19, "no_network"

    move/from16 v0, v18

    move-object/from16 v1, v17

    move-object/from16 v2, v19

    invoke-virtual {v11, v0, v1, v2}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 1353
    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/app/FragmentTransaction;->attach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 1355
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    move-object/from16 v18, v0

    const/16 v19, 0x13

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mId:I

    .line 1356
    invoke-virtual {v11}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1357
    new-instance v16, Lcom/sec/android/app/myfiles/fragment/NoNetworkFragment;

    invoke-direct/range {v16 .. v16}, Lcom/sec/android/app/myfiles/fragment/NoNetworkFragment;-><init>()V

    .line 1358
    .local v16, "noNetwork":Landroid/app/DialogFragment;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v18

    const-string v19, "storage_usage"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 954
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_7
        0x7 -> :sswitch_6
        0x8 -> :sswitch_8
        0x9 -> :sswitch_a
        0xa -> :sswitch_b
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x11 -> :sswitch_10
        0x13 -> :sswitch_12
        0x1f -> :sswitch_11
        0x25 -> :sswitch_9
        0x28 -> :sswitch_13
        0x201 -> :sswitch_1
    .end sparse-switch

    .line 1033
    :sswitch_data_1
    .sparse-switch
        0x2 -> :sswitch_15
        0x3 -> :sswitch_15
        0x4 -> :sswitch_15
        0x5 -> :sswitch_15
        0x6 -> :sswitch_16
        0x7 -> :sswitch_15
        0x8 -> :sswitch_18
        0x9 -> :sswitch_1b
        0xa -> :sswitch_1c
        0xb -> :sswitch_1c
        0xc -> :sswitch_0
        0xd -> :sswitch_1d
        0xe -> :sswitch_1e
        0xf -> :sswitch_1f
        0x11 -> :sswitch_20
        0x13 -> :sswitch_21
        0x1f -> :sswitch_1a
        0x25 -> :sswitch_19
        0x28 -> :sswitch_17
        0x201 -> :sswitch_14
    .end sparse-switch
.end method

.method public unRegisterReceiver()V
    .locals 2

    .prologue
    .line 1703
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 1705
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/MainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1710
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSideSyncReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 1712
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mSideSyncReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/MainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1717
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mConnectionChangeReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_2

    .line 1719
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity;->mConnectionChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/MainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 1724
    :cond_2
    :goto_2
    return-void

    .line 1706
    :catch_0
    move-exception v0

    .line 1707
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 1713
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 1714
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 1720
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v0

    .line 1721
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method public updateActionMode(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 2161
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mActionMode:Landroid/view/ActionMode;

    if-nez v0, :cond_0

    .line 2162
    new-instance v0, Lcom/sec/android/app/myfiles/MainActivity$7;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/myfiles/MainActivity$7;-><init>(Lcom/sec/android/app/myfiles/MainActivity;I)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/MainActivity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    .line 2586
    :cond_0
    return-void
.end method

.method public updateEmptySearchView()V
    .locals 4

    .prologue
    const v1, 0x7f0f00b0

    .line 2927
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->isSearchPeforming:Z

    if-eqz v0, :cond_0

    .line 2928
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 2929
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mEmptyTextView:Landroid/widget/TextView;

    .line 2931
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mEmptyTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 2932
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mEmptyTextView:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/android/app/myfiles/MainActivity$10;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/MainActivity$10;-><init>(Lcom/sec/android/app/myfiles/MainActivity;)V

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/TextView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2946
    :cond_0
    return-void
.end method

.method public updateEmptyView()V
    .locals 4

    .prologue
    const v3, 0x7f0f00b0

    .line 2905
    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 2906
    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/16 v1, 0x40

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2907
    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mEmptyTextView:Landroid/widget/TextView;

    .line 2909
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mEmptyTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    if-eqz v0, :cond_1

    .line 2910
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity;->mEmptyTextView:Landroid/widget/TextView;

    new-instance v1, Lcom/sec/android/app/myfiles/MainActivity$9;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/MainActivity$9;-><init>(Lcom/sec/android/app/myfiles/MainActivity;)V

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/TextView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2924
    :cond_1
    return-void
.end method
