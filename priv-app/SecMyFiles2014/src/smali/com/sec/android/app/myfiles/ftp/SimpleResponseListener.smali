.class public abstract Lcom/sec/android/app/myfiles/ftp/SimpleResponseListener;
.super Ljava/lang/Object;
.source "SimpleResponseListener.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBrowseOperationFailed(ZZI)V
    .locals 0
    .param p1, "connectionProblem"    # Z
    .param p2, "initialBrowse"    # Z
    .param p3, "errorCode"    # I

    .prologue
    .line 39
    return-void
.end method

.method public onBrowseResponseReceived(Ljava/util/List;Lcom/sec/android/app/myfiles/ftp/IFTPItem;)V
    .locals 0
    .param p2, "requestedFolderItem"    # Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ")V"
        }
    .end annotation

    .prologue
    .line 45
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    return-void
.end method

.method public onCloseConnection(Z)V
    .locals 0
    .param p1, "success"    # Z

    .prologue
    .line 50
    return-void
.end method

.method public onDiscoveryCompleted(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 75
    return-void
.end method

.method public onDiscoveryCompleted(Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;)V
    .locals 0
    .param p1, "visitor"    # Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;

    .prologue
    .line 80
    return-void
.end method

.method public onDownloadCompleted(Z[Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/FTPParams;Z)V
    .locals 0
    .param p1, "success"    # Z
    .param p2, "ftpPaths"    # [Ljava/lang/String;
    .param p3, "downloadedPath"    # Ljava/lang/String;
    .param p4, "srcParams"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p5, "move"    # Z

    .prologue
    .line 28
    return-void
.end method

.method public onFileDeleted()V
    .locals 0

    .prologue
    .line 60
    return-void
.end method

.method public onFileRenamed()V
    .locals 0

    .prologue
    .line 65
    return-void
.end method

.method public onFolderCreated()V
    .locals 0

    .prologue
    .line 55
    return-void
.end method

.method public onMoveFilesSelfCompleted()V
    .locals 0

    .prologue
    .line 70
    return-void
.end method

.method public onOpenConnection(Z)V
    .locals 0
    .param p1, "success"    # Z

    .prologue
    .line 33
    return-void
.end method

.method public onSelectCompleted(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "success"    # Z
    .param p2, "fullPath"    # Ljava/lang/String;
    .param p3, "target"    # Ljava/lang/String;

    .prologue
    .line 91
    return-void
.end method

.method public onUploadCompleted(ZLjava/lang/String;)V
    .locals 0
    .param p1, "success"    # Z
    .param p2, "moveSrc"    # Ljava/lang/String;

    .prologue
    .line 85
    return-void
.end method
