.class Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;
.super Ljava/lang/Object;
.source "CategoryHomeFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->updateCategorySize()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V
    .locals 0

    .prologue
    .line 1521
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 24

    .prologue
    .line 1526
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-nez v7, :cond_0

    .line 1651
    :goto_0
    return-void

    .line 1529
    :cond_0
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-boolean v7, v7, Lcom/sec/android/app/myfiles/AbsMainActivity;->mIsWorkingUpdateCategorySize:Z

    const/16 v22, 0x1

    move/from16 v0, v22

    if-ne v7, v0, :cond_1

    .line 1531
    const/4 v7, 0x2

    const-string v22, "CategoryHomeFragment"

    const-string v23, "updateCategorySize return! already working"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-static {v7, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1538
    :cond_1
    :try_start_0
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/16 v22, 0x1

    move/from16 v0, v22

    iput-boolean v0, v7, Lcom/sec/android/app/myfiles/AbsMainActivity;->mIsWorkingUpdateCategorySize:Z

    .line 1541
    const/4 v7, 0x2

    const-string v22, "CategoryHomeFragment"

    const-string v23, "updateCategorySize working!!! "

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-static {v7, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1544
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getEasyMode()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1549
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-static {v7, v0}, Lcom/sec/android/app/myfiles/utils/Utils;->getStorageCapacity(Landroid/content/Context;I)J

    move-result-wide v18

    .line 1551
    .local v18, "totalSize":J
    const/4 v7, 0x0

    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/Utils;->getStorageFreeSpace(I)J

    move-result-wide v10

    .line 1553
    .local v10, "totalAvailableSpace":J
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;
    invoke-static {v7}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    move-result-object v7

    const/16 v22, 0x201

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->getItemByType(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/myfiles/element/CategoryItem;

    sub-long v22, v18, v10

    move-wide/from16 v0, v22

    invoke-virtual {v7, v0, v1}, Lcom/sec/android/app/myfiles/element/CategoryItem;->setTotalSize(J)V

    .line 1556
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-static {v7, v0}, Lcom/sec/android/app/myfiles/utils/Utils;->getImageFilesSize(Landroid/content/Context;I)J

    move-result-wide v14

    .line 1558
    .local v14, "totalImageSize":J
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;
    invoke-static {v7}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    move-result-object v7

    const/16 v22, 0x2

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->getItemByType(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/myfiles/element/CategoryItem;

    invoke-virtual {v7, v14, v15}, Lcom/sec/android/app/myfiles/element/CategoryItem;->setTotalSize(J)V

    .line 1562
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-static {v7, v0}, Lcom/sec/android/app/myfiles/utils/Utils;->getVideoFilesSize(Landroid/content/Context;I)J

    move-result-wide v20

    .line 1564
    .local v20, "totalVideoSize":J
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;
    invoke-static {v7}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    move-result-object v7

    const/16 v22, 0x3

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->getItemByType(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/myfiles/element/CategoryItem;

    move-wide/from16 v0, v20

    invoke-virtual {v7, v0, v1}, Lcom/sec/android/app/myfiles/element/CategoryItem;->setTotalSize(J)V

    .line 1568
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-static {v7, v0}, Lcom/sec/android/app/myfiles/utils/Utils;->getMusicFilesSize(Landroid/content/Context;I)J

    move-result-wide v16

    .line 1570
    .local v16, "totalMusicSize":J
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;
    invoke-static {v7}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    move-result-object v7

    const/16 v22, 0x4

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->getItemByType(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/myfiles/element/CategoryItem;

    move-wide/from16 v0, v16

    invoke-virtual {v7, v0, v1}, Lcom/sec/android/app/myfiles/element/CategoryItem;->setTotalSize(J)V

    .line 1574
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-static {v7, v0}, Lcom/sec/android/app/myfiles/utils/Utils;->getDocumentFilesSize(Landroid/content/Context;I)J

    move-result-wide v12

    .line 1576
    .local v12, "totalDocumentSize":J
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;
    invoke-static {v7}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    move-result-object v7

    const/16 v22, 0x5

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->getItemByType(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/myfiles/element/CategoryItem;

    invoke-virtual {v7, v12, v13}, Lcom/sec/android/app/myfiles/element/CategoryItem;->setTotalSize(J)V

    .line 1578
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mUpdateCategorySizeHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$800(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Landroid/os/Handler;

    move-result-object v7

    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1646
    .end local v10    # "totalAvailableSpace":J
    .end local v12    # "totalDocumentSize":J
    .end local v14    # "totalImageSize":J
    .end local v16    # "totalMusicSize":J
    .end local v18    # "totalSize":J
    .end local v20    # "totalVideoSize":J
    :catch_0
    move-exception v6

    .line 1649
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 1583
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_2
    :try_start_1
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/Utils;->getRecentFilesSize(Landroid/content/Context;)J

    move-result-wide v8

    .line 1585
    .local v8, "recentlyFilesSize":J
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;
    invoke-static {v7}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    move-result-object v7

    const/16 v22, 0x7

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->getItemByType(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/myfiles/element/CategoryItem;

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/myfiles/element/CategoryItem;->setTotalSize(J)V

    .line 1589
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-static {v7, v0}, Lcom/sec/android/app/myfiles/utils/Utils;->getImageFilesSize(Landroid/content/Context;I)J

    move-result-wide v14

    .line 1591
    .restart local v14    # "totalImageSize":J
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;
    invoke-static {v7}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    move-result-object v7

    const/16 v22, 0x2

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->getItemByType(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/myfiles/element/CategoryItem;

    invoke-virtual {v7, v14, v15}, Lcom/sec/android/app/myfiles/element/CategoryItem;->setTotalSize(J)V

    .line 1595
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-static {v7, v0}, Lcom/sec/android/app/myfiles/utils/Utils;->getVideoFilesSize(Landroid/content/Context;I)J

    move-result-wide v20

    .line 1597
    .restart local v20    # "totalVideoSize":J
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;
    invoke-static {v7}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    move-result-object v7

    const/16 v22, 0x3

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->getItemByType(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/myfiles/element/CategoryItem;

    move-wide/from16 v0, v20

    invoke-virtual {v7, v0, v1}, Lcom/sec/android/app/myfiles/element/CategoryItem;->setTotalSize(J)V

    .line 1601
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-static {v7, v0}, Lcom/sec/android/app/myfiles/utils/Utils;->getMusicFilesSize(Landroid/content/Context;I)J

    move-result-wide v16

    .line 1603
    .restart local v16    # "totalMusicSize":J
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;
    invoke-static {v7}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    move-result-object v7

    const/16 v22, 0x4

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->getItemByType(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/myfiles/element/CategoryItem;

    move-wide/from16 v0, v16

    invoke-virtual {v7, v0, v1}, Lcom/sec/android/app/myfiles/element/CategoryItem;->setTotalSize(J)V

    .line 1618
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1619
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/Utils;->getDropboxFilesSize(Landroid/content/Context;)J

    move-result-wide v2

    .line 1620
    .local v2, "BaiduFilesSize":J
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;
    invoke-static {v7}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->setmTotalSize(J)V

    .line 1630
    .end local v2    # "BaiduFilesSize":J
    :goto_1
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;
    invoke-static {v7}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    move-result-object v7

    const/16 v22, 0x6

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->getItemByType(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/myfiles/element/CategoryItem;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getSavedDownloadedAppTotalSize()J

    move-result-wide v22

    move-wide/from16 v0, v22

    invoke-virtual {v7, v0, v1}, Lcom/sec/android/app/myfiles/element/CategoryItem;->setTotalSize(J)V

    .line 1633
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;
    invoke-static {v7}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    move-result-object v7

    const/16 v22, 0x5

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->getItemByType(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/myfiles/element/CategoryItem;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getSavedDocumentsTotalSize()J

    move-result-wide v22

    move-wide/from16 v0, v22

    invoke-virtual {v7, v0, v1}, Lcom/sec/android/app/myfiles/element/CategoryItem;->setTotalSize(J)V

    .line 1637
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mUpdateCategorySizeHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$800(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Landroid/os/Handler;

    move-result-object v7

    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v7, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1641
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # invokes: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getDownloadedAppSize()V
    invoke-static {v7}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$900(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    .line 1643
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # invokes: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getDocumentsTotalSize()V
    invoke-static {v7}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$1000(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V

    goto/16 :goto_0

    .line 1624
    :cond_3
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/Utils;->getDropboxFilesSize(Landroid/content/Context;)J

    move-result-wide v4

    .line 1625
    .local v4, "DropboxFilesSize":J
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$11;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;
    invoke-static {v7}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->setmTotalSize(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method
