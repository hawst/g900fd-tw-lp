.class public Lcom/sec/android/app/myfiles/hover/AbsHoverManager$HoverPopupHandler;
.super Landroid/os/Handler;
.source "AbsHoverManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "HoverPopupHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;


# direct methods
.method protected constructor <init>(Lcom/sec/android/app/myfiles/hover/AbsHoverManager;)V
    .locals 0

    .prologue
    .line 266
    iput-object p1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager$HoverPopupHandler;->this$0:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 270
    iget v6, p1, Landroid/os/Message;->what:I

    packed-switch v6, :pswitch_data_0

    .line 336
    :cond_0
    :goto_0
    return-void

    .line 273
    :pswitch_0
    const-string v6, "AbsHoverManager"

    const-string v7, "MSG_SHOW_POPUP"

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager$HoverPopupHandler;->this$0:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "pen_hovering"

    invoke-static {v6, v7, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-ne v6, v4, :cond_5

    move v2, v4

    .line 277
    .local v2, "penHovering":Z
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager$HoverPopupHandler;->this$0:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "finger_air_view"

    invoke-static {v6, v7, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-ne v6, v4, :cond_6

    move v0, v4

    .line 278
    .local v0, "fingerAirView":Z
    :goto_2
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager$HoverPopupHandler;->this$0:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "pen_hovering_information_preview"

    invoke-static {v6, v7, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-ne v6, v4, :cond_7

    move v3, v4

    .line 279
    .local v3, "penHoveringInfoPreview":Z
    :goto_3
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager$HoverPopupHandler;->this$0:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "finger_air_view_information_preview"

    invoke-static {v6, v7, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-ne v6, v4, :cond_8

    move v1, v4

    .line 281
    .local v1, "fingerAirViewInfoPreview":Z
    :goto_4
    if-eqz v2, :cond_1

    if-nez v3, :cond_2

    :cond_1
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 282
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager$HoverPopupHandler;->this$0:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mFilePath:Ljava/lang/String;

    if-nez v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager$HoverPopupHandler;->this$0:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mAppEntry:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    if-nez v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager$HoverPopupHandler;->this$0:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mRemoteDeviceImage:Landroid/graphics/drawable/Drawable;

    if-eqz v5, :cond_9

    .line 284
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager$HoverPopupHandler;->this$0:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->initViews()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 287
    iget-object v5, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager$HoverPopupHandler;->this$0:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager$HoverPopupHandler;->this$0:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getCurrentFragmentID()I

    move-result v6

    invoke-static {v6}, Lcom/sec/android/app/myfiles/utils/Utils;->getFragmentTagByID(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 288
    iget-object v5, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager$HoverPopupHandler;->this$0:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager$HoverPopupHandler;->this$0:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getCurrentFragmentID()I

    move-result v6

    invoke-static {v6}, Lcom/sec/android/app/myfiles/utils/Utils;->getFragmentTagByID(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v5

    instance-of v5, v5, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;

    if-nez v5, :cond_0

    .line 293
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager$HoverPopupHandler;->this$0:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->getHandler()Landroid/os/Handler;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .end local v0    # "fingerAirView":Z
    .end local v1    # "fingerAirViewInfoPreview":Z
    .end local v2    # "penHovering":Z
    .end local v3    # "penHoveringInfoPreview":Z
    :cond_5
    move v2, v5

    .line 276
    goto/16 :goto_1

    .restart local v2    # "penHovering":Z
    :cond_6
    move v0, v5

    .line 277
    goto/16 :goto_2

    .restart local v0    # "fingerAirView":Z
    :cond_7
    move v3, v5

    .line 278
    goto :goto_3

    .restart local v3    # "penHoveringInfoPreview":Z
    :cond_8
    move v1, v5

    .line 279
    goto :goto_4

    .line 297
    .restart local v1    # "fingerAirViewInfoPreview":Z
    :cond_9
    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager$HoverPopupHandler;->this$0:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->getHandler()Landroid/os/Handler;

    move-result-object v4

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 305
    .end local v0    # "fingerAirView":Z
    .end local v1    # "fingerAirViewInfoPreview":Z
    .end local v2    # "penHovering":Z
    .end local v3    # "penHoveringInfoPreview":Z
    :pswitch_1
    const-string v4, "AbsHoverManager"

    const-string v5, "MSG_SHOW"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager$HoverPopupHandler;->this$0:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->showDlg()V

    .line 307
    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager$HoverPopupHandler;->this$0:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->showContent()V

    goto/16 :goto_0

    .line 311
    :pswitch_2
    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager$HoverPopupHandler;->this$0:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->dissmissPopup()V

    goto/16 :goto_0

    .line 315
    :pswitch_3
    const-string v4, "AbsHoverManager"

    const-string v5, "MSG_ERROR_POPUP"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 319
    :pswitch_4
    const-string v4, "AbsHoverManager"

    const-string v5, "MSG_FOLDER_SHOW"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager$HoverPopupHandler;->this$0:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->changeList()V

    goto/16 :goto_0

    .line 324
    :pswitch_5
    const-string v4, "AbsHoverManager"

    const-string v5, "MSG_FOLDER_SHOW_STOP"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    const/4 v4, 0x4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager$HoverPopupHandler;->removeMessages(I)V

    goto/16 :goto_0

    .line 329
    :pswitch_6
    const-string v4, "AbsHoverManager"

    const-string v5, "MSG_FOLDER_SHOW_THREAD_START"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager$HoverPopupHandler;->this$0:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->changeListThreadStart()V

    goto/16 :goto_0

    .line 270
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
