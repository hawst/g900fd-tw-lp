.class Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$5;
.super Ljava/lang/Object;
.source "AbsListableFragment.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;)V
    .locals 0

    .prologue
    .line 482
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 2
    .param p1, "group"    # Landroid/widget/RadioGroup;
    .param p2, "checkedId"    # I

    .prologue
    .line 487
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget v0, v1, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCurrentSortBy:I

    .line 489
    .local v0, "sortBy":I
    packed-switch p2, :pswitch_data_0

    .line 508
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget v1, v1, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCurrentSortBy:I

    if-eq v1, v0, :cond_0

    .line 510
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iput v0, v1, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCurrentSortBy:I

    .line 512
    :cond_0
    return-void

    .line 492
    :pswitch_0
    const/4 v0, 0x1

    .line 493
    goto :goto_0

    .line 496
    :pswitch_1
    const/4 v0, 0x0

    .line 497
    goto :goto_0

    .line 500
    :pswitch_2
    const/4 v0, 0x2

    .line 501
    goto :goto_0

    .line 504
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 489
    :pswitch_data_0
    .packed-switch 0x7f0f0104
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
