.class public Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;
.super Landroid/os/AsyncTask;
.source "FTPDiscoverOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
        "Ljava/lang/Void;",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field private volatile mAbortOperation:Z

.field private final mCannotInfo:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

.field private final mDialogSync:Ljava/lang/Object;

.field private final mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

.field private final mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

.field private final mShowDialog:Z

.field private final mVisitor:Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "arg"    # Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;
    .param p2, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;
    .param p3, "visitor"    # Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;
    .param p4, "title"    # Ljava/lang/String;
    .param p5, "subtitle"    # Ljava/lang/String;
    .param p6, "cannot"    # Ljava/lang/String;
    .param p7, "showDialog"    # Z

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 189
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mAbortOperation:Z

    .line 47
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mContext:Landroid/content/Context;

    .line 48
    iput-object p6, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mCannotInfo:Ljava/lang/String;

    .line 49
    iput-object p3, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mVisitor:Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;

    .line 50
    iput-boolean p7, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mShowDialog:Z

    .line 51
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->getFTPConnectionHandler()Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    .line 52
    iput-object p2, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    .line 53
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->getDialogSync()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mDialogSync:Ljava/lang/Object;

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mVisitor:Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mVisitor:Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;

    new-instance v1, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation$1;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;)V

    invoke-interface {v0, v1}, Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;->setAbortHandler(Ljava/lang/Runnable;)V

    .line 63
    :cond_0
    new-instance v0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    invoke-virtual {v0, p4}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->setTitle(Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    invoke-virtual {v0, p5}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->setSubtitle(Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    new-instance v1, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation$2;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->setAbortAction(Ljava/lang/Runnable;)V

    .line 72
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;
    .param p1, "x1"    # Z

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mAbortOperation:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;)Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    return-object v0
.end method

.method private finishOperation()V
    .locals 1

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mShowDialog:Z

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->dismiss()V

    .line 155
    :cond_0
    return-void
.end method

.method private setFilename(Ljava/lang/String;)V
    .locals 2
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation$3;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation$3;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 178
    :cond_0
    return-void
.end method

.method private startOperation()V
    .locals 3

    .prologue
    .line 146
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mShowDialog:Z

    if-eqz v0, :cond_0

    .line 147
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v2, "fetching_dialog"

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 149
    :cond_0
    return-void
.end method

.method private visitFile(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)V
    .locals 1
    .param p1, "item"    # Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mVisitor:Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mVisitor:Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;

    invoke-interface {v0, p1}, Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;->visitFile(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)V

    .line 161
    :cond_0
    return-void
.end method

.method private visitFolder(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)V
    .locals 1
    .param p1, "item"    # Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mVisitor:Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mVisitor:Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;

    invoke-interface {v0, p1}, Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;->visitFolder(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)V

    .line 167
    :cond_0
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/sec/android/app/myfiles/ftp/IFTPItem;)Landroid/os/Bundle;
    .locals 15
    .param p1, "args"    # [Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .prologue
    .line 76
    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    .line 77
    .local v11, "result":Landroid/os/Bundle;
    iget-object v13, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mDialogSync:Ljava/lang/Object;

    monitor-enter v13

    .line 78
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->startOperation()V

    .line 80
    iget-object v12, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v12}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncPrepareConnection()Z

    move-result v12

    if-nez v12, :cond_0

    .line 82
    const/4 v11, 0x0

    monitor-exit v13

    .line 129
    .end local v11    # "result":Landroid/os/Bundle;
    :goto_0
    return-object v11

    .line 86
    .restart local v11    # "result":Landroid/os/Bundle;
    :cond_0
    new-instance v4, Ljava/util/ArrayDeque;

    invoke-direct {v4}, Ljava/util/ArrayDeque;-><init>()V

    .line 87
    .local v4, "discoveredFolders":Ljava/util/ArrayDeque;, "Ljava/util/ArrayDeque<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    new-instance v6, Ljava/util/ArrayDeque;

    invoke-direct {v6}, Ljava/util/ArrayDeque;-><init>()V

    .line 88
    .local v6, "discoveryOrderFolder":Ljava/util/ArrayDeque;, "Ljava/util/ArrayDeque<Ljava/lang/String;>;"
    new-instance v5, Ljava/util/ArrayDeque;

    invoke-direct {v5}, Ljava/util/ArrayDeque;-><init>()V

    .line 89
    .local v5, "discoveryOrderFile":Ljava/util/ArrayDeque;, "Ljava/util/ArrayDeque<Ljava/lang/String;>;"
    move-object/from16 v0, p1

    .local v0, "arr$":[Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    array-length v10, v0

    .local v10, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    move v8, v7

    .end local v7    # "i$":I
    .local v8, "i$":I
    :goto_1
    if-ge v8, v10, :cond_1

    aget-object v9, v0, v8

    .line 90
    .local v9, "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    iget-boolean v12, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mAbortOperation:Z

    if-eqz v12, :cond_3

    .line 121
    .end local v9    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    :cond_1
    iget-boolean v12, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mAbortOperation:Z

    if-nez v12, :cond_2

    .line 122
    const-string v14, "recur_disc_files"

    invoke-virtual {v5}, Ljava/util/ArrayDeque;->size()I

    move-result v12

    new-array v12, v12, [Ljava/lang/String;

    invoke-virtual {v5, v12}, Ljava/util/ArrayDeque;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v12

    check-cast v12, [Ljava/lang/String;

    invoke-virtual {v11, v14, v12}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 124
    const-string v14, "recur_disc_folders"

    invoke-virtual {v6}, Ljava/util/ArrayDeque;->size()I

    move-result v12

    new-array v12, v12, [Ljava/lang/String;

    invoke-virtual {v6, v12}, Ljava/util/ArrayDeque;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v12

    check-cast v12, [Ljava/lang/String;

    invoke-virtual {v11, v14, v12}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 127
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->finishOperation()V

    .line 128
    monitor-exit v13

    goto :goto_0

    .end local v0    # "arr$":[Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .end local v4    # "discoveredFolders":Ljava/util/ArrayDeque;, "Ljava/util/ArrayDeque<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    .end local v5    # "discoveryOrderFile":Ljava/util/ArrayDeque;, "Ljava/util/ArrayDeque<Ljava/lang/String;>;"
    .end local v6    # "discoveryOrderFolder":Ljava/util/ArrayDeque;, "Ljava/util/ArrayDeque<Ljava/lang/String;>;"
    .end local v8    # "i$":I
    .end local v10    # "len$":I
    .end local v11    # "result":Landroid/os/Bundle;
    :catchall_0
    move-exception v12

    monitor-exit v13
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v12

    .line 93
    .restart local v0    # "arr$":[Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .restart local v4    # "discoveredFolders":Ljava/util/ArrayDeque;, "Ljava/util/ArrayDeque<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    .restart local v5    # "discoveryOrderFile":Ljava/util/ArrayDeque;, "Ljava/util/ArrayDeque<Ljava/lang/String;>;"
    .restart local v6    # "discoveryOrderFolder":Ljava/util/ArrayDeque;, "Ljava/util/ArrayDeque<Ljava/lang/String;>;"
    .restart local v8    # "i$":I
    .restart local v9    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .restart local v10    # "len$":I
    .restart local v11    # "result":Landroid/os/Bundle;
    :cond_3
    if-eqz v9, :cond_a

    .line 94
    :try_start_1
    invoke-interface {v9}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isDirectory()Z

    move-result v12

    if-nez v12, :cond_4

    invoke-interface {v9}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isSymbolicLink()Z

    move-result v12

    if-eqz v12, :cond_8

    .line 95
    :cond_4
    invoke-virtual {v4, v9}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 96
    invoke-interface {v9}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v6, v12}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 97
    invoke-direct {p0, v9}, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->visitFolder(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)V

    .line 102
    .end local v8    # "i$":I
    :cond_5
    :goto_2
    iget-boolean v12, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mAbortOperation:Z

    if-nez v12, :cond_a

    invoke-virtual {v4}, Ljava/util/ArrayDeque;->size()I

    move-result v12

    if-lez v12, :cond_a

    .line 103
    invoke-virtual {v4}, Ljava/util/ArrayDeque;->pop()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .line 104
    .local v3, "currItem":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    invoke-interface {v3}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getTitle()Ljava/lang/String;

    move-result-object v12

    invoke-direct {p0, v12}, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->setFilename(Ljava/lang/String;)V

    .line 105
    iget-object v12, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v12, v3}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncBrowseForItemContent(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)Ljava/util/List;

    move-result-object v2

    .line 106
    .local v2, "children":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    if-eqz v2, :cond_5

    .line 107
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_6
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .line 108
    .local v1, "child":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    invoke-interface {v1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isDirectory()Z

    move-result v12

    if-nez v12, :cond_7

    invoke-interface {v1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isSymbolicLink()Z

    move-result v12

    if-eqz v12, :cond_9

    .line 109
    :cond_7
    invoke-virtual {v4, v1}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 110
    invoke-interface {v1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v6, v12}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 111
    invoke-direct {p0, v1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->visitFolder(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)V

    goto :goto_3

    .line 98
    .end local v1    # "child":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .end local v2    # "children":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    .end local v3    # "currItem":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .end local v7    # "i$":Ljava/util/Iterator;
    .restart local v8    # "i$":I
    :cond_8
    invoke-interface {v9}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isFile()Z

    move-result v12

    if-eqz v12, :cond_5

    .line 99
    invoke-interface {v9}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v12}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 100
    invoke-direct {p0, v9}, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->visitFile(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)V

    goto :goto_2

    .line 112
    .end local v8    # "i$":I
    .restart local v1    # "child":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .restart local v2    # "children":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    .restart local v3    # "currItem":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .restart local v7    # "i$":Ljava/util/Iterator;
    :cond_9
    invoke-interface {v1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isFile()Z

    move-result v12

    if-eqz v12, :cond_6

    .line 113
    invoke-interface {v1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v12}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 114
    invoke-direct {p0, v1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->visitFile(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 89
    .end local v1    # "child":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .end local v2    # "children":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    .end local v3    # "currItem":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .end local v7    # "i$":Ljava/util/Iterator;
    :cond_a
    add-int/lit8 v7, v8, 0x1

    .local v7, "i$":I
    move v8, v7

    .end local v7    # "i$":I
    .restart local v8    # "i$":I
    goto/16 :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 38
    check-cast p1, [Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->doInBackground([Lcom/sec/android/app/myfiles/ftp/IFTPItem;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "result"    # Landroid/os/Bundle;

    .prologue
    .line 134
    if-eqz p1, :cond_1

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mVisitor:Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mVisitor:Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;

    invoke-interface {v0, v1}, Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;->onDiscoveryCompleted(Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;)V

    .line 143
    :goto_0
    return-void

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    invoke-interface {v0, p1}, Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;->onDiscoveryCompleted(Landroid/os/Bundle;)V

    goto :goto_0

    .line 141
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->mCannotInfo:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 38
    check-cast p1, Landroid/os/Bundle;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPDiscoverOperation;->onPostExecute(Landroid/os/Bundle;)V

    return-void
.end method
