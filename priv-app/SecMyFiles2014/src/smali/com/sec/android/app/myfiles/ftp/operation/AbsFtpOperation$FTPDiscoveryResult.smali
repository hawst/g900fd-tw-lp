.class public Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;
.super Ljava/lang/Object;
.source "AbsFtpOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "FTPDiscoveryResult"
.end annotation


# instance fields
.field mFiles:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mFolders:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mJobFile:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;"
        }
    .end annotation
.end field

.field mJobFolder:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 927
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 929
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mJobFolder:Ljava/util/ArrayDeque;

    .line 931
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mFolders:Ljava/util/Stack;

    .line 933
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mJobFile:Ljava/util/ArrayDeque;

    .line 935
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mFiles:Ljava/util/Stack;

    return-void
.end method
