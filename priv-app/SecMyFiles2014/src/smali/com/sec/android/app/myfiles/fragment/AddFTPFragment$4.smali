.class Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$4;
.super Ljava/lang/Object;
.source "AddFTPFragment.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)V
    .locals 0

    .prologue
    .line 187
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 191
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 202
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 193
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mServerView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mAnonymousLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->access$500(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)Landroid/widget/RelativeLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mServerView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getHighlightColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    goto :goto_0

    .line 199
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mAnonymousLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->access$500(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)Landroid/widget/RelativeLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08003a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    goto :goto_0

    .line 191
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
