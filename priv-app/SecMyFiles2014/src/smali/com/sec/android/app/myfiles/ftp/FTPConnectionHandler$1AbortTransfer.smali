.class Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler$1AbortTransfer;
.super Landroid/os/AsyncTask;
.source "FTPConnectionHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->abortTransfer()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AbortTransfer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;)V
    .locals 0

    .prologue
    .line 887
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler$1AbortTransfer;->this$0:Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 887
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler$1AbortTransfer;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 5
    .param p1, "args"    # [Ljava/lang/Void;

    .prologue
    const/4 v4, 0x0

    .line 891
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler$1AbortTransfer;->this$0:Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;

    # setter for: Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mTransferring:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->access$002(Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;Z)Z

    .line 892
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler$1AbortTransfer;->this$0:Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mAbortTransferring:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->access$102(Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;Z)Z

    .line 895
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler$1AbortTransfer;->this$0:Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;

    # getter for: Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mClient:Lorg/apache/commons/net/ftp/FTPClient;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->access$200(Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;)Lorg/apache/commons/net/ftp/FTPClient;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/commons/net/ftp/FTPClient;->disconnect()V

    .line 897
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler$1AbortTransfer;->this$0:Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;

    # getter for: Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadIs:Ljava/io/InputStream;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->access$300(Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;)Ljava/io/InputStream;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 899
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler$1AbortTransfer;->this$0:Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;

    # getter for: Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadIs:Ljava/io/InputStream;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->access$300(Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;)Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 903
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler$1AbortTransfer;->this$0:Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;

    # getter for: Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadOs:Ljava/io/OutputStream;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->access$400(Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;)Ljava/io/OutputStream;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 905
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler$1AbortTransfer;->this$0:Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;

    # getter for: Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->mDownloadOs:Ljava/io/OutputStream;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->access$400(Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;)Ljava/io/OutputStream;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 923
    :cond_1
    :goto_0
    const/4 v1, 0x0

    return-object v1

    .line 909
    :catch_0
    move-exception v0

    .line 911
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "FTPConnectionHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "abortTransfer IOException: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 913
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler$1AbortTransfer;->this$0:Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;

    # invokes: Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->disconnect()V
    invoke-static {v1}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->access$500(Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;)V

    goto :goto_0

    .line 915
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 917
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "FTPConnectionHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "EXCEPTION: abortTransfer failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 919
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler$1AbortTransfer;->this$0:Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;

    # invokes: Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->recreateFtpClient()V
    invoke-static {v1}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;->access$600(Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandler;)V

    goto :goto_0
.end method
