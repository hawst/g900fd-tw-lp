.class public Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;
.super Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;
.source "NearbyDevicesSearchFragment.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/fragment/ISearchable;


# instance fields
.field private bBackOnsearch:Z

.field public mAdvancedSearchResult:Landroid/widget/TextView;

.field private final mHandler:Landroid/os/Handler;

.field private mLastSearchKeyowrd:Ljava/lang/String;

.field private mSavedSearchDate:Ljava/lang/String;

.field private mSavedSearchExtension:Ljava/lang/String;

.field private mSavedSearchFileType:Ljava/lang/String;

.field private mSearchNavigation:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

.field private mSearchView:Landroid/widget/SearchView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->bBackOnsearch:Z

    .line 281
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->bBackOnsearch:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 55
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->bBackOnsearch:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->callSuperAsyncOpenCompleted()V

    return-void
.end method

.method private afterAdvanceSearch()V
    .locals 15

    .prologue
    const/4 v14, 0x4

    const/4 v13, 0x1

    const/16 v12, 0x8

    const/4 v11, 0x0

    .line 284
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v8}, Landroid/widget/SearchView;->clearFocus()V

    .line 285
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v8, v12}, Landroid/widget/SearchView;->setVisibility(I)V

    .line 287
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    const v9, 0x7f0f004e

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    move-object v2, v8

    check-cast v2, Landroid/widget/RelativeLayout;

    .line 288
    .local v2, "searchFor":Landroid/widget/RelativeLayout;
    invoke-virtual {v2, v11}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 290
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v10, 0x7f0b0145

    invoke-virtual {v9, v10}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 291
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v8

    invoke-virtual {v8, v13}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 292
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v8, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/MainActivity;->invalidateOptionsMenu()V

    .line 293
    new-array v1, v14, [Z

    fill-array-data v1, :array_0

    .line 294
    .local v1, "changedItems":[Z
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-static {}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getmChangedItemsForAdvanceSearch()[Z

    move-result-object v1

    .line 295
    new-array v0, v14, [Z

    fill-array-data v0, :array_1

    .line 296
    .local v0, "a":[Z
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setmChangedItemsForAdvanceSearch([Z)V

    .line 297
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    const v9, 0x7f0f0050

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    move-object v7, v8

    check-cast v7, Landroid/widget/Button;

    .line 298
    .local v7, "searchForName":Landroid/widget/Button;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    const v9, 0x7f0f0051

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    move-object v6, v8

    check-cast v6, Landroid/widget/Button;

    .line 299
    .local v6, "searchForLocation":Landroid/widget/Button;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    const v9, 0x7f0f0052

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    move-object v5, v8

    check-cast v5, Landroid/widget/Button;

    .line 300
    .local v5, "searchForFileType":Landroid/widget/Button;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    const v9, 0x7f0f0053

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    move-object v3, v8

    check-cast v3, Landroid/widget/Button;

    .line 301
    .local v3, "searchForDate":Landroid/widget/Button;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    const v9, 0x7f0f0054

    invoke-virtual {v8, v9}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    move-object v4, v8

    check-cast v4, Landroid/widget/Button;

    .line 303
    .local v4, "searchForExtension":Landroid/widget/Button;
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mLastSearchKeyowrd:Ljava/lang/String;

    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 304
    invoke-virtual {v7, v11}, Landroid/widget/Button;->setVisibility(I)V

    .line 305
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mLastSearchKeyowrd:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 306
    new-instance v8, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$3;

    invoke-direct {v8, p0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$3;-><init>(Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;)V

    invoke-virtual {v7, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 315
    :cond_0
    aget-boolean v8, v1, v11

    if-eqz v8, :cond_1

    .line 316
    invoke-virtual {v6, v11}, Landroid/widget/Button;->setVisibility(I)V

    .line 317
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getSearchFileLocation()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/CharSequence;

    invoke-virtual {v6, v8}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 318
    new-instance v8, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$4;

    invoke-direct {v8, p0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$4;-><init>(Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;)V

    invoke-virtual {v6, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 327
    :cond_1
    aget-boolean v8, v1, v13

    if-eqz v8, :cond_2

    .line 328
    invoke-virtual {v5, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 329
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mSavedSearchFileType:Ljava/lang/String;

    invoke-virtual {v5, v8}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 330
    new-instance v8, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$5;

    invoke-direct {v8, p0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$5;-><init>(Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;)V

    invoke-virtual {v5, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 339
    :cond_2
    const/4 v8, 0x2

    aget-boolean v8, v1, v8

    if-eqz v8, :cond_3

    .line 340
    invoke-virtual {v3, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 341
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mSavedSearchDate:Ljava/lang/String;

    invoke-virtual {v3, v8}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 342
    new-instance v8, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$6;

    invoke-direct {v8, p0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$6;-><init>(Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;)V

    invoke-virtual {v3, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 351
    :cond_3
    const/4 v8, 0x3

    aget-boolean v8, v1, v8

    if-eqz v8, :cond_4

    .line 352
    invoke-virtual {v4, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 353
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mSavedSearchExtension:Ljava/lang/String;

    invoke-virtual {v4, v8}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 354
    new-instance v8, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$7;

    invoke-direct {v8, p0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$7;-><init>(Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;)V

    invoke-virtual {v4, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 363
    :cond_4
    return-void

    .line 293
    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 295
    :array_1
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method private callSuperAsyncOpenCompleted()V
    .locals 0

    .prologue
    .line 275
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->AsyncOpenCompleted()V

    .line 277
    return-void
.end method


# virtual methods
.method public AsyncOpenCompleted()V
    .locals 2

    .prologue
    .line 238
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$2;-><init>(Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 271
    return-void
.end method

.method public clean()V
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mSearchNavigation:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mSearchNavigation:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->clean()V

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mSearchNavigation:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 210
    :cond_0
    return-void
.end method

.method public getOptionsMenuResId(Z)I
    .locals 1
    .param p1, "isSelectMode"    # Z

    .prologue
    .line 150
    const v0, 0x7f0e0016

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 114
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 115
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->showTreeView(Z)V

    .line 116
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mSearchView:Landroid/widget/SearchView;

    .line 117
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mAdvancedSearchResult:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mAdvancedSearchResult:Landroid/widget/TextView;

    .line 125
    return-void
.end method

.method public onBackPressed()Z
    .locals 4

    .prologue
    .line 187
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-static {}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getmBackFragmentID()I

    move-result v1

    .line 188
    .local v1, "mBackFragment":I
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 189
    .local v0, "b":Landroid/os/Bundle;
    if-nez v1, :cond_1

    .line 190
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView(Landroid/os/Bundle;)V

    .line 201
    :cond_0
    :goto_0
    const/4 v2, 0x1

    return v2

    .line 191
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    instance-of v2, v2, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v2, :cond_0

    .line 192
    const/16 v2, 0x201

    if-eq v1, v2, :cond_2

    const/16 v2, 0x9

    if-eq v1, v2, :cond_2

    const/16 v2, 0xa

    if-ne v1, v2, :cond_3

    .line 194
    :cond_2
    const-string v2, "FOLDERPATH"

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentDirectory()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v2, v1, v0}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 231
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 232
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->showTreeView(Z)V

    .line 233
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 12
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 395
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-nez v5, :cond_0

    move v5, v6

    .line 452
    :goto_0
    return v5

    .line 398
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    iget v8, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mContextMenuPosition:I

    invoke-virtual {v5, v8}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/Cursor;

    .line 400
    .local v1, "cursor":Landroid/database/Cursor;
    if-nez v1, :cond_1

    move v5, v6

    .line 401
    goto :goto_0

    .line 404
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    :pswitch_0
    move v5, v6

    .line 452
    goto :goto_0

    .line 407
    :pswitch_1
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v5

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v5, v7}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setmAdapter(Landroid/widget/BaseAdapter;)V

    .line 408
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->showDetail(Ljava/lang/String;)V

    move v5, v6

    .line 409
    goto :goto_0

    .line 413
    :pswitch_2
    const/4 v3, 0x0

    .line 414
    .local v3, "retValue":Z
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v5, :cond_4

    .line 415
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 416
    .local v4, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v5, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    iget v8, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mContextMenuPosition:I

    invoke-virtual {v5, v8}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->getSelectedItemForSearchDownload(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 417
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/myfiles/element/BrowserItem;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getSelectedRemoteItem()Lcom/samsung/android/allshare/Item;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 419
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_3

    .line 421
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    if-nez v5, :cond_2

    .line 422
    const-string v5, "downloadItems()"

    const-string v6, "mProvider == null."

    invoke-static {v7, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    move v5, v7

    .line 423
    goto :goto_0

    .line 426
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/media/Provider;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/myfiles/nearbydevices/DownloadMenuOperation;->downloadItems(Ljava/util/ArrayList;Ljava/lang/String;)Z

    move-result v3

    .line 428
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 430
    .local v0, "count":I
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0c0003

    new-array v10, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v7

    invoke-virtual {v8, v9, v0, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .end local v0    # "count":I
    .end local v2    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v4    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    :goto_1
    move v5, v6

    .line 447
    goto/16 :goto_0

    .line 437
    .restart local v2    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .restart local v4    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v8, 0x7f0b0084

    invoke-virtual {p0, v8}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 443
    .end local v2    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v4    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    :cond_4
    const-string v5, "downloadItems()"

    const-string v8, "mAdapter == null."

    invoke-static {v7, v5, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 404
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 67
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->onCreate(Landroid/os/Bundle;)V

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mSearchNavigation:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    .line 73
    :cond_0
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 7
    .param p1, "menu"    # Landroid/view/ContextMenu;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    const/4 v6, 0x0

    .line 366
    if-nez p3, :cond_1

    .line 391
    :cond_0
    :goto_0
    return-void

    .line 370
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    iget v5, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mContextMenuPosition:I

    invoke-virtual {v4, v5}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    .line 372
    .local v2, "item":Ljava/lang/Object;
    if-eqz v2, :cond_0

    .line 377
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    iget v5, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mContextMenuPosition:I

    invoke-virtual {v4, v5}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 378
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 380
    const-string v4, "Extension"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 383
    .local v1, "ext":Ljava/lang/String;
    if-eqz v1, :cond_0

    if-eqz v1, :cond_2

    const-string v4, ""

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 386
    :cond_2
    const-string v4, "title"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 387
    .local v3, "name":Ljava/lang/String;
    invoke-interface {p1, v3}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    .line 388
    const/16 v4, 0xc

    const v5, 0x7f0b0148

    invoke-interface {p1, v6, v4, v6, v5}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 389
    const/16 v4, 0xa

    const v5, 0x7f0b0025

    invoke-interface {p1, v6, v4, v6, v5}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method protected onCreateItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;
    .locals 1

    .prologue
    .line 130
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 92
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 94
    .local v0, "view":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    if-eqz v1, :cond_0

    .line 95
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 97
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->registerForContextMenu(Landroid/view/View;)V

    .line 99
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->registerForContextMenu(Landroid/view/View;)V

    .line 102
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mEmptyView:Landroid/view/View;

    const v2, 0x7f0f00b0

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mEmptyTextView:Landroid/widget/TextView;

    .line 107
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mEmptyTextView:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 109
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 78
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->onDestroy()V

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->closeCursor()V

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mSearchNavigation:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    if-eqz v0, :cond_1

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mSearchNavigation:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->setSearchParameters(Lcom/sec/android/app/myfiles/utils/SearchParameter;)V

    .line 86
    :cond_1
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v4, 0x7f0f0134

    const v3, 0x7f0f0133

    const/4 v2, 0x0

    .line 215
    if-eqz p1, :cond_2

    .line 216
    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 217
    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 218
    :cond_0
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 219
    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 220
    :cond_1
    const v1, 0x7f0f0136

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 221
    .local v0, "homeMenuItem":Landroid/view/MenuItem;
    if-eqz v0, :cond_2

    .line 223
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 227
    .end local v0    # "homeMenuItem":Landroid/view/MenuItem;
    :cond_2
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 228
    return-void
.end method

.method public search(Lcom/sec/android/app/myfiles/utils/SearchParameter;)V
    .locals 10
    .param p1, "param"    # Lcom/sec/android/app/myfiles/utils/SearchParameter;

    .prologue
    const-wide/16 v8, 0x3e8

    const/4 v6, 0x0

    .line 156
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mSearchNavigation:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    if-eqz v2, :cond_2

    .line 157
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v2, v2, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v2, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mSearchFromAdvance:Z

    if-eqz v2, :cond_1

    .line 158
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getKeyword()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mLastSearchKeyowrd:Ljava/lang/String;

    .line 159
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 160
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getFileTypes()Ljava/util/EnumSet;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/EnumSet;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mSavedSearchFileType:Ljava/lang/String;

    .line 161
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mSavedSearchFileType:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mSavedSearchFileType:Ljava/lang/String;

    const/16 v4, 0x5b

    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mSavedSearchFileType:Ljava/lang/String;

    .line 162
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mSavedSearchFileType:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mSavedSearchFileType:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mSavedSearchFileType:Ljava/lang/String;

    .line 164
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getDateFrom()J

    move-result-wide v4

    mul-long/2addr v4, v8

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/Utils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 165
    .local v0, "fromDate":[Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getDateTo()J

    move-result-wide v4

    mul-long/2addr v4, v8

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/utils/Utils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 166
    .local v1, "toDate":[Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v3, v0, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  -  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, v1, v6

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mSavedSearchDate:Ljava/lang/String;

    .line 167
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getExtentionString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mSavedSearchExtension:Ljava/lang/String;

    .line 169
    .end local v0    # "fromDate":[Ljava/lang/String;
    .end local v1    # "toDate":[Ljava/lang/String;
    :cond_1
    invoke-virtual {p0, v6}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->resetPathIndicator(Z)V

    .line 170
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mSearchNavigation:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->setSearchParameters(Lcom/sec/android/app/myfiles/utils/SearchParameter;)V

    .line 171
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mSearchNavigation:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->goToRoot(Ljava/lang/Boolean;)Z

    .line 172
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v2, v2, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v2, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mSearchFromAdvance:Z

    if-eqz v2, :cond_2

    .line 173
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;->afterAdvanceSearch()V

    .line 176
    :cond_2
    return-void
.end method
