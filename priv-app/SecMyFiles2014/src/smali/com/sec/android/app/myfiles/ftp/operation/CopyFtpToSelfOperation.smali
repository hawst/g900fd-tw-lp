.class public Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;
.super Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;
.source "CopyFtpToSelfOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation$1;
    }
.end annotation


# static fields
.field private static final MODULE:Ljava/lang/String; = "CopyFtpToSelfOperation"


# instance fields
.field private mDoneCount:I

.field private mParam:Lcom/sec/android/app/myfiles/element/FileOperationParam;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "operation"    # I
    .param p3, "contentType"    # I
    .param p4, "progressView"    # Landroid/view/View;
    .param p5, "ftpHandlerParams"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;-><init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;)V

    .line 222
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mDoneCount:I

    .line 42
    return-void
.end method


# virtual methods
.method protected cancelOperation()V
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v0}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->abortTransfer()V

    .line 216
    invoke-super {p0}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->cancelOperation()V

    .line 218
    return-void
.end method

.method protected varargs doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;
    .locals 23
    .param p1, "args"    # [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .prologue
    .line 54
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->checkFileOperationParam(Z[Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/ftp/FTPErr;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 56
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    move-object/from16 v19, v0

    sget-object v20, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-ne v0, v1, :cond_0

    .line 58
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->checkConnection(Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;)Lcom/sec/android/app/myfiles/ftp/FTPErr;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 62
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    move-object/from16 v19, v0

    sget-object v20, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-ne v0, v1, :cond_7

    .line 64
    const/16 v19, 0x0

    aget-object v17, p1, v19

    .line 66
    .local v17, "param":Lcom/sec/android/app/myfiles/element/FileOperationParam;
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mParam:Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .line 68
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mDownloadTempDir:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mSrcFolder:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    move-object/from16 v3, v21

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->discoverFtpAndMakeLocalDirectories(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;

    move-result-object v18

    .line 71
    .local v18, "res":Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    move-object/from16 v19, v0

    sget-object v20, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-ne v0, v1, :cond_7

    .line 73
    const/4 v7, 0x0

    .line 75
    .local v7, "exist":Z
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mFiles:Ljava/util/Stack;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mFiles:Ljava/util/Stack;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/Stack;->size()I

    move-result v20

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/util/Stack;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Ljava/lang/String;

    .line 77
    .local v10, "files":[Ljava/lang/String;
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mFolders:Ljava/util/Stack;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mFolders:Ljava/util/Stack;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/Stack;->size()I

    move-result v20

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/util/Stack;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [Ljava/lang/String;

    .line 79
    .local v13, "folders":[Ljava/lang/String;
    array-length v11, v10

    .line 81
    .local v11, "filesToDownload":I
    array-length v14, v13

    .line 83
    .local v14, "foldersToCreate":I
    const/16 v19, 0x2

    const-string v20, "CopyFtpToSelfOperation"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "filesToDownload = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " foldersToCreate= "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 85
    move-object v4, v10

    .local v4, "arr$":[Ljava/lang/String;
    array-length v0, v4

    move/from16 v16, v0

    .local v16, "len$":I
    const/4 v15, 0x0

    .local v15, "i$":I
    :goto_0
    move/from16 v0, v16

    if-ge v15, v0, :cond_2

    aget-object v9, v4, v15

    .line 86
    .local v9, "filepath":Ljava/lang/String;
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 87
    .local v8, "existFile":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v19

    if-eqz v19, :cond_1

    .line 88
    const/4 v7, 0x1

    .line 85
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    .line 90
    :cond_1
    const/4 v7, 0x0

    .line 96
    .end local v8    # "existFile":Ljava/io/File;
    .end local v9    # "filepath":Ljava/lang/String;
    :cond_2
    if-nez v7, :cond_3

    .line 98
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mProcessingHandler:Landroid/os/Handler;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 100
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mJobFile:Ljava/util/ArrayDeque;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mDownloadTempDir:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mSrcFolder:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    move-object/from16 v3, v21

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->downloadItems(Ljava/util/ArrayDeque;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    .line 103
    .local v6, "downloadedItems":I
    const/16 v19, 0x2

    const-string v20, "CopyFtpToSelfOperation"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "downloadedItems = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 112
    .end local v6    # "downloadedItems":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    move-object/from16 v19, v0

    sget-object v20, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-ne v0, v1, :cond_7

    .line 114
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mDownloadTempDir:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    move-object/from16 v3, v21

    invoke-virtual {v0, v1, v13, v2, v3}, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->createDirectoriesOnFtp(Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 117
    .local v5, "dirCreated":I
    const/16 v19, 0x2

    const-string v20, "CopyFtpToSelfOperation"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "dirCreated = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 126
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    move-object/from16 v19, v0

    sget-object v20, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-ne v0, v1, :cond_7

    .line 128
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mProcessingHandler:Landroid/os/Handler;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 130
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    move-object/from16 v19, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mDownloadTempDir:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    move-object/from16 v3, v21

    invoke-virtual {v0, v1, v10, v2, v3}, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->uploadFiles(Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v12

    .line 132
    .local v12, "filesUploaded":I
    const/16 v19, 0x2

    const-string v20, "CopyFtpToSelfOperation"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "filesUploaded = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 135
    invoke-static {v13, v10}, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->deleteItemsOnLocal([Ljava/lang/String;[Ljava/lang/String;)I

    .line 137
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mFolders:Ljava/util/Stack;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/Stack;->size()I

    move-result v19

    move/from16 v0, v19

    if-ne v5, v0, :cond_4

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mFiles:Ljava/util/Stack;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/Stack;->size()I

    move-result v19

    move/from16 v0, v19

    if-eq v12, v0, :cond_5

    .line 139
    :cond_4
    sget-object v19, Lcom/sec/android/app/myfiles/ftp/FTPErr;->PARTIALLY_DONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 143
    :cond_5
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mFiles:Ljava/util/Stack;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/Stack;->size()I

    move-result v19

    if-lez v19, :cond_6

    if-nez v12, :cond_6

    .line 145
    sget-object v19, Lcom/sec/android/app/myfiles/ftp/FTPErr;->CONNECT_FAILED:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 148
    :cond_6
    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mDoneCount:I

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mDoneCount:I

    .line 158
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v5    # "dirCreated":I
    .end local v7    # "exist":Z
    .end local v10    # "files":[Ljava/lang/String;
    .end local v11    # "filesToDownload":I
    .end local v12    # "filesUploaded":I
    .end local v13    # "folders":[Ljava/lang/String;
    .end local v14    # "foldersToCreate":I
    .end local v15    # "i$":I
    .end local v16    # "len$":I
    .end local v17    # "param":Lcom/sec/android/app/myfiles/element/FileOperationParam;
    .end local v18    # "res":Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;
    :cond_7
    const/16 v19, 0x0

    return-object v19
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 33
    check-cast p1, [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V
    .locals 6
    .param p1, "result"    # Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .prologue
    const/16 v5, 0xa

    .line 165
    const/4 v1, 0x2

    const-string v2, "CopyFtpToSelfOperation"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " error =  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/ftp/FTPErr;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 166
    sget-object v1, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation$1;->$SwitchMap$com$sec$android$app$myfiles$ftp$FTPErr:[I

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPErr;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 197
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    if-eqz v1, :cond_0

    .line 199
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mDoneCount:I

    invoke-interface {v1, v2, v3}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;->onCancelled(Ljava/util/ArrayList;I)V

    .line 203
    :cond_0
    const v1, 0x7f0b00c9

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->showToast(I)V

    .line 209
    :goto_0
    return-void

    .line 171
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    if-eqz v1, :cond_2

    .line 173
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 175
    .local v0, "arg":Landroid/os/Bundle;
    const-string v1, "FILE_OPERATION"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 177
    const-string v1, "src_fragment_id"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 179
    const-string v1, "dest_fragment_id"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 181
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mParam:Lcom/sec/android/app/myfiles/element/FileOperationParam;

    if-eqz v1, :cond_1

    .line 183
    const-string v1, "target_folder"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mParam:Lcom/sec/android/app/myfiles/element/FileOperationParam;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    iget v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mDoneCount:I

    iget v3, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mDoneCount:I

    invoke-interface {v1, v2, v3, v0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;->onCompleted(IILandroid/os/Bundle;)V

    .line 191
    .end local v0    # "arg":Landroid/os/Bundle;
    :cond_2
    const v1, 0x7f0c0005

    iget v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mDoneCount:I

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->showToastPrurals(II)V

    goto :goto_0

    .line 166
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 33
    check-cast p1, Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V

    return-void
.end method

.method protected setupProgressView()V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 49
    return-void
.end method
