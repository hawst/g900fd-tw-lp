.class public Lcom/sec/android/app/myfiles/view/ActionBarManager;
.super Ljava/lang/Object;
.source "ActionBarManager.java"


# instance fields
.field private mActionBar:Landroid/app/ActionBar;

.field private mCurrentActionBarId:I

.field private mCurrentActionBarView:Landroid/view/View;

.field private mOwnerActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/myfiles/AbsMainActivity;)V
    .locals 1
    .param p1, "activity"    # Lcom/sec/android/app/myfiles/AbsMainActivity;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mOwnerActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mOwnerActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mCurrentActionBarId:I

    .line 65
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/view/ActionBarManager;)Lcom/sec/android/app/myfiles/AbsMainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/view/ActionBarManager;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mOwnerActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    return-object v0
.end method


# virtual methods
.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public initialize()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 75
    return-void
.end method

.method public declared-synchronized setActionBar(I)V
    .locals 2
    .param p1, "actionBarId"    # I

    .prologue
    .line 88
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 92
    iget v0, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mCurrentActionBarId:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x7

    if-eq p1, v0, :cond_0

    .line 121
    :goto_0
    :pswitch_0
    monitor-exit p0

    return-void

    .line 97
    :cond_0
    :try_start_1
    iput p1, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mCurrentActionBarId:I

    .line 99
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 118
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 99
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public declared-synchronized setActionBar(ILandroid/view/View;)V
    .locals 2
    .param p1, "actionBarId"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 126
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mCurrentActionBarId:I

    .line 128
    const/4 v0, 0x7

    if-ne p1, v0, :cond_1

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 138
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, p2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 142
    iput-object p2, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mCurrentActionBarView:Landroid/view/View;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143
    monitor-exit p0

    return-void

    .line 132
    :cond_1
    const/16 v0, 0x9

    if-ne p1, v0, :cond_0

    .line 134
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setActionBar(ILandroid/view/View;Landroid/app/ActionBar$LayoutParams;)V
    .locals 2
    .param p1, "actionBarId"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "lp"    # Landroid/app/ActionBar$LayoutParams;

    .prologue
    .line 148
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mCurrentActionBarId:I

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mCurrentActionBarView:Landroid/view/View;

    invoke-virtual {v0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 153
    iput p1, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mCurrentActionBarId:I

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, p2, p3}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 157
    iput-object p2, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mCurrentActionBarView:Landroid/view/View;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 159
    :cond_1
    monitor-exit p0

    return-void

    .line 148
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setFragmentActionbar(ILandroid/content/Context;)V
    .locals 8
    .param p1, "FragmentId"    # I
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 227
    iget-object v3, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v3, v5}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 231
    sparse-switch p1, :sswitch_data_0

    .line 312
    :goto_0
    return-void

    .line 233
    :sswitch_0
    const/4 v3, -0x1

    iput v3, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mCurrentActionBarId:I

    .line 236
    iget-object v3, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mOwnerActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f040001

    invoke-virtual {v3, v4, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 238
    .local v2, "mSelectActionBarView":Landroid/view/View;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v3, v6}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 239
    iget-object v3, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v3, v5}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 240
    iget-object v3, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v3, v5}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 241
    iget-object v3, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v3, v5}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 242
    iget-object v3, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v3, v5}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 244
    iget-object v3, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v3, v2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    goto :goto_0

    .line 266
    .end local v2    # "mSelectActionBarView":Landroid/view/View;
    :sswitch_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v3, v5}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 267
    iget-object v3, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v3, v5}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 268
    iget-object v3, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v3, v5}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 269
    iget-object v3, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v3, v6}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 271
    iget-object v3, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mOwnerActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f040004

    invoke-virtual {v3, v4, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 273
    .local v0, "actionbar":Landroid/view/View;
    const v3, 0x7f0f000b

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 275
    .local v1, "actionbarBtn":Landroid/widget/LinearLayout;
    new-instance v3, Lcom/sec/android/app/myfiles/view/ActionBarManager$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/myfiles/view/ActionBarManager$1;-><init>(Lcom/sec/android/app/myfiles/view/ActionBarManager;)V

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 284
    iget-object v3, p0, Lcom/sec/android/app/myfiles/view/ActionBarManager;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v3, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    goto :goto_0

    .line 231
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_1
        0x4 -> :sswitch_1
        0x5 -> :sswitch_1
        0x6 -> :sswitch_1
        0x7 -> :sswitch_1
        0x8 -> :sswitch_1
        0x9 -> :sswitch_1
        0xa -> :sswitch_1
        0xd -> :sswitch_1
        0xe -> :sswitch_1
        0xf -> :sswitch_1
        0x11 -> :sswitch_1
        0x12 -> :sswitch_1
        0x13 -> :sswitch_1
        0x14 -> :sswitch_1
        0x1f -> :sswitch_1
        0x28 -> :sswitch_1
        0x201 -> :sswitch_1
    .end sparse-switch
.end method
