.class Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$4;
.super Ljava/lang/Object;
.source "AbsFileOperationTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->showStopDialog(Ljava/io/File;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

.field final synthetic val$dstFile:Ljava/io/File;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 1151
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$4;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iput-object p2, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$4;->val$dstFile:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 1156
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$4;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 1157
    .local v3, "res":Landroid/content/res/Resources;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$4;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mContext:Landroid/content/Context;

    invoke-direct {v0, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1158
    .local v0, "b":Landroid/app/AlertDialog$Builder;
    const-string v2, ""

    .line 1159
    .local v2, "message":Ljava/lang/String;
    const-string v4, ""

    .line 1160
    .local v4, "title":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$4;->val$dstFile:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 1162
    .local v1, "fileName":Ljava/lang/String;
    const v5, 0x7f0b0176

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v8, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1164
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$4;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget v5, v5, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mOperationType:I

    if-nez v5, :cond_1

    .line 1166
    const v5, 0x7f0b0022

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1176
    :cond_0
    :goto_0
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1178
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1180
    const v5, 0x7f0b0017

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$4$1;

    invoke-direct {v6, p0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$4$1;-><init>(Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$4;)V

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1208
    const v5, 0x7f0b0178

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$4$2;

    invoke-direct {v6, p0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$4$2;-><init>(Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$4;)V

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1236
    new-instance v5, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$4$3;

    invoke-direct {v5, p0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$4$3;-><init>(Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$4;)V

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 1266
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$4;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, v5, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mStopDialog:Landroid/app/AlertDialog;

    .line 1267
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$4;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mStopDialog:Landroid/app/AlertDialog;

    invoke-virtual {v5}, Landroid/app/AlertDialog;->show()V

    .line 1268
    return-void

    .line 1168
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$4;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget v5, v5, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mOperationType:I

    if-ne v5, v8, :cond_0

    .line 1170
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$4;->val$dstFile:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxFolder(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1171
    const v5, 0x7f0b0163

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 1173
    :cond_2
    const v5, 0x7f0b0021

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method
