.class public final Lcom/sec/android/app/myfiles/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final action_bar_button_left_margin:I = 0x7f090000

.field public static final action_bar_button_width:I = 0x7f090001

.field public static final action_bar_home_as_up_margin_end:I = 0x7f090004

.field public static final action_bar_home_as_up_margin_start:I = 0x7f090003

.field public static final action_bar_searchview_right_margin:I = 0x7f090002

.field public static final action_bar_selected_spinner_height:I = 0x7f0901ce

.field public static final action_bar_selected_spinner_left_margin:I = 0x7f0901cf

.field public static final action_bar_selected_spinner_left_padding:I = 0x7f0901d0

.field public static final action_bar_selected_spinner_right_padding:I = 0x7f0901d1

.field public static final action_bar_selected_spinner_text_size:I = 0x7f0901d2

.field public static final action_bar_selected_spinner_width:I = 0x7f0901cd

.field public static final action_bar_title_text_margin_end:I = 0x7f090005

.field public static final actionmode_search_header:I = 0x7f090160

.field public static final actionmode_search_layout_padding_left:I = 0x7f09015f

.field public static final add_ftp_default_left_padding:I = 0x7f0901b5

.field public static final add_ftp_default_list_devider_line:I = 0x7f0901b9

.field public static final add_ftp_default_list_row_height:I = 0x7f0901b7

.field public static final add_ftp_default_right_padding:I = 0x7f0901b6

.field public static final add_ftp_list_edittext_height:I = 0x7f0901bd

.field public static final add_ftp_list_left_padding:I = 0x7f0901ba

.field public static final add_ftp_list_right_padding:I = 0x7f0901bb

.field public static final add_ftp_list_textview_bottom_margin:I = 0x7f0901bc

.field public static final add_ftp_multiple_list_row_height:I = 0x7f0901b8

.field public static final add_ftp_textview_oneline:I = 0x7f0901b3

.field public static final add_ftp_textview_twolines:I = 0x7f0901b4

.field public static final advance_search_bottom_margin:I = 0x7f090186

.field public static final advance_search_date_delete_right_margin:I = 0x7f09018b

.field public static final advance_search_left_margin:I = 0x7f090187

.field public static final advance_search_right_margin:I = 0x7f090188

.field public static final advance_search_right_padding:I = 0x7f09018a

.field public static final advance_search_textview_width:I = 0x7f090184

.field public static final advance_search_top_margin:I = 0x7f090185

.field public static final advanced_search_item_height:I = 0x7f090182

.field public static final advanced_search_item_height2:I = 0x7f090183

.field public static final category_item_container_margin_left:I = 0x7f090111

.field public static final category_item_container_margin_right:I = 0x7f090112

.field public static final category_list_header_height:I = 0x7f090031

.field public static final clear_icon_margin_bottom:I = 0x7f09018c

.field public static final confirm_oma_content_padding_bottom:I = 0x7f0901e6

.field public static final confirm_oma_content_padding_left:I = 0x7f0901e7

.field public static final confirm_oma_content_padding_right:I = 0x7f0901e8

.field public static final confirm_oma_content_padding_top:I = 0x7f0901e5

.field public static final confirm_oma_field_text_size:I = 0x7f0901eb

.field public static final confirm_oma_label_space:I = 0x7f0901e9

.field public static final confirm_oma_label_text_size:I = 0x7f0901ea

.field public static final detail_view_first_text_size:I = 0x7f0901ed

.field public static final detail_view_row_height:I = 0x7f0901ec

.field public static final detail_view_second_text_size:I = 0x7f0901ee

.field public static final details_list_item_min_height:I = 0x7f090014

.field public static final dialog_list_checkbox_right_padding:I = 0x7f09017b

.field public static final dialog_list_item_height:I = 0x7f090189

.field public static final dialog_list_item_size:I = 0x7f090179

.field public static final dialog_list_padding:I = 0x7f09016f

.field public static final dialog_list_padding_bottom:I = 0x7f090171

.field public static final dialog_list_padding_right:I = 0x7f09017a

.field public static final dialog_list_padding_top:I = 0x7f090170

.field public static final dialog_percent_text_size:I = 0x7f090173

.field public static final dialog_processing_text_size:I = 0x7f090172

.field public static final dialog_progressbar_height:I = 0x7f090176

.field public static final dialog_progressbar_marginleft:I = 0x7f090177

.field public static final dialog_progressbar_marginright:I = 0x7f090178

.field public static final dialog_progressbar_padding_bottom:I = 0x7f090174

.field public static final dialog_progressbar_padding_top:I = 0x7f090175

.field public static final downloaded_app_list_icon_height:I = 0x7f090018

.field public static final downloaded_app_list_icon_width:I = 0x7f090017

.field public static final drag_shadow_height:I = 0x7f090197

.field public static final drag_shadow_icon_height:I = 0x7f090199

.field public static final drag_shadow_icon_margin_bottom:I = 0x7f09019b

.field public static final drag_shadow_icon_margin_left:I = 0x7f09019c

.field public static final drag_shadow_icon_margin_right:I = 0x7f09019d

.field public static final drag_shadow_icon_margin_top:I = 0x7f09019a

.field public static final drag_shadow_icon_width:I = 0x7f090198

.field public static final drag_shadow_items_count_2digit_right_shift:I = 0x7f0901b1

.field public static final drag_shadow_items_count_3digit_right_shift:I = 0x7f0901b2

.field public static final drag_shadow_items_count_layout_padding_left:I = 0x7f0901af

.field public static final drag_shadow_items_count_layout_padding_right:I = 0x7f0901b0

.field public static final drag_shadow_itmes_count_text_margin:I = 0x7f0901ae

.field public static final drag_shadow_text_margin_right:I = 0x7f09019f

.field public static final drag_shadow_text_size:I = 0x7f09019e

.field public static final drag_shadow_width:I = 0x7f090196

.field public static final drag_shadow_with_thumbs_drag_file_height:I = 0x7f0901a3

.field public static final drag_shadow_with_thumbs_drag_file_width:I = 0x7f0901a2

.field public static final drag_shadow_with_thumbs_height:I = 0x7f0901a1

.field public static final drag_shadow_with_thumbs_icon_height:I = 0x7f0901a5

.field public static final drag_shadow_with_thumbs_icon_margin:I = 0x7f0901a6

.field public static final drag_shadow_with_thumbs_icon_width:I = 0x7f0901a4

.field public static final drag_shadow_with_thumbs_items_count_height:I = 0x7f0901ad

.field public static final drag_shadow_with_thumbs_items_count_width:I = 0x7f0901ac

.field public static final drag_shadow_with_thumbs_size_text_margin_right:I = 0x7f0901aa

.field public static final drag_shadow_with_thumbs_size_text_size:I = 0x7f0901ab

.field public static final drag_shadow_with_thumbs_text_margin_left:I = 0x7f0901a7

.field public static final drag_shadow_with_thumbs_text_margin_right:I = 0x7f0901a8

.field public static final drag_shadow_with_thumbs_text_size:I = 0x7f0901a9

.field public static final drag_shadow_with_thumbs_width:I = 0x7f0901a0

.field public static final empty_view_icon_height:I = 0x7f0901bf

.field public static final empty_view_icon_width:I = 0x7f0901be

.field public static final empty_view_title_text_margin_top:I = 0x7f0901c1

.field public static final empty_view_title_text_size:I = 0x7f0901c0

.field public static final expandable_list_collapsed_height:I = 0x7f090141

.field public static final expandable_list_fold_indicator_height:I = 0x7f090146

.field public static final expandable_list_fold_indicator_margin_right:I = 0x7f090147

.field public static final expandable_list_fold_indicator_width:I = 0x7f090145

.field public static final expandable_list_land_height:I = 0x7f09013f

.field public static final expandable_list_port_height:I = 0x7f090140

.field public static final expandable_list_sub_titlebar_height:I = 0x7f090142

.field public static final expandable_list_sub_titlebar_text_margin_left:I = 0x7f090143

.field public static final expandable_list_sub_titlebar_text_size:I = 0x7f090144

.field public static final hover_folder_icon_top_margin:I = 0x7f09000f

.field public static final hover_image_btn_layout_height:I = 0x7f09000a

.field public static final hover_image_btn_layout_margin:I = 0x7f09000b

.field public static final hover_image_btn_size:I = 0x7f09000e

.field public static final hover_no_item_height:I = 0x7f090009

.field public static final hover_no_item_width:I = 0x7f090008

.field public static final hover_popup_layout_padding:I = 0x7f09000c

.field public static final hover_popup_layout_padding_bottom:I = 0x7f09000d

.field public static final input_button_layout_height:I = 0x7f090063

.field public static final input_button_margin_bottom:I = 0x7f090064

.field public static final input_button_textsize:I = 0x7f090065

.field public static final input_checkbox_padding_left:I = 0x7f090067

.field public static final input_edittext_layout_height:I = 0x7f09005e

.field public static final input_edittext_margin_bottom:I = 0x7f090062

.field public static final input_edittext_margin_left:I = 0x7f09005f

.field public static final input_edittext_margin_right:I = 0x7f090061

.field public static final input_edittext_margin_top:I = 0x7f090060

.field public static final input_textview_text_size:I = 0x7f090066

.field public static final knox_dialog_height:I = 0x7f090181

.field public static final knox_dialog_icon_height:I = 0x7f09017e

.field public static final knox_dialog_icon_margin_bottom:I = 0x7f09017f

.field public static final knox_dialog_icon_width:I = 0x7f09017d

.field public static final knox_dialog_item_container_padding_top:I = 0x7f09017c

.field public static final knox_dialog_text_height:I = 0x7f090180

.field public static final left_panel_width:I = 0x7f090148

.field public static final list_checkbox_container_width:I = 0x7f09001d

.field public static final list_checkbox_right_margin:I = 0x7f09001f

.field public static final list_checkbox_width:I = 0x7f09001e

.field public static final list_divider_margin_top:I = 0x7f090030

.field public static final list_file_date_text_size:I = 0x7f090026

.field public static final list_file_name_right_margin:I = 0x7f090023

.field public static final list_file_name_text_size:I = 0x7f090024

.field public static final list_file_size_text_size:I = 0x7f090025

.field public static final list_icon_height:I = 0x7f090016

.field public static final list_icon_margin_left:I = 0x7f090019

.field public static final list_icon_margin_right:I = 0x7f09001a

.field public static final list_icon_overlay_height:I = 0x7f09001c

.field public static final list_icon_overlay_width:I = 0x7f09001b

.field public static final list_icon_width:I = 0x7f090015

.field public static final list_item_height:I = 0x7f090012

.field public static final list_item_min_height:I = 0x7f090013

.field public static final list_personal_contents_icon_height:I = 0x7f090029

.field public static final list_personal_contents_icon_margin_right:I = 0x7f09002a

.field public static final list_personal_contents_icon_width:I = 0x7f090028

.field public static final list_radiobutton_left_margin:I = 0x7f090021

.field public static final list_radiobutton_right_margin:I = 0x7f090022

.field public static final list_radiobutton_width:I = 0x7f090020

.field public static final list_text_row_space:I = 0x7f09002b

.field public static final list_title_bar_height:I = 0x7f090027

.field public static final list_view_item_drm_lock_height:I = 0x7f09002d

.field public static final list_view_item_drm_lock_marginRight:I = 0x7f09002f

.field public static final list_view_item_drm_lock_marginTop:I = 0x7f09002e

.field public static final list_view_item_drm_lock_width:I = 0x7f09002c

.field public static final main_category_cell_height:I = 0x7f09007f

.field public static final main_category_cell_height_multiwindow:I = 0x7f0900ac

.field public static final main_category_cell_icon_height:I = 0x7f09008c

.field public static final main_category_cell_icon_height_multiwindow:I = 0x7f0900b2

.field public static final main_category_cell_icon_margin_bottom:I = 0x7f09008d

.field public static final main_category_cell_icon_margin_right:I = 0x7f09008e

.field public static final main_category_cell_icon_margin_right_multiwindow:I = 0x7f0900b4

.field public static final main_category_cell_icon_margin_top_multiwindow:I = 0x7f0900b3

.field public static final main_category_cell_icon_width:I = 0x7f09008b

.field public static final main_category_cell_icon_width_multiwindow:I = 0x7f0900b1

.field public static final main_category_cell_size_height:I = 0x7f090085

.field public static final main_category_cell_size_margin_left:I = 0x7f090086

.field public static final main_category_cell_size_margin_right:I = 0x7f090087

.field public static final main_category_cell_size_margin_top:I = 0x7f090088

.field public static final main_category_cell_size_text_size:I = 0x7f090089

.field public static final main_category_cell_size_text_size_easymode:I = 0x7f09008a

.field public static final main_category_cell_size_text_size_multiwindow:I = 0x7f0900b0

.field public static final main_category_cell_text_container_padding_left_multiwindow:I = 0x7f0900ad

.field public static final main_category_cell_text_container_padding_top_multiwindow:I = 0x7f0900ae

.field public static final main_category_cell_title_margin_left:I = 0x7f090080

.field public static final main_category_cell_title_margin_top:I = 0x7f090081

.field public static final main_category_cell_title_max_width:I = 0x7f090082

.field public static final main_category_cell_title_text_size:I = 0x7f090083

.field public static final main_category_cell_title_text_size_easymode:I = 0x7f090084

.field public static final main_category_cell_title_text_size_multiwindow:I = 0x7f0900af

.field public static final main_category_cell_width:I = 0x7f09007e

.field public static final main_category_cell_width_multiwindow:I = 0x7f0900ab

.field public static final main_category_container_height:I = 0x7f09006a

.field public static final main_category_container_height_easyux:I = 0x7f09006b

.field public static final main_category_container_height_multiwindow:I = 0x7f09009a

.field public static final main_category_container_margin_bottom:I = 0x7f09006d

.field public static final main_category_container_margin_bottom_multiwindow:I = 0x7f09009c

.field public static final main_category_container_margin_left:I = 0x7f09006e

.field public static final main_category_container_margin_left_multiwindow:I = 0x7f09009d

.field public static final main_category_container_margin_right:I = 0x7f09006f

.field public static final main_category_container_margin_right_multiwindow:I = 0x7f09009e

.field public static final main_category_container_margin_top:I = 0x7f09006c

.field public static final main_category_container_margin_top_multiwindow:I = 0x7f09009b

.field public static final main_category_container_padding_bottom:I = 0x7f090071

.field public static final main_category_container_padding_bottom_multiwindow:I = 0x7f0900a0

.field public static final main_category_container_padding_left:I = 0x7f090072

.field public static final main_category_container_padding_left_multiwindow:I = 0x7f0900a1

.field public static final main_category_container_padding_right:I = 0x7f090073

.field public static final main_category_container_padding_right_multiwindow:I = 0x7f0900a2

.field public static final main_category_container_padding_top:I = 0x7f090070

.field public static final main_category_container_padding_top_multiwindow:I = 0x7f09009f

.field public static final main_category_container_width:I = 0x7f090069

.field public static final main_category_container_width_multiwindow:I = 0x7f090099

.field public static final main_category_first_header_left_margin:I = 0x7f090093

.field public static final main_category_first_header_top_margin:I = 0x7f090092

.field public static final main_category_grid_height:I = 0x7f090075

.field public static final main_category_grid_height_multiwindow:I = 0x7f0900a4

.field public static final main_category_grid_horizontal_divider_first_margin_top_multiwindow:I = 0x7f0900a9

.field public static final main_category_grid_horizontal_divider_margin_top_multiwindow:I = 0x7f0900aa

.field public static final main_category_grid_horizontal_divider_thick:I = 0x7f09007d

.field public static final main_category_grid_horizontal_divider_thick_multiwindow:I = 0x7f0900a8

.field public static final main_category_grid_horizontal_spacing:I = 0x7f09007b

.field public static final main_category_grid_padding_bottom:I = 0x7f090079

.field public static final main_category_grid_padding_left:I = 0x7f090076

.field public static final main_category_grid_padding_right:I = 0x7f090077

.field public static final main_category_grid_padding_top:I = 0x7f090078

.field public static final main_category_grid_side_padding:I = 0x7f090094

.field public static final main_category_grid_vertical_divider_first_margin_left_multiwindow:I = 0x7f0900a6

.field public static final main_category_grid_vertical_divider_margin_left_multiwindow:I = 0x7f0900a7

.field public static final main_category_grid_vertical_divider_thick:I = 0x7f09007c

.field public static final main_category_grid_vertical_divider_thick_multiwindow:I = 0x7f0900a5

.field public static final main_category_grid_vertical_spacing:I = 0x7f09007a

.field public static final main_category_grid_width:I = 0x7f090074

.field public static final main_category_grid_width_multiwindow:I = 0x7f0900a3

.field public static final main_category_header_top_margin:I = 0x7f090090

.field public static final main_category_list_divider_top_margin:I = 0x7f090091

.field public static final main_category_list_header_text_size:I = 0x7f090096

.field public static final main_category_list_item_text_size:I = 0x7f090095

.field public static final main_category_panel_margin_bottom:I = 0x7f090068

.field public static final main_category_panel_margin_bottom_multiwindow:I = 0x7f090098

.field public static final main_category_side_padding:I = 0x7f09008f

.field public static final main_category_sub_list_item_size:I = 0x7f090097

.field public static final main_cloud_list_item_height:I = 0x7f090010

.field public static final main_list_header_height:I = 0x7f0901ef

.field public static final main_list_icon_height:I = 0x7f0901c8

.field public static final main_list_icon_left_margin:I = 0x7f0901c9

.field public static final main_list_icon_right_margin:I = 0x7f0901ca

.field public static final main_list_icon_width:I = 0x7f0901c7

.field public static final main_list_item_height:I = 0x7f090011

.field public static final main_list_name_text_size:I = 0x7f0901cb

.field public static final multiwindow_app_default_height:I = 0x7f090193

.field public static final multiwindow_app_default_width:I = 0x7f090192

.field public static final multiwindow_app_minimum_height:I = 0x7f090195

.field public static final multiwindow_app_minimum_width:I = 0x7f090194

.field public static final path_indicator_arrow_width:I = 0x7f09013e

.field public static final path_indicator_button_margin_left:I = 0x7f090138

.field public static final path_indicator_button_padding_left:I = 0x7f09013b

.field public static final path_indicator_button_padding_right:I = 0x7f09013c

.field public static final path_indicator_button_text_size:I = 0x7f090139

.field public static final path_indicator_first_button_padding_left:I = 0x7f09013a

.field public static final path_indicator_height:I = 0x7f090136

.field public static final path_indicator_margin:I = 0x7f09013d

.field public static final path_indicator_top_line_thick:I = 0x7f090137

.field public static final right_panel_width:I = 0x7f090149

.field public static final search_dropdown_button_height:I = 0x7f090153

.field public static final search_dropdown_button_item_height:I = 0x7f090154

.field public static final search_dropdown_button_item_minwidth:I = 0x7f090156

.field public static final search_dropdown_button_padding_left:I = 0x7f090157

.field public static final search_dropdown_button_text_size:I = 0x7f090158

.field public static final search_dropdown_button_width:I = 0x7f090155

.field public static final search_dropdown_left_padding:I = 0x7f09015d

.field public static final search_file_type_list_row_checkbox_marginright:I = 0x7f09015b

.field public static final search_for_button_height:I = 0x7f090164

.field public static final search_for_button_lauoyt_bottom_margin:I = 0x7f090167

.field public static final search_for_button_margin:I = 0x7f090166

.field public static final search_for_button_padding:I = 0x7f090165

.field public static final search_for_text_bottom_margin:I = 0x7f090163

.field public static final search_for_text_left_margin:I = 0x7f090168

.field public static final search_for_text_size:I = 0x7f090161

.field public static final search_for_text_top_margin:I = 0x7f090162

.field public static final search_framelayout_margin_top:I = 0x7f09015c

.field public static final search_header_layout_height:I = 0x7f090150

.field public static final search_header_layout_height_padding_left:I = 0x7f090151

.field public static final search_header_layout_height_padding_right:I = 0x7f090152

.field public static final search_list_folder_left_margin:I = 0x7f09016d

.field public static final search_list_folder_right_margin:I = 0x7f09016e

.field public static final search_list_icon_left_margin:I = 0x7f09016b

.field public static final search_list_icon_right_margin:I = 0x7f09016c

.field public static final search_list_thumbnail_height:I = 0x7f09016a

.field public static final search_list_thumbnail_width:I = 0x7f090169

.field public static final search_result_header:I = 0x7f09015e

.field public static final search_search_view_height:I = 0x7f090159

.field public static final search_search_view_width:I = 0x7f09015a

.field public static final select_all_dropdown_padding_bottom:I = 0x7f0901d9

.field public static final select_all_dropdown_padding_left:I = 0x7f0901d8

.field public static final select_all_dropdown_padding_right:I = 0x7f0901d7

.field public static final select_all_popup_dropdown_text_padding_left:I = 0x7f0901dd

.field public static final select_all_popup_dropdown_text_padding_right:I = 0x7f0901de

.field public static final select_custom_layout_padding_right:I = 0x7f0901d6

.field public static final select_divider_height:I = 0x7f0901e4

.field public static final select_divider_margin_top:I = 0x7f0901e2

.field public static final select_divider_text_size:I = 0x7f0901e1

.field public static final select_divider_width:I = 0x7f0901e3

.field public static final select_dropdown_button_height:I = 0x7f0901d3

.field public static final select_dropdown_button_min_width:I = 0x7f0901e0

.field public static final select_dropdown_button_text_size:I = 0x7f0901d4

.field public static final select_dropdown_left_padding_selector_activity:I = 0x7f0901df

.field public static final select_dropdown_padding_left:I = 0x7f0901d5

.field public static final select_dropdown_position_x:I = 0x7f0901da

.field public static final select_dropdown_position_y:I = 0x7f0901db

.field public static final select_dropdown_text_padding_left:I = 0x7f0901dc

.field public static final select_dropdown_width:I = 0x7f0901cc

.field public static final setdate_margin_bottom:I = 0x7f09018e

.field public static final setdate_margin_left:I = 0x7f09018f

.field public static final setdate_margin_top:I = 0x7f09018d

.field public static final setdate_text_margin_top:I = 0x7f090191

.field public static final setdate_text_size:I = 0x7f090190

.field public static final shortcut_cell_add_icon_height:I = 0x7f0900c7

.field public static final shortcut_cell_add_icon_height_multiwindow:I = 0x7f0900e4

.field public static final shortcut_cell_add_icon_width:I = 0x7f0900c6

.field public static final shortcut_cell_add_icon_width_multiwindow:I = 0x7f0900e3

.field public static final shortcut_cell_checkbox_height:I = 0x7f0900c9

.field public static final shortcut_cell_checkbox_height_multiwindow:I = 0x7f0900e6

.field public static final shortcut_cell_checkbox_margin_right:I = 0x7f0900cb

.field public static final shortcut_cell_checkbox_margin_right_multiwindow:I = 0x7f0900e8

.field public static final shortcut_cell_checkbox_margin_top:I = 0x7f0900ca

.field public static final shortcut_cell_checkbox_margin_top_multiwindow:I = 0x7f0900e7

.field public static final shortcut_cell_checkbox_scale:I = 0x7f0900cf

.field public static final shortcut_cell_checkbox_width:I = 0x7f0900c8

.field public static final shortcut_cell_checkbox_width_multiwindow:I = 0x7f0900e5

.field public static final shortcut_cell_height:I = 0x7f0900bc

.field public static final shortcut_cell_height_multiwindow:I = 0x7f0900db

.field public static final shortcut_cell_icon_height:I = 0x7f0900be

.field public static final shortcut_cell_icon_height_multiwindow:I = 0x7f0900dd

.field public static final shortcut_cell_icon_margin_bottom:I = 0x7f0900c0

.field public static final shortcut_cell_icon_margin_bottom_multiwindow:I = 0x7f0900df

.field public static final shortcut_cell_icon_margin_top:I = 0x7f0900bf

.field public static final shortcut_cell_icon_margin_top_multiwindow:I = 0x7f0900de

.field public static final shortcut_cell_icon_width:I = 0x7f0900bd

.field public static final shortcut_cell_icon_width_multiwindow:I = 0x7f0900dc

.field public static final shortcut_cell_title_height:I = 0x7f0900c1

.field public static final shortcut_cell_title_height_multiwindow:I = 0x7f0900e0

.field public static final shortcut_cell_title_margin_left:I = 0x7f0900c2

.field public static final shortcut_cell_title_margin_right:I = 0x7f0900c3

.field public static final shortcut_cell_title_padding_left:I = 0x7f0900c4

.field public static final shortcut_cell_title_padding_left_multiwindow:I = 0x7f0900e1

.field public static final shortcut_cell_title_padding_right:I = 0x7f0900c5

.field public static final shortcut_cell_title_padding_right_multiwindow:I = 0x7f0900e2

.field public static final shortcut_cell_width:I = 0x7f0900bb

.field public static final shortcut_cell_width_multiwindow:I = 0x7f0900da

.field public static final shortcut_list_icon_height:I = 0x7f0901c4

.field public static final shortcut_list_icon_margin_left:I = 0x7f0901c5

.field public static final shortcut_list_icon_margin_right:I = 0x7f0901c6

.field public static final shortcut_list_icon_width:I = 0x7f0901c3

.field public static final shortcut_list_item_height:I = 0x7f0901c2

.field public static final shortcut_page_arrow_margin_outside:I = 0x7f0900d0

.field public static final shortcut_panal_left_arrow_margin:I = 0x7f0900b9

.field public static final shortcut_panal_right_arrow_margin:I = 0x7f0900ba

.field public static final shortcut_panel_height:I = 0x7f0900b6

.field public static final shortcut_panel_height_multiwindow:I = 0x7f0900d2

.field public static final shortcut_panel_horizontal_divider_margin_top_multiwindow:I = 0x7f0900d9

.field public static final shortcut_panel_horizontal_divider_thick_multiwindow:I = 0x7f0900d8

.field public static final shortcut_panel_margin_bottom:I = 0x7f0900b8

.field public static final shortcut_panel_margin_bottom_multiwindow:I = 0x7f0900d4

.field public static final shortcut_panel_middile_space:I = 0x7f0900ce

.field public static final shortcut_panel_padding_left:I = 0x7f0900cc

.field public static final shortcut_panel_padding_right:I = 0x7f0900cd

.field public static final shortcut_panel_padding_top:I = 0x7f0900b7

.field public static final shortcut_panel_padding_top_multiwindow:I = 0x7f0900d3

.field public static final shortcut_panel_vertical_divider_first_margin_left_multiwindow:I = 0x7f0900d6

.field public static final shortcut_panel_vertical_divider_margin_left_multiwindow:I = 0x7f0900d7

.field public static final shortcut_panel_vertical_divider_thick_multiwindow:I = 0x7f0900d5

.field public static final shortcut_panel_width:I = 0x7f0900b5

.field public static final shortcut_panel_width_multiwindow:I = 0x7f0900d1

.field public static final split_bar_margin_left:I = 0x7f09014e

.field public static final split_bar_margin_right:I = 0x7f09014f

.field public static final split_bar_width:I = 0x7f09014d

.field public static final split_view_left_panel_default_width:I = 0x7f09014a

.field public static final split_view_left_panel_min_width:I = 0x7f09014b

.field public static final split_view_right_panel_min_width:I = 0x7f09014c

.field public static final storage_usage_bar_height:I = 0x7f0900ef

.field public static final storage_usage_bar_height_multiwindow:I = 0x7f090115

.field public static final storage_usage_bar_margin_bottom:I = 0x7f0900f1

.field public static final storage_usage_bar_margin_left:I = 0x7f0900f2

.field public static final storage_usage_bar_margin_left_multiwindow:I = 0x7f090117

.field public static final storage_usage_bar_margin_right:I = 0x7f0900f3

.field public static final storage_usage_bar_margin_right_multiwindow:I = 0x7f090118

.field public static final storage_usage_bar_margin_top:I = 0x7f0900f0

.field public static final storage_usage_bar_margin_top_multiwindow:I = 0x7f090116

.field public static final storage_usage_bar_min_tick_width:I = 0x7f0900f4

.field public static final storage_usage_bar_min_tick_width_multiwindow:I = 0x7f090119

.field public static final storage_usage_bar_padding:I = 0x7f0900f5

.field public static final storage_usage_category_icon_height:I = 0x7f090109

.field public static final storage_usage_category_icon_height_multiwindow:I = 0x7f090126

.field public static final storage_usage_category_icon_width:I = 0x7f090108

.field public static final storage_usage_category_icon_width_multiwindow:I = 0x7f090125

.field public static final storage_usage_category_margin_left:I = 0x7f090107

.field public static final storage_usage_category_margin_left_multiwindow:I = 0x7f090124

.field public static final storage_usage_category_margin_top:I = 0x7f090100

.field public static final storage_usage_category_text_margin_left:I = 0x7f09010a

.field public static final storage_usage_category_text_margin_left_multiwindow:I = 0x7f090127

.field public static final storage_usage_category_text_margin_right:I = 0x7f09010b

.field public static final storage_usage_category_text_margin_right_multiwindow:I = 0x7f090128

.field public static final storage_usage_category_text_size:I = 0x7f09010c

.field public static final storage_usage_dialog_height:I = 0x7f0900ea

.field public static final storage_usage_dialog_width:I = 0x7f0900e9

.field public static final storage_usage_height:I = 0x7f0900ee

.field public static final storage_usage_height_multiwindow:I = 0x7f090114

.field public static final storage_usage_information_height:I = 0x7f0900f6

.field public static final storage_usage_information_height_multiwindow:I = 0x7f09011a

.field public static final storage_usage_information_margin_top:I = 0x7f0900f9

.field public static final storage_usage_information_margin_top_multiwindow:I = 0x7f09011b

.field public static final storage_usage_left_arrow_margin:I = 0x7f0900f7

.field public static final storage_usage_padding_bottom:I = 0x7f0900ec

.field public static final storage_usage_padding_top:I = 0x7f0900eb

.field public static final storage_usage_page_arrow_height:I = 0x7f09010e

.field public static final storage_usage_page_arrow_height_multiwindow:I = 0x7f09012a

.field public static final storage_usage_page_arrow_margin_outside:I = 0x7f090110

.field public static final storage_usage_page_arrow_margin_outside_multiwindow:I = 0x7f09012c

.field public static final storage_usage_page_arrow_margin_top:I = 0x7f09010f

.field public static final storage_usage_page_arrow_margin_top_multiwindow:I = 0x7f09012b

.field public static final storage_usage_page_arrow_width:I = 0x7f09010d

.field public static final storage_usage_page_arrow_width_multiwindow:I = 0x7f090129

.field public static final storage_usage_page_indicator_bubble_height:I = 0x7f090105

.field public static final storage_usage_page_indicator_bubble_height_multiwindow:I = 0x7f090122

.field public static final storage_usage_page_indicator_bubble_margin_left:I = 0x7f090106

.field public static final storage_usage_page_indicator_bubble_margin_left_multiwindow:I = 0x7f090123

.field public static final storage_usage_page_indicator_bubble_width:I = 0x7f090104

.field public static final storage_usage_page_indicator_bubble_width_multiwindow:I = 0x7f090121

.field public static final storage_usage_page_indicator_height:I = 0x7f090101

.field public static final storage_usage_page_indicator_height_multiwindow:I = 0x7f09011e

.field public static final storage_usage_page_indicator_margin_left:I = 0x7f090102

.field public static final storage_usage_page_indicator_margin_left_multiwindow:I = 0x7f09011f

.field public static final storage_usage_page_indicator_margin_right:I = 0x7f090103

.field public static final storage_usage_page_indicator_margin_right_multiwindow:I = 0x7f090120

.field public static final storage_usage_right_arrow_margin:I = 0x7f0900f8

.field public static final storage_usage_space_info_height:I = 0x7f0900fc

.field public static final storage_usage_space_info_height_multiwindow:I = 0x7f09011c

.field public static final storage_usage_space_info_margin_left:I = 0x7f0900fe

.field public static final storage_usage_space_info_margin_left_multiwindow:I = 0x7f09011d

.field public static final storage_usage_space_info_text_size:I = 0x7f0900fb

.field public static final storage_usage_space_information_height:I = 0x7f0900fd

.field public static final storage_usage_space_information_margin_top:I = 0x7f0900ff

.field public static final storage_usage_storage_type_text_size:I = 0x7f0900fa

.field public static final storage_usage_width:I = 0x7f0900ed

.field public static final storage_usage_width_multiwindow:I = 0x7f090113

.field public static final thumbnail_view_grid_horizontal_spacing:I = 0x7f090051

.field public static final thumbnail_view_grid_padding_bottom:I = 0x7f09004d

.field public static final thumbnail_view_grid_padding_left:I = 0x7f09004e

.field public static final thumbnail_view_grid_padding_right:I = 0x7f09004f

.field public static final thumbnail_view_grid_padding_top:I = 0x7f09004c

.field public static final thumbnail_view_grid_vertical_spacing:I = 0x7f090050

.field public static final thumbnail_view_item_checkbox_height:I = 0x7f090049

.field public static final thumbnail_view_item_checkbox_margin_right:I = 0x7f090053

.field public static final thumbnail_view_item_checkbox_margin_top:I = 0x7f090052

.field public static final thumbnail_view_item_checkbox_width:I = 0x7f090048

.field public static final thumbnail_view_item_container_height:I = 0x7f090032

.field public static final thumbnail_view_item_drm_lock_height:I = 0x7f09005b

.field public static final thumbnail_view_item_drm_lock_marginRight:I = 0x7f09005d

.field public static final thumbnail_view_item_drm_lock_marginTop:I = 0x7f09005c

.field public static final thumbnail_view_item_drm_lock_width:I = 0x7f09005a

.field public static final thumbnail_view_item_height:I = 0x7f090034

.field public static final thumbnail_view_item_icon_height:I = 0x7f090038

.field public static final thumbnail_view_item_icon_margin_bottom:I = 0x7f09003a

.field public static final thumbnail_view_item_icon_margin_top:I = 0x7f090039

.field public static final thumbnail_view_item_icon_overlay_height:I = 0x7f09003c

.field public static final thumbnail_view_item_icon_overlay_width:I = 0x7f09003b

.field public static final thumbnail_view_item_icon_width:I = 0x7f090037

.field public static final thumbnail_view_item_margin_bottom:I = 0x7f090057

.field public static final thumbnail_view_item_margin_left:I = 0x7f090059

.field public static final thumbnail_view_item_margin_right:I = 0x7f090058

.field public static final thumbnail_view_item_margin_top:I = 0x7f090056

.field public static final thumbnail_view_item_padding:I = 0x7f090035

.field public static final thumbnail_view_item_padding_left:I = 0x7f090036

.field public static final thumbnail_view_item_radiobutton_height:I = 0x7f09004b

.field public static final thumbnail_view_item_radiobutton_margin_right:I = 0x7f090055

.field public static final thumbnail_view_item_radiobutton_margin_top:I = 0x7f090054

.field public static final thumbnail_view_item_radiobutton_width:I = 0x7f09004a

.field public static final thumbnail_view_item_thumbnail_height:I = 0x7f090043

.field public static final thumbnail_view_item_thumbnail_margin_bottom:I = 0x7f090045

.field public static final thumbnail_view_item_thumbnail_margin_left:I = 0x7f090046

.field public static final thumbnail_view_item_thumbnail_margin_right:I = 0x7f090047

.field public static final thumbnail_view_item_thumbnail_margin_top:I = 0x7f090044

.field public static final thumbnail_view_item_thumbnail_width:I = 0x7f090042

.field public static final thumbnail_view_item_title_height:I = 0x7f09003d

.field public static final thumbnail_view_item_title_margin_top:I = 0x7f09003e

.field public static final thumbnail_view_item_title_padding_left:I = 0x7f09003f

.field public static final thumbnail_view_item_title_padding_right:I = 0x7f090040

.field public static final thumbnail_view_item_title_size:I = 0x7f090041

.field public static final thumbnail_view_item_width:I = 0x7f090033

.field public static final title_bar_background_height:I = 0x7f090135

.field public static final title_bar_height:I = 0x7f090006

.field public static final title_bar_sub_title_margin_left:I = 0x7f090007

.field public static final tree_view_item_depth_width:I = 0x7f09012e

.field public static final tree_view_item_expandable_indicator_height:I = 0x7f090130

.field public static final tree_view_item_expandable_indicator_width:I = 0x7f09012f

.field public static final tree_view_item_height:I = 0x7f09012d

.field public static final tree_view_item_icon_height:I = 0x7f090132

.field public static final tree_view_item_icon_margin_left:I = 0x7f090133

.field public static final tree_view_item_icon_margin_right:I = 0x7f090134

.field public static final tree_view_item_icon_width:I = 0x7f090131


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 274
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
