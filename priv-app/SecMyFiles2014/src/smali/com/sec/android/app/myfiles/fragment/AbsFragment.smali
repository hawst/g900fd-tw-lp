.class public abstract Lcom/sec/android/app/myfiles/fragment/AbsFragment;
.super Landroid/app/Fragment;
.source "AbsFragment.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/fragment/ISelectModeFragment;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/fragment/AbsFragment$OnFragmentStateListener;,
        Lcom/sec/android/app/myfiles/fragment/AbsFragment$ManagedDialog;
    }
.end annotation


# static fields
.field private static final DEVICE_SCANNING_TOAST_DELAY:I = 0x1388

.field private static final MODULE:Ljava/lang/String; = "AbsFragment"

.field protected static mOptionsMenu:Landroid/view/Menu;


# instance fields
.field protected mActionBarManager:Lcom/sec/android/app/myfiles/view/ActionBarManager;

.field public mActionbar_cancel:Landroid/widget/TextView;

.field public mActionbar_ok:Landroid/widget/TextView;

.field protected mAdapterManager:Lcom/sec/android/app/myfiles/adapter/AdapterManager;

.field protected mFragmentId:I

.field private mIsFocused:Z

.field private mIsSelectMode:Z

.field private mManagedDialogs:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/sec/android/app/myfiles/fragment/AbsFragment$ManagedDialog;",
            ">;"
        }
    .end annotation
.end field

.field protected mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

.field protected mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

.field protected mRunFrom:I

.field private mSelectModeFrom:I

.field protected mSelectedItemChangeListener:Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;

.field private mSelectionType:I

.field protected mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 87
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 63
    iput v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mRunFrom:I

    .line 65
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mIsFocused:Z

    .line 69
    iput v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mSelectionType:I

    .line 71
    iput v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mSelectModeFrom:I

    .line 89
    return-void
.end method

.method private createDialog(Ljava/lang/Integer;Landroid/os/Bundle;Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2
    .param p1, "dialogId"    # Ljava/lang/Integer;
    .param p2, "state"    # Landroid/os/Bundle;
    .param p3, "args"    # Landroid/os/Bundle;

    .prologue
    .line 357
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    .line 359
    .local v0, "dialog":Landroid/app/Dialog;
    if-nez v0, :cond_0

    .line 361
    const/4 v0, 0x0

    .line 364
    .end local v0    # "dialog":Landroid/app/Dialog;
    :cond_0
    return-object v0
.end method

.method private onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "dialog"    # Landroid/app/Dialog;
    .param p3, "args"    # Landroid/os/Bundle;

    .prologue
    .line 376
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 377
    return-void
.end method

.method private final showDialog(ILandroid/os/Bundle;)Z
    .locals 3
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 284
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mManagedDialogs:Landroid/util/SparseArray;

    if-nez v1, :cond_0

    .line 286
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mManagedDialogs:Landroid/util/SparseArray;

    .line 289
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mManagedDialogs:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/AbsFragment$ManagedDialog;

    .line 291
    .local v0, "dialog":Lcom/sec/android/app/myfiles/fragment/AbsFragment$ManagedDialog;
    if-nez v0, :cond_2

    .line 293
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/AbsFragment$ManagedDialog;

    .end local v0    # "dialog":Lcom/sec/android/app/myfiles/fragment/AbsFragment$ManagedDialog;
    invoke-direct {v0, v2}, Lcom/sec/android/app/myfiles/fragment/AbsFragment$ManagedDialog;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsFragment$1;)V

    .line 295
    .restart local v0    # "dialog":Lcom/sec/android/app/myfiles/fragment/AbsFragment$ManagedDialog;
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v1, v2, p2}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->createDialog(Ljava/lang/Integer;Landroid/os/Bundle;Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/myfiles/fragment/AbsFragment$ManagedDialog;->mDialog:Landroid/app/Dialog;

    .line 297
    iget-object v1, v0, Lcom/sec/android/app/myfiles/fragment/AbsFragment$ManagedDialog;->mDialog:Landroid/app/Dialog;

    if-nez v1, :cond_1

    .line 299
    const/4 v1, 0x0

    .line 311
    :goto_0
    return v1

    .line 302
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mManagedDialogs:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 305
    :cond_2
    iput-object p2, v0, Lcom/sec/android/app/myfiles/fragment/AbsFragment$ManagedDialog;->mArgs:Landroid/os/Bundle;

    .line 307
    iget-object v1, v0, Lcom/sec/android/app/myfiles/fragment/AbsFragment$ManagedDialog;->mDialog:Landroid/app/Dialog;

    iget-object v2, v0, Lcom/sec/android/app/myfiles/fragment/AbsFragment$ManagedDialog;->mArgs:Landroid/os/Bundle;

    invoke-direct {p0, p1, v1, v2}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V

    .line 309
    iget-object v1, v0, Lcom/sec/android/app/myfiles/fragment/AbsFragment$ManagedDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 311
    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public abstract AsyncOpenCompleted()V
.end method

.method public final dismissDialog(I)V
    .locals 5
    .param p1, "id"    # I

    .prologue
    const/4 v4, 0x0

    .line 317
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mManagedDialogs:Landroid/util/SparseArray;

    if-nez v1, :cond_0

    .line 319
    const-string v1, "AbsFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "no dialog with id "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " was ever shown via Activity#showDialog"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 336
    :goto_0
    return-void

    .line 325
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mManagedDialogs:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/AbsFragment$ManagedDialog;

    .line 327
    .local v0, "md":Lcom/sec/android/app/myfiles/fragment/AbsFragment$ManagedDialog;
    if-nez v0, :cond_1

    .line 329
    const-string v1, "AbsFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "no dialog with id "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " was ever shown via Activity#showDialog"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 335
    :cond_1
    iget-object v1, v0, Lcom/sec/android/app/myfiles/fragment/AbsFragment$ManagedDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    goto :goto_0
.end method

.method public finishSelectMode()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 495
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mIsSelectMode:Z

    .line 497
    iput v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mSelectionType:I

    .line 499
    iput v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mSelectModeFrom:I

    .line 501
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 503
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->setMainActionBar()V

    .line 515
    :goto_0
    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mFragmentId:I

    if-nez v1, :cond_2

    .line 517
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mAdapterManager:Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    const/16 v2, 0x1a

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->finishSelectMode(I)V

    .line 530
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->invalidateOptionsMenu()V

    .line 531
    return-void

    .line 507
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    instance-of v1, v1, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v1, :cond_1

    .line 509
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/MainActivity;->closeActionmode()V

    .line 512
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->setMainActionBar()V

    goto :goto_0

    .line 521
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "detail"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/app/DialogFragment;

    .line 523
    .local v0, "detailDialog":Landroid/app/DialogFragment;
    if-eqz v0, :cond_3

    .line 525
    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismissAllowingStateLoss()V

    .line 527
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mAdapterManager:Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mFragmentId:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->finishSelectMode(I)V

    goto :goto_1
.end method

.method protected getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;
    .locals 4

    .prologue
    .line 422
    const/4 v0, 0x0

    const-string v1, "AbsFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AbsFragment : getAdapterManager() - mAdapterManager : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mAdapterManager:Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 425
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mAdapterManager:Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v0, :cond_0

    .line 427
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mAdapterManager:Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    .line 430
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mAdapterManager:Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    return-object v0
.end method

.method public getFragmentId()I
    .locals 1

    .prologue
    .line 566
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mFragmentId:I

    return v0
.end method

.method public abstract getOptionsMenuResId(Z)I
.end method

.method public getSelectModeFrom()I
    .locals 1

    .prologue
    .line 560
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mSelectModeFrom:I

    return v0
.end method

.method public getSelectionType()I
    .locals 1

    .prologue
    .line 544
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mSelectionType:I

    return v0
.end method

.method public isSelectMode()Z
    .locals 1

    .prologue
    .line 537
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mIsSelectMode:Z

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 210
    invoke-super {p0, p1, p2, p3}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 212
    packed-switch p1, :pswitch_data_0

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 214
    :pswitch_0
    const/4 v2, -0x1

    if-ne p2, v2, :cond_0

    .line 215
    if-eqz p3, :cond_0

    .line 216
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 218
    .local v0, "argument":Landroid/os/Bundle;
    const-string v2, "src_fragment_id"

    const/16 v3, 0x12

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 219
    const-string v2, "target_folder"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 220
    .local v1, "dstFolder":Ljava/lang/String;
    const-string v2, "FOLDERPATH"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v2, v2, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v2, :cond_0

    .line 223
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v2, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/myfiles/MainActivity;->onCopyMoveSelected(Landroid/os/Bundle;Ljava/lang/String;)V

    goto :goto_0

    .line 212
    :pswitch_data_0
    .packed-switch 0x33
        :pswitch_0
    .end packed-switch
.end method

.method public abstract onBackPressed()Z
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    .line 93
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 95
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/AbsMainActivity;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    .line 97
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-nez v1, :cond_0

    .line 99
    new-instance v1, Ljava/lang/ExceptionInInitializerError;

    const-string v2, "Wrong parent activity"

    invoke-direct {v1, v2}, Ljava/lang/ExceptionInInitializerError;-><init>(Ljava/lang/String;)V

    throw v1

    .line 102
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 104
    .local v0, "argument":Landroid/os/Bundle;
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mRunFrom:I

    .line 106
    if-eqz v0, :cond_1

    .line 108
    const-string v1, "id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mFragmentId:I

    .line 110
    const-string v1, "run_from"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mRunFrom:I

    .line 113
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBarManager()Lcom/sec/android/app/myfiles/view/ActionBarManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mActionBarManager:Lcom/sec/android/app/myfiles/view/ActionBarManager;

    .line 114
    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mRunFrom:I

    const/16 v2, 0x15

    if-eq v1, v2, :cond_2

    .line 115
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->setMainActionBar()V

    .line 117
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mAdapterManager:Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    .line 119
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isEnableMultiWindow(Landroid/content/Context;)Z

    move-result v1

    if-ne v1, v3, :cond_3

    .line 120
    new-instance v1, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 126
    :goto_0
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mManagedDialogs:Landroid/util/SparseArray;

    .line 129
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onCreateSelectedItemChangeListener()Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mSelectedItemChangeListener:Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;

    .line 131
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    .line 133
    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->setHasOptionsMenu(Z)V

    .line 134
    return-void

    .line 123
    :cond_3
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 370
    const/4 v0, 0x0

    return-object v0
.end method

.method protected abstract onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
.end method

.method protected abstract onCreateItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 183
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mIsFocused:Z

    if-eqz v1, :cond_2

    .line 185
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mIsSelectMode:Z

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getOptionsMenuResId(Z)I

    move-result v0

    .line 187
    .local v0, "menuResId":I
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v1, v1, Lcom/sec/android/app/myfiles/MainActivity;->mIsSearchMode:Z

    if-eqz v1, :cond_0

    .line 188
    const v0, 0x7f0e0017

    .line 190
    :cond_0
    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    .line 192
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 194
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    .line 197
    :cond_1
    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 199
    sput-object p1, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mOptionsMenu:Landroid/view/Menu;

    .line 205
    .end local v0    # "menuResId":I
    :cond_2
    :goto_0
    return-void

    .line 202
    .restart local v0    # "menuResId":I
    :cond_3
    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    goto :goto_0
.end method

.method protected abstract onCreateSelectedItemChangeListener()Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 146
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mAdapterManager:Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mAdapterManager:Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mFragmentId:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->finishSelectMode(I)V

    .line 151
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x1

    .line 236
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 272
    invoke-super {p0, p1}, Landroid/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_0
    return v1

    .line 255
    :sswitch_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-class v3, Lcom/sec/android/app/myfiles/SettingsActivity;

    invoke-direct {v0, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 257
    .local v0, "intent":Landroid/content/Intent;
    const/16 v1, 0xd

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->startActivityForResult(Landroid/content/Intent;I)V

    move v1, v2

    .line 259
    goto :goto_0

    .line 263
    .end local v0    # "intent":Landroid/content/Intent;
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 264
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    instance-of v1, v1, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v1, :cond_0

    .line 266
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/MainActivity;->searchModeStart()V

    :cond_0
    move v1, v2

    .line 269
    goto :goto_0

    .line 236
    :sswitch_data_0
    .sparse-switch
        0x7f0f0135 -> :sswitch_1
        0x7f0f013d -> :sswitch_0
    .end sparse-switch
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 382
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {p2, v0}, Landroid/app/Dialog;->setOwnerActivity(Landroid/app/Activity;)V

    .line 383
    return-void
.end method

.method public abstract onRefresh()V
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 139
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 141
    const-string v0, "VerificationLog"

    const-string v1, "Executed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    return-void
.end method

.method public final removeDialog(I)V
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 341
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mManagedDialogs:Landroid/util/SparseArray;

    if-eqz v1, :cond_0

    .line 343
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mManagedDialogs:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/AbsFragment$ManagedDialog;

    .line 345
    .local v0, "md":Lcom/sec/android/app/myfiles/fragment/AbsFragment$ManagedDialog;
    if-eqz v0, :cond_0

    .line 347
    iget-object v1, v0, Lcom/sec/android/app/myfiles/fragment/AbsFragment$ManagedDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 349
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mManagedDialogs:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 352
    .end local v0    # "md":Lcom/sec/android/app/myfiles/fragment/AbsFragment$ManagedDialog;
    :cond_0
    return-void
.end method

.method public selectmodeActionbar()V
    .locals 0

    .prologue
    .line 607
    return-void
.end method

.method public setMainActionBar()V
    .locals 3

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mIsSearchMode:Z

    if-eqz v0, :cond_1

    .line 177
    :cond_0
    :goto_0
    return-void

    .line 162
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mActionBarManager:Lcom/sec/android/app/myfiles/view/ActionBarManager;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mActionBarManager:Lcom/sec/android/app/myfiles/view/ActionBarManager;

    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mFragmentId:I

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/view/ActionBarManager;->setFragmentActionbar(ILandroid/content/Context;)V

    goto :goto_0
.end method

.method protected setSelectModeFrom(I)V
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 554
    iput p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mSelectModeFrom:I

    .line 555
    return-void
.end method

.method public setmSelectionType(I)V
    .locals 0
    .param p1, "mSelectionType"    # I

    .prologue
    .line 549
    iput p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mSelectionType:I

    .line 550
    return-void
.end method

.method protected final showDialog(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 278
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->showDialog(ILandroid/os/Bundle;)Z

    .line 279
    return-void
.end method

.method protected showToastForScanningNearbyDevices(Z)V
    .locals 7
    .param p1, "enabled"    # Z

    .prologue
    const-wide/16 v2, 0x1388

    .line 387
    if-eqz p1, :cond_1

    .line 389
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0079

    const/4 v4, 0x0

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 391
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/AbsFragment$1;

    move-object v1, p0

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/fragment/AbsFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsFragment;JJ)V

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment$1;->start()Landroid/os/CountDownTimer;

    .line 418
    :cond_0
    :goto_0
    return-void

    .line 411
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 412
    new-instance v6, Lcom/sec/android/app/myfiles/fragment/NeedWifiFragment;

    invoke-direct {v6}, Lcom/sec/android/app/myfiles/fragment/NeedWifiFragment;-><init>()V

    .line 413
    .local v6, "needwifi":Landroid/app/DialogFragment;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "need_wifi"

    invoke-virtual {v6, v0, v1}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public startSelectMode(III)V
    .locals 4
    .param p1, "selectMode"    # I
    .param p2, "initialSelectedPosition"    # I
    .param p3, "from"    # I

    .prologue
    const/4 v3, 0x6

    const/4 v2, 0x5

    const/4 v1, 0x1

    .line 437
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mIsSelectMode:Z

    .line 439
    iput p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mSelectionType:I

    .line 441
    iput p3, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mSelectModeFrom:I

    .line 443
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mSelectionType:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mSelectModeFrom:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mSelectModeFrom:I

    if-eq v0, v2, :cond_0

    .line 446
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->selectmodeActionbar()V

    .line 448
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 449
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->setSelectModeActionBar()V

    .line 477
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mAdapterManager:Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    if-eqz v0, :cond_2

    .line 479
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mFragmentId:I

    if-nez v0, :cond_8

    .line 481
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mAdapterManager:Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    const/16 v1, 0x1a

    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mSelectModeFrom:I

    invoke-virtual {v0, v1, p1, p2, v2}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->startSelectMode(IIII)V

    .line 489
    :cond_2
    :goto_1
    return-void

    .line 451
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mFragmentId:I

    if-eqz v0, :cond_5

    .line 452
    if-ne p3, v2, :cond_4

    .line 453
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v0, p3}, Lcom/sec/android/app/myfiles/MainActivity;->updateActionMode(I)V

    goto :goto_0

    .line 455
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/MainActivity;->updateActionMode(I)V

    goto :goto_0

    .line 457
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mFragmentId:I

    if-nez v0, :cond_1

    .line 458
    if-ne p3, v3, :cond_6

    .line 459
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/myfiles/MainActivity;->updateActionMode(I)V

    goto :goto_0

    .line 461
    :cond_6
    const/4 v0, -0x1

    if-ne p2, v0, :cond_7

    .line 462
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/MainActivity;->updateActionMode(I)V

    goto :goto_0

    .line 464
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/MainActivity;->updateActionMode(I)V

    goto :goto_0

    .line 485
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mAdapterManager:Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mFragmentId:I

    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mSelectModeFrom:I

    invoke-virtual {v0, v1, p1, p2, v2}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->startSelectMode(IIII)V

    goto :goto_1
.end method
