.class public Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;
.super Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
.source "ShortcutListAdapter.java"


# instance fields
.field private categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

.field private mContext:Landroid/content/Context;

.field private mIsSelectMode:Z

.field private mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/ShortcutItem;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectItemPosition:I

.field public mShortCutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "categoryHomeFragment"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mShortCutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 32
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mContext:Landroid/content/Context;

    .line 33
    iput-object p2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .line 34
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->loadShortcuts()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "categoryHomeFragment"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/ShortcutItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p3, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/ShortcutItem;>;"
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mShortCutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 37
    iput-object p2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .line 38
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mContext:Landroid/content/Context;

    .line 39
    iput-object p3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    .line 40
    return-void
.end method


# virtual methods
.method public finishSelectMode()V
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mIsSelectMode:Z

    .line 63
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->notifyDataSetChanged()V

    .line 64
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->unselectAllItem()V

    .line 66
    return-void
.end method

.method public getArrayList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/ShortcutItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 439
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getCount()I
    .locals 3

    .prologue
    .line 161
    const/4 v0, 0x0

    .line 163
    .local v0, "value":I
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v2

    .line 165
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 166
    monitor-exit v2

    .line 168
    return v0

    .line 166
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 175
    const/4 v1, 0x0

    .line 179
    .local v1, "obj":Ljava/lang/Object;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 181
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 182
    monitor-exit v3

    .line 189
    .end local v1    # "obj":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 182
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 184
    :catch_0
    move-exception v0

    .line 186
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 195
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getSelectedItem()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 125
    .local v2, "selectedItem":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v4

    .line 127
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 129
    .local v1, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 131
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 134
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 136
    return-object v2
.end method

.method public getSelectedItemCount()I
    .locals 5

    .prologue
    .line 142
    const/4 v2, 0x0

    .line 144
    .local v2, "result":I
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v4

    .line 146
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 148
    .local v1, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 150
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 153
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :cond_1
    monitor-exit v4

    .line 155
    return v2

    .line 153
    .end local v0    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public getSelectedItemPosition()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    const/4 v0, 0x0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 200
    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mContext:Landroid/content/Context;

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 201
    .local v1, "inflater":Landroid/view/LayoutInflater;
    if-nez p2, :cond_0

    .line 202
    const v5, 0x7f040031

    invoke-virtual {v1, v5, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 204
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 205
    .local v2, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    const v5, 0x7f0f0031

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 206
    .local v0, "icon":Landroid/widget/ImageView;
    const v5, 0x7f0f0032

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 207
    .local v4, "txt":Landroid/widget/TextView;
    const v5, 0x7f0f0037

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    .line 209
    .local v3, "mSelectCheckBox":Landroid/widget/CheckBox;
    invoke-virtual {v3, v7}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 211
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v5

    const/16 v6, 0x1a

    if-ne v5, v6, :cond_2

    .line 212
    const v5, 0x7f020030

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 220
    :cond_1
    :goto_0
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 221
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->isSelectMode()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 222
    invoke-virtual {v3, v7}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 223
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->isSelected()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 224
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 232
    :goto_1
    return-object p2

    .line 213
    :cond_2
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v5

    const/16 v6, 0x1c

    if-ne v5, v6, :cond_3

    .line 214
    const v5, 0x7f020051

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 215
    :cond_3
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v5

    if-ne v5, v8, :cond_4

    .line 216
    const v5, 0x7f020050

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 217
    :cond_4
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v5

    const/16 v6, 0x1f

    if-ne v5, v6, :cond_1

    .line 218
    const v5, 0x7f02004f

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 226
    :cond_5
    invoke-virtual {v3, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1

    .line 229
    :cond_6
    invoke-virtual {v3, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 230
    invoke-virtual {v3, v8}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_1
.end method

.method public isSelectMode()Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mIsSelectMode:Z

    return v0
.end method

.method public loadShortcuts()Ljava/util/ArrayList;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/ShortcutItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 236
    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    .line 237
    .local v14, "hashMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Lcom/sec/android/app/myfiles/element/ShortcutItem;>;"
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 239
    .local v18, "shortcutList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/ShortcutItem;>;"
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getLastUsedShortcutIndex(Landroid/content/Context;)I

    move-result v16

    .line 242
    .local v16, "maxItems":I
    const/4 v4, 0x0

    .line 243
    .local v4, "where":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Shortcut;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v5, 0x0

    const-string v6, "shortcut_index ASC"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 245
    .local v11, "cursor":Landroid/database/Cursor;
    if-eqz v11, :cond_1

    .line 246
    :goto_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 248
    const-string v1, "shortcut_index"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 249
    .local v10, "index":I
    const-string v1, "type"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 251
    .local v6, "shortcutType":I
    const/4 v5, 0x0

    .line 253
    .local v5, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    packed-switch v6, :pswitch_data_0

    .line 269
    new-instance v5, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .end local v5    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    const-string v1, "title"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v1, "_data"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    invoke-direct/range {v5 .. v10}, Lcom/sec/android/app/myfiles/element/ShortcutItem;-><init>(ILjava/lang/String;Ljava/lang/String;ZI)V

    .line 281
    .restart local v5    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :goto_1
    const-string v1, "shortcut_drawable"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->convertByteArrayToDrawable([B)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 282
    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v1

    const/16 v2, 0x1a

    if-ne v1, v2, :cond_0

    .line 283
    new-instance v13, Ljava/io/File;

    const-string v1, "_data"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v13, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 286
    .local v13, "file":Ljava/io/File;
    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 287
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteShortcut(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v17

    .line 289
    .local v17, "rowsDeleted":I
    if-lez v17, :cond_0

    .line 290
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->removeItem(Ljava/lang/Object;)Z

    .line 293
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->notifyDataSetChanged()V

    .line 297
    .end local v13    # "file":Ljava/io/File;
    .end local v17    # "rowsDeleted":I
    :cond_0
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v14, v1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 314
    .end local v5    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    .end local v6    # "shortcutType":I
    .end local v10    # "index":I
    .end local v11    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v12

    .line 317
    .local v12, "e":Ljava/lang/Exception;
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V

    .line 319
    .end local v12    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_2
    return-object v18

    .line 257
    .restart local v5    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    .restart local v6    # "shortcutType":I
    .restart local v10    # "index":I
    .restart local v11    # "cursor":Landroid/database/Cursor;
    :pswitch_0
    :try_start_1
    new-instance v5, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .end local v5    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    const-string v1, "title"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Security;->getInstance()Lcom/sec/android/app/myfiles/utils/Security;

    move-result-object v1

    const-string v2, "_data"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/utils/Security;->decrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    invoke-direct/range {v5 .. v10}, Lcom/sec/android/app/myfiles/element/ShortcutItem;-><init>(ILjava/lang/String;Ljava/lang/String;ZI)V

    .line 265
    .restart local v5    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    goto :goto_1

    .line 299
    .end local v5    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    .end local v6    # "shortcutType":I
    .end local v10    # "index":I
    :cond_2
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_3
    add-int/lit8 v1, v16, 0x1

    if-ge v15, v1, :cond_4

    .line 300
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v14, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 302
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v14, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v1

    const/16 v2, 0x9

    if-eq v1, v2, :cond_3

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v14, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v1

    const/16 v2, 0xa

    if-eq v1, v2, :cond_3

    .line 303
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v14, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 299
    :cond_3
    add-int/lit8 v15, v15, 0x1

    goto :goto_3

    .line 311
    :cond_4
    invoke-interface {v11}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 253
    nop

    :pswitch_data_0
    .packed-switch 0x1c
        :pswitch_0
    .end packed-switch
.end method

.method public onRefresh()V
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->loadShortcuts()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    .line 45
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->notifyDataSetChanged()V

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->refreshShortcutHeader()V

    .line 48
    :cond_0
    return-void
.end method

.method public removeAllItem(Ljava/util/ArrayList;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 384
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    if-eqz p1, :cond_0

    .line 386
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v1

    .line 388
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    monitor-exit v1

    .line 393
    :goto_0
    return v0

    .line 389
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 393
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeItem(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 398
    const/4 v0, 0x0

    .line 400
    .local v0, "result":Z
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v2

    .line 402
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 403
    monitor-exit v2

    .line 405
    return v0

    .line 403
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public selectAllItem()V
    .locals 4

    .prologue
    .line 90
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v3

    .line 92
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 94
    .local v1, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->setSelected(Z)V

    goto :goto_0

    .line 96
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 97
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setAllItemCount()V

    .line 98
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->notifyDataSetChanged()V

    .line 99
    return-void
.end method

.method public selectItem(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 77
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v2

    .line 79
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 81
    .local v0, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->isSelected()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->setSelected(Z)V

    .line 82
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setAllItemCount()V

    .line 84
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->notifyDataSetChanged()V

    .line 85
    return-void

    .line 81
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 82
    .end local v0    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public selectMode(Z)V
    .locals 0
    .param p1, "selectMode"    # Z

    .prologue
    .line 446
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mIsSelectMode:Z

    .line 447
    return-void
.end method

.method public setArrayList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/ShortcutItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 442
    .local p1, "mArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/ShortcutItem;>;"
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    .line 443
    return-void
.end method

.method public setCurrentWorkingIndex()V
    .locals 3

    .prologue
    .line 372
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v2

    .line 374
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 376
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 377
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getIndex()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mSelectItemPosition:I

    .line 374
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 380
    :cond_1
    monitor-exit v2

    .line 381
    return-void

    .line 380
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public shortcutEdit()V
    .locals 5

    .prologue
    .line 357
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 358
    .local v2, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->setCurrentWorkingIndex()V

    .line 359
    const/16 v1, 0xd

    .line 360
    .local v1, "categoryType":I
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 361
    .local v0, "argument":Landroid/os/Bundle;
    const-string v3, "FOLDERPATH"

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    const-string v3, "shortcut_working_index_intent_key"

    iget v4, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mSelectItemPosition:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 363
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mContext:Landroid/content/Context;

    instance-of v3, v3, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v3, :cond_0

    .line 364
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v3, v1, v0}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    .line 367
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->finishSelectMode()V

    .line 369
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->finishSelectMode()V

    .line 370
    return-void
.end method

.method public shortcutRename()Landroid/app/DialogFragment;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 323
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->getSelectedItemCount()I

    move-result v3

    if-nez v3, :cond_0

    move-object v2, v4

    .line 354
    :goto_0
    return-object v2

    .line 327
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 329
    .local v1, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    new-instance v2, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    invoke-direct {v2}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;-><init>()V

    .line 331
    .local v2, "newName":Landroid/app/DialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 333
    .local v0, "argument":Landroid/os/Bundle;
    const-string v3, "current_item"

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    const-string v3, "title"

    const v5, 0x7f0b0023

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 337
    const-string v3, "file_name"

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    invoke-virtual {v2, v0}, Landroid/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 341
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    if-nez v3, :cond_1

    move-object v2, v4

    .line 343
    goto :goto_0

    .line 346
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "category_home"

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/16 v4, 0x10

    invoke-virtual {v2, v3, v4}, Landroid/app/DialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 348
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "rename"

    invoke-virtual {v2, v3, v4}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 350
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->finishSelectMode()V

    .line 352
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->finishSelectMode()V

    goto :goto_0
.end method

.method public startBrowserFTPorDropBox()V
    .locals 5

    .prologue
    .line 409
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mShortCutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    if-nez v2, :cond_1

    .line 410
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutItemForWarning:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mShortCutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 411
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mShortCutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    if-nez v2, :cond_1

    .line 436
    :cond_0
    :goto_0
    return-void

    .line 415
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v3

    .line 417
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 419
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mShortCutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 420
    iput v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mSelectItemPosition:I

    .line 417
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 423
    :cond_3
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 426
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 428
    .local v0, "argument":Landroid/os/Bundle;
    const-string v2, "FOLDERPATH"

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mShortCutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    const-string v2, "shortcut_working_index_intent_key"

    iget v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mSelectItemPosition:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 430
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mContext:Landroid/content/Context;

    instance-of v2, v2, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v2, :cond_0

    .line 432
    const-string v2, "IS_SHORTCUT_FOLDER"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 433
    const-string v2, "SHORTCUT_ROOT_FOLDER"

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mShortCutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mShortCutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getCategoryType()I

    move-result v3

    invoke-virtual {v2, v3, v0}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 423
    .end local v0    # "argument":Landroid/os/Bundle;
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public startSelectMode(II)V
    .locals 1
    .param p1, "selectType"    # I
    .param p2, "from"    # I

    .prologue
    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mIsSelectMode:Z

    .line 56
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->notifyDataSetChanged()V

    .line 57
    return-void
.end method

.method public unselectAllItem()V
    .locals 4

    .prologue
    .line 104
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v3

    .line 106
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 108
    .local v1, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->setSelected(Z)V

    goto :goto_0

    .line 110
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 111
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setAllItemCount()V

    .line 112
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->notifyDataSetChanged()V

    .line 113
    return-void
.end method
