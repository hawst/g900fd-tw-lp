.class Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;
.super Landroid/content/AsyncQueryHandler;
.source "RemoteShareNavigation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "QueryHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;Landroid/content/ContentResolver;)V
    .locals 0
    .param p2, "cr"    # Landroid/content/ContentResolver;

    .prologue
    .line 291
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;->this$0:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    .line 292
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 293
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 4
    .param p1, "token"    # I
    .param p2, "cookie"    # Ljava/lang/Object;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v3, 0x3

    .line 297
    invoke-super {p0, p1, p2, p3}, Landroid/content/AsyncQueryHandler;->onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V

    .line 298
    packed-switch p1, :pswitch_data_0

    .line 331
    :cond_0
    :goto_0
    return-void

    .line 300
    :pswitch_0
    if-eqz p3, :cond_0

    invoke-interface {p3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 301
    if-eqz p3, :cond_2

    invoke-interface {p3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 302
    const-string v1, "media_box"

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 303
    .local v0, "mediaBox":I
    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_3

    .line 304
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;->this$0:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    const/16 v2, 0x27

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->setMessageFolder(I)V

    .line 309
    .end local v0    # "mediaBox":I
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;->this$0:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    # getter for: Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mFolderInfoReceivedListener:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$FolderInfoReceiverListener;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->access$200(Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;)Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$FolderInfoReceiverListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 310
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;->this$0:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    # getter for: Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mFolderInfoReceivedListener:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$FolderInfoReceiverListener;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->access$200(Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;)Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$FolderInfoReceiverListener;

    move-result-object v1

    invoke-interface {v1, v3, p3}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$FolderInfoReceiverListener;->onFolderInfoReceived(ILandroid/database/Cursor;)V

    goto :goto_0

    .line 306
    .restart local v0    # "mediaBox":I
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;->this$0:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    const/16 v2, 0x26

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->setMessageFolder(I)V

    goto :goto_1

    .line 315
    .end local v0    # "mediaBox":I
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;->this$0:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;->this$0:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    # getter for: Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mRequestedFolder:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->access$300(Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->getFolderInfoById(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->access$400(Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;Ljava/lang/String;)V

    .line 318
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;->this$0:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;->this$0:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    # getter for: Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mRequestedFolder:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->access$300(Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 319
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;->this$0:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    # setter for: Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mCurrentCursor:Landroid/database/Cursor;
    invoke-static {v1, p3}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->access$502(Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 320
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;->this$0:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v1, :cond_4

    .line 321
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;->this$0:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->AsyncOpenCompleted()V

    .line 323
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;->this$0:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mIsSyncing:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->access$102(Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;Z)Z

    goto :goto_0

    .line 326
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;->this$0:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    # getter for: Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mFolderInfoReceivedListener:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$FolderInfoReceiverListener;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->access$200(Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;)Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$FolderInfoReceiverListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 327
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;->this$0:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    # getter for: Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mFolderInfoReceivedListener:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$FolderInfoReceiverListener;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->access$200(Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;)Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$FolderInfoReceiverListener;

    move-result-object v1

    const/4 v2, 0x4

    invoke-interface {v1, v2, p3}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$FolderInfoReceiverListener;->onFolderInfoReceived(ILandroid/database/Cursor;)V

    goto/16 :goto_0

    .line 298
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
