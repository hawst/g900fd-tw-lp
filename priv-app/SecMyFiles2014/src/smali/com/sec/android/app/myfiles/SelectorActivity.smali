.class public Lcom/sec/android/app/myfiles/SelectorActivity;
.super Lcom/sec/android/app/myfiles/AbsMainActivity;
.source "SelectorActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/SelectorActivity$13;,
        Lcom/sec/android/app/myfiles/SelectorActivity$ManagedDialog;,
        Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;,
        Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;,
        Lcom/sec/android/app/myfiles/SelectorActivity$FtpTabMode;
    }
.end annotation


# static fields
.field private static final FTP_BROWSER_TAG:Ljava/lang/String; = "ftp-browser"

.field private static final FTP_SHORTCUT_TAG:Ljava/lang/String; = "ftp-shortcut"

.field private static final MODULE:Ljava/lang/String;

.field private static final OPTION_SELECT_ALL:I = 0x0

.field private static final OPTION_UNSELECT_ALL:I = 0x1

.field private static final SELECTOR_ACTIVITY_TAG:Ljava/lang/String; = "SelectorActivity"

.field private static final USERSHORTCUT_BROWSER_TAG:Ljava/lang/String; = "user-browser"

.field private static final USERSHORTCUT_TAG:Ljava/lang/String; = "user-shortcut"

.field private static mSelectionLock:Ljava/lang/Object;


# instance fields
.field private bDoNotCheckDropBox:Z

.field private bDoNotCheckFTP:Z

.field public mActionbarSelectAllContainer:Landroid/view/View;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mAudioPlayer:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

.field private mCategoryType:I

.field private mCurrentTabTag:Ljava/lang/String;

.field private mDailog:Landroid/app/Dialog;

.field private mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

.field private mExternalStorageReceiverFilter:Landroid/content/IntentFilter;

.field private mFileOperation:I

.field private mFileOperationPrivate:I

.field private mForWarningPosition:I

.field protected mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

.field private mIsCheckedShowDialogWLANOn:Z

.field private mLatestPosition:I

.field private mManagedDialogs:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/sec/android/app/myfiles/SelectorActivity$ManagedDialog;",
            ">;"
        }
    .end annotation
.end field

.field private mRefreshHandler:Landroid/os/Handler;

.field public mSelectAllButton:Landroid/widget/TextView;

.field public mSelectAllCheckBox:Landroid/widget/ImageView;

.field mSelectAllClickListener:Landroid/view/View$OnClickListener;

.field public mSelectAllContainer:Landroid/widget/RelativeLayout;

.field private mSelectOptions:[Ljava/lang/String;

.field private mSelectOptionsIds:[I

.field private mSelectionType:I

.field private mShowFolderFileType:I

.field private mStartFolder:Ljava/lang/String;

.field private mTabHost:Landroid/widget/TabHost;

.field private mTabMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mTabWidget:Landroid/widget/TabWidget;

.field private mTabsAdapter:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;

.field mUnselectAllClickListener:Landroid/view/View$OnClickListener;

.field private mViewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 101
    const-class v0, Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/SelectorActivity;->MODULE:Ljava/lang/String;

    .line 127
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectionLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 99
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/AbsMainActivity;-><init>()V

    .line 158
    iput v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mFileOperation:I

    .line 160
    iput v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mFileOperationPrivate:I

    .line 170
    iput-boolean v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->bDoNotCheckDropBox:Z

    .line 172
    iput-boolean v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->bDoNotCheckFTP:Z

    .line 175
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mIsCheckedShowDialogWLANOn:Z

    .line 183
    iput v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mCategoryType:I

    .line 185
    iput v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mLatestPosition:I

    .line 187
    iput v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mForWarningPosition:I

    .line 1579
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/myfiles/SelectorActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/SelectorActivity$2;-><init>(Lcom/sec/android/app/myfiles/SelectorActivity;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mRefreshHandler:Landroid/os/Handler;

    .line 1991
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mAudioManager:Landroid/media/AudioManager;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/SelectorActivity;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SelectorActivity;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/myfiles/SelectorActivity;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SelectorActivity;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectOptionsIds:[I

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/myfiles/SelectorActivity;[I)[I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SelectorActivity;
    .param p1, "x1"    # [I

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectOptionsIds:[I

    return-object p1
.end method

.method static synthetic access$1100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    sget-object v0, Lcom/sec/android/app/myfiles/SelectorActivity;->MODULE:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/myfiles/SelectorActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SelectorActivity;

    .prologue
    .line 99
    iget v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mCategoryType:I

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/myfiles/SelectorActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SelectorActivity;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mRefreshHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/myfiles/SelectorActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SelectorActivity;

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->bDoNotCheckDropBox:Z

    return v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/myfiles/SelectorActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SelectorActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 99
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->bDoNotCheckDropBox:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/myfiles/SelectorActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SelectorActivity;

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->bDoNotCheckFTP:Z

    return v0
.end method

.method static synthetic access$1602(Lcom/sec/android/app/myfiles/SelectorActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SelectorActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 99
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->bDoNotCheckFTP:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/sec/android/app/myfiles/SelectorActivity;)Landroid/widget/TabHost;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SelectorActivity;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabHost:Landroid/widget/TabHost;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/myfiles/SelectorActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SelectorActivity;

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mIsCheckedShowDialogWLANOn:Z

    return v0
.end method

.method static synthetic access$1802(Lcom/sec/android/app/myfiles/SelectorActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SelectorActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 99
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mIsCheckedShowDialogWLANOn:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/SelectorActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SelectorActivity;

    .prologue
    .line 99
    iget v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectionType:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/myfiles/SelectorActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SelectorActivity;

    .prologue
    .line 99
    iget v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mShowFolderFileType:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/myfiles/SelectorActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SelectorActivity;

    .prologue
    .line 99
    iget v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mForWarningPosition:I

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/myfiles/SelectorActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SelectorActivity;
    .param p1, "x1"    # I

    .prologue
    .line 99
    iput p1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mForWarningPosition:I

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/myfiles/SelectorActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SelectorActivity;

    .prologue
    .line 99
    iget v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mLatestPosition:I

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/myfiles/SelectorActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SelectorActivity;
    .param p1, "x1"    # I

    .prologue
    .line 99
    iput p1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mLatestPosition:I

    return p1
.end method

.method static synthetic access$800()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 99
    sget-object v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectionLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/myfiles/SelectorActivity;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SelectorActivity;
    .param p1, "x1"    # [Ljava/lang/String;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectOptions:[Ljava/lang/String;

    return-object p1
.end method

.method public static appendIntentArgs(Landroid/os/Bundle;Landroid/content/Intent;)Landroid/os/Bundle;
    .locals 5
    .param p0, "arg"    # Landroid/os/Bundle;
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 731
    if-eqz p1, :cond_0

    .line 732
    const-string v1, "SELECTOR_SOURCE_TYPE"

    const-string v2, "SELECTOR_SOURCE_TYPE"

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {p0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 734
    const-string v1, "SELECTOR_SOURCE_PATH"

    const-string v2, "SELECTOR_SOURCE_PATH"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 736
    const-string v1, "SELECTOR_FTP_MOVE"

    const-string v2, "SELECTOR_FTP_MOVE"

    invoke-virtual {p1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {p0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 738
    const-string v1, "shortcut_working_index_intent_key"

    const-string v2, "shortcut_working_index_intent_key"

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {p0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 740
    const-string v1, "from_add_shortcut"

    const-string v2, "from_add_shortcut"

    invoke-virtual {p1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {p0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 742
    const-string v1, "SELECTOR_SAME_FTP"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 743
    .local v0, "params":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 744
    const-string v1, "SELECTOR_SAME_FTP"

    invoke-virtual {p0, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 747
    .end local v0    # "params":Ljava/lang/String;
    :cond_0
    return-object p0
.end method

.method private createDialog(Ljava/lang/Integer;Landroid/os/Bundle;Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2
    .param p1, "dialogId"    # Ljava/lang/Integer;
    .param p2, "state"    # Landroid/os/Bundle;
    .param p3, "args"    # Landroid/os/Bundle;

    .prologue
    .line 1675
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->onCreateWarningDialog(I)Landroid/app/Dialog;

    move-result-object v0

    .line 1677
    .local v0, "dialog":Landroid/app/Dialog;
    if-nez v0, :cond_0

    .line 1679
    const/4 v0, 0x0

    .line 1682
    .end local v0    # "dialog":Landroid/app/Dialog;
    :cond_0
    return-object v0
.end method

.method private insertInDatabase(Ljava/lang/String;)Z
    .locals 6
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 754
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 756
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v2, "external"

    invoke-static {v2}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 758
    .local v1, "uri":Landroid/net/Uri;
    const-string v2, "_data"

    invoke-virtual {v0, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 760
    const-string v2, "format"

    const-wide/16 v4, 0x3001

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 762
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SelectorActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 764
    const/4 v2, 0x1

    .line 767
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private onCreateWarningDialog(I)Landroid/app/Dialog;
    .locals 12
    .param p1, "id"    # I

    .prologue
    const v11, 0x7f0b0125

    const v10, 0x7f0b0017

    const v9, 0x7f040032

    const/4 v8, 0x0

    const/4 v6, 0x0

    .line 1688
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1690
    .local v1, "dialog":Landroid/app/AlertDialog$Builder;
    packed-switch p1, :pswitch_data_0

    .line 1846
    :goto_0
    :pswitch_0
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mDailog:Landroid/app/Dialog;

    .line 1848
    iget-object v6, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mDailog:Landroid/app/Dialog;

    :cond_0
    return-object v6

    .line 1695
    :pswitch_1
    move v2, p1

    .line 1697
    .local v2, "dialogId":I
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v5

    .line 1699
    .local v5, "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    const/4 v7, 0x4

    if-ne v2, v7, :cond_1

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckFTP()Z

    move-result v7

    if-nez v7, :cond_0

    .line 1703
    :cond_1
    const/4 v7, 0x3

    if-eq v2, v7, :cond_2

    const/4 v7, 0x5

    if-ne v2, v7, :cond_3

    :cond_2
    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckDropBox()Z

    move-result v7

    if-nez v7, :cond_0

    .line 1710
    :cond_3
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 1712
    .local v3, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {v3, v9, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1714
    .local v0, "customView":Landroid/view/View;
    invoke-virtual {v1, v11}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1716
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isChinaModel()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1717
    const v6, 0x7f0f00c5

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 1718
    .local v4, "mobileDataWarningDialog":Landroid/widget/TextView;
    const v6, 0x7f0b00e0

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(I)V

    .line 1720
    .end local v4    # "mobileDataWarningDialog":Landroid/widget/TextView;
    :cond_4
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 1722
    new-instance v6, Lcom/sec/android/app/myfiles/SelectorActivity$3;

    invoke-direct {v6, p0}, Lcom/sec/android/app/myfiles/SelectorActivity$3;-><init>(Lcom/sec/android/app/myfiles/SelectorActivity;)V

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 1736
    const v6, 0x7f0b017a

    new-instance v7, Lcom/sec/android/app/myfiles/SelectorActivity$4;

    invoke-direct {v7, p0, v2, v5}, Lcom/sec/android/app/myfiles/SelectorActivity$4;-><init>(Lcom/sec/android/app/myfiles/SelectorActivity;ILcom/sec/android/app/myfiles/utils/SharedDataStore;)V

    invoke-virtual {v1, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1765
    new-instance v6, Lcom/sec/android/app/myfiles/SelectorActivity$5;

    invoke-direct {v6, p0, v2}, Lcom/sec/android/app/myfiles/SelectorActivity$5;-><init>(Lcom/sec/android/app/myfiles/SelectorActivity;I)V

    invoke-virtual {v1, v10, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 1791
    .end local v0    # "customView":Landroid/view/View;
    .end local v2    # "dialogId":I
    .end local v3    # "inflater":Landroid/view/LayoutInflater;
    .end local v5    # "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    :pswitch_2
    move v2, p1

    .line 1793
    .restart local v2    # "dialogId":I
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v5

    .line 1795
    .restart local v5    # "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckWLANOn()Z

    move-result v7

    if-nez v7, :cond_0

    .line 1799
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 1800
    .restart local v3    # "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {v3, v9, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1801
    .restart local v0    # "customView":Landroid/view/View;
    invoke-virtual {v1, v11}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 1802
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 1804
    new-instance v6, Lcom/sec/android/app/myfiles/SelectorActivity$6;

    invoke-direct {v6, p0}, Lcom/sec/android/app/myfiles/SelectorActivity$6;-><init>(Lcom/sec/android/app/myfiles/SelectorActivity;)V

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 1818
    const v6, 0x7f0b017a

    new-instance v7, Lcom/sec/android/app/myfiles/SelectorActivity$7;

    invoke-direct {v7, p0, v5}, Lcom/sec/android/app/myfiles/SelectorActivity$7;-><init>(Lcom/sec/android/app/myfiles/SelectorActivity;Lcom/sec/android/app/myfiles/utils/SharedDataStore;)V

    invoke-virtual {v1, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1834
    new-instance v6, Lcom/sec/android/app/myfiles/SelectorActivity$8;

    invoke-direct {v6, p0}, Lcom/sec/android/app/myfiles/SelectorActivity$8;-><init>(Lcom/sec/android/app/myfiles/SelectorActivity;)V

    invoke-virtual {v1, v10, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_0

    .line 1690
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private onPrepareWarningDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "dialog"    # Landroid/app/Dialog;
    .param p3, "args"    # Landroid/os/Bundle;

    .prologue
    .line 1854
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/myfiles/SelectorActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 1855
    return-void
.end method

.method private setExternalStorageReceiver()V
    .locals 2

    .prologue
    .line 1547
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mExternalStorageReceiverFilter:Landroid/content/IntentFilter;

    .line 1549
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mExternalStorageReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1550
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mExternalStorageReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1552
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mExternalStorageReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 1554
    new-instance v0, Lcom/sec/android/app/myfiles/SelectorActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/SelectorActivity$1;-><init>(Lcom/sec/android/app/myfiles/SelectorActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

    .line 1577
    return-void
.end method

.method private final showWarningDialog(ILandroid/os/Bundle;)Z
    .locals 3
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 1643
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mManagedDialogs:Landroid/util/SparseArray;

    if-nez v1, :cond_0

    .line 1645
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mManagedDialogs:Landroid/util/SparseArray;

    .line 1648
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mManagedDialogs:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/SelectorActivity$ManagedDialog;

    .line 1650
    .local v0, "dialog":Lcom/sec/android/app/myfiles/SelectorActivity$ManagedDialog;
    if-nez v0, :cond_2

    .line 1652
    new-instance v0, Lcom/sec/android/app/myfiles/SelectorActivity$ManagedDialog;

    .end local v0    # "dialog":Lcom/sec/android/app/myfiles/SelectorActivity$ManagedDialog;
    invoke-direct {v0, v2}, Lcom/sec/android/app/myfiles/SelectorActivity$ManagedDialog;-><init>(Lcom/sec/android/app/myfiles/SelectorActivity$1;)V

    .line 1654
    .restart local v0    # "dialog":Lcom/sec/android/app/myfiles/SelectorActivity$ManagedDialog;
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v1, v2, p2}, Lcom/sec/android/app/myfiles/SelectorActivity;->createDialog(Ljava/lang/Integer;Landroid/os/Bundle;Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/myfiles/SelectorActivity$ManagedDialog;->mDialog:Landroid/app/Dialog;

    .line 1656
    iget-object v1, v0, Lcom/sec/android/app/myfiles/SelectorActivity$ManagedDialog;->mDialog:Landroid/app/Dialog;

    if-nez v1, :cond_1

    .line 1658
    const/4 v1, 0x0

    .line 1670
    :goto_0
    return v1

    .line 1661
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mManagedDialogs:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1664
    :cond_2
    iput-object p2, v0, Lcom/sec/android/app/myfiles/SelectorActivity$ManagedDialog;->mArgs:Landroid/os/Bundle;

    .line 1666
    iget-object v1, v0, Lcom/sec/android/app/myfiles/SelectorActivity$ManagedDialog;->mDialog:Landroid/app/Dialog;

    iget-object v2, v0, Lcom/sec/android/app/myfiles/SelectorActivity$ManagedDialog;->mArgs:Landroid/os/Bundle;

    invoke-direct {p0, p1, v1, v2}, Lcom/sec/android/app/myfiles/SelectorActivity;->onPrepareWarningDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V

    .line 1668
    iget-object v1, v0, Lcom/sec/android/app/myfiles/SelectorActivity$ManagedDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 1670
    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getAudioPlayer()Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;
    .locals 1

    .prologue
    .line 1998
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mAudioPlayer:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    return-object v0
.end method

.method protected getMainLayoutResId()I
    .locals 1

    .prologue
    .line 560
    const v0, 0x7f040046

    return v0
.end method

.method public getmFileOperation()I
    .locals 1

    .prologue
    .line 1626
    iget v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mFileOperation:I

    return v0
.end method

.method public getmSelectionType()I
    .locals 1

    .prologue
    .line 1496
    iget v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectionType:I

    return v0
.end method

.method public initialActionBar()V
    .locals 1

    .prologue
    .line 572
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SelectorActivity;->getActionBarManager()Lcom/sec/android/app/myfiles/view/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/view/ActionBarManager;->initialize()V

    .line 573
    return-void
.end method

.method protected loadComponentFromView()V
    .locals 0

    .prologue
    .line 566
    return-void
.end method

.method public mediaExecute(Ljava/lang/String;I)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "position"    # I

    .prologue
    .line 2002
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    if-nez v1, :cond_0

    .line 2003
    const-string v1, "phone"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 2005
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 2006
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mAudioPlayer:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2007
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mAudioPlayer:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->stop()V

    .line 2009
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->showNoPlayDuringCallToast(Landroid/content/Context;)V

    .line 2010
    const/4 v1, 0x0

    const-string v2, "SelectorActivity"

    const-string v3, "TelephonyManager.CALL_STATE_OFFHOOK"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 2047
    :goto_0
    return-void

    .line 2013
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mAudioPlayer:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2018
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mAudioPlayer:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->stop()V

    .line 2021
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mAudioPlayer:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->play(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 2022
    :catch_0
    move-exception v0

    .line 2024
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 2025
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 2027
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 2028
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 2030
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 2035
    .end local v0    # "e":Ljava/io/IOException;
    :cond_3
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mAudioPlayer:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->play(Ljava/lang/String;I)I
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    goto :goto_0

    .line 2036
    :catch_3
    move-exception v0

    .line 2038
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 2039
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_4
    move-exception v0

    .line 2041
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 2042
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_5
    move-exception v0

    .line 2044
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 678
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v0, :cond_1

    .line 680
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onBackPressed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SelectorActivity;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 682
    invoke-super {p0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onBackPressed()V

    .line 689
    :cond_0
    :goto_0
    return-void

    .line 687
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1503
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1506
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 28
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 194
    invoke-super/range {p0 .. p1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onCreate(Landroid/os/Bundle;)V

    .line 196
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/SelectorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    .line 198
    .local v22, "res":Landroid/content/res/Resources;
    const v24, 0x1020012

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v24

    check-cast v24, Landroid/widget/TabHost;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabHost:Landroid/widget/TabHost;

    .line 200
    const v24, 0x1020013

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v24

    check-cast v24, Landroid/widget/TabWidget;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabWidget:Landroid/widget/TabWidget;

    .line 202
    new-instance v24, Ljava/util/HashMap;

    invoke-direct/range {v24 .. v24}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabMap:Ljava/util/HashMap;

    .line 204
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabHost:Landroid/widget/TabHost;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/widget/TabHost;->setup()V

    .line 206
    const v24, 0x7f0f0102

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v24

    check-cast v24, Landroid/support/v4/view/ViewPager;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 208
    new-instance v24, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabHost:Landroid/widget/TabHost;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    move-object/from16 v26, v0

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    move-object/from16 v2, p0

    move-object/from16 v3, v25

    move-object/from16 v4, v26

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;-><init>(Lcom/sec/android/app/myfiles/SelectorActivity;Landroid/app/Activity;Landroid/widget/TabHost;Landroid/support/v4/view/ViewPager;)V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabsAdapter:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

    .line 210
    if-eqz p1, :cond_0

    .line 212
    const-string v24, "tab"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mCurrentTabTag:Ljava/lang/String;

    .line 213
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/SelectorActivity;->finish()V

    .line 216
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/SelectorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    .line 218
    .local v8, "data":Landroid/content/Intent;
    invoke-virtual {v8}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    .line 220
    .local v5, "action":Ljava/lang/String;
    const-string v24, "FILE_OPERATION"

    const/16 v25, -0x1

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mFileOperation:I

    .line 222
    const-string v24, "FILE_OPERATION_PRIVATE"

    const/16 v25, -0x1

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mFileOperationPrivate:I

    .line 224
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_1

    .line 226
    const-string v24, "FOLDERPATH"

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mStartFolder:Ljava/lang/String;

    .line 227
    const-string v24, "SELECTOR_CATEGORY_TYPE"

    const/16 v25, -0x1

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mCategoryType:I

    .line 229
    const-string v24, "com.sec.android.app.myfiles.PICK_DATA"

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_b

    .line 231
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectionType:I

    .line 233
    const/16 v24, 0x2

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mShowFolderFileType:I

    .line 301
    :cond_1
    :goto_0
    const-string v24, "CONTENT_TYPE"

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 303
    .local v16, "filterMimeType":Ljava/lang/String;
    if-eqz v16, :cond_2

    .line 304
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setmMimeTypeForCategory(Ljava/lang/String;)V

    .line 307
    :cond_2
    const-string v24, "CONTENT_EXTENSION"

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 309
    .local v15, "filterExtension":Ljava/lang/String;
    if-eqz v15, :cond_3

    .line 310
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v15}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setmExtensionForCategory(Ljava/lang/String;)V

    .line 312
    :cond_3
    const-string v24, "EXCEPT_CONTENT_TYPE"

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 314
    .local v14, "filterExceptMimeType":Ljava/lang/String;
    const-string v24, "EXCEPT_CONTENT_EXTENSION"

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 316
    .local v13, "filterExceptExtension":Ljava/lang/String;
    const-string v24, "from_add_shortcut"

    const/16 v25, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v18

    .line 318
    .local v18, "fromAddShortcut":Z
    const-string v24, "from_personal_widget"

    const/16 v25, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v19

    .line 320
    .local v19, "fromPersonalWidget":Z
    const-string v24, "JUST_SELECT_MODE"

    const/16 v25, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v20

    .line 322
    .local v20, "justSelectMode":Z
    const-string v24, "reply_by_broadcast"

    const/16 v25, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v21

    .line 326
    .local v21, "replyByBroadcast":Z
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 328
    .local v7, "baseArgument":Landroid/os/Bundle;
    const-string v24, "run_from"

    const/16 v25, 0x15

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 329
    const/16 v24, 0x1

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mRunFromSelector:Z

    .line 331
    const-string v24, "selection_type"

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectionType:I

    move/from16 v25, v0

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 333
    const-string v24, "show_folder_file_type"

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mShowFolderFileType:I

    move/from16 v25, v0

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 335
    const-string v24, "JUST_SELECT_MODE"

    move-object/from16 v0, v24

    move/from16 v1, v20

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 337
    const-string v24, "FILE_OPERATION"

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mFileOperation:I

    move/from16 v25, v0

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 339
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mStartFolder:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_4

    .line 341
    const-string v24, "FOLDERPATH"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mStartFolder:Ljava/lang/String;

    move-object/from16 v25, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    :cond_4
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_5

    .line 346
    const-string v24, "CONTENT_EXTENSION"

    move-object/from16 v0, v24

    invoke-virtual {v7, v0, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    :cond_5
    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_6

    .line 351
    const-string v24, "*/*"

    move-object/from16 v0, v16

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_11

    .line 353
    const/16 v24, 0x2

    const-string v25, "SelectorActivity"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "filterMimeType should be setted to null as "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v24 .. v26}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 362
    :cond_6
    :goto_1
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_7

    .line 364
    const-string v24, "EXCEPT_CONTENT_TYPE"

    move-object/from16 v0, v24

    invoke-virtual {v7, v0, v14}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    :cond_7
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_8

    .line 369
    const-string v24, "EXCEPT_CONTENT_EXTENSION"

    move-object/from16 v0, v24

    invoke-virtual {v7, v0, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    :cond_8
    if-eqz v21, :cond_9

    .line 374
    const-string v24, "reply_by_broadcast"

    move-object/from16 v0, v24

    move/from16 v1, v21

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 379
    :cond_9
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6, v7}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 381
    .local v6, "argument":Landroid/os/Bundle;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mCategoryType:I

    move/from16 v24, v0

    const/16 v25, -0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_12

    .line 383
    const-string v24, "id"

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mCategoryType:I

    move/from16 v25, v0

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 385
    const-string v24, "SELECTOR_CATEGORY_TYPE"

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mCategoryType:I

    move/from16 v25, v0

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 387
    const-string v24, "category_type"

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mCategoryType:I

    move/from16 v25, v0

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 389
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabsAdapter:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabHost:Landroid/widget/TabHost;

    move-object/from16 v25, v0

    const-string v26, "document"

    invoke-virtual/range {v25 .. v26}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v25

    const v26, 0x7f0b0006

    move-object/from16 v0, v22

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v25

    const-class v26, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2, v6}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->addTab(Landroid/widget/TabHost$TabSpec;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 529
    :cond_a
    :goto_2
    return-void

    .line 235
    .end local v6    # "argument":Landroid/os/Bundle;
    .end local v7    # "baseArgument":Landroid/os/Bundle;
    .end local v13    # "filterExceptExtension":Ljava/lang/String;
    .end local v14    # "filterExceptMimeType":Ljava/lang/String;
    .end local v15    # "filterExtension":Ljava/lang/String;
    .end local v16    # "filterMimeType":Ljava/lang/String;
    .end local v18    # "fromAddShortcut":Z
    .end local v19    # "fromPersonalWidget":Z
    .end local v20    # "justSelectMode":Z
    .end local v21    # "replyByBroadcast":Z
    :cond_b
    const-string v24, "com.sec.android.app.myfiles.PICK_DATA_MULTIPLE"

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_c

    .line 237
    const/16 v24, 0x1

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectionType:I

    .line 239
    const/16 v24, 0x2

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mShowFolderFileType:I

    goto/16 :goto_0

    .line 241
    :cond_c
    const-string v24, "com.sec.android.app.myfiles.PICK_SELECT_PATH"

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_d

    .line 243
    const/16 v24, 0x2

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectionType:I

    .line 245
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mShowFolderFileType:I

    goto/16 :goto_0

    .line 248
    :cond_d
    const-string v24, "com.sec.android.app.myfiles.PICK_DATA_DOWNLOAD"

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_1

    .line 250
    const/4 v9, 0x0

    .line 252
    .local v9, "dstFile":Ljava/io/File;
    const/16 v23, 0x1

    .line 254
    .local v23, "result":Z
    const/16 v24, 0x3

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectionType:I

    .line 256
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mShowFolderFileType:I

    .line 258
    const-string v24, "uri"

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mStartFolder:Ljava/lang/String;

    .line 260
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mStartFolder:Ljava/lang/String;

    move-object/from16 v24, v0

    if-eqz v24, :cond_e

    .line 262
    new-instance v9, Ljava/io/File;

    .end local v9    # "dstFile":Ljava/io/File;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mStartFolder:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 265
    .restart local v9    # "dstFile":Ljava/io/File;
    :cond_e
    if-eqz v9, :cond_f

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v24

    if-nez v24, :cond_f

    .line 267
    invoke-virtual {v9}, Ljava/io/File;->mkdir()Z

    move-result v23

    .line 270
    :cond_f
    const/16 v24, 0x0

    const-string v25, "SelectorActivity"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "SelectorActivity result="

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v24 .. v26}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 272
    if-eqz v23, :cond_10

    .line 276
    const/16 v24, 0x0

    :try_start_0
    const-string v25, "SelectorActivity"

    const-string v26, "insertInDatabase"

    invoke-static/range {v24 .. v26}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 278
    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->insertInDatabase(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 285
    :goto_3
    const/16 v24, 0x0

    const-string v25, "SelectorActivity"

    const-string v26, "sendScan"

    invoke-static/range {v24 .. v26}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 287
    invoke-virtual {v9}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/utils/Utils;->sendScan(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 280
    :catch_0
    move-exception v10

    .line 282
    .local v10, "e":Ljava/lang/Exception;
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 291
    .end local v10    # "e":Ljava/lang/Exception;
    :cond_10
    const v24, 0x7f0b00fb

    const/16 v25, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/widget/Toast;->show()V

    .line 293
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getRootFolder()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mStartFolder:Ljava/lang/String;

    goto/16 :goto_0

    .line 358
    .end local v9    # "dstFile":Ljava/io/File;
    .end local v23    # "result":Z
    .restart local v7    # "baseArgument":Landroid/os/Bundle;
    .restart local v13    # "filterExceptExtension":Ljava/lang/String;
    .restart local v14    # "filterExceptMimeType":Ljava/lang/String;
    .restart local v15    # "filterExtension":Ljava/lang/String;
    .restart local v16    # "filterMimeType":Ljava/lang/String;
    .restart local v18    # "fromAddShortcut":Z
    .restart local v19    # "fromPersonalWidget":Z
    .restart local v20    # "justSelectMode":Z
    .restart local v21    # "replyByBroadcast":Z
    :cond_11
    const-string v24, "CONTENT_TYPE"

    move-object/from16 v0, v24

    move-object/from16 v1, v16

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 392
    .restart local v6    # "argument":Landroid/os/Bundle;
    :cond_12
    if-eqz v16, :cond_13

    const-string v24, "audio/"

    move-object/from16 v0, v16

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v24

    if-eqz v24, :cond_13

    .line 394
    const/16 v24, 0x1

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mIsAudioSelector:Z

    .line 395
    invoke-static {}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->getInstance()Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mAudioPlayer:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    .line 397
    const-string v24, "id"

    const/16 v25, 0x4

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 399
    const-string v24, "SELECTOR_CATEGORY_TYPE"

    const/16 v25, 0x4

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 401
    const-string v24, "category_type"

    const/16 v25, 0x4

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 403
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabsAdapter:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabHost:Landroid/widget/TabHost;

    move-object/from16 v25, v0

    const-string v26, "audio"

    invoke-virtual/range {v25 .. v26}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v25

    const v26, 0x7f0b0147

    move-object/from16 v0, v22

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v25

    const-class v26, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2, v6}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->addTab(Landroid/widget/TabHost$TabSpec;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 405
    const-string v24, "audio"

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/media/AudioManager;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mAudioManager:Landroid/media/AudioManager;

    .line 406
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mAudioPlayer:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mAudioManager:Landroid/media/AudioManager;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->setAudioManager(Landroid/media/AudioManager;)V

    goto/16 :goto_2

    .line 410
    :cond_13
    const-string v24, "id"

    const/16 v25, 0x201

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 412
    if-nez v18, :cond_14

    if-nez v19, :cond_14

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mFileOperationPrivate:I

    move/from16 v24, v0

    const/16 v25, 0xe

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_15

    .line 414
    :cond_14
    const-string v24, "hide_storage"

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [I

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const/16 v27, 0x9

    aput v27, v25, v26

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 418
    :cond_15
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mFileOperationPrivate:I

    move/from16 v24, v0

    const/16 v25, 0xf

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_16

    .line 420
    const-string v24, "hide_storage"

    const/16 v25, 0x8

    move/from16 v0, v25

    new-array v0, v0, [I

    move-object/from16 v25, v0

    fill-array-data v25, :array_0

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 425
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabsAdapter:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabHost:Landroid/widget/TabHost;

    move-object/from16 v25, v0

    const-string v26, "local"

    invoke-virtual/range {v25 .. v26}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v25

    const v26, 0x7f0b0001

    move-object/from16 v0, v22

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v25

    const-class v26, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2, v6}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->addTab(Landroid/widget/TabHost$TabSpec;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 428
    const/4 v11, 0x1

    .line 430
    .local v11, "enableDropbox":Z
    const/4 v12, 0x1

    .line 432
    .local v12, "enableFtp":Z
    const-string v24, "SELECTOR_SOURCE_TYPE"

    const/16 v25, -0x1

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    .line 438
    .local v17, "fragmentId":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/SelectorActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/sec/android/app/myfiles/utils/Utils;->isInKNOXMode(Landroid/content/Context;)Z

    move-result v24

    if-nez v24, :cond_17

    const/16 v24, 0x8

    move/from16 v0, v17

    move/from16 v1, v24

    if-eq v0, v1, :cond_17

    const/16 v24, 0x1f

    move/from16 v0, v17

    move/from16 v1, v24

    if-eq v0, v1, :cond_17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectionType:I

    move/from16 v24, v0

    const/16 v25, 0x2

    move/from16 v0, v24

    move/from16 v1, v25

    if-lt v0, v1, :cond_17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mFileOperationPrivate:I

    move/from16 v24, v0

    const/16 v25, 0xf

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_18

    .line 439
    :cond_17
    const/4 v12, 0x0

    .line 441
    :cond_18
    const/16 v24, 0xa

    move/from16 v0, v17

    move/from16 v1, v24

    if-eq v0, v1, :cond_19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectionType:I

    move/from16 v24, v0

    const/16 v25, 0x2

    move/from16 v0, v24

    move/from16 v1, v25

    if-lt v0, v1, :cond_19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mFileOperationPrivate:I

    move/from16 v24, v0

    const/16 v25, 0xf

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_1a

    .line 442
    :cond_19
    const/4 v11, 0x0

    .line 445
    :cond_1a
    move-object/from16 v0, p0

    invoke-static {v0, v11, v12}, Lcom/sec/android/app/myfiles/utils/Utils;->getUserShortCutCount(Landroid/content/Context;ZZ)I

    move-result v24

    if-lez v24, :cond_1b

    if-nez v18, :cond_1b

    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v24

    if-nez v24, :cond_1b

    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v24

    if-nez v24, :cond_1b

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectionType:I

    move/from16 v24, v0

    const/16 v25, 0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-le v0, v1, :cond_1b

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mFileOperationPrivate:I

    move/from16 v24, v0

    const/16 v25, 0xf

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_1b

    .line 448
    new-instance v6, Landroid/os/Bundle;

    .end local v6    # "argument":Landroid/os/Bundle;
    invoke-direct {v6, v7}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 450
    .restart local v6    # "argument":Landroid/os/Bundle;
    const-string v24, "id"

    const/16 v25, 0x23

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 452
    const-string v24, "run_from"

    const/16 v25, 0x15

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 453
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabsAdapter:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabHost:Landroid/widget/TabHost;

    move-object/from16 v25, v0

    const-string v26, "user-shortcut"

    invoke-virtual/range {v25 .. v26}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v25

    const v26, 0x7f0b00d4

    move-object/from16 v0, v22

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v25

    const-class v26, Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2, v6}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->addTab(Landroid/widget/TabHost$TabSpec;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 456
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabWidget:Landroid/widget/TabWidget;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Landroid/widget/TabWidget;->setVisibility(I)V

    .line 460
    :cond_1b
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v24

    if-eqz v24, :cond_1e

    .line 462
    const/16 v24, 0x2

    const-string v25, "SelectorActivity"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "enableDropbox="

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, ", mSelectionType="

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectionType:I

    move/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v24 .. v26}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 465
    const/16 v24, 0x2

    const-string v25, "SelectorActivity"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "isNetworkOn="

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v24 .. v26}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 469
    if-eqz v11, :cond_1d

    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isBaiduCloudAccountIsSignin(Landroid/content/Context;)Z

    move-result v24

    if-eqz v24, :cond_1d

    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v24

    if-eqz v24, :cond_1d

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectionType:I

    move/from16 v24, v0

    const/16 v25, 0x3

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_1d

    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v24

    if-nez v24, :cond_1d

    .line 474
    new-instance v6, Landroid/os/Bundle;

    .end local v6    # "argument":Landroid/os/Bundle;
    invoke-direct {v6, v7}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 476
    .restart local v6    # "argument":Landroid/os/Bundle;
    const-string v24, "id"

    const/16 v25, 0x1f

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 478
    const/16 v24, 0x2

    const-string v25, "SelectorActivity"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "add baidu browser tab mStartFolder="

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mStartFolder:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v24 .. v26}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 481
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mStartFolder:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_1c

    .line 483
    const-string v24, "FOLDERPATH"

    const-string v25, "/"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    :cond_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabsAdapter:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabHost:Landroid/widget/TabHost;

    move-object/from16 v25, v0

    const-string v26, "baidu"

    invoke-virtual/range {v25 .. v26}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v25

    const v26, 0x7f0b00fa

    move-object/from16 v0, v22

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v25

    const-class v26, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2, v6}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->addTab(Landroid/widget/TabHost$TabSpec;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 489
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabWidget:Landroid/widget/TabWidget;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Landroid/widget/TabWidget;->setVisibility(I)V

    .line 515
    :cond_1d
    :goto_4
    if-eqz v12, :cond_a

    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/myfiles/utils/Utils;->getFTPShortCutCount(Landroid/content/Context;)I

    move-result v24

    if-lez v24, :cond_a

    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v24

    if-eqz v24, :cond_a

    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v24

    if-nez v24, :cond_a

    .line 517
    new-instance v6, Landroid/os/Bundle;

    .end local v6    # "argument":Landroid/os/Bundle;
    invoke-direct {v6, v7}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 519
    .restart local v6    # "argument":Landroid/os/Bundle;
    const-string v24, "id"

    const/16 v25, 0x10

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 521
    invoke-static {v6, v8}, Lcom/sec/android/app/myfiles/SelectorActivity;->appendIntentArgs(Landroid/os/Bundle;Landroid/content/Intent;)Landroid/os/Bundle;

    .line 523
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabsAdapter:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabHost:Landroid/widget/TabHost;

    move-object/from16 v25, v0

    const-string v26, "ftp-shortcut"

    invoke-virtual/range {v25 .. v26}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v25

    const v26, 0x7f0b00a4

    move-object/from16 v0, v22

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v25

    const-class v26, Lcom/sec/android/app/myfiles/fragment/FTPShortcutFragment;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2, v6}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->addTab(Landroid/widget/TabHost$TabSpec;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 526
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabWidget:Landroid/widget/TabWidget;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Landroid/widget/TabWidget;->setVisibility(I)V

    goto/16 :goto_2

    .line 495
    :cond_1e
    if-eqz v11, :cond_1d

    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isDropboxAccountIsSignin(Landroid/content/Context;)Z

    move-result v24

    if-eqz v24, :cond_1d

    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v24

    if-eqz v24, :cond_1d

    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v24

    if-nez v24, :cond_1d

    .line 497
    new-instance v6, Landroid/os/Bundle;

    .end local v6    # "argument":Landroid/os/Bundle;
    invoke-direct {v6, v7}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 499
    .restart local v6    # "argument":Landroid/os/Bundle;
    const-string v24, "id"

    const/16 v25, 0x8

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 501
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mStartFolder:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-static/range {v24 .. v24}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_1f

    .line 504
    const-string v24, "FOLDERPATH"

    const-string v25, "/"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 507
    :cond_1f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabsAdapter:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabHost:Landroid/widget/TabHost;

    move-object/from16 v25, v0

    const-string v26, "dropbox"

    invoke-virtual/range {v25 .. v26}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v25

    const v26, 0x7f0b0008

    move-object/from16 v0, v22

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Landroid/widget/TabHost$TabSpec;->setIndicator(Ljava/lang/CharSequence;)Landroid/widget/TabHost$TabSpec;

    move-result-object v25

    const-class v26, Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2, v6}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->addTab(Landroid/widget/TabHost$TabSpec;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 510
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabWidget:Landroid/widget/TabWidget;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Landroid/widget/TabWidget;->setVisibility(I)V

    goto/16 :goto_4

    .line 420
    :array_0
    .array-data 4
        0x101
        0x202
        0x603
        0x604
        0x605
        0x606
        0x607
        0x608
    .end array-data
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 536
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setmMimeTypeForCategory(Ljava/lang/String;)V

    .line 537
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setmExtensionForCategory(Ljava/lang/String;)V

    .line 538
    invoke-super {p0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onDestroy()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 544
    :goto_0
    return-void

    .line 540
    :catch_0
    move-exception v0

    .line 542
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 697
    iget v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mFileOperation:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    const/4 v1, 0x4

    if-ne p1, v1, :cond_1

    iget v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectionType:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectionType:I

    if-ne v1, v0, :cond_1

    .line 700
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->setResult(I)V

    .line 702
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SelectorActivity;->finish()V

    .line 707
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 714
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 725
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 718
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SelectorActivity;->onBackPressed()V

    .line 720
    const/4 v0, 0x1

    goto :goto_0

    .line 714
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 1530
    invoke-super {p0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onPause()V

    .line 1532
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mAudioPlayer:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    if-eqz v1, :cond_0

    .line 1533
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mAudioPlayer:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->stop()V

    .line 1535
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 1537
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1542
    :cond_1
    :goto_0
    return-void

    .line 1538
    :catch_0
    move-exception v0

    .line 1539
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 10
    .param p1, "id"    # I
    .param p2, "dialog"    # Landroid/app/Dialog;

    .prologue
    const/4 v9, 0x0

    const v8, 0x7f0f00a8

    const/4 v7, 0x0

    .line 1863
    invoke-virtual {p2, p0}, Landroid/app/Dialog;->setOwnerActivity(Landroid/app/Activity;)V

    .line 1865
    packed-switch p1, :pswitch_data_0

    .line 1959
    .end local p2    # "dialog":Landroid/app/Dialog;
    :goto_0
    :pswitch_0
    return-void

    .line 1870
    .restart local p2    # "dialog":Landroid/app/Dialog;
    :pswitch_1
    move v2, p1

    .line 1872
    .local v2, "dialogId":I
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 1874
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v6, 0x7f040032

    invoke-virtual {v3, v6, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1875
    .local v1, "customView":Landroid/view/View;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isChinaModel()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1876
    const v6, 0x7f0f00c5

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 1877
    .local v4, "mobileDataWarningDialog":Landroid/widget/TextView;
    const v6, 0x7f0b00e0

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(I)V

    .line 1880
    .end local v4    # "mobileDataWarningDialog":Landroid/widget/TextView;
    :cond_0
    const v6, 0x7f0f00c7

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 1882
    .local v0, "checkDoNotShow":Landroid/widget/CheckBox;
    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 1884
    .local v5, "textDoNotShow":Landroid/widget/TextView;
    new-instance v6, Lcom/sec/android/app/myfiles/SelectorActivity$9;

    invoke-direct {v6, p0, v2, v0}, Lcom/sec/android/app/myfiles/SelectorActivity$9;-><init>(Lcom/sec/android/app/myfiles/SelectorActivity;ILandroid/widget/CheckBox;)V

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1906
    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setSoundEffectsEnabled(Z)V

    .line 1907
    new-instance v6, Lcom/sec/android/app/myfiles/SelectorActivity$10;

    invoke-direct {v6, p0, v0}, Lcom/sec/android/app/myfiles/SelectorActivity$10;-><init>(Lcom/sec/android/app/myfiles/SelectorActivity;Landroid/widget/CheckBox;)V

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1916
    check-cast p2, Landroid/app/AlertDialog;

    .end local p2    # "dialog":Landroid/app/Dialog;
    invoke-virtual {p2, v1}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    goto :goto_0

    .line 1925
    .end local v0    # "checkDoNotShow":Landroid/widget/CheckBox;
    .end local v1    # "customView":Landroid/view/View;
    .end local v2    # "dialogId":I
    .end local v3    # "inflater":Landroid/view/LayoutInflater;
    .end local v5    # "textDoNotShow":Landroid/widget/TextView;
    .restart local p2    # "dialog":Landroid/app/Dialog;
    :pswitch_2
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 1926
    .restart local v3    # "inflater":Landroid/view/LayoutInflater;
    const v6, 0x7f040033

    invoke-virtual {v3, v6, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1927
    .restart local v1    # "customView":Landroid/view/View;
    const v6, 0x7f0f00c9

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 1928
    .restart local v0    # "checkDoNotShow":Landroid/widget/CheckBox;
    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 1930
    .restart local v5    # "textDoNotShow":Landroid/widget/TextView;
    new-instance v6, Lcom/sec/android/app/myfiles/SelectorActivity$11;

    invoke-direct {v6, p0, v0}, Lcom/sec/android/app/myfiles/SelectorActivity$11;-><init>(Lcom/sec/android/app/myfiles/SelectorActivity;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1943
    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setSoundEffectsEnabled(Z)V

    .line 1944
    new-instance v6, Lcom/sec/android/app/myfiles/SelectorActivity$12;

    invoke-direct {v6, p0, v0}, Lcom/sec/android/app/myfiles/SelectorActivity$12;-><init>(Lcom/sec/android/app/myfiles/SelectorActivity;Landroid/widget/CheckBox;)V

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1952
    check-cast p2, Landroid/app/AlertDialog;

    .end local p2    # "dialog":Landroid/app/Dialog;
    invoke-virtual {p2, v1}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    goto :goto_0

    .line 1865
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v2, 0x7f0f0148

    .line 577
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 579
    iget v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mFileOperation:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 581
    iget v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mFileOperation:I

    if-nez v0, :cond_1

    .line 583
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f0b0013

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 591
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 587
    :cond_1
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f0b0014

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 1510
    invoke-super {p0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onResume()V

    .line 1511
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mExternalStorageReceiverFilter:Landroid/content/IntentFilter;

    if-nez v1, :cond_1

    .line 1513
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/SelectorActivity;->setExternalStorageReceiver()V

    .line 1515
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mExternalStorageReceiverFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/myfiles/SelectorActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1517
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v1, :cond_2

    .line 1518
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getmAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-result-object v0

    .line 1519
    .local v0, "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
    if-eqz v0, :cond_2

    .line 1520
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->refreshSelection()V

    .line 1524
    .end local v0    # "adapter":Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SelectorActivity;->invalidateOptionsMenu()V

    .line 1525
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 549
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 551
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabHost:Landroid/widget/TabHost;

    if-eqz v0, :cond_0

    .line 552
    const-string v0, "tab"

    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    :cond_0
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 4
    .param p1, "level"    # I

    .prologue
    .line 1603
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onTrimMemory(I)V

    .line 1605
    const/4 v0, 0x0

    const-string v1, "SelectorActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SelectorActivity : onTrimMemory : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1606
    sparse-switch p1, :sswitch_data_0

    .line 1622
    :sswitch_0
    return-void

    .line 1606
    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0xa -> :sswitch_0
        0xf -> :sswitch_0
        0x14 -> :sswitch_0
        0x28 -> :sswitch_0
        0x3c -> :sswitch_0
        0x50 -> :sswitch_0
    .end sparse-switch
.end method

.method public setDefaultView()V
    .locals 3

    .prologue
    .line 598
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mCurrentTabTag:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 600
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabHost:Landroid/widget/TabHost;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mCurrentTabTag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->setCurrentTabByTag(Ljava/lang/String;)V

    .line 603
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabsAdapter:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

    if-eqz v0, :cond_1

    .line 605
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabsAdapter:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

    const/16 v1, 0x1c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->switchFtpMode(ILandroid/os/Bundle;)V

    .line 639
    :cond_1
    return-void
.end method

.method public setDefaultView(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "b"    # Landroid/os/Bundle;

    .prologue
    const/16 v4, 0x24

    const/16 v3, 0x1a

    .line 645
    if-eqz p1, :cond_0

    const-string v2, "id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "FOLDERPATH"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 647
    const-string v2, "id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 649
    .local v0, "fragmentId":I
    const-string v2, "FOLDERPATH"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 651
    .local v1, "path":Ljava/lang/String;
    if-ltz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 652
    if-eq v0, v3, :cond_1

    if-eq v0, v4, :cond_1

    .line 653
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabsAdapter:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

    invoke-virtual {v2, v0, p1}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->switchFtpMode(ILandroid/os/Bundle;)V

    .line 673
    .end local v0    # "fragmentId":I
    .end local v1    # "path":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 654
    .restart local v0    # "fragmentId":I
    .restart local v1    # "path":Ljava/lang/String;
    :cond_1
    if-ne v0, v4, :cond_2

    .line 655
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabsAdapter:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

    invoke-virtual {v2, v0, p1}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->switchUserShortcutMode(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 656
    :cond_2
    if-ne v0, v3, :cond_0

    .line 657
    const-string v2, "shortcut_type"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v3, :cond_3

    .line 658
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabsAdapter:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

    invoke-virtual {v2, v0, p1}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->switchUserShortcutMode(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 659
    :cond_3
    const-string v2, "shortcut_type"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabMap:Ljava/util/HashMap;

    const-string v3, "dropbox"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 661
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabsAdapter:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabMap:Ljava/util/HashMap;

    const-string v4, "dropbox"

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v3, v2, v1}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->goToCloudShortcut(ILjava/lang/String;)V

    goto :goto_0

    .line 662
    :cond_4
    const-string v2, "shortcut_type"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const/16 v3, 0x1f

    if-ne v2, v3, :cond_5

    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabMap:Ljava/util/HashMap;

    const-string v3, "baidu"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 664
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabsAdapter:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabMap:Ljava/util/HashMap;

    const-string v4, "baidu"

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v3, v2, v1}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->goToBaiduShortcut(ILjava/lang/String;)V

    goto :goto_0

    .line 667
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabMap:Ljava/util/HashMap;

    const-string v3, "ftp-shortcut"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 668
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabsAdapter:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mTabMap:Ljava/util/HashMap;

    const-string v4, "ftp-shortcut"

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v3, v2, v1}, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->goToFtpShortcut(ILjava/lang/String;)V

    goto/16 :goto_0
.end method

.method public setSpinnerWidth(Ljava/lang/String;)V
    .locals 10
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    const v9, 0x7f0b0037

    const v8, 0x7f0b0035

    .line 1963
    iget-object v6, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllButton:Landroid/widget/TextView;

    if-eqz v6, :cond_0

    .line 1965
    iget-object v6, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllButton:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    .line 1967
    .local v3, "paint":Landroid/text/TextPaint;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SelectorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SelectorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-le v6, v7, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SelectorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1969
    .local v2, "opts":Ljava/lang/String;
    :goto_0
    invoke-virtual {v3, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SelectorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0901d8

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SelectorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0901d7

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    add-int v0, v6, v7

    .line 1972
    .local v0, "contentWidth":I
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SelectorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0901cc

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-static {v0, v6}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1974
    .local v1, "dropdownWidth":I
    if-nez p1, :cond_2

    .line 1988
    .end local v0    # "contentWidth":I
    .end local v1    # "dropdownWidth":I
    .end local v2    # "opts":Ljava/lang/String;
    .end local v3    # "paint":Landroid/text/TextPaint;
    :cond_0
    :goto_1
    return-void

    .line 1967
    .restart local v3    # "paint":Landroid/text/TextPaint;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SelectorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1979
    .restart local v0    # "contentWidth":I
    .restart local v1    # "dropdownWidth":I
    .restart local v2    # "opts":Ljava/lang/String;
    :cond_2
    invoke-virtual {v3, p1}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SelectorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0901d0

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SelectorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0901d1

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    add-int v4, v6, v7

    .line 1982
    .local v4, "spinnerWidth":I
    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 1984
    .local v5, "width":I
    iget-object v6, p0, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllButton:Landroid/widget/TextView;

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setWidth(I)V

    goto :goto_1
.end method

.method public final showWarningDialog(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 1638
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/myfiles/SelectorActivity;->showWarningDialog(ILandroid/os/Bundle;)Z

    .line 1639
    return-void
.end method
