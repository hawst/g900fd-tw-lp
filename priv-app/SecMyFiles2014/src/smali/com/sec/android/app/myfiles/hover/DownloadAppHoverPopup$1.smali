.class Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup$1;
.super Ljava/lang/Object;
.source "DownloadAppHoverPopup.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->initViews()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    .line 111
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 113
    const/16 v1, 0xa

    const/4 v2, -0x1

    invoke-static {v1, v2}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    :cond_0
    :goto_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 147
    :cond_1
    :goto_1
    :pswitch_0
    const/4 v1, 0x1

    return v1

    .line 117
    :catch_0
    move-exception v0

    .line 119
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 125
    .end local v0    # "e":Landroid/os/RemoteException;
    :pswitch_1
    const-string v1, "DownloadAppHoverPopup"

    const-string v2, "MotionEvent.ACTION_HOVER_ENTER"

    invoke-static {v4, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    .line 126
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 127
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_1

    .line 137
    :pswitch_2
    const-string v1, "DownloadAppHoverPopup"

    const-string v2, "MotionEvent.ACTION_HOVER_EXIT"

    invoke-static {v4, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    .line 138
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 139
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->removeDelayedMessage()V

    goto :goto_1

    .line 122
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
