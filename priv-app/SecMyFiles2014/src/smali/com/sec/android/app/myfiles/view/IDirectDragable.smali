.class public interface abstract Lcom/sec/android/app/myfiles/view/IDirectDragable;
.super Ljava/lang/Object;
.source "IDirectDragable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/view/IDirectDragable$OnDirectDragListener;
    }
.end annotation


# virtual methods
.method public abstract setDirectDragMode(Z)V
.end method

.method public abstract setOnDirectDragListener(Lcom/sec/android/app/myfiles/view/IDirectDragable$OnDirectDragListener;)V
.end method

.method public abstract triggerLongClick(ILandroid/os/Bundle;)V
.end method
