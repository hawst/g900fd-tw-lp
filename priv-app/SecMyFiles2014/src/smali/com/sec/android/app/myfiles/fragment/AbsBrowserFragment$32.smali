.class Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$32;
.super Ljava/lang/Object;
.source "AbsBrowserFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->showDialogForSoundShot(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

.field final synthetic val$MIMEType:Ljava/lang/String;

.field final synthetic val$path:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 6220
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$32;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iput-object p2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$32;->val$path:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$32;->val$MIMEType:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 6225
    packed-switch p2, :pswitch_data_0

    .line 6265
    :cond_0
    :goto_0
    return-void

    .line 6229
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$32;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    # invokes: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->dismissDialog()V
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$700(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V

    .line 6231
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$32;->val$path:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getAvailableSpace(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0xa

    cmp-long v1, v2, v4

    if-ltz v1, :cond_2

    .line 6233
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$32;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v1, v7}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->showWaitProgressDialog(Z)V

    .line 6235
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$32;->val$MIMEType:Ljava/lang/String;

    const-string v2, "image/golf"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 6237
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$32;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$32;->val$path:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$32;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mConvertHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$800(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Landroid/os/Handler;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/FileUtils;->convertShare(Landroid/content/Context;Ljava/lang/String;Landroid/os/Handler;)Z

    goto :goto_0

    .line 6241
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$32;->val$path:Ljava/lang/String;

    invoke-static {v1}, Lcom/quramsoft/qdio/QdioJNI;->checkAudioInJPEG(Ljava/lang/String;)Lcom/quramsoft/qdio/QdioJNI$QdioJPEGData;

    move-result-object v0

    .line 6243
    .local v0, "qdioData":Lcom/quramsoft/qdio/QdioJNI$QdioJPEGData;
    if-eqz v0, :cond_0

    .line 6246
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$32;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$32;->val$path:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$32;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mConvertHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$800(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Landroid/os/Handler;

    move-result-object v3

    invoke-static {v1, v0, v2, v3}, Lcom/sec/android/app/myfiles/utils/FileUtils;->convertShare(Landroid/content/Context;Lcom/quramsoft/qdio/QdioJNI$QdioJPEGData;Ljava/lang/String;Landroid/os/Handler;)Z

    goto :goto_0

    .line 6251
    .end local v0    # "qdioData":Lcom/quramsoft/qdio/QdioJNI$QdioJPEGData;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$32;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0b0067

    invoke-static {v1, v2, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 6258
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$32;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iput-boolean v7, v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mIsConverting:Z

    .line 6259
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$32;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$32;->val$path:Ljava/lang/String;

    invoke-virtual {v1, v2, v6}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->share(Ljava/lang/String;Z)V

    .line 6261
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$32;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    # invokes: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->dismissDialog()V
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$700(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V

    goto :goto_0

    .line 6225
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
