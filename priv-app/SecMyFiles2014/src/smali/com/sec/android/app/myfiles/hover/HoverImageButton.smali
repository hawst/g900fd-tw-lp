.class public Lcom/sec/android/app/myfiles/hover/HoverImageButton;
.super Landroid/widget/ImageButton;
.source "HoverImageButton.java"


# static fields
.field private static final MODULE:Ljava/lang/String; = "HoverImageButton"


# instance fields
.field private mHoverHandler:Landroid/os/Handler;

.field private mHoverManager:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/HoverImageButton;->mHoverManager:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 57
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/myfiles/hover/HoverImageButton$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/hover/HoverImageButton$1;-><init>(Lcom/sec/android/app/myfiles/hover/HoverImageButton;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/HoverImageButton;->mHoverHandler:Landroid/os/Handler;

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/HoverImageButton;->mHoverManager:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 57
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/myfiles/hover/HoverImageButton$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/hover/HoverImageButton$1;-><init>(Lcom/sec/android/app/myfiles/hover/HoverImageButton;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/HoverImageButton;->mHoverHandler:Landroid/os/Handler;

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/HoverImageButton;->mHoverManager:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 57
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/myfiles/hover/HoverImageButton$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/hover/HoverImageButton$1;-><init>(Lcom/sec/android/app/myfiles/hover/HoverImageButton;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/HoverImageButton;->mHoverHandler:Landroid/os/Handler;

    .line 43
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/hover/HoverImageButton;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/hover/HoverImageButton;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/HoverImageButton;->mHoverManager:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    return-object v0
.end method


# virtual methods
.method public getmHoverManager()Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/HoverImageButton;->mHoverManager:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    return-object v0
.end method

.method public onHoverChanged(Z)V
    .locals 5
    .param p1, "hovered"    # Z

    .prologue
    const/4 v4, 0x0

    .line 47
    invoke-super {p0, p1}, Landroid/widget/ImageButton;->onHoverChanged(Z)V

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/HoverImageButton;->mHoverManager:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    if-eqz v0, :cond_1

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/HoverImageButton;->mHoverManager:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    iput-boolean p1, v0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverOverButton:Z

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/HoverImageButton;->mHoverManager:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverOverButton:Z

    if-nez v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/HoverImageButton;->mHoverHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 55
    :cond_0
    :goto_0
    return-void

    .line 54
    :cond_1
    const-string v0, "HoverImageButton"

    const-string v1, "HoverManager not initialized"

    invoke-static {v4, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setmHoverManager(Lcom/sec/android/app/myfiles/hover/AbsHoverManager;)V
    .locals 0
    .param p1, "mHoverManager"    # Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/android/app/myfiles/hover/HoverImageButton;->mHoverManager:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 28
    return-void
.end method
