.class public Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;
.super Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;
.source "ExtractOperationTask.java"


# instance fields
.field private mCompressManager:Lcom/sec/android/app/myfiles/compression/CompressManager;

.field private mCompressorListener:Lcom/sec/android/app/myfiles/compression/CompressManager$OnCompressorListener;

.field private mIsCancelled:Z

.field private mResultCode:I

.field private mZipType:I


# direct methods
.method public constructor <init>(Landroid/content/Context;IILandroid/view/View;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "operation"    # I
    .param p3, "contentType"    # I
    .param p4, "progressView"    # Landroid/view/View;
    .param p5, "zipType"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;-><init>(Landroid/content/Context;IILandroid/view/View;)V

    .line 107
    new-instance v0, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask$1;-><init>(Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;->mCompressorListener:Lcom/sec/android/app/myfiles/compression/CompressManager$OnCompressorListener;

    .line 45
    new-instance v0, Lcom/sec/android/app/myfiles/compression/CompressManager;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/compression/CompressManager;-><init>(Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;->mCompressManager:Lcom/sec/android/app/myfiles/compression/CompressManager;

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;->mCompressManager:Lcom/sec/android/app/myfiles/compression/CompressManager;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/compression/CompressManager;->setProgressBar(Landroid/widget/ProgressBar;)V

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;->mCompressManager:Lcom/sec/android/app/myfiles/compression/CompressManager;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;->mPercentText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/compression/CompressManager;->setProgressBarPercent(Landroid/widget/TextView;)V

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;->mCompressManager:Lcom/sec/android/app/myfiles/compression/CompressManager;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;->mCountText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/compression/CompressManager;->setProgressBarCounter(Landroid/widget/TextView;)V

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;->mCompressManager:Lcom/sec/android/app/myfiles/compression/CompressManager;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;->mCompressorListener:Lcom/sec/android/app/myfiles/compression/CompressManager$OnCompressorListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/compression/CompressManager;->setOnCompressorListener(Lcom/sec/android/app/myfiles/compression/CompressManager$OnCompressorListener;)V

    .line 55
    iput p5, p0, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;->mZipType:I

    .line 56
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;
    .param p1, "x1"    # I

    .prologue
    .line 31
    iput p1, p0, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;->mResultCode:I

    return p1
.end method

.method static synthetic access$102(Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;
    .param p1, "x1"    # Z

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;->mIsCancelled:Z

    return p1
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;
    .locals 6
    .param p1, "params"    # [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .prologue
    const/4 v4, 0x0

    .line 67
    aget-object v0, p1, v4

    .line 69
    .local v0, "param":Lcom/sec/android/app/myfiles/element/FileOperationParam;
    iget-object v1, v0, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    .line 71
    iget-object v1, v0, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;->mTargetFolder:Ljava/lang/String;

    .line 73
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 74
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;->mCompressManager:Lcom/sec/android/app/myfiles/compression/CompressManager;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;->mTargetFolder:Ljava/lang/String;

    iget v5, p0, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;->mZipType:I

    invoke-virtual {v2, v3, v1, v4, v5}, Lcom/sec/android/app/myfiles/compression/CompressManager;->doExtract(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 77
    :cond_0
    const/4 v1, 0x0

    return-object v1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 31
    check-cast p1, [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;->doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V
    .locals 3
    .param p1, "result"    # Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .prologue
    const/4 v2, 0x0

    .line 83
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V

    .line 85
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;->mIsCancelled:Z

    if-eqz v0, :cond_1

    .line 87
    iget v0, p0, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;->mResultCode:I

    packed-switch v0, :pswitch_data_0

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 91
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;->mContext:Landroid/content/Context;

    const v1, 0x7f0b0103

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 97
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;->mContext:Landroid/content/Context;

    const v1, 0x7f0b00ec

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 101
    :cond_1
    iget v0, p0, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;->mResultCode:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 103
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/FileUtils;->setExtractSuccess(Z)V

    goto :goto_0

    .line 87
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 31
    check-cast p1, Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;->onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V

    return-void
.end method

.method protected setupProgressView()V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 62
    return-void
.end method
