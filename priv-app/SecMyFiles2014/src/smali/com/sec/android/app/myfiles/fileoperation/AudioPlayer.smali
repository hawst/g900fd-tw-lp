.class public Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;
.super Landroid/media/MediaPlayer;
.source "AudioPlayer.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayErrorListener;,
        Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayCompletionListener;,
        Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayListener;
    }
.end annotation


# static fields
.field public static final LIST_PLAYER_COMPLETED:Z = true

.field public static final LIST_PLAYER_ERROR:I = 0x1

.field public static final LIST_PLAYER_ERROR_CHECK_VOLUME:I = 0x4

.field public static final LIST_PLAYER_ERROR_NOT_FILE:I = 0x2

.field public static final LIST_PLAYER_FADE_IN:I = 0x5

.field private static final LIST_PLAYER_FADE_IN_TIMEOUT:I = 0x14

.field public static final LIST_PLAYER_NOT_COMPLETED:Z = false

.field public static final LIST_PLAYER_NOT_EXECUTABLE:I = 0x3

.field public static final LIST_PLAYER_OK:I = 0x0

.field static final MONITORING_INTERVAL_BASIC:I = 0x1f4

.field static final MONITORING_INTERVAL_LONG:I = 0x3e8

.field static final MONITORING_INTERVAL_SHORT:I = 0x12c

.field private static final TAG:Ljava/lang/String; = "AudioPlayer"

.field private static final _instance:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;


# instance fields
.field private isExecutable:Z

.field private isPreparing:Z

.field private mAudioManager:Landroid/media/AudioManager;

.field private mCompleteFlag:Z

.field private mCurrentVolume:F

.field private mFileName:Ljava/lang/String;

.field private mFilepath:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field private mListPlayerHandler:Landroid/os/Handler;

.field private mMonitoring:Ljava/lang/Runnable;

.field private mOnCompletionListener:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayCompletionListener;

.field private mOnListPlayErrorListener:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayErrorListener;

.field private mOnListPlayListener:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayListener;

.field private mPosition:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    new-instance v0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->_instance:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 97
    invoke-direct {p0}, Landroid/media/MediaPlayer;-><init>()V

    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->isExecutable:Z

    .line 68
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mAudioManager:Landroid/media/AudioManager;

    .line 70
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mOnListPlayListener:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayListener;

    .line 72
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mOnCompletionListener:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayCompletionListener;

    .line 74
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mOnListPlayErrorListener:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayErrorListener;

    .line 80
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->isPreparing:Z

    .line 82
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mCurrentVolume:F

    .line 84
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mHandler:Landroid/os/Handler;

    .line 86
    new-instance v0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$1;-><init>(Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mMonitoring:Ljava/lang/Runnable;

    .line 272
    new-instance v0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$2;-><init>(Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mListPlayerHandler:Landroid/os/Handler;

    .line 98
    invoke-virtual {p0, p0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 99
    invoke-virtual {p0, p0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 100
    invoke-virtual {p0, p0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 101
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->checkListPlayerAndUpdateView()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mCompleteFlag:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    .prologue
    .line 31
    iget v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mCurrentVolume:F

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;
    .param p1, "x1"    # F

    .prologue
    .line 31
    iput p1, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mCurrentVolume:F

    return p1
.end method

.method static synthetic access$316(Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;F)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;
    .param p1, "x1"    # F

    .prologue
    .line 31
    iget v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mCurrentVolume:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mCurrentVolume:F

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mListPlayerHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private checkListPlayerAndUpdateView()V
    .locals 4

    .prologue
    .line 238
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->isExecutable:Z

    if-nez v2, :cond_1

    .line 239
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->stop()V

    .line 253
    :cond_0
    :goto_0
    return-void

    .line 242
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 243
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->getCurrentPosition()I

    move-result v1

    .line 244
    .local v1, "position":I
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->getDuration()I

    move-result v0

    .line 245
    .local v0, "duration":I
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mOnListPlayListener:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayListener;

    if-eqz v2, :cond_0

    .line 246
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mOnListPlayListener:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayListener;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mFileName:Ljava/lang/String;

    invoke-interface {v2, v3, v1, v0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayListener;->onListPlaying(Ljava/lang/String;II)V

    goto :goto_0

    .line 248
    .end local v0    # "duration":I
    .end local v1    # "position":I
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->isComplete()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 249
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mOnCompletionListener:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayCompletionListener;

    if-eqz v2, :cond_0

    .line 250
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mOnCompletionListener:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayCompletionListener;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mFileName:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayCompletionListener;->onCompletion(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getInstance()Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;
    .locals 1

    .prologue
    .line 104
    sget-object v0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->_instance:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    return-object v0
.end method

.method private setVolume()V
    .locals 1

    .prologue
    const/high16 v0, 0x40400000    # 3.0f

    .line 263
    invoke-virtual {p0, v0, v0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->setVolume(FF)V

    .line 264
    return-void
.end method

.method private startAndFadeIn()V
    .locals 4

    .prologue
    .line 267
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mCurrentVolume:F

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mListPlayerHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    const-wide/16 v2, 0x14

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 270
    return-void
.end method


# virtual methods
.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mFileName:Ljava/lang/String;

    return-object v0
.end method

.method public isComplete()Z
    .locals 1

    .prologue
    .line 172
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mCompleteFlag:Z

    return v0
.end method

.method public onAudioFocusChange(I)V
    .locals 3
    .param p1, "focusChange"    # I

    .prologue
    .line 341
    packed-switch p1, :pswitch_data_0

    .line 356
    const-string v0, "AudioPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown audio focus change code,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    :cond_0
    :goto_0
    return-void

    .line 343
    :pswitch_0
    const-string v0, "AudioPlayer"

    const-string v1, "AudioFocus: received AUDIOFOCUS_LOSS"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 345
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->stop()V

    goto :goto_0

    .line 350
    :pswitch_1
    const-string v0, "AudioPlayer"

    const-string v1, "AudioFocus: received AUDIOFOCUS_LOSS_TRANSIENT"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 352
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->stop()V

    goto :goto_0

    .line 341
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 177
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mOnCompletionListener:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayCompletionListener;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mOnCompletionListener:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayCompletionListener;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mFileName:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayCompletionListener;->onCompletion(Ljava/lang/String;)V

    .line 179
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mCompleteFlag:Z

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_1

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 183
    :cond_1
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 4
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    const/4 v3, 0x1

    .line 322
    const-string v0, "AudioPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "OnError what="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", extra="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", isPreparing="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->isPreparing:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->isPreparing:Z

    if-eqz v0, :cond_0

    .line 326
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->onPrepared(Landroid/media/MediaPlayer;)V

    .line 327
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->stop()V

    .line 328
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mFileName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mOnListPlayErrorListener:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayErrorListener;

    if-eqz v0, :cond_0

    .line 329
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mOnListPlayErrorListener:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayErrorListener;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mFileName:Ljava/lang/String;

    invoke-interface {v0, v1, v3}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayErrorListener;->onListPlayError(Ljava/lang/String;I)V

    .line 331
    :cond_0
    if-ne p2, v3, :cond_1

    .line 332
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->stop()V

    .line 333
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mFileName:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mOnListPlayErrorListener:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayErrorListener;

    if-eqz v0, :cond_1

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mOnListPlayErrorListener:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayErrorListener;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mFileName:Ljava/lang/String;

    invoke-interface {v0, v1, v3}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayErrorListener;->onListPlayError(Ljava/lang/String;I)V

    .line 336
    :cond_1
    return v3
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 3
    .param p1, "mediaPlayer"    # Landroid/media/MediaPlayer;

    .prologue
    .line 301
    iget v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mPosition:I

    if-eqz v0, :cond_0

    .line 302
    iget v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mPosition:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->seekTo(I)V

    .line 303
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mFilepath:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/sec/android/app/myfiles/MediaFile;->isMIDFileType(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 304
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->setVolume()V

    .line 306
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x3

    const/4 v2, 0x2

    invoke-virtual {v0, p0, v1, v2}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 309
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 310
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->startAndFadeIn()V

    .line 315
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mMonitoring:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 317
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->isPreparing:Z

    .line 318
    return-void

    .line 312
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->start()V

    goto :goto_0
.end method

.method public declared-synchronized play(Ljava/lang/String;)I
    .locals 4
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 186
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->isExecutable:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    .line 187
    const/4 v1, 0x3

    .line 208
    :cond_0
    :goto_0
    monitor-exit p0

    return v1

    .line 189
    :cond_1
    :try_start_1
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->isPreparing:Z

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->isPlaying()Z

    move-result v2

    if-nez v2, :cond_0

    .line 193
    :cond_2
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->isPreparing:Z

    .line 195
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->stop()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 196
    const/4 v1, 0x0

    .line 198
    .local v1, "result":I
    const/4 v2, 0x0

    :try_start_2
    invoke-virtual {p0, p1, v2}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->play(Ljava/lang/String;I)I

    move-result v1

    .line 199
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mOnListPlayErrorListener:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayErrorListener;

    if-eqz v2, :cond_0

    .line 200
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mOnListPlayErrorListener:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayErrorListener;

    invoke-interface {v2, p1}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayErrorListener;->onListPlayError(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 201
    :catch_0
    move-exception v0

    .line 202
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 203
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mOnListPlayErrorListener:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayErrorListener;

    if-eqz v2, :cond_0

    .line 204
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mOnListPlayErrorListener:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayErrorListener;

    invoke-interface {v2, p1}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayErrorListener;->onListPlayError(Ljava/lang/String;)V

    .line 205
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mOnListPlayErrorListener:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayErrorListener;

    const/4 v3, 0x1

    invoke-interface {v2, p1, v3}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayErrorListener;->onListPlayError(Ljava/lang/String;I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 186
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "result":I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public play(Ljava/lang/String;I)I
    .locals 4
    .param p1, "filename"    # Ljava/lang/String;
    .param p2, "position"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 119
    iput p2, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mPosition:I

    .line 120
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mFilepath:Ljava/lang/String;

    .line 121
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mFileName:Ljava/lang/String;

    .line 122
    .local v1, "stopFile":Ljava/lang/String;
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->isExecutable:Z

    if-nez v3, :cond_1

    .line 123
    const/4 v2, 0x3

    .line 141
    :cond_0
    :goto_0
    return v2

    .line 124
    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 125
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v3

    if-nez v3, :cond_2

    .line 126
    const/4 v2, 0x2

    goto :goto_0

    .line 128
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->isPlaying()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 129
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mFileName:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 132
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->stop(Ljava/lang/String;)V

    .line 135
    :cond_3
    iput-boolean v2, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mCompleteFlag:Z

    .line 136
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->reset()V

    .line 137
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->setDataSource(Ljava/lang/String;)V

    .line 138
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mFileName:Ljava/lang/String;

    .line 140
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->prepareAsync()V

    goto :goto_0
.end method

.method public setAudioManager(Landroid/media/AudioManager;)V
    .locals 0
    .param p1, "audioManager"    # Landroid/media/AudioManager;

    .prologue
    .line 259
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mAudioManager:Landroid/media/AudioManager;

    .line 260
    return-void
.end method

.method public setExecutable(Z)V
    .locals 0
    .param p1, "executable"    # Z

    .prologue
    .line 108
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->isExecutable:Z

    .line 109
    if-nez p1, :cond_0

    .line 110
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->stop()V

    .line 112
    :cond_0
    return-void
.end method

.method public setOnListPlayCompletionListener(Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayCompletionListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayCompletionListener;

    .prologue
    .line 217
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mOnCompletionListener:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayCompletionListener;

    .line 218
    return-void
.end method

.method public setOnListPlayErrorListener(Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayErrorListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayErrorListener;

    .prologue
    .line 221
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mOnListPlayErrorListener:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayErrorListener;

    .line 222
    return-void
.end method

.method public setOnListPlayListener(Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayListener;

    .prologue
    .line 213
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mOnListPlayListener:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayListener;

    .line 214
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mFileName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mListPlayerHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mFileName:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->stop(Ljava/lang/String;)V

    .line 157
    :cond_0
    return-void
.end method

.method public stop(Ljava/lang/String;)V
    .locals 1
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    invoke-super {p0}, Landroid/media/MediaPlayer;->stop()V

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mOnCompletionListener:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayCompletionListener;

    if-eqz v0, :cond_1

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mOnCompletionListener:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayCompletionListener;

    invoke-interface {v0, p1}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$OnListPlayCompletionListener;->onCompletion(Ljava/lang/String;)V

    .line 168
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mCompleteFlag:Z

    .line 169
    return-void
.end method
