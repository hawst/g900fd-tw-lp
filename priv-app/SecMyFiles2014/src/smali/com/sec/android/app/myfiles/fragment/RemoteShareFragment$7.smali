.class Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$7;
.super Ljava/lang/Object;
.source "RemoteShareFragment.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)V
    .locals 0

    .prologue
    .line 752
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPathSelected(ILjava/lang/String;)V
    .locals 4
    .param p1, "targetFolderID"    # I
    .param p2, "targetFolderPath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 756
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 757
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->finishSelectMode()V

    .line 759
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    # invokes: Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->backToRealViewMode()V
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->access$500(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)V

    .line 760
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mRemoteShareRoot:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    const/4 v1, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 761
    :cond_1
    const-string v0, "RemoteShareFragment"

    const-string v1, "Can\'t go to the folder"

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 762
    const/4 v0, 0x2

    const-string v1, "RemoteShareFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "=> path = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 764
    :cond_2
    return-void
.end method
