.class public Lcom/sec/android/app/myfiles/fragment/DetailFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;
.source "DetailFragment.java"


# static fields
.field private static final MODULE:Ljava/lang/String; = "DetailFragment"


# instance fields
.field paths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;-><init>()V

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DetailFragment;->paths:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public getFileLastModifiedTime(Ljava/lang/String;I)J
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 73
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 75
    .local v0, "file":Ljava/io/File;
    if-eqz v0, :cond_0

    .line 76
    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    .line 79
    :goto_0
    return-wide v2

    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public getFileName(Ljava/lang/String;I)Ljava/lang/String;
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 32
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 34
    .local v0, "file":Ljava/io/File;
    if-eqz v0, :cond_0

    .line 35
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 38
    :goto_0
    return-object v1

    .line 37
    :cond_0
    const/4 v1, 0x0

    const-string v2, "DetailFragment"

    const-string v3, "file is null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 38
    const-string v1, ""

    goto :goto_0
.end method

.method public getFileSize(Ljava/lang/String;I)J
    .locals 2
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 61
    if-nez p1, :cond_0

    .line 63
    const-wide/16 v0, 0x0

    .line 68
    :goto_0
    return-wide v0

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DetailFragment;->paths:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 68
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->getTotalSize(Ljava/io/File;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 8

    .prologue
    .line 43
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/DetailFragment;->paths:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 44
    .local v2, "str":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 45
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_2

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/DetailFragment;->dismiss()V

    .line 55
    .end local v0    # "file":Ljava/io/File;
    .end local v2    # "str":Ljava/lang/String;
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->onResume()V

    .line 56
    return-void

    .line 49
    .restart local v0    # "file":Ljava/io/File;
    .restart local v2    # "str":Ljava/lang/String;
    :cond_2
    iget-wide v4, p0, Lcom/sec/android/app/myfiles/fragment/DetailFragment;->lastModifiedTime:J

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/myfiles/fragment/DetailFragment;->getFileLastModifiedTime(Ljava/lang/String;I)J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    .line 50
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/DetailFragment;->dismiss()V

    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/DetailFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->finishSelectMode()V

    goto :goto_0
.end method
