.class public Lcom/sec/android/app/myfiles/SortCursor$FileComparator;
.super Ljava/lang/Object;
.source "SortCursor.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/SortCursor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FileComparator"
.end annotation


# instance fields
.field private entryLocaleName1:Ljava/lang/String;

.field private entryLocaleName2:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/myfiles/SortCursor$FileComparator;->entryLocaleName1:Ljava/lang/String;

    .line 85
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/myfiles/SortCursor$FileComparator;->entryLocaleName2:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 10
    .param p1, "arg0"    # Ljava/lang/Object;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, -0x1

    .line 89
    move-object v1, p1

    check-cast v1, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;

    .local v1, "entry1":Lcom/sec/android/app/myfiles/SortCursor$SortEntry;
    move-object v2, p2

    .line 90
    check-cast v2, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;

    .line 91
    .local v2, "entry2":Lcom/sec/android/app/myfiles/SortCursor$SortEntry;
    const/4 v0, 0x0

    .line 92
    .local v0, "compare":I
    # getter for: Lcom/sec/android/app/myfiles/SortCursor;->sort_type:I
    invoke-static {}, Lcom/sec/android/app/myfiles/SortCursor;->access$000()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 149
    iget-object v3, v1, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->name:Ljava/lang/String;

    iget-object v4, v2, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->name:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/myfiles/SortCursor$FileComparator;->compareName(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 151
    :cond_0
    :goto_0
    return v3

    .line 94
    :pswitch_0
    # getter for: Lcom/sec/android/app/myfiles/SortCursor;->inOrder:I
    invoke-static {}, Lcom/sec/android/app/myfiles/SortCursor;->access$100()I

    move-result v6

    if-nez v6, :cond_5

    .line 95
    iget-boolean v6, v1, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->isDirectory:Z

    if-eqz v6, :cond_1

    iget-boolean v6, v2, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->isDirectory:Z

    if-eqz v6, :cond_0

    .line 97
    :cond_1
    iget-boolean v6, v1, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->isDirectory:Z

    if-nez v6, :cond_2

    iget-boolean v6, v2, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->isDirectory:Z

    if-eqz v6, :cond_2

    move v3, v4

    .line 98
    goto :goto_0

    .line 100
    :cond_2
    iget-wide v6, v1, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->time:J

    iget-wide v8, v2, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->time:J

    cmp-long v6, v6, v8

    if-nez v6, :cond_3

    move v0, v5

    .line 101
    :goto_1
    if-nez v0, :cond_15

    .line 102
    iget-object v3, v1, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->name:Ljava/lang/String;

    iget-object v4, v2, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->name:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/myfiles/SortCursor$FileComparator;->compareName(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    goto :goto_0

    .line 100
    :cond_3
    iget-wide v6, v1, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->time:J

    iget-wide v8, v2, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->time:J

    cmp-long v5, v6, v8

    if-lez v5, :cond_4

    move v0, v4

    goto :goto_1

    :cond_4
    move v0, v3

    goto :goto_1

    .line 106
    :cond_5
    iget-boolean v6, v1, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->isDirectory:Z

    if-eqz v6, :cond_6

    iget-boolean v6, v2, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->isDirectory:Z

    if-nez v6, :cond_6

    move v3, v4

    .line 107
    goto :goto_0

    .line 108
    :cond_6
    iget-boolean v6, v1, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->isDirectory:Z

    if-nez v6, :cond_7

    iget-boolean v6, v2, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->isDirectory:Z

    if-nez v6, :cond_0

    .line 111
    :cond_7
    iget-wide v6, v1, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->time:J

    iget-wide v8, v2, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->time:J

    cmp-long v6, v6, v8

    if-nez v6, :cond_8

    move v0, v5

    .line 112
    :goto_2
    if-nez v0, :cond_15

    .line 113
    iget-object v3, v1, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->name:Ljava/lang/String;

    iget-object v4, v2, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->name:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/myfiles/SortCursor$FileComparator;->compareName(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    goto :goto_0

    .line 111
    :cond_8
    iget-wide v6, v1, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->time:J

    iget-wide v8, v2, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->time:J

    cmp-long v5, v6, v8

    if-lez v5, :cond_9

    move v0, v3

    goto :goto_2

    :cond_9
    move v0, v4

    goto :goto_2

    .line 118
    :pswitch_1
    # getter for: Lcom/sec/android/app/myfiles/SortCursor;->inOrder:I
    invoke-static {}, Lcom/sec/android/app/myfiles/SortCursor;->access$100()I

    move-result v6

    if-nez v6, :cond_c

    .line 119
    iget v6, v1, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->type:I

    iget v7, v2, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->type:I

    if-ne v6, v7, :cond_a

    move v0, v5

    .line 120
    :goto_3
    if-nez v0, :cond_15

    .line 121
    iget-object v3, v1, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->name:Ljava/lang/String;

    iget-object v4, v2, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->name:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/myfiles/SortCursor$FileComparator;->compareName(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_0

    .line 119
    :cond_a
    iget v5, v1, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->type:I

    iget v6, v2, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->type:I

    if-le v5, v6, :cond_b

    move v0, v4

    goto :goto_3

    :cond_b
    move v0, v3

    goto :goto_3

    .line 123
    :cond_c
    iget v6, v1, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->type:I

    iget v7, v2, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->type:I

    if-ne v6, v7, :cond_d

    move v0, v5

    .line 124
    :goto_4
    if-nez v0, :cond_15

    .line 125
    iget-object v3, v1, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->name:Ljava/lang/String;

    iget-object v4, v2, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->name:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/myfiles/SortCursor$FileComparator;->compareName(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_0

    .line 123
    :cond_d
    iget v5, v1, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->type:I

    iget v6, v2, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->type:I

    if-le v5, v6, :cond_e

    move v0, v3

    goto :goto_4

    :cond_e
    move v0, v4

    goto :goto_4

    .line 129
    :pswitch_2
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v3

    const-string v4, "CscFeature_MyFiles_EnablePinyinSort"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 130
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->getIntance()Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;

    move-result-object v3

    iget-object v4, v1, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->getSortKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/myfiles/SortCursor$FileComparator;->entryLocaleName1:Ljava/lang/String;

    .line 131
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->getIntance()Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;

    move-result-object v3

    iget-object v4, v2, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->getSortKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/myfiles/SortCursor$FileComparator;->entryLocaleName2:Ljava/lang/String;

    .line 132
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SortCursor$FileComparator;->entryLocaleName1:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/SortCursor$FileComparator;->entryLocaleName2:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/myfiles/SortCursor$FileComparator;->compareName(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_0

    .line 134
    :cond_f
    iget-object v3, v1, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->name:Ljava/lang/String;

    iget-object v4, v2, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->name:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/myfiles/SortCursor$FileComparator;->compareName(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_0

    .line 138
    :pswitch_3
    # getter for: Lcom/sec/android/app/myfiles/SortCursor;->inOrder:I
    invoke-static {}, Lcom/sec/android/app/myfiles/SortCursor;->access$100()I

    move-result v6

    if-nez v6, :cond_12

    .line 139
    iget-wide v6, v1, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->size:J

    iget-wide v8, v2, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->size:J

    cmp-long v6, v6, v8

    if-nez v6, :cond_10

    move v0, v5

    .line 140
    :goto_5
    if-nez v0, :cond_15

    .line 141
    iget-object v3, v1, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->name:Ljava/lang/String;

    iget-object v4, v2, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->name:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/myfiles/SortCursor$FileComparator;->compareName(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_0

    .line 139
    :cond_10
    iget-wide v6, v1, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->size:J

    iget-wide v8, v2, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->size:J

    cmp-long v5, v6, v8

    if-lez v5, :cond_11

    move v0, v4

    goto :goto_5

    :cond_11
    move v0, v3

    goto :goto_5

    .line 143
    :cond_12
    iget-wide v6, v1, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->size:J

    iget-wide v8, v2, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->size:J

    cmp-long v6, v6, v8

    if-nez v6, :cond_13

    move v0, v5

    .line 144
    :goto_6
    if-nez v0, :cond_15

    .line 145
    iget-object v3, v1, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->name:Ljava/lang/String;

    iget-object v4, v2, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->name:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/myfiles/SortCursor$FileComparator;->compareName(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_0

    .line 143
    :cond_13
    iget-wide v6, v1, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->size:J

    iget-wide v8, v2, Lcom/sec/android/app/myfiles/SortCursor$SortEntry;->size:J

    cmp-long v5, v6, v8

    if-lez v5, :cond_14

    move v0, v3

    goto :goto_6

    :cond_14
    move v0, v4

    goto :goto_6

    :cond_15
    move v3, v0

    .line 151
    goto/16 :goto_0

    .line 92
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public compareName(Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p1, "entry1"    # Ljava/lang/String;
    .param p2, "entry2"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 155
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    .line 156
    .local v0, "collator":Ljava/text/Collator;
    invoke-virtual {v0, v2}, Ljava/text/Collator;->setStrength(I)V

    .line 157
    # getter for: Lcom/sec/android/app/myfiles/SortCursor;->inOrder:I
    invoke-static {}, Lcom/sec/android/app/myfiles/SortCursor;->access$100()I

    move-result v1

    if-nez v1, :cond_0

    .line 158
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 162
    :goto_0
    return v1

    .line 159
    :cond_0
    # getter for: Lcom/sec/android/app/myfiles/SortCursor;->inOrder:I
    invoke-static {}, Lcom/sec/android/app/myfiles/SortCursor;->access$100()I

    move-result v1

    if-ne v1, v2, :cond_1

    .line 160
    invoke-virtual {p2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    .line 162
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
