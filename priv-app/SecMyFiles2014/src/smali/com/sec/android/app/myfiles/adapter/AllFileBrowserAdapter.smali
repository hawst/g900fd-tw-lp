.class public Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;
.super Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
.source "AllFileBrowserAdapter.java"


# static fields
.field private static final HOVER_DETECT_MS_FOLDER:I = 0x12c

.field private static final INVALID_ENTER_TIME:I = -0x1

.field private static final MODULE:Ljava/lang/String; = "AllFileBrowserAdapter"


# instance fields
.field private mAudioHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

.field private mContext:Landroid/content/Context;

.field private mDocumentHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

.field private mFolderHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

.field private mFolderHoverCursor:Landroid/database/Cursor;

.field public mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

.field private mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

.field private mLastHoverEnterEventTime:J

.field private mLastHoverEnteredView:Landroid/view/View;

.field private mLastMotionEvent:Landroid/view/MotionEvent;

.field private mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

.field private mSelectedItemBackground:I

.field private mVideoHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "c"    # Landroid/database/Cursor;
    .param p4, "flags"    # I
    .param p5, "navigation"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .prologue
    const/4 v3, 0x0

    .line 91
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 66
    const/4 v0, 0x2

    const/16 v1, 0x55

    const/16 v2, 0x6a

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mSelectedItemBackground:I

    .line 71
    iput-object v3, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 73
    iput-object v3, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mDocumentHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 75
    iput-object v3, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mVideoHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 77
    iput-object v3, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mAudioHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 79
    iput-object v3, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mFolderHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 81
    iput-object v3, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mFolderHoverCursor:Landroid/database/Cursor;

    .line 83
    iput-object v3, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 86
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mLastHoverEnterEventTime:J

    .line 93
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mContext:Landroid/content/Context;

    .line 95
    iput-object p5, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mContext:Landroid/content/Context;

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-static {v0, v1}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    new-instance v0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 102
    new-instance v0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mVideoHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 104
    new-instance v0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mAudioHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 106
    new-instance v0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mFolderHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 108
    new-instance v0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mDocumentHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 110
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    .prologue
    .line 58
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mSelectedItemBackground:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mLastHoverEnteredView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mLastHoverEnteredView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mVideoHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mAudioHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mFolderHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mDocumentHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Landroid/database/Cursor;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mFolderHoverCursor:Landroid/database/Cursor;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mFolderHoverCursor:Landroid/database/Cursor;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mLastHoverEnterEventTime:J

    return-wide v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;
    .param p1, "x1"    # J

    .prologue
    .line 58
    iput-wide p1, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mLastHoverEnterEventTime:J

    return-wide p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;)Landroid/view/MotionEvent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mLastMotionEvent:Landroid/view/MotionEvent;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;
    .param p1, "x1"    # Landroid/view/MotionEvent;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mLastMotionEvent:Landroid/view/MotionEvent;

    return-object p1
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 26
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 116
    const-string v5, "_data"

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 118
    .local v10, "filePath":Ljava/lang/String;
    new-instance v12, Ljava/io/File;

    invoke-direct {v12, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 120
    .local v12, "file":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_1

    .line 691
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    invoke-virtual {v12}, Ljava/io/File;->lastModified()J

    move-result-wide v16

    .line 125
    .local v16, "lastmodified":J
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;

    .line 127
    .local v20, "vh":Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;
    if-eqz v20, :cond_0

    .line 132
    invoke-super/range {p0 .. p3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 134
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    if-eqz v5, :cond_2

    .line 137
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    const/16 v21, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 140
    :cond_2
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v5, :cond_3

    .line 142
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    const/16 v21, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 143
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    const/16 v21, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 146
    :cond_3
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const/16 v21, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 147
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const/16 v21, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 149
    const/16 v5, 0x2f

    invoke-virtual {v10, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v10, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    .line 151
    .local v13, "fileName":Ljava/lang/String;
    const-string v5, "format"

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 154
    .local v14, "format":I
    const/16 v5, 0x3001

    if-ne v14, v5, :cond_16

    .line 156
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getVisibility()I

    move-result v5

    const/16 v21, 0x8

    move/from16 v0, v21

    if-ne v5, v0, :cond_4

    .line 158
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 161
    :cond_4
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mHasThumbnail:Z

    .line 164
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mSelectModeFrom:I

    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v5, v0, :cond_5

    .line 166
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mSelectionType:I

    if-nez v5, :cond_a

    .line 168
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectRadioButton:Landroid/widget/RadioButton;

    const/16 v21, 0x8

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 177
    :cond_5
    :goto_1
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 179
    invoke-static {v10}, Lcom/sec/android/app/myfiles/utils/Utils;->isInternalRootFolder(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 181
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    const v21, 0x7f0b003e

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(I)V

    .line 183
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const v21, 0x7f02005d

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 287
    :cond_6
    :goto_2
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mViewMode:I

    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v5, v0, :cond_22

    .line 292
    const/16 v5, 0x3001

    if-ne v14, v5, :cond_21

    .line 294
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    .line 303
    .local v19, "sb":Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v21, 0x7f0c0002

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-static {v0, v10}, Lcom/sec/android/app/myfiles/utils/Utils;->getChildFileCount(Landroid/content/Context;Ljava/lang/String;)I

    move-result v22

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-static {v0, v10}, Lcom/sec/android/app/myfiles/utils/Utils;->getChildFileCount(Landroid/content/Context;Ljava/lang/String;)I

    move-result v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v23, v24

    move/from16 v0, v21

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v5, v0, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 305
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 313
    .end local v19    # "sb":Ljava/lang/StringBuilder;
    .local v11, "content":Ljava/lang/String;
    :goto_3
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    if-eqz v5, :cond_7

    .line 315
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    invoke-virtual {v5, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 323
    :cond_7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mContext:Landroid/content/Context;

    const-wide/16 v22, 0x3e8

    mul-long v22, v22, v16

    move-wide/from16 v0, v22

    invoke-static {v5, v0, v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v18

    .line 325
    .local v18, "lastmodifiedtime":Ljava/lang/String;
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    if-eqz v5, :cond_8

    .line 327
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 343
    .end local v11    # "content":Ljava/lang/String;
    .end local v18    # "lastmodifiedtime":Ljava/lang/String;
    :cond_8
    :goto_4
    new-instance v5, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$1;

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v14, v10}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$1;-><init>(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;ILjava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/view/View;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 483
    move-object/from16 v0, p2

    invoke-static {v10, v0}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;Landroid/content/Context;)I

    move-result v8

    .line 484
    .local v8, "fileType":I
    move-object v7, v10

    .line 485
    .local v7, "hoverFilePath":Ljava/lang/String;
    const/16 v5, 0x3001

    if-ne v14, v5, :cond_23

    const/4 v6, 0x1

    .line 486
    .local v6, "bFolderFormat":Z
    :goto_5
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v9

    .line 488
    .local v9, "filePosition":I
    new-instance v4, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;

    move-object/from16 v5, p0

    invoke-direct/range {v4 .. v10}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter$2;-><init>(Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;ZLjava/lang/String;IILjava/lang/String;)V

    .line 662
    .local v4, "hoverListener":Landroid/view/View$OnHoverListener;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mContext:Landroid/content/Context;

    const-string v21, "com.sec.feature.hovering_ui"

    move-object/from16 v0, v21

    invoke-static {v5, v0}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 664
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mSelectModeFrom:I

    const/16 v21, 0x2

    move/from16 v0, v21

    if-eq v5, v0, :cond_0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mSelectModeFrom:I

    const/16 v21, 0x1

    move/from16 v0, v21

    if-eq v5, v0, :cond_0

    .line 667
    invoke-static {v10}, Lcom/sec/android/app/myfiles/utils/Utils;->isInternalRootFolder(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_25

    invoke-static {v10}, Lcom/sec/android/app/myfiles/utils/Utils;->isExternalRootFolder(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_25

    invoke-static {v10}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxRootFolder(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_25

    invoke-static {v10}, Lcom/sec/android/app/myfiles/utils/Utils;->isUsbStorageRootFolder(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_25

    .line 672
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mViewMode:I

    if-eqz v5, :cond_9

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mViewMode:I

    const/16 v21, 0x1

    move/from16 v0, v21

    if-eq v5, v0, :cond_9

    if-eqz v6, :cond_24

    .line 674
    :cond_9
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    goto/16 :goto_0

    .line 170
    .end local v4    # "hoverListener":Landroid/view/View$OnHoverListener;
    .end local v6    # "bFolderFormat":Z
    .end local v7    # "hoverFilePath":Ljava/lang/String;
    .end local v8    # "fileType":I
    .end local v9    # "filePosition":I
    :cond_a
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mSelectionType:I

    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v5, v0, :cond_5

    .line 172
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    const/16 v21, 0x8

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto/16 :goto_1

    .line 185
    :cond_b
    invoke-static {v10}, Lcom/sec/android/app/myfiles/utils/Utils;->isExternalRootFolder(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 187
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    const v21, 0x7f0b003f

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(I)V

    .line 189
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const v21, 0x7f02005f

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_2

    .line 191
    :cond_c
    invoke-static {v10}, Lcom/sec/android/app/myfiles/utils/Utils;->isUsbStorageRootFolder(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_13

    .line 193
    const-string v5, "UsbDriveA"

    invoke-virtual {v13, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 194
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    const v21, 0x7f0b0042

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(I)V

    .line 206
    :cond_d
    :goto_6
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const v21, 0x7f020060

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_2

    .line 195
    :cond_e
    const-string v5, "UsbDriveB"

    invoke-virtual {v13, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 196
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    const v21, 0x7f0b0043

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_6

    .line 197
    :cond_f
    const-string v5, "UsbDriveC"

    invoke-virtual {v13, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 198
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    const v21, 0x7f0b0044

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_6

    .line 199
    :cond_10
    const-string v5, "UsbDriveD"

    invoke-virtual {v13, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 200
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    const v21, 0x7f0b0045

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_6

    .line 201
    :cond_11
    const-string v5, "UsbDriveE"

    invoke-virtual {v13, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_12

    .line 202
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    const v21, 0x7f0b0046

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_6

    .line 203
    :cond_12
    const-string v5, "UsbDriveF"

    invoke-virtual {v13, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 204
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    const v21, 0x7f0b0047

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_6

    .line 208
    :cond_13
    invoke-static {v10}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxRootFolder(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_14

    .line 210
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    const v21, 0x7f0b0151

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(I)V

    .line 212
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const v21, 0x7f02005c

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_2

    .line 216
    :cond_14
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v5, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 218
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mViewMode:I

    const/16 v21, 0x2

    move/from16 v0, v21

    if-ne v5, v0, :cond_15

    .line 220
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const v21, 0x7f0200b1

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_2

    .line 224
    :cond_15
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const v21, 0x7f0200af

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_2

    .line 232
    :cond_16
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mSelectModeFrom:I

    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v5, v0, :cond_17

    .line 234
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mSelectionType:I

    if-nez v5, :cond_1b

    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v5}, Landroid/widget/RadioButton;->getVisibility()I

    move-result v5

    const/16 v21, 0x8

    move/from16 v0, v21

    if-ne v5, v0, :cond_1b

    .line 237
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectRadioButton:Landroid/widget/RadioButton;

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 250
    :cond_17
    :goto_7
    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_18

    .line 253
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowFileExtensionStatus()Z

    move-result v5

    if-eqz v5, :cond_1e

    .line 255
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v5, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 273
    :cond_18
    :goto_8
    if-eqz v20, :cond_6

    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v5, :cond_6

    .line 275
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    const/16 v21, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 277
    const-string v5, "/storage/extSdCard"

    invoke-virtual {v10, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_19

    sget-boolean v5, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v5, :cond_20

    :cond_19
    const-string v5, "/storage/UsbDrive"

    invoke-virtual {v10, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1a

    sget-boolean v5, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-eqz v5, :cond_20

    .line 279
    :cond_1a
    const/4 v5, 0x0

    const-string v21, "AllFileBrowserAdapter"

    const-string v22, "loadThumbnail"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-static {v5, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 280
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v10}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->loadThumbnail(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 239
    :cond_1b
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mSelectionType:I

    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v5, v0, :cond_1c

    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v5

    const/16 v21, 0x8

    move/from16 v0, v21

    if-ne v5, v0, :cond_1c

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_1d

    :cond_1c
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mSelectionType:I

    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v5, v0, :cond_17

    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v5

    const/16 v21, 0x8

    move/from16 v0, v21

    if-ne v5, v0, :cond_17

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentView()I

    move-result v5

    const/16 v21, 0x2

    move/from16 v0, v21

    if-ne v5, v0, :cond_17

    .line 246
    :cond_1d
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    const/16 v21, 0x8

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto/16 :goto_7

    .line 259
    :cond_1e
    const/16 v5, 0x2e

    invoke-virtual {v13, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v15

    .line 262
    .local v15, "lastDot":I
    if-lez v15, :cond_1f

    .line 264
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v13, v0, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_8

    .line 268
    :cond_1f
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v5, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_8

    .line 282
    .end local v15    # "lastDot":I
    :cond_20
    const/4 v5, 0x0

    const-string v21, "AllFileBrowserAdapter"

    const-string v22, "loadThumbnail exit, file is not exist"

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-static {v5, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 309
    :cond_21
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mContext:Landroid/content/Context;

    const-string v21, "_size"

    move-object/from16 v0, p3

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    move-object/from16 v0, p3

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    move-wide/from16 v0, v22

    invoke-static {v5, v0, v1}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v11

    .restart local v11    # "content":Ljava/lang/String;
    goto/16 :goto_3

    .line 331
    .end local v11    # "content":Ljava/lang/String;
    :cond_22
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mViewMode:I

    const/16 v21, 0x2

    move/from16 v0, v21

    if-ne v5, v0, :cond_8

    .line 333
    const/16 v5, 0x3001

    if-ne v14, v5, :cond_8

    .line 335
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v5, :cond_8

    .line 337
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    const/16 v21, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_4

    .line 485
    .restart local v7    # "hoverFilePath":Ljava/lang/String;
    .restart local v8    # "fileType":I
    :cond_23
    const/4 v6, 0x0

    goto/16 :goto_5

    .line 678
    .restart local v4    # "hoverListener":Landroid/view/View$OnHoverListener;
    .restart local v6    # "bFolderFormat":Z
    .restart local v9    # "filePosition":I
    :cond_24
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    goto/16 :goto_0

    .line 683
    :cond_25
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const/16 v21, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 684
    move-object/from16 v0, v20

    iget-object v5, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    const/16 v21, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    goto/16 :goto_0
.end method

.method protected getContentType()I
    .locals 1

    .prologue
    .line 910
    const/16 v0, 0x201

    return v0
.end method

.method public getItemSize(Ljava/lang/String;)J
    .locals 4
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 925
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 926
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 927
    const-wide/16 v2, 0x0

    .line 929
    :goto_0
    return-wide v2

    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    goto :goto_0
.end method

.method protected getItemType(Ljava/lang/String;)I
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 916
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 917
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 918
    const/16 v1, 0x3001

    .line 920
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected onContentChanged()V
    .locals 1

    .prologue
    .line 936
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 938
    invoke-super {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->onContentChanged()V

    .line 939
    return-void
.end method
