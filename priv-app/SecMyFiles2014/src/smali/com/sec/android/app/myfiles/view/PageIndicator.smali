.class public Lcom/sec/android/app/myfiles/view/PageIndicator;
.super Landroid/widget/LinearLayout;
.source "PageIndicator.java"


# instance fields
.field private mBubbleList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentPage:I

.field private mTotalPages:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/PageIndicator;->mBubbleList:Ljava/util/ArrayList;

    .line 43
    return-void
.end method


# virtual methods
.method public setCurrentPage(I)V
    .locals 3
    .param p1, "currentPage"    # I

    .prologue
    .line 88
    iput p1, p0, Lcom/sec/android/app/myfiles/view/PageIndicator;->mCurrentPage:I

    .line 92
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/android/app/myfiles/view/PageIndicator;->mTotalPages:I

    if-ge v1, v2, :cond_1

    .line 94
    iget v2, p0, Lcom/sec/android/app/myfiles/view/PageIndicator;->mCurrentPage:I

    if-ne v1, v2, :cond_0

    .line 96
    const v0, 0x7f020068

    .line 103
    .local v0, "bubbleResId":I
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/PageIndicator;->mBubbleList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 92
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 100
    .end local v0    # "bubbleResId":I
    :cond_0
    const v0, 0x7f020067

    .restart local v0    # "bubbleResId":I
    goto :goto_1

    .line 105
    .end local v0    # "bubbleResId":I
    :cond_1
    return-void
.end method

.method public setTotalPages(I)V
    .locals 7
    .param p1, "totalPages"    # I

    .prologue
    const/4 v6, 0x0

    .line 48
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/PageIndicator;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 50
    .local v4, "res":Landroid/content/res/Resources;
    iput p1, p0, Lcom/sec/android/app/myfiles/view/PageIndicator;->mTotalPages:I

    .line 52
    const/4 v0, 0x0

    .line 54
    .local v0, "bubble":Landroid/widget/ImageView;
    const/4 v3, 0x0

    .line 56
    .local v3, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/view/PageIndicator;->mBubbleList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 58
    .local v1, "bubbleCount":I
    if-le p1, v1, :cond_0

    .line 60
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    sub-int v5, p1, v1

    if-ge v2, v5, :cond_1

    .line 62
    new-instance v0, Landroid/widget/ImageView;

    .end local v0    # "bubble":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/PageIndicator;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v0, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 64
    .restart local v0    # "bubble":Landroid/widget/ImageView;
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    .end local v3    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    const v5, 0x7f090104

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    const v6, 0x7f090105

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    invoke-direct {v3, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 67
    .restart local v3    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    const v5, 0x7f090106

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    iput v5, v3, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 69
    invoke-virtual {p0, v0, v3}, Lcom/sec/android/app/myfiles/view/PageIndicator;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 71
    iget-object v5, p0, Lcom/sec/android/app/myfiles/view/PageIndicator;->mBubbleList:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 60
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 74
    .end local v2    # "i":I
    :cond_0
    if-ge p1, v1, :cond_1

    .line 76
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    sub-int v5, v1, p1

    if-ge v2, v5, :cond_1

    .line 78
    iget-object v5, p0, Lcom/sec/android/app/myfiles/view/PageIndicator;->mBubbleList:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    invoke-virtual {p0, v5}, Lcom/sec/android/app/myfiles/view/PageIndicator;->removeView(Landroid/view/View;)V

    .line 80
    iget-object v5, p0, Lcom/sec/android/app/myfiles/view/PageIndicator;->mBubbleList:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 76
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 83
    .end local v2    # "i":I
    :cond_1
    return-void
.end method
