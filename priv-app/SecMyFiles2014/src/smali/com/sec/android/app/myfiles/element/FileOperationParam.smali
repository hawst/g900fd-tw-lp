.class public Lcom/sec/android/app/myfiles/element/FileOperationParam;
.super Ljava/lang/Object;
.source "FileOperationParam.java"


# instance fields
.field public mContainerId:I

.field public mSrcFolder:Ljava/lang/String;

.field public mStartIndex:I

.field public mTargetDatas:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mTargetFolder:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;II)V
    .locals 0
    .param p1, "srcFolder"    # Ljava/lang/String;
    .param p2, "targetFolder"    # Ljava/lang/String;
    .param p4, "startIndex"    # I
    .param p5, "containerId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p3, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mSrcFolder:Ljava/lang/String;

    .line 39
    iput-object p2, p0, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    .line 41
    iput-object p3, p0, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    .line 43
    iput p4, p0, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mStartIndex:I

    .line 45
    iput p5, p0, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mContainerId:I

    .line 46
    return-void
.end method
