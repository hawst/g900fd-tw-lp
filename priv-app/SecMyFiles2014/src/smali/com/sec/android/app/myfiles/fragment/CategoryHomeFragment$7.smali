.class Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$7;
.super Ljava/lang/Object;
.source "CategoryHomeFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setupViews(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V
    .locals 0

    .prologue
    .line 684
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 10
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 688
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 690
    .local v1, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v5, v5, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v5, :cond_0

    .line 691
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v5, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mIsSearchMode:Z

    if-eqz v5, :cond_0

    move v5, v6

    .line 735
    :goto_0
    return v5

    .line 695
    :cond_0
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v5

    const/16 v8, 0x1a

    if-ne v5, v8, :cond_5

    .line 696
    new-instance v0, Ljava/io/File;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 697
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxRootFolder(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretMode(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_3

    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    const-string v8, "/storage/extSdCard"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    sget-boolean v5, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v5, :cond_3

    :cond_2
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    const-string v8, "/storage/UsbDrive"

    invoke-virtual {v5, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    sget-boolean v5, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-nez v5, :cond_5

    .line 702
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 704
    .local v3, "res":Landroid/content/res/Resources;
    const v5, 0x7f0c0013

    new-array v8, v7, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v6

    invoke-virtual {v3, v5, v7, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 705
    .local v2, "notiStr":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v5, v2, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 706
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v8

    invoke-static {v5, v7, v8}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteShortcut(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v4

    .line 707
    .local v4, "rowsDeleted":I
    if-lez v4, :cond_4

    .line 709
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    move-result-object v5

    invoke-virtual {v5, v1}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->removeItem(Ljava/lang/Object;)Z

    .line 710
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->onRefresh()V

    .line 711
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListView:Landroid/widget/ListView;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Landroid/widget/ListView;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/myfiles/ListViewHeightRefresh;->getListViewSize(Landroid/widget/ListView;)V

    move v5, v6

    .line 713
    goto/16 :goto_0

    .line 717
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v7, 0x7f0b0063

    invoke-static {v5, v7, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    move v5, v6

    .line 718
    goto/16 :goto_0

    .line 724
    .end local v0    # "file":Ljava/io/File;
    .end local v2    # "notiStr":Ljava/lang/String;
    .end local v3    # "res":Landroid/content/res/Resources;
    .end local v4    # "rowsDeleted":I
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->isSelectMode()Z

    move-result v5

    if-nez v5, :cond_6

    .line 725
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v5, v7, p3, v6}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->startSelectMode(III)V

    .line 726
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    move-result-object v5

    invoke-virtual {v5, v6, v6}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->startSelectMode(II)V

    .line 727
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    move-result-object v5

    invoke-virtual {v5, p3}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->selectItem(I)V

    .line 728
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    move-result-object v5

    invoke-virtual {v5, v6, v6}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->startSelectMode(II)V

    .line 729
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    move-result-object v5

    invoke-virtual {v5, v6, v6}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->startSelectMode(II)V

    .line 730
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mStorageListAdapter:Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$500(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;

    move-result-object v5

    invoke-virtual {v5, v6, v6}, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->startSelectMode(II)V

    .line 731
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mRemoteShareListAdapter:Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$600(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;

    move-result-object v5

    invoke-virtual {v5, v6, v6}, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->startSelectMode(II)V

    .line 732
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setAllItemCount()V

    move v5, v7

    .line 733
    goto/16 :goto_0

    :cond_6
    move v5, v6

    .line 735
    goto/16 :goto_0
.end method
