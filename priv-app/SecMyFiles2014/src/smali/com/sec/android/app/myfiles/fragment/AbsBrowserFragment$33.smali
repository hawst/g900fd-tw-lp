.class Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$33;
.super Ljava/lang/Object;
.source "AbsBrowserFragment.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V
    .locals 0

    .prologue
    .line 6272
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$33;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 6277
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    move v2, v3

    .line 6307
    :cond_0
    :goto_0
    return v2

    .line 6281
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 6283
    .local v0, "path":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 6284
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$33;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    new-instance v5, Landroid/content/Intent;

    const-string v6, "Intent.ACTION_MEDIA_SCAN_LAUNCH"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "file://"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->getStorage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v6, "where"

    const-string v7, "myfiles"

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 6286
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$33;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iput-boolean v2, v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mIsConverting:Z

    .line 6287
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$33;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setmChooserFromConvert(Z)V

    .line 6288
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$33;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v4, v0, v3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->share(Ljava/lang/String;Z)V

    .line 6295
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$33;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v4, v3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->showWaitProgressDialog(Z)V

    .line 6297
    new-instance v1, Ljava/io/File;

    sget-object v3, Lcom/sec/android/app/myfiles/utils/Constant;->GOLF_TEMP_VIDEO_PATH:Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 6299
    .local v1, "tmpVideoDir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 6301
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$33;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    # invokes: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->deleteGolfTempFiles(Ljava/io/File;)V
    invoke-static {v3, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$900(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;Ljava/io/File;)V

    goto :goto_0

    .line 6292
    .end local v1    # "tmpVideoDir":Ljava/io/File;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$33;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-string v5, "Convert Fail"

    invoke-static {v4, v5, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 6277
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method
