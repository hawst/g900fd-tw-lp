.class public abstract Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;
.source "AbsFTPDetailFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;
    }
.end annotation


# static fields
.field protected static final SIZE_PREFIX:Ljava/lang/String; = ""


# instance fields
.field protected mAdapter:Landroid/widget/BaseAdapter;

.field protected mDetailsAdapter:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;

.field protected mDiscoveryVisitor:Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;

.field private mSizeTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;-><init>()V

    .line 159
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$5;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->mDiscoveryVisitor:Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;

    .line 208
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->mSizeTextView:Landroid/widget/TextView;

    .line 210
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->mSizeTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->mSizeTextView:Landroid/widget/TextView;

    return-object p1
.end method

.method protected static final getSizeString(Landroid/content/Context;J)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "size"    # J

    .prologue
    .line 147
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 149
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-static {p0, p1, p2}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-static {v2}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    .line 151
    .local v0, "nf":Ljava/text/NumberFormat;
    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0, p1, p2}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f0b006f

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x29

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 153
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method protected abstract fillListView(Landroid/widget/ListView;Landroid/os/Bundle;)V
.end method

.method public getDiscoveryVisitor()Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->mDiscoveryVisitor:Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;

    return-object v0
.end method

.method public getFileLastModifiedTime(Ljava/lang/String;I)J
    .locals 2
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 117
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getFileName(Ljava/lang/String;I)Ljava/lang/String;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 107
    const-string v0, ""

    return-object v0
.end method

.method public getFileSize(Ljava/lang/String;I)J
    .locals 2
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 112
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method protected abstract getInitValue(I)Ljava/lang/String;
.end method

.method protected abstract getItemCount()I
.end method

.method protected abstract getLabel(I)Ljava/lang/String;
.end method

.method protected abstract getSizeTextViewPosition()I
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->onCreate(Landroid/os/Bundle;)V

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getAdapter(I)Landroid/widget/BaseAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->mAdapter:Landroid/widget/BaseAdapter;

    .line 52
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 56
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v1, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 57
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 58
    .local v2, "factory":Landroid/view/LayoutInflater;
    const v5, 0x1090014

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 59
    .local v4, "view":Landroid/view/View;
    const v5, 0x102000a

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    .line 61
    .local v3, "listView":Landroid/widget/ListView;
    if-eqz v3, :cond_0

    .line 63
    new-instance v5, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;)V

    invoke-virtual {v3, v5}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 76
    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 77
    const v5, 0x7f0b0025

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 79
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 81
    .local v0, "argument":Landroid/os/Bundle;
    invoke-virtual {p0, v3, v0}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->fillListView(Landroid/widget/ListView;Landroid/os/Bundle;)V

    .line 84
    .end local v0    # "argument":Landroid/os/Bundle;
    :cond_0
    const v5, 0x7f0b0015

    new-instance v6, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$2;

    invoke-direct {v6, p0}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$2;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;)V

    invoke-virtual {v1, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 92
    new-instance v5, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$3;

    invoke-direct {v5, p0}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$3;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;)V

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 102
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    return-object v5
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 299
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->mSizeTextView:Landroid/widget/TextView;

    .line 300
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->onDestroyView()V

    .line 301
    return-void
.end method

.method protected abstract setSize(Ljava/lang/String;)V
.end method

.method public updateSize(Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;Ljava/lang/String;)V
    .locals 4
    .param p1, "adapter"    # Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;
    .param p2, "size"    # Ljava/lang/String;

    .prologue
    .line 125
    const-string v1, "MyFiles"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "size: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    if-eqz p1, :cond_0

    .line 127
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 128
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 129
    .local v0, "activity":Landroid/app/Activity;
    new-instance v1, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$4;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$4;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 144
    .end local v0    # "activity":Landroid/app/Activity;
    :cond_0
    return-void
.end method
