.class public Lcom/sec/android/app/myfiles/adapter/DocumentBrowserAdapter;
.super Lcom/sec/android/app/myfiles/adapter/AbsCategoryBrowserAdapter;
.source "DocumentBrowserAdapter.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "c"    # Landroid/database/Cursor;
    .param p4, "flags"    # I
    .param p5, "navigation"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .prologue
    .line 42
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/myfiles/adapter/AbsCategoryBrowserAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DocumentBrowserAdapter;->mContext:Landroid/content/Context;

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-static {v0, v1}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    new-instance v0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DocumentBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/DocumentBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 48
    :cond_0
    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 10
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 53
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/adapter/AbsCategoryBrowserAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 55
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;

    .line 57
    .local v7, "vh":Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;
    iget v8, p0, Lcom/sec/android/app/myfiles/adapter/DocumentBrowserAdapter;->mDisplayMode:I

    if-nez v8, :cond_7

    .line 59
    const-string v8, "_data"

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 61
    .local v1, "filePath":Ljava/lang/String;
    const/16 v8, 0x2f

    invoke-virtual {v1, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 63
    .local v0, "fileName":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 66
    iget-object v8, p0, Lcom/sec/android/app/myfiles/adapter/DocumentBrowserAdapter;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowFileExtensionStatus()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 68
    iget-object v8, v7, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    :cond_0
    :goto_0
    iget-object v8, v7, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v8, :cond_1

    .line 87
    iget-object v8, v7, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 90
    :cond_1
    invoke-virtual {p0, v7, v1}, Lcom/sec/android/app/myfiles/adapter/DocumentBrowserAdapter;->loadThumbnail(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;Ljava/lang/String;)V

    .line 94
    move-object v4, v1

    .line 95
    .local v4, "hoverFilePath":Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v3

    .line 96
    .local v3, "fileType":I
    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    .line 98
    .local v2, "filePosition":I
    new-instance v5, Lcom/sec/android/app/myfiles/adapter/DocumentBrowserAdapter$1;

    invoke-direct {v5, p0, v3, v4, v2}, Lcom/sec/android/app/myfiles/adapter/DocumentBrowserAdapter$1;-><init>(Lcom/sec/android/app/myfiles/adapter/DocumentBrowserAdapter;ILjava/lang/String;I)V

    .line 177
    .local v5, "hoverListener":Landroid/view/View$OnHoverListener;
    iget v8, p0, Lcom/sec/android/app/myfiles/adapter/DocumentBrowserAdapter;->mViewMode:I

    if-eqz v8, :cond_2

    iget v8, p0, Lcom/sec/android/app/myfiles/adapter/DocumentBrowserAdapter;->mViewMode:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_6

    .line 178
    :cond_2
    iget-object v8, v7, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    if-eqz v8, :cond_3

    .line 179
    iget-object v8, v7, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v8, v5}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 193
    .end local v0    # "fileName":Ljava/lang/String;
    .end local v1    # "filePath":Ljava/lang/String;
    .end local v2    # "filePosition":I
    .end local v3    # "fileType":I
    .end local v4    # "hoverFilePath":Ljava/lang/String;
    .end local v5    # "hoverListener":Landroid/view/View$OnHoverListener;
    :cond_3
    :goto_1
    return-void

    .line 72
    .restart local v0    # "fileName":Ljava/lang/String;
    .restart local v1    # "filePath":Ljava/lang/String;
    :cond_4
    const/16 v8, 0x2e

    invoke-virtual {v0, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v6

    .line 75
    .local v6, "lastDot":I
    if-lez v6, :cond_5

    .line 77
    iget-object v8, v7, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    const/4 v9, 0x0

    invoke-virtual {v0, v9, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 81
    :cond_5
    iget-object v8, v7, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 182
    .end local v6    # "lastDot":I
    .restart local v2    # "filePosition":I
    .restart local v3    # "fileType":I
    .restart local v4    # "hoverFilePath":Ljava/lang/String;
    .restart local v5    # "hoverListener":Landroid/view/View$OnHoverListener;
    :cond_6
    iget-object v8, v7, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v8, :cond_3

    .line 183
    iget-object v8, v7, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v8, v5}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    goto :goto_1

    .line 191
    .end local v0    # "fileName":Ljava/lang/String;
    .end local v1    # "filePath":Ljava/lang/String;
    .end local v2    # "filePosition":I
    .end local v3    # "fileType":I
    .end local v4    # "hoverFilePath":Ljava/lang/String;
    .end local v5    # "hoverListener":Landroid/view/View$OnHoverListener;
    :cond_7
    iget-object v8, v7, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const v9, 0x7f0200af

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method

.method protected getContentType()I
    .locals 1

    .prologue
    .line 199
    const/4 v0, 0x5

    return v0
.end method
