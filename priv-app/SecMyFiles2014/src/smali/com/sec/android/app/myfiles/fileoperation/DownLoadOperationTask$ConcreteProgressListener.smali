.class Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;
.super Ljava/lang/Object;
.source "DownLoadOperationTask.java"

# interfaces
.implements Lcom/samsung/scloud/response/ProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ConcreteProgressListener"
.end annotation


# instance fields
.field private mFile:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;

.field private mTotalSize:J

.field private mUpFile:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;

.field final synthetic this$0:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 486
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;->this$0:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    .line 488
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 466
    iput-wide v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;->mTotalSize:J

    .line 468
    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;->mFile:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;

    .line 470
    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;->mUpFile:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;

    .line 489
    iput-wide v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;->mTotalSize:J

    .line 490
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;JLcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;)V
    .locals 4
    .param p2, "totalSize"    # J
    .param p4, "file"    # Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;

    .prologue
    const/4 v2, 0x0

    .line 472
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;->this$0:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    .line 474
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 466
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;->mTotalSize:J

    .line 468
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;->mFile:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;

    .line 470
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;->mUpFile:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;

    .line 475
    iput-wide p2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;->mTotalSize:J

    .line 476
    iput-object p4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;->mFile:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;

    .line 477
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;JLcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;)V
    .locals 4
    .param p2, "totalSize"    # J
    .param p4, "file"    # Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;

    .prologue
    const/4 v2, 0x0

    .line 479
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;->this$0:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    .line 481
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 466
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;->mTotalSize:J

    .line 468
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;->mFile:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;

    .line 470
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;->mUpFile:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;

    .line 482
    iput-wide p2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;->mTotalSize:J

    .line 483
    iput-object p4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;->mUpFile:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;

    .line 484
    return-void
.end method


# virtual methods
.method public onException(Lcom/samsung/scloud/exception/SCloudException;)V
    .locals 0
    .param p1, "e"    # Lcom/samsung/scloud/exception/SCloudException;

    .prologue
    .line 495
    return-void
.end method

.method public onUpdate(Ljava/lang/String;IJ)V
    .locals 5
    .param p1, "file"    # Ljava/lang/String;
    .param p2, "statusTransferOngoing"    # I
    .param p3, "progress"    # J

    .prologue
    .line 500
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;->mTotalSize:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 501
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;->mFile:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;

    if-eqz v0, :cond_0

    .line 502
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;->mFile:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;

    iput-wide p3, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;->mDownloadPercent:J

    .line 504
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;->mUpFile:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;

    if-eqz v0, :cond_1

    .line 505
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;->mUpFile:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;

    iput-wide p3, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mUploadPercent:J

    .line 507
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;->this$0:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    # invokes: Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->updateProgressBar()V
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->access$500(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;)V

    .line 514
    :goto_0
    return-void

    .line 510
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;->this$0:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    long-to-int v1, p3

    iput v1, v0, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mCurrentTargetPercentage:I

    .line 512
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;->this$0:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;->this$0:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    aput-object v3, v1, v2

    # invokes: Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->publishProgress([Ljava/lang/Object;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->access$600(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;[Ljava/lang/Object;)V

    goto :goto_0
.end method
