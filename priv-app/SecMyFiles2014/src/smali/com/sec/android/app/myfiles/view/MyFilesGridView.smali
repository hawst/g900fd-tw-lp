.class public Lcom/sec/android/app/myfiles/view/MyFilesGridView;
.super Landroid/widget/GridView;
.source "MyFilesGridView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/sec/android/app/myfiles/view/IDirectDragable;


# static fields
.field private static final DIRECT_DRAG_FIRE_TIME:I = 0x3e8


# instance fields
.field private mArgument:Landroid/os/Bundle;

.field private mDirectDragBackground:I

.field private mDirectDragEnabled:Z

.field private mDirectDragFireTimer:Landroid/os/CountDownTimer;

.field private mDirectDragListener:Lcom/sec/android/app/myfiles/view/IDirectDragable$OnDirectDragListener;

.field private mLongClickTriggered:Z

.field private mTouchedRowPosition:I

.field private mTouchedRowView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->mTouchedRowPosition:I

    .line 57
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08003e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->mDirectDragBackground:I

    .line 59
    invoke-virtual {p0, p0}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 60
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/view/MyFilesGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    .prologue
    .line 31
    iget v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->mDirectDragBackground:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/view/MyFilesGridView;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->mTouchedRowView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/view/MyFilesGridView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->performDirectDrag()V

    return-void
.end method

.method private endDirectDrag()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 160
    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->mDirectDragEnabled:Z

    .line 166
    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->mLongClickTriggered:Z

    .line 167
    return-void
.end method

.method private performDirectDrag()V
    .locals 3

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->mDirectDragListener:Lcom/sec/android/app/myfiles/view/IDirectDragable$OnDirectDragListener;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->mDirectDragListener:Lcom/sec/android/app/myfiles/view/IDirectDragable$OnDirectDragListener;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->mTouchedRowView:Landroid/view/View;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->mArgument:Landroid/os/Bundle;

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/myfiles/view/IDirectDragable$OnDirectDragListener;->onDirectDragStarted(Landroid/view/View;Landroid/os/Bundle;)V

    .line 146
    :cond_0
    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 66
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->mDirectDragEnabled:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->mLongClickTriggered:Z

    if-eqz v2, :cond_0

    .line 68
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 70
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 92
    .end local v0    # "action":I
    :cond_0
    :goto_0
    const/4 v2, 0x0

    return v2

    .line 74
    .restart local v0    # "action":I
    :pswitch_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->pointToPosition(II)I

    move-result v1

    .line 76
    .local v1, "newRowPosition":I
    iget v2, p0, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->mTouchedRowPosition:I

    if-eq v2, v1, :cond_0

    .line 78
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->endDirectDrag()V

    goto :goto_0

    .line 86
    .end local v1    # "newRowPosition":I
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->endDirectDrag()V

    goto :goto_0

    .line 70
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setDirectDragMode(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 99
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->mDirectDragEnabled:Z

    .line 100
    return-void
.end method

.method public setOnDirectDragListener(Lcom/sec/android/app/myfiles/view/IDirectDragable$OnDirectDragListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/myfiles/view/IDirectDragable$OnDirectDragListener;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->mDirectDragListener:Lcom/sec/android/app/myfiles/view/IDirectDragable$OnDirectDragListener;

    .line 153
    return-void
.end method

.method public triggerLongClick(ILandroid/os/Bundle;)V
    .locals 6
    .param p1, "position"    # I
    .param p2, "argument"    # Landroid/os/Bundle;

    .prologue
    .line 106
    iput-object p2, p0, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->mArgument:Landroid/os/Bundle;

    .line 108
    iput p1, p0, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->mTouchedRowPosition:I

    .line 110
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->getFirstVisiblePosition()I

    move-result v0

    sub-int v0, p1, v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0f002f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->mTouchedRowView:Landroid/view/View;

    .line 112
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->mLongClickTriggered:Z

    .line 116
    new-instance v0, Lcom/sec/android/app/myfiles/view/MyFilesGridView$1;

    const-wide/16 v2, 0x3e8

    const-wide/16 v4, 0xa

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/view/MyFilesGridView$1;-><init>(Lcom/sec/android/app/myfiles/view/MyFilesGridView;JJ)V

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/view/MyFilesGridView$1;->start()Landroid/os/CountDownTimer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->mDirectDragFireTimer:Landroid/os/CountDownTimer;

    .line 137
    return-void
.end method
