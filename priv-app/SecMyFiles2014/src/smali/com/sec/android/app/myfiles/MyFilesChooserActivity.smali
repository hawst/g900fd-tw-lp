.class public Lcom/sec/android/app/myfiles/MyFilesChooserActivity;
.super Lcom/android/internal/app/ChooserActivity;
.source "MyFilesChooserActivity.java"


# static fields
.field private static final MODULE:Ljava/lang/String; = "MyFilesChooserActivity"


# instance fields
.field private mIntentFilter:Landroid/content/IntentFilter;

.field private mPathList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/android/internal/app/ChooserActivity;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/MyFilesChooserActivity;->mPathList:Ljava/util/ArrayList;

    return-void
.end method

.method private setBroadcastReceiver()V
    .locals 3

    .prologue
    .line 79
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/MyFilesChooserActivity;->mIntentFilter:Landroid/content/IntentFilter;

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MyFilesChooserActivity;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MyFilesChooserActivity;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MyFilesChooserActivity;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 83
    new-instance v0, Lcom/sec/android/app/myfiles/MyFilesChooserActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/MyFilesChooserActivity$1;-><init>(Lcom/sec/android/app/myfiles/MyFilesChooserActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/MyFilesChooserActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MyFilesChooserActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/MyFilesChooserActivity;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/myfiles/MyFilesChooserActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 95
    const/4 v0, 0x0

    const-string v1, "MyFilesChooserActivity"

    const-string v2, "setBroadcastReceiver"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 96
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 41
    invoke-super {p0, p1}, Lcom/android/internal/app/ChooserActivity;->onCreate(Landroid/os/Bundle;)V

    .line 42
    const-string v0, "MyFilesChooserActivity"

    const-string v1, "onCreate"

    invoke-static {v2, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 43
    if-eqz p1, :cond_0

    .line 44
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MyFilesChooserActivity;->finish()V

    .line 47
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MyFilesChooserActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 48
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->setNotibarAutoShowing(Landroid/app/Activity;)V

    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/MyFilesChooserActivity;->setBroadcastReceiver()V

    .line 51
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MyFilesChooserActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "path_list"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 52
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MyFilesChooserActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "path_list"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/MyFilesChooserActivity;->mPathList:Ljava/util/ArrayList;

    .line 54
    :cond_1
    const-string v0, "MyFilesChooserActivity"

    const-string v1, "onCreate"

    invoke-static {v2, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 55
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 102
    const-string v0, "MyFilesChooserActivity"

    const-string v1, "onDestroy"

    invoke-static {v2, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MyFilesChooserActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/MyFilesChooserActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 104
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setmChooserFromConvert(Z)V

    .line 105
    invoke-super {p0}, Lcom/android/internal/app/ChooserActivity;->onDestroy()V

    .line 106
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 59
    invoke-super {p0}, Lcom/android/internal/app/ChooserActivity;->onResume()V

    .line 60
    const-string v2, "MyFilesChooserActivity"

    const-string v3, "onResume"

    invoke-static {v4, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 61
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MyFilesChooserActivity;->mPathList:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 62
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MyFilesChooserActivity;->mPathList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 63
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MyFilesChooserActivity;->mPathList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 64
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/MyFilesChooserActivity;->finish()V

    .line 62
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 69
    .end local v0    # "file":Ljava/io/File;
    .end local v1    # "i":I
    :cond_1
    const-string v2, "MyFilesChooserActivity"

    const-string v3, "onResume"

    invoke-static {v4, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 70
    return-void
.end method

.method public setSafeForwardingMode(Z)V
    .locals 1
    .param p1, "safeForwarding"    # Z

    .prologue
    .line 75
    const/4 v0, 0x0

    invoke-super {p0, v0}, Lcom/android/internal/app/ChooserActivity;->setSafeForwardingMode(Z)V

    .line 76
    return-void
.end method
