.class public Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;
.super Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;
.source "SelectOperation.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mFromMultipleSelection:Z

.field private final mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "arg"    # Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;
    .param p2, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "subtitle"    # Ljava/lang/String;
    .param p5, "fileName"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;->mContext:Landroid/content/Context;

    .line 35
    iput-object p2, p0, Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;->mFromMultipleSelection:Z

    .line 37
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "arg"    # Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;
    .param p2, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "subtitle"    # Ljava/lang/String;
    .param p5, "fileName"    # Ljava/lang/String;
    .param p6, "fromMultipleSelection"    # Z

    .prologue
    .line 40
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;->mContext:Landroid/content/Context;

    .line 42
    iput-object p2, p0, Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    .line 43
    iput-boolean p6, p0, Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;->mFromMultipleSelection:Z

    .line 44
    return-void
.end method


# virtual methods
.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 8
    .param p1, "success"    # Ljava/lang/Boolean;

    .prologue
    const/4 v6, -0x1

    .line 48
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 49
    iget-object v4, p0, Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;->mDownloadItems:[Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    if-eqz v4, :cond_0

    .line 51
    iget-boolean v4, p0, Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;->mFromMultipleSelection:Z

    if-nez v4, :cond_1

    .line 53
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 55
    .local v2, "result":Landroid/content/Intent;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;->mDownloadItem:Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/ftp/DownloadItem;->getTarget()Ljava/lang/String;

    move-result-object v1

    .line 57
    .local v1, "path":Ljava/lang/String;
    const-string v4, "FILE"

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 59
    const-string v5, "FILE_URI"

    iget-object v4, p0, Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-static {v4, v1}, Lcom/sec/android/app/myfiles/utils/FileUtils;->pathToUri(Landroid/app/Activity;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 61
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 63
    const-string v4, "CONTENT_TYPE"

    invoke-static {v1}, Lcom/sec/android/app/myfiles/MediaFile;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 65
    iget-object v4, p0, Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4, v6, v2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 67
    iget-object v4, p0, Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->finish()V

    .line 92
    .end local v1    # "path":Ljava/lang/String;
    .end local v2    # "result":Landroid/content/Intent;
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;->mDownloadItem:Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/ftp/DownloadItem;->getFtpItem()Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-result-object v6

    invoke-interface {v6}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;->mDownloadItem:Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/ftp/DownloadItem;->getTarget()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v4, v5, v6, v7}, Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;->onSelectCompleted(ZLjava/lang/String;Ljava/lang/String;)V

    .line 96
    return-void

    .line 71
    :cond_1
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 73
    .restart local v2    # "result":Landroid/content/Intent;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;->mDownloadItems:[Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    array-length v4, v4

    new-array v3, v4, [Ljava/lang/String;

    .line 75
    .local v3, "resultPaths":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;->mDownloadItems:[Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    array-length v4, v4

    if-ge v0, v4, :cond_2

    .line 77
    iget-object v4, p0, Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;->mDownloadItems:[Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/ftp/DownloadItem;->getTarget()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    .line 75
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 81
    :cond_2
    const-string v4, "FILE"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 83
    iget-object v4, p0, Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4, v6, v2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 85
    iget-object v4, p0, Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;->mContext:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 31
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/SelectOperation;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
