.class public Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;
.super Landroid/widget/BaseAdapter;
.source "AbsFTPDetailFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "FTPDetailsDialogAdapter"
.end annotation


# instance fields
.field private isFile:Z

.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mLabels:[Ljava/lang/String;

.field private mTxtViews:[Landroid/widget/TextView;

.field private mValues:[Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;Landroid/content/Context;)V
    .locals 6
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x2

    .line 212
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 211
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->isFile:Z

    .line 213
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->getCount()I

    move-result v0

    .line 215
    .local v0, "count":I
    if-le v0, v5, :cond_0

    .line 216
    invoke-virtual {p1, v5}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->getInitValue(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 217
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->isFile:Z

    .line 220
    :cond_0
    new-array v3, v0, [Ljava/lang/String;

    iput-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->mLabels:[Ljava/lang/String;

    .line 221
    new-array v3, v0, [Ljava/lang/String;

    iput-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->mValues:[Ljava/lang/String;

    .line 222
    new-array v3, v0, [Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->mTxtViews:[Landroid/widget/TextView;

    .line 223
    const-string v3, "layout_inflater"

    invoke-virtual {p2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    iput-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 224
    const/4 v1, 0x0

    .line 225
    .local v1, "ctr":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->getItemCount()I

    move-result v3

    if-ge v2, v3, :cond_4

    .line 226
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->isFile:Z

    if-eqz v3, :cond_1

    if-ne v2, v5, :cond_2

    :cond_1
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->isFile:Z

    if-nez v3, :cond_3

    .line 227
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->mLabels:[Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->getLabel(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    .line 228
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->mValues:[Ljava/lang/String;

    invoke-virtual {p1, v2}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->getInitValue(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    .line 229
    add-int/lit8 v1, v1, 0x1

    .line 225
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 232
    :cond_4
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 236
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->isFile:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->getItemCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->getItemCount()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 241
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->getItemCount()I

    move-result v0

    if-gt p1, v0, :cond_0

    .line 242
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->mLabels:[Ljava/lang/String;

    aget-object v2, v2, p1

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->mValues:[Ljava/lang/String;

    aget-object v2, v2, p1

    aput-object v2, v0, v1

    .line 244
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 250
    int-to-long v0, p1

    return-wide v0
.end method

.method public getSizeValueTextView()Landroid/widget/TextView;
    .locals 2

    .prologue
    .line 287
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->mTxtViews:[Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->getSizeTextViewPosition()I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v7, 0x1020015

    const v6, 0x1020014

    .line 257
    if-nez p2, :cond_3

    .line 258
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f04002b

    const/4 v5, 0x0

    invoke-virtual {v3, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 259
    .local v1, "v":Landroid/view/View;
    if-ltz p1, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->getItemCount()I

    move-result v3

    if-gt p1, v3, :cond_1

    .line 260
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->getItemCount()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    if-nez p1, :cond_2

    .line 261
    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 262
    .local v0, "label":Landroid/widget/TextView;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->mSizeTextView:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;)Landroid/widget/TextView;

    move-result-object v3

    if-nez v3, :cond_0

    .line 263
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->mSizeTextView:Landroid/widget/TextView;
    invoke-static {v4, v3}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->access$002(Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 266
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->mLabels:[Ljava/lang/String;

    aget-object v3, v3, p1

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 267
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->mValues:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;

    invoke-virtual {v4, p1}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->getInitValue(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, p1

    .line 268
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->mSizeTextView:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->mValues:[Ljava/lang/String;

    aget-object v4, v4, p1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 269
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->mTxtViews:[Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->mSizeTextView:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;)Landroid/widget/TextView;

    move-result-object v4

    aput-object v4, v3, p1

    .line 283
    .end local v0    # "label":Landroid/widget/TextView;
    :cond_1
    :goto_0
    return-object v1

    .line 271
    :cond_2
    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 272
    .restart local v0    # "label":Landroid/widget/TextView;
    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 274
    .local v2, "value":Landroid/widget/TextView;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->mLabels:[Ljava/lang/String;

    aget-object v3, v3, p1

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 275
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->mValues:[Ljava/lang/String;

    aget-object v3, v3, p1

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 276
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->mTxtViews:[Landroid/widget/TextView;

    aput-object v2, v3, p1

    goto :goto_0

    .line 280
    .end local v0    # "label":Landroid/widget/TextView;
    .end local v1    # "v":Landroid/view/View;
    .end local v2    # "value":Landroid/widget/TextView;
    :cond_3
    move-object v1, p2

    .restart local v1    # "v":Landroid/view/View;
    goto :goto_0
.end method
