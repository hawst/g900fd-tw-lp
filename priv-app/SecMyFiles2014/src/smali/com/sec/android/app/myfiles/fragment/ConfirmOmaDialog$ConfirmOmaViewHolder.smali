.class Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$ConfirmOmaViewHolder;
.super Ljava/lang/Object;
.source "ConfirmOmaDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ConfirmOmaViewHolder"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field public mDescription:Landroid/widget/TextView;

.field public mName:Landroid/widget/TextView;

.field public mSize:Landroid/widget/TextView;

.field public mType:Landroid/widget/TextView;

.field public mVendor:Landroid/widget/TextView;

.field public mVersion:Landroid/widget/TextView;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 408
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fromView(Landroid/content/Context;Landroid/view/View;)Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$ConfirmOmaViewHolder;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 420
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$ConfirmOmaViewHolder;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$ConfirmOmaViewHolder;-><init>()V

    .line 422
    .local v0, "d":Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$ConfirmOmaViewHolder;
    iput-object p0, v0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$ConfirmOmaViewHolder;->mContext:Landroid/content/Context;

    .line 423
    const v1, 0x7f0f009d

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$ConfirmOmaViewHolder;->mName:Landroid/widget/TextView;

    .line 424
    const v1, 0x7f0f009e

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$ConfirmOmaViewHolder;->mVendor:Landroid/widget/TextView;

    .line 425
    const v1, 0x7f0f009f

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$ConfirmOmaViewHolder;->mSize:Landroid/widget/TextView;

    .line 426
    const v1, 0x7f0f00a0

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$ConfirmOmaViewHolder;->mVersion:Landroid/widget/TextView;

    .line 427
    const v1, 0x7f0f00a1

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$ConfirmOmaViewHolder;->mType:Landroid/widget/TextView;

    .line 428
    const v1, 0x7f0f00a2

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$ConfirmOmaViewHolder;->mDescription:Landroid/widget/TextView;

    .line 430
    return-object v0
.end method


# virtual methods
.method public updateForItem(Lcom/sec/android/app/myfiles/element/HistoryItem;)V
    .locals 4
    .param p1, "item"    # Lcom/sec/android/app/myfiles/element/HistoryItem;

    .prologue
    .line 436
    if-eqz p1, :cond_0

    .line 438
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$ConfirmOmaViewHolder;->mName:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 439
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$ConfirmOmaViewHolder;->mVendor:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getVendor()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 440
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$ConfirmOmaViewHolder;->mSize:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$ConfirmOmaViewHolder;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getSize()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 441
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$ConfirmOmaViewHolder;->mVersion:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 442
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$ConfirmOmaViewHolder;->mType:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getMimeType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 443
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog$ConfirmOmaViewHolder;->mDescription:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 447
    :cond_0
    return-void
.end method
