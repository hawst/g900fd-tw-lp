.class public abstract Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;
.super Landroid/app/DialogFragment;
.source "AbsDetailFragment.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/fragment/OnShowDetailsListener;


# static fields
.field private static final CONTENT:Ljava/lang/String; = "content"

.field private static final TITLE:Ljava/lang/String; = "title"


# instance fields
.field lastModifiedTime:J

.field protected mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 57
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 59
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/AbsMainActivity;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    .line 60
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 30
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 66
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v10

    .line 68
    .local v10, "argument":Landroid/os/Bundle;
    if-eqz v10, :cond_b

    .line 70
    const-string v5, "detail_item_path"

    invoke-virtual {v10, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 72
    .local v25, "path":Ljava/lang/String;
    const-string v5, "detail_item_index"

    invoke-virtual {v10, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v22

    .line 74
    .local v22, "index":I
    const/4 v12, 0x0

    .line 76
    .local v12, "datas":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 79
    .local v6, "dataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    new-instance v12, Ljava/util/HashMap;

    .end local v12    # "datas":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    .line 81
    .restart local v12    # "datas":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v5, "title"

    const v7, 0x7f0b0033

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v12, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    const-string v5, "content"

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->getFileName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v12, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    invoke-virtual {v6, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    new-instance v12, Ljava/util/HashMap;

    .end local v12    # "datas":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    .line 91
    .restart local v12    # "datas":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v5, "title"

    const v7, 0x7f0b0034

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v12, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    .line 95
    .local v27, "sb":Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->getFileSize(Ljava/lang/String;I)J

    move-result-wide v16

    .line 97
    .local v16, "fileLength":J
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    move-wide/from16 v0, v16

    invoke-static {v5, v0, v1}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v27

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-static {v5}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v24

    .line 101
    .local v24, "nf":Ljava/text/NumberFormat;
    const-string v5, " ("

    move-object/from16 v0, v27

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v24

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v7, 0x20

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    const v7, 0x7f0b006f

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v7, 0x29

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 103
    const-string v5, "content"

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v12, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    invoke-virtual {v6, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    const-string v5, "detail_item_format"

    invoke-virtual {v10, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 109
    const/16 v18, 0x0

    .line 110
    .local v18, "folderCnt":I
    const/4 v15, 0x0

    .line 111
    .local v15, "fileCnt":I
    const-string v5, "detail_item_format"

    invoke-virtual {v10, v5}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v20

    .line 112
    .local v20, "formats":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v21

    .local v21, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v19

    .line 113
    .local v19, "format":I
    const/16 v5, 0x3001

    move/from16 v0, v19

    if-ne v0, v5, :cond_0

    .line 114
    add-int/lit8 v18, v18, 0x1

    goto :goto_0

    .line 116
    :cond_0
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    .line 120
    .end local v19    # "format":I
    :cond_1
    new-instance v12, Ljava/util/HashMap;

    .end local v12    # "datas":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    .line 121
    .restart local v12    # "datas":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v5, "title"

    const v7, 0x7f0b00ab

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v12, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    const-string v14, ""

    .line 123
    .local v14, "fCnt":Ljava/lang/String;
    if-eqz v15, :cond_2

    .line 124
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const/high16 v8, 0x7f0c0000

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/16 v28, 0x0

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    aput-object v29, v9, v28

    invoke-virtual {v7, v8, v15, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 127
    :cond_2
    if-eqz v18, :cond_4

    .line 128
    const-string v5, ""

    invoke-virtual {v14, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 129
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 130
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0c0001

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/16 v28, 0x0

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    aput-object v29, v9, v28

    move/from16 v0, v18

    invoke-virtual {v7, v8, v0, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 133
    :cond_4
    const-string v5, "content"

    invoke-virtual {v12, v5, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    invoke-virtual {v6, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    .end local v14    # "fCnt":Ljava/lang/String;
    .end local v15    # "fileCnt":I
    .end local v18    # "folderCnt":I
    .end local v20    # "formats":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v21    # "i$":Ljava/util/Iterator;
    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->getFileLastModifiedTime(Ljava/lang/String;I)J

    move-result-wide v8

    move-object/from16 v0, p0

    iput-wide v8, v0, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->lastModifiedTime:J

    .line 140
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->lastModifiedTime:J

    const-wide/16 v28, -0x1

    cmp-long v5, v8, v28

    if-eqz v5, :cond_6

    .line 142
    new-instance v12, Ljava/util/HashMap;

    .end local v12    # "datas":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    .line 144
    .restart local v12    # "datas":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v5, "title"

    const v7, 0x7f0b006e

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v12, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    const-string v5, "content"

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->lastModifiedTime:J

    const-wide/16 v28, 0x3e8

    mul-long v8, v8, v28

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/Utils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v12, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    invoke-virtual {v6, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 152
    :cond_6
    if-eqz v25, :cond_7

    .line 154
    new-instance v12, Ljava/util/HashMap;

    .end local v12    # "datas":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    .line 156
    .restart local v12    # "datas":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v5, "title"

    const v7, 0x7f0b00e7

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v12, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v5, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/MainActivity;->getCurrentFragmentID()I

    move-result v5

    const/16 v7, 0x9

    if-ne v5, v7, :cond_a

    .line 159
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    .line 160
    .local v23, "nearbyPath":Ljava/lang/StringBuilder;
    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    const/16 v5, 0x2f

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 162
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->getFileName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    const-string v5, "content"

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v12, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    .end local v23    # "nearbyPath":Ljava/lang/StringBuilder;
    :goto_1
    invoke-virtual {v6, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    :cond_7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v0, v25

    invoke-static {v5, v0}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isOMADrmFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 175
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f0b00e8

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    .line 177
    .local v26, "rightStatus":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v0, v25

    invoke-static {v5, v0}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isForwardable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 179
    new-instance v12, Ljava/util/HashMap;

    .end local v12    # "datas":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    .line 181
    .restart local v12    # "datas":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v5, "title"

    move-object/from16 v0, v26

    invoke-virtual {v12, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    const-string v5, "content"

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b00e9

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v12, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    invoke-virtual {v6, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 361
    .end local v26    # "rightStatus":Ljava/lang/String;
    :cond_8
    new-instance v4, Landroid/widget/SimpleAdapter;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const v7, 0x7f040024

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v28, "title"

    aput-object v28, v8, v9

    const/4 v9, 0x1

    const-string v28, "content"

    aput-object v28, v8, v9

    const/4 v9, 0x2

    new-array v9, v9, [I

    fill-array-data v9, :array_0

    invoke-direct/range {v4 .. v9}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 368
    .local v4, "adapter":Landroid/widget/SimpleAdapter;
    new-instance v11, Landroid/app/AlertDialog$Builder;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v11, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 370
    .local v11, "builder":Landroid/app/AlertDialog$Builder;
    const v5, 0x7f0b0025

    invoke-virtual {v11, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 372
    const/4 v5, 0x0

    invoke-virtual {v11, v4, v5}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 374
    const v5, 0x7f0b0015

    new-instance v7, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment$1;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;)V

    invoke-virtual {v11, v5, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 383
    invoke-virtual {v11}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v13

    .line 385
    .local v13, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v13}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v5

    if-eqz v5, :cond_9

    .line 386
    invoke-virtual {v13}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v5

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Landroid/widget/ListView;->setFocusable(Z)V

    .line 387
    invoke-virtual {v13}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v5

    new-instance v7, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment$2;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment$2;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;)V

    invoke-virtual {v5, v7}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 405
    .end local v4    # "adapter":Landroid/widget/SimpleAdapter;
    .end local v6    # "dataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v11    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v12    # "datas":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v13    # "dialog":Landroid/app/AlertDialog;
    .end local v16    # "fileLength":J
    .end local v22    # "index":I
    .end local v24    # "nf":Ljava/text/NumberFormat;
    .end local v25    # "path":Ljava/lang/String;
    .end local v27    # "sb":Ljava/lang/StringBuilder;
    :cond_9
    :goto_2
    return-object v13

    .line 165
    .restart local v6    # "dataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    .restart local v12    # "datas":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v16    # "fileLength":J
    .restart local v22    # "index":I
    .restart local v24    # "nf":Ljava/text/NumberFormat;
    .restart local v25    # "path":Ljava/lang/String;
    .restart local v27    # "sb":Ljava/lang/StringBuilder;
    :cond_a
    const-string v5, "content"

    move-object/from16 v0, v25

    invoke-virtual {v12, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 405
    .end local v6    # "dataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v12    # "datas":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v16    # "fileLength":J
    .end local v22    # "index":I
    .end local v24    # "nf":Ljava/text/NumberFormat;
    .end local v25    # "path":Ljava/lang/String;
    .end local v27    # "sb":Ljava/lang/StringBuilder;
    :cond_b
    invoke-super/range {p0 .. p1}, Landroid/app/DialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v13

    goto :goto_2

    .line 361
    :array_0
    .array-data 4
        0x1020014
        0x1020015
    .end array-data
.end method
