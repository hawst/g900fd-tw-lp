.class public abstract Lcom/sec/android/app/myfiles/compression/AbsCompressor;
.super Ljava/lang/Object;
.source "AbsCompressor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/compression/AbsCompressor$OnOperationListener;
    }
.end annotation


# static fields
.field public static final RESULT_ERROR_CANNOT_OPEN_ZIP:I = 0x1

.field public static final RESULT_ERROR_NOT_SUPPORT_ENCODING:I = 0x0

.field public static final RESULT_NONE:I = -0x1


# instance fields
.field protected mCurrentCounter:I

.field protected mCurrentSize:J

.field protected mFilesOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

.field protected mOperationListener:Lcom/sec/android/app/myfiles/compression/AbsCompressor$OnOperationListener;

.field protected mProgressBar:Landroid/widget/ProgressBar;

.field protected mProgressBarCounter:Landroid/widget/TextView;

.field protected mProgressBarPercent:Landroid/widget/TextView;

.field private mProgressPercentHandler:Landroid/os/Handler;

.field protected mTotalFilesCount:I

.field protected mTotalSize:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object v0, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mFilesOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    .line 41
    iput-object v0, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mProgressBar:Landroid/widget/ProgressBar;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mProgressBarPercent:Landroid/widget/TextView;

    .line 45
    iput-object v0, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mProgressBarCounter:Landroid/widget/TextView;

    .line 47
    iput-wide v2, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mTotalSize:J

    .line 49
    iput-wide v2, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mCurrentSize:J

    .line 51
    iput v1, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mCurrentCounter:I

    .line 53
    iput v1, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mTotalFilesCount:I

    .line 180
    new-instance v0, Lcom/sec/android/app/myfiles/compression/AbsCompressor$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/compression/AbsCompressor$1;-><init>(Lcom/sec/android/app/myfiles/compression/AbsCompressor;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mProgressPercentHandler:Landroid/os/Handler;

    .line 207
    return-void
.end method


# virtual methods
.method public abstract doCompress(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract doExtract(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method public getTotalItemCount(Ljava/lang/String;)I
    .locals 9
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 139
    const/4 v6, 0x0

    .line 141
    .local v6, "totalCount":I
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    move v7, v6

    .line 176
    .end local v6    # "totalCount":I
    .local v7, "totalCount":I
    :goto_0
    return v7

    .line 146
    .end local v7    # "totalCount":I
    .restart local v6    # "totalCount":I
    :cond_0
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 148
    .local v5, "target":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_1

    move v7, v6

    .line 150
    .end local v6    # "totalCount":I
    .restart local v7    # "totalCount":I
    goto :goto_0

    .line 154
    .end local v7    # "totalCount":I
    .restart local v6    # "totalCount":I
    :cond_1
    add-int/lit8 v6, v6, 0x1

    .line 156
    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 158
    invoke-virtual {v5}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 160
    .local v2, "children":[Ljava/io/File;
    if-eqz v2, :cond_3

    .line 162
    move-object v0, v2

    .local v0, "arr$":[Ljava/io/File;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_3

    aget-object v1, v0, v3

    .line 164
    .local v1, "child":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 166
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->getTotalItemCount(Ljava/lang/String;)I

    move-result v8

    add-int/2addr v6, v8

    .line 162
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 170
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "child":Ljava/io/File;
    .end local v2    # "children":[Ljava/io/File;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :cond_3
    move v7, v6

    .line 176
    .end local v6    # "totalCount":I
    .restart local v7    # "totalCount":I
    goto :goto_0
.end method

.method public getTotalItemCount(Ljava/util/ArrayList;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 121
    .local p1, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gtz v3, :cond_2

    .line 123
    :cond_0
    const/4 v2, 0x0

    .line 133
    :cond_1
    return v2

    .line 126
    :cond_2
    const/4 v2, 0x0

    .line 128
    .local v2, "totalCount":I
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 130
    .local v1, "path":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->getTotalItemCount(Ljava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 131
    goto :goto_0
.end method

.method public isCancelled()Z
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mFilesOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mFilesOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    const/4 v0, 0x1

    .line 67
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFileOperationTask(Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;)V
    .locals 0
    .param p1, "fileOperationTask"    # Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mFilesOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    .line 75
    return-void
.end method

.method public setOnOperationListener(Lcom/sec/android/app/myfiles/compression/AbsCompressor$OnOperationListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/myfiles/compression/AbsCompressor$OnOperationListener;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mOperationListener:Lcom/sec/android/app/myfiles/compression/AbsCompressor$OnOperationListener;

    .line 117
    return-void
.end method

.method protected setProgress(J)V
    .locals 7
    .param p1, "curFileSize"    # J

    .prologue
    const/4 v6, 0x0

    .line 97
    iget-object v1, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v1, :cond_0

    iget-wide v2, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mTotalSize:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 99
    iget-wide v2, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mCurrentSize:J

    add-long/2addr v2, p1

    iput-wide v2, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mCurrentSize:J

    .line 101
    iget-wide v2, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mCurrentSize:J

    long-to-double v2, v2

    iget-wide v4, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mTotalSize:J

    long-to-double v4, v4

    div-double/2addr v2, v4

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    mul-double/2addr v2, v4

    double-to-int v0, v2

    .line 103
    .local v0, "progress":I
    iget-object v1, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 105
    const/16 v1, 0x64

    if-gt v0, v1, :cond_1

    .line 107
    iget-object v1, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mProgressPercentHandler:Landroid/os/Handler;

    invoke-virtual {v1, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 108
    iget-object v1, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mProgressPercentHandler:Landroid/os/Handler;

    invoke-virtual {v1, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 112
    .end local v0    # "progress":I
    :cond_0
    :goto_0
    return-void

    .line 110
    .restart local v0    # "progress":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mProgressPercentHandler:Landroid/os/Handler;

    invoke-virtual {v1, v6}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method

.method public setProgressBar(Landroid/widget/ProgressBar;)V
    .locals 0
    .param p1, "progressBar"    # Landroid/widget/ProgressBar;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mProgressBar:Landroid/widget/ProgressBar;

    .line 81
    return-void
.end method

.method public setProgressBarCounter(Landroid/widget/TextView;)V
    .locals 0
    .param p1, "countText"    # Landroid/widget/TextView;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mProgressBarCounter:Landroid/widget/TextView;

    .line 93
    return-void
.end method

.method public setProgressBarPercent(Landroid/widget/TextView;)V
    .locals 0
    .param p1, "percent"    # Landroid/widget/TextView;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mProgressBarPercent:Landroid/widget/TextView;

    .line 87
    return-void
.end method
