.class Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$3;
.super Ljava/lang/Object;
.source "UserBrowserFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;)V
    .locals 0

    .prologue
    .line 1023
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 10
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1027
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v5, 0x0

    const-string v6, "UserBrowserFragment"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onItemClick() : parent = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1029
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v5

    invoke-interface {v5, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 1031
    .local v0, "cursor":Landroid/database/Cursor;
    const-string v5, "_data"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1034
    .local v3, "path":Ljava/lang/String;
    const-string v5, "format"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    const/16 v6, 0x3001

    if-ne v5, v6, :cond_3

    .line 1036
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->isSelectMode()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v5

    if-lez v5, :cond_0

    .line 1037
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->unselectAllItem()V

    .line 1039
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    # invokes: Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->moveToFolder(Ljava/lang/String;Ljava/lang/Boolean;)V
    invoke-static {v5, v3, v6}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1040
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isCheckMountedStorage()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1041
    const/4 v2, 0x0

    .line 1043
    .local v2, "menuItem":Landroid/view/MenuItem;
    sget-object v5, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mOptionsMenu:Landroid/view/Menu;

    const v6, 0x7f0f0148

    invoke-interface {v5, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1044
    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isRootFolder(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    const/4 v5, 0x1

    :goto_0
    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1084
    .end local v2    # "menuItem":Landroid/view/MenuItem;
    :cond_1
    :goto_1
    return-void

    .line 1044
    .restart local v2    # "menuItem":Landroid/view/MenuItem;
    :cond_2
    const/4 v5, 0x0

    goto :goto_0

    .line 1049
    .end local v2    # "menuItem":Landroid/view/MenuItem;
    :cond_3
    const/4 v4, 0x0

    .line 1050
    .local v4, "selectionButton":Landroid/view/View;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    iget v5, v5, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mRunFrom:I

    const/16 v6, 0x15

    if-ne v5, v6, :cond_5

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    iget-boolean v5, v5, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mJustSelectMode:Z

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getSelectionType()I

    move-result v5

    if-nez v5, :cond_5

    .line 1052
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getSelectionType()I

    move-result v5

    if-nez v5, :cond_4

    .line 1054
    const v5, 0x7f0f0038

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 1058
    :cond_4
    if-eqz v4, :cond_1

    .line 1059
    invoke-virtual {v4}, Landroid/view/View;->performClick()Z

    goto :goto_1

    .line 1062
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    iget v5, v5, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mRunFrom:I

    const/16 v6, 0x15

    if-ne v5, v6, :cond_6

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getSelectionType()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_6

    .line 1064
    const v5, 0x7f0f0037

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 1066
    if-eqz v4, :cond_1

    .line 1067
    invoke-virtual {v4}, Landroid/view/View;->performClick()Z

    goto :goto_1

    .line 1072
    :cond_6
    invoke-static {v3}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v1

    .line 1073
    .local v1, "fileTypeInt":I
    invoke-static {v1}, Lcom/sec/android/app/myfiles/MediaFile;->isArchiveFileType(I)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1074
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    const/4 v6, 0x1

    # setter for: Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mExtractExecuteNormalMode:Z
    invoke-static {v5, v6}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->access$102(Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;Z)Z

    .line 1075
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    # setter for: Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mSelectedPathForExtract:Ljava/lang/String;
    invoke-static {v5, v3}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->access$202(Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 1076
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    # invokes: Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->createFolderForExtract(Ljava/lang/String;)Z
    invoke-static {v5, v3}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;Ljava/lang/String;)Z

    goto :goto_1

    .line 1079
    :cond_7
    const/16 v5, 0x201

    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getCurrentSortBy()I

    move-result v9

    invoke-static {v5, v6, v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/FileUtils;->openFile(ILjava/io/File;Landroid/app/Activity;ZI)V

    goto :goto_1
.end method
