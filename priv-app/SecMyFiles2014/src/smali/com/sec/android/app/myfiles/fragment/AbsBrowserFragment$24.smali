.class Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;
.super Ljava/lang/Object;
.source "AbsBrowserFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onCreateTreeViewItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V
    .locals 0

    .prologue
    .line 4802
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 4807
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    invoke-virtual {v4, p3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/element/TreeViewItem;

    .line 4809
    .local v3, "item":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    iget v4, v3, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    const/16 v5, 0xd

    if-ne v4, v5, :cond_0

    .line 4810
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->getFTPShortCutCount(Landroid/content/Context;)I

    move-result v4

    if-lez v4, :cond_0

    .line 4811
    const/16 v4, 0x11

    iput v4, v3, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    .line 4816
    :cond_0
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isCheckMountedStorage()Z

    move-result v4

    if-nez v4, :cond_2

    if-nez p3, :cond_2

    iget-object v4, v3, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mPath:Ljava/lang/String;

    if-eqz v4, :cond_2

    iget-object v4, v3, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mPath:Ljava/lang/String;

    const-string v5, "/storage"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 4923
    :cond_1
    :goto_0
    return-void

    .line 4821
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    invoke-virtual {v4, v3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->isCurrentItem(Lcom/sec/android/app/myfiles/element/TreeViewItem;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 4823
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    invoke-virtual {v4, v3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->toggleExpandAndFold(Lcom/sec/android/app/myfiles/element/TreeViewItem;)Z

    .line 4826
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectModeFrom()I

    move-result v4

    const/4 v5, 0x2

    if-eq v4, v5, :cond_4

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectModeFrom()I

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectModeFrom()I

    move-result v4

    const/4 v5, 0x5

    if-ne v4, v5, :cond_5

    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->isSelectMode()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 4829
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->finishSelectMode()V

    .line 4831
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->saveTreeYPosition()V

    .line 4833
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 4835
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 4837
    .local v2, "intent":Landroid/content/Intent;
    if-eqz v2, :cond_6

    const-string v4, "android.intent.action.VIEW_DOWNLOADS"

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 4839
    const-string v4, ""

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 4842
    :cond_6
    if-eqz v2, :cond_7

    const-string v4, "remote_share_viewer"

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 4844
    const-string v4, "remote_share_viewer"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 4848
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_7
    iget v4, v3, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    if-nez v4, :cond_a

    .line 4849
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-nez v4, :cond_8

    .line 4851
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 4853
    .local v0, "argument":Landroid/os/Bundle;
    const-string v4, "FOLDERPATH"

    iget-object v5, v3, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mPath:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4855
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v4, Lcom/sec/android/app/myfiles/MainActivity;

    const/16 v5, 0x201

    invoke-virtual {v4, v5, v0}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto/16 :goto_0

    .line 4857
    .end local v0    # "argument":Landroid/os/Bundle;
    :cond_8
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    instance-of v4, v4, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    if-nez v4, :cond_9

    .line 4858
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 4860
    .restart local v0    # "argument":Landroid/os/Bundle;
    const-string v4, "FOLDERPATH"

    iget-object v5, v3, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mPath:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4862
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v4, Lcom/sec/android/app/myfiles/MainActivity;

    const/16 v5, 0x201

    invoke-virtual {v4, v5, v0}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto/16 :goto_0

    .line 4864
    .end local v0    # "argument":Landroid/os/Bundle;
    :cond_9
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    invoke-virtual {v4, v3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->isCurrentItem(Lcom/sec/android/app/myfiles/element/TreeViewItem;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 4866
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    invoke-virtual {v4, p3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->setCurrentPosition(I)V

    .line 4868
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    iget-object v5, v3, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mPath:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z

    .line 4869
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->setListOrGridPosition(I)V

    goto/16 :goto_0

    .line 4876
    :cond_a
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v4, v4, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v4, :cond_1

    .line 4878
    iget-boolean v4, v3, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mEnabled:Z

    if-eqz v4, :cond_1

    .line 4881
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    invoke-virtual {v4, p3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->setCurrentPosition(I)V

    .line 4883
    iget v4, v3, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    const/16 v5, 0x8

    if-eq v4, v5, :cond_b

    iget v4, v3, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    const/16 v5, 0x1f

    if-ne v4, v5, :cond_d

    :cond_b
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->isWifiOn(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_d

    .line 4885
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckDropBox()Z

    move-result v4

    if-nez v4, :cond_d

    .line 4887
    iget v4, v3, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    const/16 v5, 0x1f

    if-ne v4, v5, :cond_c

    .line 4888
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->showDialog(I)V

    goto/16 :goto_0

    .line 4890
    :cond_c
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->showDialog(I)V

    goto/16 :goto_0

    .line 4894
    :cond_d
    iget v4, v3, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    const/16 v5, 0x11

    if-ne v4, v5, :cond_e

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->isWifiOn(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_e

    .line 4896
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckFTP()Z

    move-result v4

    if-nez v4, :cond_e

    .line 4898
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->showDialog(I)V

    goto/16 :goto_0

    .line 4902
    :cond_e
    const/4 v0, 0x0

    .line 4903
    .restart local v0    # "argument":Landroid/os/Bundle;
    iget v4, v3, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    const/16 v5, 0x9

    if-ne v4, v5, :cond_f

    .line 4904
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "argument":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 4905
    .restart local v0    # "argument":Landroid/os/Bundle;
    const-string v4, "FOLDERPATH"

    const-string v5, "devices_list"

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4908
    :cond_f
    iget v4, v3, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    const/16 v5, 0x26

    if-eq v4, v5, :cond_10

    iget v4, v3, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    const/16 v5, 0x27

    if-ne v4, v5, :cond_11

    .line 4910
    :cond_10
    const/16 v1, 0x25

    .line 4911
    .local v1, "categoryType":I
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "argument":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 4912
    .restart local v0    # "argument":Landroid/os/Bundle;
    const-string v4, "REMOTE_SHARE_DIR"

    iget v5, v3, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 4913
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v4, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v4, v1, v0}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto/16 :goto_0

    .line 4917
    .end local v1    # "categoryType":I
    :cond_11
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->notifyDataSetChanged()V

    .line 4919
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v4, Lcom/sec/android/app/myfiles/MainActivity;

    iget v5, v3, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    invoke-virtual {v4, v5, v0}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto/16 :goto_0
.end method
