.class Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$10;
.super Ljava/lang/Object;
.source "FileBrowserFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->moveToFolder(Ljava/lang/String;Ljava/lang/Boolean;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

.field final synthetic val$isBack:Ljava/lang/Boolean;

.field final synthetic val$path:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 1685
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iput-object p2, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$10;->val$path:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$10;->val$isBack:Ljava/lang/Boolean;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1689
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$10;->val$path:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setCurrentDirectory(Ljava/lang/String;)V

    .line 1690
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$10;->val$path:Ljava/lang/String;

    iput-object v3, v2, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mCurrentFolder:Ljava/lang/String;

    .line 1691
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mLoadingHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1692
    .local v0, "msg":Landroid/os/Message;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v1

    .line 1693
    .local v1, "tempCurrentFolder":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$10;->val$path:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->setCurrentPath(Ljava/lang/String;)V

    .line 1694
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mTempCursor:Landroid/database/Cursor;

    .line 1695
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->setCurrentPath(Ljava/lang/String;)V

    .line 1696
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$10;->val$path:Ljava/lang/String;

    iput-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1697
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$10;->val$isBack:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    # setter for: Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mIsBack:Z
    invoke-static {v2, v3}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->access$702(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;Z)Z

    .line 1698
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mLoadingHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1699
    return-void
.end method
