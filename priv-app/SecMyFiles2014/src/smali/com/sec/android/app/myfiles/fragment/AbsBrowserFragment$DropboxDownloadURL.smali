.class Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$DropboxDownloadURL;
.super Landroid/os/AsyncTask;
.source "AbsBrowserFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DropboxDownloadURL"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field intent:Landroid/content/Intent;

.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V
    .locals 2

    .prologue
    .line 6519
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$DropboxDownloadURL;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 6521
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$DropboxDownloadURL;->intent:Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 6519
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$DropboxDownloadURL;->doInBackground([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    .line 6559
    const/4 v2, 0x0

    .line 6563
    .local v2, "mDownLoadURL":Ljava/lang/String;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$DropboxDownloadURL;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v3}, Lcom/samsung/scloud/DropboxAPI;->getInstance(Landroid/content/Context;)Lcom/samsung/scloud/DropboxAPI;

    move-result-object v0

    .line 6565
    .local v0, "dAPI":Lcom/samsung/scloud/DropboxAPI;
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {v0, v3}, Lcom/samsung/scloud/DropboxAPI;->getDownloadURL(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 6572
    .end local v0    # "dAPI":Lcom/samsung/scloud/DropboxAPI;
    :goto_0
    return-object v2

    .line 6567
    :catch_0
    move-exception v1

    .line 6569
    .local v1, "e":Lcom/samsung/scloud/exception/SCloudException;
    invoke-virtual {v1}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V

    goto :goto_0
.end method

.method protected onCanceled()V
    .locals 1

    .prologue
    .line 6592
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$DropboxDownloadURL;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mWaitProgressDlg:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$1000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 6594
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$DropboxDownloadURL;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mWaitProgressDlg:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$1000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->dismissWaitProgressDialog()V

    .line 6596
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$DropboxDownloadURL;->cancel(Z)Z

    .line 6598
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 6519
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$DropboxDownloadURL;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 4
    .param p1, "dropboxURL"    # Ljava/lang/String;

    .prologue
    .line 6579
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$DropboxDownloadURL;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mWaitProgressDlg:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$1000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->dismissWaitProgressDialog()V

    .line 6581
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$DropboxDownloadURL;->intent:Landroid/content/Intent;

    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 6583
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$DropboxDownloadURL;->intent:Landroid/content/Intent;

    const-string v1, "from-myfiles"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 6585
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$DropboxDownloadURL;->intent:Landroid/content/Intent;

    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6587
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$DropboxDownloadURL;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$DropboxDownloadURL;->intent:Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$DropboxDownloadURL;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b001f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startActivity(Landroid/content/Intent;)V

    .line 6588
    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 6535
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$DropboxDownloadURL;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mWaitProgressDlg:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$1000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    move-result-object v0

    if-nez v0, :cond_0

    .line 6537
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$DropboxDownloadURL;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    new-instance v1, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$DropboxDownloadURL;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-direct {v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mWaitProgressDlg:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$1002(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;Lcom/sec/android/app/myfiles/utils/MyFilesDialog;)Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    .line 6544
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$DropboxDownloadURL;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mWaitProgressDlg:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$1000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->setDialogInstance(Lcom/sec/android/app/myfiles/utils/MyFilesDialog;)V

    .line 6546
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$DropboxDownloadURL;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mWaitProgressDlg:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$1000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    move-result-object v0

    const-string v1, "Create Link..."

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->setDisplayString(Ljava/lang/String;)V

    .line 6548
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$DropboxDownloadURL;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mWaitProgressDlg:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$1000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->showWaitProgressDialog()V

    .line 6553
    return-void

    .line 6541
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$DropboxDownloadURL;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mWaitProgressDlg:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$1000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->dismissWaitProgressDialog()V

    goto :goto_0
.end method
