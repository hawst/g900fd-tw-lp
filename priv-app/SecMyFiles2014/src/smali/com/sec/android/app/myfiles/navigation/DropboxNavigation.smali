.class public Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;
.super Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
.source "DropboxNavigation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$DropboxAsyncOpenOperationFragment;,
        Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$AsyncOpenRunnable;
    }
.end annotation


# static fields
.field private static final AUTHTOKEN_KEY:Ljava/lang/String; = "com.sec.android.cloudagent.dropbox.credential.tokenkey"

.field private static final AUTHTOKEN_SECRET:Ljava/lang/String; = "com.sec.android.cloudagent.dropbox.credential.tokensecret"

.field private static final CLOUD_ACTION_REQUEST_SIGNIN:Ljava/lang/String; = "com.sec.android.cloudagent.ACTION_REQUEST_SIGNIN"

.field private static final CLOUD_ACTION_RESPONSE_TOKEN:Ljava/lang/String; = "com.sec.android.cloudagent.ACTION_RESPONSE_TOKEN"

.field private static final CLOUD_ACTION_SIGNED_IN:Ljava/lang/String; = "com.sec.android.cloudagent.ACTION_ACCOUNT_SIGNED_IN"

.field private static final CLOUD_EXTRA_RESULT:Ljava/lang/String; = "result"

.field private static final FOLDER_DATA_INDEX:I = 0x1

.field private static final FOLDER_ID_INDEX:I = 0x0

.field private static final MODULE:Ljava/lang/String; = "DropboxNavigation"

.field private static final TAG:Ljava/lang/String; = "DropboxAPIHelper"

.field private static final mFilesProjection:[Ljava/lang/String;

.field private static final mFolderProjection:[Ljava/lang/String;


# instance fields
.field private CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

.field private Depth:I

.field public cloudLoginReceiver:Landroid/content/BroadcastReceiver;

.field public mAsyncDBtask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mCurrentFolderID:I

.field private mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

.field private mFileUri:Landroid/net/Uri;

.field private mIsSyncing:Z

.field private mLoginReceiverIntentFilter:Landroid/content/IntentFilter;

.field private mLoginReceiverRegistered:Z

.field private mResigninRequested:Z

.field private mbAbort:Z

.field private mbGotoRoot:Z

.field private mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

.field private showDialog:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 101
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "_data"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mFolderProjection:[Ljava/lang/String;

    .line 106
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "_data"

    aput-object v1, v0, v3

    const-string v1, "date_modified"

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mFilesProjection:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 113
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;-><init>()V

    .line 69
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->Depth:I

    .line 88
    iput-object v2, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    .line 90
    iput-object v2, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->cloudLoginReceiver:Landroid/content/BroadcastReceiver;

    .line 92
    iput-object v2, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    .line 94
    iput-object v2, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mLoginReceiverIntentFilter:Landroid/content/IntentFilter;

    .line 96
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mLoginReceiverRegistered:Z

    .line 97
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->showDialog:Z

    .line 98
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mResigninRequested:Z

    .line 99
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mIsSyncing:Z

    .line 241
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mbGotoRoot:Z

    .line 114
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 117
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;-><init>(Landroid/content/Context;I)V

    .line 69
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->Depth:I

    .line 88
    iput-object v2, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    .line 90
    iput-object v2, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->cloudLoginReceiver:Landroid/content/BroadcastReceiver;

    .line 92
    iput-object v2, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    .line 94
    iput-object v2, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mLoginReceiverIntentFilter:Landroid/content/IntentFilter;

    .line 96
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mLoginReceiverRegistered:Z

    .line 97
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->showDialog:Z

    .line 98
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mResigninRequested:Z

    .line 99
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mIsSyncing:Z

    .line 241
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mbGotoRoot:Z

    .line 119
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->setAllAttachMode(Z)V

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mContentResolver:Landroid/content/ContentResolver;

    .line 123
    iput v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mCurrentFolderID:I

    .line 126
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mLoginReceiverIntentFilter:Landroid/content/IntentFilter;

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mLoginReceiverIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.cloudagent.ACTION_ACCOUNT_SIGNED_IN"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mLoginReceiverIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.sec.android.cloudagent.ACTION_RESPONSE_TOKEN"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 130
    new-instance v0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$1;-><init>(Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->cloudLoginReceiver:Landroid/content/BroadcastReceiver;

    .line 159
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->registerCloudLoginReceiver()V

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    .line 161
    return-void
.end method

.method private NewOpenAsyncTask()Landroid/os/AsyncTask;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 561
    new-instance v0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$2;-><init>(Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;)V

    .line 578
    .local v0, "task":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/Void;Ljava/lang/Void;Ljava/lang/Void;>;"
    return-object v0
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;)Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->actionsOnSignIn()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mResigninRequested:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;
    .param p1, "x1"    # Z

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mResigninRequested:Z

    return p1
.end method

.method private actionsOnSignIn()V
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->requestGetToken()V

    .line 179
    return-void
.end method

.method private createExceptFilteringWhere()Ljava/lang/String;
    .locals 4

    .prologue
    .line 902
    const-string v0, ""

    .line 904
    .local v0, "filteringWhere":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mExceptFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 907
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_data NOT LIKE ? AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 904
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 910
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 912
    const/4 v2, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 920
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " OR format = ? "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 922
    return-object v0

    .line 917
    :cond_1
    const-string v0, "_data NOT LIKE ?"

    goto :goto_1
.end method

.method private createExceptFilteringWhereArgs()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 927
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 929
    .local v1, "extensionResultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mExceptFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 932
    .local v0, "extension":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "%."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 935
    .end local v0    # "extension":Ljava/lang/String;
    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 938
    const-string v3, "%."

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 941
    :cond_1
    const/16 v3, 0x3001

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 943
    return-object v1
.end method

.method private createFilteringWhere()Ljava/lang/String;
    .locals 4

    .prologue
    .line 856
    const-string v0, ""

    .line 858
    .local v0, "filteringWhere":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 861
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_data LIKE ? OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 858
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 864
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 866
    const/4 v2, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 874
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " OR format = ? "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 876
    return-object v0

    .line 871
    :cond_1
    const-string v0, "_data LIKE ?"

    goto :goto_1
.end method

.method private createFilteringWhereArgs()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 881
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 883
    .local v1, "extensionResultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 886
    .local v0, "extension":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "%."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 889
    .end local v0    # "extension":Ljava/lang/String;
    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 892
    const-string v3, "%."

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 895
    :cond_1
    const/16 v3, 0x3001

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 897
    return-object v1
.end method

.method private getDepthCount(Ljava/lang/String;)I
    .locals 3
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 538
    const/4 v0, 0x1

    .line 540
    .local v0, "Count":I
    if-eqz p1, :cond_0

    const-string v2, ""

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 541
    :cond_0
    const/4 v2, 0x1

    .line 553
    :goto_0
    return v2

    .line 546
    :cond_1
    const-string v2, "/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 548
    .local v1, "split":[Ljava/lang/String;
    array-length v2, v1

    if-gtz v2, :cond_2

    .line 549
    const/4 v0, 0x1

    :goto_1
    move v2, v0

    .line 553
    goto :goto_0

    .line 551
    :cond_2
    array-length v0, v1

    goto :goto_1
.end method

.method private getFilesInPath(Ljava/lang/String;III)Landroid/database/Cursor;
    .locals 16
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "sortBy"    # I
    .param p3, "inOrder"    # I
    .param p4, "target"    # I

    .prologue
    .line 356
    const/4 v7, 0x0

    .line 358
    .local v7, "where":Ljava/lang/String;
    const/4 v8, 0x0

    .line 360
    .local v8, "whereArgs":[Ljava/lang/String;
    const/4 v4, 0x0

    move/from16 v0, p2

    move/from16 v1, p3

    move/from16 v2, p4

    invoke-static {v0, v1, v2, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesDB;->getOrderBy(IIIZ)Ljava/lang/String;

    move-result-object v9

    .line 362
    .local v9, "orderBy":Ljava/lang/String;
    const-string v13, "format=12289"

    .line 364
    .local v13, "onlyFolder":Ljava/lang/String;
    const-string v12, "NOT format=12289"

    .line 367
    .local v12, "onlyFile":Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isAbsoluteRootFolder(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 369
    if-eqz p2, :cond_0

    if-eqz p2, :cond_3

    if-nez p4, :cond_3

    .line 371
    :cond_0
    new-instance v11, Landroid/database/MatrixCursor;

    sget-object v4, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mFilesProjection:[Ljava/lang/String;

    invoke-direct {v11, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 373
    .local v11, "mc":Landroid/database/MatrixCursor;
    new-instance v10, Ljava/io/File;

    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getRootFolder()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v10, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 375
    .local v10, "file":Ljava/io/File;
    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "0"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const/16 v6, 0x3001

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-virtual {v10}, Ljava/io/File;->lastModified()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v11, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 377
    sget-boolean v4, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v4, :cond_1

    .line 379
    new-instance v10, Ljava/io/File;

    .end local v10    # "file":Ljava/io/File;
    sget-object v4, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardStoragePath:Ljava/lang/String;

    invoke-direct {v10, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 381
    .restart local v10    # "file":Ljava/io/File;
    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "1"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "/storage/extSdCard"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const/16 v6, 0x3001

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-virtual {v10}, Ljava/io/File;->lastModified()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v11, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 425
    .end local v10    # "file":Ljava/io/File;
    .end local v11    # "mc":Landroid/database/MatrixCursor;
    :cond_1
    :goto_0
    return-object v11

    .line 387
    :cond_2
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isInternalRootFolder(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 389
    const-string v7, "_data LIKE ? AND parent=0"

    .line 391
    const/4 v4, 0x1

    new-array v8, v4, [Ljava/lang/String;

    .end local v8    # "whereArgs":[Ljava/lang/String;
    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v8, v4

    .line 411
    .restart local v8    # "whereArgs":[Ljava/lang/String;
    :cond_3
    :goto_1
    packed-switch p4, :pswitch_data_0

    .line 425
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "external"

    invoke-static {v5}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    goto :goto_0

    .line 393
    :cond_4
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isExternalRootFolder(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 395
    const-string v7, "_data LIKE ? AND parent=0"

    .line 397
    const/4 v4, 0x1

    new-array v8, v4, [Ljava/lang/String;

    .end local v8    # "whereArgs":[Ljava/lang/String;
    const/4 v4, 0x0

    const-string v5, "/storage/extSdCard%"

    aput-object v5, v8, v4

    .restart local v8    # "whereArgs":[Ljava/lang/String;
    goto :goto_1

    .line 399
    :cond_5
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxRootFolder(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 401
    const-string v7, "_data LIKE ? AND parent=0"

    .line 403
    const/4 v4, 0x1

    new-array v8, v4, [Ljava/lang/String;

    .end local v8    # "whereArgs":[Ljava/lang/String;
    const/4 v4, 0x0

    const-string v5, "/storage/SecretBox%"

    aput-object v5, v8, v4

    .restart local v8    # "whereArgs":[Ljava/lang/String;
    goto :goto_1

    .line 407
    :cond_6
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "parent="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mCurrentFolderID:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    .line 414
    :pswitch_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 415
    goto :goto_2

    .line 418
    :pswitch_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 419
    goto :goto_2

    .line 411
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getParentPathInDropbox()Ljava/lang/String;
    .locals 3

    .prologue
    .line 518
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 519
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mCurrentFolderPath:Ljava/lang/String;

    const/16 v2, 0x2f

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 520
    .local v0, "pos":I
    if-ltz v0, :cond_0

    .line 521
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mCurrentFolderPath:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 524
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 526
    .end local v0    # "pos":I
    :goto_1
    return-object v1

    .line 523
    .restart local v0    # "pos":I
    :cond_0
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mCurrentFolderPath:Ljava/lang/String;

    goto :goto_0

    .line 526
    .end local v0    # "pos":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mCurrentFolderPath:Ljava/lang/String;

    goto :goto_1
.end method

.method private declared-synchronized registerCloudLoginReceiver()V
    .locals 3

    .prologue
    .line 164
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mLoginReceiverIntentFilter:Landroid/content/IntentFilter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->cloudLoginReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mLoginReceiverRegistered:Z

    if-nez v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->cloudLoginReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mLoginReceiverIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 166
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mLoginReceiverRegistered:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 168
    :cond_0
    monitor-exit p0

    return-void

    .line 164
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized unregisterCloudLoginReceiver()V
    .locals 2

    .prologue
    .line 171
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->cloudLoginReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mLoginReceiverRegistered:Z

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->cloudLoginReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 173
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mLoginReceiverRegistered:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 175
    :cond_0
    monitor-exit p0

    return-void

    .line 171
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public AsyncOpen()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 601
    new-instance v2, Lcom/sec/android/app/myfiles/db/CacheDB;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mContext:Landroid/content/Context;

    invoke-direct {v2, v6}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    .line 602
    .local v2, "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 603
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/db/CacheDB;->getDropboxCursor()Ljava/lang/String;

    move-result-object v3

    .line 604
    .local v3, "delta":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 607
    if-nez v3, :cond_3

    .line 609
    new-instance v1, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$DropboxAsyncOpenOperationFragment;

    invoke-direct {v1}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$DropboxAsyncOpenOperationFragment;-><init>()V

    .line 611
    .local v1, "asyncUi":Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$DropboxAsyncOpenOperationFragment;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v1, v6}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$DropboxAsyncOpenOperationFragment;->setParentFragment(Lcom/sec/android/app/myfiles/fragment/AbsFragment;)V

    .line 613
    invoke-virtual {v1, v9}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$DropboxAsyncOpenOperationFragment;->setCancelButtonActive(Z)V

    .line 615
    invoke-virtual {v1, v9}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$DropboxAsyncOpenOperationFragment;->setWaitOnCancel(Z)V

    .line 617
    new-instance v6, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$AsyncOpenRunnable;

    invoke-direct {v6, p0, v1}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$AsyncOpenRunnable;-><init>(Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;)V

    invoke-virtual {v1, v6}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$DropboxAsyncOpenOperationFragment;->setAsyncOpenRunnable(Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$AsyncOpenRunnable;)V

    .line 619
    new-instance v6, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$3;

    invoke-direct {v6, p0}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$3;-><init>(Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;)V

    invoke-virtual {v1, v6}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$DropboxAsyncOpenOperationFragment;->setCancelOperation(Ljava/lang/Runnable;)V

    .line 630
    iput-object v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    .line 632
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v6, :cond_2

    .line 633
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    .line 634
    .local v5, "fManager":Landroid/app/FragmentManager;
    if-nez v5, :cond_0

    .line 635
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 636
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 637
    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    .line 640
    .end local v0    # "activity":Landroid/app/Activity;
    :cond_0
    if-eqz v5, :cond_1

    .line 641
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    const-string v7, "OpenOperation"

    invoke-virtual {v6, v5, v7}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 642
    :cond_1
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->showDialog:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 677
    .end local v1    # "asyncUi":Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$DropboxAsyncOpenOperationFragment;
    .end local v5    # "fManager":Landroid/app/FragmentManager;
    :cond_2
    :goto_0
    return-void

    .line 643
    .restart local v1    # "asyncUi":Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$DropboxAsyncOpenOperationFragment;
    .restart local v5    # "fManager":Landroid/app/FragmentManager;
    :catch_0
    move-exception v4

    .line 644
    .local v4, "ex":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 645
    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->showDialog:Z

    goto :goto_0

    .line 655
    .end local v1    # "asyncUi":Lcom/sec/android/app/myfiles/navigation/DropboxNavigation$DropboxAsyncOpenOperationFragment;
    .end local v4    # "ex":Ljava/lang/Exception;
    .end local v5    # "fManager":Landroid/app/FragmentManager;
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    if-nez v6, :cond_4

    .line 658
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->NewOpenAsyncTask()Landroid/os/AsyncTask;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    .line 659
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    new-array v7, v8, [Ljava/lang/Void;

    invoke-virtual {v6, v7}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 663
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    invoke-virtual {v6}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v6

    sget-object v7, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v6, v7, :cond_2

    .line 666
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    invoke-virtual {v6}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v6

    sget-object v7, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-ne v6, v7, :cond_2

    .line 668
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->NewOpenAsyncTask()Landroid/os/AsyncTask;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    .line 670
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mAsyncDBtask:Landroid/os/AsyncTask;

    sget-object v7, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v8, v8, [Ljava/lang/Void;

    invoke-virtual {v6, v7, v8}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method protected AsyncOpenWithFullSync()V
    .locals 1

    .prologue
    .line 592
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mbAbort:Z

    .line 594
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->AsyncOpen()V

    .line 596
    return-void
.end method

.method public OnFragmentDestroy()V
    .locals 0

    .prologue
    .line 947
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->unregisterCloudLoginReceiver()V

    .line 948
    return-void
.end method

.method protected abortCurrentOperation()V
    .locals 1

    .prologue
    .line 587
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mbAbort:Z

    .line 588
    return-void
.end method

.method public getCurrentFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 514
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mCurrentFolderPath:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentFolderID()I
    .locals 1

    .prologue
    .line 344
    iget v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mCurrentFolderID:I

    return v0
.end method

.method public getFilesInCurrentFolder()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->getFilesInFolder(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getFilesInFolder(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 436
    iget v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mShowFolderFileType:I

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->getFilesInFolder(Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getFilesInFolder(Ljava/lang/String;I)Landroid/database/Cursor;
    .locals 11
    .param p1, "folderPath"    # Ljava/lang/String;
    .param p2, "showFolderFileType"    # I

    .prologue
    .line 448
    new-instance v1, Lcom/sec/android/app/myfiles/db/CacheDB;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    .line 450
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 451
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->getDepthCount(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->Depth:I

    .line 452
    const/4 v8, 0x0

    .line 454
    .local v8, "c":Landroid/database/Cursor;
    const-string v6, ""

    .line 456
    .local v6, "where":Ljava/lang/String;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 458
    .local v10, "whereArgsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v7, 0x0

    .line 462
    .local v7, "whereArgs":[Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->isEnableFiltering()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 464
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->isAllAttachMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 466
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->createExceptFilteringWhere()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 468
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->createExceptFilteringWhereArgs()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 470
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, [Ljava/lang/String;

    move-object v7, v0

    .line 484
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    iget v3, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->Depth:I

    iget v4, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mCurrentSortBy:I

    iget v5, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mCurrentInOrder:I

    move-object v2, p1

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/app/myfiles/db/CacheDB;->getCloudDirListByFilter(Ljava/lang/String;IIILjava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 486
    if-nez v8, :cond_2

    .line 487
    new-instance v1, Lcom/samsung/scloud/exception/SCloudException;

    invoke-direct {v1}, Lcom/samsung/scloud/exception/SCloudException;-><init>()V

    throw v1
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 500
    :catch_0
    move-exception v9

    .line 502
    .local v9, "e":Lcom/samsung/scloud/exception/SCloudException;
    :try_start_1
    invoke-virtual {v9}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 504
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 507
    .end local v9    # "e":Lcom/samsung/scloud/exception/SCloudException;
    :goto_1
    return-object v8

    .line 476
    :cond_0
    :try_start_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->createFilteringWhere()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 478
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->createFilteringWhereArgs()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 480
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, [Ljava/lang/String;

    move-object v7, v0

    goto :goto_0

    .line 492
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    iget v2, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->Depth:I

    iget v3, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mCurrentSortBy:I

    iget v4, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mCurrentInOrder:I

    invoke-virtual {v1, p1, v2, v3, v4}, Lcom/sec/android/app/myfiles/db/CacheDB;->getCloudDirList(Ljava/lang/String;III)Landroid/database/Cursor;

    move-result-object v8

    .line 494
    if-nez v8, :cond_2

    .line 495
    new-instance v1, Lcom/samsung/scloud/exception/SCloudException;

    invoke-direct {v1}, Lcom/samsung/scloud/exception/SCloudException;-><init>()V

    throw v1
    :try_end_2
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 504
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    throw v1

    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    goto :goto_1
.end method

.method public getItemsInFolder(Ljava/lang/String;)I
    .locals 2
    .param p1, "dirpath"    # Ljava/lang/String;

    .prologue
    .line 532
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    iget v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->Depth:I

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->getCloudChildFileCount(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public goBack()Z
    .locals 4

    .prologue
    .line 302
    const/4 v1, 0x0

    .line 304
    .local v1, "result":Z
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mHistoricalPaths:Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;->pop()Ljava/lang/String;

    move-result-object v2

    .line 305
    .local v2, "upPath":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 306
    new-instance v0, Lcom/sec/android/app/myfiles/db/CacheDB;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    .line 307
    .local v0, "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 330
    invoke-virtual {v0, v2}, Lcom/sec/android/app/myfiles/db/CacheDB;->getIfDropboxFileExist(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 331
    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->getDepthCount(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->Depth:I

    .line 332
    iput-object v2, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 333
    const/4 v1, 0x1

    .line 336
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 339
    .end local v0    # "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    :cond_1
    return v1
.end method

.method public goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z
    .locals 3
    .param p1, "folderPath"    # Ljava/lang/String;
    .param p2, "isBack"    # Ljava/lang/Boolean;

    .prologue
    .line 227
    const/4 v0, 0x1

    .line 229
    .local v0, "result":Z
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->getDepthCount(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->Depth:I

    .line 231
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mHistoricalPaths:Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mCurrentFolderPath:Ljava/lang/String;

    if-nez v1, :cond_1

    const-string v1, "/"

    :goto_0
    invoke-virtual {v2, v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$HistoricalPathRepository;->push(Ljava/lang/String;)V

    .line 232
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    if-eqz v1, :cond_0

    .line 233
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->setmIsSelector(Z)V

    .line 236
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 237
    return v0

    .line 231
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mCurrentFolderPath:Ljava/lang/String;

    goto :goto_0
.end method

.method public goToRoot(Ljava/lang/Boolean;)Z
    .locals 3
    .param p1, "isBack"    # Ljava/lang/Boolean;

    .prologue
    const/4 v2, 0x1

    .line 244
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->registerCloudLoginReceiver()V

    .line 245
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mResigninRequested:Z

    .line 246
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 247
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->isDropboxAccountIsSignin()Z

    move-result v1

    if-nez v1, :cond_0

    .line 248
    iput-boolean v2, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mResigninRequested:Z

    .line 249
    new-instance v0, Lcom/sec/android/app/myfiles/db/CacheDB;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    .line 250
    .local v0, "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 251
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->clearCloudData()J

    .line 252
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 253
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->requestSignIn()V

    .line 263
    .end local v0    # "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    :goto_0
    return v2

    .line 256
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->isAuthKeySaved()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 258
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->AsyncOpenWithFullSync()V

    goto :goto_0

    .line 261
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->requestGetToken()V

    goto :goto_0
.end method

.method public goUp()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 270
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->Depth:I

    if-ne v1, v0, :cond_1

    .line 272
    :cond_0
    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mbAbort:Z

    .line 274
    const/4 v0, 0x0

    .line 294
    :goto_0
    return v0

    .line 280
    :cond_1
    iget v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->Depth:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 282
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 284
    iput v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->Depth:I

    goto :goto_0

    .line 288
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->getParentPathInDropbox()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 290
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->getDepthCount(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->Depth:I

    goto :goto_0
.end method

.method public isCurrentFolderRoot()Z
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isRootFolder(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 183
    invoke-super {p0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->onPause()V

    .line 184
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->unregisterCloudLoginReceiver()V

    .line 185
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 189
    invoke-super {p0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->onResume()V

    .line 190
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->showDialog:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    if-eqz v2, :cond_1

    .line 191
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->showDialog:Z

    .line 192
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 193
    .local v1, "fManager":Landroid/app/FragmentManager;
    if-nez v1, :cond_0

    .line 194
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 195
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 196
    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 198
    .end local v0    # "activity":Landroid/app/Activity;
    :cond_0
    if-eqz v1, :cond_1

    .line 199
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    const-string v3, "OpenOperation"

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 201
    .end local v1    # "fManager":Landroid/app/FragmentManager;
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->registerCloudLoginReceiver()V

    .line 202
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    iget-boolean v2, v2, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->mSignInRequested:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->isDropboxAccountIsSignin()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 203
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    iput-boolean v4, v2, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->mSignInRequested:Z

    .line 204
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->actionsOnSignIn()V

    .line 206
    :cond_2
    return-void
.end method

.method public refreshNavigation()V
    .locals 0

    .prologue
    .line 213
    return-void
.end method

.method public setCurrentPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 442
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 444
    return-void
.end method

.method public synclist(Z)V
    .locals 13
    .param p1, "bUsingTransaction"    # Z

    .prologue
    .line 686
    const/4 v9, 0x2

    :try_start_0
    const-string v10, "DropboxNavigation"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "SyncList Dropbox syncing bUsingTransaction = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 689
    iget-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mIsSyncing:Z

    if-eqz v9, :cond_0

    .line 690
    const/4 v9, 0x0

    const-string v10, "DropboxNavigation"

    const-string v11, "SyncList  Dropbox syncing cancelling, syncing has already started"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 691
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mbAbort:Z

    .line 851
    :goto_0
    return-void

    .line 695
    :cond_0
    const/4 v9, 0x2

    const-string v10, "DropboxNavigation"

    const-string v11, "SyncList  Dropbox syncing has started"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 699
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mIsSyncing:Z

    .line 700
    const/4 v0, 0x0

    .line 701
    .local v0, "CloudDeltaCursor":Ljava/lang/String;
    new-instance v3, Lcom/sec/android/app/myfiles/db/CacheDB;

    iget-object v9, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mContext:Landroid/content/Context;

    invoke-direct {v3, v9}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    .line 702
    .local v3, "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 703
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->getDropboxCursor()Ljava/lang/String;

    move-result-object v0

    .line 704
    if-eqz v0, :cond_1

    const-string v9, ""

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 705
    :cond_1
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->clearCloudData()J

    .line 706
    :cond_2
    const/4 v7, 0x0

    .line 708
    .local v7, "nodes":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<Lcom/samsung/scloud/data/SCloudNode;>;"
    const/4 v1, 0x0

    .line 710
    .local v1, "SyncCount":I
    const/4 v2, 0x0

    .line 713
    .local v2, "TotalCount":I
    iget-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mbAbort:Z

    if-eqz v9, :cond_5

    .line 715
    const/4 v9, 0x2

    const-string v10, "DropboxNavigation"

    const-string v11, "SyncList Dropbox syncing has End mbAbort is ture"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 716
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mIsSyncing:Z

    .line 718
    iget-object v9, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    if-eqz v9, :cond_3

    .line 719
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->showDialog:Z

    .line 720
    iget-object v9, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->setCompletedCondition(Z)V

    .line 722
    :cond_3
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mbAbort:Z

    .line 723
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 837
    .end local v0    # "CloudDeltaCursor":Ljava/lang/String;
    .end local v1    # "SyncCount":I
    .end local v2    # "TotalCount":I
    .end local v3    # "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    .end local v7    # "nodes":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<Lcom/samsung/scloud/data/SCloudNode;>;"
    :catch_0
    move-exception v4

    .line 840
    .local v4, "e":Ljava/lang/Exception;
    const/4 v9, 0x2

    const-string v10, "DropboxNavigation"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "SyncList Dropbox syncing exception "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 841
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 843
    iget-object v9, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    if-eqz v9, :cond_4

    .line 844
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->showDialog:Z

    .line 845
    iget-object v9, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->setCompletedCondition(Z)V

    .line 847
    :cond_4
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mIsSyncing:Z

    .line 848
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mbAbort:Z

    goto :goto_0

    .line 729
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v0    # "CloudDeltaCursor":Ljava/lang/String;
    .restart local v1    # "SyncCount":I
    .restart local v2    # "TotalCount":I
    .restart local v3    # "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    .restart local v7    # "nodes":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<Lcom/samsung/scloud/data/SCloudNode;>;"
    :cond_5
    move-object v8, v0

    .line 733
    .local v8, "workingDelta":Ljava/lang/String;
    :cond_6
    :try_start_1
    iget-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mbAbort:Z

    if-eqz v9, :cond_9

    .line 735
    const/4 v9, 0x2

    const-string v10, "DropboxNavigation"

    const-string v11, "SyncList Dropbox mbAbort is ture break"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 736
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mbAbort:Z

    .line 827
    :cond_7
    :goto_1
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 828
    iget-object v9, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    if-eqz v9, :cond_8

    .line 829
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->showDialog:Z

    .line 830
    iget-object v9, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mfg:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->setCompletedCondition(Z)V

    .line 834
    :cond_8
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mIsSyncing:Z

    .line 836
    const/4 v9, 0x2

    const-string v10, "DropboxNavigation"

    const-string v11, "SyncList Dropbox syncing has End"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 740
    :cond_9
    iget-object v9, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    invoke-virtual {v9, v8}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->getDeltaList(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;

    move-result-object v7

    .line 742
    if-eqz v7, :cond_7

    .line 745
    invoke-virtual {v7}, Lcom/samsung/scloud/data/SCloudNodeList;->getCursor()Ljava/lang/String;

    move-result-object v8

    .line 746
    const/4 v9, 0x2

    const-string v10, "DropboxNavigation"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "SyncList getList - nodes :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v7}, Lcom/samsung/scloud/data/SCloudNodeList;->getNodes()Ljava/util/List;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 748
    if-eqz p1, :cond_a

    .line 750
    invoke-virtual {v7}, Lcom/samsung/scloud/data/SCloudNodeList;->getNodes()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_a

    .line 751
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->beginTransaction()V

    .line 754
    :cond_a
    if-eqz v7, :cond_b

    .line 755
    invoke-virtual {v7}, Lcom/samsung/scloud/data/SCloudNodeList;->getNodes()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/scloud/data/SCloudNode;

    .line 757
    .local v6, "node":Lcom/samsung/scloud/data/SCloudNode;
    iget-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mbAbort:Z

    if-eqz v9, :cond_d

    .line 759
    const/4 v9, 0x2

    const-string v10, "DropboxNavigation"

    const-string v11, "SyncList Dropbox mbAbort is ture break for loop"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 771
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "node":Lcom/samsung/scloud/data/SCloudNode;
    :cond_b
    const/4 v9, 0x2

    const-string v10, "DropboxNavigation"

    const-string v11, "SyncList db.insert done "

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 773
    if-eqz p1, :cond_c

    .line 775
    invoke-virtual {v7}, Lcom/samsung/scloud/data/SCloudNodeList;->getNodes()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v1

    .line 776
    invoke-virtual {v7}, Lcom/samsung/scloud/data/SCloudNodeList;->getNodes()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    add-int/2addr v2, v9

    .line 780
    if-lez v1, :cond_c

    .line 782
    const/4 v9, 0x2

    const-string v10, "DropboxNavigation"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "SyncList SyncCount Transcation : SyncCount = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 783
    const/4 v9, 0x2

    const-string v10, "DropboxNavigation"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "SyncList SyncCount Transcation : TotalCount = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 785
    const/4 v1, 0x0

    .line 787
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->setTransactionSuccessful()V

    .line 788
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->endTransaction()V

    .line 790
    const/4 v9, 0x2

    const-string v10, "DropboxNavigation"

    const-string v11, "SyncList SyncCount Transcation : endTransaction "

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 812
    :cond_c
    iget-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mbAbort:Z

    if-eqz v9, :cond_f

    .line 813
    const/4 v9, 0x2

    const-string v10, "DropboxNavigation"

    const-string v11, "SyncList Dropbox mbAbort is ture break ,do while loop"

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 814
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/navigation/DropboxNavigation;->mbAbort:Z

    goto/16 :goto_1

    .line 763
    .restart local v5    # "i$":Ljava/util/Iterator;
    .restart local v6    # "node":Lcom/samsung/scloud/data/SCloudNode;
    :cond_d
    invoke-virtual {v6}, Lcom/samsung/scloud/data/SCloudNode;->isDeleted()Z

    move-result v9

    if-eqz v9, :cond_e

    .line 764
    invoke-virtual {v3, v6}, Lcom/sec/android/app/myfiles/db/CacheDB;->deleteCloudData(Lcom/samsung/scloud/data/SCloudNode;)I

    goto/16 :goto_2

    .line 766
    :cond_e
    invoke-virtual {v3, v6, p1}, Lcom/sec/android/app/myfiles/db/CacheDB;->insertCloudData(Lcom/samsung/scloud/data/SCloudNode;Z)J

    goto/16 :goto_2

    .line 819
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "node":Lcom/samsung/scloud/data/SCloudNode;
    :cond_f
    move-object v0, v8

    .line 820
    invoke-virtual {v3, v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->setDropboxCursor(Ljava/lang/String;)V

    .line 824
    invoke-virtual {v7}, Lcom/samsung/scloud/data/SCloudNodeList;->getHasMore()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v9

    const/4 v10, 0x1

    if-eq v9, v10, :cond_6

    goto/16 :goto_1
.end method
