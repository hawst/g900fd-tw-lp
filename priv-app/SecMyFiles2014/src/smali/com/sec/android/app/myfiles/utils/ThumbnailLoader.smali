.class public Lcom/sec/android/app/myfiles/utils/ThumbnailLoader;
.super Ljava/lang/Object;
.source "ThumbnailLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;,
        Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailLoaderThread;
    }
.end annotation


# static fields
.field private static final DECODE_COMPLETE:I = 0x0

.field private static final MODULE:Ljava/lang/String; = "ThumbnailLoader"

.field public static SCROLL_DONE:Z = false

.field private static final THREAD_NAME:Ljava/lang/String; = "ThumbnailLoader"

.field public static final sThumbnailLoader:Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailLoaderThread;

.field private static final sThumbnailUpdateHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 43
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader;->SCROLL_DONE:Z

    .line 45
    new-instance v0, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailLoaderThread;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailLoaderThread;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader;->sThumbnailLoader:Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailLoaderThread;

    .line 49
    const/4 v0, 0x0

    const-string v1, "ThumbnailLoader"

    const-string v2, "ThumbnailLoaderThread start"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 51
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader;->sThumbnailLoader:Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailLoaderThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 148
    new-instance v0, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$1;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader;->sThumbnailUpdateHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 229
    return-void
.end method

.method static synthetic access$000()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader;->sThumbnailUpdateHandler:Landroid/os/Handler;

    return-object v0
.end method
