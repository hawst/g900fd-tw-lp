.class Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$17;
.super Landroid/content/BroadcastReceiver;
.source "AbsListableFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;)V
    .locals 0

    .prologue
    .line 1111
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$17;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1116
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.app.myfiles.SORTBY_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1118
    const/4 v0, 0x0

    const-string v1, "AbsListableFragment"

    const-string v2, "sort by is changed"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1120
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$17;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    const-string v1, "sort_by"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$17;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget v2, v2, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCurrentSortBy:I

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCurrentSortBy:I

    .line 1122
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$17;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    const-string v1, "in_order"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$17;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget v2, v2, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCurrentInOrder:I

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCurrentInOrder:I

    .line 1124
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$17;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$17;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget v1, v1, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCurrentSortBy:I

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$17;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget v2, v2, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mCurrentInOrder:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->setSortBy(II)V

    .line 1126
    :cond_0
    return-void
.end method
