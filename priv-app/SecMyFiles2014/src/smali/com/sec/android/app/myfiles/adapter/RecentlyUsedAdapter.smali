.class public Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;
.super Lcom/sec/android/app/myfiles/adapter/AbsCategoryBrowserAdapter;
.source "RecentlyUsedAdapter.java"


# static fields
.field private static final HOVER_DETECT_MS_FOLDER:I = 0x12c

.field private static final INVALID_ENTER_TIME:I = -0x1

.field private static final MODULE:Ljava/lang/String; = "RecentlyUsedAdapter"


# instance fields
.field private mAudioHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

.field private mDocumentHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

.field private mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

.field private mLastHoverEnterEventTime:J

.field private mLastHoverEnteredView:Landroid/view/View;

.field private mLastMotionEvent:Landroid/view/MotionEvent;

.field private mVideoHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "c"    # Landroid/database/Cursor;
    .param p4, "flags"    # I
    .param p5, "navigation"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .prologue
    const/4 v0, 0x0

    .line 69
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/myfiles/adapter/AbsCategoryBrowserAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 52
    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 54
    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mDocumentHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 56
    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mVideoHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 58
    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mAudioHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 60
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mLastHoverEnterEventTime:J

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mContext:Landroid/content/Context;

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-static {v0, v1}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    new-instance v0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 75
    new-instance v0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mVideoHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 77
    new-instance v0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mAudioHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 79
    new-instance v0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mDocumentHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 81
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mVideoHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mAudioHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mDocumentHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mLastHoverEnterEventTime:J

    return-wide v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;
    .param p1, "x1"    # J

    .prologue
    .line 43
    iput-wide p1, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mLastHoverEnterEventTime:J

    return-wide p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;)Landroid/view/MotionEvent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mLastMotionEvent:Landroid/view/MotionEvent;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;
    .param p1, "x1"    # Landroid/view/MotionEvent;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mLastMotionEvent:Landroid/view/MotionEvent;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mLastHoverEnteredView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mLastHoverEnteredView:Landroid/view/View;

    return-object p1
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 11
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 86
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/adapter/AbsCategoryBrowserAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 88
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;

    .line 90
    .local v8, "vh":Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;
    const-string v9, "_data"

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 92
    .local v2, "filePath":Ljava/lang/String;
    const/16 v9, 0x2f

    invoke-virtual {v2, v9}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    invoke-virtual {v2, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 95
    .local v1, "fileName":Ljava/lang/String;
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    if-eqz v9, :cond_0

    .line 98
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 103
    :cond_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 106
    iget-object v9, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowFileExtensionStatus()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 108
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v9, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    :cond_1
    :goto_0
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v9, :cond_2

    .line 128
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 131
    :cond_2
    invoke-virtual {p0, v8, v2}, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->loadThumbnail(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;Ljava/lang/String;)V

    .line 207
    invoke-static {v2}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v4

    .line 208
    .local v4, "fileType":I
    move-object v5, v2

    .line 209
    .local v5, "hoverFilePath":Ljava/lang/String;
    const/4 v0, 0x0

    .line 210
    .local v0, "bFolderFormat":Z
    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    .line 212
    .local v3, "filePosition":I
    new-instance v6, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;

    invoke-direct {v6, p0, v4, v5, v3}, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;-><init>(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;ILjava/lang/String;I)V

    .line 359
    .local v6, "hoverListener":Landroid/view/View$OnHoverListener;
    iget-object v9, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mContext:Landroid/content/Context;

    const-string v10, "com.sec.feature.hovering_ui"

    invoke-static {v9, v10}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 361
    iget v9, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mSelectModeFrom:I

    const/4 v10, 0x2

    if-eq v9, v10, :cond_4

    iget v9, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mSelectModeFrom:I

    const/4 v10, 0x1

    if-eq v9, v10, :cond_4

    .line 364
    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isInternalRootFolder(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_8

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isExternalRootFolder(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_8

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxRootFolder(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_8

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isUsbStorageRootFolder(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_8

    .line 368
    iget v9, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mViewMode:I

    if-eqz v9, :cond_3

    iget v9, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mViewMode:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_7

    .line 370
    :cond_3
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    if-eqz v9, :cond_4

    .line 372
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v9, v6}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 396
    :cond_4
    :goto_1
    return-void

    .line 112
    .end local v0    # "bFolderFormat":Z
    .end local v3    # "filePosition":I
    .end local v4    # "fileType":I
    .end local v5    # "hoverFilePath":Ljava/lang/String;
    .end local v6    # "hoverListener":Landroid/view/View$OnHoverListener;
    :cond_5
    const/16 v9, 0x2e

    invoke-virtual {v1, v9}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v7

    .line 115
    .local v7, "lastDot":I
    if-lez v7, :cond_6

    .line 117
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    const/4 v10, 0x0

    invoke-virtual {v1, v10, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 121
    :cond_6
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v9, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 377
    .end local v7    # "lastDot":I
    .restart local v0    # "bFolderFormat":Z
    .restart local v3    # "filePosition":I
    .restart local v4    # "fileType":I
    .restart local v5    # "hoverFilePath":Ljava/lang/String;
    .restart local v6    # "hoverListener":Landroid/view/View$OnHoverListener;
    :cond_7
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v9, :cond_4

    .line 379
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v9, v6}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    goto :goto_1

    .line 385
    :cond_8
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    if-eqz v9, :cond_4

    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v9, :cond_4

    .line 387
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 388
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    goto :goto_1
.end method

.method protected getContentType()I
    .locals 1

    .prologue
    .line 585
    const/4 v0, 0x7

    return v0
.end method
