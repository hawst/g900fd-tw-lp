.class Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$2;
.super Ljava/lang/Object;
.source "AbsFileOperationTask.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;)V
    .locals 0

    .prologue
    .line 822
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$2;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 827
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$2;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iput-boolean v4, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRunRename:Z

    .line 828
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$2;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iput-boolean v3, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mOverwrite:Z

    .line 829
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$2;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iput-boolean v3, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRenameCancelled:Z

    .line 831
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$2;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget v2, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mTempOverwriteFileCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mTempOverwriteFileCount:I

    .line 832
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$2;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mTempOverwriteFileCount:I

    if-gt v1, v4, :cond_0

    .line 833
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$2;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iput v3, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mTempOverwriteFileCount:I

    .line 836
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$2;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget-object v2, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRunnable:Ljava/lang/Runnable;

    monitor-enter v2

    .line 840
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$2;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    const/4 v3, 0x0

    # setter for: Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mWaitDialog:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->access$002(Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;Z)Z

    .line 842
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3$2;->this$1:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$3;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 848
    :goto_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 850
    if-eqz p1, :cond_1

    .line 852
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 854
    :cond_1
    return-void

    .line 844
    :catch_0
    move-exception v0

    .line 846
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 848
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method
