.class Lcom/sec/android/app/myfiles/view/PercentageBar$SineEase;
.super Ljava/lang/Object;
.source "PercentageBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/view/PercentageBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SineEase"
.end annotation


# instance fields
.field private final mBeginTimeGap:I

.field private final mEntriesCount:I

.field private final mOneStripDuration:I

.field private final mTotalDuration:I


# direct methods
.method public constructor <init>(III)V
    .locals 2
    .param p1, "entriesCount"    # I
    .param p2, "oneStripDuration"    # I
    .param p3, "beginTimeGap"    # I

    .prologue
    .line 331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 333
    iput p1, p0, Lcom/sec/android/app/myfiles/view/PercentageBar$SineEase;->mEntriesCount:I

    .line 335
    iput p2, p0, Lcom/sec/android/app/myfiles/view/PercentageBar$SineEase;->mOneStripDuration:I

    .line 337
    iput p3, p0, Lcom/sec/android/app/myfiles/view/PercentageBar$SineEase;->mBeginTimeGap:I

    .line 339
    iget v0, p0, Lcom/sec/android/app/myfiles/view/PercentageBar$SineEase;->mEntriesCount:I

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/sec/android/app/myfiles/view/PercentageBar$SineEase;->mBeginTimeGap:I

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/sec/android/app/myfiles/view/PercentageBar$SineEase;->mOneStripDuration:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/myfiles/view/PercentageBar$SineEase;->mTotalDuration:I

    .line 340
    return-void
.end method

.method private getCurrentWidth(III)I
    .locals 8
    .param p1, "elapsedTime"    # I
    .param p2, "totalTime"    # I
    .param p3, "totalWidth"    # I

    .prologue
    .line 349
    int-to-double v2, p3

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v4

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    int-to-double v6, p1

    mul-double/2addr v4, v6

    int-to-double v6, p2

    div-double/2addr v4, v6

    const-wide v6, 0x3ff921fb54442d18L    # 1.5707963267948966

    sub-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    add-double/2addr v4, v6

    mul-double v0, v2, v4

    .line 351
    .local v0, "width":D
    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    return v2
.end method


# virtual methods
.method public getStripCurrentWidth(III)I
    .locals 4
    .param p1, "stripIndex"    # I
    .param p2, "totalElapsedTime"    # I
    .param p3, "stripFullWidth"    # I

    .prologue
    const/4 v1, 0x0

    .line 356
    iget v2, p0, Lcom/sec/android/app/myfiles/view/PercentageBar$SineEase;->mEntriesCount:I

    if-gt v2, p1, :cond_1

    move p3, v1

    .line 373
    .end local p3    # "stripFullWidth":I
    :cond_0
    :goto_0
    return p3

    .line 361
    .restart local p3    # "stripFullWidth":I
    :cond_1
    iget v2, p0, Lcom/sec/android/app/myfiles/view/PercentageBar$SineEase;->mEntriesCount:I

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v2, p1

    iget v3, p0, Lcom/sec/android/app/myfiles/view/PercentageBar$SineEase;->mBeginTimeGap:I

    mul-int/2addr v2, v3

    sub-int v0, p2, v2

    .line 363
    .local v0, "stripElapsedTime":I
    if-gtz v0, :cond_2

    move p3, v1

    .line 365
    goto :goto_0

    .line 368
    :cond_2
    iget v1, p0, Lcom/sec/android/app/myfiles/view/PercentageBar$SineEase;->mOneStripDuration:I

    if-gt v0, v1, :cond_0

    .line 373
    iget v1, p0, Lcom/sec/android/app/myfiles/view/PercentageBar$SineEase;->mOneStripDuration:I

    invoke-direct {p0, v0, v1, p3}, Lcom/sec/android/app/myfiles/view/PercentageBar$SineEase;->getCurrentWidth(III)I

    move-result p3

    goto :goto_0
.end method

.method public getTotalDuration()I
    .locals 1

    .prologue
    .line 344
    iget v0, p0, Lcom/sec/android/app/myfiles/view/PercentageBar$SineEase;->mTotalDuration:I

    return v0
.end method
