.class public Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "ShortcutPagerAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Lcom/sec/android/app/myfiles/adapter/ISelectModeAdapter;
.implements Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesShortcutAdapter;


# instance fields
.field private categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

.field dialog:Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;

.field private isSelectMode:Z

.field private mArrayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/ShortcutItem;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mCurrentWorkingIndex:I

.field private mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

.field private mIsShortCutRunning:Z

.field private mItemCount:I

.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field private mLayoutResId:I

.field private mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

.field private mScreenState:I

.field private mShortCutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

.field private volatile mShortcutClickCounter:I

.field myCurrentPageIndex:I


# direct methods
.method public constructor <init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V
    .locals 3
    .param p1, "context"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 110
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 80
    iput-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->dialog:Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;

    .line 90
    iput v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mShortcutClickCounter:I

    .line 92
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mCurrentWorkingIndex:I

    .line 94
    iput-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mShortCutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 96
    iput v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->myCurrentPageIndex:I

    .line 112
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .line 114
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    .line 116
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 117
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;Ljava/util/ArrayList;)V
    .locals 3
    .param p1, "context"    # Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/ShortcutItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/ShortcutItem;>;"
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 98
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 80
    iput-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->dialog:Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;

    .line 90
    iput v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mShortcutClickCounter:I

    .line 92
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mCurrentWorkingIndex:I

    .line 94
    iput-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mShortCutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 96
    iput v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->myCurrentPageIndex:I

    .line 100
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    .line 102
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    .line 104
    iput-object p2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    .line 106
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 107
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;)Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    return-object v0
.end method

.method private getCells(Landroid/view/View;)Ljava/util/ArrayList;
    .locals 10
    .param p1, "layout"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    const v9, 0x7f0f008d

    const v8, 0x7f0f008c

    const v7, 0x7f0f008b

    const v6, 0x7f0f008a

    const/4 v5, 0x1

    .line 492
    new-instance v0, Ljava/util/ArrayList;

    iget v4, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mItemCount:I

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 493
    .local v0, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 494
    .local v1, "v":Landroid/view/View;
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v6, v4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 495
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 497
    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 498
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v7, v4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 499
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 501
    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 502
    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v8, v4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 503
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 505
    invoke-virtual {p1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 506
    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v9, v4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 507
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 509
    iget v4, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mScreenState:I

    if-ne v4, v5, :cond_0

    .line 511
    const v4, 0x7f0f0090

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 512
    .local v2, "v5":Landroid/view/View;
    const v4, 0x7f0f0091

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 513
    .local v3, "v6":Landroid/view/View;
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 515
    const v4, 0x7f0f0090

    const/4 v5, 0x4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 516
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 517
    const v4, 0x7f0f0091

    const/4 v5, 0x5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 518
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 523
    .end local v2    # "v5":Landroid/view/View;
    .end local v3    # "v6":Landroid/view/View;
    :cond_0
    return-object v0
.end method

.method private showDeleteDailog(Lcom/sec/android/app/myfiles/element/ShortcutItem;)V
    .locals 7
    .param p1, "item"    # Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .prologue
    const/4 v6, 0x1

    .line 942
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 943
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0b0020

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 944
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0004

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v6, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 945
    const v1, 0x7f0b0015

    new-instance v2, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter$3;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter$3;-><init>(Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;Lcom/sec/android/app/myfiles/element/ShortcutItem;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 967
    const v1, 0x7f0b0017

    new-instance v2, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter$4;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter$4;-><init>(Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;Lcom/sec/android/app/myfiles/element/ShortcutItem;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 978
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 979
    return-void
.end method


# virtual methods
.method public GetShortcutDialogState()Z
    .locals 3

    .prologue
    .line 148
    const-string v0, "MyFiles"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GetShortcutDialogState - mIsShortCutRunning : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mIsShortCutRunning:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mIsShortCutRunning:Z

    return v0
.end method

.method public SetShortcutDialogState(Z)V
    .locals 3
    .param p1, "state"    # Z

    .prologue
    .line 153
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mIsShortCutRunning:Z

    .line 154
    if-nez p1, :cond_0

    .line 155
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mShortcutClickCounter:I

    .line 157
    :cond_0
    const-string v0, "MyFiles"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SetShortcutDialogState - mIsShortCutRunning : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mIsShortCutRunning:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    return-void
.end method

.method public addItem(ILcom/sec/android/app/myfiles/element/ShortcutItem;)V
    .locals 2
    .param p1, "position"    # I
    .param p2, "item"    # Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .prologue
    .line 1129
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    monitor-enter v1

    .line 1131
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1132
    monitor-exit v1

    .line 1133
    return-void

    .line 1132
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addItem(Lcom/sec/android/app/myfiles/element/ShortcutItem;)V
    .locals 2
    .param p1, "item"    # Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .prologue
    .line 1120
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    monitor-enter v1

    .line 1122
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1123
    monitor-exit v1

    .line 1124
    return-void

    .line 1123
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public clear()V
    .locals 6

    .prologue
    .line 1184
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    monitor-enter v4

    .line 1186
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1188
    .local v2, "removeItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/ShortcutItem;>;"
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 1190
    .local v1, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v3

    const/16 v5, 0x1a

    if-eq v3, v5, :cond_1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v3

    const/16 v5, 0x1c

    if-ne v3, v5, :cond_0

    .line 1192
    :cond_1
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1197
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    .end local v2    # "removeItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/ShortcutItem;>;"
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 1196
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v2    # "removeItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/ShortcutItem;>;"
    :cond_2
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 1197
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1198
    return-void
.end method

.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 196
    check-cast p3, Landroid/view/View;

    .end local p3    # "object":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 197
    return-void
.end method

.method public finishSelectMode()V
    .locals 1

    .prologue
    .line 993
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->isSelectMode:Z

    .line 995
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->unselectAllItem()V

    .line 996
    return-void
.end method

.method public getArrayList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/ShortcutItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mItemCount:I

    div-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getCurrentWorkingIndex()I
    .locals 1

    .prologue
    .line 820
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mCurrentWorkingIndex:I

    return v0
.end method

.method public getItemByType(I)Ljava/lang/Object;
    .locals 6
    .param p1, "type"    # I

    .prologue
    .line 1138
    const/4 v3, 0x0

    .line 1142
    .local v3, "obj":Ljava/lang/Object;
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    monitor-enter v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1144
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 1146
    .local v2, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v4

    if-ne v4, p1, :cond_0

    .line 1148
    move-object v3, v2

    .line 1153
    .end local v2    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    .end local v3    # "obj":Ljava/lang/Object;
    :cond_1
    monitor-exit v5

    .line 1160
    .end local v1    # "i$":Ljava/util/Iterator;
    :goto_0
    return-object v3

    .line 1153
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v4
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 1155
    :catch_0
    move-exception v0

    .line 1157
    .local v0, "e":Ljava/lang/Exception;
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 164
    const/4 v0, -0x2

    return v0
.end method

.method public getSelectedItem()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1081
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1083
    .local v2, "selectedItem":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    monitor-enter v4

    .line 1085
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 1087
    .local v1, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1089
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1092
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1094
    return-object v2
.end method

.method public getSelectedItemCount()I
    .locals 5

    .prologue
    .line 1101
    const/4 v2, 0x0

    .line 1103
    .local v2, "result":I
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    monitor-enter v4

    .line 1105
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 1107
    .local v1, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1109
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1112
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :cond_1
    monitor-exit v4

    .line 1114
    return v2

    .line 1112
    .end local v0    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public getSelectedItemPosition()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1074
    const/4 v0, 0x0

    return-object v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 24
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 300
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mLayoutResId:I

    move/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    move-object/from16 v2, p1

    move/from16 v3, v23

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v20

    .line 302
    .local v20, "view":Landroid/view/View;
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->getCells(Landroid/view/View;)Ljava/util/ArrayList;

    move-result-object v17

    .line 304
    .local v17, "shortcutList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/view/View;

    .line 306
    .local v14, "shortcut":Landroid/view/View;
    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 308
    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 311
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v14, v0}, Landroid/view/View;->setLongClickable(Z)V

    .line 312
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->isSelectMode:Z

    move/from16 v21, v0

    if-eqz v21, :cond_0

    .line 314
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v14, v0}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    .line 318
    :cond_0
    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v14, v0}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    .line 322
    .end local v14    # "shortcut":Landroid/view/View;
    :cond_1
    const/4 v13, 0x0

    .line 324
    .local v13, "select":Landroid/widget/CheckBox;
    const/4 v8, 0x0

    .line 326
    .local v8, "icon":Landroid/widget/ImageView;
    const/4 v4, 0x0

    .line 328
    .local v4, "convertView":Landroid/view/View;
    const v21, 0x7f0f008e

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    .line 330
    .local v11, "leftPageArrow":Landroid/widget/ImageView;
    new-instance v21, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter$1;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter$1;-><init>(Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;)V

    move-object/from16 v0, v21

    invoke-virtual {v11, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 338
    const v21, 0x7f0f008f

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    .line 340
    .local v12, "rightPageArrow":Landroid/widget/ImageView;
    new-instance v21, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter$2;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter$2;-><init>(Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;)V

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 348
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->getCount()I

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_3

    .line 350
    const/16 v21, 0x8

    move/from16 v0, v21

    invoke-virtual {v11, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 352
    const/16 v21, 0x8

    move/from16 v0, v21

    invoke-virtual {v12, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 373
    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mItemCount:I

    move/from16 v21, v0

    mul-int v5, p2, v21

    .line 375
    .local v5, "from":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mItemCount:I

    move/from16 v21, v0

    add-int v19, v5, v21

    .line 377
    .local v19, "to":I
    move v6, v5

    .local v6, "i":I
    :goto_2
    move/from16 v0, v19

    if-ge v6, v0, :cond_10

    .line 379
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    if-eqz v21, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v21

    move/from16 v0, v21

    if-ge v6, v0, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    if-eqz v21, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v21

    const/16 v22, 0x1b

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_2

    .line 381
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 386
    .local v10, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getIndex()I

    move-result v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mItemCount:I

    move/from16 v22, v0

    rem-int v9, v21, v22

    .line 387
    .local v9, "index":I
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v21

    move/from16 v0, v21

    if-lt v9, v0, :cond_6

    .line 377
    .end local v9    # "index":I
    .end local v10    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :cond_2
    :goto_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 354
    .end local v5    # "from":I
    .end local v6    # "i":I
    .end local v19    # "to":I
    :cond_3
    if-nez p2, :cond_4

    .line 356
    const/16 v21, 0x8

    move/from16 v0, v21

    invoke-virtual {v11, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 358
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v12, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    .line 360
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->getCount()I

    move-result v21

    add-int/lit8 v21, v21, -0x1

    move/from16 v0, p2

    move/from16 v1, v21

    if-ne v0, v1, :cond_5

    .line 362
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v11, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 364
    const/16 v21, 0x8

    move/from16 v0, v21

    invoke-virtual {v12, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    .line 368
    :cond_5
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v11, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 370
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v12, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    .line 390
    .restart local v5    # "from":I
    .restart local v6    # "i":I
    .restart local v9    # "index":I
    .restart local v10    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    .restart local v19    # "to":I
    :cond_6
    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "convertView":Landroid/view/View;
    check-cast v4, Landroid/view/View;

    .line 392
    .restart local v4    # "convertView":Landroid/view/View;
    const v21, 0x7f0f0092

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/view/ViewGroup;

    .line 396
    .local v16, "shortcutItemContainer":Landroid/view/ViewGroup;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 398
    const v21, 0x7f0f0095

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/view/ViewGroup;

    .line 400
    .local v15, "shortcutItemAddContainer":Landroid/view/ViewGroup;
    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v21

    const/16 v22, 0x1a

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_7

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v21

    const/16 v22, 0x9

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_7

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v21

    const/16 v22, 0x1c

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_7

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v21

    const/16 v22, 0x8

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_7

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v21

    const/16 v22, 0x1f

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_f

    .line 405
    :cond_7
    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Landroid/view/View;->setLongClickable(Z)V

    .line 407
    const/16 v21, 0x1

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 409
    const/16 v21, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 411
    const/16 v21, 0x8

    move/from16 v0, v21

    invoke-virtual {v15, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 413
    const v21, 0x7f0f0093

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .end local v8    # "icon":Landroid/widget/ImageView;
    check-cast v8, Landroid/widget/ImageView;

    .line 415
    .restart local v8    # "icon":Landroid/widget/ImageView;
    const v21, 0x7f0f0037

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    .end local v13    # "select":Landroid/widget/CheckBox;
    check-cast v13, Landroid/widget/CheckBox;

    .line 417
    .restart local v13    # "select":Landroid/widget/CheckBox;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->isSelectMode:Z

    move/from16 v21, v0

    if-eqz v21, :cond_9

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->isSelectable()Z

    move-result v21

    if-eqz v21, :cond_9

    .line 419
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 420
    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->isSelected()Z

    move-result v21

    move/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 427
    :goto_4
    const v21, 0x7f0f0094

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    .line 428
    .local v18, "title":Landroid/widget/TextView;
    const/16 v21, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 430
    sget-object v21, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 432
    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getTitle()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 433
    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v21

    if-eqz v21, :cond_a

    .line 435
    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v21

    const/16 v22, 0x9

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_8

    .line 437
    const v21, 0x7f0f0061

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    const/16 v22, 0x8

    invoke-virtual/range {v21 .. v22}, Landroid/view/View;->setVisibility(I)V

    .line 440
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 443
    :cond_8
    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    .line 424
    .end local v18    # "title":Landroid/widget/TextView;
    :cond_9
    const/16 v21, 0x8

    move/from16 v0, v21

    invoke-virtual {v13, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_4

    .line 445
    .restart local v18    # "title":Landroid/widget/TextView;
    :cond_a
    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v21

    const/16 v22, 0x1a

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_b

    .line 447
    const v21, 0x7f020032

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_3

    .line 449
    :cond_b
    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v21

    const/16 v22, 0x9

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_c

    .line 451
    const/16 v21, 0x8

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 453
    const v21, 0x7f0f0061

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3

    .line 456
    :cond_c
    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v21

    const/16 v22, 0x1c

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_d

    .line 458
    const v21, 0x7f020051

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_3

    .line 460
    :cond_d
    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v21

    const/16 v22, 0x8

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_e

    .line 462
    const v21, 0x7f020050

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_3

    .line 463
    :cond_e
    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v21

    const/16 v22, 0x1f

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_2

    .line 465
    const v21, 0x7f02004f

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_3

    .line 470
    .end local v18    # "title":Landroid/widget/TextView;
    :cond_f
    const/16 v21, 0x8

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 472
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v15, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 474
    const v21, 0x7f0f0096

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .end local v8    # "icon":Landroid/widget/ImageView;
    check-cast v8, Landroid/widget/ImageView;

    .restart local v8    # "icon":Landroid/widget/ImageView;
    goto/16 :goto_3

    .line 482
    .end local v9    # "index":I
    .end local v10    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    .end local v15    # "shortcutItemAddContainer":Landroid/view/ViewGroup;
    .end local v16    # "shortcutItemContainer":Landroid/view/ViewGroup;
    :cond_10
    const v21, 0x7f0200e3

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->setBackgroundResource(I)V

    .line 484
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 486
    return-object v20
.end method

.method public isDragMode()Z
    .locals 1

    .prologue
    .line 202
    const/4 v0, 0x0

    return v0
.end method

.method public isSelectMode()Z
    .locals 1

    .prologue
    .line 1002
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->isSelectMode:Z

    return v0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Ljava/lang/Object;

    .prologue
    .line 189
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadShortcuts()Ljava/util/ArrayList;
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/ShortcutItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 208
    new-instance v20, Ljava/util/HashMap;

    invoke-direct/range {v20 .. v20}, Ljava/util/HashMap;-><init>()V

    .line 209
    .local v20, "hashMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Lcom/sec/android/app/myfiles/element/ShortcutItem;>;"
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 211
    .local v23, "shortcutList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/ShortcutItem;>;"
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getLastUsedShortcutIndex(Landroid/content/Context;)I

    move-result v21

    .line 212
    .local v21, "maxItems":I
    const/16 v17, 0x0

    .line 214
    .local v17, "cursor":Landroid/database/Cursor;
    const/4 v4, 0x0

    .line 215
    .local v4, "where":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isDropBoxEnabled(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 216
    const-string v4, "type!=28"

    .line 218
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Shortcut;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v5, 0x0

    const-string v6, "shortcut_index ASC"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 220
    if-eqz v17, :cond_3

    .line 221
    :goto_0
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 223
    const-string v1, "shortcut_index"

    move-object/from16 v0, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 224
    .local v10, "index":I
    const-string v1, "type"

    move-object/from16 v0, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 226
    .local v6, "shortcutType":I
    const/4 v5, 0x0

    .line 228
    .local v5, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    packed-switch v6, :pswitch_data_0

    .line 244
    new-instance v5, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .end local v5    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    const-string v1, "title"

    move-object/from16 v0, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v1, "_data"

    move-object/from16 v0, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    invoke-direct/range {v5 .. v10}, Lcom/sec/android/app/myfiles/element/ShortcutItem;-><init>(ILjava/lang/String;Ljava/lang/String;ZI)V

    .line 256
    .restart local v5    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :goto_1
    const-string v1, "shortcut_drawable"

    move-object/from16 v0, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->convertByteArrayToDrawable([B)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 257
    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v1

    const/16 v2, 0x1a

    if-ne v1, v2, :cond_1

    .line 258
    new-instance v19, Ljava/io/File;

    const-string v1, "_data"

    move-object/from16 v0, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v19

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 259
    .local v19, "file":Ljava/io/File;
    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 260
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteShortcut(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v22

    .line 261
    .local v22, "rowsDeleted":I
    if-lez v22, :cond_1

    .line 263
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->removeItem(Ljava/lang/Object;)Z

    .line 264
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mShortcutClickCounter:I

    .line 265
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->notifyDataSetChanged()V

    .line 269
    .end local v19    # "file":Ljava/io/File;
    .end local v22    # "rowsDeleted":I
    :cond_1
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, v20

    invoke-virtual {v0, v1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 284
    .end local v5    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    .end local v6    # "shortcutType":I
    .end local v10    # "index":I
    :catch_0
    move-exception v18

    .line 285
    .local v18, "e":Ljava/lang/Exception;
    if-eqz v17, :cond_2

    .line 286
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 289
    :cond_2
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Exception;->printStackTrace()V

    .line 291
    .end local v18    # "e":Ljava/lang/Exception;
    :cond_3
    :goto_2
    return-object v23

    .line 232
    .restart local v5    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    .restart local v6    # "shortcutType":I
    .restart local v10    # "index":I
    :pswitch_0
    :try_start_1
    new-instance v5, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .end local v5    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    const-string v1, "title"

    move-object/from16 v0, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Security;->getInstance()Lcom/sec/android/app/myfiles/utils/Security;

    move-result-object v1

    const-string v2, "_data"

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/utils/Security;->decrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    invoke-direct/range {v5 .. v10}, Lcom/sec/android/app/myfiles/element/ShortcutItem;-><init>(ILjava/lang/String;Ljava/lang/String;ZI)V

    .line 240
    .restart local v5    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    goto/16 :goto_1

    .line 271
    .end local v5    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    .end local v6    # "shortcutType":I
    .end local v10    # "index":I
    :cond_4
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_3
    add-int/lit8 v1, v21, 0x1

    move/from16 v0, v16

    if-ge v0, v1, :cond_6

    .line 272
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 274
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v0, v23

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 271
    :goto_4
    add-int/lit8 v16, v16, 0x1

    goto :goto_3

    .line 277
    :cond_5
    new-instance v11, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    const/16 v12, 0x1b

    const-string v13, ""

    const-string v14, ""

    const/4 v15, 0x0

    invoke-direct/range {v11 .. v16}, Lcom/sec/android/app/myfiles/element/ShortcutItem;-><init>(ILjava/lang/String;Ljava/lang/String;ZI)V

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 281
    :cond_6
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 228
    nop

    :pswitch_data_0
    .packed-switch 0x1c
        :pswitch_0
    .end packed-switch
.end method

.method public notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 1178
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->loadShortcuts()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    .line 1179
    invoke-super {p0}, Landroid/support/v4/view/PagerAdapter;->notifyDataSetChanged()V

    .line 1180
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 17
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 530
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    if-eqz v14, :cond_0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getTag()Ljava/lang/String;

    move-result-object v14

    if-nez v14, :cond_2

    .line 531
    :cond_0
    const-string v14, "MyFiles"

    const-string v15, " categoryHomeFragment or categoryHomeFragment.getTag() is null "

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 799
    :cond_1
    :goto_0
    return-void

    .line 535
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v14

    if-nez v14, :cond_3

    .line 536
    const-string v14, "MyFiles"

    const-string v15, " categoryHomeFragment.getActivity() is null "

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 540
    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-boolean v14, v14, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mResumeSate:Z

    if-nez v14, :cond_4

    .line 541
    const-string v14, "MyFiles"

    const-string v15, " categoryHomeFragment.mResumeSate is false "

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 545
    :cond_4
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v14

    if-eqz v14, :cond_5

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v14

    instance-of v14, v14, Ljava/lang/Integer;

    if-eqz v14, :cond_5

    .line 548
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Integer;

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->myCurrentPageIndex:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mItemCount:I

    move/from16 v16, v0

    mul-int v15, v15, v16

    add-int/2addr v14, v15

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mCurrentWorkingIndex:I

    .line 553
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->isSelectMode:Z

    if-eqz v14, :cond_7

    .line 555
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v14

    if-eqz v14, :cond_1

    .line 557
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 559
    .local v7, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    const v14, 0x7f0f0037

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/CheckBox;

    .line 561
    .local v13, "select":Landroid/widget/CheckBox;
    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->isSelected()Z

    move-result v14

    if-eqz v14, :cond_6

    .line 563
    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 564
    const/4 v14, 0x0

    invoke-virtual {v7, v14}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->setSelected(Z)V

    .line 570
    :goto_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setAllItemCount()V

    .line 571
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v14

    invoke-virtual {v14}, Landroid/app/Activity;->invalidateOptionsMenu()V

    goto/16 :goto_0

    .line 551
    .end local v7    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    .end local v13    # "select":Landroid/widget/CheckBox;
    :cond_5
    const/4 v14, -0x1

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mCurrentWorkingIndex:I

    goto :goto_1

    .line 566
    .restart local v7    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    .restart local v13    # "select":Landroid/widget/CheckBox;
    :cond_6
    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 567
    const/4 v14, 0x1

    invoke-virtual {v7, v14}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->setSelected(Z)V

    goto :goto_2

    .line 575
    .end local v7    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    .end local v13    # "select":Landroid/widget/CheckBox;
    :cond_7
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mShortcutClickCounter:I

    if-nez v14, :cond_1

    .line 576
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mShortcutClickCounter:I

    .line 577
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v14

    if-eqz v14, :cond_18

    .line 579
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 581
    .restart local v7    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v3

    .line 583
    .local v3, "categoryType":I
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 585
    .local v2, "argument":Landroid/os/Bundle;
    sparse-switch v3, :sswitch_data_0

    .line 727
    :cond_8
    :goto_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    instance-of v14, v14, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v14, :cond_1

    .line 728
    const-string v14, "IS_SHORTCUT_FOLDER"

    const/4 v15, 0x1

    invoke-virtual {v2, v14, v15}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 729
    const-string v14, "shortcut_working_index_intent_key"

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mCurrentWorkingIndex:I

    invoke-virtual {v2, v14, v15}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 730
    const-string v14, "SHORTCUT_ROOT_FOLDER"

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v2, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 732
    packed-switch v3, :pswitch_data_0

    .line 748
    :cond_9
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    check-cast v14, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v14, v3, v2}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto/16 :goto_0

    .line 589
    :sswitch_0
    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getCategoryType()I

    move-result v3

    .line 591
    new-instance v5, Ljava/io/File;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v5, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 593
    .local v5, "file":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v14

    if-eqz v14, :cond_c

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxRootFolder(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_a

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretMode(Landroid/content/Context;)Z

    move-result v14

    if-eqz v14, :cond_c

    :cond_a
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    const-string v15, "/storage/extSdCard"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_b

    sget-boolean v14, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v14, :cond_c

    :cond_b
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    const-string v15, "/storage/UsbDrive"

    invoke-virtual {v14, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_e

    sget-boolean v14, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-nez v14, :cond_e

    .line 597
    :cond_c
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 598
    .local v10, "res":Landroid/content/res/Resources;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v12

    .line 599
    .local v12, "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const v15, 0x7f0b003b

    invoke-virtual {v10, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const v15, 0x7f0b003c

    invoke-virtual {v10, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 601
    .local v8, "notiStr":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    const/4 v15, 0x0

    invoke-static {v14, v8, v15}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v14

    invoke-virtual {v14}, Landroid/widget/Toast;->show()V

    .line 602
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v16

    invoke-static/range {v14 .. v16}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteShortcut(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v11

    .line 603
    .local v11, "rowsDeleted":I
    if-lez v11, :cond_d

    .line 605
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    const v15, 0x7f0b00d8

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v14

    invoke-virtual {v14}, Landroid/widget/Toast;->show()V

    .line 606
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->removeItem(Ljava/lang/Object;)Z

    .line 607
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mShortcutClickCounter:I

    .line 608
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0

    .line 614
    :cond_d
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    const v15, 0x7f0b0063

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v14

    invoke-virtual {v14}, Landroid/widget/Toast;->show()V

    .line 619
    .end local v8    # "notiStr":Ljava/lang/String;
    .end local v10    # "res":Landroid/content/res/Resources;
    .end local v11    # "rowsDeleted":I
    .end local v12    # "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    :cond_e
    const-string v14, "FOLDERPATH"

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v2, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 625
    .end local v5    # "file":Ljava/io/File;
    :sswitch_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    if-nez v14, :cond_f

    .line 627
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getNearbyDevicesProvider()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->setNearbyDevicesProvider(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)V

    .line 629
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-static {}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getCategoryAdapterDevicesCollection()Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->setDevicesCollection(Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;)V

    .line 633
    :cond_f
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    if-eqz v14, :cond_8

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->checkAllshareEnabled()Z

    move-result v14

    if-eqz v14, :cond_8

    .line 635
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    if-eqz v14, :cond_8

    .line 637
    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v9

    .line 639
    .local v9, "path":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->getInstance()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    invoke-virtual {v15, v9}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->getDevice(Ljava/lang/String;)Lcom/samsung/android/allshare/Device;

    move-result-object v15

    invoke-virtual {v14, v9, v15}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->registerDevice(Ljava/lang/String;Lcom/samsung/android/allshare/Device;)V

    .line 641
    const-string v14, "FOLDERPATH"

    invoke-virtual {v2, v14, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 649
    .end local v9    # "path":Ljava/lang/String;
    :sswitch_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v14

    if-eqz v14, :cond_10

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/myfiles/utils/Utils;->isWifiOn(Landroid/content/Context;)Z

    move-result v14

    if-nez v14, :cond_10

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckFTP()Z

    move-result v14

    if-nez v14, :cond_10

    .line 652
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mShortCutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 654
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/4 v15, 0x4

    invoke-virtual {v14, v15}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->showDataWarningDialog(I)V

    goto/16 :goto_0

    .line 658
    :cond_10
    sget-boolean v14, Lcom/sec/android/app/myfiles/utils/Utils;->mIsActiveWarningUsingWLAN:Z

    if-eqz v14, :cond_11

    const-string v14, "ChinaNalSecurity"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v15

    const-string v16, "CscFeature_Common_ConfigLocalSecurityPolicy"

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_11

    .line 660
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/myfiles/utils/Utils;->isWifiOn(Landroid/content/Context;)Z

    move-result v14

    if-eqz v14, :cond_11

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckWLANOn()Z

    move-result v14

    if-nez v14, :cond_11

    .line 662
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mShortCutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 663
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/16 v15, 0xa

    invoke-virtual {v14, v15}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->showDataWarningDialog(I)V

    goto/16 :goto_0

    .line 668
    :cond_11
    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getCategoryType()I

    move-result v3

    .line 670
    const-string v14, "FOLDERPATH"

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v2, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 671
    const-string v14, "shortcut_working_index_intent_key"

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mCurrentWorkingIndex:I

    invoke-virtual {v2, v14, v15}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_3

    .line 678
    :sswitch_3
    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/sec/android/app/myfiles/utils/Utils;->isDropboxFolder(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_12

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/myfiles/utils/Utils;->isDropboxAccountIsSignin(Landroid/content/Context;)Z

    move-result v14

    if-nez v14, :cond_13

    :cond_12
    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/sec/android/app/myfiles/utils/Utils;->isBaiduFolder(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_8

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/myfiles/utils/Utils;->isBaiduCloudAccountIsSignin(Landroid/content/Context;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 681
    :cond_13
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v14

    if-eqz v14, :cond_16

    .line 683
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/myfiles/utils/Utils;->isWifiOn(Landroid/content/Context;)Z

    move-result v14

    if-nez v14, :cond_15

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckDropBox()Z

    move-result v14

    if-nez v14, :cond_15

    .line 686
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mShortCutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 687
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setDropBoxFromShortcut(Z)V

    .line 688
    const/16 v14, 0x1f

    if-ne v3, v14, :cond_14

    .line 689
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/4 v15, 0x5

    invoke-virtual {v14, v15}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->showDataWarningDialog(I)V

    goto/16 :goto_0

    .line 691
    :cond_14
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/4 v15, 0x3

    invoke-virtual {v14, v15}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->showDataWarningDialog(I)V

    goto/16 :goto_0

    .line 695
    :cond_15
    const-string v14, "FOLDERPATH"

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v2, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 696
    const-string v14, "shortcut_working_index_intent_key"

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mCurrentWorkingIndex:I

    invoke-virtual {v2, v14, v15}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_3

    .line 701
    :cond_16
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 702
    .restart local v10    # "res":Landroid/content/res/Resources;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v12

    .line 703
    .restart local v12    # "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const v15, 0x7f0b003b

    invoke-virtual {v10, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const v15, 0x7f0b003c

    invoke-virtual {v10, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 705
    .restart local v8    # "notiStr":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    const/4 v15, 0x0

    invoke-static {v14, v8, v15}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v14

    invoke-virtual {v14}, Landroid/widget/Toast;->show()V

    .line 706
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v16

    invoke-static/range {v14 .. v16}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteShortcut(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v11

    .line 708
    .restart local v11    # "rowsDeleted":I
    if-lez v11, :cond_17

    .line 710
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    const v15, 0x7f0b00d8

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v14

    invoke-virtual {v14}, Landroid/widget/Toast;->show()V

    .line 711
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->removeItem(Ljava/lang/Object;)Z

    .line 712
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mShortcutClickCounter:I

    .line 713
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0

    .line 719
    :cond_17
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    const v15, 0x7f0b0063

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v14

    invoke-virtual {v14}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 736
    .end local v8    # "notiStr":Ljava/lang/String;
    .end local v10    # "res":Landroid/content/res/Resources;
    .end local v11    # "rowsDeleted":I
    .end local v12    # "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v14

    if-nez v14, :cond_9

    .line 738
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    const v16, 0x7f0b00c7

    invoke-virtual/range {v15 .. v16}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v14

    invoke-virtual {v14}, Landroid/widget/Toast;->show()V

    .line 740
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mShortcutClickCounter:I

    goto/16 :goto_0

    .line 757
    .end local v2    # "argument":Landroid/os/Bundle;
    .end local v3    # "categoryType":I
    .end local v7    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :cond_18
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/myfiles/utils/Utils;->isDropBoxEnabled(Landroid/content/Context;)Z

    move-result v14

    if-eqz v14, :cond_1a

    .line 758
    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->SetShortcutDialogState(Z)V

    .line 760
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    if-eqz v14, :cond_1

    .line 761
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getStartBrowserState()Z

    move-result v14

    if-nez v14, :cond_1

    .line 762
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 763
    .local v1, "arg":Landroid/os/Bundle;
    const-string v14, "shortcut_working_index_intent_key"

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mCurrentWorkingIndex:I

    invoke-virtual {v1, v14, v15}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 764
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->dialog:Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;

    if-nez v14, :cond_19

    .line 766
    new-instance v14, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;

    invoke-direct {v14}, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;-><init>()V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->dialog:Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;

    .line 767
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->dialog:Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;

    invoke-virtual {v14, v1}, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->setArguments(Landroid/os/Bundle;)V

    .line 768
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->dialog:Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/16 v16, 0x7

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 769
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->dialog:Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;

    const-string v15, "category_home"

    invoke-virtual {v14, v15}, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->setTargetFragmentTag(Ljava/lang/String;)V

    .line 770
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->dialog:Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->setShowsDialog(Z)V

    .line 771
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->dialog:Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v15}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v15

    const-class v16, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 773
    :cond_19
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->dialog:Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;

    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->getShowsDialog()Z

    move-result v14

    if-nez v14, :cond_1

    .line 774
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->dialog:Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->setShowsDialog(Z)V

    .line 775
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->dialog:Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;

    invoke-virtual {v14, v1}, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->setArguments(Landroid/os/Bundle;)V

    .line 776
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->dialog:Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v15}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v15

    const-class v16, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/myfiles/fragment/AddShortcutDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 782
    .end local v1    # "arg":Landroid/os/Bundle;
    :cond_1a
    new-instance v6, Landroid/content/Intent;

    const-string v14, "com.sec.android.app.myfiles.PICK_SELECT_PATH"

    invoke-direct {v6, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 783
    .local v6, "intent":Landroid/content/Intent;
    const-string v14, "SELECTOR_SOURCE_TYPE"

    const/4 v15, 0x0

    invoke-virtual {v6, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 784
    const-string v14, "from_add_shortcut"

    const/4 v15, 0x1

    invoke-virtual {v6, v14, v15}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 785
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    instance-of v14, v14, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v14, :cond_1

    .line 789
    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/4 v15, 0x3

    invoke-virtual {v14, v6, v15}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 791
    :catch_0
    move-exception v4

    .line 793
    .local v4, "e":Landroid/content/ActivityNotFoundException;
    const/4 v14, 0x2

    const-string v15, "shortcut"

    const-string v16, "com.sec.android.app.myfiles.PICK_SELECT_PATH not found - click"

    invoke-static/range {v14 .. v16}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 585
    nop

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_3
        0x9 -> :sswitch_1
        0x1a -> :sswitch_0
        0x1c -> :sswitch_2
        0x1f -> :sswitch_3
    .end sparse-switch

    .line 732
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 5
    .param p1, "shortcutView"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 825
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 828
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->myCurrentPageIndex:I

    iget v4, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mItemCount:I

    mul-int/2addr v3, v4

    add-int/2addr v0, v3

    iput v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mCurrentWorkingIndex:I

    .line 832
    :goto_0
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mCurrentWorkingIndex:I

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    iget v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mCurrentWorkingIndex:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v0

    const/16 v3, 0x1b

    if-ne v0, v3, :cond_2

    :cond_0
    move v0, v2

    .line 840
    :goto_1
    return v0

    .line 830
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mCurrentWorkingIndex:I

    goto :goto_0

    .line 835
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mCurrentWorkingIndex:I

    invoke-virtual {v0, v1, v3, v2}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->startSelectMode(III)V

    .line 838
    invoke-virtual {p0, v2, v2}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->startSelectMode(II)V

    .line 839
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mCurrentWorkingIndex:I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->selectItem(I)V

    move v0, v1

    .line 840
    goto :goto_1
.end method

.method public refresh()V
    .locals 1

    .prologue
    .line 1164
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mShortcutClickCounter:I

    .line 1173
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->notifyDataSetChanged()V

    .line 1174
    return-void
.end method

.method public removeAllItem(Ljava/util/ArrayList;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1216
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    if-eqz p1, :cond_2

    .line 1218
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    monitor-enter v4

    .line 1220
    const/4 v2, 0x0

    .line 1222
    .local v2, "removeItem":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    :try_start_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 1224
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    move-object v2, v0

    .line 1226
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v3

    const/16 v5, 0x9

    if-ne v3, v5, :cond_0

    .line 1228
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->nearByDevicesConnected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1230
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const v6, 0x7f0b0083

    invoke-virtual {v5, v6}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-static {v3, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 1233
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->resetNearByDevicesList()V

    .line 1222
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1238
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    move-result v3

    monitor-exit v4

    .line 1243
    .end local v1    # "i":I
    .end local v2    # "removeItem":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :goto_1
    return v3

    .line 1239
    .restart local v1    # "i":I
    .restart local v2    # "removeItem":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 1243
    .end local v1    # "i":I
    .end local v2    # "removeItem":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public removeItem(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 1203
    const/4 v0, 0x0

    .line 1205
    .local v0, "result":Z
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    monitor-enter v2

    .line 1207
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 1208
    monitor-exit v2

    .line 1210
    return v0

    .line 1208
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public selectAllItem()V
    .locals 4

    .prologue
    .line 1032
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    monitor-enter v3

    .line 1034
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 1036
    .local v1, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->isSelectable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1038
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->setSelected(Z)V

    goto :goto_0

    .line 1041
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1043
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setAllItemCount()V

    .line 1044
    invoke-super {p0}, Landroid/support/v4/view/PagerAdapter;->notifyDataSetChanged()V

    .line 1045
    return-void
.end method

.method public selectItem(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 1009
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    monitor-enter v2

    .line 1011
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 1013
    .local v0, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->isSelected()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1015
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->isSelectable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1017
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->setSelected(Z)V

    .line 1023
    :cond_0
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1024
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setAllItemCount()V

    .line 1025
    invoke-super {p0}, Landroid/support/v4/view/PagerAdapter;->notifyDataSetChanged()V

    .line 1026
    return-void

    .line 1021
    :cond_1
    const/4 v1, 0x0

    :try_start_1
    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->setSelected(Z)V

    goto :goto_0

    .line 1023
    .end local v0    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public selectMode(Z)V
    .locals 0
    .param p1, "selectMode"    # Z

    .prologue
    .line 1262
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->isSelectMode:Z

    .line 1263
    return-void
.end method

.method public setArrayList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/ShortcutItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 176
    .local p1, "mArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/ShortcutItem;>;"
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    .line 177
    return-void
.end method

.method public setDevicesCollection(Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;)V
    .locals 0
    .param p1, "mDevicesCollection"    # Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    .prologue
    .line 1250
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    .line 1251
    return-void
.end method

.method public setNearbyDevicesProvider(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)V
    .locals 0
    .param p1, "mNearbyDevicesProvider"    # Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    .prologue
    .line 1256
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    .line 1257
    return-void
.end method

.method public setScreenState(I)V
    .locals 3
    .param p1, "screenState"    # I

    .prologue
    const v2, 0x7f04001d

    const/4 v1, 0x4

    .line 122
    iput p1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mScreenState:I

    .line 124
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mScreenState:I

    packed-switch v0, :pswitch_data_0

    .line 141
    :pswitch_0
    iput v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mItemCount:I

    .line 142
    iput v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mLayoutResId:I

    .line 145
    :goto_0
    return-void

    .line 127
    :pswitch_1
    iput v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mItemCount:I

    .line 128
    iput v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mLayoutResId:I

    goto :goto_0

    .line 131
    :pswitch_2
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mItemCount:I

    .line 132
    iput v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mLayoutResId:I

    goto :goto_0

    .line 136
    :pswitch_3
    iput v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mItemCount:I

    .line 137
    const v0, 0x7f04001f

    iput v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mLayoutResId:I

    goto :goto_0

    .line 124
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public setSelectedItemChangeListener(Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;

    .prologue
    .line 1069
    return-void
.end method

.method public shortcutEdit()V
    .locals 5

    .prologue
    .line 1295
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 1296
    .local v2, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    const/16 v1, 0xd

    .line 1297
    .local v1, "categoryType":I
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1298
    .local v0, "argument":Landroid/os/Bundle;
    const-string v3, "FOLDERPATH"

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1299
    const-string v3, "shortcut_working_index_intent_key"

    iget v4, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mCurrentWorkingIndex:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1300
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    instance-of v3, v3, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v3, :cond_0

    .line 1301
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v3, v1, v0}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    .line 1304
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->finishSelectMode()V

    .line 1306
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->finishSelectMode()V

    .line 1307
    return-void
.end method

.method public shortcutRename()V
    .locals 5

    .prologue
    .line 1268
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 1269
    .local v1, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    new-instance v2, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    invoke-direct {v2}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;-><init>()V

    .line 1271
    .local v2, "newName":Landroid/app/DialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1273
    .local v0, "argument":Landroid/os/Bundle;
    const-string v3, "current_item"

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1275
    const-string v3, "title"

    const v4, 0x7f0b0023

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1277
    const-string v3, "file_name"

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1279
    invoke-virtual {v2, v0}, Landroid/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 1281
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    if-nez v3, :cond_0

    .line 1293
    :goto_0
    return-void

    .line 1286
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "category_home"

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/16 v4, 0x10

    invoke-virtual {v2, v3, v4}, Landroid/app/DialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 1288
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "rename"

    invoke-virtual {v2, v3, v4}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 1290
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->finishSelectMode()V

    .line 1292
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->finishSelectMode()V

    goto :goto_0
.end method

.method public startBrowserFTPorDropBox()V
    .locals 3

    .prologue
    .line 803
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mShortCutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    if-nez v1, :cond_1

    .line 817
    :cond_0
    :goto_0
    return-void

    .line 806
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 808
    .local v0, "argument":Landroid/os/Bundle;
    const-string v1, "FOLDERPATH"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mShortCutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 809
    const-string v1, "shortcut_working_index_intent_key"

    iget v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mCurrentWorkingIndex:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 811
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v1, :cond_0

    .line 813
    const-string v1, "IS_SHORTCUT_FOLDER"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 814
    const-string v1, "SHORTCUT_ROOT_FOLDER"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mShortCutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 815
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mShortCutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getCategoryType()I

    move-result v2

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public startSelectMode(II)V
    .locals 1
    .param p1, "selectType"    # I
    .param p2, "from"    # I

    .prologue
    .line 984
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->isSelectMode:Z

    .line 986
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->notifyDataSetChanged()V

    .line 987
    return-void
.end method

.method public unselectAllItem()V
    .locals 4

    .prologue
    .line 1051
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    monitor-enter v3

    .line 1053
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 1054
    .local v1, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    if-eqz v1, :cond_0

    .line 1055
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->setSelected(Z)V

    goto :goto_0

    .line 1057
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1058
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setAllItemCount()V

    .line 1059
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->isSelectMode:Z

    if-eqz v2, :cond_2

    .line 1060
    invoke-super {p0}, Landroid/support/v4/view/PagerAdapter;->notifyDataSetChanged()V

    .line 1064
    :goto_1
    return-void

    .line 1062
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->notifyDataSetChanged()V

    goto :goto_1
.end method
