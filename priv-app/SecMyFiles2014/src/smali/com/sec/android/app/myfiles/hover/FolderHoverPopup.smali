.class public Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;
.super Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
.source "FolderHoverPopup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;,
        Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;,
        Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;,
        Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;
    }
.end annotation


# static fields
.field private static final MODULE:Ljava/lang/String; = "FolderHoverPopup"


# instance fields
.field private final DISPLAY_NUM:I

.field private final SLEEP_TIME:I

.field private final SLEEP_TIME_EXITING:I

.field private folderHoverPopupAdapter:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;

.field private mArrayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;",
            ">;"
        }
    .end annotation
.end field

.field private mChangeListThread:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;

.field private mDisplayFileCount:I

.field private mDisplayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;",
            ">;"
        }
    .end annotation
.end field

.field private mExitingHovering:Z

.field private mFileTouchListener:Landroid/view/View$OnTouchListener;

.field private mGridItemWidth:I

.field private mHoverFileList:Landroid/widget/GridView;

.field private mHoverMoved:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 94
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;-><init>(Landroid/content/Context;)V

    .line 63
    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverFileList:Landroid/widget/GridView;

    .line 65
    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mArrayList:Ljava/util/ArrayList;

    .line 67
    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mDisplayList:Ljava/util/ArrayList;

    .line 69
    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->folderHoverPopupAdapter:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;

    .line 71
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mDisplayFileCount:I

    .line 73
    const/16 v0, 0x10

    iput v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->DISPLAY_NUM:I

    .line 75
    const/16 v0, 0x54

    iput v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mGridItemWidth:I

    .line 77
    const/16 v0, 0xbb8

    iput v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->SLEEP_TIME:I

    .line 79
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->SLEEP_TIME_EXITING:I

    .line 91
    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mChangeListThread:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;

    .line 461
    new-instance v0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$3;-><init>(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mFileTouchListener:Landroid/view/View$OnTouchListener;

    .line 96
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverMoved:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;
    .param p1, "x1"    # Z

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverMoved:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mArrayList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mExitingHovering:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;
    .param p1, "x1"    # Z

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mExitingHovering:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mChangeListThread:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;)Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;
    .param p1, "x1"    # Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mChangeListThread:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->folderHoverPopupAdapter:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mDisplayList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    .prologue
    .line 59
    iget v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mDisplayFileCount:I

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;
    .param p1, "x1"    # I

    .prologue
    .line 59
    iput p1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mDisplayFileCount:I

    return p1
.end method

.method static synthetic access$608(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    .prologue
    .line 59
    iget v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mDisplayFileCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mDisplayFileCount:I

    return v0
.end method


# virtual methods
.method protected changeList()V
    .locals 1

    .prologue
    .line 586
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->folderHoverPopupAdapter:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;

    if-eqz v0, :cond_0

    .line 587
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->folderHoverPopupAdapter:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;->notifyDataSetChanged()V

    .line 588
    :cond_0
    return-void
.end method

.method protected changeListThreadStart()V
    .locals 3

    .prologue
    .line 571
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mChangeListThread:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;

    if-nez v0, :cond_0

    .line 573
    new-instance v0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;-><init>(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mChangeListThread:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;

    .line 576
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mChangeListThread:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mChangeListThread:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_1

    .line 578
    const/4 v0, 0x0

    const-string v1, "FolderHoverPopup"

    const-string v2, "mChangeListThread.start()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 579
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mChangeListThread:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;->start()V

    .line 581
    :cond_1
    return-void
.end method

.method public convertDpToPixels(FLandroid/content/Context;)I
    .locals 3
    .param p1, "dp"    # F
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1019
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1020
    .local v0, "resources":Landroid/content/res/Resources;
    const/4 v1, 0x1

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v1, p1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    return v1
.end method

.method protected dissmissPopup()V
    .locals 4

    .prologue
    .line 523
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->isShow()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 532
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 534
    const/4 v1, 0x0

    const-string v2, "FolderHoverPopup"

    const-string v3, "FolderHoverDlg.dismiss(); "

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 542
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    .line 544
    :cond_0
    return-void

    .line 536
    :catch_0
    move-exception v0

    .line 538
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method protected initViews()Z
    .locals 12

    .prologue
    .line 101
    const/4 v0, 0x0

    const-string v10, "FolderHoverPopup"

    const-string v11, "initViews"

    invoke-static {v0, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v10, "layout_inflater"

    invoke-virtual {v0, v10}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/LayoutInflater;

    .line 104
    .local v7, "inflater":Landroid/view/LayoutInflater;
    const/4 v8, 0x0

    .line 106
    .local v8, "layout":Landroid/widget/RelativeLayout;
    new-instance v0, Landroid/app/Dialog;

    iget-object v10, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-direct {v0, v10}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverCursor:Landroid/database/Cursor;

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_8

    .line 115
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mDisplayFileCount:I

    .line 117
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mArrayList:Ljava/util/ArrayList;

    .line 118
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mDisplayList:Ljava/util/ArrayList;

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 129
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverCursor:Landroid/database/Cursor;

    iget-object v10, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverCursor:Landroid/database/Cursor;

    const-string v11, "format"

    invoke-interface {v10, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 132
    .local v4, "fileFormat":I
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverCursor:Landroid/database/Cursor;

    iget-object v10, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverCursor:Landroid/database/Cursor;

    const-string v11, "_data"

    invoke-interface {v10, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 133
    .local v2, "filePath":Ljava/lang/String;
    const/16 v0, 0x2f

    invoke-virtual {v2, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 134
    .local v3, "fileName":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v2, v0}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;Landroid/content/Context;)I

    move-result v5

    .line 135
    .local v5, "fileType":I
    invoke-static {v2}, Lcom/sec/android/app/myfiles/MediaFile;->getSmallIcon(Ljava/lang/String;)I

    move-result v1

    .line 137
    .local v1, "fileIconId":I
    invoke-static {v5}, Lcom/sec/android/app/myfiles/MediaFile;->isVideoFileType(I)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {v5}, Lcom/sec/android/app/myfiles/MediaFile;->isImageFileType(I)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {v5}, Lcom/sec/android/app/myfiles/MediaFile;->isAudioFileType(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 140
    :cond_2
    iget-object v10, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mArrayList:Ljava/util/ArrayList;

    new-instance v0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;-><init>(ILjava/lang/String;Ljava/lang/String;II)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 149
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 153
    .end local v1    # "fileIconId":I
    .end local v2    # "filePath":Ljava/lang/String;
    .end local v3    # "fileName":Ljava/lang/String;
    .end local v4    # "fileFormat":I
    .end local v5    # "fileType":I
    :cond_3
    const v0, 0x7f040028

    const/4 v10, 0x0

    invoke-virtual {v7, v0, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    .end local v8    # "layout":Landroid/widget/RelativeLayout;
    check-cast v8, Landroid/widget/RelativeLayout;

    .line 154
    .restart local v8    # "layout":Landroid/widget/RelativeLayout;
    const v0, 0x7f0f00b2

    invoke-virtual {v8, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverFileList:Landroid/widget/GridView;

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverFileList:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    .line 156
    .local v9, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v10, 0x4

    if-ge v0, v10, :cond_4

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 185
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v10, 0x10

    if-le v0, v10, :cond_7

    .line 187
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_2
    const/16 v0, 0x10

    if-ge v6, v0, :cond_6

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mDisplayList:Ljava/util/ArrayList;

    iget-object v10, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mArrayList:Ljava/util/ArrayList;

    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 190
    iput v6, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mDisplayFileCount:I

    .line 187
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 144
    .end local v6    # "i":I
    .end local v9    # "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    .restart local v1    # "fileIconId":I
    .restart local v2    # "filePath":Ljava/lang/String;
    .restart local v3    # "fileName":Ljava/lang/String;
    .restart local v4    # "fileFormat":I
    .restart local v5    # "fileType":I
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mArrayList:Ljava/util/ArrayList;

    new-instance v10, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;

    invoke-direct {v10, v1, v2, v3, v4}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;-><init>(ILjava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 161
    .end local v1    # "fileIconId":I
    .end local v2    # "filePath":Ljava/lang/String;
    .end local v3    # "fileName":Ljava/lang/String;
    .end local v4    # "fileFormat":I
    .end local v5    # "fileType":I
    .restart local v9    # "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    :pswitch_0
    iget v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mGridItemWidth:I

    int-to-float v0, v0

    iget-object v10, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {p0, v0, v10}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->convertDpToPixels(FLandroid/content/Context;)I

    move-result v0

    iput v0, v9, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverFileList:Landroid/widget/GridView;

    invoke-virtual {v0, v9}, Landroid/widget/GridView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverFileList:Landroid/widget/GridView;

    const/4 v10, 0x1

    invoke-virtual {v0, v10}, Landroid/widget/GridView;->setNumColumns(I)V

    goto :goto_1

    .line 168
    :pswitch_1
    iget v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mGridItemWidth:I

    int-to-float v0, v0

    iget-object v10, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {p0, v0, v10}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->convertDpToPixels(FLandroid/content/Context;)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    iput v0, v9, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverFileList:Landroid/widget/GridView;

    invoke-virtual {v0, v9}, Landroid/widget/GridView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverFileList:Landroid/widget/GridView;

    const/4 v10, 0x2

    invoke-virtual {v0, v10}, Landroid/widget/GridView;->setNumColumns(I)V

    goto :goto_1

    .line 175
    :pswitch_2
    iget v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mGridItemWidth:I

    int-to-float v0, v0

    iget-object v10, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {p0, v0, v10}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->convertDpToPixels(FLandroid/content/Context;)I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    iput v0, v9, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverFileList:Landroid/widget/GridView;

    invoke-virtual {v0, v9}, Landroid/widget/GridView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverFileList:Landroid/widget/GridView;

    const/4 v10, 0x3

    invoke-virtual {v0, v10}, Landroid/widget/GridView;->setNumColumns(I)V

    goto :goto_1

    .line 195
    .restart local v6    # "i":I
    :cond_6
    iget v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mDisplayFileCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mDisplayFileCount:I

    .line 198
    new-instance v0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;

    iget-object v10, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v11, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mDisplayList:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v10, v11}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;-><init>(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->folderHoverPopupAdapter:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;

    .line 207
    .end local v6    # "i":I
    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverFileList:Landroid/widget/GridView;

    iget-object v10, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->folderHoverPopupAdapter:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;

    invoke-virtual {v0, v10}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 209
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->setDialogProperties()V

    .line 210
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->setDialogPosition()V

    .line 212
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v0, v8}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverFileList:Landroid/widget/GridView;

    iget-object v10, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mFileTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v10}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 225
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverMoved:Z

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverFileList:Landroid/widget/GridView;

    new-instance v10, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;

    invoke-direct {v10, p0}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;-><init>(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)V

    invoke-virtual {v0, v10}, Landroid/widget/GridView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 394
    const/4 v0, 0x1

    .line 458
    .end local v9    # "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    :goto_4
    return v0

    .line 202
    .restart local v9    # "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    :cond_7
    new-instance v0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;

    iget-object v10, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v11, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mArrayList:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v10, v11}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;-><init>(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->folderHoverPopupAdapter:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;

    goto :goto_3

    .line 396
    .end local v9    # "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_9

    .line 398
    const v0, 0x7f040029

    const/4 v10, 0x0

    invoke-virtual {v7, v0, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    .end local v8    # "layout":Landroid/widget/RelativeLayout;
    check-cast v8, Landroid/widget/RelativeLayout;

    .line 400
    .restart local v8    # "layout":Landroid/widget/RelativeLayout;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->setDialogProperties()V

    .line 401
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->setDialogPosition()V

    .line 403
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v0, v8}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 406
    new-instance v0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$2;-><init>(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)V

    invoke-virtual {v8, v0}, Landroid/widget/RelativeLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 455
    const/4 v0, 0x1

    goto :goto_4

    .line 458
    :cond_9
    const/4 v0, 0x0

    goto :goto_4

    .line 158
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected setDialogPosition()V
    .locals 4

    .prologue
    .line 548
    const/4 v1, 0x0

    const-string v2, "FolderHoverPopup"

    const-string v3, "setPosition"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    .line 550
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 559
    .local v0, "params":Landroid/view/WindowManager$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v1, v1, -0x32

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 560
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    add-int/lit8 v1, v1, -0x64

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 563
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 564
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x33

    invoke-virtual {v1, v2}, Landroid/view/Window;->setGravity(I)V

    .line 566
    return-void
.end method

.method protected showContent()V
    .locals 0

    .prologue
    .line 518
    return-void
.end method
