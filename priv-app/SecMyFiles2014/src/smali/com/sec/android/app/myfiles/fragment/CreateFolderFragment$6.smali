.class Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$6;
.super Ljava/lang/Object;
.source "CreateFolderFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->setupInputEditText(Landroid/widget/EditText;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;)V
    .locals 0

    .prologue
    .line 454
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 483
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 479
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    const/4 v1, 0x1

    .line 458
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->mOkButton:Landroid/widget/Button;

    if-eqz v0, :cond_1

    .line 460
    if-eqz p1, :cond_0

    .line 462
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result p4

    .line 466
    :cond_0
    if-ge p4, v1, :cond_2

    .line 468
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->mOkButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 475
    :cond_1
    :goto_0
    return-void

    .line 472
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->mOkButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method
