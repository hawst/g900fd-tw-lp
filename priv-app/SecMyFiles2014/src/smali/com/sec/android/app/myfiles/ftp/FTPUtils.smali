.class public Lcom/sec/android/app/myfiles/ftp/FTPUtils;
.super Ljava/lang/Object;
.source "FTPUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCurrentTime()J
    .locals 2

    .prologue
    .line 54
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method public static getCurrentTimeStr()Ljava/lang/String;
    .locals 2

    .prologue
    .line 60
    invoke-static {}, Lcom/sec/android/app/myfiles/ftp/FTPUtils;->getCurrentTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 42
    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 44
    .local v0, "lastIndex":I
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 48
    .end local v0    # "lastIndex":I
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "/"

    goto :goto_0
.end method

.method public static getFtpUri(Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/FTPParams;)Landroid/net/Uri;
    .locals 6
    .param p0, "itemStr"    # Ljava/lang/String;
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .prologue
    .line 80
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    if-eqz p1, :cond_2

    .line 82
    invoke-static {p0}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getItem(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPItem;

    move-result-object v1

    .line 84
    .local v1, "item":Lcom/sec/android/app/myfiles/ftp/FTPItem;
    if-eqz v1, :cond_2

    .line 86
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 88
    .local v0, "builder":Landroid/net/Uri$Builder;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 90
    const-string v2, "size"

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getSize()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 91
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->isSymbolicLink()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 92
    const-string v2, "type"

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 97
    :goto_0
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 103
    .end local v0    # "builder":Landroid/net/Uri$Builder;
    .end local v1    # "item":Lcom/sec/android/app/myfiles/ftp/FTPItem;
    :goto_1
    return-object v2

    .line 94
    .restart local v0    # "builder":Landroid/net/Uri$Builder;
    .restart local v1    # "item":Lcom/sec/android/app/myfiles/ftp/FTPItem;
    :cond_0
    const-string v3, "type"

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_2
    invoke-virtual {v0, v3, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 103
    .end local v0    # "builder":Landroid/net/Uri$Builder;
    .end local v1    # "item":Lcom/sec/android/app/myfiles/ftp/FTPItem;
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static getItemPathFromUri(Landroid/net/Uri;)Ljava/lang/String;
    .locals 8
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 109
    if-eqz p0, :cond_0

    .line 111
    invoke-virtual {p0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 113
    .local v0, "fullPath":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 115
    invoke-static {v0}, Lcom/sec/android/app/myfiles/ftp/FTPUtils;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "type"

    invoke-virtual {p0, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v0}, Lcom/sec/android/app/myfiles/ftp/FTPUtils;->getParentDir(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "size"

    invoke-virtual {p0, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v2, v3, v4, v6, v7}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getItem(Ljava/lang/String;ILjava/lang/String;J)Lcom/sec/android/app/myfiles/ftp/FTPItem;

    move-result-object v1

    .line 118
    .local v1, "item":Lcom/sec/android/app/myfiles/ftp/FTPItem;
    if-eqz v1, :cond_0

    .line 120
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->toString()Ljava/lang/String;

    move-result-object v2

    .line 128
    .end local v0    # "fullPath":Ljava/lang/String;
    .end local v1    # "item":Lcom/sec/android/app/myfiles/ftp/FTPItem;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getParentDir(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getParentFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 34
    .local v0, "parent":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .end local v0    # "parent":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "parent":Ljava/lang/String;
    :cond_0
    sget-object v0, Ljava/io/File;->separator:Ljava/lang/String;

    goto :goto_0
.end method

.method public static final trimFtpPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 66
    if-eqz p0, :cond_0

    .line 68
    const-string v0, "/+"

    const-string v1, "/"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 72
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "/"

    goto :goto_0
.end method
