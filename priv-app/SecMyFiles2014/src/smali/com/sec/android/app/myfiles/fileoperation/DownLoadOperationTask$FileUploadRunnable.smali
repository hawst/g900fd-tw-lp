.class public Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$FileUploadRunnable;
.super Ljava/lang/Object;
.source "DownLoadOperationTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FileUploadRunnable"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$FileUploadRunnable;->this$0:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 127
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$FileUploadRunnable;->this$0:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->access$100(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;)Ljava/util/ArrayList;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 128
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$FileUploadRunnable;->this$0:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->access$100(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;

    .line 129
    .local v3, "file":Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$FileUploadRunnable;->this$0:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->isCancelled()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 155
    .end local v3    # "file":Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_1
    monitor-enter p0

    .line 159
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 165
    :goto_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 166
    return-void

    .line 131
    .restart local v3    # "file":Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;
    .restart local v4    # "i$":Ljava/util/Iterator;
    :cond_2
    const/4 v7, 0x0

    .line 132
    .local v7, "started":Z
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$FileUploadRunnable;->this$0:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->access$100(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;)Ljava/util/ArrayList;

    move-result-object v9

    monitor-enter v9

    .line 133
    :try_start_2
    iget-boolean v7, v3, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mStarted:Z

    .line 134
    const/4 v8, 0x1

    iput-boolean v8, v3, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mStarted:Z

    .line 135
    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 136
    if-nez v7, :cond_0

    .line 138
    :try_start_3
    iget-object v8, v3, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mUpload:Lcom/samsung/scloud/DropboxAPI$CancelableUpload;

    invoke-virtual {v8}, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->putFileCancelable()Lcom/samsung/scloud/data/SCloudFile;

    move-result-object v0

    .line 139
    .local v0, "cloudFile":Lcom/samsung/scloud/data/SCloudFile;
    if-eqz v0, :cond_0

    iget-object v8, v3, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mNewFileName:Ljava/lang/String;

    if-eqz v8, :cond_0

    iget-object v8, v3, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mNewFileName:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_0

    iget-object v8, v3, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mNewFileName:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/samsung/scloud/data/SCloudFile;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 141
    iget-object v5, v3, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mNewFileName:Ljava/lang/String;

    .line 142
    .local v5, "newName":Ljava/lang/String;
    const/16 v8, 0x2e

    invoke-virtual {v5, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v6

    .line 143
    .local v6, "pos":I
    if-lez v6, :cond_3

    .line 144
    const/4 v8, 0x0

    invoke-virtual {v5, v8, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 145
    :cond_3
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$FileUploadRunnable;->this$0:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;
    invoke-static {v8}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->access$200(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;)Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    move-result-object v8

    invoke-virtual {v0}, Lcom/samsung/scloud/data/SCloudFile;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9, v5}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->renameFile(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_3
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 147
    .end local v0    # "cloudFile":Lcom/samsung/scloud/data/SCloudFile;
    .end local v5    # "newName":Ljava/lang/String;
    .end local v6    # "pos":I
    :catch_0
    move-exception v1

    .line 148
    .local v1, "e":Lcom/samsung/scloud/exception/SCloudException;
    invoke-virtual {v1}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V

    goto :goto_0

    .line 135
    .end local v1    # "e":Lcom/samsung/scloud/exception/SCloudException;
    :catchall_0
    move-exception v8

    :try_start_4
    monitor-exit v9
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v8

    .line 149
    :catch_1
    move-exception v1

    .line 150
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 161
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v3    # "file":Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v7    # "started":Z
    :catch_2
    move-exception v2

    .line 163
    .local v2, "ex":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 165
    .end local v2    # "ex":Ljava/lang/Exception;
    :catchall_1
    move-exception v8

    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v8
.end method
