.class Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$5;
.super Ljava/lang/Object;
.source "RenameOnConflictFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->setupInputEditText(Landroid/widget/EditText;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;)V
    .locals 0

    .prologue
    .line 187
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 204
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 201
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    const/4 v1, 0x1

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mOkButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 191
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result p4

    .line 192
    if-ge p4, v1, :cond_1

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mOkButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 195
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mOkButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method
