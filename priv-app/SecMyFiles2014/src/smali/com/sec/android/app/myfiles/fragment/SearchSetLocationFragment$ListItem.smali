.class Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;
.super Ljava/lang/Object;
.source "SearchSetLocationFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ListItem"
.end annotation


# instance fields
.field private isChecked:Z

.field private text:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Z)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "isChecked"    # Z

    .prologue
    .line 591
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 592
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->text:Ljava/lang/String;

    .line 593
    iput-boolean p2, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->isChecked:Z

    .line 594
    return-void
.end method


# virtual methods
.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 597
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->text:Ljava/lang/String;

    return-object v0
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 603
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->isChecked:Z

    return v0
.end method

.method public setChecked(Z)V
    .locals 0
    .param p1, "isChecked"    # Z

    .prologue
    .line 606
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->isChecked:Z

    .line 607
    return-void
.end method
