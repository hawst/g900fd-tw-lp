.class public interface abstract Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$ContentColumns;
.super Ljava/lang/Object;
.source "RemoteShareNavigation.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ContentColumns"
.end annotation


# static fields
.field public static final CHUNK_COUNT:Ljava/lang/String; = "chunk_count"

.field public static final CHUNK_SIZE:Ljava/lang/String; = "chunk_size"

.field public static final COMPLETE_COUNT:Ljava/lang/String; = "complete_count"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "content_type"

.field public static final FILE_NAME:Ljava/lang/String; = "filename"

.field public static final FILE_SIZE:Ljava/lang/String; = "file_size"

.field public static final LOCAL_FILE:Ljava/lang/String; = "local_file"

.field public static final MEDIA_ID:Ljava/lang/String; = "media_id"

.field public static final REMOTE_URI:Ljava/lang/String; = "remote_uri"

.field public static final STATUS:Ljava/lang/String; = "status"

.field public static final THUMBNAIL_URI:Ljava/lang/String; = "thumbnail_uri"
