.class public Lcom/sec/android/app/myfiles/element/ShortcutItem;
.super Ljava/lang/Object;
.source "ShortcutItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/element/ShortcutItem$1;
    }
.end annotation


# static fields
.field private static final MODULE:Ljava/lang/String;


# instance fields
.field private mCategoryType:I

.field private mIcon:Landroid/graphics/drawable/Drawable;

.field private mIndex:I

.field private mPath:Ljava/lang/String;

.field private mSelectable:Z

.field private mSelected:Z

.field private mTitle:Ljava/lang/String;

.field private mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    const-class v0, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->MODULE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;ZI)V
    .locals 6
    .param p1, "type"    # I
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;
    .param p4, "selectable"    # Z
    .param p5, "index"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mSelected:Z

    .line 60
    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mSelectable:Z

    .line 86
    iput p1, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mType:I

    .line 88
    iput-object p2, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mTitle:Ljava/lang/String;

    .line 92
    iput-object p3, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mPath:Ljava/lang/String;

    .line 94
    iput-boolean p4, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mSelectable:Z

    .line 96
    iput p5, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mIndex:I

    .line 100
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mPath:Ljava/lang/String;

    const-string v4, "Baidu/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 101
    const/16 v3, 0x1f

    iput v3, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mCategoryType:I

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mPath:Ljava/lang/String;

    const-string v4, "Dropbox/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-ne v3, v5, :cond_2

    .line 105
    const/16 v3, 0x8

    iput v3, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mCategoryType:I

    goto :goto_0

    .line 108
    :cond_2
    const/4 v2, 0x0

    .line 112
    .local v2, "success":Z
    :try_start_0
    new-instance v1, Lcom/sec/android/app/myfiles/ftp/FTPParams;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mPath:Ljava/lang/String;

    invoke-direct {v1, v3}, Lcom/sec/android/app/myfiles/ftp/FTPParams;-><init>(Ljava/lang/String;)V

    .line 114
    .local v1, "param":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    sget-object v3, Lcom/sec/android/app/myfiles/element/ShortcutItem$1;->$SwitchMap$com$sec$android$app$myfiles$ftp$FTPType:[I

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getType()Lcom/sec/android/app/myfiles/ftp/FTPType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/ftp/FTPType;->ordinal()I

    move-result v4

    aget v3, v3, v4
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    packed-switch v3, :pswitch_data_0

    .line 136
    :goto_1
    const/4 v2, 0x1

    .line 144
    .end local v1    # "param":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    :goto_2
    if-nez v2, :cond_0

    .line 146
    const/16 v3, 0x201

    iput v3, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mCategoryType:I

    goto :goto_0

    .line 118
    .restart local v1    # "param":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    :pswitch_0
    const/16 v3, 0xa

    :try_start_1
    iput v3, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mCategoryType:I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 138
    .end local v1    # "param":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    :catch_0
    move-exception v0

    .line 140
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v3, Lcom/sec/android/app/myfiles/element/ShortcutItem;->MODULE:Ljava/lang/String;

    const-string v4, "FTPParams string is not in the JSON format."

    invoke-static {v5, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 124
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v1    # "param":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    :pswitch_1
    const/16 v3, 0xb

    :try_start_2
    iput v3, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mCategoryType:I

    goto :goto_1

    .line 130
    :pswitch_2
    const/16 v3, 0xc

    iput v3, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mCategoryType:I
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 114
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 283
    if-nez p1, :cond_1

    .line 306
    :cond_0
    :goto_0
    return v1

    .line 288
    :cond_1
    instance-of v2, p1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    if-eqz v2, :cond_0

    .line 290
    iget v2, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mCategoryType:I

    const/16 v3, 0xa

    if-eq v2, v3, :cond_2

    iget v2, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mCategoryType:I

    const/16 v3, 0xb

    if-eq v2, v3, :cond_2

    iget v2, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mCategoryType:I

    const/16 v3, 0xc

    if-ne v2, v3, :cond_3

    :cond_2
    move-object v0, p1

    .line 294
    check-cast v0, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 296
    .local v0, "other":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    iget v2, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mCategoryType:I

    iget v3, v0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mCategoryType:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mTitle:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mTitle:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mPath:Ljava/lang/String;

    iget-object v3, v0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 301
    .end local v0    # "other":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :cond_3
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public declared-synchronized getCategoryType()I
    .locals 1

    .prologue
    .line 155
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mCategoryType:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public declared-synchronized getIconDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 193
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mIcon:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getIndex()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mIndex:I

    return v0
.end method

.method public declared-synchronized getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 215
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mPath:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 171
    iget v0, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mType:I

    return v0
.end method

.method public isSelectable()Z
    .locals 1

    .prologue
    .line 258
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mSelectable:Z

    return v0
.end method

.method public isSelected()Z
    .locals 1

    .prologue
    .line 237
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mSelected:Z

    return v0
.end method

.method public declared-synchronized setCategoryType(I)V
    .locals 1
    .param p1, "CategoryType"    # I

    .prologue
    .line 161
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mCategoryType:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 162
    monitor-exit p0

    return-void

    .line 161
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "mIcon"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 276
    iput-object p1, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mIcon:Landroid/graphics/drawable/Drawable;

    .line 277
    return-void
.end method

.method public declared-synchronized setIconDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "icon"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 204
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mIcon:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 205
    monitor-exit p0

    return-void

    .line 204
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setIndex(I)V
    .locals 0
    .param p1, "mIndex"    # I

    .prologue
    .line 73
    iput p1, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mIndex:I

    .line 74
    return-void
.end method

.method public declared-synchronized setPath(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 226
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mPath:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 227
    monitor-exit p0

    return-void

    .line 226
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setSelectable(Z)V
    .locals 0
    .param p1, "selectable"    # Z

    .prologue
    .line 268
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mSelectable:Z

    .line 269
    return-void
.end method

.method public setSelected(Z)V
    .locals 0
    .param p1, "selected"    # Z

    .prologue
    .line 248
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mSelected:Z

    .line 249
    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "mType"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/android/app/myfiles/element/ShortcutItem;->mType:I

    .line 35
    return-void
.end method
