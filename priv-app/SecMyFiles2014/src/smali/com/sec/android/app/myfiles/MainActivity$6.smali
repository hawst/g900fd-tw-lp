.class Lcom/sec/android/app/myfiles/MainActivity$6;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/MainActivity;->createSelectOptionClickListener()Landroid/widget/AdapterView$OnItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/MainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/MainActivity;)V
    .locals 0

    .prologue
    .line 2142
    iput-object p1, p0, Lcom/sec/android/app/myfiles/MainActivity$6;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2146
    .local p1, "list":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mSelectionLock:Ljava/lang/Object;
    invoke-static {}, Lcom/sec/android/app/myfiles/MainActivity;->access$500()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 2147
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$6;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mSelectOptionsIds:[I
    invoke-static {v0}, Lcom/sec/android/app/myfiles/MainActivity;->access$600(Lcom/sec/android/app/myfiles/MainActivity;)[I

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$6;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mSelectOptionsIds:[I
    invoke-static {v0}, Lcom/sec/android/app/myfiles/MainActivity;->access$600(Lcom/sec/android/app/myfiles/MainActivity;)[I

    move-result-object v0

    array-length v0, v0

    add-int/lit8 v2, p3, 0x1

    if-lt v0, v2, :cond_0

    .line 2148
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$6;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mSelectOptionsIds:[I
    invoke-static {v0}, Lcom/sec/android/app/myfiles/MainActivity;->access$600(Lcom/sec/android/app/myfiles/MainActivity;)[I

    move-result-object v0

    aget v0, v0, p3

    if-nez v0, :cond_1

    .line 2149
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$6;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p2}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 2154
    :cond_0
    :goto_0
    monitor-exit v1

    .line 2155
    return-void

    .line 2151
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$6;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mUnselectAllClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p2}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0

    .line 2154
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
