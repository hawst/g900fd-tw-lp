.class public Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;
.super Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
.source "ImageHoverPopup.java"


# static fields
.field private static final GOLF_TEMP_PATH:Ljava/lang/String;

.field private static final MODULE:Ljava/lang/String; = "ImageHoverPopup"


# instance fields
.field private final baseZoomInValue:I

.field private layout:Landroid/widget/RelativeLayout;

.field private mHoverImg:Landroid/graphics/Bitmap;

.field private mHoverOperationContainer:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/.thumbnails/golf/tmp"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->GOLF_TEMP_PATH:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 72
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;-><init>(Landroid/content/Context;)V

    .line 62
    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mHoverImg:Landroid/graphics/Bitmap;

    .line 64
    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->layout:Landroid/widget/RelativeLayout;

    .line 66
    const/16 v0, 0x190

    iput v0, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->baseZoomInValue:I

    .line 73
    return-void
.end method

.method private computeSampleSize(Landroid/graphics/BitmapFactory$Options;I)I
    .locals 7
    .param p1, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p2, "target"    # I

    .prologue
    const/4 v5, 0x1

    .line 93
    iget v4, p1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 94
    .local v4, "w":I
    iget v3, p1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 95
    .local v3, "h":I
    div-int v2, v4, p2

    .line 96
    .local v2, "candidateW":I
    div-int v1, v3, p2

    .line 97
    .local v1, "candidateH":I
    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 99
    .local v0, "candidate":I
    if-nez v0, :cond_0

    .line 116
    :goto_0
    return v5

    .line 103
    :cond_0
    if-le v0, v5, :cond_1

    .line 104
    if-le v4, p2, :cond_1

    div-int v6, v4, v0

    if-ge v6, p2, :cond_1

    .line 105
    add-int/lit8 v0, v0, -0x1

    .line 108
    :cond_1
    if-le v0, v5, :cond_2

    .line 110
    if-le v3, p2, :cond_2

    div-int v5, v3, v0

    if-ge v5, p2, :cond_2

    .line 112
    add-int/lit8 v0, v0, -0x1

    :cond_2
    move v5, v0

    .line 116
    goto :goto_0
.end method

.method private getExifOrientation(Ljava/lang/String;)I
    .locals 9
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    const/4 v8, -0x1

    const/4 v5, 0x0

    .line 184
    if-nez p1, :cond_1

    .line 185
    const-string v6, "ImageHoverPopup"

    const-string v7, "exif is null"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    move v0, v5

    .line 229
    :cond_0
    :goto_0
    return v0

    .line 190
    :cond_1
    const/4 v0, 0x0

    .line 192
    .local v0, "degree":I
    const/4 v2, 0x0

    .line 196
    .local v2, "exif":Landroid/media/ExifInterface;
    :try_start_0
    new-instance v3, Landroid/media/ExifInterface;

    invoke-direct {v3, p1}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "exif":Landroid/media/ExifInterface;
    .local v3, "exif":Landroid/media/ExifInterface;
    move-object v2, v3

    .line 203
    .end local v3    # "exif":Landroid/media/ExifInterface;
    .restart local v2    # "exif":Landroid/media/ExifInterface;
    :goto_1
    if-eqz v2, :cond_0

    .line 205
    const-string v5, "Orientation"

    invoke-virtual {v2, v5, v8}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v4

    .line 207
    .local v4, "orientation":I
    if-eq v4, v8, :cond_0

    .line 209
    packed-switch v4, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 216
    :pswitch_1
    const/16 v0, 0xb4

    .line 217
    goto :goto_0

    .line 198
    .end local v4    # "orientation":I
    :catch_0
    move-exception v1

    .line 200
    .local v1, "ex":Ljava/io/IOException;
    const-string v6, "ImageHoverPopup"

    const-string v7, "cannot read exif"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 212
    .end local v1    # "ex":Ljava/io/IOException;
    .restart local v4    # "orientation":I
    :pswitch_2
    const/16 v0, 0x5a

    .line 213
    goto :goto_0

    .line 220
    :pswitch_3
    const/16 v0, 0x10e

    .line 221
    goto :goto_0

    .line 209
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private getSampleSizeBitmap(ILjava/lang/String;)Landroid/graphics/Bitmap;
    .locals 12
    .param p1, "targetWidthHeight"    # I
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x1

    const/4 v9, 0x0

    const v10, 0x3f19999a    # 0.6f

    .line 122
    new-instance v6, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v6}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 125
    .local v6, "options":Landroid/graphics/BitmapFactory$Options;
    iput-boolean v11, v6, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 127
    const-string v8, ".golf"

    invoke-virtual {p2, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 129
    sget-object v8, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->GOLF_TEMP_PATH:Ljava/lang/String;

    invoke-static {v8, p2, v9}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->getJpgTempFilePath(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p2

    .line 132
    :cond_0
    invoke-static {p2, v6}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 133
    .local v0, "bm":Landroid/graphics/Bitmap;
    iput-boolean v9, v6, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 135
    const/4 v8, -0x1

    if-eq p1, v8, :cond_1

    .line 137
    invoke-direct {p0, v6, p1}, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->computeSampleSize(Landroid/graphics/BitmapFactory$Options;I)I

    move-result v8

    iput v8, v6, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 140
    :cond_1
    invoke-static {p2, v6}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 143
    iget v8, v6, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-ge v8, p1, :cond_2

    iget v8, v6, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-ge v8, p1, :cond_2

    .line 146
    iget-object v8, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v9, "window"

    invoke-virtual {v8, v9}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/WindowManager;

    .line 148
    .local v7, "wm":Landroid/view/WindowManager;
    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    .line 150
    .local v2, "display":Landroid/view/Display;
    invoke-virtual {v2}, Landroid/view/Display;->getWidth()I

    move-result v8

    int-to-float v8, v8

    iget v9, v6, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v9, v9

    div-float v5, v8, v9

    .line 151
    .local v5, "limitWidth":F
    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v8

    int-to-float v8, v8

    iget v9, v6, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-float v9, v9

    div-float v3, v8, v9

    .line 153
    .local v3, "limitHeight":F
    const/4 v4, 0x0

    .line 160
    .local v4, "limitWeight":F
    cmpl-float v8, v5, v3

    if-lez v8, :cond_4

    .line 161
    mul-float v4, v3, v10

    .line 165
    :goto_0
    if-eqz v0, :cond_2

    .line 166
    iget v8, v6, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-float v8, v8

    mul-float/2addr v8, v4

    float-to-int v8, v8

    iget v9, v6, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-float v9, v9

    mul-float/2addr v9, v4

    float-to-int v9, v9

    invoke-static {v0, v8, v9, v11}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 169
    .end local v2    # "display":Landroid/view/Display;
    .end local v3    # "limitHeight":F
    .end local v4    # "limitWeight":F
    .end local v5    # "limitWidth":F
    .end local v7    # "wm":Landroid/view/WindowManager;
    :cond_2
    const/4 v1, 0x0

    .line 171
    .local v1, "degree":I
    invoke-direct {p0, p2}, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->getExifOrientation(Ljava/lang/String;)I

    move-result v1

    .line 173
    if-eqz v1, :cond_3

    .line 175
    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 178
    :cond_3
    return-object v0

    .line 163
    .end local v1    # "degree":I
    .restart local v2    # "display":Landroid/view/Display;
    .restart local v3    # "limitHeight":F
    .restart local v4    # "limitWeight":F
    .restart local v5    # "limitWidth":F
    .restart local v7    # "wm":Landroid/view/WindowManager;
    :cond_4
    mul-float v4, v5, v10

    goto :goto_0
.end method

.method private loadFullBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 77
    const/4 v0, 0x0

    .line 79
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz p1, :cond_0

    .line 81
    const/16 v1, 0x2bc

    invoke-direct {p0, v1, p1}, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->getSampleSizeBitmap(ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 88
    :goto_0
    return-object v0

    .line 85
    :cond_0
    const/4 v1, 0x0

    const-string v2, "ImageHoverPopup"

    const-string v3, "null"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "b"    # Landroid/graphics/Bitmap;
    .param p2, "degrees"    # I

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/4 v9, 0x0

    .line 235
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    .line 237
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 239
    .local v5, "m":Landroid/graphics/Matrix;
    int-to-float v0, p2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-virtual {v5, v0, v1, v2}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 243
    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 245
    .local v7, "b2":Landroid/graphics/Bitmap;
    invoke-virtual {p1, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 247
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 248
    move-object p1, v7

    .line 257
    .end local v5    # "m":Landroid/graphics/Matrix;
    .end local v7    # "b2":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-object p1

    .line 251
    .restart local v5    # "m":Landroid/graphics/Matrix;
    :catch_0
    move-exception v8

    .line 253
    .local v8, "ex":Ljava/lang/OutOfMemoryError;
    const-string v0, "ImageHoverPopup"

    const-string v1, "Out of memory"

    invoke-static {v9, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private scaledBitmapForSmallImage(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/16 v2, 0x190

    .line 262
    if-eqz p1, :cond_0

    .line 264
    const/4 v1, 0x1

    invoke-static {p1, v2, v2, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 269
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected changeList()V
    .locals 0

    .prologue
    .line 521
    return-void
.end method

.method protected changeListThreadStart()V
    .locals 0

    .prologue
    .line 527
    return-void
.end method

.method protected dissmissPopup()V
    .locals 4

    .prologue
    .line 487
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->isShow()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 490
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 491
    const/4 v1, 0x0

    const-string v2, "ImageHoverPopup"

    const-string v3, "ImageHoverDlg.dismiss(); "

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 499
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    .line 501
    :cond_0
    return-void

    .line 493
    :catch_0
    move-exception v0

    .line 495
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method protected initViews()Z
    .locals 10

    .prologue
    const/4 v7, 0x0

    const/4 v9, 0x0

    .line 275
    const/4 v4, 0x0

    .line 277
    .local v4, "noThumbnail":Landroid/graphics/Bitmap;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mFilePath:Ljava/lang/String;

    if-nez v6, :cond_0

    iget-boolean v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->bFromNearByDevice:Z

    if-nez v6, :cond_0

    iget-boolean v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->bFromDropBox:Z

    if-nez v6, :cond_0

    iget v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mCategoryType:I

    const/16 v8, 0xa

    if-eq v6, v8, :cond_0

    move v6, v7

    .line 477
    :goto_0
    return v6

    .line 280
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mFilePath:Ljava/lang/String;

    if-eqz v6, :cond_1

    iget-boolean v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->bFromDropBox:Z

    if-nez v6, :cond_1

    .line 282
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mFilePath:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->loadFullBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mHoverImg:Landroid/graphics/Bitmap;

    .line 315
    :goto_1
    new-instance v6, Landroid/app/Dialog;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-direct {v6, v8}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    .line 316
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v8, 0x7f04002d

    invoke-static {v6, v8, v9}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout;

    iput-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->layout:Landroid/widget/RelativeLayout;

    .line 318
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->layout:Landroid/widget/RelativeLayout;

    const v8, 0x7f0f00bf

    invoke-virtual {v6, v8}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mHoverOperationContainer:Landroid/view/View;

    .line 322
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mHoverOperationContainer:Landroid/view/View;

    const/16 v8, 0x8

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    .line 325
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->layout:Landroid/widget/RelativeLayout;

    const v8, 0x7f0f00be

    invoke-virtual {v6, v8}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 327
    .local v3, "iv":Landroid/widget/ImageView;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mHoverImg:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_6

    .line 328
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mHoverImg:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 338
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->setDialogProperties()V

    .line 339
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->setDialogPosition()V

    .line 341
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    iget-object v7, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v7}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 343
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->layout:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v6}, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->initHoverOperationView(Landroid/view/View;)V

    .line 345
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->layout:Landroid/widget/RelativeLayout;

    new-instance v7, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup$1;

    invoke-direct {v7, p0}, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup$1;-><init>(Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;)V

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 393
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->layout:Landroid/widget/RelativeLayout;

    new-instance v7, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup$2;

    invoke-direct {v7, p0}, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup$2;-><init>(Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;)V

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 477
    const/4 v6, 0x1

    goto :goto_0

    .line 284
    .end local v3    # "iv":Landroid/widget/ImageView;
    :cond_1
    iget-boolean v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->bFromNearByDevice:Z

    if-eqz v6, :cond_3

    .line 286
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mRemoteDeviceImage:Landroid/graphics/drawable/Drawable;

    if-eqz v6, :cond_2

    .line 288
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mRemoteDeviceImage:Landroid/graphics/drawable/Drawable;

    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    .line 289
    .local v2, "drawable":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 291
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    invoke-direct {p0, v1}, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->scaledBitmapForSmallImage(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mHoverImg:Landroid/graphics/Bitmap;

    goto :goto_1

    .line 293
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    :cond_2
    iput-object v9, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mHoverImg:Landroid/graphics/Bitmap;

    goto :goto_1

    .line 295
    :cond_3
    iget-boolean v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->bFromDropBox:Z

    if-eqz v6, :cond_5

    .line 297
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mDropBoxThumb:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_4

    .line 299
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mDropBoxThumb:Landroid/graphics/Bitmap;

    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->scaledBitmapForSmallImage(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mHoverImg:Landroid/graphics/Bitmap;

    goto/16 :goto_1

    .line 303
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mFilePath:Ljava/lang/String;

    invoke-static {v6}, Lcom/sec/android/app/myfiles/utils/Utils;->getFileThumbnailFolderName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 304
    .local v5, "serverFolderName":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "/"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "large.jpg"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 306
    .local v0, "ThumbnailFilePath":Ljava/lang/String;
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mHoverImg:Landroid/graphics/Bitmap;

    .line 307
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mHoverImg:Landroid/graphics/Bitmap;

    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->scaledBitmapForSmallImage(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mHoverImg:Landroid/graphics/Bitmap;

    goto/16 :goto_1

    .line 312
    .end local v0    # "ThumbnailFilePath":Ljava/lang/String;
    .end local v5    # "serverFolderName":Ljava/lang/String;
    :cond_5
    iput-object v9, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mHoverImg:Landroid/graphics/Bitmap;

    goto/16 :goto_1

    .line 331
    .restart local v3    # "iv":Landroid/widget/ImageView;
    :cond_6
    const-string v6, "ImageHoverPopup"

    const-string v8, "mHoverImg is empty ~ !!! "

    invoke-static {v7, v6, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 333
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0200b4

    invoke-static {v6, v7}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->scaledBitmapForSmallImage(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 335
    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_2
.end method

.method protected setDialogPosition()V
    .locals 4

    .prologue
    .line 505
    const/4 v1, 0x0

    const-string v2, "ImageHoverPopup"

    const-string v3, "setPosition"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    .line 507
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 510
    .local v0, "params":Landroid/view/WindowManager$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mHoverRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v1, v1, -0x32

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 511
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mHoverRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    add-int/lit8 v1, v1, -0x64

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 512
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 513
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x33

    invoke-virtual {v1, v2}, Landroid/view/Window;->setGravity(I)V

    .line 515
    return-void
.end method

.method protected showContent()V
    .locals 0

    .prologue
    .line 483
    return-void
.end method
