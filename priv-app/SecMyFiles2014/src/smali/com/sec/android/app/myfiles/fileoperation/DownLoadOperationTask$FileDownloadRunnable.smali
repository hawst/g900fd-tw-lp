.class public Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$FileDownloadRunnable;
.super Ljava/lang/Object;
.source "DownLoadOperationTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FileDownloadRunnable"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$FileDownloadRunnable;->this$0:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 88
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$FileDownloadRunnable;->this$0:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFiles:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->access$000(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;)Ljava/util/ArrayList;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 89
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$FileDownloadRunnable;->this$0:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFiles:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->access$000(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;

    .line 90
    .local v2, "file":Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$FileDownloadRunnable;->this$0:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->isCancelled()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 108
    .end local v2    # "file":Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_1
    monitor-enter p0

    .line 112
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 118
    :goto_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 119
    return-void

    .line 92
    .restart local v2    # "file":Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_2
    const/4 v4, 0x0

    .line 93
    .local v4, "started":Z
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$FileDownloadRunnable;->this$0:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFiles:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->access$000(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;)Ljava/util/ArrayList;

    move-result-object v6

    monitor-enter v6

    .line 94
    :try_start_2
    iget-boolean v4, v2, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;->mStarted:Z

    .line 95
    const/4 v5, 0x1

    iput-boolean v5, v2, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;->mStarted:Z

    .line 96
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 97
    if-nez v4, :cond_0

    .line 99
    :try_start_3
    iget-object v5, v2, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;->mDownload:Lcom/samsung/scloud/DropboxAPI$CancelableDownload;

    invoke-virtual {v5}, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->getFileCancelable()V
    :try_end_3
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 100
    :catch_0
    move-exception v0

    .line 101
    .local v0, "e":Lcom/samsung/scloud/exception/SCloudException;
    invoke-virtual {v0}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V

    goto :goto_0

    .line 96
    .end local v0    # "e":Lcom/samsung/scloud/exception/SCloudException;
    :catchall_0
    move-exception v5

    :try_start_4
    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v5

    .line 102
    :catch_1
    move-exception v0

    .line 103
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 114
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "file":Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "started":Z
    :catch_2
    move-exception v1

    .line 116
    .local v1, "ex":Ljava/lang/Exception;
    :try_start_5
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 118
    .end local v1    # "ex":Ljava/lang/Exception;
    :catchall_1
    move-exception v5

    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v5
.end method
