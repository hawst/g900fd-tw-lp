.class public Lcom/sec/android/app/myfiles/ftp/FTPItem;
.super Ljava/lang/Object;
.source "FTPItem.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/ftp/IFTPItem;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/ftp/FTPItem$Tag;
    }
.end annotation


# static fields
.field public static final MODULE:Ljava/lang/String;


# instance fields
.field private mCurrentId:J

.field private final mFile:Lorg/apache/commons/net/ftp/FTPFile;

.field private final mPath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 241
    const-class v0, Lcom/sec/android/app/myfiles/ftp/FTPItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->MODULE:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>()V
    .locals 2

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 239
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mCurrentId:J

    .line 109
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mFile:Lorg/apache/commons/net/ftp/FTPFile;

    .line 110
    sget-object v0, Ljava/io/File;->separator:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mPath:Ljava/lang/String;

    .line 111
    return-void
.end method

.method private constructor <init>(Lorg/apache/commons/net/ftp/FTPFile;Ljava/lang/String;)V
    .locals 2
    .param p1, "file"    # Lorg/apache/commons/net/ftp/FTPFile;
    .param p2, "parentDir"    # Ljava/lang/String;

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 239
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mCurrentId:J

    .line 101
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    .line 102
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 104
    :cond_1
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mFile:Lorg/apache/commons/net/ftp/FTPFile;

    .line 105
    const-string v0, "/+"

    const-string v1, "/"

    invoke-virtual {p2, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mPath:Ljava/lang/String;

    .line 106
    return-void
.end method

.method private static getExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 205
    if-nez p0, :cond_1

    .line 213
    :cond_0
    :goto_0
    return-object v1

    .line 209
    :cond_1
    const/16 v2, 0x2e

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 210
    .local v0, "lastDot":I
    if-ltz v0, :cond_0

    .line 213
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getItem(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPItem;
    .locals 8
    .param p0, "itemStr"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 60
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 64
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 66
    .local v2, "json":Lorg/json/JSONObject;
    new-instance v1, Lorg/apache/commons/net/ftp/FTPFile;

    invoke-direct {v1}, Lorg/apache/commons/net/ftp/FTPFile;-><init>()V

    .line 68
    .local v1, "file":Lorg/apache/commons/net/ftp/FTPFile;
    const-string v3, "name"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/apache/commons/net/ftp/FTPFile;->setName(Ljava/lang/String;)V

    .line 70
    const-string v3, "type"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v3}, Lorg/apache/commons/net/ftp/FTPFile;->setType(I)V

    .line 72
    const-string v3, "size"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v1, v6, v7}, Lorg/apache/commons/net/ftp/FTPFile;->setSize(J)V

    .line 74
    new-instance v3, Lcom/sec/android/app/myfiles/ftp/FTPItem;

    const-string v5, "dir"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v1, v5}, Lcom/sec/android/app/myfiles/ftp/FTPItem;-><init>(Lorg/apache/commons/net/ftp/FTPFile;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    .end local v1    # "file":Lorg/apache/commons/net/ftp/FTPFile;
    .end local v2    # "json":Lorg/json/JSONObject;
    :goto_0
    return-object v3

    .line 76
    :catch_0
    move-exception v0

    .line 78
    .local v0, "e":Lorg/json/JSONException;
    const/4 v3, 0x1

    sget-object v5, Lcom/sec/android/app/myfiles/ftp/FTPItem;->MODULE:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Not a correct FTPItem format: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    move-object v3, v4

    .line 80
    goto :goto_0

    .end local v0    # "e":Lorg/json/JSONException;
    :cond_0
    move-object v3, v4

    .line 86
    goto :goto_0
.end method

.method public static getItem(Ljava/lang/String;ILjava/lang/String;J)Lcom/sec/android/app/myfiles/ftp/FTPItem;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "type"    # I
    .param p2, "directory"    # Ljava/lang/String;
    .param p3, "size"    # J

    .prologue
    .line 38
    if-eqz p0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 40
    new-instance v0, Lorg/apache/commons/net/ftp/FTPFile;

    invoke-direct {v0}, Lorg/apache/commons/net/ftp/FTPFile;-><init>()V

    .line 42
    .local v0, "file":Lorg/apache/commons/net/ftp/FTPFile;
    invoke-virtual {v0, p0}, Lorg/apache/commons/net/ftp/FTPFile;->setName(Ljava/lang/String;)V

    .line 44
    invoke-virtual {v0, p1}, Lorg/apache/commons/net/ftp/FTPFile;->setType(I)V

    .line 46
    invoke-virtual {v0, p3, p4}, Lorg/apache/commons/net/ftp/FTPFile;->setSize(J)V

    .line 48
    new-instance v1, Lcom/sec/android/app/myfiles/ftp/FTPItem;

    invoke-direct {v1, v0, p2}, Lcom/sec/android/app/myfiles/ftp/FTPItem;-><init>(Lorg/apache/commons/net/ftp/FTPFile;Ljava/lang/String;)V

    .line 52
    .end local v0    # "file":Lorg/apache/commons/net/ftp/FTPFile;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getItem(Lorg/apache/commons/net/ftp/FTPFile;Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPItem;
    .locals 1
    .param p0, "file"    # Lorg/apache/commons/net/ftp/FTPFile;
    .param p1, "parentDir"    # Ljava/lang/String;

    .prologue
    .line 93
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPItem;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/myfiles/ftp/FTPItem;-><init>(Lorg/apache/commons/net/ftp/FTPFile;Ljava/lang/String;)V

    return-object v0
.end method

.method public static getRoot()Lcom/sec/android/app/myfiles/ftp/FTPItem;
    .locals 2

    .prologue
    .line 97
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPRootItem;

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/ftp/FTPRootItem;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public getDate()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mFile:Lorg/apache/commons/net/ftp/FTPFile;

    invoke-virtual {v0}, Lorg/apache/commons/net/ftp/FTPFile;->getTimestamp()Ljava/util/Calendar;

    move-result-object v0

    return-object v0
.end method

.method public getDir()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method public getExtension()Ljava/lang/String;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mFile:Lorg/apache/commons/net/ftp/FTPFile;

    invoke-virtual {v0}, Lorg/apache/commons/net/ftp/FTPFile;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFullPath()Ljava/lang/String;
    .locals 3

    .prologue
    .line 140
    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 141
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mFile:Lorg/apache/commons/net/ftp/FTPFile;

    invoke-virtual {v1}, Lorg/apache/commons/net/ftp/FTPFile;->getName()Ljava/lang/String;

    move-result-object v0

    .line 142
    .local v0, "name":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 143
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mFile:Lorg/apache/commons/net/ftp/FTPFile;

    invoke-virtual {v2}, Lorg/apache/commons/net/ftp/FTPFile;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 147
    .end local v0    # "name":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 145
    .restart local v0    # "name":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mPath:Ljava/lang/String;

    goto :goto_0

    .line 147
    .end local v0    # "name":Ljava/lang/String;
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mFile:Lorg/apache/commons/net/ftp/FTPFile;

    invoke-virtual {v2}, Lorg/apache/commons/net/ftp/FTPFile;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getID()J
    .locals 2

    .prologue
    .line 120
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mCurrentId:J

    return-wide v0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 197
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    const-wide/16 v0, 0x0

    .line 200
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mFile:Lorg/apache/commons/net/ftp/FTPFile;

    invoke-virtual {v0}, Lorg/apache/commons/net/ftp/FTPFile;->getSize()J

    move-result-wide v0

    goto :goto_0
.end method

.method public getThumbnail()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 168
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mFile:Lorg/apache/commons/net/ftp/FTPFile;

    invoke-virtual {v0}, Lorg/apache/commons/net/ftp/FTPFile;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mFile:Lorg/apache/commons/net/ftp/FTPFile;

    invoke-virtual {v0}, Lorg/apache/commons/net/ftp/FTPFile;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public hasId()Z
    .locals 4

    .prologue
    .line 130
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mCurrentId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDirectory()Z
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mFile:Lorg/apache/commons/net/ftp/FTPFile;

    invoke-virtual {v0}, Lorg/apache/commons/net/ftp/FTPFile;->isDirectory()Z

    move-result v0

    return v0
.end method

.method public isFile()Z
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mFile:Lorg/apache/commons/net/ftp/FTPFile;

    invoke-virtual {v0}, Lorg/apache/commons/net/ftp/FTPFile;->isFile()Z

    move-result v0

    return v0
.end method

.method public isRoot()Z
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    return v0
.end method

.method public isSymbolicLink()Z
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mFile:Lorg/apache/commons/net/ftp/FTPFile;

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mFile:Lorg/apache/commons/net/ftp/FTPFile;

    invoke-virtual {v0}, Lorg/apache/commons/net/ftp/FTPFile;->isSymbolicLink()Z

    move-result v0

    .line 186
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setId(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 125
    iput-wide p1, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mCurrentId:J

    .line 126
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 219
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 221
    .local v1, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "name"

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mFile:Lorg/apache/commons/net/ftp/FTPFile;

    invoke-virtual {v3}, Lorg/apache/commons/net/ftp/FTPFile;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    const-string v2, "type"

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mFile:Lorg/apache/commons/net/ftp/FTPFile;

    invoke-virtual {v3}, Lorg/apache/commons/net/ftp/FTPFile;->getType()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    const-string v2, "dir"

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mPath:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    const-string v2, "size"

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPItem;->mFile:Lorg/apache/commons/net/ftp/FTPFile;

    invoke-virtual {v3}, Lorg/apache/commons/net/ftp/FTPFile;->getSize()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    .line 231
    .local v0, "json":Lorg/json/JSONObject;
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
