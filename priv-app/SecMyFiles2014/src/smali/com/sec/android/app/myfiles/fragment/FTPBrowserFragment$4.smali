.class Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$4;
.super Ljava/lang/Object;
.source "FTPBrowserFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)V
    .locals 0

    .prologue
    .line 359
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 363
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->getCurrentItemIndex()I

    move-result v0

    .line 365
    .local v0, "index":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->getFirstVisiblePosition()I

    move-result v1

    if-lt v0, v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->getLastVisiblePosition()I

    move-result v1

    if-le v0, v1, :cond_1

    .line 367
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->getCurrentItemIndex()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->setTreeViewYPos(I)V

    .line 369
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->restoreTreeYPosition()V

    .line 371
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v1, :cond_1

    .line 372
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->notifyUpdate(Z)V

    .line 373
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrefix:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 376
    :cond_1
    return-void
.end method
