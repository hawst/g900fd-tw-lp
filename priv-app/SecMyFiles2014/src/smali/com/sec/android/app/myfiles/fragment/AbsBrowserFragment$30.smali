.class Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$30;
.super Landroid/content/BroadcastReceiver;
.source "AbsBrowserFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V
    .locals 0

    .prologue
    .line 6169
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$30;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 6174
    const-string v0, "AbsBrowserFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "receive the broadcast message : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 6177
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$30;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$30;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFileOperationDoneReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->access$600(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Landroid/content/BroadcastReceiver;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 6179
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$30;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onDropFinished(Landroid/content/Intent;)V

    .line 6181
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$30;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6183
    if-eqz p2, :cond_0

    .line 6185
    const-string v0, "success"

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6187
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$30;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->finishSelectMode()V

    .line 6191
    :cond_0
    return-void
.end method
