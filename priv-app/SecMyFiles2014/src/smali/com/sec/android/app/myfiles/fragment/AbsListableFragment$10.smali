.class Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$10;
.super Ljava/lang/Object;
.source "AbsListableFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;)V
    .locals 0

    .prologue
    .line 633
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v2, 0x0

    .line 638
    packed-switch p2, :pswitch_data_0

    .line 654
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->removeDialog(I)V

    .line 655
    return-void

    .line 640
    :pswitch_0
    sput v2, Lcom/sec/android/app/myfiles/utils/Utils;->mAddShortcutSuccess:I

    .line 641
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mFragmentId:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 642
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->handleAddShortcutRequest()V

    goto :goto_0

    .line 644
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->addCurrentFolderToShortcut()V

    goto :goto_0

    .line 649
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$10;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->addShortcutToHome()V

    goto :goto_0

    .line 638
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
