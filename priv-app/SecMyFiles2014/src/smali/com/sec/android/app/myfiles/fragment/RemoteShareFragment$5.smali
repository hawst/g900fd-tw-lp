.class Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$5;
.super Ljava/lang/Object;
.source "RemoteShareFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->onCreateItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)V
    .locals 0

    .prologue
    .line 716
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 720
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v0, :cond_0

    .line 721
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mIsSearchMode:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 731
    :goto_0
    return v0

    .line 725
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->isSelectMode()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 726
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;

    if-eqz v0, :cond_1

    .line 727
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    invoke-virtual {v0, v2, p3, v1}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->startSelectMode(III)V

    move v0, v2

    .line 728
    goto :goto_0

    :cond_1
    move v0, v1

    .line 731
    goto :goto_0
.end method
