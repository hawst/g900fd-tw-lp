.class public Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$FileUploadRunnable;
.super Ljava/lang/Object;
.source "BaiduDownLoadOperationTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FileUploadRunnable"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$FileUploadRunnable;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 120
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$FileUploadRunnable;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->access$100(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;)Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 121
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$FileUploadRunnable;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->access$100(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;

    .line 122
    .local v1, "file":Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$FileUploadRunnable;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->isCancelled()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 123
    const-string v4, "BaiduDownLoadOperationTask"

    const-string v5, "FileUploadRunnable is cancelled"

    invoke-static {v6, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 137
    .end local v1    # "file":Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    monitor-enter p0

    .line 141
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 147
    :goto_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 148
    return-void

    .line 126
    .restart local v1    # "file":Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_2
    const/4 v3, 0x0

    .line 127
    .local v3, "started":Z
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$FileUploadRunnable;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->access$100(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;)Ljava/util/ArrayList;

    move-result-object v5

    monitor-enter v5

    .line 128
    :try_start_2
    iget-boolean v3, v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;->mStarted:Z

    .line 129
    const/4 v4, 0x1

    iput-boolean v4, v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;->mStarted:Z

    .line 130
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 131
    if-nez v3, :cond_0

    .line 132
    const-string v4, "BaiduDownLoadOperationTask"

    const-string v5, "FileUploadRunnable not started"

    invoke-static {v6, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 130
    :catchall_0
    move-exception v4

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v4

    .line 143
    .end local v1    # "file":Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "started":Z
    :catch_0
    move-exception v0

    .line 145
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 147
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_1
    move-exception v4

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v4
.end method
