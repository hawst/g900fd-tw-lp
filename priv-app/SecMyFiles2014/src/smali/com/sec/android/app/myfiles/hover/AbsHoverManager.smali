.class public abstract Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
.super Ljava/lang/Object;
.source "AbsHoverManager.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/hover/AbsHoverManager$HoverPopupHandler;,
        Lcom/sec/android/app/myfiles/hover/AbsHoverManager$HoverMsg;
    }
.end annotation


# static fields
.field private static final MODULE:Ljava/lang/String; = "AbsHoverManager"


# instance fields
.field protected final LEFT_MARGIN:I

.field protected final TOP_MARGIN:I

.field protected bFromDropBox:Z

.field protected bFromNearByDevice:Z

.field protected mAirBtnMenu:Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;

.field public mAirButtonMenuSelectItemListener:Lcom/samsung/android/airbutton/AirButtonImpl$OnItemSelectedListener;

.field protected mAppEntry:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

.field protected mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field protected mAudioManager:Landroid/media/AudioManager;

.field protected mCategoryType:I

.field protected mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

.field protected mDismissFolderHover:Z

.field protected mDropBoxThumb:Landroid/graphics/Bitmap;

.field private mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

.field protected mFile:Ljava/io/File;

.field protected mFileMimeType:Ljava/lang/String;

.field protected mFilePath:Ljava/lang/String;

.field protected mFileUri:Ljava/lang/String;

.field protected mHandler:Landroid/os/Handler;

.field protected mHasAudioFocus:Z

.field protected mHoverCursor:Landroid/database/Cursor;

.field protected mHoverDialog:Landroid/app/Dialog;

.field protected mHoverFolderNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

.field protected mHoverItemPosition:I

.field protected mHoverOverButton:Z

.field protected mHoverOverLayout:Z

.field protected mHoverPopupDeleteBtn:Lcom/sec/android/app/myfiles/hover/HoverImageButton;

.field protected mHoverPopupShareViaBtn:Lcom/sec/android/app/myfiles/hover/HoverImageButton;

.field protected mHoverRect:Landroid/graphics/Rect;

.field protected mRemoteDeviceImage:Landroid/graphics/drawable/Drawable;

.field protected mResolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverDialog:Landroid/app/Dialog;

    .line 64
    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mAudioManager:Landroid/media/AudioManager;

    .line 65
    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    .line 66
    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHandler:Landroid/os/Handler;

    .line 67
    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mResolver:Landroid/content/ContentResolver;

    .line 69
    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mFile:Ljava/io/File;

    .line 70
    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mFilePath:Ljava/lang/String;

    .line 78
    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverCursor:Landroid/database/Cursor;

    .line 80
    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverFolderNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .line 82
    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mAppEntry:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .line 84
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHasAudioFocus:Z

    .line 86
    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mAirBtnMenu:Lcom/sec/android/app/myfiles/myfilesAirButton/MyFilesAirButtonImpl;

    .line 88
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mDismissFolderHover:Z

    .line 90
    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mRemoteDeviceImage:Landroid/graphics/drawable/Drawable;

    .line 92
    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mDropBoxThumb:Landroid/graphics/Bitmap;

    .line 94
    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mFileUri:Ljava/lang/String;

    .line 96
    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mFileMimeType:Ljava/lang/String;

    .line 98
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->bFromNearByDevice:Z

    .line 100
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->bFromDropBox:Z

    .line 102
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverOverButton:Z

    .line 104
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverOverLayout:Z

    .line 106
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->LEFT_MARGIN:I

    .line 107
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->TOP_MARGIN:I

    .line 383
    new-instance v0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager$1;-><init>(Lcom/sec/android/app/myfiles/hover/AbsHoverManager;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    move-object v0, p1

    .line 136
    check-cast v0, Lcom/sec/android/app/myfiles/AbsMainActivity;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mResolver:Landroid/content/ContentResolver;

    .line 138
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mAudioManager:Landroid/media/AudioManager;

    .line 176
    return-void
.end method


# virtual methods
.method protected abandonAudioFocus()V
    .locals 2

    .prologue
    .line 425
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHasAudioFocus:Z

    if-eqz v0, :cond_0

    .line 427
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHasAudioFocus:Z

    .line 428
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 430
    :cond_0
    return-void
.end method

.method protected abstract changeList()V
.end method

.method protected abstract changeListThreadStart()V
.end method

.method protected abstract dissmissPopup()V
.end method

.method protected getHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 344
    new-instance v0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager$HoverPopupHandler;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager$HoverPopupHandler;-><init>(Lcom/sec/android/app/myfiles/hover/AbsHoverManager;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHandler:Landroid/os/Handler;

    .line 347
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method protected initHoverOperationView(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v3, 0x8

    .line 522
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mFilePath:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 523
    const v1, 0x7f0f00bc

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/hover/HoverImageButton;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverPopupDeleteBtn:Lcom/sec/android/app/myfiles/hover/HoverImageButton;

    .line 524
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverPopupDeleteBtn:Lcom/sec/android/app/myfiles/hover/HoverImageButton;

    if-eqz v1, :cond_0

    .line 525
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverPopupDeleteBtn:Lcom/sec/android/app/myfiles/hover/HoverImageButton;

    invoke-virtual {v1, p0}, Lcom/sec/android/app/myfiles/hover/HoverImageButton;->setmHoverManager(Lcom/sec/android/app/myfiles/hover/AbsHoverManager;)V

    .line 526
    :cond_0
    const v1, 0x7f0f00bb

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/hover/HoverImageButton;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverPopupShareViaBtn:Lcom/sec/android/app/myfiles/hover/HoverImageButton;

    .line 527
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverPopupShareViaBtn:Lcom/sec/android/app/myfiles/hover/HoverImageButton;

    if-eqz v1, :cond_1

    .line 528
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverPopupShareViaBtn:Lcom/sec/android/app/myfiles/hover/HoverImageButton;

    invoke-virtual {v1, p0}, Lcom/sec/android/app/myfiles/hover/HoverImageButton;->setmHoverManager(Lcom/sec/android/app/myfiles/hover/AbsHoverManager;)V

    .line 529
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverPopupShareViaBtn:Lcom/sec/android/app/myfiles/hover/HoverImageButton;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mFilePath:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mFilePath:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isForwardable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 530
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverPopupShareViaBtn:Lcom/sec/android/app/myfiles/hover/HoverImageButton;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/myfiles/hover/HoverImageButton;->setVisibility(I)V

    .line 533
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverPopupDeleteBtn:Lcom/sec/android/app/myfiles/hover/HoverImageButton;

    if-eqz v1, :cond_3

    .line 534
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverPopupDeleteBtn:Lcom/sec/android/app/myfiles/hover/HoverImageButton;

    invoke-virtual {v1, p0}, Lcom/sec/android/app/myfiles/hover/HoverImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 535
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverPopupDeleteBtn:Lcom/sec/android/app/myfiles/hover/HoverImageButton;

    invoke-virtual {v1, p0}, Lcom/sec/android/app/myfiles/hover/HoverImageButton;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 537
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverPopupShareViaBtn:Lcom/sec/android/app/myfiles/hover/HoverImageButton;

    if-eqz v1, :cond_4

    .line 538
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverPopupShareViaBtn:Lcom/sec/android/app/myfiles/hover/HoverImageButton;

    invoke-virtual {v1, p0}, Lcom/sec/android/app/myfiles/hover/HoverImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 539
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverPopupShareViaBtn:Lcom/sec/android/app/myfiles/hover/HoverImageButton;

    invoke-virtual {v1, p0}, Lcom/sec/android/app/myfiles/hover/HoverImageButton;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 547
    :cond_4
    :goto_0
    return-void

    .line 543
    :cond_5
    const v1, 0x7f0f00ba

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 544
    .local v0, "v1":Landroid/view/View;
    if-eqz v0, :cond_4

    .line 545
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected abstract initViews()Z
.end method

.method protected isAnyHoverOperationBtnHovered()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 580
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverPopupShareViaBtn:Lcom/sec/android/app/myfiles/hover/HoverImageButton;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverPopupShareViaBtn:Lcom/sec/android/app/myfiles/hover/HoverImageButton;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/hover/HoverImageButton;->isHovered()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 585
    :cond_0
    :goto_0
    return v0

    .line 582
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverPopupDeleteBtn:Lcom/sec/android/app/myfiles/hover/HoverImageButton;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverPopupDeleteBtn:Lcom/sec/android/app/myfiles/hover/HoverImageButton;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/hover/HoverImageButton;->isHovered()Z

    move-result v1

    if-nez v1, :cond_0

    .line 585
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isShow()Z
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 369
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    .line 371
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 593
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 614
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->dissmissPopup()V

    .line 615
    return-void

    .line 596
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getCurrentFragmentID()I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getFragmentTagByID(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 598
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v1, "HOPT"

    const-string v2, "delete"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/Utils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 599
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getCurrentFragmentID()I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getFragmentTagByID(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mFilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->initiateToDeleteHoveredItem(Ljava/lang/String;)V

    goto :goto_0

    .line 604
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getCurrentFragmentID()I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getFragmentTagByID(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 606
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v1, "HOPT"

    const-string v2, "share"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/Utils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 607
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getCurrentFragmentID()I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getFragmentTagByID(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mFilePath:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->share(Ljava/lang/String;Z)V

    goto :goto_0

    .line 593
    :pswitch_data_0
    .packed-switch 0x7f0f00bb
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x2

    .line 552
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 576
    :cond_0
    :goto_0
    :pswitch_0
    return v3

    .line 554
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 555
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->removeDelayedMessage()V

    .line 559
    :pswitch_2
    const-string v0, "AbsHoverManager"

    const-string v1, "MotionEvent.ACTION_HOVER_ENTER"

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    .line 561
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 562
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 569
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 571
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 552
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public removeDelayedMessage()V
    .locals 7

    .prologue
    const/4 v6, 0x6

    const/4 v5, 0x5

    const/4 v3, 0x4

    const/4 v2, 0x0

    const/4 v4, 0x2

    .line 463
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 465
    const-string v0, "AbsHoverManager"

    const-string v1, "removeDelayedMessage: removeMessages(HoverMsg.MSG_SHOW_POPUP)"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 469
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 471
    const-string v0, "AbsHoverManager"

    const-string v1, "removeDelayedMessage: removeMessages(HoverMsg.MSG_FOLDER_SHOW)"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 475
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 477
    const-string v0, "AbsHoverManager"

    const-string v1, "removeDelayedMessage: removeMessages(HoverMsg.MSG_DISMISS_POPUP)"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 481
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 483
    const-string v0, "AbsHoverManager"

    const-string v1, "removeDelayedMessage: removeMessages(HoverMsg.MSG_FOLDER_SHOW_STOP)"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 484
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 487
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 489
    const-string v0, "AbsHoverManager"

    const-string v1, "removeDelayedMessage: removeMessages(HoverMsg.MSG_FOLDER_SHOW_THREAD_START)"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 493
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 497
    return-void
.end method

.method public removeListChangeMessage()V
    .locals 2

    .prologue
    .line 511
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 512
    return-void
.end method

.method protected requestAudioFocus()Z
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v1, 0x1

    .line 396
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHasAudioFocus:Z

    if-nez v2, :cond_2

    .line 398
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHasAudioFocus:Z

    .line 399
    const/4 v0, 0x0

    .line 400
    .local v0, "ret":I
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2}, Landroid/media/AudioManager;->isMusicActive()Z

    move-result v2

    if-ne v2, v1, :cond_0

    .line 402
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mAudioManager:Landroid/media/AudioManager;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v5, v4}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    .line 411
    :goto_0
    if-ne v0, v1, :cond_1

    .line 420
    .end local v0    # "ret":I
    :goto_1
    return v1

    .line 406
    .restart local v0    # "ret":I
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mAudioManager:Landroid/media/AudioManager;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v2, v3, v5, v1}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    goto :goto_0

    .line 417
    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHasAudioFocus:Z

    .line 420
    .end local v0    # "ret":I
    :cond_2
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHasAudioFocus:Z

    goto :goto_1
.end method

.method public sendDelayedMessage(I)V
    .locals 5
    .param p1, "ms"    # I

    .prologue
    const/4 v4, 0x0

    .line 449
    const-string v0, "AbsHoverManager"

    const-string v1, "sendDelayedMessage"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 451
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 452
    const-string v0, "AbsHoverManager"

    const-string v1, "sendDelayedMessage: removeMessages(HoverMsg.MSG_SHOW_POPUP)"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 456
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->isPaused()Z

    move-result v0

    if-nez v0, :cond_1

    .line 457
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->getHandler()Landroid/os/Handler;

    move-result-object v0

    int-to-long v2, p1

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 459
    :cond_1
    return-void
.end method

.method public sendListChangeMessage()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 501
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 503
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 506
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 507
    return-void
.end method

.method protected abstract setDialogPosition()V
.end method

.method protected setDialogProperties()V
    .locals 3

    .prologue
    .line 377
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverDialog:Landroid/app/Dialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 378
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 379
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 380
    return-void
.end method

.method public setFilePath(Ljava/lang/String;)V
    .locals 0
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 516
    iput-object p1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mFilePath:Ljava/lang/String;

    .line 517
    return-void
.end method

.method public setParam(ILandroid/graphics/Rect;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "categoryType"    # I
    .param p2, "mRect"    # Landroid/graphics/Rect;
    .param p3, "drawable"    # Landroid/graphics/drawable/Drawable;
    .param p4, "fileUri"    # Ljava/lang/String;
    .param p5, "mimeType"    # Ljava/lang/String;
    .param p6, "fromRemoteDevice"    # Z

    .prologue
    .line 228
    iput p1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mCategoryType:I

    .line 230
    iput-object p2, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverRect:Landroid/graphics/Rect;

    .line 232
    iput-object p3, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mRemoteDeviceImage:Landroid/graphics/drawable/Drawable;

    .line 234
    iput-object p4, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mFileUri:Ljava/lang/String;

    .line 236
    iput-object p5, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mFileMimeType:Ljava/lang/String;

    .line 238
    iput-boolean p6, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->bFromNearByDevice:Z

    .line 240
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->bFromDropBox:Z

    .line 241
    return-void
.end method

.method public setParam(ILandroid/graphics/Rect;Ljava/lang/String;)V
    .locals 2
    .param p1, "categoryType"    # I
    .param p2, "rect"    # Landroid/graphics/Rect;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 180
    iput p1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mCategoryType:I

    .line 182
    iput-object p2, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverRect:Landroid/graphics/Rect;

    .line 184
    if-eqz p3, :cond_0

    .line 186
    iput-object p3, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mFilePath:Ljava/lang/String;

    .line 188
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mFilePath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mFile:Ljava/io/File;

    .line 192
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->bFromDropBox:Z

    .line 193
    return-void
.end method

.method public setParam(ILandroid/graphics/Rect;Ljava/lang/String;I)V
    .locals 2
    .param p1, "categoryType"    # I
    .param p2, "rect"    # Landroid/graphics/Rect;
    .param p3, "path"    # Ljava/lang/String;
    .param p4, "position"    # I

    .prologue
    .line 197
    iput p1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mCategoryType:I

    .line 199
    iput-object p2, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverRect:Landroid/graphics/Rect;

    .line 201
    iput-object p3, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mFilePath:Ljava/lang/String;

    .line 203
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mFilePath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mFile:Ljava/io/File;

    .line 205
    iput p4, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverItemPosition:I

    .line 207
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->bFromDropBox:Z

    .line 208
    return-void
.end method

.method public setParam(ILandroid/graphics/Rect;Ljava/lang/String;Landroid/database/Cursor;Lcom/sec/android/app/myfiles/navigation/AbsNavigation;)V
    .locals 1
    .param p1, "categoryType"    # I
    .param p2, "mRect"    # Landroid/graphics/Rect;
    .param p3, "path"    # Ljava/lang/String;
    .param p4, "cursor"    # Landroid/database/Cursor;
    .param p5, "navigation"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .prologue
    .line 212
    iput p1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mCategoryType:I

    .line 214
    iput-object p2, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverRect:Landroid/graphics/Rect;

    .line 216
    iput-object p3, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mFilePath:Ljava/lang/String;

    .line 218
    iput-object p4, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverCursor:Landroid/database/Cursor;

    .line 220
    iput-object p5, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverFolderNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .line 222
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->bFromDropBox:Z

    .line 223
    return-void
.end method

.method public setParam(ILandroid/graphics/Rect;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "categoryType"    # I
    .param p2, "mRect"    # Landroid/graphics/Rect;
    .param p3, "path"    # Ljava/lang/String;
    .param p4, "thumbBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 246
    iput p1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mCategoryType:I

    .line 248
    iput-object p2, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverRect:Landroid/graphics/Rect;

    .line 250
    iput-object p3, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mFilePath:Ljava/lang/String;

    .line 252
    iput-object p4, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mDropBoxThumb:Landroid/graphics/Bitmap;

    .line 254
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->bFromDropBox:Z

    .line 255
    return-void
.end method

.method public setParam(Landroid/graphics/Rect;Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;)V
    .locals 1
    .param p1, "mRect"    # Landroid/graphics/Rect;
    .param p2, "appEntry"    # Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .prologue
    .line 259
    iput-object p1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverRect:Landroid/graphics/Rect;

    .line 261
    iput-object p2, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mAppEntry:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .line 263
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->bFromDropBox:Z

    .line 264
    return-void
.end method

.method protected abstract showContent()V
.end method

.method protected showDlg()V
    .locals 3

    .prologue
    .line 352
    const-string v1, "AbsHoverManager"

    const-string v2, "showDlg()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->isShow()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 356
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 358
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverDialog:Landroid/app/Dialog;

    if-eqz v1, :cond_1

    .line 359
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 364
    :cond_1
    :goto_0
    return-void

    .line 361
    :catch_0
    move-exception v0

    .line 362
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method
