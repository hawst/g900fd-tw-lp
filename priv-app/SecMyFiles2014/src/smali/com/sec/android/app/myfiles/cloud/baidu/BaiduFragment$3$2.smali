.class Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$3$2;
.super Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;
.source "BaiduFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$3;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$3;

.field final synthetic val$mime:Ljava/lang/String;

.field final synthetic val$uri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$3;Lcom/sec/android/app/myfiles/fragment/AbsFragment;Ljava/lang/Runnable;ZZLandroid/net/Uri;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    .param p3, "x1"    # Ljava/lang/Runnable;
    .param p4, "x2"    # Z
    .param p5, "x3"    # Z

    .prologue
    .line 1123
    iput-object p1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$3$2;->this$1:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$3;

    iput-object p6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$3$2;->val$uri:Landroid/net/Uri;

    iput-object p7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$3$2;->val$mime:Ljava/lang/String;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsFragment;Ljava/lang/Runnable;ZZ)V

    return-void
.end method


# virtual methods
.method public AsyncOpenExcute()V
    .locals 7

    .prologue
    .line 1128
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$3$2;->setCompletedCondition(Z)V

    .line 1130
    const/4 v0, 0x0

    .line 1134
    .local v0, "cachedPath":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$3$2;->getActivity()Landroid/app/Activity;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$3$2;->val$uri:Landroid/net/Uri;

    invoke-static {v4, v5}, Lcom/sec/android/cloudagent/CloudStore$API;->prefetchWithBlocking(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/sec/android/cloudagent/exception/CloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1142
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$3$2;->this$1:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$3;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$3;->mCachingCancelled:Z
    invoke-static {v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$3;->access$3300(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$3;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1162
    :goto_1
    return-void

    .line 1137
    :catch_0
    move-exception v1

    .line 1139
    .local v1, "e":Lcom/sec/android/cloudagent/exception/CloudException;
    invoke-virtual {v1}, Lcom/sec/android/cloudagent/exception/CloudException;->printStackTrace()V

    goto :goto_0

    .line 1147
    .end local v1    # "e":Lcom/sec/android/cloudagent/exception/CloudException;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$3$2;->val$uri:Landroid/net/Uri;

    .line 1149
    .local v3, "imageUri":Landroid/net/Uri;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_1

    .line 1151
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1153
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1155
    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    .line 1159
    .end local v2    # "file":Ljava/io/File;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$3$2;->this$1:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$3;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$3$2;->val$uri:Landroid/net/Uri;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$3$2;->val$mime:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$3;->intentForImageOpen(Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;)V
    invoke-static {v4, v3, v5, v6}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$3;->access$3400(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$3;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;)V

    .line 1161
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduFragment$3$2;->setCompletedCondition(Z)V

    goto :goto_1
.end method
