.class public Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;
.source "AddFTPFragment.java"


# static fields
.field private static final DEFAULT_ENCODING:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

.field private static final MODULE:Ljava/lang/String; = "AddFTPFragment"

.field private static final NUM_OF_SPINNERS:I = 0x2


# instance fields
.field private mAnonymousLayout:Landroid/widget/RelativeLayout;

.field private mAnonymousView:Landroid/widget/CheckBox;

.field private mEncodingView:Landroid/widget/Spinner;

.field private mModeView:Landroid/widget/TextView;

.field private final mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private mPasswordView:Landroid/widget/TextView;

.field private mPortView:Landroid/widget/EditText;

.field private mServerView:Landroid/widget/TextView;

.field private mSessionNameView:Landroid/widget/TextView;

.field private volatile mSpinnersCounter:I

.field private mStrLen:I

.field private final mTextWatcher:Landroid/text/TextWatcher;

.field private mUsernameView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 448
    sget-object v0, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->UTF_8:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    sput-object v0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->DEFAULT_ENCODING:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 60
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;-><init>()V

    .line 62
    iput v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mStrLen:I

    .line 446
    iput v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mSpinnersCounter:I

    .line 450
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$6;-><init>(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 467
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$7;-><init>(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mTextWatcher:Landroid/text/TextWatcher;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mAnonymousView:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mUsernameView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPasswordView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPortView:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mServerView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mAnonymousLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    .prologue
    .line 60
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mSpinnersCounter:I

    return v0
.end method

.method static synthetic access$604(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    .prologue
    .line 60
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mSpinnersCounter:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mSpinnersCounter:I

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;
    .param p1, "x1"    # I

    .prologue
    .line 60
    iput p1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mStrLen:I

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mModeView:Landroid/widget/TextView;

    return-object v0
.end method

.method private setShowInputMethod(Landroid/widget/TextView;)V
    .locals 4
    .param p1, "editText"    # Landroid/widget/TextView;

    .prologue
    .line 285
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$5;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$5;-><init>(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;Landroid/widget/TextView;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {p1, v0, v2, v3}, Landroid/widget/TextView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 296
    return-void
.end method


# virtual methods
.method protected checkData()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 348
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mServerView:Landroid/widget/TextView;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPortView:Landroid/widget/EditText;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mAnonymousView:Landroid/widget/CheckBox;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mUsernameView:Landroid/widget/TextView;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPasswordView:Landroid/widget/TextView;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mModeView:Landroid/widget/TextView;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mEncodingView:Landroid/widget/Spinner;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mSessionNameView:Landroid/widget/TextView;

    if-eqz v2, :cond_4

    .line 351
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mAnonymousView:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 352
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPortView:Landroid/widget/EditText;

    const v3, 0x2000006

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 353
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mServerView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->checkServer(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPortView:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->checkPort(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mSessionNameView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->checkSessionName(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 365
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 353
    goto :goto_0

    .line 357
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPortView:Landroid/widget/EditText;

    const v3, 0x2000005

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 358
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mServerView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->checkServer(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPortView:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->checkPort(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mUsernameView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->checkUsername(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPasswordView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->checkPassword(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mSessionNameView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->checkSessionName(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 365
    goto :goto_0
.end method

.method protected collectTextViews(Landroid/view/View;)Ljava/util/List;
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 302
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 304
    .local v0, "txtViews":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/widget/TextView;>;"
    if-eqz p1, :cond_0

    .line 306
    const v1, 0x7f0f0016

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 308
    const v1, 0x7f0f0019

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 310
    const v1, 0x7f0f001c

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 312
    const v1, 0x7f0f001f

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 314
    const v1, 0x7f0f0022

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 316
    const v1, 0x7f0f0025

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 318
    const v1, 0x7f0f0029

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 320
    const v1, 0x7f0f002b

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 324
    :cond_0
    return-object v0
.end method

.method protected gatherParameters()Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .locals 3

    .prologue
    .line 371
    const-string v0, "/"

    .line 372
    .local v0, "startPath":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mOldParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    if-eqz v1, :cond_0

    .line 373
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mOldParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getStartPath()Ljava/lang/String;

    move-result-object v0

    .line 375
    :cond_0
    new-instance v1, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    invoke-direct {v1}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;-><init>()V

    sget-object v2, Lcom/sec/android/app/myfiles/ftp/FTPType;->FTP:Lcom/sec/android/app/myfiles/ftp/FTPType;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->type(Lcom/sec/android/app/myfiles/ftp/FTPType;)Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mSessionNameView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->sessionName(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mServerView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->host(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPortView:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->getPort(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->port(I)Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mUsernameView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->getUsername(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->username(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPasswordView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->getPassword(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->password(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mModeView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->getMode(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPMode;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->mode(Lcom/sec/android/app/myfiles/ftp/FTPMode;)Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mAnonymousView:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->anonymous(Z)Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mEncodingView:Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->getEncoding(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->encoding(Lcom/sec/android/app/myfiles/ftp/FTPEncoding;)Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->startPath(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mSessionNameView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->rootSessionName(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->build()Lcom/sec/android/app/myfiles/ftp/FTPParams;

    move-result-object v1

    return-object v1
.end method

.method public getEditTextHasFocus()V
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mSessionNameView:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mSessionNameView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 265
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mSessionNameView:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->setShowInputMethod(Landroid/widget/TextView;)V

    .line 283
    :cond_0
    :goto_0
    return-void

    .line 267
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mServerView:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mServerView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mServerView:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->setShowInputMethod(Landroid/widget/TextView;)V

    goto :goto_0

    .line 270
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPortView:Landroid/widget/EditText;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPortView:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPortView:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->setShowInputMethod(Landroid/widget/TextView;)V

    goto :goto_0

    .line 273
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mUsernameView:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mUsernameView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mUsernameView:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->setShowInputMethod(Landroid/widget/TextView;)V

    goto :goto_0

    .line 276
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPasswordView:Landroid/widget/TextView;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPasswordView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPasswordView:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->setShowInputMethod(Landroid/widget/TextView;)V

    goto :goto_0

    .line 279
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mSessionNameView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mSessionNameView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mSessionNameView:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->setShowInputMethod(Landroid/widget/TextView;)V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5
    .param p1, "id"    # I

    .prologue
    .line 489
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v1

    .line 491
    .local v1, "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 493
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    packed-switch p1, :pswitch_data_0

    .line 546
    :goto_0
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    return-object v3

    .line 497
    :pswitch_0
    const/4 v2, 0x0

    .line 499
    .local v2, "selectedPosition":I
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mModeView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0b00bb

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 500
    const/4 v2, 0x1

    .line 505
    :goto_1
    const v3, 0x7f0b00b4

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 507
    const v3, 0x7f070003

    new-instance v4, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$8;

    invoke-direct {v4, p0, v1}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$8;-><init>(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;Lcom/sec/android/app/myfiles/utils/SharedDataStore;)V

    invoke-virtual {v0, v3, v2, v4}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 535
    const v3, 0x7f0b0017

    new-instance v4, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$9;

    invoke-direct {v4, p0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$9;-><init>(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 502
    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    .line 493
    nop

    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 69
    const-string v2, "AddFTPFragment"

    const-string v3, "onCreateView "

    invoke-static {v5, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 71
    const/4 v1, 0x0

    .line 75
    .local v1, "view":Landroid/view/View;
    const v2, 0x7f04000c

    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 84
    iput v5, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mSpinnersCounter:I

    .line 85
    const v2, 0x7f0f0017

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mSessionNameView:Landroid/widget/TextView;

    .line 86
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mSessionNameView:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    .line 87
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mSessionNameView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const/16 v4, 0x32

    invoke-static {v3, v4}, Lcom/sec/android/app/myfiles/utils/Utils;->getEditTextFilter(Landroid/content/Context;I)[Landroid/text/InputFilter;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    .line 88
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mSessionNameView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 89
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mSessionNameView:Landroid/widget/TextView;

    const-string v3, "inputType=PredictionOff;disableEmoticonInput=true"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 90
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mSessionNameView:Landroid/widget/TextView;

    const v3, 0x2000005

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setImeOptions(I)V

    .line 94
    :cond_0
    const v2, 0x7f0f001a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mServerView:Landroid/widget/TextView;

    .line 95
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mServerView:Landroid/widget/TextView;

    if-eqz v2, :cond_1

    .line 97
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mServerView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 98
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mServerView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget v4, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mStrLen:I

    add-int/lit8 v4, v4, 0x1

    invoke-static {v3, v4}, Lcom/sec/android/app/myfiles/utils/Utils;->getEditTextFilter(Landroid/content/Context;I)[Landroid/text/InputFilter;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    .line 99
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mServerView:Landroid/widget/TextView;

    const-string v3, "inputType=PredictionOff"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 102
    :cond_1
    const v2, 0x7f0f001d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPortView:Landroid/widget/EditText;

    .line 103
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPortView:Landroid/widget/EditText;

    if-eqz v2, :cond_2

    .line 105
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPortView:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 106
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPortView:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget v4, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mStrLen:I

    add-int/lit8 v4, v4, 0x1

    invoke-static {v3, v4}, Lcom/sec/android/app/myfiles/utils/Utils;->getEditTextFilterDigit(Landroid/content/Context;I)[Landroid/text/InputFilter;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 107
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPortView:Landroid/widget/EditText;

    const-string v3, "inputType=PredictionOff"

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 108
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPortView:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPortView:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setSelection(I)V

    .line 111
    :cond_2
    const v2, 0x7f0f0020

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mModeView:Landroid/widget/TextView;

    .line 112
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mModeView:Landroid/widget/TextView;

    if-eqz v2, :cond_3

    .line 113
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mModeView:Landroid/widget/TextView;

    new-instance v3, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mModeView:Landroid/widget/TextView;

    const v3, 0x7f0b00bb

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    :cond_3
    const v2, 0x7f0f0023

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mUsernameView:Landroid/widget/TextView;

    .line 126
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mUsernameView:Landroid/widget/TextView;

    if-eqz v2, :cond_4

    .line 128
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mUsernameView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 129
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mUsernameView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget v4, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mStrLen:I

    add-int/lit8 v4, v4, 0x1

    invoke-static {v3, v4}, Lcom/sec/android/app/myfiles/utils/Utils;->getEditTextFilter(Landroid/content/Context;I)[Landroid/text/InputFilter;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    .line 130
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mUsernameView:Landroid/widget/TextView;

    const-string v3, "inputType=PredictionOff;disableEmoticonInput=true"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 133
    :cond_4
    const v2, 0x7f0f0026

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPasswordView:Landroid/widget/TextView;

    .line 134
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPasswordView:Landroid/widget/TextView;

    if-eqz v2, :cond_5

    .line 136
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPasswordView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 137
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPasswordView:Landroid/widget/TextView;

    const-string v3, "inputType=PredictionOff"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 140
    :cond_5
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->collectTextViews(Landroid/view/View;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->checkTextViewsTextSize(Ljava/util/List;)V

    .line 142
    const v2, 0x7f0f0027

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mAnonymousLayout:Landroid/widget/RelativeLayout;

    .line 143
    const v2, 0x7f0f0028

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mAnonymousView:Landroid/widget/CheckBox;

    .line 144
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mAnonymousLayout:Landroid/widget/RelativeLayout;

    new-instance v3, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$2;-><init>(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 167
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mAnonymousView:Landroid/widget/CheckBox;

    new-instance v3, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$3;-><init>(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 187
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mAnonymousLayout:Landroid/widget/RelativeLayout;

    new-instance v3, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$4;

    invoke-direct {v3, p0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$4;-><init>(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 218
    const v2, 0x7f0f002c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mEncodingView:Landroid/widget/Spinner;

    .line 219
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mEncodingView:Landroid/widget/Spinner;

    if-eqz v2, :cond_6

    .line 220
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mEncodingView:Landroid/widget/Spinner;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 234
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->setDefaultValues()V

    .line 235
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->runRestore()V

    .line 236
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->checkData()Z

    .line 238
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mAnonymousView:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 239
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mUsernameView:Landroid/widget/TextView;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPasswordView:Landroid/widget/TextView;

    if-eqz v2, :cond_7

    .line 240
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mUsernameView:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 241
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPasswordView:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_7
    :goto_0
    move-object v2, v1

    .line 250
    :goto_1
    return-object v2

    .line 77
    :catch_0
    move-exception v0

    .line 79
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->onDestroy()V

    .line 81
    const/4 v2, 0x0

    goto :goto_1

    .line 244
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mUsernameView:Landroid/widget/TextView;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPasswordView:Landroid/widget/TextView;

    if-eqz v2, :cond_7

    .line 245
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mUsernameView:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 246
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPasswordView:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 258
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->onResume()V

    .line 259
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->getEditTextHasFocus()V

    .line 262
    return-void
.end method

.method protected restoreState(Lcom/sec/android/app/myfiles/ftp/FTPParams;)V
    .locals 6
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 392
    if-eqz p1, :cond_7

    .line 393
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mSessionNameView:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    .line 394
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getSessionName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "/"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 395
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getLabel()Ljava/lang/String;

    move-result-object v1

    .line 396
    .local v1, "tmp":Ljava/lang/String;
    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 397
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mSessionNameView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 403
    .end local v1    # "tmp":Ljava/lang/String;
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mServerView:Landroid/widget/TextView;

    if-eqz v2, :cond_1

    .line 404
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mServerView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getHost()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 406
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPortView:Landroid/widget/EditText;

    if-eqz v2, :cond_2

    .line 407
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPortView:Landroid/widget/EditText;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getPort()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 409
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mModeView:Landroid/widget/TextView;

    if-eqz v2, :cond_3

    .line 410
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getMode()Lcom/sec/android/app/myfiles/ftp/FTPMode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPMode;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v5, "active"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 411
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mModeView:Landroid/widget/TextView;

    const v5, 0x7f0b00ba

    invoke-virtual {p0, v5}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 417
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mAnonymousView:Landroid/widget/CheckBox;

    if-eqz v2, :cond_4

    .line 418
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getAnonymous()Z

    move-result v0

    .line 419
    .local v0, "isAnonymous":Z
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mAnonymousView:Landroid/widget/CheckBox;

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 420
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mUsernameView:Landroid/widget/TextView;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPasswordView:Landroid/widget/TextView;

    if-eqz v2, :cond_4

    .line 421
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mUsernameView:Landroid/widget/TextView;

    if-nez v0, :cond_a

    move v2, v3

    :goto_2
    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 422
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPasswordView:Landroid/widget/TextView;

    if-nez v0, :cond_b

    :goto_3
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 425
    .end local v0    # "isAnonymous":Z
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mUsernameView:Landroid/widget/TextView;

    if-eqz v2, :cond_5

    .line 426
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mUsernameView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getUsername()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 428
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPasswordView:Landroid/widget/TextView;

    if-eqz v2, :cond_6

    .line 429
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mPasswordView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getPassword()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 431
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mEncodingView:Landroid/widget/Spinner;

    if-eqz v2, :cond_7

    .line 432
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mEncodingView:Landroid/widget/Spinner;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getEncoding()Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->getEncodingPosition(Lcom/sec/android/app/myfiles/ftp/FTPEncoding;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setSelection(I)V

    .line 435
    :cond_7
    return-void

    .line 400
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mSessionNameView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getSessionName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 414
    :cond_9
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mModeView:Landroid/widget/TextView;

    const v5, 0x7f0b00bb

    invoke-virtual {p0, v5}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .restart local v0    # "isAnonymous":Z
    :cond_a
    move v2, v4

    .line 421
    goto :goto_2

    :cond_b
    move v3, v4

    .line 422
    goto :goto_3
.end method

.method protected setDefaultValues()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 329
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mEncodingView:Landroid/widget/Spinner;

    if-eqz v2, :cond_1

    .line 330
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mEncodingView:Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ArrayAdapter;

    .line 331
    .local v0, "adapter":Landroid/widget/ArrayAdapter;
    if-eqz v0, :cond_1

    .line 332
    sget-object v2, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->DEFAULT_ENCODING:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->getPosition(Ljava/lang/Object;)I

    move-result v1

    .line 333
    .local v1, "position":I
    if-ltz v1, :cond_0

    .line 334
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mEncodingView:Landroid/widget/Spinner;

    invoke-virtual {v2, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 337
    :cond_0
    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v2

    if-ne v2, v3, :cond_2

    .line 338
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mEncodingView:Landroid/widget/Spinner;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 344
    .end local v0    # "adapter":Landroid/widget/ArrayAdapter;
    .end local v1    # "position":I
    :cond_1
    :goto_0
    return-void

    .line 340
    .restart local v0    # "adapter":Landroid/widget/ArrayAdapter;
    .restart local v1    # "position":I
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mEncodingView:Landroid/widget/Spinner;

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setEnabled(Z)V

    goto :goto_0
.end method
