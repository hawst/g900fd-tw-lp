.class public Lcom/sec/android/app/myfiles/navigation/FTPShortcutNavigation;
.super Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
.source "FTPShortcutNavigation.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;-><init>(Landroid/content/Context;I)V

    .line 27
    return-void
.end method


# virtual methods
.method public AsyncOpen()V
    .locals 0

    .prologue
    .line 91
    return-void
.end method

.method public getFilesInCurrentFolder()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPShortcutNavigation;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->getFtpShortcutCursor(Landroid/content/Context;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getFilesInFolder(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/FTPShortcutNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getFilesInFolder(Ljava/lang/String;I)Landroid/database/Cursor;
    .locals 2
    .param p1, "folderPath"    # Ljava/lang/String;
    .param p2, "showFolderFileType"    # I

    .prologue
    .line 69
    iget v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPShortcutNavigation;->mShowFolderFileType:I

    .line 71
    .local v1, "prevShowFolderFileType":I
    iput p2, p0, Lcom/sec/android/app/myfiles/navigation/FTPShortcutNavigation;->mShowFolderFileType:I

    .line 73
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/navigation/FTPShortcutNavigation;->getFilesInFolder(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 75
    .local v0, "c":Landroid/database/Cursor;
    iput v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPShortcutNavigation;->mShowFolderFileType:I

    .line 77
    return-object v0
.end method

.method public goBack()Z
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x1

    return v0
.end method

.method public goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z
    .locals 1
    .param p1, "folderPath"    # Ljava/lang/String;
    .param p2, "isBack"    # Ljava/lang/Boolean;

    .prologue
    .line 36
    const/4 v0, 0x0

    return v0
.end method

.method public goToRoot(Ljava/lang/Boolean;)Z
    .locals 1
    .param p1, "isBack"    # Ljava/lang/Boolean;

    .prologue
    .line 41
    const/4 v0, 0x1

    return v0
.end method

.method public goUp()Z
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x1

    return v0
.end method

.method public isCurrentFolderRoot()Z
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x1

    return v0
.end method

.method public refreshNavigation()V
    .locals 0

    .prologue
    .line 88
    return-void
.end method

.method public setCurrentPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/FTPShortcutNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 64
    return-void
.end method
