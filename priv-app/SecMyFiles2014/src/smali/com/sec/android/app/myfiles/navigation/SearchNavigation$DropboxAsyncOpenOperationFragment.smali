.class public Lcom/sec/android/app/myfiles/navigation/SearchNavigation$DropboxAsyncOpenOperationFragment;
.super Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;
.source "SearchNavigation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/navigation/SearchNavigation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DropboxAsyncOpenOperationFragment"
.end annotation


# instance fields
.field private mAsyncOpenRunnable:Lcom/sec/android/app/myfiles/navigation/SearchNavigation$AsyncOpenRunnable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1705
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public AsyncOpenExcute()V
    .locals 1

    .prologue
    .line 1716
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$DropboxAsyncOpenOperationFragment;->mAsyncOpenRunnable:Lcom/sec/android/app/myfiles/navigation/SearchNavigation$AsyncOpenRunnable;

    if-eqz v0, :cond_0

    .line 1718
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$DropboxAsyncOpenOperationFragment;->mAsyncOpenRunnable:Lcom/sec/android/app/myfiles/navigation/SearchNavigation$AsyncOpenRunnable;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$AsyncOpenRunnable;->run()V

    .line 1722
    :cond_0
    return-void
.end method

.method public setAsyncOpenRunnable(Lcom/sec/android/app/myfiles/navigation/SearchNavigation$AsyncOpenRunnable;)V
    .locals 0
    .param p1, "asyncOpenRunnable"    # Lcom/sec/android/app/myfiles/navigation/SearchNavigation$AsyncOpenRunnable;

    .prologue
    .line 1709
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$DropboxAsyncOpenOperationFragment;->mAsyncOpenRunnable:Lcom/sec/android/app/myfiles/navigation/SearchNavigation$AsyncOpenRunnable;

    .line 1711
    return-void
.end method
