.class public Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;
.source "SearchsSetFileTypeFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$MyListAdapter;,
        Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;
    }
.end annotation


# static fields
.field private static _instance:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;


# instance fields
.field intent:Landroid/content/Intent;

.field isDownloadedAppsNeedToHidden:Z

.field isfromAdvance:Z

.field mActivity:Landroid/app/Activity;

.field mData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;",
            ">;"
        }
    .end annotation
.end field

.field mFileTypeImage:Landroid/widget/LinearLayout;

.field sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->_instance:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;-><init>()V

    .line 47
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->intent:Landroid/content/Intent;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    .line 49
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    .line 299
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->isAllSelected()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->isAnySelected()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->clickOK()V

    return-void
.end method

.method private clickOK()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v3, 0x1

    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 228
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 266
    :goto_0
    return-void

    .line 234
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;->isChecked()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFileTypeImages(Z)V

    .line 235
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;->isChecked()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFileTypeVideos(Z)V

    .line 236
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;->isChecked()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFileTypeMusic(Z)V

    .line 237
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;->isChecked()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFileTypeDocument(Z)V

    .line 241
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->intent:Landroid/content/Intent;

    const v0, 0x7f0b0003

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;->isChecked()Z

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 242
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->intent:Landroid/content/Intent;

    const v0, 0x7f0b0004

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;->isChecked()Z

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 243
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->intent:Landroid/content/Intent;

    const v0, 0x7f0b0147

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;->isChecked()Z

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 244
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->intent:Landroid/content/Intent;

    const v0, 0x7f0b0006

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;->isChecked()Z

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 246
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->isDownloadedAppsNeedToHidden:Z

    if-eqz v0, :cond_1

    .line 247
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->isAllSelected()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFileTypeAll(Z)V

    .line 248
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->intent:Landroid/content/Intent;

    const v1, 0x7f0b0001

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->isAllSelected()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFileTypeDownloadedApps(Z)V

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->intent:Landroid/content/Intent;

    const v1, 0x7f0b0007

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 261
    :goto_1
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->isfromAdvance:Z

    if-eqz v0, :cond_2

    .line 262
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getTargetRequestCode()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->intent:Landroid/content/Intent;

    invoke-virtual {v0, v1, v5, v2}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    goto/16 :goto_0

    .line 252
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;->isChecked()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFileTypeAll(Z)V

    .line 253
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->intent:Landroid/content/Intent;

    const v0, 0x7f0b0001

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;->isChecked()Z

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 254
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;->isChecked()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFileTypeDownloadedApps(Z)V

    .line 255
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->intent:Landroid/content/Intent;

    const v0, 0x7f0b0007

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;->isChecked()Z

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_1

    .line 264
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getTargetRequestCode()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->intent:Landroid/content/Intent;

    invoke-virtual {p0, v0, v5, v1}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->onClickSearchCallback(IILandroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public static getInstance()Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->_instance:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    if-nez v0, :cond_0

    .line 59
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->_instance:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    .line 62
    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->_instance:Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    return-object v0
.end method

.method private isAllSelected()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->isDownloadedAppsNeedToHidden:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 169
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isAnySelected()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->isDownloadedAppsNeedToHidden:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 178
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 373
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 374
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getTargetRequestCode()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 375
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v10, 0x7f09016f

    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mActivity:Landroid/app/Activity;

    .line 69
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v5}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "title"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 71
    .local v4, "titleResId":I
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const v6, 0x7f0f00e5

    invoke-virtual {v5, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mFileTypeImage:Landroid/widget/LinearLayout;

    .line 72
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "search_advance_intent_key"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->isfromAdvance:Z

    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "search_file_type_index_intent_key"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 74
    .local v2, "index":I
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->intent:Landroid/content/Intent;

    const-string v6, "index"

    invoke-virtual {v5, v6, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 76
    new-instance v3, Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v3, v5}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 77
    .local v3, "listView":Landroid/widget/ListView;
    const/high16 v5, 0x2000000

    invoke-virtual {v3, v5}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    .line 78
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090170

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    invoke-virtual {v3, v5, v6, v7, v8}, Landroid/widget/ListView;->setPadding(IIII)V

    .line 79
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v1, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 80
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 81
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "search_advance_is_downloaded_app_enable"

    invoke-virtual {v5, v6, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-nez v5, :cond_0

    .line 82
    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->isDownloadedAppsNeedToHidden:Z

    .line 83
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v5, v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFileTypeDownloadedApps(Z)V

    .line 86
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    new-instance v6, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    const v7, 0x7f0b0001

    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeAll()Z

    move-result v8

    invoke-direct {v6, v7, v8}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 87
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    new-instance v6, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    const v7, 0x7f0b0003

    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeImages()Z

    move-result v8

    invoke-direct {v6, v7, v8}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 88
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    new-instance v6, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    const v7, 0x7f0b0004

    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeVideos()Z

    move-result v8

    invoke-direct {v6, v7, v8}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    new-instance v6, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    const v7, 0x7f0b0147

    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeMusic()Z

    move-result v8

    invoke-direct {v6, v7, v8}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    new-instance v6, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    const v7, 0x7f0b0006

    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeDocument()Z

    move-result v8

    invoke-direct {v6, v7, v8}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeAll()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 93
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v5, v9}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFileTypeDownloadedApps(Z)V

    .line 95
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mData:Ljava/util/ArrayList;

    new-instance v6, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;

    const v7, 0x7f0b0007

    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->sharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSearchFileTypeDownloadedApps()Z

    move-result v8

    invoke-direct {v6, v7, v8}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$ListItem;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 98
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$MyListAdapter;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$MyListAdapter;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;)V

    .line 99
    .local v0, "adapter":Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$MyListAdapter;
    invoke-virtual {v3, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 100
    new-instance v5, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$1;

    invoke-direct {v5, p0, v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$MyListAdapter;)V

    invoke-virtual {v3, v5}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 155
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 156
    const v5, 0x7f0b00eb

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 158
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->setupDialog(Landroid/app/AlertDialog$Builder;)V

    .line 160
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    return-object v5
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 183
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->onDestroy()V

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mFileTypeImage:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->mFileTypeImage:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 190
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 360
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->isAnySelected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 362
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->isfromAdvance:Z

    if-nez v0, :cond_0

    .line 364
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 365
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    .line 368
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->onResume()V

    .line 369
    return-void
.end method

.method protected setupDialog(Landroid/app/AlertDialog$Builder;)V
    .locals 2
    .param p1, "builder"    # Landroid/app/AlertDialog$Builder;

    .prologue
    .line 195
    const v0, 0x7f0b0015

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$2;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 204
    const v0, 0x7f0b0017

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment$3;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 223
    return-void
.end method

.method protected setupInputEditText(Landroid/widget/EditText;)V
    .locals 0
    .param p1, "editText"    # Landroid/widget/EditText;

    .prologue
    .line 272
    return-void
.end method
