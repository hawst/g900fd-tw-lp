.class Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$5;
.super Lcom/samsung/android/privatemode/IPrivateModeClient$Stub;
.source "FTPBrowserFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)V
    .locals 0

    .prologue
    .line 412
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    invoke-direct {p0}, Lcom/samsung/android/privatemode/IPrivateModeClient$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onStateChange(II)V
    .locals 4
    .param p1, "state"    # I
    .param p2, "extInfo"    # I

    .prologue
    const/4 v3, 0x2

    .line 417
    # getter for: Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->MODULE:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->access$400()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PrivateModeClient onStateChange: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 420
    if-nez p1, :cond_0

    .line 422
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

    if-eqz v0, :cond_0

    .line 424
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

    invoke-virtual {v1, p0}, Lcom/samsung/android/privatemode/PrivateModeManager;->registerClient(Lcom/samsung/android/privatemode/IPrivateModeClient;)Landroid/os/IBinder;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    .line 426
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    if-nez v0, :cond_0

    .line 427
    # getter for: Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->MODULE:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->access$400()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PrivateModeClient is not registered : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 433
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 434
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->moveFTPToSecretBox()V

    .line 437
    :cond_1
    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    .line 439
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    if-eqz v0, :cond_2

    .line 441
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/privatemode/PrivateModeManager;->unregisterClient(Landroid/os/IBinder;Z)Z

    .line 443
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    .line 446
    :cond_2
    return-void
.end method
