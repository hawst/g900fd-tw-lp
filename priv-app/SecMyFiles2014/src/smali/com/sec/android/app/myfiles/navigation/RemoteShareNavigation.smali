.class public Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;
.super Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
.source "RemoteShareNavigation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$FolderInfoReceiverListener;,
        Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;,
        Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$Media;,
        Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$ContentColumns;,
        Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$MediaColumns;
    }
.end annotation


# static fields
.field public static final CONTENT_APPEND:Ljava/lang/String; = "content"

.field private static final CONTENT_PROJECTION:[Ljava/lang/String;

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final FILES_CONTENT_URI:Landroid/net/Uri;

.field public static final FOLDER_CONTENT_TOKEN:I = 0x2

.field public static final GET_FOLERS_INFO_TOKEN:I = 0x4

.field public static final GET_FOLER_NAME_TOKEN:I = 0x3

.field public static final INBOX_CONTENT_URI:Landroid/net/Uri;

.field public static final INBOX_TOKEN:I = 0x0

.field private static final MEDIA_PROJECTION:[Ljava/lang/String;

.field public static final OUTBOX_CONTENT_URI:Landroid/net/Uri;

.field public static final OUTBOX_TOKEN:I = 0x1

.field public static final STATUS_CANCELED:I = 0xc9

.field public static final STATUS_COMPLETED:I = 0x3

.field public static final STATUS_FAILED:I = -0x1

.field public static final STATUS_NETWORK_ERROR:I = 0x190

.field public static final STATUS_NOT_START:I = 0x0

.field public static final STATUS_ON_PROGRESS:I = 0x2

.field public static final STATUS_ON_PROGRESS_FOLDER:I = 0x64

.field public static final STATUS_PAUSED:I = 0x12c

.field public static final STATUS_PENDING:I = 0x1

.field public static final STATUS_PREPARING:I = 0x0

.field public static final STATUS_SUCCESS:I = 0xc8

.field public static final STATUS_UNEXPECTED_ERROR:I = 0x191


# instance fields
.field private mCurrentCursor:Landroid/database/Cursor;

.field private mFolderInfoReceivedListener:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$FolderInfoReceiverListener;

.field private mIsSyncing:Z

.field private mMessageFolder:I

.field private mQueryHandler:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;

.field private mRequestedFolder:Ljava/lang/String;

.field private mRequestedUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 41
    const-string v0, "content://com.sec.rshare/media"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->CONTENT_URI:Landroid/net/Uri;

    .line 42
    sget-object v0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "inbox"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->INBOX_CONTENT_URI:Landroid/net/Uri;

    .line 43
    sget-object v0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "outbox"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->OUTBOX_CONTENT_URI:Landroid/net/Uri;

    .line 44
    const-string v0, "content://com.sec.rshare/content"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->FILES_CONTENT_URI:Landroid/net/Uri;

    .line 85
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "recipient_ids"

    aput-object v1, v0, v4

    const-string v1, "title"

    aput-object v1, v0, v5

    const-string v1, "date"

    aput-object v1, v0, v6

    const-string v1, "expire_date"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "media_box"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "status"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->MEDIA_PROJECTION:[Ljava/lang/String;

    .line 95
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "thumbnail_uri"

    aput-object v1, v0, v4

    const-string v1, "status"

    aput-object v1, v0, v5

    const-string v1, "filename"

    aput-object v1, v0, v6

    const-string v1, "remote_uri"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "file_size"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "content_type"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "local_file"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "complete_count"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "chunk_size"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "chunk_count"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->CONTENT_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 126
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mIsSyncing:Z

    .line 50
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mMessageFolder:I

    .line 127
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I

    .prologue
    const/4 v3, 0x0

    .line 130
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;-><init>(Landroid/content/Context;I)V

    .line 49
    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mIsSyncing:Z

    .line 50
    const/4 v2, -0x1

    iput v2, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mMessageFolder:I

    .line 131
    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->setAllAttachMode(Z)V

    .line 132
    new-instance v2, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;-><init>(Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;Landroid/content/ContentResolver;)V

    iput-object v2, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mQueryHandler:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;

    .line 134
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 135
    .local v0, "contentResolver":Landroid/content/ContentResolver;
    new-instance v1, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$1;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$1;-><init>(Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;Landroid/os/Handler;)V

    .line 152
    .local v1, "observer":Landroid/database/ContentObserver;
    sget-object v2, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3, v1}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 153
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mMessageFolder:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mIsSyncing:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;
    .param p1, "x1"    # Z

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mIsSyncing:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;)Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$FolderInfoReceiverListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mFolderInfoReceivedListener:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$FolderInfoReceiverListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mRequestedFolder:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->getFolderInfoById(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$502(Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mCurrentCursor:Landroid/database/Cursor;

    return-object p1
.end method

.method private getFolderInfoById(Ljava/lang/String;)V
    .locals 8
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 376
    sget-object v3, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->CONTENT_URI:Landroid/net/Uri;

    .line 377
    .local v3, "uri":Landroid/net/Uri;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 378
    .local v5, "selection":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mQueryHandler:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;

    const/4 v1, 0x3

    sget-object v4, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->MEDIA_PROJECTION:[Ljava/lang/String;

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    return-void
.end method


# virtual methods
.method public AsyncOpen()V
    .locals 0

    .prologue
    .line 384
    return-void
.end method

.method public OnFragmentDestroy()V
    .locals 0

    .prologue
    .line 287
    return-void
.end method

.method protected abortCurrentOperation()V
    .locals 0

    .prologue
    .line 284
    return-void
.end method

.method public getCurrentFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mCurrentFolderPath:Ljava/lang/String;

    return-object v0
.end method

.method public getFilesInCurrentFolder()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->getFilesInFolder(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getFilesInFolder(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 258
    iget v0, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mShowFolderFileType:I

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->getFilesInFolder(Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getFilesInFolder(Ljava/lang/String;I)Landroid/database/Cursor;
    .locals 1
    .param p1, "folderPath"    # Ljava/lang/String;
    .param p2, "showFolderFileType"    # I

    .prologue
    .line 268
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mCurrentCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mCurrentCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mCurrentCursor:Landroid/database/Cursor;

    .line 271
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemsInFolder(Ljava/lang/String;)I
    .locals 1
    .param p1, "dirpath"    # Ljava/lang/String;

    .prologue
    .line 280
    const/4 v0, 0x0

    return v0
.end method

.method public getMessageFolder()I
    .locals 1

    .prologue
    .line 162
    iget v0, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mMessageFolder:I

    return v0
.end method

.method public goBack()Z
    .locals 1

    .prologue
    .line 248
    const/4 v0, 0x0

    return v0
.end method

.method public goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z
    .locals 13
    .param p1, "folderId"    # Ljava/lang/String;
    .param p2, "isBack"    # Ljava/lang/Boolean;

    .prologue
    .line 192
    if-nez p1, :cond_0

    .line 193
    invoke-virtual {p0, p2}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->goToRoot(Ljava/lang/Boolean;)Z

    .line 234
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 195
    :cond_0
    sget-object v0, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 196
    const/4 v0, 0x0

    goto :goto_1

    .line 198
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mIsSyncing:Z

    if-eqz v0, :cond_2

    .line 199
    const/4 v0, 0x1

    goto :goto_1

    .line 201
    :cond_2
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mRequestedFolder:Ljava/lang/String;

    .line 202
    sget-object v0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0, p1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "content"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 203
    .local v3, "mediaUri":Landroid/net/Uri;
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mIsSyncing:Z

    .line 204
    iput-object v3, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mRequestedUri:Landroid/net/Uri;

    .line 205
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 206
    .local v12, "sortByStr":Ljava/lang/StringBuilder;
    iget v0, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mCurrentSortBy:I

    packed-switch v0, :pswitch_data_0

    .line 223
    :goto_2
    iget v0, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mCurrentInOrder:I

    if-nez v0, :cond_3

    const-string v0, " ASC"

    :goto_3
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mQueryHandler:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;

    const/4 v1, 0x2

    const/4 v2, 0x0

    sget-object v4, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->CONTENT_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    const/4 v0, 0x3

    new-array v8, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "media_id"

    aput-object v1, v8, v0

    const/4 v0, 0x1

    const-string v1, " COUNT(media_id)"

    aput-object v1, v8, v0

    const/4 v0, 0x2

    const-string v1, " SUM(file_size) "

    aput-object v1, v8, v0

    .line 232
    .local v8, "infoProjection":[Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mQueryHandler:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;

    const/4 v5, 0x4

    const/4 v6, 0x0

    sget-object v7, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->FILES_CONTENT_URI:Landroid/net/Uri;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "media_id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v4 .. v11}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 209
    .end local v8    # "infoProjection":[Ljava/lang/String;
    :pswitch_0
    const-string v0, "filename"

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    const-string v0, " COLLATE LOCALIZED"

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 213
    :pswitch_1
    const-string v0, "file_size"

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 216
    :pswitch_2
    const-string v0, "content_type"

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 219
    :pswitch_3
    const-string v0, "filename"

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    const-string v0, " COLLATE LOCALIZED"

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 223
    :cond_3
    const-string v0, " DESC"

    goto :goto_3

    .line 206
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method public goToRoot(Ljava/lang/Boolean;)Z
    .locals 23
    .param p1, "isBack"    # Ljava/lang/Boolean;

    .prologue
    .line 336
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mIsSyncing:Z

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mMessageFolder:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    .line 337
    :cond_0
    const/4 v2, 0x1

    .line 372
    :goto_0
    return v2

    .line 339
    :cond_1
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mIsSyncing:Z

    .line 340
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mRequestedFolder:Ljava/lang/String;

    .line 342
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v18

    .line 344
    .local v18, "calendar":Ljava/util/Calendar;
    invoke-virtual/range {v18 .. v18}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v20

    .line 346
    .local v20, "currentTime":J
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mMessageFolder:I

    const/16 v3, 0x27

    if-ne v2, v3, :cond_2

    .line 348
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "expire_date > "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, v20

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 350
    .local v19, "selectionExpireDate":Ljava/lang/String;
    const-string v22, "status<>200"

    .line 352
    .local v22, "selectionStatus":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " OR "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "status<>200"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "media_box"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " IN ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 355
    .local v7, "selection":Ljava/lang/String;
    const-string v9, "date DESC"

    .line 356
    .local v9, "sortBy":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mRequestedUri:Landroid/net/Uri;

    .line 357
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mQueryHandler:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;

    const/4 v3, 0x1

    const/4 v4, 0x0

    sget-object v5, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->CONTENT_URI:Landroid/net/Uri;

    sget-object v6, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->MEDIA_PROJECTION:[Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual/range {v2 .. v9}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    .end local v19    # "selectionExpireDate":Ljava/lang/String;
    .end local v22    # "selectionStatus":Ljava/lang/String;
    :goto_1
    const/4 v2, 0x3

    new-array v14, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "media_id"

    aput-object v3, v14, v2

    const/4 v2, 0x1

    const-string v3, " COUNT(media_id)"

    aput-object v3, v14, v2

    const/4 v2, 0x2

    const-string v3, " SUM(file_size) "

    aput-object v3, v14, v2

    .line 371
    .local v14, "infoProjection":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mQueryHandler:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;

    const/4 v11, 0x4

    const/4 v12, 0x0

    sget-object v13, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->FILES_CONTENT_URI:Landroid/net/Uri;

    const-string v15, "1) GROUP BY (media_id"

    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-virtual/range {v10 .. v17}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 359
    .end local v7    # "selection":Ljava/lang/String;
    .end local v9    # "sortBy":Ljava/lang/String;
    .end local v14    # "infoProjection":[Ljava/lang/String;
    :cond_2
    const-string v9, "date DESC"

    .line 360
    .restart local v9    # "sortBy":Ljava/lang/String;
    sget-object v2, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->INBOX_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mRequestedUri:Landroid/net/Uri;

    .line 361
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "expire_date > "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, v20

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 362
    .restart local v7    # "selection":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mQueryHandler:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-object v5, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->INBOX_CONTENT_URI:Landroid/net/Uri;

    sget-object v6, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->MEDIA_PROJECTION:[Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual/range {v2 .. v9}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$QueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public goUp()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 239
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 243
    :goto_0
    return v0

    .line 242
    :cond_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->goToRoot(Ljava/lang/Boolean;)Z

    .line 243
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isCurrentFolderRoot()Z
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mCurrentFolderPath:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 185
    const/4 v0, 0x1

    .line 187
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 167
    invoke-super {p0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->onPause()V

    .line 168
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 172
    invoke-super {p0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->onResume()V

    .line 173
    return-void
.end method

.method public refreshNavigation()V
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mCurrentFolderPath:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z

    .line 180
    :cond_0
    return-void
.end method

.method public setCurrentPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 263
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 264
    return-void
.end method

.method public setMessageFolder(I)V
    .locals 1
    .param p1, "messageFolder"    # I

    .prologue
    .line 156
    const/16 v0, 0x26

    if-eq p1, v0, :cond_0

    const/16 v0, 0x27

    if-ne p1, v0, :cond_1

    .line 157
    :cond_0
    iput p1, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mMessageFolder:I

    .line 159
    :cond_1
    return-void
.end method

.method public setOnFolderInfoReceivedListener(Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$FolderInfoReceiverListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$FolderInfoReceiverListener;

    .prologue
    .line 387
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mFolderInfoReceivedListener:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$FolderInfoReceiverListener;

    .line 388
    return-void
.end method
