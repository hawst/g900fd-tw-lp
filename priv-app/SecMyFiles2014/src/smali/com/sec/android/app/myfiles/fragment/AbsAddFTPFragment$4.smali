.class synthetic Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment$4;
.super Ljava/lang/Object;
.source "AbsAddFTPFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$sec$android$app$myfiles$ftp$FTPEncryption:[I

.field static final synthetic $SwitchMap$com$sec$android$app$myfiles$ftp$FTPMode:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 461
    invoke-static {}, Lcom/sec/android/app/myfiles/ftp/FTPEncryption;->values()[Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment$4;->$SwitchMap$com$sec$android$app$myfiles$ftp$FTPEncryption:[I

    :try_start_0
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment$4;->$SwitchMap$com$sec$android$app$myfiles$ftp$FTPEncryption:[I

    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPEncryption;->IMPLICIT:Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_0
    :try_start_1
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment$4;->$SwitchMap$com$sec$android$app$myfiles$ftp$FTPEncryption:[I

    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPEncryption;->EXPLICIT:Lcom/sec/android/app/myfiles/ftp/FTPEncryption;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    .line 421
    :goto_1
    invoke-static {}, Lcom/sec/android/app/myfiles/ftp/FTPMode;->values()[Lcom/sec/android/app/myfiles/ftp/FTPMode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment$4;->$SwitchMap$com$sec$android$app$myfiles$ftp$FTPMode:[I

    :try_start_2
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment$4;->$SwitchMap$com$sec$android$app$myfiles$ftp$FTPMode:[I

    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPMode;->ACTIVE:Lcom/sec/android/app/myfiles/ftp/FTPMode;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_2
    :try_start_3
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment$4;->$SwitchMap$com$sec$android$app$myfiles$ftp$FTPMode:[I

    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPMode;->PASSIVE:Lcom/sec/android/app/myfiles/ftp/FTPMode;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_3
    return-void

    :catch_0
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v0

    goto :goto_2

    .line 461
    :catch_2
    move-exception v0

    goto :goto_1

    :catch_3
    move-exception v0

    goto :goto_0
.end method
