.class Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$2;
.super Ljava/lang/Object;
.source "AbsFileOperationTask.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;)V
    .locals 0

    .prologue
    .line 233
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x1

    .line 238
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 247
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRunnable:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    .line 249
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget-object v2, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRunnable:Ljava/lang/Runnable;

    monitor-enter v2

    .line 253
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    const/4 v3, 0x0

    # setter for: Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mWaitDialog:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->access$002(Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;Z)Z

    .line 255
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 261
    :goto_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 267
    :cond_0
    return v4

    .line 242
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iput-boolean v4, v1, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->mRenameCancelled:Z

    goto :goto_0

    .line 257
    :catch_0
    move-exception v0

    .line 259
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 261
    .end local v0    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 238
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
