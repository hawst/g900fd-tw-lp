.class public Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;
.source "RenameOnConflictFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$RenameOnConflictListener;
    }
.end annotation


# static fields
.field private static final MESSAGE_RENAME_CANCEL:I = 0x1

.field private static final MESSAGE_RENAME_SUCCESS:I


# instance fields
.field private mExtension:Ljava/lang/String;

.field private mFolderName:Ljava/lang/String;

.field private mFormat:I

.field private mHandler:Landroid/os/Handler;

.field private mListener:Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$RenameOnConflictListener;

.field private mName:Ljava/lang/String;

.field private mPath:Ljava/lang/String;

.field private mTargetFragmentId:I

.field private mToast:Landroid/widget/Toast;

.field private newName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;-><init>()V

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mPath:Ljava/lang/String;

    .line 47
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mExtension:Ljava/lang/String;

    .line 48
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mName:Ljava/lang/String;

    .line 49
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mFolderName:Ljava/lang/String;

    .line 50
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->newName:Ljava/lang/String;

    .line 51
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mTargetFragmentId:I

    .line 52
    const/16 v0, 0x3001

    iput v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mFormat:I

    .line 53
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mToast:Landroid/widget/Toast;

    .line 54
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mListener:Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$RenameOnConflictListener;

    .line 61
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;)Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$RenameOnConflictListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mListener:Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$RenameOnConflictListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->newName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->clickOK()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mToast:Landroid/widget/Toast;

    return-object p1
.end method

.method private clickOK()V
    .locals 10

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 129
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mInputEditText:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 130
    .local v1, "dstName":Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->checkIfFilesNameIsValid(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 131
    const v5, 0x7f0b0075

    invoke-direct {p0, v5}, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->showToast(I)V

    .line 157
    :goto_0
    return-void

    .line 134
    :cond_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mExtension:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 135
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mFolderName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 136
    .local v4, "resPath":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    const/16 v8, 0xff

    if-le v7, v8, :cond_1

    .line 137
    const v5, 0x7f0b0077

    invoke-direct {p0, v5}, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->showToast(I)V

    goto :goto_0

    .line 140
    :cond_1
    const/4 v2, 0x0

    .line 141
    .local v2, "exists":Z
    iget v7, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mTargetFragmentId:I

    const/16 v8, 0x8

    if-eq v7, v8, :cond_2

    iget v7, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mTargetFragmentId:I

    const/16 v8, 0x1f

    if-ne v7, v8, :cond_3

    .line 142
    :cond_2
    new-instance v0, Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v0, v5}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    .line 143
    .local v0, "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    invoke-virtual {v0, v4}, Lcom/sec/android/app/myfiles/db/CacheDB;->getIfDropboxFileExist(Ljava/lang/String;)Z

    move-result v2

    .line 151
    .end local v0    # "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    :goto_1
    if-eqz v2, :cond_7

    .line 152
    const v5, 0x7f0b012a

    invoke-direct {p0, v5}, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->showToast(I)V

    goto :goto_0

    .line 144
    :cond_3
    iget v7, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mTargetFragmentId:I

    const/16 v8, 0xa

    if-ne v7, v8, :cond_4

    .line 145
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->dismiss()V

    goto :goto_0

    .line 148
    :cond_4
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 149
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v8

    iget v7, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mFormat:I

    const/16 v9, 0x3001

    if-ne v7, v9, :cond_5

    move v7, v5

    :goto_2
    if-ne v8, v7, :cond_6

    move v2, v5

    :goto_3
    goto :goto_1

    :cond_5
    move v7, v6

    goto :goto_2

    :cond_6
    move v2, v6

    goto :goto_3

    .line 154
    .end local v3    # "file":Ljava/io/File;
    :cond_7
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->newName:Ljava/lang/String;

    .line 155
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mHandler:Landroid/os/Handler;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0
.end method

.method private showToast(I)V
    .locals 2
    .param p1, "resId"    # I

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$4;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$4;-><init>(Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;I)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 171
    return-void
.end method


# virtual methods
.method public onResume()V
    .locals 2

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 87
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mOkButton:Landroid/widget/Button;

    .line 88
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mCancelButton:Landroid/widget/Button;

    .line 90
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->onResume()V

    .line 91
    return-void
.end method

.method public setRenameOnConflictListener(Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$RenameOnConflictListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$RenameOnConflictListener;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mListener:Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$RenameOnConflictListener;

    .line 82
    return-void
.end method

.method protected setupDialog(Landroid/app/AlertDialog$Builder;)V
    .locals 4
    .param p1, "builder"    # Landroid/app/AlertDialog$Builder;

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "current_item"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mPath:Ljava/lang/String;

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "dest_fragment_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mTargetFragmentId:I

    .line 97
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "format"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mFormat:I

    .line 98
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mPath:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 99
    .local v1, "srcFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mFolderName:Ljava/lang/String;

    .line 100
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mName:Ljava/lang/String;

    .line 101
    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mFormat:I

    const/16 v3, 0x3001

    if-eq v2, v3, :cond_0

    .line 102
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mName:Ljava/lang/String;

    const/16 v3, 0x2e

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 103
    .local v0, "dotIndex":I
    if-ltz v0, :cond_0

    .line 104
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mName:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mExtension:Ljava/lang/String;

    .line 105
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mName:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mName:Ljava/lang/String;

    .line 108
    .end local v0    # "dotIndex":I
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mInputEditText:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 109
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mInputEditText:Landroid/widget/EditText;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 110
    const v2, 0x7f0b0015

    new-instance v3, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$2;-><init>(Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;)V

    invoke-virtual {p1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 119
    const v2, 0x7f0b0017

    new-instance v3, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$3;-><init>(Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;)V

    invoke-virtual {p1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 126
    return-void
.end method

.method protected setupInputEditText(Landroid/widget/EditText;)V
    .locals 3
    .param p1, "editText"    # Landroid/widget/EditText;

    .prologue
    const/4 v2, 0x1

    .line 175
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 206
    :cond_0
    :goto_0
    return-void

    .line 178
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;->mPath:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getEditTextFilter(Landroid/content/Context;I)[Landroid/text/InputFilter;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 179
    invoke-virtual {p1, v2}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 180
    invoke-virtual {p1, v2}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 181
    const-string v0, "inputType=PredictionOff;inputType=filename;disableEmoticonInput=true"

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 184
    const v0, 0x84001

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setInputType(I)V

    .line 187
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment$5;-><init>(Lcom/sec/android/app/myfiles/fragment/RenameOnConflictFragment;)V

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    goto :goto_0
.end method
