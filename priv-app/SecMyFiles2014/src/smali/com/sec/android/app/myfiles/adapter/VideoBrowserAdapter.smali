.class public Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;
.super Lcom/sec/android/app/myfiles/adapter/AbsCategoryBrowserAdapter;
.source "VideoBrowserAdapter.java"


# instance fields
.field private previousListener:Landroid/view/View$OnHoverListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "c"    # Landroid/database/Cursor;
    .param p4, "flags"    # I
    .param p5, "navigation"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .prologue
    .line 50
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/myfiles/adapter/AbsCategoryBrowserAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;->previousListener:Landroid/view/View$OnHoverListener;

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;->mContext:Landroid/content/Context;

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-static {v0, v1}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    new-instance v0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 57
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;)Landroid/view/View$OnHoverListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;->previousListener:Landroid/view/View$OnHoverListener;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;Landroid/view/View$OnHoverListener;)Landroid/view/View$OnHoverListener;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;
    .param p1, "x1"    # Landroid/view/View$OnHoverListener;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;->previousListener:Landroid/view/View$OnHoverListener;

    return-object p1
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 12
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v11, 0x0

    .line 62
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/adapter/AbsCategoryBrowserAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 64
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;

    .line 67
    .local v8, "vh":Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    invoke-virtual {v9, v11}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 69
    iget v9, p0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;->mDisplayMode:I

    if-nez v9, :cond_9

    .line 71
    const-string v9, "_data"

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 73
    .local v2, "filePath":Ljava/lang/String;
    const/16 v9, 0x2f

    invoke-virtual {v2, v9}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    invoke-virtual {v2, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 75
    .local v1, "fileName":Ljava/lang/String;
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    if-eqz v9, :cond_0

    .line 76
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v9, v11}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 78
    :cond_0
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v9, :cond_1

    .line 79
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v9, v11}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 81
    :cond_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 84
    iget-object v9, p0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowFileExtensionStatus()Z

    move-result v9

    if-eqz v9, :cond_6

    .line 86
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v9, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 104
    :cond_2
    :goto_0
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v9, :cond_3

    .line 106
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v9, v11}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 109
    :cond_3
    invoke-virtual {p0, v8, v2}, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;->loadThumbnail(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;Ljava/lang/String;)V

    .line 112
    const-string v9, "_data"

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 113
    .local v5, "hoverFilePath":Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v4

    .line 114
    .local v4, "fileType":I
    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    .line 116
    .local v3, "filePosition":I
    new-instance v6, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter$1;

    invoke-direct {v6, p0, v4, v5, v3}, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter$1;-><init>(Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;ILjava/lang/String;I)V

    .line 204
    .local v6, "hoverListener":Landroid/view/View$OnHoverListener;
    iget v9, p0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;->mViewMode:I

    if-eqz v9, :cond_4

    iget v9, p0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;->mViewMode:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_8

    .line 205
    :cond_4
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    if-eqz v9, :cond_5

    .line 206
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v9, v6}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 227
    .end local v1    # "fileName":Ljava/lang/String;
    .end local v2    # "filePath":Ljava/lang/String;
    .end local v3    # "filePosition":I
    .end local v4    # "fileType":I
    .end local v5    # "hoverFilePath":Ljava/lang/String;
    .end local v6    # "hoverListener":Landroid/view/View$OnHoverListener;
    :cond_5
    :goto_1
    return-void

    .line 90
    .restart local v1    # "fileName":Ljava/lang/String;
    .restart local v2    # "filePath":Ljava/lang/String;
    :cond_6
    const/16 v9, 0x2e

    invoke-virtual {v1, v9}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v7

    .line 93
    .local v7, "lastDot":I
    if-lez v7, :cond_7

    .line 95
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    const/4 v10, 0x0

    invoke-virtual {v1, v10, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 99
    :cond_7
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v9, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 209
    .end local v7    # "lastDot":I
    .restart local v3    # "filePosition":I
    .restart local v4    # "fileType":I
    .restart local v5    # "hoverFilePath":Ljava/lang/String;
    .restart local v6    # "hoverListener":Landroid/view/View$OnHoverListener;
    :cond_8
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v9, :cond_5

    .line 210
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v9, v6}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    goto :goto_1

    .line 218
    .end local v1    # "fileName":Ljava/lang/String;
    .end local v2    # "filePath":Ljava/lang/String;
    .end local v3    # "filePosition":I
    .end local v4    # "fileType":I
    .end local v5    # "hoverFilePath":Ljava/lang/String;
    .end local v6    # "hoverListener":Landroid/view/View$OnHoverListener;
    :cond_9
    const-string v9, "bucket_display_name"

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 220
    .local v0, "bucketName":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_a

    .line 222
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 225
    :cond_a
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const v10, 0x7f0200af

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method

.method protected getContentType()I
    .locals 1

    .prologue
    .line 334
    const/4 v0, 0x3

    return v0
.end method
