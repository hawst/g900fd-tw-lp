.class Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1$1;
.super Ljava/lang/Object;
.source "FolderHoverPopup.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;->onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;)V
    .locals 0

    .prologue
    .line 346
    iput-object p1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1$1;->this$1:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 352
    const-wide/16 v2, 0x64

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 359
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1$1;->this$1:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    # getter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverMoved:Z
    invoke-static {v1}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$000(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 360
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1$1;->this$1:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mDismissFolderHover:Z

    .line 361
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1$1;->this$1:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    # getter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mChangeListThread:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$300(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1$1;->this$1:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    # getter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mChangeListThread:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$300(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 362
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1$1;->this$1:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    # getter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mChangeListThread:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$300(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;->interrupt()V

    .line 363
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1$1;->this$1:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mChangeListThread:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;
    invoke-static {v1, v2}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$302(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;)Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$ChangeListThread;

    .line 366
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1$1;->this$1:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->removeListChangeMessage()V

    .line 367
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1$1;->this$1:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->removeDelayedMessage()V

    .line 368
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1$1;->this$1:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->getHandler()Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 370
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1$1;->this$1:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mExitingHovering:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$202(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;Z)Z

    .line 371
    return-void

    .line 354
    :catch_0
    move-exception v0

    .line 356
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
