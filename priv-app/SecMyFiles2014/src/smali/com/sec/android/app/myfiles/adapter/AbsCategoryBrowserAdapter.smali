.class public abstract Lcom/sec/android/app/myfiles/adapter/AbsCategoryBrowserAdapter;
.super Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
.source "AbsCategoryBrowserAdapter.java"


# static fields
.field protected static final FOLDER:I = 0x1

.field protected static final LIST:I = 0x0

.field private static final MODULE:Ljava/lang/String; = "AbsCategoryBrowserAdapter"


# instance fields
.field protected mDisplayMode:I

.field public mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "c"    # Landroid/database/Cursor;
    .param p4, "flags"    # I
    .param p5, "navigation"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .prologue
    .line 47
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCategoryBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 48
    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 10
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/16 v8, 0x8

    const/4 v9, 0x0

    .line 59
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 61
    const-string v7, "_data"

    invoke-interface {p3, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {p3, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 63
    .local v2, "filePath":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 65
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_1

    .line 114
    :cond_0
    :goto_0
    return-void

    .line 68
    :cond_1
    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    .line 70
    .local v4, "lastmodified":J
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;

    .line 72
    .local v6, "vh":Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;
    if-nez v6, :cond_2

    .line 74
    const-string v7, "AbsCategoryBrowserAdapter"

    const-string v8, " bindView vh is null ! "

    invoke-static {v9, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 79
    :cond_2
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDivider:Landroid/widget/ImageView;

    if-eqz v7, :cond_3

    .line 81
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDivider:Landroid/widget/ImageView;

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 84
    :cond_3
    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxFolder(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 86
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mPersonalContentsIcon:Landroid/widget/ImageView;

    invoke-virtual {v7, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 93
    :goto_1
    iget v7, p0, Lcom/sec/android/app/myfiles/adapter/AbsCategoryBrowserAdapter;->mViewMode:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_0

    .line 98
    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/AbsCategoryBrowserAdapter;->mContext:Landroid/content/Context;

    const-string v8, "_size"

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-static {v7, v8, v9}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 100
    .local v0, "content":Ljava/lang/String;
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    if-eqz v7, :cond_4

    .line 102
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/AbsCategoryBrowserAdapter;->mContext:Landroid/content/Context;

    const-wide/16 v8, 0x3e8

    mul-long/2addr v8, v4

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/Utils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    .line 110
    .local v3, "lastmodifiedtime":Ljava/lang/String;
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    if-eqz v7, :cond_0

    .line 111
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    invoke-virtual {v7, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 90
    .end local v0    # "content":Ljava/lang/String;
    .end local v3    # "lastmodifiedtime":Ljava/lang/String;
    :cond_5
    iget-object v7, v6, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mPersonalContentsIcon:Landroid/widget/ImageView;

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method public setDisplayMode(I)V
    .locals 0
    .param p1, "displayMode"    # I

    .prologue
    .line 53
    iput p1, p0, Lcom/sec/android/app/myfiles/adapter/AbsCategoryBrowserAdapter;->mDisplayMode:I

    .line 54
    return-void
.end method
