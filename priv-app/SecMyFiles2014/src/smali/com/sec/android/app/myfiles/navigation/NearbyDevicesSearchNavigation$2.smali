.class Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$2;
.super Ljava/lang/Object;
.source "NearbyDevicesSearchNavigation.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->setOnScrollListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;)V
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$2;->this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 2
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 151
    sub-int v0, p4, p3

    add-int/lit8 v1, p2, 0x0

    if-gt v0, v1, :cond_0

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$2;->this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->isCurrentFolderRoot()Z

    move-result v0

    if-nez v0, :cond_1

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$2;->this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    invoke-virtual {v0, p4}, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->loadMoreItems(I)V

    .line 158
    :cond_0
    :goto_0
    return-void

    .line 155
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$2;->this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    invoke-virtual {v0, p4}, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->searchMoreItems(I)V

    goto :goto_0
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 164
    packed-switch p2, :pswitch_data_0

    .line 183
    :cond_0
    :goto_0
    return-void

    .line 166
    :pswitch_0
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader;->SCROLL_DONE:Z

    goto :goto_0

    .line 170
    :pswitch_1
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader;->SCROLL_DONE:Z

    .line 172
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$2;->this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    if-eqz v1, :cond_0

    .line 173
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$2;->this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 174
    .local v0, "currentFocus":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 175
    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    goto :goto_0

    .line 164
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
