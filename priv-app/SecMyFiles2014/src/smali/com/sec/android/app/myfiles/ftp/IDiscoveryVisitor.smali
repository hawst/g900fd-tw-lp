.class public interface abstract Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;
.super Ljava/lang/Object;
.source "IDiscoveryVisitor.java"


# virtual methods
.method public abstract abort()V
.end method

.method public abstract done()V
.end method

.method public abstract getResult()Landroid/os/Bundle;
.end method

.method public abstract setAbortHandler(Ljava/lang/Runnable;)V
.end method

.method public abstract visitFile(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)V
.end method

.method public abstract visitFolder(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)V
.end method
