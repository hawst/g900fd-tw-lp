.class Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment$1;
.super Ljava/lang/Object;
.source "BaiduSearchFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;)V
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment$1;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 21
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 165
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual/range {p1 .. p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v4

    move/from16 v0, p3

    invoke-interface {v4, v0}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/database/Cursor;

    .line 166
    .local v11, "cursor":Landroid/database/Cursor;
    const-string v4, "_id"

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 167
    .local v12, "cursorId":J
    const-string v4, "_data"

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 169
    .local v18, "path":Ljava/lang/String;
    const-string v4, "format"

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/16 v7, 0x3001

    if-ne v4, v7, :cond_1

    .line 172
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment$1;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    instance-of v4, v4, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v4, :cond_0

    .line 174
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 175
    .local v8, "argument":Landroid/os/Bundle;
    const-string v4, "FOLDERPATH"

    move-object/from16 v0, v18

    invoke-virtual {v8, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    const-string v4, "search_option_type"

    const/4 v7, 0x6

    invoke-virtual {v8, v4, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 177
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment$1;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/myfiles/MainActivity;

    const/16 v7, 0x1f

    invoke-virtual {v4, v7, v8}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    .line 178
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment$1;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->access$000(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setCurrentDirectory(Ljava/lang/String;)V

    .line 255
    .end local v8    # "argument":Landroid/os/Bundle;
    :cond_0
    :goto_0
    return-void

    .line 182
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment$1;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->getSelectionType()I

    move-result v4

    const/4 v7, 0x1

    if-ne v4, v7, :cond_2

    .line 184
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment$1;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->access$100(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;)Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v4, v12, v13, v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->multipleSelect(JLjava/lang/String;)V

    goto :goto_0

    .line 188
    :cond_2
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v15

    .line 189
    .local v15, "fileType":I
    invoke-static {v15}, Lcom/sec/android/app/myfiles/MediaFile;->isImageFileType(I)Z

    move-result v4

    if-nez v4, :cond_4

    .line 191
    invoke-static {v15}, Lcom/sec/android/app/myfiles/MediaFile;->isArchiveFileType(I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 192
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment$1;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->access$200(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    const v7, 0x7f0b00ec

    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-static {v4, v7, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 196
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment$1;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;

    const/16 v7, 0xb

    move-object/from16 v0, v18

    # invokes: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->startDownload(Ljava/lang/String;I)V
    invoke-static {v4, v0, v7}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->access$300(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;Ljava/lang/String;I)V

    .line 198
    :cond_4
    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Images;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    .line 199
    .local v3, "PhotoUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment$1;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->access$400(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 201
    .local v2, "cr":Landroid/content/ContentResolver;
    const-string v5, "cloud_server_id  =  ?   COLLATE NOCASE "

    .line 203
    .local v5, "where":Ljava/lang/String;
    const/4 v4, 0x1

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v18, v6, v4

    .line 204
    .local v6, "whereArgs":[Ljava/lang/String;
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v20, "_id"

    aput-object v20, v4, v7

    const/4 v7, 0x1

    const-string v20, "_data"

    aput-object v20, v4, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 206
    .local v10, "cloudDBcursor":Landroid/database/Cursor;
    if-eqz v10, :cond_0

    .line 207
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 208
    const-string v4, "cloudDBcursor"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "cloudDBcursor.getCount() : "

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v4

    const/4 v7, 0x1

    if-ne v4, v7, :cond_6

    .line 212
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 213
    const/4 v4, 0x0

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 214
    .local v9, "cid":I
    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v19

    .line 216
    .local v19, "view_uri":Landroid/net/Uri;
    const-string v4, "mime_type"

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 219
    .local v17, "mimetype":Ljava/lang/String;
    new-instance v4, Landroid/content/Intent;

    const-string v7, "android.intent.action.VIEW"

    invoke-direct {v4, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v16

    .line 223
    .local v16, "intent":Landroid/content/Intent;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment$1;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->access$500(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;)Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 251
    .end local v9    # "cid":I
    .end local v16    # "intent":Landroid/content/Intent;
    .end local v17    # "mimetype":Ljava/lang/String;
    .end local v19    # "view_uri":Landroid/net/Uri;
    :cond_5
    :goto_1
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 225
    .restart local v9    # "cid":I
    .restart local v16    # "intent":Landroid/content/Intent;
    .restart local v17    # "mimetype":Ljava/lang/String;
    .restart local v19    # "view_uri":Landroid/net/Uri;
    :catch_0
    move-exception v14

    .line 229
    .local v14, "e":Ljava/lang/Exception;
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 235
    .end local v9    # "cid":I
    .end local v14    # "e":Ljava/lang/Exception;
    .end local v16    # "intent":Landroid/content/Intent;
    .end local v17    # "mimetype":Ljava/lang/String;
    .end local v19    # "view_uri":Landroid/net/Uri;
    :cond_6
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-nez v4, :cond_5

    invoke-static {v15}, Lcom/sec/android/app/myfiles/MediaFile;->isImageFileType(I)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 243
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment$1;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;

    const/16 v7, 0xb

    move-object/from16 v0, v18

    # invokes: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->startDownload(Ljava/lang/String;I)V
    invoke-static {v4, v0, v7}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;->access$600(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;Ljava/lang/String;I)V

    goto :goto_1
.end method
