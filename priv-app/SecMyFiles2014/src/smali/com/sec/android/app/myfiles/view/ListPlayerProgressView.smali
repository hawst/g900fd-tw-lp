.class public Lcom/sec/android/app/myfiles/view/ListPlayerProgressView;
.super Landroid/widget/ImageView;
.source "ListPlayerProgressView.java"


# instance fields
.field private mProgress:F

.field private mProgressDrawable:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/ListPlayerProgressView;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    .line 30
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/myfiles/view/ListPlayerProgressView;->mProgress:F

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/ListPlayerProgressView;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    .line 30
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/myfiles/view/ListPlayerProgressView;->mProgress:F

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/ListPlayerProgressView;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    .line 30
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/myfiles/view/ListPlayerProgressView;->mProgress:F

    .line 42
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 54
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 55
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/ListPlayerProgressView;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 56
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 57
    .local v0, "r":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/ListPlayerProgressView;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    int-to-float v1, v1

    iget v2, v0, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/view/ListPlayerProgressView;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/myfiles/view/ListPlayerProgressView;->mProgress:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 59
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/ListPlayerProgressView;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 60
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/ListPlayerProgressView;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 62
    .end local v0    # "r":Landroid/graphics/Rect;
    :cond_0
    return-void
.end method

.method public setProgress(F)V
    .locals 0
    .param p1, "progress"    # F

    .prologue
    .line 45
    iput p1, p0, Lcom/sec/android/app/myfiles/view/ListPlayerProgressView;->mProgress:F

    .line 46
    return-void
.end method

.method public setProgressDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "d"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/app/myfiles/view/ListPlayerProgressView;->mProgressDrawable:Landroid/graphics/drawable/Drawable;

    .line 50
    return-void
.end method
