.class public Lcom/sec/android/app/myfiles/view/StorageUsageView;
.super Landroid/widget/LinearLayout;
.source "StorageUsageView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/view/StorageUsageView$CategoryInfo;
    }
.end annotation


# static fields
.field private static final MIDDLE_PAGE:I = 0x2

.field private static final MOST_LEFT_PAGE:I = 0x0

.field private static final MOST_RIGHT_PAGE:I = 0x1

.field private static final ONLY_ONE_PAGE:I = -0x1


# instance fields
.field private mCategoryInfos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/view/StorageUsageView$CategoryInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mCategoryItemContainer:Landroid/widget/LinearLayout;

.field private mLeftPageArrow:Landroid/widget/ImageView;

.field private mPageIndicator:Lcom/sec/android/app/myfiles/view/PageIndicator;

.field private mRightPageArrow:Landroid/widget/ImageView;

.field private mScreenState:I

.field private mSpaceInfoTextView:Landroid/widget/TextView;

.field private mStorageAvailableText:Ljava/lang/String;

.field private mStorageName:Landroid/widget/TextView;

.field private mStorageType:I

.field private mStorageUsageBar:Lcom/sec/android/app/myfiles/view/PercentageBar;

.field private mStorageUsageCategory:Landroid/widget/RelativeLayout;

.field private mTotalAvailableSpace:Ljava/lang/String;

.field private mTotalPages:I

.field private mTotalSpace:Ljava/lang/String;

.field private mUnderLine:Landroid/widget/LinearLayout;

.field private mUsageEntries:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 126
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 73
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mScreenState:I

    .line 129
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 133
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 73
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mScreenState:I

    .line 136
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 140
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 73
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mScreenState:I

    .line 143
    return-void
.end method

.method private loadComponent()V
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mUsageEntries:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 150
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mUsageEntries:Ljava/util/ArrayList;

    .line 153
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mCategoryInfos:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 155
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mCategoryInfos:Ljava/util/ArrayList;

    .line 158
    :cond_1
    const v0, 0x7f0f010e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageName:Landroid/widget/TextView;

    .line 160
    const v0, 0x7f0f0110

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/view/PageIndicator;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mPageIndicator:Lcom/sec/android/app/myfiles/view/PageIndicator;

    .line 162
    const v0, 0x7f0f0111

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/view/PercentageBar;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageUsageBar:Lcom/sec/android/app/myfiles/view/PercentageBar;

    .line 164
    const v0, 0x7f0f0112

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageUsageCategory:Landroid/widget/RelativeLayout;

    .line 166
    const v0, 0x7f0f0113

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mCategoryItemContainer:Landroid/widget/LinearLayout;

    .line 168
    const v0, 0x7f0f010f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mSpaceInfoTextView:Landroid/widget/TextView;

    .line 170
    const v0, 0x7f0f0114

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mUnderLine:Landroid/widget/LinearLayout;

    .line 172
    const v0, 0x7f0f008e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mLeftPageArrow:Landroid/widget/ImageView;

    .line 174
    const v0, 0x7f0f008f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mRightPageArrow:Landroid/widget/ImageView;

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageUsageBar:Lcom/sec/android/app/myfiles/view/PercentageBar;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mUsageEntries:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/PercentageBar;->setUsageEntries(Ljava/util/ArrayList;)V

    .line 177
    return-void
.end method

.method private updateCategoryItems()V
    .locals 9

    .prologue
    const/4 v8, -0x2

    .line 475
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090108

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v3, v6

    .line 477
    .local v3, "iconWidth":I
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f090109

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v2, v6

    .line 480
    .local v2, "iconHeight":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mCategoryInfos:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v0, v6, :cond_0

    .line 483
    new-instance v4, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v4, v6}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 485
    .local v4, "itemContainer":Landroid/widget/LinearLayout;
    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 487
    const/16 v6, 0x10

    invoke-virtual {v4, v6}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 491
    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v1, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 505
    .local v1, "icon":Landroid/widget/ImageView;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mCategoryInfos:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/myfiles/view/StorageUsageView$CategoryInfo;

    iget v6, v6, Lcom/sec/android/app/myfiles/view/StorageUsageView$CategoryInfo;->mIconResId:I

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 509
    new-instance v5, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 511
    .local v5, "name":Landroid/widget/TextView;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mCategoryInfos:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/myfiles/view/StorageUsageView$CategoryInfo;

    iget v6, v6, Lcom/sec/android/app/myfiles/view/StorageUsageView$CategoryInfo;->mTitle:I

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    .line 515
    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v6, v8, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v1, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 517
    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v6, v8, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 521
    iget-object v6, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mCategoryItemContainer:Landroid/widget/LinearLayout;

    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v7, v8, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v6, v4, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 480
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 524
    .end local v1    # "icon":Landroid/widget/ImageView;
    .end local v4    # "itemContainer":Landroid/widget/LinearLayout;
    .end local v5    # "name":Landroid/widget/TextView;
    :cond_0
    return-void
.end method

.method private updatePageArrow(I)V
    .locals 3
    .param p1, "pagePosition"    # I

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 427
    packed-switch p1, :pswitch_data_0

    .line 458
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mLeftPageArrow:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 460
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mRightPageArrow:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 464
    :goto_0
    return-void

    .line 431
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mLeftPageArrow:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 433
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mRightPageArrow:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 440
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mLeftPageArrow:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 442
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mRightPageArrow:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 449
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mLeftPageArrow:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 451
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mRightPageArrow:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 427
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public addCategoryEntry(IIIF)V
    .locals 4
    .param p1, "nameResId"    # I
    .param p2, "iconResId"    # I
    .param p3, "blockResId"    # I
    .param p4, "percentage"    # F

    .prologue
    .line 182
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 184
    .local v0, "res":Landroid/content/res/Resources;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mUsageEntries:Ljava/util/ArrayList;

    monitor-enter v2

    .line 188
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mUsageEntries:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-static {p4, v3}, Lcom/sec/android/app/myfiles/view/PercentageBar;->createUsageEntry(FLandroid/graphics/drawable/Drawable;)Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 189
    monitor-exit v2

    .line 190
    return-void

    .line 189
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public addCategoryEntry(Landroid/graphics/drawable/Drawable;F)V
    .locals 3
    .param p1, "blockColorDrawable"    # Landroid/graphics/drawable/Drawable;
    .param p2, "percentage"    # F

    .prologue
    .line 195
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mUsageEntries:Ljava/util/ArrayList;

    monitor-enter v1

    .line 197
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mUsageEntries:Ljava/util/ArrayList;

    invoke-static {p2, p1}, Lcom/sec/android/app/myfiles/view/PercentageBar;->createUsageEntry(FLandroid/graphics/drawable/Drawable;)Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 198
    monitor-exit v1

    .line 199
    return-void

    .line 198
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public clear()V
    .locals 2

    .prologue
    .line 204
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mUsageEntries:Ljava/util/ArrayList;

    monitor-enter v1

    .line 206
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mUsageEntries:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mCategoryInfos:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 209
    monitor-exit v1

    .line 210
    return-void

    .line 209
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public commit()V
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageUsageBar:Lcom/sec/android/app/myfiles/view/PercentageBar;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/view/PercentageBar;->drawPercentage()V

    .line 218
    return-void
.end method

.method public setCurrentPage(I)V
    .locals 0
    .param p1, "currentPage"    # I

    .prologue
    .line 422
    return-void
.end method

.method public setScreenState(I)V
    .locals 2
    .param p1, "screenState"    # I

    .prologue
    .line 312
    iput p1, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mScreenState:I

    .line 314
    const v0, 0x7f04004b

    .line 316
    .local v0, "layoutResId":I
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->removeAllViews()V

    .line 318
    iget v1, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mScreenState:I

    packed-switch v1, :pswitch_data_0

    .line 330
    :goto_0
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 332
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->loadComponent()V

    .line 333
    return-void

    .line 322
    :pswitch_1
    const v0, 0x7f04004b

    .line 323
    goto :goto_0

    .line 326
    :pswitch_2
    const v0, 0x7f04004c

    goto :goto_0

    .line 318
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public setStorageType(IZ)V
    .locals 5
    .param p1, "storageType"    # I
    .param p2, "isLastCircuit"    # Z

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 223
    iput p1, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageType:I

    .line 225
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 227
    .local v0, "res":Landroid/content/res/Resources;
    sparse-switch p1, :sswitch_data_0

    .line 271
    const v1, 0x7f0b0048

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageAvailableText:Ljava/lang/String;

    .line 275
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 277
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageName:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageAvailableText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 307
    :goto_1
    return-void

    .line 230
    :sswitch_0
    const v1, 0x7f0b0161

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageAvailableText:Ljava/lang/String;

    .line 232
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 234
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mUnderLine:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 240
    :sswitch_1
    const v1, 0x7f0b003e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageAvailableText:Ljava/lang/String;

    goto :goto_0

    .line 245
    :sswitch_2
    const v1, 0x7f0b003f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageAvailableText:Ljava/lang/String;

    goto :goto_0

    .line 254
    :sswitch_3
    const/16 v1, 0x603

    if-ne p1, v1, :cond_1

    .line 255
    const v1, 0x7f0b0042

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageAvailableText:Ljava/lang/String;

    .line 267
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageUsageCategory:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 256
    :cond_1
    const/16 v1, 0x604

    if-ne p1, v1, :cond_2

    .line 257
    const v1, 0x7f0b0043

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageAvailableText:Ljava/lang/String;

    goto :goto_2

    .line 258
    :cond_2
    const/16 v1, 0x605

    if-ne p1, v1, :cond_3

    .line 259
    const v1, 0x7f0b0044

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageAvailableText:Ljava/lang/String;

    goto :goto_2

    .line 260
    :cond_3
    const/16 v1, 0x606

    if-ne p1, v1, :cond_4

    .line 261
    const v1, 0x7f0b0045

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageAvailableText:Ljava/lang/String;

    goto :goto_2

    .line 262
    :cond_4
    const/16 v1, 0x607

    if-ne p1, v1, :cond_5

    .line 263
    const v1, 0x7f0b0046

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageAvailableText:Ljava/lang/String;

    goto :goto_2

    .line 265
    :cond_5
    const v1, 0x7f0b0047

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageAvailableText:Ljava/lang/String;

    goto :goto_2

    .line 281
    :cond_6
    const/16 v1, 0x101

    if-eq p1, v1, :cond_7

    if-eqz p1, :cond_7

    .line 283
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageAvailableText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageAvailableText:Ljava/lang/String;

    .line 296
    :goto_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageName:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageAvailableText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 298
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mSpaceInfoTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 300
    if-eqz p2, :cond_9

    .line 301
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageUsageCategory:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 285
    :cond_7
    if-nez p1, :cond_8

    .line 287
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageAvailableText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageAvailableText:Ljava/lang/String;

    goto :goto_3

    .line 291
    :cond_8
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageAvailableText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageAvailableText:Ljava/lang/String;

    goto :goto_3

    .line 303
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageUsageCategory:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 227
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x101 -> :sswitch_1
        0x202 -> :sswitch_2
        0x603 -> :sswitch_3
        0x604 -> :sswitch_3
        0x605 -> :sswitch_3
        0x606 -> :sswitch_3
        0x607 -> :sswitch_3
        0x608 -> :sswitch_3
    .end sparse-switch
.end method

.method public setTotalAvailableSpace(Ljava/lang/String;)V
    .locals 3
    .param p1, "totalAvailableSpace"    # Ljava/lang/String;

    .prologue
    .line 353
    iput-object p1, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mTotalAvailableSpace:Ljava/lang/String;

    .line 355
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mSpaceInfoTextView:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mTotalAvailableSpace:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mTotalSpace:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 357
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 359
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageName:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageAvailableText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mSpaceInfoTextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 361
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mSpaceInfoTextView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 363
    :cond_0
    return-void
.end method

.method public setTotalPages(I)V
    .locals 0
    .param p1, "pages"    # I

    .prologue
    .line 368
    iput p1, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mTotalPages:I

    .line 386
    return-void
.end method

.method public setTotalSpace(Ljava/lang/String;)V
    .locals 3
    .param p1, "totalSpace"    # Ljava/lang/String;

    .prologue
    .line 338
    iput-object p1, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mTotalSpace:Ljava/lang/String;

    .line 340
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mSpaceInfoTextView:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mTotalAvailableSpace:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mTotalSpace:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 342
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/StorageUsageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 344
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageName:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mStorageAvailableText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mSpaceInfoTextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 346
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/StorageUsageView;->mSpaceInfoTextView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 348
    :cond_0
    return-void
.end method
