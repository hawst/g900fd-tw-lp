.class public final enum Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;
.super Ljava/lang/Enum;
.source "ChangePasswordsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401c
    name = "Stage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

.field public static final enum ConfirmWrong:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

.field public static final enum Introduction:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

.field public static final enum NeedToConfirm:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;


# instance fields
.field public final buttonText:I

.field public final numericHint:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const v7, 0x7f0b0015

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 108
    new-instance v0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    const-string v1, "Introduction"

    const v2, 0x7f0b0113

    const v3, 0x7f0b0110

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;->Introduction:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    .line 111
    new-instance v0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    const-string v1, "NeedToConfirm"

    const v2, 0x7f0b0112

    invoke-direct {v0, v1, v5, v2, v7}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;->NeedToConfirm:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    .line 113
    new-instance v0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    const-string v1, "ConfirmWrong"

    const v2, 0x7f0b0114

    invoke-direct {v0, v1, v6, v2, v7}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;->ConfirmWrong:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    .line 107
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    sget-object v1, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;->Introduction:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;->NeedToConfirm:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;->ConfirmWrong:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;->$VALUES:[Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "hintInNumeric"    # I
    .param p4, "nextButtonText"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 115
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 116
    iput p3, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;->numericHint:I

    .line 117
    iput p4, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;->buttonText:I

    .line 118
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 107
    const-class v0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;
    .locals 1

    .prologue
    .line 107
    sget-object v0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;->$VALUES:[Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    invoke-virtual {v0}, [Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    return-object v0
.end method
