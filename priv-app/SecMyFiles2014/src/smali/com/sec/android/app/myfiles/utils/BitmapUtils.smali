.class public Lcom/sec/android/app/myfiles/utils/BitmapUtils;
.super Ljava/lang/Object;
.source "BitmapUtils.java"


# static fields
.field private static final MODULE:Ljava/lang/String; = "BitmapUtils"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static recycleOtherObject(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .locals 3
    .param p0, "srcBitmap"    # Landroid/graphics/Bitmap;
    .param p1, "resultBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 110
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 112
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 116
    :try_start_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    :cond_0
    :goto_0
    return-void

    .line 118
    :catch_0
    move-exception v0

    .line 120
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "b"    # Landroid/graphics/Bitmap;
    .param p1, "degrees"    # I

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 78
    const/4 v7, 0x0

    .line 80
    .local v7, "b2":Landroid/graphics/Bitmap;
    if-eqz p1, :cond_0

    if-eqz p0, :cond_0

    .line 82
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 84
    .local v5, "m":Landroid/graphics/Matrix;
    int-to-float v0, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-virtual {v5, v0, v1, v2}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 88
    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 90
    invoke-static {p0, v7}, Lcom/sec/android/app/myfiles/utils/BitmapUtils;->recycleOtherObject(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    .end local v5    # "m":Landroid/graphics/Matrix;
    :cond_0
    :goto_0
    return-object v7

    .line 98
    .restart local v5    # "m":Landroid/graphics/Matrix;
    :catch_0
    move-exception v8

    .line 101
    .local v8, "ex":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v8}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0
.end method

.method public static transform(Landroid/graphics/Matrix;Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    .locals 12
    .param p0, "scaler"    # Landroid/graphics/Matrix;
    .param p1, "source"    # Landroid/graphics/Bitmap;
    .param p2, "targetWidth"    # I
    .param p3, "targetHeight"    # I
    .param p4, "scaleUp"    # Z

    .prologue
    .line 31
    if-nez p1, :cond_0

    .line 33
    const/4 v0, 0x0

    const-string v1, "BitmapUtils"

    const-string v2, "BitmapUtils.transform source bitmap is null"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 35
    const/4 v9, 0x0

    .line 72
    :goto_0
    return-object v9

    .line 40
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 42
    int-to-float v0, p2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float v10, v0, v1

    .line 51
    .local v10, "scale":F
    :goto_1
    if-eqz p0, :cond_2

    .line 54
    invoke-virtual {p0, v10, v10}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 56
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p1

    move-object v5, p0

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 57
    .local v11, "tmpBmp":Landroid/graphics/Bitmap;
    invoke-static {p1, v11}, Lcom/sec/android/app/myfiles/utils/BitmapUtils;->recycleOtherObject(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 64
    :goto_2
    const/4 v0, 0x0

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int/2addr v1, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 66
    .local v7, "dx1":I
    const/4 v0, 0x0

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sub-int/2addr v1, p3

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v8

    .line 68
    .local v8, "dy1":I
    div-int/lit8 v0, v7, 0x2

    div-int/lit8 v1, v8, 0x2

    invoke-static {v11, v0, v1, p2, p3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 70
    .local v9, "retBmp":Landroid/graphics/Bitmap;
    invoke-static {v11, v9}, Lcom/sec/android/app/myfiles/utils/BitmapUtils;->recycleOtherObject(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 46
    .end local v7    # "dx1":I
    .end local v8    # "dy1":I
    .end local v9    # "retBmp":Landroid/graphics/Bitmap;
    .end local v10    # "scale":F
    .end local v11    # "tmpBmp":Landroid/graphics/Bitmap;
    :cond_1
    int-to-float v0, p3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float v10, v0, v1

    .restart local v10    # "scale":F
    goto :goto_1

    .line 61
    :cond_2
    move-object v11, p1

    .restart local v11    # "tmpBmp":Landroid/graphics/Bitmap;
    goto :goto_2
.end method
