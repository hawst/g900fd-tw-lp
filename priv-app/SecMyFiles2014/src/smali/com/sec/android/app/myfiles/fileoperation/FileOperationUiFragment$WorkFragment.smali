.class public Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;
.super Landroid/app/Fragment;
.source "FileOperationUiFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "WorkFragment"
.end annotation


# instance fields
.field mContainerId:I

.field mDstFolder:Ljava/lang/String;

.field mDstFtpParams:Ljava/lang/String;

.field mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

.field mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

.field mFileOperationUiFragmentTag:Ljava/lang/String;

.field mOperationType:I

.field mProgressView:Landroid/view/View;

.field mQuiting:Z

.field mReady:Z

.field mSourceFragmentId:I

.field mSrcFolder:Ljava/lang/String;

.field mSrcFtpParams:Ljava/lang/String;

.field mTargetDatas:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mTargetFragmentId:I

.field mZipType:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 520
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 540
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    .line 542
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mDstFtpParams:Ljava/lang/String;

    .line 544
    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mReady:Z

    .line 546
    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mQuiting:Z

    .line 1158
    new-instance v0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment$1;-><init>(Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;Landroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 520
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->updateShortcutInHomescreen(Landroid/os/Bundle;)V

    return-void
.end method

.method private updateShortcutInHomescreen(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "data"    # Landroid/os/Bundle;

    .prologue
    .line 1112
    if-nez p1, :cond_1

    .line 1151
    :cond_0
    return-void

    .line 1120
    :cond_1
    const-string v7, "done_file_list"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1122
    .local v0, "doneFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v7, "done_folder_list"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1124
    .local v1, "doneFolderList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v5

    .line 1126
    .local v5, "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    if-eqz v0, :cond_3

    .line 1128
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1130
    .local v2, "filePath":Ljava/lang/String;
    invoke-virtual {v5, v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSetHoemScreenPath(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1132
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-static {v7, v2}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteFileShortcutFromHome(Landroid/content/Context;Ljava/lang/String;)V

    .line 1135
    :cond_2
    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->removeThumbnailCache(Ljava/lang/String;)V

    goto :goto_0

    .line 1139
    .end local v2    # "filePath":Ljava/lang/String;
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_3
    if-eqz v1, :cond_0

    .line 1141
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .restart local v4    # "i$":Ljava/util/Iterator;
    :cond_4
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1143
    .local v3, "folderPath":Ljava/lang/String;
    invoke-virtual {v5, v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSetHoemScreenPath(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1145
    sget-char v7, Ljava/io/File;->separatorChar:C

    invoke-virtual {v3, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v3, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 1147
    .local v6, "shortcutName":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-static {v7, v3, v6}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteShortcutFromHome(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public cancel()V
    .locals 3

    .prologue
    .line 1100
    const/4 v0, 0x2

    const-string v1, "FileOperationUiFragment"

    const-string v2, "cancel() "

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1102
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    if-eqz v0, :cond_0

    .line 1104
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->cancelOperation()V

    .line 1107
    :cond_0
    return-void
.end method

.method public getBaseTargetFragmentId()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 636
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    .line 638
    .local v0, "f":Landroid/app/Fragment;
    if-nez v0, :cond_1

    .line 650
    :cond_0
    :goto_0
    return v2

    .line 643
    :cond_1
    invoke-virtual {v0}, Landroid/app/Fragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .line 645
    .local v1, "ff":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    if-eqz v1, :cond_0

    .line 650
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getFragmentId()I

    move-result v2

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 662
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 664
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    .line 668
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    if-nez v1, :cond_1

    .line 670
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getTargetRequestCode()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1065
    :cond_0
    :goto_0
    return-void

    .line 675
    :cond_1
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mOperationType:I

    packed-switch v1, :pswitch_data_0

    .line 1055
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    if-eqz v1, :cond_0

    .line 1060
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->setFileOperationListener(Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;)V

    .line 1062
    new-instance v0, Lcom/sec/android/app/myfiles/element/FileOperationParam;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFolder:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mDstFolder:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetDatas:Ljava/util/ArrayList;

    const/4 v4, 0x0

    iget v5, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mContainerId:I

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/element/FileOperationParam;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;II)V

    .line 1064
    .local v0, "param":Lcom/sec/android/app/myfiles/element/FileOperationParam;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 680
    .end local v0    # "param":Lcom/sec/android/app/myfiles/element/FileOperationParam;
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 682
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x1f

    if-ne v1, v2, :cond_5

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0x201

    if-eq v1, v2, :cond_3

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0x24

    if-ne v1, v2, :cond_5

    .line 685
    :cond_3
    const/4 v1, 0x0

    const-string v2, "FileOperationUiFragment"

    const-string v3, "Copy download from baidu to local"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 686
    new-instance v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;-><init>(Landroid/content/Context;ILandroid/view/View;I)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    .line 814
    :cond_4
    :goto_2
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x25

    if-ne v1, v2, :cond_2

    .line 816
    new-instance v1, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/myfiles/fileoperation/RemoteShareDownloadTask;-><init>(Landroid/content/Context;ILandroid/view/View;I)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto :goto_1

    .line 688
    :cond_5
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x201

    if-eq v1, v2, :cond_6

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_6

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_6

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_6

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x5

    if-eq v1, v2, :cond_6

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x7

    if-eq v1, v2, :cond_6

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x12

    if-eq v1, v2, :cond_6

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x28

    if-ne v1, v2, :cond_7

    :cond_6
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0x1f

    if-ne v1, v2, :cond_7

    .line 695
    const/4 v1, 0x0

    const-string v2, "FileOperationUiFragment"

    const-string v3, "Copy upload from local to baidu"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 696
    new-instance v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;-><init>(Landroid/content/Context;ILandroid/view/View;I)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto :goto_2

    .line 698
    :cond_7
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x1f

    if-ne v1, v2, :cond_8

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0x1f

    if-ne v1, v2, :cond_8

    .line 701
    const/4 v1, 0x0

    const-string v2, "FileOperationUiFragment"

    const-string v3, "Copy upload from baidu to baidu"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 702
    new-instance v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    iget v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;-><init>(Landroid/content/Context;ILandroid/view/View;II)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_2

    .line 704
    :cond_8
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_a

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0x201

    if-eq v1, v2, :cond_9

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0x24

    if-ne v1, v2, :cond_a

    .line 706
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 708
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationUiFragmentTag:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;-><init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_2

    .line 712
    :cond_a
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_c

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x201

    if-eq v1, v2, :cond_b

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_b

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_b

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x5

    if-eq v1, v2, :cond_b

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_b

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x7

    if-eq v1, v2, :cond_b

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x12

    if-eq v1, v2, :cond_b

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x28

    if-ne v1, v2, :cond_c

    .line 718
    :cond_b
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mDstFtpParams:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 720
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mDstFtpParams:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;-><init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_2

    .line 724
    :cond_c
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_e

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_e

    .line 726
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mDstFtpParams:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 728
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mDstFtpParams:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 730
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;-><init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_2

    .line 734
    :cond_d
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mDstFtpParams:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;-><init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_2

    .line 740
    :cond_e
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    if-nez v1, :cond_f

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0x1f

    if-ne v1, v2, :cond_f

    .line 742
    new-instance v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    iget v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;-><init>(Landroid/content/Context;ILandroid/view/View;II)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_2

    .line 746
    :cond_f
    new-instance v1, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, 0x0

    iget v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;-><init>(Landroid/content/Context;IILandroid/view/View;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_2

    .line 749
    :cond_10
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_12

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0x201

    if-eq v1, v2, :cond_11

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0x24

    if-ne v1, v2, :cond_12

    .line 752
    :cond_11
    new-instance v1, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;-><init>(Landroid/content/Context;ILandroid/view/View;I)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_2

    .line 754
    :cond_12
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x201

    if-eq v1, v2, :cond_13

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_13

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_13

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_13

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x5

    if-eq v1, v2, :cond_13

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x7

    if-eq v1, v2, :cond_13

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x12

    if-eq v1, v2, :cond_13

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x28

    if-ne v1, v2, :cond_14

    :cond_13
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_14

    .line 761
    new-instance v1, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;-><init>(Landroid/content/Context;ILandroid/view/View;I)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_2

    .line 763
    :cond_14
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_15

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_15

    .line 766
    new-instance v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    iget v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;-><init>(Landroid/content/Context;ILandroid/view/View;II)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_2

    .line 768
    :cond_15
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_17

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0x201

    if-eq v1, v2, :cond_16

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0x24

    if-ne v1, v2, :cond_17

    .line 770
    :cond_16
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 772
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationUiFragmentTag:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;-><init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_2

    .line 776
    :cond_17
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_19

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x201

    if-eq v1, v2, :cond_18

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_18

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_18

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x5

    if-eq v1, v2, :cond_18

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_18

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x7

    if-eq v1, v2, :cond_18

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x12

    if-eq v1, v2, :cond_18

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x28

    if-ne v1, v2, :cond_19

    .line 782
    :cond_18
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mDstFtpParams:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 784
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mDstFtpParams:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;-><init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_2

    .line 788
    :cond_19
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_1b

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_1b

    .line 790
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mDstFtpParams:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 792
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mDstFtpParams:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 794
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/ftp/operation/CopyFtpToSelfOperation;-><init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_2

    .line 798
    :cond_1a
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mDstFtpParams:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;-><init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_2

    .line 804
    :cond_1b
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    if-nez v1, :cond_1c

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_1c

    .line 806
    new-instance v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    iget v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;-><init>(Landroid/content/Context;ILandroid/view/View;II)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_2

    .line 810
    :cond_1c
    new-instance v1, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, 0x0

    iget v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;-><init>(Landroid/content/Context;IILandroid/view/View;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_2

    .line 825
    :pswitch_1
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v1

    if-eqz v1, :cond_29

    .line 827
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x1f

    if-ne v1, v2, :cond_1e

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0x201

    if-eq v1, v2, :cond_1d

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0x24

    if-ne v1, v2, :cond_1e

    .line 830
    :cond_1d
    new-instance v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    const/4 v5, 0x1

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;-><init>(Landroid/content/Context;ILandroid/view/View;I)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_1

    .line 832
    :cond_1e
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x201

    if-eq v1, v2, :cond_1f

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1f

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1f

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1f

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x5

    if-eq v1, v2, :cond_1f

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x7

    if-eq v1, v2, :cond_1f

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x12

    if-eq v1, v2, :cond_1f

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x28

    if-ne v1, v2, :cond_20

    :cond_1f
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0x1f

    if-ne v1, v2, :cond_20

    .line 840
    new-instance v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    const/4 v5, 0x1

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;-><init>(Landroid/content/Context;ILandroid/view/View;I)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_1

    .line 842
    :cond_20
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x1f

    if-ne v1, v2, :cond_21

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0x1f

    if-ne v1, v2, :cond_21

    .line 845
    new-instance v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    iget v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;-><init>(Landroid/content/Context;ILandroid/view/View;II)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_1

    .line 847
    :cond_21
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_23

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0x201

    if-eq v1, v2, :cond_22

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0x24

    if-ne v1, v2, :cond_23

    .line 850
    :cond_22
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 851
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x1

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationUiFragmentTag:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;-><init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_1

    .line 854
    :cond_23
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_25

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x201

    if-eq v1, v2, :cond_24

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_24

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_24

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x5

    if-eq v1, v2, :cond_24

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_24

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x7

    if-eq v1, v2, :cond_24

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x12

    if-eq v1, v2, :cond_24

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x28

    if-ne v1, v2, :cond_25

    .line 863
    :cond_24
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mDstFtpParams:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 864
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x1

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mDstFtpParams:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;-><init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_1

    .line 867
    :cond_25
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_27

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_27

    .line 869
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mDstFtpParams:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 871
    new-instance v9, Lcom/sec/android/app/myfiles/ftp/FTPParams;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    invoke-direct {v9, v1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;-><init>(Ljava/lang/String;)V

    .line 872
    .local v9, "srcParams":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    new-instance v7, Lcom/sec/android/app/myfiles/ftp/FTPParams;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mDstFtpParams:Ljava/lang/String;

    invoke-direct {v7, v1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;-><init>(Ljava/lang/String;)V

    .line 873
    .local v7, "dstParams":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v8

    .line 875
    .local v8, "srcLabel":Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 877
    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_26

    .line 878
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x1

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;-><init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_1

    .line 880
    :cond_26
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x1

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mDstFtpParams:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;-><init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_1

    .line 884
    .end local v7    # "dstParams":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .end local v8    # "srcLabel":Ljava/lang/String;
    .end local v9    # "srcParams":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    :cond_27
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    if-nez v1, :cond_28

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0x1f

    if-ne v1, v2, :cond_28

    .line 885
    new-instance v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    iget v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;-><init>(Landroid/content/Context;ILandroid/view/View;II)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_1

    .line 888
    :cond_28
    new-instance v1, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, 0x1

    iget v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;-><init>(Landroid/content/Context;IILandroid/view/View;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_1

    .line 893
    :cond_29
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_2b

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0x201

    if-eq v1, v2, :cond_2a

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0x24

    if-ne v1, v2, :cond_2b

    .line 896
    :cond_2a
    new-instance v1, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    const/4 v5, 0x1

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;-><init>(Landroid/content/Context;ILandroid/view/View;I)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_1

    .line 898
    :cond_2b
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x201

    if-eq v1, v2, :cond_2c

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2c

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_2c

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_2c

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x5

    if-eq v1, v2, :cond_2c

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x7

    if-eq v1, v2, :cond_2c

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x12

    if-eq v1, v2, :cond_2c

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x28

    if-ne v1, v2, :cond_2d

    :cond_2c
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_2d

    .line 905
    new-instance v1, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    const/4 v5, 0x1

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;-><init>(Landroid/content/Context;ILandroid/view/View;I)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_1

    .line 907
    :cond_2d
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_2e

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_2e

    .line 910
    new-instance v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    iget v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;-><init>(Landroid/content/Context;ILandroid/view/View;II)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_1

    .line 912
    :cond_2e
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_30

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0x201

    if-eq v1, v2, :cond_2f

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0x24

    if-ne v1, v2, :cond_30

    .line 914
    :cond_2f
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 916
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x1

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationUiFragmentTag:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;-><init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_1

    .line 920
    :cond_30
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_32

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x201

    if-eq v1, v2, :cond_31

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_31

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_31

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x5

    if-eq v1, v2, :cond_31

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_31

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/4 v2, 0x7

    if-eq v1, v2, :cond_31

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x12

    if-eq v1, v2, :cond_31

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0x28

    if-ne v1, v2, :cond_32

    .line 926
    :cond_31
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mDstFtpParams:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 928
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x1

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mDstFtpParams:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;-><init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_1

    .line 932
    :cond_32
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_34

    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_34

    .line 934
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mDstFtpParams:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 936
    new-instance v9, Lcom/sec/android/app/myfiles/ftp/FTPParams;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    invoke-direct {v9, v1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;-><init>(Ljava/lang/String;)V

    .line 938
    .restart local v9    # "srcParams":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    new-instance v7, Lcom/sec/android/app/myfiles/ftp/FTPParams;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mDstFtpParams:Ljava/lang/String;

    invoke-direct {v7, v1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;-><init>(Ljava/lang/String;)V

    .line 940
    .restart local v7    # "dstParams":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v8

    .line 942
    .restart local v8    # "srcLabel":Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 944
    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_33

    .line 946
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x1

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;-><init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_1

    .line 950
    :cond_33
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x1

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mDstFtpParams:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;-><init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_1

    .line 960
    .end local v7    # "dstParams":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .end local v8    # "srcLabel":Ljava/lang/String;
    .end local v9    # "srcParams":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    :cond_34
    new-instance v1, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, 0x1

    iget v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/myfiles/fileoperation/CopyOrMoveOperationTask;-><init>(Landroid/content/Context;IILandroid/view/View;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_1

    .line 968
    :pswitch_2
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    sparse-switch v1, :sswitch_data_0

    .line 985
    new-instance v1, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, 0x2

    iget v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/myfiles/fileoperation/DeleteOperationTask;-><init>(Landroid/content/Context;IILandroid/view/View;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_1

    .line 973
    :sswitch_0
    new-instance v1, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, 0x2

    iget v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/myfiles/fileoperation/RemoteDeleteOperationTask;-><init>(Landroid/content/Context;IILandroid/view/View;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_1

    .line 979
    :sswitch_1
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/DeleteFromFtpOperation;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x2

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/ftp/operation/DeleteFromFtpOperation;-><init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_1

    .line 995
    :pswitch_3
    new-instance v1, Lcom/sec/android/app/myfiles/fileoperation/CompressOperationTask;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, 0x3

    iget v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/myfiles/fileoperation/CompressOperationTask;-><init>(Landroid/content/Context;IILandroid/view/View;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_1

    .line 1002
    :pswitch_4
    new-instance v0, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x4

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    iget v5, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mZipType:I

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/fileoperation/ExtractOperationTask;-><init>(Landroid/content/Context;IILandroid/view/View;I)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_1

    .line 1008
    :pswitch_5
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    packed-switch v1, :pswitch_data_1

    goto/16 :goto_1

    .line 1012
    :pswitch_6
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/CreateDirectoryOnFtpOperation;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/16 v2, 0x8

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/ftp/operation/CreateDirectoryOnFtpOperation;-><init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_1

    .line 1026
    :pswitch_7
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    packed-switch v1, :pswitch_data_2

    goto/16 :goto_1

    .line 1030
    :pswitch_8
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/RenameItemOnFtpOperation;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x7

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/ftp/operation/RenameItemOnFtpOperation;-><init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_1

    .line 1046
    :pswitch_9
    new-instance v1, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mOperationType:I

    iget v4, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;-><init>(Landroid/content/Context;IILandroid/view/View;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    goto/16 :goto_1

    .line 675
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_9
        :pswitch_9
        :pswitch_7
        :pswitch_5
    .end packed-switch

    .line 968
    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_0
        0xa -> :sswitch_1
        0x1f -> :sswitch_0
    .end sparse-switch

    .line 1008
    :pswitch_data_1
    .packed-switch 0xa
        :pswitch_6
    .end packed-switch

    .line 1026
    :pswitch_data_2
    .packed-switch 0xa
        :pswitch_8
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v4, 0x12

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 585
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 589
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->setRetainInstance(Z)V

    .line 592
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "operation"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mOperationType:I

    .line 594
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "persona_item_id"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mContainerId:I

    .line 596
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "src_fragment_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    .line 598
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "dest_fragment_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetFragmentId:I

    .line 600
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "src_folder"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFolder:Ljava/lang/String;

    .line 602
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "dst_folder"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mDstFolder:Ljava/lang/String;

    .line 604
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "zip_type"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mZipType:I

    .line 606
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "file_operation_ui_fragment_tag"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationUiFragmentTag:Ljava/lang/String;

    .line 608
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "target_datas"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetDatas:Ljava/util/ArrayList;

    .line 610
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "src-ftp-param"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "src-ftp-param"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    .line 615
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "dst-ftp-param"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 617
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "dst-ftp-param"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mDstFtpParams:Ljava/lang/String;

    .line 620
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetDatas:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 631
    :cond_2
    :goto_0
    return-void

    .line 624
    :cond_3
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    if-ne v0, v4, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "/storage"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 626
    const/16 v0, 0x1f

    iput v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    goto :goto_0

    .line 628
    :cond_4
    iget v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    if-ne v0, v4, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "/storage"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 629
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSourceFragmentId:I

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 1075
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 1076
    return-void
.end method

.method public onDetach()V
    .locals 0

    .prologue
    .line 1088
    invoke-super {p0}, Landroid/app/Fragment;->onDetach()V

    .line 1089
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 566
    :try_start_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 572
    :goto_0
    return-void

    .line 568
    :catch_0
    move-exception v0

    .line 570
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public restart()V
    .locals 0

    .prologue
    .line 1096
    return-void
.end method

.method protected setProgressView(Landroid/view/View;)V
    .locals 0
    .param p1, "progressView"    # Landroid/view/View;

    .prologue
    .line 556
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mProgressView:Landroid/view/View;

    .line 557
    return-void
.end method
