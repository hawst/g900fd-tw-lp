.class public Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;
.super Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;
.source "CopyMoveFtpToLocalOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation$1;
    }
.end annotation


# instance fields
.field private mDoneCount:I

.field private final mDownload:Z

.field private final mMove:Z

.field private mParam:Lcom/sec/android/app/myfiles/element/FileOperationParam;

.field private mTotalCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "operation"    # I
    .param p3, "contentType"    # I
    .param p4, "progressView"    # Landroid/view/View;
    .param p5, "ftpHandlerParams"    # Ljava/lang/String;
    .param p6, "tag"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 39
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;-><init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;)V

    .line 217
    iput v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mTotalCount:I

    .line 219
    iput v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mDoneCount:I

    .line 41
    const-string v2, "cacheDownload"

    invoke-virtual {v2, p6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mDownload:Z

    .line 43
    if-ne p2, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mMove:Z

    .line 45
    return-void

    :cond_0
    move v0, v1

    .line 43
    goto :goto_0
.end method


# virtual methods
.method protected cancelOperation()V
    .locals 2

    .prologue
    .line 191
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v1}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->abortTransfer()V

    .line 193
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mDownload:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mParam:Lcom/sec/android/app/myfiles/element/FileOperationParam;

    if-eqz v1, :cond_0

    .line 195
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mParam:Lcom/sec/android/app/myfiles/element/FileOperationParam;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 199
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mParam:Lcom/sec/android/app/myfiles/element/FileOperationParam;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 201
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 203
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 211
    .end local v0    # "file":Ljava/io/File;
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->cancelOperation()V

    .line 213
    return-void
.end method

.method protected varargs doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;
    .locals 6
    .param p1, "args"    # [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .prologue
    const/4 v4, 0x0

    .line 57
    invoke-static {v4, p1}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->checkFileOperationParam(Z[Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/ftp/FTPErr;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 59
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    sget-object v3, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    if-ne v2, v3, :cond_0

    .line 61
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->checkConnection(Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;)Lcom/sec/android/app/myfiles/ftp/FTPErr;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 65
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    sget-object v3, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    if-ne v2, v3, :cond_3

    .line 67
    aget-object v0, p1, v4

    .line 69
    .local v0, "param":Lcom/sec/android/app/myfiles/element/FileOperationParam;
    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mParam:Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .line 71
    iget-object v2, v0, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    iget-object v3, v0, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mSrcFolder:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mDownload:Z

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->discoverFtpAndMakeLocalDirectories(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Z)Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;

    move-result-object v1

    .line 73
    .local v1, "res":Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    sget-object v3, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    if-ne v2, v3, :cond_3

    .line 75
    iget-object v2, v1, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mJobFile:Ljava/util/ArrayDeque;

    invoke-virtual {v2}, Ljava/util/ArrayDeque;->size()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mTotalCount:I

    .line 77
    iget-object v2, v1, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mJobFile:Ljava/util/ArrayDeque;

    iget-object v3, v0, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mSrcFolder:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mDownload:Z

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->downloadItems(Ljava/util/ArrayDeque;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mDoneCount:I

    .line 79
    iget v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mTotalCount:I

    iget v3, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mDoneCount:I

    if-eq v2, v3, :cond_1

    .line 81
    sget-object v2, Lcom/sec/android/app/myfiles/ftp/FTPErr;->PARTIALLY_DONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 85
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    sget-object v3, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mMove:Z

    if-eqz v2, :cond_2

    .line 87
    iget-object v2, v1, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mFolders:Ljava/util/Stack;

    iget-object v3, v1, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mFiles:Ljava/util/Stack;

    iget-object v4, v0, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mSrcFolder:Ljava/lang/String;

    iget-object v5, v0, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->deleteItemsOnFtp(Ljava/util/Stack;Ljava/util/Stack;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPErr;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 91
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    sget-object v3, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    if-ne v2, v3, :cond_3

    .line 93
    iget-object v2, v0, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mDoneCount:I

    .line 101
    .end local v0    # "param":Lcom/sec/android/app/myfiles/element/FileOperationParam;
    .end local v1    # "res":Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;
    :cond_3
    const/4 v2, 0x0

    return-object v2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 34
    check-cast p1, [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V
    .locals 4
    .param p1, "result"    # Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .prologue
    .line 108
    sget-object v1, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation$1;->$SwitchMap$com$sec$android$app$myfiles$ftp$FTPErr:[I

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPErr;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 158
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    if-eqz v1, :cond_0

    .line 160
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mDoneCount:I

    invoke-interface {v1, v2, v3}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;->onCancelled(Ljava/util/ArrayList;I)V

    .line 164
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mDownload:Z

    if-eqz v1, :cond_7

    .line 166
    const v1, 0x7f0b00ad

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->showToast(I)V

    .line 186
    :cond_1
    :goto_0
    return-void

    .line 112
    :pswitch_0
    iget v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mDoneCount:I

    if-lez v1, :cond_2

    .line 114
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mParam:Lcom/sec/android/app/myfiles/element/FileOperationParam;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/android/app/myfiles/utils/Utils;->sendScan(Landroid/content/Context;Ljava/lang/String;)V

    .line 118
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    if-eqz v1, :cond_4

    .line 120
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 122
    .local v0, "arg":Landroid/os/Bundle;
    const-string v2, "FILE_OPERATION"

    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mMove:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 124
    const-string v1, "src_fragment_id"

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 126
    const-string v1, "dest_fragment_id"

    const/16 v2, 0x201

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 128
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mParam:Lcom/sec/android/app/myfiles/element/FileOperationParam;

    if-eqz v1, :cond_3

    .line 130
    const-string v1, "target_folder"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mParam:Lcom/sec/android/app/myfiles/element/FileOperationParam;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    const-string v1, "target_datas"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mParam:Lcom/sec/android/app/myfiles/element/FileOperationParam;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 136
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    iget v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mDoneCount:I

    iget v3, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mDoneCount:I

    invoke-interface {v1, v2, v3, v0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;->onCompleted(IILandroid/os/Bundle;)V

    .line 140
    .end local v0    # "arg":Landroid/os/Bundle;
    :cond_4
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mDownload:Z

    if-nez v1, :cond_1

    .line 142
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mMove:Z

    if-eqz v1, :cond_6

    .line 144
    const v1, 0x7f0c0008

    iget v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mDoneCount:I

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->showToastPrurals(II)V

    goto :goto_0

    .line 122
    .restart local v0    # "arg":Landroid/os/Bundle;
    :cond_5
    const/4 v1, 0x0

    goto :goto_1

    .line 148
    .end local v0    # "arg":Landroid/os/Bundle;
    :cond_6
    const v1, 0x7f0c0005

    iget v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mDoneCount:I

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->showToastPrurals(II)V

    goto :goto_0

    .line 170
    :cond_7
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mMove:Z

    if-eqz v1, :cond_8

    .line 172
    const v1, 0x7f0b00cb

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->showToast(I)V

    goto :goto_0

    .line 176
    :cond_8
    const v1, 0x7f0b00c9

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->showToast(I)V

    goto :goto_0

    .line 108
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 34
    check-cast p1, Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V

    return-void
.end method

.method protected setupProgressView()V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToLocalOperation;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 52
    return-void
.end method
