.class Lcom/sec/android/app/myfiles/fragment/HistoryFragment$4;
.super Ljava/lang/Object;
.source "HistoryFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->showDeleteOfRetryDownloadDialog(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

.field final synthetic val$externalId:J


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;J)V
    .locals 0

    .prologue
    .line 690
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    iput-wide p2, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$4;->val$externalId:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v4, 0x0

    .line 698
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mDownloadManager:Landroid/app/DownloadManager;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->access$500(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;)Landroid/app/DownloadManager;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [J

    iget-wide v2, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$4;->val$externalId:J

    aput-wide v2, v1, v4

    invoke-virtual {v0, v1}, Landroid/app/DownloadManager;->restartDownload([J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 700
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b00ad

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 704
    :cond_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 705
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    # invokes: Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->refreshContent()V
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->access$600(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;)V

    .line 706
    return-void
.end method
