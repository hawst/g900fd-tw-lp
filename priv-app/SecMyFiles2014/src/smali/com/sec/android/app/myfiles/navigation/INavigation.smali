.class public interface abstract Lcom/sec/android/app/myfiles/navigation/INavigation;
.super Ljava/lang/Object;
.source "INavigation.java"


# virtual methods
.method public abstract getFilesInCurrentFolder()Landroid/database/Cursor;
.end method

.method public abstract getFilesInFolder(Ljava/lang/String;)Landroid/database/Cursor;
.end method

.method public abstract getFilesInFolder(Ljava/lang/String;I)Landroid/database/Cursor;
.end method

.method public abstract getRootPath()Ljava/lang/String;
.end method

.method public abstract goBack()Z
.end method

.method public abstract goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z
.end method

.method public abstract goToRoot(Ljava/lang/Boolean;)Z
.end method

.method public abstract goUp()Z
.end method

.method public abstract isCurrentFolderRoot()Z
.end method

.method public abstract onPause()V
.end method

.method public abstract onResume()V
.end method

.method public abstract setCurrentPath(Ljava/lang/String;)V
.end method
