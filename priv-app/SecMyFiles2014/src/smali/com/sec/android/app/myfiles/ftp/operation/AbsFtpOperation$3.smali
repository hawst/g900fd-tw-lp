.class Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$3;
.super Ljava/lang/Object;
.source "AbsFtpOperation.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->updateProgressBar(III)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;

.field final synthetic val$current:I

.field final synthetic val$progress:I

.field final synthetic val$total:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;III)V
    .locals 0

    .prologue
    .line 607
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$3;->this$0:Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;

    iput p2, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$3;->val$progress:I

    iput p3, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$3;->val$current:I

    iput p4, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$3;->val$total:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 612
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$3;->this$0:Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;

    # getter for: Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mPercentText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->access$000(Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$3;->val$progress:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 614
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$3;->this$0:Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;

    # getter for: Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mCountText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->access$100(Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$3;->val$current:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$3;->val$total:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 617
    return-void
.end method
