.class Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter$MyDataSetObserver;
.super Landroid/database/DataSetObserver;
.source "AbsCursorAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyDataSetObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;)V
    .locals 0

    .prologue
    .line 508
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter$MyDataSetObserver;->this$0:Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;
    .param p2, "x1"    # Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter$1;

    .prologue
    .line 508
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter$MyDataSetObserver;-><init>(Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;)V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 2

    .prologue
    .line 511
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter$MyDataSetObserver;->this$0:Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mDataValid:Z

    .line 512
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter$MyDataSetObserver;->this$0:Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->notifyDataSetChanged()V

    .line 513
    return-void
.end method

.method public onInvalidated()V
    .locals 2

    .prologue
    .line 517
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter$MyDataSetObserver;->this$0:Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mDataValid:Z

    .line 518
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter$MyDataSetObserver;->this$0:Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->notifyDataSetInvalidated()V

    .line 519
    return-void
.end method
