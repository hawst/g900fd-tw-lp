.class public interface abstract Lcom/sec/android/app/myfiles/fragment/ISelectModeFragment;
.super Ljava/lang/Object;
.source "ISelectModeFragment.java"


# virtual methods
.method public abstract finishDragMode()V
.end method

.method public abstract finishSelectMode()V
.end method

.method public abstract getSelectModeFrom()I
.end method

.method public abstract getSelectionType()I
.end method

.method public abstract isSelectMode()Z
.end method

.method public abstract setMainActionBar()V
.end method

.method public abstract setSelectModeActionBar()V
.end method

.method public abstract startDragMode(Landroid/view/View;Landroid/os/Bundle;)V
.end method

.method public abstract startSelectMode(III)V
.end method
