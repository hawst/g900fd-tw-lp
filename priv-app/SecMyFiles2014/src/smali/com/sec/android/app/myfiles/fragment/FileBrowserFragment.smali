.class public Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;
.source "FileBrowserFragment.java"


# static fields
.field private static final LOADING_FINISH:I = 0x1

.field private static final LOADING_START:I = 0x0

.field private static final MODULE:Ljava/lang/String; = "FileBrowserFragment"

.field private static final MOVETOFOLDER_FINISH:I = 0x2

.field private static final REQUEST_CREATE_FOLDER:I = 0x4

.field private static final REQUEST_EXTRACT:I = 0x6

.field private static final REQUEST_FILE_OPERATION:I = 0x2

.field private static final REQUEST_MOVE_TO_KNOX:I = 0x15

.field private static final REQUEST_REMOVE_FROM_KNOX:I = 0x16

.field private static final REQUEST_ZIP:I = 0x5


# instance fields
.field public mCurrentFolder:Ljava/lang/String;

.field private mExtractExecuteNormalMode:Z

.field private mFromFindoFile:Ljava/lang/String;

.field private mFromSelector:Z

.field private mIsBack:Z

.field private mIsCopyMove:Z

.field private mIsFolderFromFindo:Z

.field private mIsZip:Z

.field private mLastModifiedTime:J

.field private mLoadingHandler:Landroid/os/Handler;

.field private mModeFromFindo:Z

.field private mMtpRemoveReceiver:Landroid/content/BroadcastReceiver;

.field private mPathChangeListener:Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;

.field private mPathToMove:Ljava/lang/String;

.field private mPreviousfolder:Ljava/lang/String;

.field private mReceiverIsRegistered:Z

.field private mRefreshHandler:Landroid/os/Handler;

.field private mRemoveUSBStorage:Landroid/content/BroadcastReceiver;

.field private mSaveStartPathFromFindo:Ljava/lang/String;

.field private mScanFileReceiver:Landroid/content/BroadcastReceiver;

.field private mSecretReceiver:Landroid/content/BroadcastReceiver;

.field private mSecretReceiverFilter:Landroid/content/IntentFilter;

.field private mSelectedPathForExtract:Ljava/lang/String;

.field private mShortcutIndex:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 76
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;-><init>()V

    .line 93
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mFromFindoFile:Ljava/lang/String;

    .line 95
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mSaveStartPathFromFindo:Ljava/lang/String;

    .line 97
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mPreviousfolder:Ljava/lang/String;

    .line 101
    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mIsFolderFromFindo:Z

    .line 103
    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mModeFromFindo:Z

    .line 105
    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mReceiverIsRegistered:Z

    .line 109
    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mExtractExecuteNormalMode:Z

    .line 111
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mSelectedPathForExtract:Ljava/lang/String;

    .line 113
    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mFromSelector:Z

    .line 115
    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mIsCopyMove:Z

    .line 121
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mCurrentFolder:Ljava/lang/String;

    .line 123
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mPathToMove:Ljava/lang/String;

    .line 131
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mScanFileReceiver:Landroid/content/BroadcastReceiver;

    .line 150
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$2;-><init>(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mMtpRemoveReceiver:Landroid/content/BroadcastReceiver;

    .line 1634
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$8;-><init>(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mPathChangeListener:Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;

    .line 1793
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;-><init>(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mLoadingHandler:Landroid/os/Handler;

    .line 2147
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$17;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$17;-><init>(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mRefreshHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mRefreshHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->setPositionZero()V

    return-void
.end method

.method static synthetic access$102(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;
    .param p1, "x1"    # J

    .prologue
    .line 76
    iput-wide p1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mLastModifiedTime:J

    return-wide p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mPreviousfolder:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mPreviousfolder:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mLoadingHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/Boolean;

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->moveToFolder(Ljava/lang/String;Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic access$402(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mExtractExecuteNormalMode:Z

    return p1
.end method

.method static synthetic access$502(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mSelectedPathForExtract:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->createFolderForExtract(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mIsBack:Z

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mIsBack:Z

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mPathToMove:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mFromFindoFile:Ljava/lang/String;

    return-object v0
.end method

.method private createFolderForExtract(Ljava/lang/String;)Z
    .locals 9
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x6

    const/4 v6, 0x0

    .line 1202
    const/4 v3, 0x0

    .line 1206
    .local v3, "fileName":Ljava/lang/String;
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_0

    .line 1208
    invoke-static {p1}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v4

    .line 1210
    .local v4, "fileTypeInt":I
    invoke-static {v4}, Lcom/sec/android/app/myfiles/MediaFile;->isArchiveFileType(I)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1212
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v8, "Please select .zip file"

    invoke-static {v7, v8, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    .line 1278
    .end local v4    # "fileTypeInt":I
    :cond_0
    :goto_0
    return v6

    .line 1217
    .restart local v4    # "fileTypeInt":I
    :cond_1
    const/16 v7, 0x2f

    invoke-virtual {p1, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {p1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 1219
    const-string v7, ".zip"

    invoke-virtual {v3, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1221
    const/16 v7, 0x2e

    invoke-virtual {v3, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 1223
    .local v1, "dotIndex":I
    if-ltz v1, :cond_2

    .line 1225
    invoke-virtual {v3, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 1254
    .end local v1    # "dotIndex":I
    :cond_2
    new-instance v5, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    invoke-direct {v5}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;-><init>()V

    .line 1256
    .local v5, "inputName":Landroid/app/DialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1258
    .local v0, "argument":Landroid/os/Bundle;
    const-string v6, "title"

    const v7, 0x7f0b0074

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1260
    const-string v6, "kind_of_operation"

    invoke-virtual {v0, v6, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1262
    const-string v6, "current_item"

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1264
    const-string v6, "file_name"

    invoke-virtual {v0, v6, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1266
    const-string v6, "src_fragment_id"

    iget v7, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mFragmentId:I

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1268
    invoke-virtual {v5, v0}, Landroid/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 1270
    invoke-virtual {v5, p0, v8}, Landroid/app/DialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 1273
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "extract"

    invoke-virtual {v5, v6, v7}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1278
    :goto_1
    const/4 v6, 0x1

    goto :goto_0

    .line 1274
    :catch_0
    move-exception v2

    .line 1275
    .local v2, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v2}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1
.end method

.method private extractNotification(Landroid/content/Intent;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    .line 1111
    const-string v2, "used"

    invoke-virtual {p1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1113
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 1115
    .local v1, "uri":Landroid/net/Uri;
    if-eqz v1, :cond_1

    .line 1117
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getFilePathFromUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 1119
    .local v0, "path":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v2, :cond_0

    .line 1121
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getParentFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z

    .line 1124
    :cond_0
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mExtractExecuteNormalMode:Z

    .line 1126
    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mSelectedPathForExtract:Ljava/lang/String;

    .line 1128
    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->createFolderForExtract(Ljava/lang/String;)Z

    .line 1130
    .end local v0    # "path":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private moveToFolder(Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "isBack"    # Ljava/lang/Boolean;

    .prologue
    .line 1683
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->showWaitProgressDialog(Z)V

    .line 1685
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$10;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$10;-><init>(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1702
    return-void
.end method

.method private setExternalStorageReceiver()V
    .locals 3

    .prologue
    .line 1707
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1710
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1711
    const-string v1, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1712
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1714
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 1716
    new-instance v1, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$11;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$11;-><init>(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mRemoveUSBStorage:Landroid/content/BroadcastReceiver;

    .line 1777
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mRemoveUSBStorage:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1778
    return-void
.end method

.method private setPositionZero()V
    .locals 2

    .prologue
    .line 1667
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$9;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$9;-><init>(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->post(Ljava/lang/Runnable;)Z

    .line 1675
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setSelection(I)V

    .line 1678
    return-void
.end method

.method private setSecretReceiver()V
    .locals 3

    .prologue
    .line 2110
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mSecretReceiverFilter:Landroid/content/IntentFilter;

    .line 2112
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mSecretReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2113
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mSecretReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2116
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mSecretReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 2118
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$16;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$16;-><init>(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mSecretReceiver:Landroid/content/BroadcastReceiver;

    .line 2144
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mSecretReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mSecretReceiverFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2145
    return-void
.end method

.method private startCompress(Ljava/lang/String;)V
    .locals 11
    .param p1, "targetZipFile"    # Ljava/lang/String;

    .prologue
    .line 1164
    const/4 v8, 0x3

    invoke-static {v8}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(I)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v3

    .line 1168
    .local v3, "fragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    const/4 v8, 0x2

    :try_start_0
    invoke-virtual {v3, p0, v8}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setTargetFragment(Landroid/app/Fragment;I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1175
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v6

    .line 1177
    .local v6, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1179
    .local v7, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 1181
    .local v5, "selectedItem":Ljava/lang/Object;
    check-cast v5, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .end local v5    # "selectedItem":Ljava/lang/Object;
    iget-object v8, v5, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1170
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v6    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v7    # "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_0
    move-exception v1

    .line 1198
    :cond_0
    :goto_1
    return-void

    .line 1184
    .restart local v4    # "i$":Ljava/util/Iterator;
    .restart local v6    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .restart local v7    # "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 1186
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v8, "src_folder"

    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1188
    const-string v8, "dst_folder"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1190
    const-string v8, "target_datas"

    invoke-virtual {v0, v8, v7}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1192
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    .line 1194
    .local v2, "fm":Landroid/app/FragmentManager;
    if-eqz v2, :cond_0

    .line 1196
    const-string v8, "compress"

    invoke-virtual {v3, v2, v8}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private startExtract(Ljava/lang/String;I)V
    .locals 10
    .param p1, "targetFoler"    # Ljava/lang/String;
    .param p2, "zipType"    # I

    .prologue
    .line 1289
    const/4 v8, 0x4

    invoke-static {v8}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(I)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v3

    .line 1293
    .local v3, "fragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    const/4 v8, 0x2

    :try_start_0
    invoke-virtual {v3, p0, v8}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setTargetFragment(Landroid/app/Fragment;I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1300
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1302
    .local v7, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-boolean v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mExtractExecuteNormalMode:Z

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mSelectedPathForExtract:Ljava/lang/String;

    if-eqz v8, :cond_2

    .line 1304
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mSelectedPathForExtract:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1316
    :cond_0
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mExtractExecuteNormalMode:Z

    .line 1318
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 1320
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v8, "src_folder"

    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1322
    const-string v8, "dst_folder"

    invoke-virtual {v0, v8, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1324
    const-string v8, "zip_type"

    invoke-virtual {v0, v8, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1326
    const-string v8, "target_datas"

    invoke-virtual {v0, v8, v7}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1328
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    .line 1330
    .local v2, "fm":Landroid/app/FragmentManager;
    if-eqz v2, :cond_1

    .line 1332
    const-string v8, "extract"

    invoke-virtual {v3, v2, v8}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 1334
    .end local v0    # "arguments":Landroid/os/Bundle;
    .end local v2    # "fm":Landroid/app/FragmentManager;
    .end local v7    # "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    :goto_0
    return-void

    .line 1295
    :catch_0
    move-exception v1

    .line 1297
    .local v1, "ex":Ljava/lang/IllegalStateException;
    goto :goto_0

    .line 1308
    .end local v1    # "ex":Ljava/lang/IllegalStateException;
    .restart local v7    # "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v6

    .line 1310
    .local v6, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 1312
    .local v5, "selectedItem":Ljava/lang/Object;
    check-cast v5, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .end local v5    # "selectedItem":Ljava/lang/Object;
    iget-object v8, v5, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method public AsyncOpenCompleted()V
    .locals 0

    .prologue
    .line 1783
    return-void
.end method

.method public addCurrentFolderToShortcut()V
    .locals 9

    .prologue
    .line 1067
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->addCurrentFolderToShortcut()V

    .line 1068
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v0, :cond_0

    .line 1069
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mAddShortcutFromActionmode:Z

    if-eqz v0, :cond_7

    .line 1070
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_2

    .line 1071
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/element/BrowserItem;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 1073
    .local v1, "path":Ljava/lang/String;
    sget-char v0, Ljava/io/File;->separatorChar:C

    invoke-virtual {v1, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 1075
    .local v2, "shortcutName":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/16 v3, 0x1a

    const/4 v4, -0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/myfiles/utils/Utils;->addShortCut(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/drawable/Drawable;Z)V

    .line 1104
    .end local v1    # "path":Ljava/lang/String;
    .end local v2    # "shortcutName":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1105
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->finishSelectMode()V

    .line 1107
    :cond_1
    return-void

    .line 1077
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    .line 1078
    .local v8, "item":Ljava/lang/Object;
    check-cast v8, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .end local v8    # "item":Ljava/lang/Object;
    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 1080
    .restart local v1    # "path":Ljava/lang/String;
    sget-char v0, Ljava/io/File;->separatorChar:C

    invoke-virtual {v1, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 1082
    .restart local v2    # "shortcutName":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/16 v3, 0x1a

    const/4 v4, -0x1

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/myfiles/utils/Utils;->addShortCut(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/drawable/Drawable;Z)V

    goto :goto_1

    .line 1084
    .end local v1    # "path":Ljava/lang/String;
    .end local v2    # "shortcutName":Ljava/lang/String;
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v0

    sget v3, Lcom/sec/android/app/myfiles/utils/Utils;->mAddShortcutSuccess:I

    if-ne v0, v3, :cond_4

    .line 1085
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v3, 0x7f0b0060

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1087
    :cond_4
    sget v0, Lcom/sec/android/app/myfiles/utils/Utils;->mAddShortcutSuccess:I

    const/4 v3, 0x1

    if-ne v0, v3, :cond_5

    .line 1088
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v3, "1 shortcut added, Existing shortcuts were excluded"

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1089
    :cond_5
    sget v0, Lcom/sec/android/app/myfiles/utils/Utils;->mAddShortcutSuccess:I

    if-nez v0, :cond_6

    .line 1090
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v3, "Shortcuts already exist."

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1092
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget v4, Lcom/sec/android/app/myfiles/utils/Utils;->mAddShortcutSuccess:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " shortcuts added, Existing shortcuts were excluded"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1097
    .end local v7    # "i$":Ljava/util/Iterator;
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v1

    .line 1099
    .restart local v1    # "path":Ljava/lang/String;
    sget-char v0, Ljava/io/File;->separatorChar:C

    invoke-virtual {v1, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 1101
    .restart local v2    # "shortcutName":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/16 v3, 0x1a

    const/4 v4, -0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/myfiles/utils/Utils;->addShortCut(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/drawable/Drawable;Z)V

    goto/16 :goto_0
.end method

.method public addShortcutToHome()V
    .locals 5

    .prologue
    .line 2085
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->addShortcutToHome()V

    .line 2086
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v4, v4, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v4, :cond_1

    .line 2087
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v4, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v4, v4, Lcom/sec/android/app/myfiles/MainActivity;->mAddShortcutFromActionmode:Z

    if-eqz v4, :cond_0

    .line 2088
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 2089
    .local v1, "item":Ljava/lang/Object;
    check-cast v1, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .end local v1    # "item":Ljava/lang/Object;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 2091
    .local v2, "path":Ljava/lang/String;
    sget-char v4, Ljava/io/File;->separatorChar:C

    invoke-virtual {v2, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 2093
    .local v3, "shortcutName":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v4, v2, v3}, Lcom/sec/android/app/myfiles/utils/Utils;->addShortcutToHome(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2096
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "path":Ljava/lang/String;
    .end local v3    # "shortcutName":Ljava/lang/String;
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v2

    .line 2098
    .restart local v2    # "path":Ljava/lang/String;
    sget-char v4, Ljava/io/File;->separatorChar:C

    invoke-virtual {v2, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 2100
    .restart local v3    # "shortcutName":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v4, v2, v3}, Lcom/sec/android/app/myfiles/utils/Utils;->addShortcutToHome(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 2103
    .end local v2    # "path":Ljava/lang/String;
    .end local v3    # "shortcutName":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->isSelectMode()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2104
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->finishSelectMode()V

    .line 2106
    :cond_2
    return-void
.end method

.method public getOptionsMenuResId(Z)I
    .locals 1
    .param p1, "isSelectMode"    # Z

    .prologue
    .line 1340
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v0, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->isCurrentFolderRoot()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isCheckMountedStorage()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1342
    const v0, 0x7f0e001c

    .line 1359
    :goto_0
    return v0

    .line 1346
    :cond_0
    if-eqz p1, :cond_2

    .line 1348
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getSelectModeFrom()I

    move-result v0

    if-nez v0, :cond_1

    .line 1350
    const v0, 0x7f0e0005

    goto :goto_0

    .line 1354
    :cond_1
    const v0, 0x7f0e0018

    goto :goto_0

    .line 1359
    :cond_2
    const v0, 0x7f0e0004

    goto :goto_0
.end method

.method protected initTreeView()V
    .locals 2

    .prologue
    .line 1024
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->initTreeView()V

    .line 1032
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->setNavigation(Lcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 1034
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->setAutoExpand(Z)V

    .line 1036
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->setOnDropListener(Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;)V

    .line 1038
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1039
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    const/4 v3, -0x1

    .line 960
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 962
    packed-switch p1, :pswitch_data_0

    .line 1019
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 966
    :pswitch_1
    if-ne p2, v3, :cond_0

    .line 968
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "result_value"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".zip"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 971
    .local v2, "targetZipFile":Ljava/lang/String;
    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->startCompress(Ljava/lang/String;)V

    .line 972
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mIsZip:Z

    goto :goto_0

    .line 979
    .end local v2    # "targetZipFile":Ljava/lang/String;
    :pswitch_2
    if-ne p2, v3, :cond_0

    .line 981
    const-string v3, "result_value"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 984
    .local v0, "targetFolderName":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 987
    .local v1, "targetFolderPath":Ljava/lang/String;
    const/4 v3, 0x4

    invoke-direct {p0, v1, v3}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->startExtract(Ljava/lang/String;I)V

    goto :goto_0

    .line 994
    .end local v0    # "targetFolderName":Ljava/lang/String;
    .end local v1    # "targetFolderPath":Ljava/lang/String;
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->onRefresh()V

    goto :goto_0

    .line 1001
    :pswitch_4
    if-ne p2, v3, :cond_0

    .line 1002
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mIsCopyMove:Z

    goto :goto_0

    .line 1009
    :pswitch_5
    if-eqz p3, :cond_1

    .line 1010
    const-string v3, "target_folder"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mPathToMove:Ljava/lang/String;

    .line 1012
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mPathToMove:Ljava/lang/String;

    if-nez v3, :cond_0

    .line 1014
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->onRefresh()V

    goto :goto_0

    .line 962
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onBackPressed()Z
    .locals 14

    .prologue
    const/4 v13, -0x1

    const v12, 0x7f0f0148

    const/4 v11, 0x2

    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 554
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->initFileObserver()V

    .line 556
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-nez v8, :cond_1

    .line 558
    const-string v7, "FileBrowserFragment"

    const-string v8, "mNavigation == null"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    move v7, v6

    .line 742
    :cond_0
    :goto_0
    return v7

    .line 563
    :cond_1
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mPreviousfolder:Ljava/lang/String;

    .line 574
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getHistoricalPathWithoutRemoving()Ljava/lang/String;

    move-result-object v2

    .line 576
    .local v2, "historicalPath":Ljava/lang/String;
    sget-object v8, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mPreviousfolder:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    if-eqz v2, :cond_2

    sget-object v8, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v2, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 579
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView()V

    .line 580
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->clearHistoricalPaths()V

    .line 584
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getSelectModeFrom()I

    move-result v8

    if-eq v8, v11, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getSelectModeFrom()I

    move-result v8

    if-nez v8, :cond_4

    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->isSelectMode()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 587
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->finishSelectMode()V

    goto :goto_0

    .line 589
    :cond_4
    iget-boolean v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mIsHomeBtnSelected:Z

    if-nez v8, :cond_d

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->isSelectMode()Z

    move-result v8

    if-nez v8, :cond_d

    .line 590
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goBack()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 592
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v4

    .line 594
    .local v4, "path":Ljava/lang/String;
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-direct {p0, v4, v8}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->moveToFolder(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 596
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isCheckMountedStorage()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 598
    const/4 v3, 0x0

    .line 600
    .local v3, "menuItem":Landroid/view/MenuItem;
    sget-object v8, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    if-eqz v8, :cond_0

    sget-object v8, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v8, v12}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 602
    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->isRootFolder(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_5

    move v6, v7

    :cond_5
    invoke-interface {v3, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 606
    .end local v3    # "menuItem":Landroid/view/MenuItem;
    .end local v4    # "path":Ljava/lang/String;
    :cond_6
    iget-boolean v6, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mModeFromFindo:Z

    if-eqz v6, :cond_7

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mPreviousfolder:Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mSaveStartPathFromFindo:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 608
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v6, :cond_0

    .line 609
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/AbsMainActivity;->finish()V

    goto/16 :goto_0

    .line 614
    :cond_7
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    iget-object v4, v6, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mWrongUpPath:Ljava/lang/String;

    .line 615
    .restart local v4    # "path":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isCheckMountedStorage()Z

    move-result v6

    if-nez v6, :cond_8

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->isInExternalSdStorage(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    sget-boolean v6, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v6, :cond_9

    :cond_8
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isCheckMountedStorage()Z

    move-result v6

    if-eqz v6, :cond_a

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->isUsbStorageFolder(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_a

    sget-boolean v6, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-nez v6, :cond_a

    .line 618
    :cond_9
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getHistoricalPath()Ljava/lang/String;

    move-result-object v5

    .line 619
    .local v5, "prePath":Ljava/lang/String;
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-direct {p0, v5, v6}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->moveToFolder(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/16 :goto_0

    .line 620
    .end local v5    # "prePath":Ljava/lang/String;
    :cond_a
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isCheckMountedStorage()Z

    move-result v6

    if-nez v6, :cond_b

    .line 621
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView()V

    goto/16 :goto_0

    .line 623
    :cond_b
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mPreviousfolder:Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_c

    .line 624
    const-string v6, "/storage"

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-direct {p0, v6, v8}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->moveToFolder(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/16 :goto_0

    .line 626
    :cond_c
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView()V

    goto/16 :goto_0

    .line 632
    .end local v4    # "path":Ljava/lang/String;
    :cond_d
    iget-boolean v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mIsHomeBtnSelected:Z

    if-nez v8, :cond_e

    iget-boolean v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mIsSelector:Z

    if-nez v8, :cond_14

    .line 633
    :cond_e
    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mIsHomeBtnSelected:Z

    .line 635
    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mIsSelector:Z

    .line 637
    iget-boolean v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mIsShortcutFolder:Z

    if-eqz v8, :cond_11

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mShortcutRootFolder:Ljava/lang/String;

    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_11

    .line 640
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getSelectModeFrom()I

    move-result v8

    if-eqz v8, :cond_f

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getSelectModeFrom()I

    move-result v8

    if-eq v8, v11, :cond_f

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getSelectModeFrom()I

    move-result v8

    if-ne v8, v13, :cond_10

    .line 644
    :cond_f
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 645
    .local v1, "b":Landroid/os/Bundle;
    const-string v6, "shortcut_working_index_intent_key"

    iget v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mShortcutIndex:I

    invoke-virtual {v1, v6, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 646
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v6, v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView(Landroid/os/Bundle;)V

    .line 647
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->clearHistoricalPaths()V

    goto/16 :goto_0

    .line 652
    .end local v1    # "b":Landroid/os/Bundle;
    :cond_10
    const-string v7, "FileBrowserFragment"

    const-string v8, " failed to go up"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    move v7, v6

    .line 654
    goto/16 :goto_0

    .line 657
    :cond_11
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goUp()Z

    move-result v8

    if-eqz v8, :cond_18

    .line 660
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->isSelectMode()Z

    move-result v8

    if-eqz v8, :cond_12

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v8, :cond_12

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v8

    if-lez v8, :cond_12

    .line 662
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->unselectAllItem()V

    .line 665
    :cond_12
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v4

    .line 667
    .restart local v4    # "path":Ljava/lang/String;
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-direct {p0, v4, v8}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->moveToFolder(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 670
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isCheckMountedStorage()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 672
    const/4 v3, 0x0

    .line 674
    .restart local v3    # "menuItem":Landroid/view/MenuItem;
    sget-object v8, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    if-eqz v8, :cond_0

    sget-object v8, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v8, v12}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 676
    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->isRootFolder(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_13

    move v6, v7

    :cond_13
    invoke-interface {v3, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 683
    .end local v3    # "menuItem":Landroid/view/MenuItem;
    .end local v4    # "path":Ljava/lang/String;
    :cond_14
    iget-boolean v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mIsSelector:Z

    if-eqz v8, :cond_18

    .line 684
    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mIsSelector:Z

    .line 686
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "ISSELECTOR"

    iget-boolean v10, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mIsSelector:Z

    invoke-virtual {v8, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 688
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "RETURNPATH"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_16

    .line 690
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 691
    .local v0, "argument":Landroid/os/Bundle;
    const-string v6, "FOLDERPATH"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "RETURNPATH"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v6, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 692
    const-string v6, "RETURNPATH"

    const/4 v8, 0x0

    invoke-virtual {v0, v6, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 693
    const-string v6, "src_fragment_id"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "src_fragment_id"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v0, v6, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 694
    const-string v6, "dest_fragment_id"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "dest_fragment_id"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v0, v6, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 695
    const-string v6, "from_back"

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 697
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v8, "src-ftp-param"

    invoke-virtual {v6, v8}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_15

    .line 699
    const-string v6, "src-ftp-param"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "src-ftp-param"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v6, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 701
    :cond_15
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->clearHistoricalPaths()V

    .line 702
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mCallback:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$onCopyMoveSelectedListener;

    const-string v8, "FOLDERPATH"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v0, v8}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$onCopyMoveSelectedListener;->onCopyMoveSelected(Landroid/os/Bundle;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 705
    .end local v0    # "argument":Landroid/os/Bundle;
    :cond_16
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goBack()Z

    move-result v8

    if-eqz v8, :cond_18

    .line 707
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v4

    .line 709
    .restart local v4    # "path":Ljava/lang/String;
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-direct {p0, v4, v8}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->moveToFolder(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 711
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isCheckMountedStorage()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 713
    const/4 v3, 0x0

    .line 715
    .restart local v3    # "menuItem":Landroid/view/MenuItem;
    sget-object v8, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    if-eqz v8, :cond_0

    sget-object v8, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v8, v12}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 717
    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->isRootFolder(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_17

    move v6, v7

    :cond_17
    invoke-interface {v3, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 724
    .end local v3    # "menuItem":Landroid/view/MenuItem;
    .end local v4    # "path":Ljava/lang/String;
    :cond_18
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getSelectModeFrom()I

    move-result v8

    if-eqz v8, :cond_19

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getSelectModeFrom()I

    move-result v8

    if-eq v8, v11, :cond_19

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getSelectModeFrom()I

    move-result v8

    if-ne v8, v13, :cond_1a

    .line 727
    :cond_19
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->clearHistoricalPaths()V

    .line 728
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView()V

    goto/16 :goto_0

    .line 730
    :cond_1a
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getSelectModeFrom()I

    move-result v8

    if-ne v8, v7, :cond_1b

    .line 732
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/AbsMainActivity;->finish()V

    goto/16 :goto_0

    .line 736
    :cond_1b
    const-string v7, "FileBrowserFragment"

    const-string v8, " failed to go up"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    move v7, v6

    .line 738
    goto/16 :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1933
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1934
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mRunFrom:I

    const/16 v1, 0x15

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getSelectionType()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 1935
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->selectmodeActionbar()V

    .line 1937
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 168
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onCreate(Landroid/os/Bundle;)V

    .line 170
    const/16 v0, 0x201

    iput v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mCategoryType:I

    .line 172
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 174
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->setExternalStorageReceiver()V

    .line 175
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->setSecretReceiver()V

    .line 178
    :cond_0
    if-eqz p1, :cond_1

    .line 180
    const-string v0, "last_modified_time"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mLastModifiedTime:J

    .line 182
    :cond_1
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mRunFrom:I

    const/16 v1, 0x15

    if-ne v0, v1, :cond_3

    .line 183
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    if-eqz v0, :cond_4

    .line 184
    :cond_2
    const-string v0, "/storage"

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mCurrentFolder:Ljava/lang/String;

    .line 189
    :cond_3
    :goto_0
    return-void

    .line 186
    :cond_4
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mCurrentFolder:Ljava/lang/String;

    goto :goto_0
.end method

.method protected onCreateDetailFragmentListener()Landroid/app/DialogFragment;
    .locals 1

    .prologue
    .line 1623
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/DetailFragment;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/fragment/DetailFragment;-><init>()V

    return-object v0
.end method

.method protected onCreateFileOperationListener()Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;
    .locals 1

    .prologue
    .line 1603
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$7;-><init>(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)V

    return-object v0
.end method

.method protected onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 1368
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;-><init>(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)V

    return-object v0
.end method

.method protected onCreateItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;
    .locals 1

    .prologue
    .line 1524
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$6;-><init>(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)V

    return-object v0
.end method

.method protected onCreateMultipleDetailFragmentListener()Landroid/app/DialogFragment;
    .locals 1

    .prologue
    .line 1630
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/MultipleDetailFragment;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/fragment/MultipleDetailFragment;-><init>()V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 13
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 195
    invoke-super/range {p0 .. p3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v8

    .line 197
    .local v8, "view":Landroid/view/View;
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mPathChangeListener:Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;

    invoke-virtual {v9, v10}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setOnPathChangeListener(Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;)V

    .line 200
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 202
    .local v0, "argument":Landroid/os/Bundle;
    const/4 v6, 0x0

    .line 204
    .local v6, "startFolder":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 206
    const-string v9, "FOLDERPATH"

    invoke-virtual {v0, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 208
    if-eqz v6, :cond_0

    .line 210
    iput-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mCurrentFolder:Ljava/lang/String;

    .line 214
    :cond_0
    const-string v9, "shortcut_working_index_intent_key"

    const/4 v10, 0x0

    invoke-virtual {v0, v9, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mShortcutIndex:I

    .line 216
    const-string v9, "from_findo_is_folder"

    invoke-virtual {v0, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mIsFolderFromFindo:Z

    .line 218
    const-string v9, "from_findo_file_position"

    invoke-virtual {v0, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mFromFindoFile:Ljava/lang/String;

    .line 220
    const-string v9, "hide_storage"

    invoke-virtual {v0, v9}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v2

    .line 222
    .local v2, "hideStorages":[I
    if-eqz v2, :cond_1

    .line 224
    move-object v1, v2

    .local v1, "arr$":[I
    array-length v5, v1

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v5, :cond_1

    aget v7, v1, v3

    .line 226
    .local v7, "storage":I
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v9, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    invoke-virtual {v9, v7}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->addHideStorage(I)V

    .line 224
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 231
    .end local v1    # "arr$":[I
    .end local v2    # "hideStorages":[I
    .end local v3    # "i$":I
    .end local v5    # "len$":I
    .end local v7    # "storage":I
    :cond_1
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_6

    .line 233
    const/4 v4, 0x1

    .line 235
    .local v4, "isBack":Z
    if-eqz v0, :cond_2

    .line 236
    const-string v9, "is_from_search_intent_key"

    invoke-virtual {v0, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_5

    const/4 v4, 0x1

    .line 240
    :cond_2
    :goto_1
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v9, v6, v10}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 242
    const/4 v9, 0x0

    const-string v10, "FileBrowserFragment"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "fail to move the startFolder : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "so go to the root"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 244
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mFromFindoFile:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 245
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mModeFromFindo:Z

    .line 248
    :cond_3
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goToRoot(Ljava/lang/Boolean;)Z

    .line 250
    iget-boolean v9, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mModeFromFindo:Z

    if-eqz v9, :cond_4

    .line 251
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mSaveStartPathFromFindo:Ljava/lang/String;

    .line 261
    .end local v4    # "isBack":Z
    :cond_4
    :goto_2
    return-object v8

    .line 236
    .restart local v4    # "isBack":Z
    :cond_5
    const/4 v4, 0x0

    goto :goto_1

    .line 257
    .end local v4    # "isBack":Z
    :cond_6
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goToRoot(Ljava/lang/Boolean;)Z

    goto :goto_2
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 485
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onDestroy()V

    .line 487
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 489
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mRemoveUSBStorage:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 492
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mSecretReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 494
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mSecretReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 499
    :cond_1
    :goto_0
    return-void

    .line 495
    :catch_0
    move-exception v0

    .line 496
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onDrop(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 7
    .param p1, "targetFolder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    const/4 v5, 0x1

    .line 419
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v6, "activity"

    invoke-virtual {v4, v6}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 421
    .local v0, "activityManager":Landroid/app/ActivityManager;
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v1

    .line 423
    .local v1, "infos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-nez v1, :cond_0

    .line 453
    :goto_0
    return-void

    .line 426
    :cond_0
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v4, v4, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const-string v6, "com.sec.android.app.myfiles"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v4, v4, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const-string v6, "com.sec.android.app.myfiles"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v2, v5

    .line 429
    .local v2, "move":Z
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDragSourceFragmentId()I

    move-result v3

    .line 431
    .local v3, "srcFragment":I
    packed-switch v3, :pswitch_data_0

    .line 449
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onDrop(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 435
    :pswitch_0
    if-eqz v2, :cond_2

    .line 437
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getFragmentId()I

    move-result v4

    invoke-virtual {p0, v4, p1, p2, v5}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->handleOnDropFromFtp(ILjava/lang/String;Ljava/util/ArrayList;Z)V

    goto :goto_0

    .line 441
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onDrop(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 431
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 23
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 749
    if-nez p1, :cond_0

    .line 750
    const/16 v20, 0x0

    .line 953
    :goto_0
    return v20

    .line 752
    :cond_0
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v20

    packed-switch v20, :pswitch_data_0

    .line 953
    :pswitch_0
    invoke-super/range {p0 .. p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v20

    goto :goto_0

    .line 756
    :pswitch_1
    const/16 v20, 0x1

    const/16 v21, -0x1

    const/16 v22, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->startSelectMode(III)V

    .line 758
    const/16 v20, 0x1

    goto :goto_0

    .line 762
    :pswitch_2
    const/16 v20, 0x1

    const/16 v21, -0x1

    const/16 v22, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->startSelectMode(III)V

    .line 764
    const/16 v20, 0x1

    goto :goto_0

    .line 767
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_1

    .line 768
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->addCurrentFolderToShortcut()V

    .line 772
    :goto_1
    const/16 v20, 0x1

    goto :goto_0

    .line 770
    :cond_1
    const/16 v20, 0x8

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->showDialog(I)V

    goto :goto_1

    .line 776
    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->isResumed()Z

    move-result v20

    if-nez v20, :cond_2

    .line 777
    const/16 v20, 0x1

    goto :goto_0

    .line 779
    :cond_2
    new-instance v5, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    invoke-direct {v5}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;-><init>()V

    .line 781
    .local v5, "createFolder":Landroid/app/DialogFragment;
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 783
    .local v4, "argument":Landroid/os/Bundle;
    const-string v20, "title"

    const v21, 0x7f0b0019

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 785
    const-string v20, "kind_of_operation"

    const/16 v21, 0x4

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 787
    const-string v20, "current_folder"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 789
    const-string v20, "src_fragment_id"

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mFragmentId:I

    move/from16 v21, v0

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 791
    invoke-virtual {v5, v4}, Landroid/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 793
    const/16 v20, 0x4

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v5, v0, v1}, Landroid/app/DialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 795
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v20

    const-string v21, "createFolder"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 797
    const/16 v20, 0x1

    goto/16 :goto_0

    .line 802
    .end local v4    # "argument":Landroid/os/Bundle;
    .end local v5    # "createFolder":Landroid/app/DialogFragment;
    :pswitch_5
    new-instance v13, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    invoke-direct {v13}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;-><init>()V

    .line 804
    .local v13, "newName":Landroid/app/DialogFragment;
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 806
    .restart local v4    # "argument":Landroid/os/Bundle;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v17

    .line 808
    .local v17, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v20

    if-nez v20, :cond_3

    .line 809
    const/16 v20, 0x1

    goto/16 :goto_0

    .line 811
    :cond_3
    const/16 v20, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .line 813
    .local v16, "selectedItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    const-string v20, "current_item"

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 815
    const-string v20, "title"

    const v21, 0x7f0b0027

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 817
    const-string v20, "kind_of_operation"

    const/16 v21, 0x5

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 819
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v11

    .line 821
    .local v11, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    if-eqz v11, :cond_10

    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    if-eqz v20, :cond_10

    .line 823
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/android/app/myfiles/element/BrowserItem;

    move-object/from16 v0, v20

    iget-object v15, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    .line 825
    .local v15, "path":Ljava/lang/String;
    sget-char v20, Ljava/io/File;->separatorChar:C

    move/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v20

    add-int/lit8 v20, v20, 0x1

    move/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 827
    .local v8, "fileName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v21

    const-string v22, "/"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v21

    add-int/lit8 v21, v21, 0x1

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v19

    .line 830
    .local v19, "zipFileName":Ljava/lang/String;
    new-instance v18, Ljava/io/File;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ".zip"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 833
    .local v18, "zipFile":Ljava/io/File;
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_6

    .line 834
    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->exists()Z

    move-result v20

    if-eqz v20, :cond_5

    .line 835
    const-string v20, "current_folder"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 837
    const-string v20, "zipfilename"

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 911
    .end local v8    # "fileName":Ljava/lang/String;
    .end local v15    # "path":Ljava/lang/String;
    .end local v18    # "zipFile":Ljava/io/File;
    .end local v19    # "zipFileName":Ljava/lang/String;
    :cond_4
    :goto_2
    invoke-virtual {v13, v4}, Landroid/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 913
    const/16 v20, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v13, v0, v1}, Landroid/app/DialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 915
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v20

    const-string v21, "zip"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v13, v0, v1}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 917
    const/16 v20, 0x1

    goto/16 :goto_0

    .line 839
    .restart local v8    # "fileName":Ljava/lang/String;
    .restart local v15    # "path":Ljava/lang/String;
    .restart local v18    # "zipFile":Ljava/io/File;
    .restart local v19    # "zipFileName":Ljava/lang/String;
    :cond_5
    const-string v20, "file_name"

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 842
    :cond_6
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 843
    .local v7, "file":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->isDirectory()Z

    move-result v20

    if-eqz v20, :cond_8

    .line 844
    const-string v20, "/"

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    .line 845
    .local v14, "oneZipFileName":Ljava/lang/String;
    new-instance v6, Ljava/io/File;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ".zip"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 846
    .local v6, "existFile":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v20

    if-eqz v20, :cond_7

    .line 847
    const-string v20, "current_folder"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 849
    const-string v20, "zipfilename"

    move-object/from16 v0, v20

    invoke-virtual {v4, v0, v14}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 851
    :cond_7
    const-string v20, "file_name"

    move-object/from16 v0, v20

    invoke-virtual {v4, v0, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 854
    .end local v6    # "existFile":Ljava/io/File;
    .end local v14    # "oneZipFileName":Ljava/lang/String;
    :cond_8
    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v14

    .line 855
    .restart local v14    # "oneZipFileName":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v20

    const-string v21, "."

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 856
    const/16 v20, 0x0

    const-string v21, "."

    move-object/from16 v0, v21

    invoke-virtual {v14, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v21

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v14, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    .line 859
    :cond_9
    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v20

    const-string v21, ".zip"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_b

    .line 860
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v20

    if-eqz v20, :cond_a

    .line 861
    const-string v20, "current_folder"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 863
    const-string v20, "zipfilename"

    move-object/from16 v0, v20

    invoke-virtual {v4, v0, v14}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 865
    :cond_a
    const-string v20, "."

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v10

    .line 866
    .local v10, "lastDot":I
    if-lez v10, :cond_4

    .line 867
    const-string v20, "file_name"

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v8, v0, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 872
    .end local v10    # "lastDot":I
    :cond_b
    const/4 v6, 0x0

    .line 873
    .restart local v6    # "existFile":Ljava/io/File;
    const-string v20, "."

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_c

    .line 874
    new-instance v6, Ljava/io/File;

    .end local v6    # "existFile":Ljava/io/File;
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v21, 0x0

    const-string v22, "."

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v22

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v15, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ".zip"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 880
    .restart local v6    # "existFile":Ljava/io/File;
    :goto_3
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v20

    if-eqz v20, :cond_e

    .line 881
    const-string v20, "current_folder"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 883
    const-string v20, "."

    move-object/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_d

    .line 884
    const-string v20, "zipfilename"

    const-string v21, "/"

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v21

    const-string v22, "."

    move-object/from16 v0, v22

    invoke-virtual {v15, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v22

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v15, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 877
    :cond_c
    new-instance v6, Ljava/io/File;

    .end local v6    # "existFile":Ljava/io/File;
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ".zip"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .restart local v6    # "existFile":Ljava/io/File;
    goto :goto_3

    .line 889
    :cond_d
    const-string v20, "zipfilename"

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 894
    :cond_e
    const-string v20, "."

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v10

    .line 895
    .restart local v10    # "lastDot":I
    if-lez v10, :cond_f

    .line 896
    const-string v20, "file_name"

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v8, v0, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 899
    :cond_f
    const-string v20, "file_name"

    move-object/from16 v0, v20

    invoke-virtual {v4, v0, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 908
    .end local v6    # "existFile":Ljava/io/File;
    .end local v7    # "file":Ljava/io/File;
    .end local v8    # "fileName":Ljava/lang/String;
    .end local v10    # "lastDot":I
    .end local v14    # "oneZipFileName":Ljava/lang/String;
    .end local v15    # "path":Ljava/lang/String;
    .end local v18    # "zipFile":Ljava/io/File;
    .end local v19    # "zipFileName":Ljava/lang/String;
    :cond_10
    const-string v20, "name"

    const-string v21, ""

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 929
    .end local v4    # "argument":Landroid/os/Bundle;
    .end local v11    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v13    # "newName":Landroid/app/DialogFragment;
    .end local v16    # "selectedItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    .end local v17    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v12

    .line 930
    .local v12, "listTemp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    const/4 v9, 0x0

    .line 932
    .local v9, "filePath":Ljava/lang/String;
    invoke-virtual {v12}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v20

    if-nez v20, :cond_11

    .line 933
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/android/app/myfiles/element/BrowserItem;

    move-object/from16 v0, v20

    iget-object v9, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    .line 936
    :cond_11
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mExtractExecuteNormalMode:Z

    .line 937
    const/16 v20, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mSelectedPathForExtract:Ljava/lang/String;

    .line 938
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->createFolderForExtract(Ljava/lang/String;)Z

    move-result v20

    goto/16 :goto_0

    .line 943
    .end local v9    # "filePath":Ljava/lang/String;
    .end local v12    # "listTemp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0xb

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->startExtract(Ljava/lang/String;I)V

    .line 945
    const/16 v20, 0x1

    goto/16 :goto_0

    .line 949
    :pswitch_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v20, v0

    const-string v21, "com.vcast.mediamanager.ACTION_FILES"

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/myfiles/utils/VZCloudUtils;->launchVZCloud(Landroid/content/Context;Ljava/lang/String;)V

    .line 950
    const/16 v20, 0x1

    goto/16 :goto_0

    .line 752
    :pswitch_data_0
    .packed-switch 0x7f0f0129
        :pswitch_3
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_8
        :pswitch_4
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 458
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onPause()V

    .line 460
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->initFileObserver()V

    .line 462
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mLastModifiedTime:J

    .line 464
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mReceiverIsRegistered:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 466
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mScanFileReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 468
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mMtpRemoveReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 470
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mReceiverIsRegistered:Z

    .line 472
    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v2, 0x7f0f013a

    .line 1920
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 1921
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mFromSelector:Z

    if-eqz v1, :cond_0

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1922
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1923
    .local v0, "item":Landroid/view/MenuItem;
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1927
    .end local v0    # "item":Landroid/view/MenuItem;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v2, 0x7f0f0139

    invoke-static {v1, p1, v2}, Lcom/sec/android/app/myfiles/utils/VZCloudUtils;->prepareOptionsMenu(Landroid/content/Context;Landroid/view/Menu;I)V

    .line 1928
    return-void
.end method

.method public onRefresh()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 505
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 507
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    .line 509
    .local v0, "lastModifiedTime":J
    iget-wide v2, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mLastModifiedTime:J

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mIsCopyMove:Z

    if-eqz v2, :cond_2

    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v2

    const-string v3, "/storage"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v2

    const-string v3, "/storage/emulated/0/Bluetooth"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mIsConverting:Z

    if-nez v2, :cond_2

    invoke-static {}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getExtractSuccess()Z

    move-result v2

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mIsZip:Z

    if-nez v2, :cond_2

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->mIsLock:Z

    if-nez v2, :cond_2

    .line 514
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mIsCopyMove:Z

    .line 515
    const-string v2, "FileBrowserFragment"

    const-string v3, "refresh - contents not changed"

    invoke-static {v4, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 548
    .end local v0    # "lastModifiedTime":J
    :cond_1
    :goto_0
    return-void

    .line 519
    .restart local v0    # "lastModifiedTime":J
    :cond_2
    iput-wide v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mLastModifiedTime:J

    .line 520
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mIsConverting:Z

    .line 521
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mIsZip:Z

    .line 522
    sput-boolean v4, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->mIsLock:Z

    .line 523
    const-string v2, "FileBrowserFragment"

    const-string v3, "refresh"

    invoke-static {v4, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 525
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getExtractSuccess()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 526
    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/FileUtils;->setExtractSuccess(Z)V

    .line 528
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->showWaitProgressDialog(Z)V

    .line 530
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->checkShortcutValidationAfterMoveToKnox()V

    .line 532
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$4;

    invoke-direct {v3, p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$4;-><init>(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public onResume()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 287
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onResume()V

    .line 288
    const-string v7, "FileBrowserFragment"

    const-string v8, "onResume"

    invoke-static {v10, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 290
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 292
    .local v2, "intent":Landroid/content/Intent;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-virtual {v2}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v7

    const-string v8, "application/zip"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const-string v7, "used"

    invoke-virtual {v2, v7, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    if-nez v7, :cond_2

    .line 295
    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v6

    .line 297
    .local v6, "uri":Landroid/net/Uri;
    if-eqz v6, :cond_1

    .line 299
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-static {v7, v6}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getFilePathFromUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 301
    .local v3, "path":Ljava/lang/String;
    const-string v7, "/storage"

    invoke-virtual {v3, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 303
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    const v8, 0x7f0b00ec

    invoke-static {v7, v8, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    .line 305
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->finish()V

    .line 414
    .end local v3    # "path":Ljava/lang/String;
    .end local v6    # "uri":Landroid/net/Uri;
    :cond_0
    :goto_0
    return-void

    .line 311
    .restart local v6    # "uri":Landroid/net/Uri;
    :cond_1
    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->extractNotification(Landroid/content/Intent;)V

    .line 314
    .end local v6    # "uri":Landroid/net/Uri;
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/Utils;->isMountedStateCheck(Landroid/content/Context;)V

    .line 316
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->onRefresh()V

    .line 319
    new-instance v7, Ljava/io/File;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    .line 320
    .local v4, "lastModifiedTime":J
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getSelectModeFrom()I

    move-result v7

    if-nez v7, :cond_3

    iget-wide v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mLastModifiedTime:J

    cmp-long v7, v4, v8

    if-eqz v7, :cond_3

    .line 324
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->finishSelectMode()V

    .line 328
    :cond_3
    invoke-virtual {p0, v11}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->showPathIndicator(Z)V

    .line 332
    iget-boolean v7, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mReceiverIsRegistered:Z

    if-nez v7, :cond_4

    .line 334
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 336
    .local v0, "filter":Landroid/content/IntentFilter;
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 338
    .local v1, "filter2":Landroid/content/IntentFilter;
    const-string v7, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-virtual {v0, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 340
    const-string v7, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 342
    const-string v7, "file"

    invoke-virtual {v0, v7}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 344
    const-string v7, "com.android.MTP.OBJECT_REMOVED"

    invoke-virtual {v1, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 346
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    if-eqz v7, :cond_4

    .line 348
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mScanFileReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v7, v8, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 350
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mMtpRemoveReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v7, v8, v1}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 352
    iput-boolean v11, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mReceiverIsRegistered:Z

    .line 359
    .end local v0    # "filter":Landroid/content/IntentFilter;
    .end local v1    # "filter2":Landroid/content/IntentFilter;
    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mFromFindoFile:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_7

    .line 360
    iput-boolean v11, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mModeFromFindo:Z

    .line 361
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mSaveStartPathFromFindo:Ljava/lang/String;

    if-nez v7, :cond_5

    .line 362
    iget-boolean v7, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mIsFolderFromFindo:Z

    if-eqz v7, :cond_6

    .line 363
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mFromFindoFile:Ljava/lang/String;

    iput-object v7, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mSaveStartPathFromFindo:Ljava/lang/String;

    .line 373
    :cond_5
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->initFileObserver()V

    .line 375
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v7

    const-string v8, "/storage/emulated/0/Download"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 377
    new-instance v7, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$3;

    const-string v8, "/storage/emulated/0/Download"

    const/16 v9, 0x8

    invoke-direct {v7, p0, v8, v9}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$3;-><init>(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;Ljava/lang/String;I)V

    iput-object v7, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mFileObserver:Landroid/os/FileObserver;

    .line 411
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mFileObserver:Landroid/os/FileObserver;

    invoke-virtual {v7}, Landroid/os/FileObserver;->startWatching()V

    goto/16 :goto_0

    .line 365
    :cond_6
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mFromFindoFile:Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mFromFindoFile:Ljava/lang/String;

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v7, v10, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mSaveStartPathFromFindo:Ljava/lang/String;

    goto :goto_1

    .line 370
    :cond_7
    iput-boolean v10, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mModeFromFindo:Z

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 477
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 479
    const-string v0, "last_modified_time"

    iget-wide v2, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mLastModifiedTime:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 480
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    .line 267
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 269
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 270
    .local v0, "argument":Landroid/os/Bundle;
    if-eqz v0, :cond_1

    .line 271
    const-string v2, "selection_type"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 272
    .local v1, "selectiontype":I
    const-string v2, "run_from"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const/16 v3, 0x15

    if-ne v2, v3, :cond_1

    .line 273
    if-le v1, v4, :cond_0

    .line 274
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mCreateFolderContainer:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 276
    :cond_0
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mFromSelector:Z

    .line 277
    const/4 v2, -0x1

    invoke-virtual {p0, v1, v2, v4}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->startSelectMode(III)V

    .line 282
    .end local v1    # "selectiontype":I
    :cond_1
    return-void
.end method

.method public performExtract(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 1282
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mExtractExecuteNormalMode:Z

    .line 1283
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mSelectedPathForExtract:Ljava/lang/String;

    .line 1284
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->createFolderForExtract(Ljava/lang/String;)Z

    .line 1285
    return-void
.end method

.method protected selectAllItem()V
    .locals 2

    .prologue
    .line 1044
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->selectAllItem()V

    .line 1045
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1046
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getSelectModeFrom()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1047
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->selectAllFileItem()V

    .line 1052
    :cond_0
    :goto_0
    return-void

    .line 1049
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->selectAllItem()V

    goto :goto_0
.end method

.method public selectmodeActionbar()V
    .locals 7

    .prologue
    const v6, 0x7f0b0014

    const v5, 0x7f0b0013

    const/4 v3, 0x1

    const/4 v2, 0x0

    const v4, 0x7f0b0018

    .line 1941
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-nez v1, :cond_1

    .line 2081
    :cond_0
    :goto_0
    return-void

    .line 1944
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 1945
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 1946
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 1947
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 1949
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getSelectionType()I

    move-result v1

    if-eq v1, v3, :cond_4

    .line 1950
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040008

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1952
    .local v0, "customview":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 1953
    const v1, 0x7f0f0008

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mActionbar_cancel:Landroid/widget/TextView;

    .line 1954
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mActionbar_cancel:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0b0017

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1955
    const v1, 0x7f0f0009

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    .line 1956
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0b0016

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1959
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/SelectorActivity;

    if-eqz v1, :cond_2

    .line 1960
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->getmFileOperation()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    .line 1961
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->getmFileOperation()I

    move-result v1

    if-nez v1, :cond_3

    .line 1962
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1963
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v5}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1970
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mActionbar_cancel:Landroid/widget/TextView;

    new-instance v2, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$13;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$13;-><init>(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1986
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    new-instance v2, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$14;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$14;-><init>(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1998
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v1, :cond_0

    .line 1999
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1965
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(I)V

    .line 1966
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v6}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 2008
    .end local v0    # "customview":Landroid/view/View;
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040009

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 2010
    .restart local v0    # "customview":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 2012
    const v1, 0x7f0f0009

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    .line 2016
    const v1, 0x7f0f0003

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mSelectAllButton:Landroid/widget/TextView;

    .line 2038
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    new-instance v2, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$15;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$15;-><init>(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method

.method protected unselectAllItem()V
    .locals 1

    .prologue
    .line 1057
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->unselectAllItem()V

    .line 1059
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1061
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->unselectAllItem()V

    .line 1063
    :cond_0
    return-void
.end method
