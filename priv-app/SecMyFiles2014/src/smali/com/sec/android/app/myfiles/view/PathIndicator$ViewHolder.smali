.class Lcom/sec/android/app/myfiles/view/PathIndicator$ViewHolder;
.super Ljava/lang/Object;
.source "PathIndicator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/view/PathIndicator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewHolder"
.end annotation


# instance fields
.field public mContainer:Landroid/widget/FrameLayout;

.field public mPathIndicatorArrow:Landroid/widget/ImageView;

.field public mPathIndicatorButton:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/widget/FrameLayout;Landroid/widget/TextView;Landroid/widget/ImageView;)V
    .locals 0
    .param p1, "container"    # Landroid/widget/FrameLayout;
    .param p2, "pathIndicatorButton"    # Landroid/widget/TextView;
    .param p3, "pathIndicatorArrow"    # Landroid/widget/ImageView;

    .prologue
    .line 624
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 626
    iput-object p1, p0, Lcom/sec/android/app/myfiles/view/PathIndicator$ViewHolder;->mContainer:Landroid/widget/FrameLayout;

    .line 628
    iput-object p2, p0, Lcom/sec/android/app/myfiles/view/PathIndicator$ViewHolder;->mPathIndicatorButton:Landroid/widget/TextView;

    .line 630
    iput-object p3, p0, Lcom/sec/android/app/myfiles/view/PathIndicator$ViewHolder;->mPathIndicatorArrow:Landroid/widget/ImageView;

    .line 631
    return-void
.end method
