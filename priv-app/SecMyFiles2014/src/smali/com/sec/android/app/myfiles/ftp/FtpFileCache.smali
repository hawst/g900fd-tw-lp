.class public Lcom/sec/android/app/myfiles/ftp/FtpFileCache;
.super Ljava/lang/Object;
.source "FtpFileCache.java"


# instance fields
.field private final mCache:Lcom/sec/android/app/myfiles/db/CacheDB;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-direct {v0, p1}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FtpFileCache;->mCache:Lcom/sec/android/app/myfiles/db/CacheDB;

    .line 31
    return-void
.end method


# virtual methods
.method public cacheFile(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;)V
    .locals 2
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p2, "fileFullPath"    # Ljava/lang/String;

    .prologue
    .line 51
    if-eqz p1, :cond_0

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FtpFileCache;->mCache:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FtpFileCache;->mCache:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/myfiles/db/CacheDB;->cacheFtpFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    :cond_0
    return-void
.end method

.method public isFileCached(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p2, "fileFullPath"    # Ljava/lang/String;
    .param p3, "cachedFullPath"    # Ljava/lang/String;

    .prologue
    .line 35
    if-eqz p1, :cond_0

    .line 37
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FtpFileCache;->mCache:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 39
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FtpFileCache;->mCache:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3}, Lcom/sec/android/app/myfiles/db/CacheDB;->isFileCached(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 43
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeFromCache(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p2, "fileFullPath"    # Ljava/lang/String;
    .param p3, "cachedFullPath"    # Ljava/lang/String;

    .prologue
    .line 63
    if-eqz p1, :cond_0

    .line 65
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FtpFileCache;->mCache:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 67
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FtpFileCache;->mCache:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Lcom/sec/android/app/myfiles/db/CacheDB;->deleteFtpFileFromCache(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 73
    .local v0, "cachedFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 75
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 79
    :cond_1
    return-void
.end method
