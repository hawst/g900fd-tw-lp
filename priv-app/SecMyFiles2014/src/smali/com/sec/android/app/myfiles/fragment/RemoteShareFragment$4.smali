.class Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$4;
.super Ljava/lang/Object;
.source "RemoteShareFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)V
    .locals 0

    .prologue
    .line 633
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 28
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 637
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/database/Cursor;

    .line 638
    .local v5, "c":Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->isSelectMode()Z

    move-result v25

    if-nez v25, :cond_9

    .line 639
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v25

    if-nez v25, :cond_3

    .line 640
    const-string v25, "_id"

    move-object/from16 v0, v25

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    move/from16 v0, v25

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 641
    .local v20, "mediaId":J
    const-string v25, "expire_date"

    move-object/from16 v0, v25

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    move/from16 v0, v25

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 642
    .local v14, "expireDate":J
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 644
    .local v6, "currentCalendar":Ljava/util/Calendar;
    :try_start_0
    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    .line 645
    .local v8, "currentTimeInMillis":J
    const-wide/16 v26, 0x0

    cmp-long v25, v14, v26

    if-eqz v25, :cond_0

    cmp-long v25, v8, v14

    if-gtz v25, :cond_2

    .line 646
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    move-object/from16 v25, v0

    # invokes: Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->backToRealViewMode()V
    invoke-static/range {v25 .. v25}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->access$500(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)V

    .line 647
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v25, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v26

    const/16 v27, 0x0

    invoke-static/range {v27 .. v27}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v27

    invoke-virtual/range {v25 .. v27}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z

    .line 709
    .end local v6    # "currentCalendar":Ljava/util/Calendar;
    .end local v8    # "currentTimeInMillis":J
    .end local v14    # "expireDate":J
    .end local v20    # "mediaId":J
    :cond_1
    :goto_0
    return-void

    .line 649
    .restart local v6    # "currentCalendar":Ljava/util/Calendar;
    .restart local v8    # "currentTimeInMillis":J
    .restart local v14    # "expireDate":J
    .restart local v20    # "mediaId":J
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f0b0011

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v26

    const/16 v27, 0x0

    invoke-static/range {v25 .. v27}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 651
    .end local v8    # "currentTimeInMillis":J
    :catch_0
    move-exception v12

    .line 652
    .local v12, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v12}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 655
    .end local v6    # "currentCalendar":Ljava/util/Calendar;
    .end local v12    # "e":Ljava/lang/IllegalArgumentException;
    .end local v14    # "expireDate":J
    .end local v20    # "mediaId":J
    :cond_3
    const/4 v7, 0x0

    .line 656
    .local v7, "downloaded":Z
    const-string v25, "local_file"

    move-object/from16 v0, v25

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    move/from16 v0, v25

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 657
    .local v17, "filePath":Ljava/lang/String;
    if-eqz v17, :cond_4

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v25

    if-eqz v25, :cond_4

    .line 658
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    move-object/from16 v25, v0

    invoke-static/range {v17 .. v17}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v26

    # invokes: Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->sendOpenFileIntent(Landroid/net/Uri;)V
    invoke-static/range {v25 .. v26}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->access$600(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;Landroid/net/Uri;)V

    .line 660
    :cond_4
    if-nez v7, :cond_1

    .line 661
    const/16 v22, 0x0

    .line 662
    .local v22, "startDownload":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    instance-of v0, v0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;

    move/from16 v25, v0

    if-eqz v25, :cond_5

    .line 663
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v25, v0

    check-cast v25, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;

    move-object/from16 v0, v25

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->isAvailableForDownload(I)Z

    move-result v22

    .line 665
    :cond_5
    if-eqz v22, :cond_6

    .line 666
    const-string v25, "filename"

    move-object/from16 v0, v25

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    move/from16 v0, v25

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 667
    .local v16, "fileName":Ljava/lang/String;
    const-string v25, "_id"

    move-object/from16 v0, v25

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    move/from16 v0, v25

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 668
    .local v13, "fileId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    move-object/from16 v25, v0

    new-instance v26, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$4$1;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$4$1;-><init>(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$4;Ljava/lang/String;)V

    # setter for: Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mOnDownloadedListener:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$OnDownloadedListener;
    invoke-static/range {v25 .. v26}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->access$702(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$OnDownloadedListener;)Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$OnDownloadedListener;

    .line 674
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    move-object/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v26

    const/16 v27, 0x1b

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, v16

    move/from16 v3, v27

    invoke-virtual {v0, v13, v1, v2, v3}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->startRemoteShareDownload(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 676
    .end local v13    # "fileId":Ljava/lang/String;
    .end local v16    # "fileName":Ljava/lang/String;
    :cond_6
    const-string v25, "status"

    move-object/from16 v0, v25

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    move/from16 v0, v25

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    .line 677
    .local v18, "fileStatus":I
    const/16 v25, 0x2

    move/from16 v0, v18

    move/from16 v1, v25

    if-eq v0, v1, :cond_7

    const/16 v25, 0x1

    move/from16 v0, v18

    move/from16 v1, v25

    if-ne v0, v1, :cond_8

    .line 678
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->getActivity()Landroid/app/Activity;

    move-result-object v25

    const-string v26, "Download in progress"

    const/16 v27, 0x0

    invoke-static/range {v25 .. v27}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 679
    :cond_8
    const/16 v25, 0x3

    move/from16 v0, v18

    move/from16 v1, v25

    if-ne v0, v1, :cond_1

    .line 680
    const-string v25, "filename"

    move-object/from16 v0, v25

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    move/from16 v0, v25

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 681
    .restart local v16    # "fileName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v16

    # invokes: Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->sendOpenFileIntent(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->access$800(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 687
    .end local v7    # "downloaded":Z
    .end local v16    # "fileName":Ljava/lang/String;
    .end local v17    # "filePath":Ljava/lang/String;
    .end local v18    # "fileStatus":I
    .end local v22    # "startDownload":Z
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    instance-of v0, v0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;

    move/from16 v25, v0

    if-eqz v25, :cond_a

    .line 688
    const-string v25, "_id"

    move-object/from16 v0, v25

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    move/from16 v0, v25

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 689
    .local v10, "cursorId":J
    const-string v25, "thumbnail_uri"

    move-object/from16 v0, v25

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    move/from16 v0, v25

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 690
    .local v23, "title":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v23

    invoke-virtual {v0, v10, v11, v1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->multipleSelect(JLjava/lang/String;)V

    .line 692
    .end local v10    # "cursorId":J
    .end local v23    # "title":Ljava/lang/String;
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v25, v0

    if-eqz v25, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    instance-of v0, v0, Lcom/sec/android/app/myfiles/MainActivity;

    move/from16 v25, v0

    if-eqz v25, :cond_1

    .line 693
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v4, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v4, Lcom/sec/android/app/myfiles/MainActivity;

    .line 694
    .local v4, "activity":Lcom/sec/android/app/myfiles/MainActivity;
    const/16 v19, 0x0

    .line 695
    .local v19, "selectAll":Z
    const/16 v24, 0x0

    .line 696
    .local v24, "unselectAll":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mSelectableItemsCount:I
    invoke-static/range {v25 .. v25}, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->access$900(Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;)I

    move-result v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v26

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_b

    .line 697
    const/16 v19, 0x0

    .line 701
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RemoteShareFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v25

    if-nez v25, :cond_c

    .line 702
    const/16 v24, 0x0

    .line 706
    :goto_2
    move/from16 v0, v19

    move/from16 v1, v24

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/app/myfiles/MainActivity;->setSelectOptionsVisibility(ZZ)V

    goto/16 :goto_0

    .line 699
    :cond_b
    const/16 v19, 0x1

    goto :goto_1

    .line 704
    :cond_c
    const/16 v24, 0x1

    goto :goto_2
.end method
