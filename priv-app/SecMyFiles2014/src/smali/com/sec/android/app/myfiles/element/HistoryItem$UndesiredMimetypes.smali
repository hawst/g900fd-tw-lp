.class public final enum Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;
.super Ljava/lang/Enum;
.source "HistoryItem.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/element/HistoryItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UndesiredMimetypes"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;

.field public static final enum ANDROID_PLAY_STORE_APP:Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;


# instance fields
.field private mMimeType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 106
    new-instance v0, Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;

    const-string v1, "ANDROID_PLAY_STORE_APP"

    const-string v2, "application/vnd.android.obb"

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;->ANDROID_PLAY_STORE_APP:Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;

    .line 105
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;

    sget-object v1, Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;->ANDROID_PLAY_STORE_APP:Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;->$VALUES:[Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "mimeType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 108
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 109
    iput-object p3, p0, Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;->mMimeType:Ljava/lang/String;

    .line 110
    return-void
.end method

.method public static containsMimeType(Ljava/lang/String;)Z
    .locals 5
    .param p0, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 113
    invoke-static {}, Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;->values()[Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;

    move-result-object v0

    .local v0, "arr$":[Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 114
    .local v3, "undesiredMimetype":Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;
    iget-object v4, v3, Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;->mMimeType:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 115
    const/4 v4, 0x1

    .line 117
    .end local v3    # "undesiredMimetype":Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;
    :goto_1
    return v4

    .line 113
    .restart local v3    # "undesiredMimetype":Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 117
    .end local v3    # "undesiredMimetype":Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 105
    const-class v0, Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;->$VALUES:[Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;

    invoke-virtual {v0}, [Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;

    return-object v0
.end method
