.class public Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;
.super Landroid/app/Activity;
.source "ChangePasswordsActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;
    }
.end annotation


# static fields
.field public static final APPLOCK_ENABLE:Ljava/lang/String; = "applock_enable"

.field public static final CHANGE_PIN:Ljava/lang/String; = "change_pin"

.field private static final ERROR_MESSAGE_TIMEOUT:J = 0x3e8L

.field private static final KEY_FIRST_PIN:Ljava/lang/String; = "first_pin"

.field private static final KEY_UI_STAGE:Ljava/lang/String; = "ui_stage"

.field private static final MODULE:Ljava/lang/String; = "ChangeLockPasswordActivity"

.field public static final PREFS:Ljava/lang/String; = "AppLockPreferences"

.field public static final PREF_APPLOCK_PIN:Ljava/lang/String; = "PREF_APPLOCK_PIN"

.field public static final PRESS_CANCEL:Ljava/lang/String; = "press_cancel"

.field private static mHandler:Landroid/os/Handler;


# instance fields
.field final HIDE_KEYBOARD:I

.field final INPUT_KEYBOARD:I

.field private isChangePW:Z

.field private isConfigchange:Z

.field keyboardVisible:Z

.field keyboardVisibleBK:Z

.field private mCancelButton:Landroid/widget/Button;

.field private mFirstPin:Ljava/lang/String;

.field private mHeaderText:Landroid/widget/TextView;

.field protected mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

.field private mNextButton:Landroid/widget/Button;

.field private mPasswordEntry:Landroid/widget/TextView;

.field private mPasswordMaxLength:I

.field private mPasswordMinLength:I

.field mUiStage:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

.field protected timingHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 105
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 54
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 93
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordMinLength:I

    .line 95
    const/16 v0, 0x10

    iput v0, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordMaxLength:I

    .line 97
    iput v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->HIDE_KEYBOARD:I

    .line 99
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->INPUT_KEYBOARD:I

    .line 101
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->isConfigchange:Z

    .line 103
    sget-object v0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;->Introduction:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mUiStage:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    .line 544
    new-instance v0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$4;-><init>(Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->timingHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mHeaderText:Landroid/widget/TextView;

    return-object v0
.end method

.method private handleNext()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 343
    iget-object v2, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 344
    .local v1, "pin":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 388
    :cond_0
    :goto_0
    return-void

    .line 347
    :cond_1
    const/4 v0, 0x0

    .line 349
    .local v0, "errorMsg":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mUiStage:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    sget-object v3, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;->Introduction:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    if-ne v2, v3, :cond_3

    .line 350
    invoke-direct {p0, v1}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->validatePassword(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 351
    if-nez v0, :cond_2

    .line 352
    iput-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mFirstPin:Ljava/lang/String;

    .line 353
    sget-object v2, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;->NeedToConfirm:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->updateStage(Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;)V

    .line 354
    iget-object v2, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordEntry:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 384
    :cond_2
    :goto_1
    if-eqz v0, :cond_0

    .line 385
    iget-object v2, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mUiStage:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    invoke-direct {p0, v0, v2}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->showError(Ljava/lang/String;Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;)V

    goto :goto_0

    .line 356
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mUiStage:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    sget-object v3, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;->NeedToConfirm:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    if-ne v2, v3, :cond_2

    .line 357
    iget-object v2, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mFirstPin:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 358
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->isChangePW:Z

    if-eqz v2, :cond_4

    .line 359
    sget-object v2, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;->Introduction:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->updateStage(Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;)V

    .line 360
    iget-object v2, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordEntry:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 361
    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->isChangePW:Z

    goto :goto_1

    .line 366
    :cond_4
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "PREF_APPLOCK_PIN"

    iget-object v4, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mFirstPin:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 368
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "applock_enable"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 372
    :goto_2
    const/4 v2, -0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->setResult(I)V

    .line 374
    iget-object v2, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->timingHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v6, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 375
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->finish()V

    goto :goto_1

    .line 379
    :cond_5
    sget-object v2, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;->ConfirmWrong:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->updateStage(Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;)V

    .line 380
    iget-object v2, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordEntry:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 369
    :catch_0
    move-exception v2

    goto :goto_2
.end method

.method private initViews()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 184
    const v1, 0x7f040020

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->setContentView(I)V

    .line 185
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 186
    .local v0, "actionBar":Landroid/app/ActionBar;
    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 187
    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 190
    const v1, 0x7f0b0111

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 192
    const v1, 0x7f0f009b

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mCancelButton:Landroid/widget/Button;

    .line 193
    iget-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 194
    const v1, 0x7f0f009c

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mNextButton:Landroid/widget/Button;

    .line 195
    iget-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 197
    const v1, 0x7f0f0099

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordEntry:Landroid/widget/TextView;

    .line 198
    iget-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 199
    iget-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 201
    iget-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordEntry:Landroid/widget/TextView;

    new-array v2, v2, [Landroid/text/InputFilter;

    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    iget v4, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordMaxLength:I

    invoke-direct {v3, v4}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v2, v5

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    .line 204
    iget-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordEntry:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/DigitsKeyListener;->getInstance()Landroid/text/method/DigitsKeyListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 205
    iget-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordEntry:Landroid/widget/TextView;

    const v2, 0x10000006

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setImeOptions(I)V

    .line 207
    iget-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordEntry:Landroid/widget/TextView;

    const-string v2, "inputType=YearDateTime_edittext"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 208
    iget-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordEntry:Landroid/widget/TextView;

    const/16 v2, 0x12

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setInputType(I)V

    .line 214
    iget-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 215
    iget-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordEntry:Landroid/widget/TextView;

    new-instance v2, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$1;-><init>(Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 224
    iget-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setLongClickable(Z)V

    .line 225
    const v1, 0x7f0f0097

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mHeaderText:Landroid/widget/TextView;

    .line 227
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    .line 228
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->keyboardVisibleBK:Z

    .line 229
    return-void
.end method

.method private isChangePassword()Z
    .locals 3

    .prologue
    .line 573
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "change_pin"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private showError(Ljava/lang/String;Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;)V
    .locals 4
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "next"    # Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    .prologue
    .line 391
    iget-object v0, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mHeaderText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 392
    sget-object v0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$2;

    invoke-direct {v1, p0, p2}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$2;-><init>(Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 397
    return-void
.end method

.method private updateUi()V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 462
    iget-object v6, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 463
    .local v3, "password":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    .line 465
    .local v1, "length":I
    iget-object v6, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mUiStage:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    sget-object v7, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;->Introduction:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    if-ne v6, v7, :cond_2

    if-lez v1, :cond_2

    .line 466
    iget v6, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordMinLength:I

    if-ge v1, v6, :cond_0

    .line 467
    const v6, 0x7f0b0115

    new-array v5, v5, [Ljava/lang/Object;

    iget v7, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordMinLength:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v4

    invoke-virtual {p0, v6, v5}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 468
    .local v2, "msg":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mHeaderText:Landroid/widget/TextView;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 469
    iget-object v5, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v5, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 470
    iget-object v5, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v5, v4}, Landroid/widget/Button;->setFocusable(Z)V

    .line 489
    .end local v2    # "msg":Ljava/lang/String;
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->requestFocus()Z

    .line 490
    iget-object v4, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mNextButton:Landroid/widget/Button;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mUiStage:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    iget v5, v5, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;->buttonText:I

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setText(I)V

    .line 491
    return-void

    .line 472
    :cond_0
    invoke-direct {p0, v3}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->validatePassword(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 473
    .local v0, "error":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 474
    iget-object v5, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mHeaderText:Landroid/widget/TextView;

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 475
    iget-object v5, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v5, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 476
    iget-object v5, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v5, v4}, Landroid/widget/Button;->setFocusable(Z)V

    goto :goto_0

    .line 478
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mHeaderText:Landroid/widget/TextView;

    const v6, 0x7f0b0118

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(I)V

    .line 480
    iget-object v4, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 481
    iget-object v4, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setFocusable(Z)V

    goto :goto_0

    .line 485
    .end local v0    # "error":Ljava/lang/String;
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mHeaderText:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mUiStage:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    iget v7, v7, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;->numericHint:I

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 486
    iget-object v6, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mNextButton:Landroid/widget/Button;

    if-lez v1, :cond_3

    move v4, v5

    :cond_3
    invoke-virtual {v6, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 487
    iget-object v4, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setFocusable(Z)V

    goto :goto_0
.end method

.method private validatePassword(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 400
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    iget v5, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordMinLength:I

    if-ge v4, v5, :cond_0

    .line 401
    const v4, 0x7f0b0115

    new-array v5, v6, [Ljava/lang/Object;

    iget v6, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordMinLength:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 424
    :goto_0
    return-object v4

    .line 403
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    iget v5, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordMaxLength:I

    if-le v4, v5, :cond_1

    .line 404
    const v4, 0x7f0b0116

    new-array v5, v6, [Ljava/lang/Object;

    iget v6, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordMaxLength:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 406
    :cond_1
    const/4 v1, 0x0

    .line 407
    .local v1, "hasAlpha":Z
    const/4 v2, 0x0

    .line 408
    .local v2, "hasSymbol":Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v3, v4, :cond_6

    .line 409
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 410
    .local v0, "c":C
    const/16 v4, 0x30

    if-lt v0, v4, :cond_2

    const/16 v4, 0x39

    if-gt v0, v4, :cond_2

    .line 408
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 412
    :cond_2
    const/16 v4, 0x41

    if-lt v0, v4, :cond_3

    const/16 v4, 0x5a

    if-le v0, v4, :cond_4

    :cond_3
    const/16 v4, 0x61

    if-lt v0, v4, :cond_5

    const/16 v4, 0x7a

    if-gt v0, v4, :cond_5

    .line 413
    :cond_4
    const/4 v1, 0x1

    goto :goto_2

    .line 415
    :cond_5
    const/4 v2, 0x1

    goto :goto_2

    .line 418
    .end local v0    # "c":C
    :cond_6
    if-nez v1, :cond_7

    if-eqz v2, :cond_8

    .line 421
    :cond_7
    const v4, 0x7f0b0117

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 424
    :cond_8
    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 497
    iget-object v0, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mUiStage:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    sget-object v1, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;->ConfirmWrong:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    if-ne v0, v1, :cond_0

    .line 498
    sget-object v0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;->NeedToConfirm:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mUiStage:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    .line 499
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$3;-><init>(Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;)V

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 505
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 506
    iget-object v0, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mNextButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 511
    :goto_0
    return-void

    .line 510
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->updateUi()V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 517
    return-void
.end method

.method protected hideKeyboard()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 568
    iget-object v0, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 569
    iput-boolean v2, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->keyboardVisible:Z

    .line 570
    return-void
.end method

.method public onBackPressed()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 332
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 333
    invoke-virtual {p0, v6}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->setResult(I)V

    .line 334
    const-string v2, "AppLockPreferences"

    invoke-virtual {p0, v2, v6}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 335
    .local v1, "mSharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 336
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "press_cancel"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 337
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 338
    iget-object v2, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->timingHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v6, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 339
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->finish()V

    .line 340
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x0

    .line 308
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 328
    :goto_0
    return-void

    .line 310
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->handleNext()V

    goto :goto_0

    .line 313
    :pswitch_1
    invoke-virtual {p0, v6}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->setResult(I)V

    .line 315
    const-string v2, "AppLockPreferences"

    invoke-virtual {p0, v2, v6}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 317
    .local v1, "mSharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 318
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "press_cancel"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 319
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 321
    iget-object v2, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->timingHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v6, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 322
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->finish()V

    goto :goto_0

    .line 308
    :pswitch_data_0
    .packed-switch 0x7f0f009b
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 7
    .param p1, "arg0"    # Landroid/content/res/Configuration;

    .prologue
    const-wide/16 v4, 0x1f4

    const/4 v6, 0x0

    const/4 v3, 0x1

    .line 283
    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->isConfigchange:Z

    .line 284
    iget-object v2, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 285
    .local v1, "lockCode":Ljava/lang/String;
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->initViews()V

    .line 286
    iget-object v2, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 287
    iget-object v2, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->isFocusable()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 288
    :cond_0
    const-string v2, "keyguard"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 289
    .local v0, "km":Landroid/app/KeyguardManager;
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 290
    iget-object v2, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->timingHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v6, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 291
    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->keyboardVisible:Z

    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->keyboardVisibleBK:Z

    .line 300
    .end local v0    # "km":Landroid/app/KeyguardManager;
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->updateUi()V

    .line 301
    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->isConfigchange:Z

    .line 302
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 303
    return-void

    .line 292
    .restart local v0    # "km":Landroid/app/KeyguardManager;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->isFocusable()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 293
    iget-object v2, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->timingHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 294
    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->keyboardVisible:Z

    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->keyboardVisibleBK:Z

    goto :goto_0

    .line 295
    :cond_3
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->keyboardVisibleBK:Z

    if-eqz v2, :cond_1

    .line 296
    iget-object v2, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->requestFocus()Z

    .line 297
    iget-object v2, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->timingHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 127
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    .line 128
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 130
    const/4 v0, 0x2

    const-string v1, "ChangeLockPasswordActivity"

    const-string v2, "onCreate"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    .line 131
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->initViews()V

    .line 132
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->isChangePassword()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->isChangePW:Z

    .line 134
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "applock_enable"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->isChangePW:Z

    if-nez v0, :cond_0

    .line 137
    sget-object v0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;->NeedToConfirm:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->updateStage(Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;)V

    .line 138
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "PREF_APPLOCK_PIN"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mFirstPin:Ljava/lang/String;

    .line 151
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->timingHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 152
    return-void

    .line 140
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "applock_enable"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->isChangePW:Z

    if-eqz v0, :cond_1

    .line 143
    sget-object v0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;->NeedToConfirm:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->updateStage(Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;)V

    .line 144
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "PREF_APPLOCK_PIN"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mFirstPin:Ljava/lang/String;

    goto :goto_0

    .line 147
    :cond_1
    sget-object v0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;->Introduction:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->updateStage(Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 240
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->hideKeyboard()V

    .line 241
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 242
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->finish()V

    .line 243
    const/4 v0, 0x2

    const-string v1, "ChangeLockPasswordActivity"

    const-string v2, "onDestroy"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    .line 245
    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/widget/TextView;
    .param p2, "actionId"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 536
    if-eqz p2, :cond_0

    const/4 v0, 0x6

    if-eq p2, v0, :cond_0

    const/4 v0, 0x5

    if-ne p2, v0, :cond_1

    .line 538
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->handleNext()V

    .line 539
    const/4 v0, 0x1

    .line 541
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Z

    .prologue
    .line 235
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    .line 445
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    .line 446
    iget-object v2, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 447
    .local v1, "password":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    .line 448
    .local v0, "length":I
    if-nez v0, :cond_0

    .line 449
    iget-object v2, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mNextButton:Landroid/widget/Button;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setFocusable(Z)V

    .line 453
    .end local v0    # "length":I
    .end local v1    # "password":Ljava/lang/String;
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    return v2
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 7
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 429
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 441
    :goto_0
    return v6

    .line 431
    :pswitch_0
    const-string v2, "AppLockPreferences"

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 433
    .local v1, "mSharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 434
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "press_cancel"

    invoke-interface {v0, v2, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 435
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 437
    iget-object v2, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->timingHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 438
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->finish()V

    goto :goto_0

    .line 429
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 176
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 178
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->keyboardVisible:Z

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->keyboardVisibleBK:Z

    .line 179
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->keyboardVisibleBK:Z

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->timingHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 181
    :cond_0
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 272
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 273
    const-string v1, "ui_stage"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 274
    .local v0, "state":Ljava/lang/String;
    const-string v1, "first_pin"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mFirstPin:Ljava/lang/String;

    .line 275
    if-eqz v0, :cond_0

    .line 276
    invoke-static {v0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;->valueOf(Ljava/lang/String;)Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mUiStage:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    .line 277
    iget-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mUiStage:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->updateStage(Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;)V

    .line 279
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x1f4

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 156
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 157
    const/4 v1, 0x2

    const-string v2, "ChangeLockPasswordActivity"

    const-string v3, "onResume"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    .line 159
    iget-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mUiStage:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->updateStage(Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;)V

    .line 161
    const-string v1, "keyguard"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 162
    .local v0, "km":Landroid/app/KeyguardManager;
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 163
    iget-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->timingHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v5, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 164
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->keyboardVisible:Z

    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->keyboardVisibleBK:Z

    .line 172
    :cond_0
    :goto_0
    return-void

    .line 165
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->isFocusable()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 166
    iget-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->timingHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 167
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->keyboardVisible:Z

    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->keyboardVisibleBK:Z

    goto :goto_0

    .line 168
    :cond_2
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->keyboardVisibleBK:Z

    if-eqz v1, :cond_0

    .line 169
    iget-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->requestFocus()Z

    .line 170
    iget-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->timingHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 249
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 250
    const-string v0, "ui_stage"

    iget-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mUiStage:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    const-string v0, "first_pin"

    iget-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mFirstPin:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mUiStage:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    sget-object v1, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;->NeedToConfirm:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    if-ne v0, v1, :cond_1

    .line 258
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "applock_enable"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_0

    .line 260
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "applock_enable"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 263
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "PREF_APPLOCK_PIN"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mFirstPin:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 268
    :cond_1
    :goto_0
    return-void

    .line 265
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 6
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    const/4 v5, 0x0

    .line 523
    iget-object v2, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordEntry:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/Editable;

    .line 525
    .local v0, "edit":Landroid/text/Editable;
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v2

    iget v3, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordMaxLength:I

    if-lt v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->isConfigchange:Z

    if-nez v2, :cond_0

    .line 526
    const v2, 0x7f0b0116

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordMaxLength:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 529
    .local v1, "strToastMsg":Ljava/lang/String;
    invoke-static {p0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 531
    .end local v1    # "strToastMsg":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method protected showKeyboard()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 558
    iget-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->isAccessoryKeyboardState()I

    move-result v1

    if-nez v1, :cond_1

    .line 559
    iget-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mPasswordEntry:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    move-result v0

    .line 560
    .local v0, "result":Z
    if-nez v0, :cond_0

    .line 561
    iget-object v1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->timingHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 563
    :cond_0
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->keyboardVisible:Z

    .line 565
    .end local v0    # "result":Z
    :cond_1
    return-void
.end method

.method protected updateStage(Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;)V
    .locals 0
    .param p1, "stage"    # Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    .prologue
    .line 457
    iput-object p1, p0, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->mUiStage:Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity$Stage;

    .line 458
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/activity/ChangePasswordsActivity;->updateUi()V

    .line 459
    return-void
.end method
