.class Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;
.super Ljava/lang/Object;
.source "NearByDevicesBrowserFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;)V
    .locals 0

    .prologue
    .line 584
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 23
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 589
    .local p1, "parentView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/16 v19, 0x0

    const-string v20, "NearByDevicesBrowserFragment"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "onItemClick() : parent = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 597
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    # setter for: Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->bBack:Z
    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->access$002(Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;Z)Z

    .line 599
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mDevicesListShowing:Z
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;)Z

    move-result v19

    if-eqz v19, :cond_1

    .line 601
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v19, v0

    check-cast v19, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->cancelThumbnailsLoading(Z)V

    .line 603
    invoke-virtual/range {p1 .. p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p3

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/database/Cursor;

    .line 605
    .local v2, "cursor":Landroid/database/Cursor;
    const-string v19, "deviceid"

    move-object/from16 v0, v19

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 607
    .local v9, "iid":I
    const/16 v19, -0x1

    move/from16 v0, v19

    if-eq v9, v0, :cond_0

    .line 609
    invoke-interface {v2, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 611
    .local v3, "deviceId":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getCategoryAdapterDevicesCollection()Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    move-result-object v6

    .line 613
    .local v6, "devices":Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;
    if-eqz v6, :cond_0

    .line 615
    invoke-static {}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->getInstance()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;

    move-result-object v19

    invoke-virtual {v6, v3}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->getDevice(Ljava/lang/String;)Lcom/samsung/android/allshare/Device;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v3, v1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesContainer;->registerDevice(Ljava/lang/String;Lcom/samsung/android/allshare/Device;)V

    .line 617
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->changeProvider(Ljava/lang/String;)V

    .line 619
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-static/range {v20 .. v20}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goToRoot(Ljava/lang/Boolean;)Z

    .line 733
    .end local v2    # "cursor":Landroid/database/Cursor;
    .end local v3    # "deviceId":Ljava/lang/String;
    .end local v6    # "devices":Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;
    .end local v9    # "iid":I
    :cond_0
    :goto_0
    return-void

    .line 628
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->isSelectMode()Z

    move-result v19

    if-eqz v19, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mRunFrom:I

    move/from16 v19, v0

    const/16 v20, 0x15

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_b

    .line 630
    :cond_2
    const/16 v19, 0x0

    const-string v20, "NearByDevicesBrowserFragment"

    const-string v21, "onItemClick() : Normal mode"

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 632
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v19, v0

    check-cast v19, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->cancelThumbnailsLoading(Z)V

    .line 634
    if-eqz p1, :cond_9

    invoke-virtual/range {p1 .. p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v19

    if-eqz v19, :cond_9

    .line 636
    invoke-virtual/range {p1 .. p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p3

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/database/Cursor;

    .line 638
    .restart local v2    # "cursor":Landroid/database/Cursor;
    if-eqz v2, :cond_8

    .line 640
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    move-object/from16 v19, v0

    const-string v20, "title"

    move-object/from16 v0, v20

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    # setter for: Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mCurTitle:Ljava/lang/String;
    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->access$202(Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 641
    const-string v19, "_id"

    move-object/from16 v0, v19

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    move/from16 v0, v19

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 642
    .local v12, "itemId":J
    const-string v19, "Extension"

    move-object/from16 v0, v19

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    move/from16 v0, v19

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 644
    .local v8, "extension":Ljava/lang/String;
    if-eqz v8, :cond_7

    .line 646
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v19

    if-nez v19, :cond_3

    .line 648
    const/16 v19, 0x0

    const-string v20, "NearByDevicesBrowserFragment"

    const-string v21, "onItemClick() : Type is folder"

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 649
    const/16 v19, 0x0

    const-string v20, "NearByDevicesBrowserFragment"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "onItemClick() : Type is folder - mNavigation : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 651
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v19, v0

    if-eqz v19, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    instance-of v0, v0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    move/from16 v19, v0

    if-eqz v19, :cond_0

    .line 653
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v19, v0

    check-cast v19, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    move-object/from16 v0, v19

    invoke-virtual {v0, v12, v13}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->goTo(J)Z

    move-result v19

    if-eqz v19, :cond_0

    .line 654
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mPathList:Ljava/util/ArrayList;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;)Ljava/util/ArrayList;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mCurTitle:Ljava/lang/String;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 658
    :cond_3
    const/16 v19, 0x0

    const-string v20, "NearByDevicesBrowserFragment"

    const-string v21, "onItemClick() : Type is file"

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 659
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v19, v0

    check-cast v19, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    move-object/from16 v0, v19

    invoke-virtual {v0, v12, v13}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->getItemOfId(J)Lcom/samsung/android/allshare/Item;

    move-result-object v11

    .line 660
    .local v11, "item":Lcom/samsung/android/allshare/Item;
    if-eqz v11, :cond_6

    .line 662
    new-instance v10, Landroid/content/Intent;

    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    .line 663
    .local v10, "intent":Landroid/content/Intent;
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mCurTitle:Ljava/lang/String;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 664
    .local v16, "title":Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v17

    .line 665
    .local v17, "uri":Landroid/net/Uri;
    const/16 v19, 0x0

    const-string v20, "NearByDevicesBrowserFragment"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "onItemClick() : Title : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v11}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 666
    const/16 v19, 0x0

    const-string v20, "NearByDevicesBrowserFragment"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "onItemClick() : Item\'s type : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v11}, Lcom/samsung/android/allshare/Item;->getType()Lcom/samsung/android/allshare/Item$MediaType;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 667
    invoke-virtual {v11}, Lcom/samsung/android/allshare/Item;->getType()Lcom/samsung/android/allshare/Item$MediaType;

    move-result-object v19

    sget-object v20, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_VIDEO:Lcom/samsung/android/allshare/Item$MediaType;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-ne v0, v1, :cond_4

    .line 669
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    .line 670
    .local v15, "sb":Ljava/lang/StringBuilder;
    const-string v19, "ss"

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 671
    invoke-virtual {v11}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 672
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v18

    .line 674
    .local v18, "uri2":Landroid/net/Uri;
    invoke-virtual {v11}, Lcom/samsung/android/allshare/Item;->getMimetype()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 681
    .end local v15    # "sb":Ljava/lang/StringBuilder;
    .end local v18    # "uri2":Landroid/net/Uri;
    :goto_1
    const-string v19, "android.intent.action.VIEW"

    move-object/from16 v0, v19

    invoke-virtual {v10, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 682
    const-string v19, "android.intent.extra.STREAM"

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 683
    const-string v19, "from-myfiles"

    const/16 v20, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 684
    const-string v19, "title_name"

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 685
    const-string v20, "NIC"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v19, v0

    check-cast v19, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->getProvider()Lcom/samsung/android/allshare/media/Provider;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/media/Provider;->getNIC()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 686
    const-string v20, "CurrentProviderName"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v19, v0

    check-cast v19, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->getProvider()Lcom/samsung/android/allshare/media/Provider;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/media/Provider;->getName()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 688
    if-eqz v10, :cond_5

    .line 692
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Lcom/sec/android/app/myfiles/AbsMainActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 694
    :catch_0
    move-exception v7

    .line 696
    .local v7, "e":Landroid/content/ActivityNotFoundException;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    move-object/from16 v20, v0

    const v21, 0x7f0b007f

    invoke-virtual/range {v20 .. v21}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x0

    invoke-static/range {v19 .. v21}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/widget/Toast;->show()V

    .line 698
    const/16 v19, 0x0

    const-string v20, "NearByDevicesBrowserFragment"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Cannot find activity for the mime type: "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual {v11}, Lcom/samsung/android/allshare/Item;->getMimetype()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 678
    .end local v7    # "e":Landroid/content/ActivityNotFoundException;
    :cond_4
    invoke-virtual {v11}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v19

    invoke-virtual {v11}, Lcom/samsung/android/allshare/Item;->getMimetype()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 702
    :cond_5
    const/16 v19, 0x0

    const-string v20, "NearByDevicesBrowserFragment"

    const-string v21, "onItemClick() : intent is null"

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 705
    .end local v10    # "intent":Landroid/content/Intent;
    .end local v16    # "title":Ljava/lang/String;
    .end local v17    # "uri":Landroid/net/Uri;
    :cond_6
    const/16 v19, 0x0

    const-string v20, "NearByDevicesBrowserFragment"

    const-string v21, "onItemClick() : item is null"

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 709
    .end local v11    # "item":Lcom/samsung/android/allshare/Item;
    :cond_7
    const/16 v19, 0x0

    const-string v20, "NearByDevicesBrowserFragment"

    const-string v21, "onItemClick() : Extension is null"

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 710
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    move-object/from16 v20, v0

    const v21, 0x7f0b007f

    invoke-virtual/range {v20 .. v21}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x0

    invoke-static/range {v19 .. v21}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 714
    .end local v8    # "extension":Ljava/lang/String;
    .end local v12    # "itemId":J
    :cond_8
    const/16 v19, 0x0

    const-string v20, "NearByDevicesBrowserFragment"

    const-string v21, "onItemClick() : cursor is null"

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 718
    .end local v2    # "cursor":Landroid/database/Cursor;
    :cond_9
    if-eqz p1, :cond_a

    .line 719
    const/16 v19, 0x0

    const-string v20, "NearByDevicesBrowserFragment"

    const-string v21, "onItemClick() : parentView.getAdapter() is null "

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 721
    :cond_a
    const/16 v19, 0x0

    const-string v20, "NearByDevicesBrowserFragment"

    const-string v21, "onItemClick() : parentView is null"

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 726
    :cond_b
    const/16 v19, 0x0

    const-string v20, "NearByDevicesBrowserFragment"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "onItemClick() : mAdapter.isSelectMode = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->isSelectMode()Z

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 727
    const/16 v19, 0x0

    const-string v20, "NearByDevicesBrowserFragment"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "onItemClick() : mRunFrom = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mRunFrom:I

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v19 .. v21}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 728
    invoke-virtual/range {p1 .. p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p3

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/database/Cursor;

    .line 729
    .restart local v2    # "cursor":Landroid/database/Cursor;
    const-string v19, "_id"

    move-object/from16 v0, v19

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    move/from16 v0, v19

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 730
    .local v4, "cursorId":J
    const-string v19, "_data"

    move-object/from16 v0, v19

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    move/from16 v0, v19

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 731
    .local v14, "path":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5, v14}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->multipleSelect(JLjava/lang/String;)V

    goto/16 :goto_0
.end method
