.class final Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$1;
.super Landroid/os/Handler;
.source "ThumbnailLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/utils/ThumbnailLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(Landroid/os/Looper;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Looper;

    .prologue
    .line 148
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 153
    iget v3, p1, Landroid/os/Message;->what:I

    if-nez v3, :cond_1

    .line 155
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;

    .line 157
    .local v1, "info":Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;
    iget-object v3, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mIconImageView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 159
    .local v2, "tag":Ljava/lang/String;
    if-eqz v2, :cond_1

    iget-object v3, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 161
    iget-object v3, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 163
    iget-object v3, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mThumbnailDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_1

    .line 165
    iget v3, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mViewMode:I

    if-ne v3, v6, :cond_4

    .line 167
    iget v3, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mFileType:I

    invoke-static {v3}, Lcom/sec/android/app/myfiles/MediaFile;->isInstallFileType(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 169
    iget-object v3, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mIconImageView:Landroid/widget/ImageView;

    iget-object v4, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mThumbnailDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 178
    :goto_0
    iget-object v3, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 180
    iget-object v3, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mDocExtnOverlay:Landroid/widget/ImageView;

    if-eqz v3, :cond_0

    .line 182
    sget-object v3, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;

    iget-object v4, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mPath:Ljava/lang/String;

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 185
    iget-object v3, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mDocExtnOverlay:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 198
    :cond_0
    :goto_1
    iget v3, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mFileType:I

    invoke-static {v3}, Lcom/sec/android/app/myfiles/MediaFile;->isVideoFileType(I)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 199
    iget v3, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mViewMode:I

    if-eq v3, v6, :cond_5

    .line 200
    iget-object v3, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mOverlayIconImageView:Landroid/widget/ImageView;

    const v4, 0x7f0200b8

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 209
    :goto_2
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-direct {v0, v3, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 211
    .local v0, "anim":Landroid/view/animation/AlphaAnimation;
    const-wide/16 v4, 0x1f4

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 212
    iget v3, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mViewMode:I

    if-ne v3, v6, :cond_8

    .line 214
    iget v3, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mFileType:I

    invoke-static {v3}, Lcom/sec/android/app/myfiles/MediaFile;->isInstallFileType(I)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 215
    iget-object v3, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mIconImageView:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 225
    .end local v0    # "anim":Landroid/view/animation/AlphaAnimation;
    .end local v1    # "info":Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;
    .end local v2    # "tag":Ljava/lang/String;
    :cond_1
    :goto_3
    return-void

    .line 173
    .restart local v1    # "info":Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;
    .restart local v2    # "tag":Ljava/lang/String;
    :cond_2
    iget-object v3, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mThumbnailImageView:Landroid/widget/ImageView;

    iget-object v4, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mThumbnailDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 174
    iget-object v3, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 175
    iget-object v3, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mIconImageView:Landroid/widget/ImageView;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 189
    :cond_3
    iget-object v3, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mDocExtnOverlay:Landroid/widget/ImageView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 195
    :cond_4
    iget-object v3, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mIconImageView:Landroid/widget/ImageView;

    iget-object v4, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mThumbnailDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 202
    :cond_5
    iget-object v3, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mOverlayIconImageView:Landroid/widget/ImageView;

    const v4, 0x7f0200d5

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 206
    :cond_6
    iget-object v3, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mOverlayIconImageView:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    .line 217
    .restart local v0    # "anim":Landroid/view/animation/AlphaAnimation;
    :cond_7
    iget-object v3, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mThumbnailImageView:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_3

    .line 220
    :cond_8
    iget-object v3, v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;->mIconImageView:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_3
.end method
