.class public Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;
.super Landroid/os/AsyncTask;
.source "FTPOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/os/Bundle;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Landroid/os/Bundle;",
        ">;>;"
    }
.end annotation


# instance fields
.field private volatile mAbortOperation:Z

.field private final mAction:Lcom/sec/android/app/myfiles/ftp/FTPAction;

.field private final mCannotInfo:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

.field private final mDialogSync:Ljava/lang/Object;

.field private final mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

.field private volatile mRequestSize:I


# direct methods
.method public constructor <init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/FTPAction;Z)V
    .locals 2
    .param p1, "arg"    # Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "subtitle"    # Ljava/lang/String;
    .param p4, "cannot"    # Ljava/lang/String;
    .param p5, "action"    # Lcom/sec/android/app/myfiles/ftp/FTPAction;
    .param p6, "cancellable"    # Z

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 141
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->mAbortOperation:Z

    .line 142
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->mRequestSize:I

    .line 37
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->mContext:Landroid/content/Context;

    .line 38
    iput-object p4, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->mCannotInfo:Ljava/lang/String;

    .line 39
    iput-object p5, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->mAction:Lcom/sec/android/app/myfiles/ftp/FTPAction;

    .line 40
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->getFTPConnectionHandler()Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    .line 41
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->getDialogSync()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->mDialogSync:Ljava/lang/Object;

    .line 43
    new-instance v0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->mDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->mDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->setTitle(Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->mDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    invoke-virtual {v0, p3}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->setSubtitle(Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->mDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    new-instance v1, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation$1;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->setAbortAction(Ljava/lang/Runnable;)V

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->mDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    invoke-virtual {v0, p6}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->setCancelButtonActive(Z)V

    .line 62
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;
    .param p1, "x1"    # Z

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->mAbortOperation:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;)Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->mDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    return-object v0
.end method

.method private setTitle(Ljava/lang/String;)V
    .locals 2
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation$2;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation$2;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 132
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 34
    check-cast p1, [Landroid/os/Bundle;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->doInBackground([Landroid/os/Bundle;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Landroid/os/Bundle;)Ljava/util/List;
    .locals 11
    .param p1, "requests"    # [Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Landroid/os/Bundle;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 68
    .local v3, "failed":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    if-nez p1, :cond_0

    .line 69
    const/4 v3, 0x0

    .line 103
    .end local v3    # "failed":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    :goto_0
    return-object v3

    .line 71
    .restart local v3    # "failed":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    :cond_0
    array-length v7, p1

    iput v7, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->mRequestSize:I

    .line 73
    iget-object v8, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->mDialogSync:Ljava/lang/Object;

    monitor-enter v8

    .line 75
    :try_start_0
    iget-object v9, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->mDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    iget-object v7, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->mContext:Landroid/content/Context;

    check-cast v7, Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    const-string v10, "fetching-dialog"

    invoke-virtual {v9, v7, v10}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 77
    if-eqz p1, :cond_1

    array-length v7, p1

    if-lez v7, :cond_1

    .line 79
    const/4 v7, 0x0

    aget-object v4, p1, v7

    .line 81
    .local v4, "first":Landroid/os/Bundle;
    const-string v7, "rename_to"

    invoke-virtual {v4, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 83
    const-string v7, "rename_to"

    invoke-virtual {v4, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->setTitle(Ljava/lang/String;)V

    .line 89
    .end local v4    # "first":Landroid/os/Bundle;
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v7}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncPrepareConnection()Z

    move-result v7

    if-nez v7, :cond_2

    .line 90
    monitor-exit v8

    goto :goto_0

    .line 102
    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v7

    .line 92
    :cond_2
    move-object v1, p1

    .local v1, "arr$":[Landroid/os/Bundle;
    :try_start_1
    array-length v6, v1

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_1
    if-ge v5, v6, :cond_5

    aget-object v0, v1, v5

    .line 93
    .local v0, "arg":Landroid/os/Bundle;
    iget-object v7, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->mAction:Lcom/sec/android/app/myfiles/ftp/FTPAction;

    invoke-interface {v7}, Lcom/sec/android/app/myfiles/ftp/FTPAction;->getWorkElement()Ljava/lang/String;

    move-result-object v2

    .line 94
    .local v2, "el":Ljava/lang/String;
    if-eqz v2, :cond_4

    .end local v2    # "el":Ljava/lang/String;
    :goto_2
    invoke-direct {p0, v2}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->setTitle(Ljava/lang/String;)V

    .line 95
    iget-boolean v7, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->mAbortOperation:Z

    if-nez v7, :cond_3

    .line 96
    iget-object v7, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->mAction:Lcom/sec/android/app/myfiles/ftp/FTPAction;

    invoke-interface {v7, v0}, Lcom/sec/android/app/myfiles/ftp/FTPAction;->work(Landroid/os/Bundle;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 97
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 94
    .restart local v2    # "el":Ljava/lang/String;
    :cond_4
    const-string v7, "title"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 101
    .end local v0    # "arg":Landroid/os/Bundle;
    .end local v2    # "el":Ljava/lang/String;
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->mDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->dismiss()V

    .line 102
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 34
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "result":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    const/4 v5, 0x1

    .line 108
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 109
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 110
    .local v2, "sb":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->mCannotInfo:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    iget v3, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->mRequestSize:I

    if-le v3, v5, :cond_0

    .line 112
    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    .line 114
    .local v1, "item":Landroid/os/Bundle;
    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    const-string v3, "title"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 118
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Landroid/os/Bundle;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 121
    .end local v2    # "sb":Ljava/lang/StringBuilder;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperation;->mAction:Lcom/sec/android/app/myfiles/ftp/FTPAction;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Lcom/sec/android/app/myfiles/ftp/FTPAction;->onDone(Landroid/os/Bundle;)V

    .line 123
    return-void
.end method
