.class public Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;
.source "FtpDetailFragment.java"


# static fields
.field public static final CONTAINS_POSITION:I = 0x2

.field private static final LAST_MOD_TIME_POSITION:I = 0x3

.field private static final NAME_POSITION:I = 0x0

.field private static final NUM_OF_ITEMS:I = 0x5

.field private static final PATH:I = 0x4

.field private static final SIZE_POSITION:I = 0x1


# instance fields
.field private mContains:Ljava/lang/String;

.field private mLastModTime:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mPath:Ljava/lang/String;

.field private mSize:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;-><init>()V

    .line 161
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->mContains:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected fillListView(Landroid/widget/ListView;Landroid/os/Bundle;)V
    .locals 8
    .param p1, "listView"    # Landroid/widget/ListView;
    .param p2, "arg"    # Landroid/os/Bundle;

    .prologue
    .line 31
    const-string v4, "detail_item_path"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 32
    .local v1, "path":Ljava/lang/String;
    const-string v4, "detail_item_index"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 34
    .local v0, "index":I
    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->getFileName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->mName:Ljava/lang/String;

    .line 36
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->getFileSize(Ljava/lang/String;I)J

    move-result-wide v6

    invoke-static {v4, v6, v7}, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->getSizeString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->mSize:Ljava/lang/String;

    .line 38
    const-string v4, "detail_files_count"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 39
    const-string v4, "detail_files_count"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    const-string v5, "detail_folder_count"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->getContainsString(II)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->mContains:Ljava/lang/String;

    .line 43
    :cond_0
    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->getFileLastModifiedTime(Ljava/lang/String;I)J

    move-result-wide v2

    .line 44
    .local v2, "lastModifiedTime":J
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4, v2, v3}, Lcom/sec/android/app/myfiles/utils/Utils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->mLastModTime:Ljava/lang/String;

    .line 46
    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->getFilePath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->mPath:Ljava/lang/String;

    .line 48
    new-instance v4, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v4, p0, v5}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->mDetailsAdapter:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;

    .line 50
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->mDetailsAdapter:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;

    invoke-virtual {p1, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 51
    return-void
.end method

.method protected getContainsString(II)Ljava/lang/String;
    .locals 7
    .param p1, "filesSelected"    # I
    .param p2, "foldersSelected"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 140
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 142
    .local v0, "sb":Ljava/lang/StringBuilder;
    if-lez p1, :cond_0

    .line 144
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f0c0000

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, p1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    :cond_0
    if-lez p2, :cond_2

    .line 149
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 151
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0001

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, p2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getFileLastModifiedTime(Ljava/lang/String;I)J
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 84
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->mAdapter:Landroid/widget/BaseAdapter;

    if-eqz v1, :cond_0

    .line 85
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v1, p2}, Landroid/widget/BaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 86
    .local v0, "cursor":Landroid/database/Cursor;
    const-string v1, "date"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 88
    .end local v0    # "cursor":Landroid/database/Cursor;
    :goto_0
    return-wide v2

    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public getFileName(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 55
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->mAdapter:Landroid/widget/BaseAdapter;

    if-eqz v1, :cond_0

    .line 56
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v1, p2}, Landroid/widget/BaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 57
    .local v0, "cursor":Landroid/database/Cursor;
    const-string v1, "title"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 59
    .end local v0    # "cursor":Landroid/database/Cursor;
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public getFilePath(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 64
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->mAdapter:Landroid/widget/BaseAdapter;

    if-eqz v1, :cond_0

    .line 65
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v1, p2}, Landroid/widget/BaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 66
    .local v0, "cursor":Landroid/database/Cursor;
    const-string v1, "_data"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 68
    .end local v0    # "cursor":Landroid/database/Cursor;
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public getFileSize(Ljava/lang/String;I)J
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 74
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->mAdapter:Landroid/widget/BaseAdapter;

    if-eqz v1, :cond_0

    .line 75
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v1, p2}, Landroid/widget/BaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 76
    .local v0, "cursor":Landroid/database/Cursor;
    const-string v1, "file_size"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 78
    .end local v0    # "cursor":Landroid/database/Cursor;
    :goto_0
    return-wide v2

    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method protected getInitValue(I)Ljava/lang/String;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 112
    packed-switch p1, :pswitch_data_0

    .line 124
    const-string v0, ""

    :goto_0
    return-object v0

    .line 114
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->mName:Ljava/lang/String;

    goto :goto_0

    .line 116
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->mSize:Ljava/lang/String;

    goto :goto_0

    .line 118
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->mContains:Ljava/lang/String;

    goto :goto_0

    .line 120
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->mLastModTime:Ljava/lang/String;

    goto :goto_0

    .line 122
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->mPath:Ljava/lang/String;

    goto :goto_0

    .line 112
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected getItemCount()I
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x5

    return v0
.end method

.method protected getLabel(I)Ljava/lang/String;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 94
    packed-switch p1, :pswitch_data_0

    .line 106
    const-string v0, ""

    :goto_0
    return-object v0

    .line 96
    :pswitch_0
    const v0, 0x7f0b0033

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 98
    :pswitch_1
    const v0, 0x7f0b0034

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 100
    :pswitch_2
    const v0, 0x7f0b00ab

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 102
    :pswitch_3
    const v0, 0x7f0b006e

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 104
    :pswitch_4
    const v0, 0x7f0b00e7

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 94
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected getSizeTextViewPosition()I
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x1

    return v0
.end method

.method protected setSize(Ljava/lang/String;)V
    .locals 0
    .param p1, "size"    # Ljava/lang/String;

    .prologue
    .line 172
    return-void
.end method
