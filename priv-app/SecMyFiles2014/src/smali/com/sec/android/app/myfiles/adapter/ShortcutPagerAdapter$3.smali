.class Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter$3;
.super Ljava/lang/Object;
.source "ShortcutPagerAdapter.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->showDeleteDailog(Lcom/sec/android/app/myfiles/element/ShortcutItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;

.field final synthetic val$item:Lcom/sec/android/app/myfiles/element/ShortcutItem;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;Lcom/sec/android/app/myfiles/element/ShortcutItem;)V
    .locals 0

    .prologue
    .line 945
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter$3;->this$0:Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;

    iput-object p2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter$3;->val$item:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v4, 0x0

    .line 950
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter$3;->this$0:Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter$3;->val$item:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter$3;->val$item:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteShortcut(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    .line 952
    .local v0, "deletedRows":I
    if-lez v0, :cond_1

    .line 953
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter$3;->this$0:Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b00d8

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 958
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter$3;->this$0:Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->categoryHomeFragment:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->access$100(Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;)Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->sendShortcutAdapterRefreshBroadCast()V

    .line 959
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 960
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter$3;->this$0:Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;)Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 962
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter$3;->this$0:Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "category_home"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 964
    :cond_0
    return-void

    .line 956
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter$3;->this$0:Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/ShortcutPagerAdapter;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0063

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
