.class public Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;
.super Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
.source "NearByDeviceFileBrowserAdapter.java"


# static fields
.field private static final DEFAULT_FILE_ICON:I = 0x7f0200bd

.field private static final DEFAULT_FILE_TEXT:Ljava/lang/String; = ""

.field private static final MODULE:Ljava/lang/String; = "NearByDeviceFileBrowserAdapter"

.field private static final PATHS_CACHE_SIZE:I = 0x80000

.field private static final THUMBANAIL_LAZY_OFFSET:I = 0x3e8

.field private static final THUMBNAIL_CACHE_SIZE:I = 0x100000


# instance fields
.field private final mEnclosingViews:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/widget/AdapterView",
            "<+",
            "Landroid/widget/Adapter;",
            ">;>;"
        }
    .end annotation
.end field

.field private mFileExtension:Ljava/lang/String;

.field private mFileName:Ljava/lang/String;

.field private mFileURI:Ljava/lang/String;

.field private mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

.field private mImageOnTheFront:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/widget/ImageView;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mLoadingThumbnailsCanceled:Z

.field private mMediaType:I

.field private mMimeType:Ljava/lang/String;

.field private mNearbyDrawableProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;

.field private mPaths:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mPreviousPosition:I

.field private mThumbnailCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private mThumbnailURI:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "c"    # Landroid/database/Cursor;
    .param p4, "flags"    # I
    .param p5, "navigation"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 109
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 71
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mImageOnTheFront:Ljava/util/HashMap;

    .line 73
    iput-object v1, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mFileURI:Ljava/lang/String;

    .line 74
    iput-object v1, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mThumbnailURI:Ljava/lang/String;

    .line 75
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mMediaType:I

    .line 76
    iput-object v1, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mFileName:Ljava/lang/String;

    .line 77
    iput-object v1, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mFileExtension:Ljava/lang/String;

    .line 78
    iput-object v1, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mMimeType:Ljava/lang/String;

    .line 80
    iput-object v1, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mNearbyDrawableProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;

    .line 81
    iput-boolean v2, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mLoadingThumbnailsCanceled:Z

    .line 90
    iput-object v1, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 103
    iput v2, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mPreviousPosition:I

    .line 104
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mEnclosingViews:Ljava/util/HashMap;

    .line 111
    new-instance v0, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;

    invoke-direct {v0, p1}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mNearbyDrawableProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;

    .line 113
    new-instance v0, Landroid/util/LruCache;

    const/high16 v1, 0x100000

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mThumbnailCache:Landroid/util/LruCache;

    .line 114
    new-instance v0, Landroid/util/LruCache;

    const/high16 v1, 0x80000

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mPaths:Landroid/util/LruCache;

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mContext:Landroid/content/Context;

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-static {v0, v1}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    new-instance v0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 121
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;)Landroid/util/LruCache;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mThumbnailCache:Landroid/util/LruCache;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mImageOnTheFront:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;Landroid/widget/AdapterView;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;
    .param p1, "x1"    # Landroid/widget/AdapterView;
    .param p2, "x2"    # I

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->isPositionVisible(Landroid/widget/AdapterView;I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    .prologue
    .line 65
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mLoadingThumbnailsCanceled:Z

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;)Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mNearbyDrawableProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;

    return-object v0
.end method

.method private getDefaultFileIcon()I
    .locals 3

    .prologue
    const v0, 0x7f0200bd

    .line 572
    iget v1, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mMediaType:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 574
    iget v1, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mMediaType:I

    packed-switch v1, :pswitch_data_0

    .line 595
    :cond_0
    :goto_0
    return v0

    .line 577
    :pswitch_0
    const v0, 0x7f0200aa

    goto :goto_0

    .line 580
    :pswitch_1
    const v0, 0x7f0200af

    goto :goto_0

    .line 583
    :pswitch_2
    const v0, 0x7f0200b4

    goto :goto_0

    .line 586
    :pswitch_3
    const v0, 0x7f0200c2

    goto :goto_0

    .line 574
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getFileText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 603
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mFileName:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mFileExtension:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 605
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowFileExtensionStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 607
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mFileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mFileExtension:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 619
    :goto_0
    return-object v0

    .line 609
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mFileName:Ljava/lang/String;

    goto :goto_0

    .line 613
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mFileName:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mFileExtension:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 615
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mFileName:Ljava/lang/String;

    goto :goto_0

    .line 619
    :cond_2
    const-string v0, ""

    goto :goto_0
.end method

.method private getVisibleView()Landroid/widget/AdapterView;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/widget/AdapterView",
            "<+",
            "Landroid/widget/Adapter;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x2

    .line 528
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mViewMode:I

    if-ne v0, v1, :cond_0

    .line 529
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mEnclosingViews:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/AdapterView;

    .line 531
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mEnclosingViews:Ljava/util/HashMap;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/AdapterView;

    goto :goto_0
.end method

.method private handleSetIcon(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;Ljava/lang/String;Landroid/widget/AdapterView;IZ)V
    .locals 13
    .param p1, "viewHolder"    # Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;
    .param p2, "thumbnailUri"    # Ljava/lang/String;
    .param p4, "position"    # I
    .param p5, "isVideo"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;",
            "Ljava/lang/String;",
            "Landroid/widget/AdapterView",
            "<+",
            "Landroid/widget/Adapter;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    .line 427
    .local p3, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<+Landroid/widget/Adapter;>;"
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mMediaType:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    if-nez p2, :cond_3

    .line 429
    :cond_0
    iget-object v7, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    .line 444
    .local v7, "thumbnailImageView":Landroid/widget/ImageView;
    :goto_0
    if-eqz v7, :cond_2

    .line 446
    const/4 v12, 0x0

    .line 448
    .local v12, "thumbnailDrawable":Landroid/graphics/drawable/Drawable;
    if-eqz p2, :cond_1

    .line 449
    const/4 v0, 0x0

    const-string v1, "NearByDeviceFileBrowserAdapter"

    const-string v2, "handleSetIcon - mThumbnailURI != null"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 450
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mThumbnailCache:Landroid/util/LruCache;

    invoke-virtual {v0, p2}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    .end local v12    # "thumbnailDrawable":Landroid/graphics/drawable/Drawable;
    check-cast v12, Landroid/graphics/drawable/Drawable;

    .line 453
    .restart local v12    # "thumbnailDrawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    if-eqz v12, :cond_5

    .line 455
    invoke-virtual {v7, v12}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 456
    if-eqz p5, :cond_2

    iget-object v0, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 457
    iget-object v0, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 523
    .end local v12    # "thumbnailDrawable":Landroid/graphics/drawable/Drawable;
    :cond_2
    :goto_1
    return-void

    .line 435
    .end local v7    # "thumbnailImageView":Landroid/widget/ImageView;
    :cond_3
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mViewMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 436
    iget-object v7, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    .restart local v7    # "thumbnailImageView":Landroid/widget/ImageView;
    goto :goto_0

    .line 439
    .end local v7    # "thumbnailImageView":Landroid/widget/ImageView;
    :cond_4
    iget-object v7, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    .restart local v7    # "thumbnailImageView":Landroid/widget/ImageView;
    goto :goto_0

    .line 463
    .restart local v12    # "thumbnailDrawable":Landroid/graphics/drawable/Drawable;
    :cond_5
    if-eqz p2, :cond_2

    .line 503
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 505
    .local v4, "uri":Landroid/net/Uri;
    if-eqz p3, :cond_6

    .line 507
    new-instance v11, Landroid/os/Handler;

    invoke-direct {v11}, Landroid/os/Handler;-><init>()V

    .line 508
    .local v11, "handler":Landroid/os/Handler;
    new-instance v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$2;

    move-object v1, p0

    move-object/from16 v2, p3

    move/from16 v3, p4

    move-object v5, p1

    move/from16 v6, p5

    move-object v8, p2

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$2;-><init>(Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;Landroid/widget/AdapterView;ILandroid/net/Uri;Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;ZLandroid/widget/ImageView;Ljava/lang/String;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v11, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 518
    .end local v11    # "handler":Landroid/os/Handler;
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mNearbyDrawableProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;

    new-instance v5, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1UpdateThumbnail;

    move-object v6, p0

    move-object v8, p2

    move/from16 v9, p5

    move-object v10, p1

    invoke-direct/range {v5 .. v10}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1UpdateThumbnail;-><init>(Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;Landroid/widget/ImageView;Ljava/lang/String;ZLcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;)V

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;->setDrawable(Landroid/net/Uri;Lcom/sec/android/app/myfiles/nearbydevices/DrawableUpdatable;)V

    goto :goto_1
.end method

.method private handleSetIcon(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;Ljava/lang/String;Z)V
    .locals 6
    .param p1, "viewHolder"    # Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;
    .param p2, "thumbnailUri"    # Ljava/lang/String;
    .param p3, "isVideo"    # Z

    .prologue
    .line 418
    const/4 v3, 0x0

    const/4 v4, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->handleSetIcon(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;Ljava/lang/String;Landroid/widget/AdapterView;IZ)V

    .line 419
    return-void
.end method

.method private handleSetText(Landroid/widget/TextView;)V
    .locals 4
    .param p1, "fileName"    # Landroid/widget/TextView;

    .prologue
    .line 542
    move-object v0, p1

    .line 544
    .local v0, "label":Landroid/widget/TextView;
    if-eqz v0, :cond_2

    .line 546
    const/4 v1, 0x0

    .line 548
    .local v1, "labelText":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mFileName:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 549
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowFileExtensionStatus()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 551
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->getFileText()Ljava/lang/String;

    move-result-object v1

    .line 557
    :cond_0
    :goto_0
    if-nez v1, :cond_1

    .line 559
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->getFileText()Ljava/lang/String;

    move-result-object v1

    .line 560
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mPaths:Landroid/util/LruCache;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mFileName:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564
    :cond_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 567
    .end local v1    # "labelText":Ljava/lang/String;
    :cond_2
    return-void

    .line 553
    .restart local v1    # "labelText":Ljava/lang/String;
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mPaths:Landroid/util/LruCache;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mFileName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "labelText":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .restart local v1    # "labelText":Ljava/lang/String;
    goto :goto_0
.end method

.method private isPositionVisible(Landroid/widget/AdapterView;I)Z
    .locals 1
    .param p2, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<+",
            "Landroid/widget/Adapter;",
            ">;I)Z"
        }
    .end annotation

    .prologue
    .line 537
    .local p1, "view":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<+Landroid/widget/Adapter;>;"
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getFirstVisiblePosition()I

    move-result v0

    if-gt v0, p2, :cond_0

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getLastVisiblePosition()I

    move-result v0

    if-lt v0, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 22
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 149
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentView()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mViewMode:I

    .line 151
    const-string v2, "name"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 152
    .local v15, "iname":I
    const/4 v2, -0x1

    if-eq v15, v2, :cond_5

    .line 154
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;

    .line 155
    .local v3, "vh":Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    move-object/from16 v0, p3

    invoke-interface {v0, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 156
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v2, :cond_0

    .line 157
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 158
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 160
    :cond_0
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    if-eqz v2, :cond_1

    .line 161
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 163
    :cond_1
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    if-eqz v2, :cond_2

    .line 164
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 166
    :cond_2
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    if-eqz v2, :cond_3

    .line 167
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 169
    :cond_3
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    if-eqz v2, :cond_4

    .line 170
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const v4, 0x7f020001

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 372
    :cond_4
    :goto_0
    return-void

    .line 176
    .end local v3    # "vh":Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;
    :cond_5
    const-string v2, "_data"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mFileURI:Ljava/lang/String;

    .line 177
    const-string v2, "ThumbUri"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mThumbnailURI:Ljava/lang/String;

    .line 178
    const-string v2, "MediaType"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mMediaType:I

    .line 179
    const-string v2, "title"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mFileName:Ljava/lang/String;

    .line 180
    const-string v2, "Extension"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mFileExtension:Ljava/lang/String;

    .line 181
    const-string v2, "MimeType"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mMimeType:Ljava/lang/String;

    .line 183
    const-string v2, "Date"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 184
    .local v12, "date":J
    const-string v2, "FileSize"

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 186
    .local v16, "size":J
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;

    .line 188
    .restart local v3    # "vh":Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;
    invoke-super/range {p0 .. p3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 190
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v2, :cond_6

    .line 191
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 194
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mImageOnTheFront:Ljava/util/HashMap;

    monitor-enter v4

    .line 196
    :try_start_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mMediaType:I

    const/4 v7, 0x1

    if-ne v2, v7, :cond_9

    .line 198
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v2, :cond_7

    .line 199
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    const/4 v7, 0x0

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 201
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mImageOnTheFront:Ljava/util/HashMap;

    iget-object v7, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mThumbnailURI:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v2, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    :goto_1
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 213
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->getVisibleView()Landroid/widget/AdapterView;

    move-result-object v5

    .line 215
    .local v5, "visibleView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<+Landroid/widget/Adapter;>;"
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    if-eqz v2, :cond_8

    .line 216
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 217
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mMediaType:I

    const/4 v4, 0x4

    if-ne v2, v4, :cond_8

    .line 218
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mViewMode:I

    const/4 v4, 0x2

    if-eq v2, v4, :cond_b

    .line 219
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    const v4, 0x7f0200b8

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 226
    :cond_8
    :goto_2
    if-eqz v5, :cond_d

    .line 228
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->handleSetText(Landroid/widget/TextView;)V

    .line 229
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->getDefaultFileIcon()I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 230
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mThumbnailURI:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mPreviousPosition:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mMediaType:I

    const/4 v7, 0x4

    if-ne v2, v7, :cond_c

    const/4 v7, 0x1

    :goto_3
    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->handleSetIcon(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;Ljava/lang/String;Landroid/widget/AdapterView;IZ)V

    .line 239
    :goto_4
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mViewMode:I

    packed-switch v2, :pswitch_data_0

    .line 268
    :goto_5
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mMediaType:I

    const/4 v4, 0x1

    if-eq v2, v4, :cond_4

    .line 270
    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mMediaType:I

    .line 271
    .local v8, "fileType":I
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mThumbnailURI:Ljava/lang/String;

    .line 272
    .local v9, "thumbFileUri":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mFileURI:Ljava/lang/String;

    .line 273
    .local v10, "fileUri":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mMimeType:Ljava/lang/String;

    .line 275
    .local v11, "fileMimeType":Ljava/lang/String;
    new-instance v6, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1;

    move-object/from16 v7, p0

    invoke-direct/range {v6 .. v11}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1;-><init>(Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    .local v6, "hoverListener":Landroid/view/View$OnHoverListener;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mSelectModeFrom:I

    const/4 v4, 0x2

    if-eq v2, v4, :cond_4

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mSelectModeFrom:I

    const/4 v4, 0x1

    if-eq v2, v4, :cond_4

    .line 367
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    goto/16 :goto_0

    .line 203
    .end local v5    # "visibleView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<+Landroid/widget/Adapter;>;"
    .end local v6    # "hoverListener":Landroid/view/View$OnHoverListener;
    .end local v8    # "fileType":I
    .end local v9    # "thumbFileUri":Ljava/lang/String;
    .end local v10    # "fileUri":Ljava/lang/String;
    .end local v11    # "fileMimeType":Ljava/lang/String;
    :cond_9
    :try_start_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mViewMode:I

    const/4 v7, 0x2

    if-ne v2, v7, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mThumbnailURI:Ljava/lang/String;

    if-eqz v2, :cond_a

    .line 205
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mImageOnTheFront:Ljava/util/HashMap;

    iget-object v7, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mThumbnailURI:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v2, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 211
    :catchall_0
    move-exception v2

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 209
    :cond_a
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mImageOnTheFront:Ljava/util/HashMap;

    iget-object v7, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mThumbnailURI:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v2, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 221
    .restart local v5    # "visibleView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<+Landroid/widget/Adapter;>;"
    :cond_b
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    const v4, 0x7f0200d5

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_2

    .line 230
    :cond_c
    const/4 v7, 0x0

    goto :goto_3

    .line 234
    :cond_d
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->handleSetText(Landroid/widget/TextView;)V

    .line 235
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->getDefaultFileIcon()I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 236
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mThumbnailURI:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mMediaType:I

    const/4 v7, 0x4

    if-ne v2, v7, :cond_e

    const/4 v2, 0x1

    :goto_6
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v2}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->handleSetIcon(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;Ljava/lang/String;Z)V

    goto/16 :goto_4

    :cond_e
    const/4 v2, 0x0

    goto :goto_6

    .line 243
    :pswitch_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mMediaType:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_11

    .line 245
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 246
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 254
    :goto_7
    const-string v14, ""

    .local v14, "dateStr":Ljava/lang/String;
    const-string v18, ""

    .line 255
    .local v18, "sizeStr":Ljava/lang/String;
    const-wide/16 v20, 0x0

    cmp-long v2, v12, v20

    if-lez v2, :cond_f

    .line 256
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2, v12, v13}, Lcom/sec/android/app/myfiles/utils/Utils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v14

    .line 258
    :cond_f
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    invoke-virtual {v2, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 260
    const-wide/16 v20, 0x0

    cmp-long v2, v16, v20

    if-lez v2, :cond_10

    .line 261
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mContext:Landroid/content/Context;

    move-wide/from16 v0, v16

    invoke-static {v2, v0, v1}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v18

    .line 263
    :cond_10
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 250
    .end local v14    # "dateStr":Ljava/lang/String;
    .end local v18    # "sizeStr":Ljava/lang/String;
    :cond_11
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 251
    iget-object v2, v3, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_7

    .line 239
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public cancelThumbnailsLoading(Z)V
    .locals 1
    .param p1, "loadingCanceled"    # Z

    .prologue
    .line 138
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mLoadingThumbnailsCanceled:Z

    .line 139
    if-eqz p1, :cond_0

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mNearbyDrawableProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDrawableProvider;->abort()V

    .line 143
    :cond_0
    return-void
.end method

.method public clearCache()V
    .locals 1

    .prologue
    .line 632
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mThumbnailCache:Landroid/util/LruCache;

    if-eqz v0, :cond_0

    .line 633
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mThumbnailCache:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 634
    :cond_0
    return-void
.end method

.method protected getContentType()I
    .locals 1

    .prologue
    .line 413
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mMediaType:I

    return v0
.end method

.method public getPath()Landroid/util/LruCache;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 628
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mPaths:Landroid/util/LruCache;

    return-object v0
.end method

.method public getSelectedItem()Ljava/util/ArrayList;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 378
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 379
    .local v11, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    .line 381
    .local v9, "navi":Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;
    if-eqz v9, :cond_1

    .line 383
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->getSelectedItemPosition()Ljava/util/ArrayList;

    move-result-object v12

    .line 384
    .local v12, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    .line 386
    .local v7, "i":Ljava/lang/Integer;
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v10

    .line 387
    .local v10, "objItem":Ljava/lang/Object;
    instance-of v0, v10, Landroid/database/Cursor;

    if-eqz v0, :cond_0

    move-object v6, v10

    .line 389
    check-cast v6, Landroid/database/Cursor;

    .line 390
    .local v6, "cItem":Landroid/database/Cursor;
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 391
    .local v2, "id":J
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 393
    .local v4, "path":Ljava/lang/String;
    invoke-virtual {v9, v2, v3}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->getItemOfId(J)Lcom/samsung/android/allshare/Item;

    move-result-object v5

    .line 394
    .local v5, "item":Lcom/samsung/android/allshare/Item;
    if-eqz v5, :cond_0

    .line 395
    new-instance v0, Lcom/sec/android/app/myfiles/element/BrowserItem;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->getContentType()I

    move-result v1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/element/BrowserItem;-><init>(IJLjava/lang/String;Lcom/samsung/android/allshare/Item;)V

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 401
    .end local v2    # "id":J
    .end local v4    # "path":Ljava/lang/String;
    .end local v5    # "item":Lcom/samsung/android/allshare/Item;
    .end local v6    # "cItem":Landroid/database/Cursor;
    .end local v7    # "i":Ljava/lang/Integer;
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v10    # "objItem":Ljava/lang/Object;
    .end local v12    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_1
    return-object v11
.end method

.method public getSelectedItemForSearchDownload(I)Ljava/util/ArrayList;
    .locals 14
    .param p1, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 638
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 639
    .local v11, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v9

    check-cast v9, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    .line 641
    .local v9, "navi":Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;
    if-eqz v9, :cond_1

    .line 642
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 643
    .local v12, "selectedItemPos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 644
    move-object v13, v12

    .line 645
    .local v13, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    .line 647
    .local v7, "i":Ljava/lang/Integer;
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v10

    .line 648
    .local v10, "objItem":Ljava/lang/Object;
    instance-of v0, v10, Landroid/database/Cursor;

    if-eqz v0, :cond_0

    move-object v6, v10

    .line 650
    check-cast v6, Landroid/database/Cursor;

    .line 651
    .local v6, "cItem":Landroid/database/Cursor;
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 652
    .local v2, "id":J
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 654
    .local v4, "path":Ljava/lang/String;
    invoke-virtual {v9, v2, v3}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->getItemOfId(J)Lcom/samsung/android/allshare/Item;

    move-result-object v5

    .line 655
    .local v5, "item":Lcom/samsung/android/allshare/Item;
    if-eqz v5, :cond_0

    .line 656
    new-instance v0, Lcom/sec/android/app/myfiles/element/BrowserItem;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->getContentType()I

    move-result v1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/element/BrowserItem;-><init>(IJLjava/lang/String;Lcom/samsung/android/allshare/Item;)V

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 662
    .end local v2    # "id":J
    .end local v4    # "path":Ljava/lang/String;
    .end local v5    # "item":Lcom/samsung/android/allshare/Item;
    .end local v6    # "cItem":Landroid/database/Cursor;
    .end local v7    # "i":Ljava/lang/Integer;
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v10    # "objItem":Ljava/lang/Object;
    .end local v12    # "selectedItemPos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v13    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_1
    return-object v11
.end method

.method public getThumbnailCache()Landroid/util/LruCache;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 624
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mThumbnailCache:Landroid/util/LruCache;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "position"    # I
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # Landroid/view/ViewGroup;

    .prologue
    .line 131
    iput p1, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mPreviousPosition:I

    .line 133
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public setEnclosingViews(Ljava/lang/String;Landroid/widget/AdapterView;)V
    .locals 1
    .param p1, "mode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/widget/AdapterView",
            "<+",
            "Landroid/widget/Adapter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 125
    .local p2, "curView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<+Landroid/widget/Adapter;>;"
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mEnclosingViews:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    return-void
.end method
