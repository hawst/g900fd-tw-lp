.class public Lcom/sec/android/app/myfiles/compression/CompressManager;
.super Ljava/lang/Object;
.source "CompressManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/compression/CompressManager$OnCompressorListener;
    }
.end annotation


# static fields
.field public static final OPERATION_COMPRESS:I = 0x0

.field public static final OPERATION_EXTRACT:I = 0x1


# instance fields
.field protected mCompressorListener:Lcom/sec/android/app/myfiles/compression/CompressManager$OnCompressorListener;

.field private mCurrentCompressor:Lcom/sec/android/app/myfiles/compression/AbsCompressor;

.field private mOperationListener:Lcom/sec/android/app/myfiles/compression/AbsCompressor$OnOperationListener;

.field protected mProgressBar:Landroid/widget/ProgressBar;

.field protected mProgressBarPercent:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object v0, p0, Lcom/sec/android/app/myfiles/compression/CompressManager;->mCurrentCompressor:Lcom/sec/android/app/myfiles/compression/AbsCompressor;

    .line 36
    iput-object v0, p0, Lcom/sec/android/app/myfiles/compression/CompressManager;->mProgressBar:Landroid/widget/ProgressBar;

    .line 38
    iput-object v0, p0, Lcom/sec/android/app/myfiles/compression/CompressManager;->mProgressBarPercent:Landroid/widget/TextView;

    .line 104
    new-instance v0, Lcom/sec/android/app/myfiles/compression/CompressManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/compression/CompressManager$1;-><init>(Lcom/sec/android/app/myfiles/compression/CompressManager;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/compression/CompressManager;->mOperationListener:Lcom/sec/android/app/myfiles/compression/AbsCompressor$OnOperationListener;

    .line 44
    new-instance v0, Lcom/sec/android/app/myfiles/compression/ZipCompressor;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/compression/ZipCompressor;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/compression/CompressManager;->mCurrentCompressor:Lcom/sec/android/app/myfiles/compression/AbsCompressor;

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/myfiles/compression/CompressManager;->mCurrentCompressor:Lcom/sec/android/app/myfiles/compression/AbsCompressor;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/compression/CompressManager;->mOperationListener:Lcom/sec/android/app/myfiles/compression/AbsCompressor$OnOperationListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->setOnOperationListener(Lcom/sec/android/app/myfiles/compression/AbsCompressor$OnOperationListener;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;)V
    .locals 1
    .param p1, "fileOperationTask"    # Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/compression/CompressManager;-><init>()V

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/myfiles/compression/CompressManager;->mCurrentCompressor:Lcom/sec/android/app/myfiles/compression/AbsCompressor;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/myfiles/compression/CompressManager;->mCurrentCompressor:Lcom/sec/android/app/myfiles/compression/AbsCompressor;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->setFileOperationTask(Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;)V

    .line 57
    :cond_0
    return-void
.end method


# virtual methods
.method public doCompress(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 1
    .param p1, "curFolder"    # Ljava/lang/String;
    .param p3, "dst"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 85
    .local p2, "srcList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/sec/android/app/myfiles/compression/CompressManager;->mCurrentCompressor:Lcom/sec/android/app/myfiles/compression/AbsCompressor;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/myfiles/compression/CompressManager;->mCurrentCompressor:Lcom/sec/android/app/myfiles/compression/AbsCompressor;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->doCompress(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 89
    :cond_0
    return-void
.end method

.method public doExtract(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "src"    # Ljava/lang/String;
    .param p3, "dst"    # Ljava/lang/String;
    .param p4, "type"    # I

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/app/myfiles/compression/CompressManager;->mCurrentCompressor:Lcom/sec/android/app/myfiles/compression/AbsCompressor;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/myfiles/compression/CompressManager;->mCurrentCompressor:Lcom/sec/android/app/myfiles/compression/AbsCompressor;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->doExtract(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 97
    :cond_0
    return-void
.end method

.method public setOnCompressorListener(Lcom/sec/android/app/myfiles/compression/CompressManager$OnCompressorListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/myfiles/compression/CompressManager$OnCompressorListener;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/sec/android/app/myfiles/compression/CompressManager;->mCompressorListener:Lcom/sec/android/app/myfiles/compression/CompressManager$OnCompressorListener;

    .line 102
    return-void
.end method

.method public setProgressBar(Landroid/widget/ProgressBar;)V
    .locals 1
    .param p1, "progressBar"    # Landroid/widget/ProgressBar;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/myfiles/compression/CompressManager;->mCurrentCompressor:Lcom/sec/android/app/myfiles/compression/AbsCompressor;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/myfiles/compression/CompressManager;->mCurrentCompressor:Lcom/sec/android/app/myfiles/compression/AbsCompressor;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->setProgressBar(Landroid/widget/ProgressBar;)V

    .line 65
    :cond_0
    return-void
.end method

.method public setProgressBarCounter(Landroid/widget/TextView;)V
    .locals 1
    .param p1, "countText"    # Landroid/widget/TextView;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/myfiles/compression/CompressManager;->mCurrentCompressor:Lcom/sec/android/app/myfiles/compression/AbsCompressor;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/myfiles/compression/CompressManager;->mCurrentCompressor:Lcom/sec/android/app/myfiles/compression/AbsCompressor;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->setProgressBarCounter(Landroid/widget/TextView;)V

    .line 81
    :cond_0
    return-void
.end method

.method public setProgressBarPercent(Landroid/widget/TextView;)V
    .locals 1
    .param p1, "percent"    # Landroid/widget/TextView;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/compression/CompressManager;->mCurrentCompressor:Lcom/sec/android/app/myfiles/compression/AbsCompressor;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/myfiles/compression/CompressManager;->mCurrentCompressor:Lcom/sec/android/app/myfiles/compression/AbsCompressor;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->setProgressBarPercent(Landroid/widget/TextView;)V

    .line 73
    :cond_0
    return-void
.end method
