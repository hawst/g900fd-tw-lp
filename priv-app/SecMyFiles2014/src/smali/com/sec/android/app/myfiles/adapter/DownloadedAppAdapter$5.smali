.class Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$5;
.super Ljava/lang/Object;
.source "DownloadedAppAdapter.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;)V
    .locals 0

    .prologue
    .line 674
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$5;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;)I
    .locals 6
    .param p1, "lhs"    # Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    .param p2, "rhs"    # Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .prologue
    .line 679
    const/4 v0, 0x0

    .line 681
    .local v0, "result":I
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 683
    iget-wide v2, p1, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->size:J

    iget-wide v4, p2, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->size:J

    sub-long/2addr v2, v4

    long-to-int v0, v2

    .line 686
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$5;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    iget v1, v1, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mCurrentInOrder:I

    if-nez v1, :cond_1

    .end local v0    # "result":I
    :goto_0
    return v0

    .restart local v0    # "result":I
    :cond_1
    neg-int v0, v0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 674
    check-cast p1, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$5;->compare(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;)I

    move-result v0

    return v0
.end method
