.class public Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;
.super Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
.source "ShortcutAdapter.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/ftp/IFTPSessionListener;
.implements Lcom/sec/android/app/myfiles/nearbydevices/INearbyDevicesShortcutAdapter;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mIsItemViewLayerTypeSoftware:Z

.field private mIsSelectMode:Z

.field private mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/ShortcutItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/ShortcutItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 56
    .local p2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/ShortcutItem;>;"
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mIsItemViewLayerTypeSoftware:Z

    .line 58
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mContext:Landroid/content/Context;

    .line 60
    if-nez p2, :cond_0

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    .line 70
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->refresh()V

    .line 71
    return-void

    .line 66
    :cond_0
    iput-object p2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public addItem(ILcom/sec/android/app/myfiles/element/ShortcutItem;)V
    .locals 2
    .param p1, "position"    # I
    .param p2, "item"    # Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .prologue
    .line 84
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v1

    .line 85
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 86
    monitor-exit v1

    .line 87
    return-void

    .line 86
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addItem(Lcom/sec/android/app/myfiles/element/ShortcutItem;)V
    .locals 2
    .param p1, "item"    # Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .prologue
    .line 76
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v1

    .line 77
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    monitor-exit v1

    .line 79
    return-void

    .line 78
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public clear()V
    .locals 6

    .prologue
    .line 121
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v4

    .line 123
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 125
    .local v2, "removeItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/ShortcutItem;>;"
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 127
    .local v1, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v3

    const/16 v5, 0x1a

    if-eq v3, v5, :cond_1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v3

    const/16 v5, 0x1c

    if-ne v3, v5, :cond_0

    .line 129
    :cond_1
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 134
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    .end local v2    # "removeItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/ShortcutItem;>;"
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 133
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v2    # "removeItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/ShortcutItem;>;"
    :cond_2
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 134
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 135
    return-void
.end method

.method public finishSelectMode()V
    .locals 1

    .prologue
    .line 478
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mIsSelectMode:Z

    .line 480
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->unselectAllItem()V

    .line 481
    return-void
.end method

.method public getCount()I
    .locals 3

    .prologue
    .line 176
    const/4 v0, 0x0

    .line 178
    .local v0, "value":I
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v2

    .line 180
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 181
    monitor-exit v2

    .line 183
    return v0

    .line 181
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 216
    const/4 v1, 0x0

    .line 220
    .local v1, "obj":Ljava/lang/Object;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 222
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 223
    monitor-exit v3

    .line 230
    .end local v1    # "obj":Ljava/lang/Object;
    :goto_0
    return-object v1

    .line 223
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 225
    :catch_0
    move-exception v0

    .line 227
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getItemByType(I)Ljava/lang/Object;
    .locals 6
    .param p1, "type"    # I

    .prologue
    .line 236
    const/4 v3, 0x0

    .line 240
    .local v3, "obj":Ljava/lang/Object;
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v5
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 242
    :try_start_1
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 244
    .local v2, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v4

    if-ne v4, p1, :cond_0

    .line 246
    move-object v3, v2

    .line 251
    .end local v2    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    .end local v3    # "obj":Ljava/lang/Object;
    :cond_1
    monitor-exit v5

    .line 258
    .end local v1    # "i$":Ljava/util/Iterator;
    :goto_0
    return-object v3

    .line 251
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v4
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 253
    :catch_0
    move-exception v0

    .line 255
    .local v0, "e":Ljava/lang/Exception;
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getItemCount(I)I
    .locals 5
    .param p1, "itemType"    # I

    .prologue
    .line 189
    const/4 v2, 0x0

    .line 191
    .local v2, "result":I
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v4

    .line 193
    const/4 v3, -0x1

    if-ne p1, v3, :cond_1

    .line 195
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 207
    :cond_0
    monitor-exit v4

    .line 209
    return v2

    .line 199
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 201
    .local v1, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v3

    if-ne v3, p1, :cond_2

    .line 203
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 207
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public getItemId(I)J
    .locals 7
    .param p1, "position"    # I

    .prologue
    .line 303
    const-wide/16 v2, -0x1

    .line 307
    .local v2, "id":J
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v6
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 309
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v1

    int-to-long v2, v1

    .line 310
    monitor-exit v6

    move-wide v4, v2

    .line 317
    .end local v2    # "id":J
    .local v4, "id":J
    :goto_0
    return-wide v4

    .line 310
    .end local v4    # "id":J
    .restart local v2    # "id":J
    :catchall_0
    move-exception v1

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 312
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    move-wide v4, v2

    .line 314
    .end local v2    # "id":J
    .restart local v4    # "id":J
    goto :goto_0
.end method

.method public getSelectedItem()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 264
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 266
    .local v2, "selectedItem":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v4

    .line 268
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 270
    .local v1, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 272
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 275
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 277
    return-object v2
.end method

.method public getSelectedItemCount()I
    .locals 5

    .prologue
    .line 283
    const/4 v2, 0x0

    .line 285
    .local v2, "result":I
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v4

    .line 287
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 289
    .local v1, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 291
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 294
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :cond_1
    monitor-exit v4

    .line 296
    return v2

    .line 294
    .end local v0    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public getSelectedItemPosition()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 607
    const/4 v0, 0x0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 323
    iget-object v8, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mContext:Landroid/content/Context;

    const-string v9, "layout_inflater"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 325
    .local v1, "inflater":Landroid/view/LayoutInflater;
    iget-object v8, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v8, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 327
    .local v2, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    const/4 v4, 0x0

    .line 329
    .local v4, "select":Landroid/widget/CheckBox;
    const/4 v0, 0x0

    .line 331
    .local v0, "icon":Landroid/widget/ImageView;
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v3

    .line 333
    .local v3, "itemType":I
    if-nez p2, :cond_0

    .line 335
    iget-object v8, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 337
    const v8, 0x7f04001e

    const/4 v9, 0x0

    invoke-virtual {v1, v8, p3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 343
    :cond_0
    :goto_0
    const v8, 0x7f0f0092

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    .line 345
    .local v6, "shortcutItemContainer":Landroid/view/ViewGroup;
    const v8, 0x7f0f0095

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    .line 347
    .local v5, "shortcutItemAddContainer":Landroid/view/ViewGroup;
    const/16 v8, 0x1a

    if-eq v3, v8, :cond_1

    const/16 v8, 0x9

    if-eq v3, v8, :cond_1

    const/16 v8, 0x1c

    if-eq v3, v8, :cond_1

    const/16 v8, 0x8

    if-eq v3, v8, :cond_1

    const/16 v8, 0x1f

    if-eq v3, v8, :cond_1

    const/16 v8, 0x21

    if-ne v3, v8, :cond_7

    .line 351
    :cond_1
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 353
    const/16 v8, 0x8

    invoke-virtual {v5, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 355
    const v8, 0x7f0f0093

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "icon":Landroid/widget/ImageView;
    check-cast v0, Landroid/widget/ImageView;

    .line 357
    .restart local v0    # "icon":Landroid/widget/ImageView;
    const v8, 0x7f0f0037

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .end local v4    # "select":Landroid/widget/CheckBox;
    check-cast v4, Landroid/widget/CheckBox;

    .line 359
    .restart local v4    # "select":Landroid/widget/CheckBox;
    new-instance v8, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter$1;

    invoke-direct {v8, p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter$1;-><init>(Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;)V

    invoke-virtual {v4, v8}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 368
    new-instance v8, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter$2;

    invoke-direct {v8, p0, v2}, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter$2;-><init>(Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;Lcom/sec/android/app/myfiles/element/ShortcutItem;)V

    invoke-virtual {v4, v8}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 385
    iget-boolean v8, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mIsSelectMode:Z

    if-eqz v8, :cond_6

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->isSelectable()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 387
    const/4 v8, 0x0

    invoke-virtual {v4, v8}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 389
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->isSelected()Z

    move-result v8

    invoke-virtual {v4, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 396
    :goto_1
    const v8, 0x7f0f0094

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 398
    .local v7, "title":Landroid/widget/TextView;
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 400
    sget-object v8, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 402
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 414
    .end local v7    # "title":Landroid/widget/TextView;
    :goto_2
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getIconDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v8

    if-eqz v8, :cond_8

    .line 417
    const/16 v8, 0x9

    if-ne v3, v8, :cond_2

    .line 419
    const v8, 0x7f0f0061

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 421
    const/4 v8, 0x0

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 424
    :cond_2
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getIconDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 452
    :cond_3
    :goto_3
    iget-boolean v8, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mIsItemViewLayerTypeSoftware:Z

    if-eqz v8, :cond_4

    .line 454
    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-virtual {p2, v8, v9}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 457
    :cond_4
    return-object p2

    .line 340
    .end local v5    # "shortcutItemAddContainer":Landroid/view/ViewGroup;
    .end local v6    # "shortcutItemContainer":Landroid/view/ViewGroup;
    :cond_5
    const v8, 0x7f040047

    const/4 v9, 0x0

    invoke-virtual {v1, v8, p3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    goto/16 :goto_0

    .line 393
    .restart local v5    # "shortcutItemAddContainer":Landroid/view/ViewGroup;
    .restart local v6    # "shortcutItemContainer":Landroid/view/ViewGroup;
    :cond_6
    const/16 v8, 0x8

    invoke-virtual {v4, v8}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_1

    .line 406
    :cond_7
    const/16 v8, 0x8

    invoke-virtual {v6, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 408
    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 410
    const v8, 0x7f0f0096

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "icon":Landroid/widget/ImageView;
    check-cast v0, Landroid/widget/ImageView;

    .restart local v0    # "icon":Landroid/widget/ImageView;
    goto :goto_2

    .line 430
    :cond_8
    const/16 v8, 0x1a

    if-ne v3, v8, :cond_9

    .line 432
    const v8, 0x7f020032

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_3

    .line 434
    :cond_9
    const/16 v8, 0x9

    if-ne v3, v8, :cond_a

    .line 436
    const/16 v8, 0x8

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 438
    const v8, 0x7f0f0061

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 440
    :cond_a
    const/16 v8, 0x1c

    if-ne v3, v8, :cond_b

    .line 442
    const v8, 0x7f0200d2

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 443
    const/4 v8, 0x0

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 444
    const v8, 0x7f0f0061

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 446
    :cond_b
    const/16 v8, 0x8

    if-ne v3, v8, :cond_3

    .line 448
    const v8, 0x7f020050

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_3
.end method

.method public isSelectMode()Z
    .locals 1

    .prologue
    .line 487
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mIsSelectMode:Z

    return v0
.end method

.method protected loadShortcuts()Ljava/util/ArrayList;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/ShortcutItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v10, 0x9

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v2, 0x0

    .line 550
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 553
    .local v7, "userShortcutList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/ShortcutItem;>;"
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Shortcut;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    const-string v5, "shortcut_index ASC"

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 554
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_3

    .line 556
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 558
    const-string v2, "shortcut_index"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 559
    .local v5, "index":I
    const-string v2, "type"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 561
    .local v1, "shortcutType":I
    const/4 v0, 0x0

    .line 563
    .local v0, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    packed-switch v1, :pswitch_data_0

    .line 579
    new-instance v0, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .end local v0    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    const-string v2, "title"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "_data"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    if-eq v1, v10, :cond_1

    move v4, v8

    :goto_1
    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/element/ShortcutItem;-><init>(ILjava/lang/String;Ljava/lang/String;ZI)V

    .line 591
    .restart local v0    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :goto_2
    const-string v2, "shortcut_drawable"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->convertByteArrayToDrawable([B)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 592
    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 567
    :pswitch_0
    new-instance v0, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .end local v0    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    const-string v2, "title"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Security;->getInstance()Lcom/sec/android/app/myfiles/utils/Security;

    move-result-object v3

    const-string v4, "_data"

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/utils/Security;->decrypt(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eq v1, v10, :cond_0

    move v4, v8

    :goto_3
    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/element/ShortcutItem;-><init>(ILjava/lang/String;Ljava/lang/String;ZI)V

    .line 575
    .restart local v0    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    goto :goto_2

    .end local v0    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :cond_0
    move v4, v9

    .line 567
    goto :goto_3

    :cond_1
    move v4, v9

    .line 579
    goto :goto_1

    .line 594
    .end local v1    # "shortcutType":I
    .end local v5    # "index":I
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 596
    :cond_3
    return-object v7

    .line 563
    :pswitch_data_0
    .packed-switch 0x1c
        :pswitch_0
    .end packed-switch
.end method

.method public notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 152
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->loadShortcuts()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    .line 153
    invoke-super {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;->notifyDataSetChanged()V

    .line 154
    return-void
.end method

.method public onFTPSessionAdded(Lcom/sec/android/app/myfiles/element/ShortcutItem;)V
    .locals 0
    .param p1, "item"    # Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .prologue
    .line 612
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->refresh()V

    .line 613
    return-void
.end method

.method public onFTPSessionRemoved(Lcom/sec/android/app/myfiles/element/ShortcutItem;)V
    .locals 0
    .param p1, "item"    # Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .prologue
    .line 617
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->refresh()V

    .line 618
    return-void
.end method

.method public refresh()V
    .locals 4

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->loadShortcuts()Ljava/util/ArrayList;

    move-result-object v2

    .line 107
    .local v2, "userShortcut":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/ShortcutItem;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->clear()V

    .line 109
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 111
    .local v1, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->addItem(Lcom/sec/android/app/myfiles/element/ShortcutItem;)V

    goto :goto_0

    .line 114
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->notifyDataSetChanged()V

    .line 115
    return-void
.end method

.method public removeAllItem(Ljava/util/ArrayList;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 159
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    if-eqz p1, :cond_0

    .line 161
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v1

    .line 163
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    move-result v0

    monitor-exit v1

    .line 168
    :goto_0
    return v0

    .line 164
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 168
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeItem(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 140
    const/4 v0, 0x0

    .line 142
    .local v0, "result":Z
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v2

    .line 144
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 145
    monitor-exit v2

    .line 147
    return v0

    .line 145
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public selectAllItem()V
    .locals 4

    .prologue
    .line 519
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v3

    .line 521
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 523
    .local v1, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->isSelectable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 524
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->setSelected(Z)V

    goto :goto_0

    .line 527
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 529
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->notifyDataSetChanged()V

    .line 530
    return-void
.end method

.method public selectItem(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 494
    const/4 v1, -0x1

    if-eq p1, v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_1

    .line 496
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v2

    .line 498
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 499
    .local v0, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->isSelected()Z

    move-result v1

    if-nez v1, :cond_2

    .line 501
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->isSelectable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 503
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->setSelected(Z)V

    .line 508
    :cond_0
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 510
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->notifyDataSetChanged()V

    .line 513
    .end local v0    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :cond_1
    return-void

    .line 506
    .restart local v0    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :cond_2
    const/4 v1, 0x0

    :try_start_1
    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->setSelected(Z)V

    goto :goto_0

    .line 508
    .end local v0    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public setItemViewLayerTypeSoftware(Z)V
    .locals 0
    .param p1, "useSoftwareLayer"    # Z

    .prologue
    .line 463
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mIsItemViewLayerTypeSoftware:Z

    .line 464
    return-void
.end method

.method public setSelectedItemChangeListener(Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;

    .prologue
    .line 602
    return-void
.end method

.method public startSelectMode(II)V
    .locals 1
    .param p1, "selectionType"    # I
    .param p2, "from"    # I

    .prologue
    .line 470
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mIsSelectMode:Z

    .line 472
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->notifyDataSetChanged()V

    .line 473
    return-void
.end method

.method public unselectAllItem()V
    .locals 4

    .prologue
    .line 536
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    monitor-enter v3

    .line 538
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 540
    .local v1, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->setSelected(Z)V

    goto :goto_0

    .line 542
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 544
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;->notifyDataSetChanged()V

    .line 545
    return-void
.end method
