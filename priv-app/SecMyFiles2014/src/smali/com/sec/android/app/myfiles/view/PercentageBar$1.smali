.class Lcom/sec/android/app/myfiles/view/PercentageBar$1;
.super Ljava/lang/Thread;
.source "PercentageBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/view/PercentageBar;->drawPercentage()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/view/PercentageBar;

.field final synthetic val$animationDuration:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/view/PercentageBar;I)V
    .locals 0

    .prologue
    .line 195
    iput-object p1, p0, Lcom/sec/android/app/myfiles/view/PercentageBar$1;->this$0:Lcom/sec/android/app/myfiles/view/PercentageBar;

    iput p2, p0, Lcom/sec/android/app/myfiles/view/PercentageBar$1;->val$animationDuration:I

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 202
    const-wide/16 v4, 0x12c

    :try_start_0
    invoke-static {v4, v5}, Lcom/sec/android/app/myfiles/view/PercentageBar$1;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 209
    :goto_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 211
    .local v0, "c":Ljava/util/Calendar;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/view/PercentageBar$1;->this$0:Lcom/sec/android/app/myfiles/view/PercentageBar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    # setter for: Lcom/sec/android/app/myfiles/view/PercentageBar;->mBeginTimePoint:J
    invoke-static {v4, v6, v7}, Lcom/sec/android/app/myfiles/view/PercentageBar;->access$002(Lcom/sec/android/app/myfiles/view/PercentageBar;J)J

    .line 213
    const/16 v2, 0xa

    .line 215
    .local v2, "interval":I
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    iget v4, p0, Lcom/sec/android/app/myfiles/view/PercentageBar$1;->val$animationDuration:I

    if-gt v3, v4, :cond_0

    .line 217
    iget-object v4, p0, Lcom/sec/android/app/myfiles/view/PercentageBar$1;->this$0:Lcom/sec/android/app/myfiles/view/PercentageBar;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/view/PercentageBar;->postInvalidate()V

    .line 221
    int-to-long v4, v2

    :try_start_1
    invoke-static {v4, v5}, Lcom/sec/android/app/myfiles/view/PercentageBar$1;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 215
    :goto_2
    add-int/2addr v3, v2

    goto :goto_1

    .line 204
    .end local v0    # "c":Ljava/util/Calendar;
    .end local v2    # "interval":I
    .end local v3    # "j":I
    :catch_0
    move-exception v1

    .line 206
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 223
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "c":Ljava/util/Calendar;
    .restart local v2    # "interval":I
    .restart local v3    # "j":I
    :catch_1
    move-exception v1

    .line 225
    .restart local v1    # "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 228
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    return-void
.end method
