.class Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$6;
.super Ljava/lang/Object;
.source "CategoryBrowserFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->onCreateItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;)V
    .locals 0

    .prologue
    .line 808
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 7
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 813
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v3, v3, Lcom/sec/android/app/myfiles/SelectorActivity;

    if-eqz v3, :cond_0

    move v3, v4

    .line 885
    :goto_0
    return v3

    .line 817
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v3, v3, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v3, :cond_1

    .line 818
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v3, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v3, v3, Lcom/sec/android/app/myfiles/MainActivity;->mIsSearchMode:Z

    if-eqz v3, :cond_1

    move v3, v4

    .line 819
    goto :goto_0

    .line 823
    :cond_1
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 825
    .local v0, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 827
    .local v2, "path":Ljava/lang/String;
    if-eqz v0, :cond_9

    .line 829
    const-string v3, "_data"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 831
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->isSelectMode()Z

    move-result v3

    if-nez v3, :cond_5

    .line 833
    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isInternalRootFolder(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_9

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isExternalRootFolder(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_9

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxRootFolder(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 835
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v3, v3, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->isSearchMode()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 839
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v3, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/MainActivity;->finishSearchMode()V

    .line 842
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    invoke-virtual {v3, v5, p3, v4}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->startSelectMode(III)V

    .line 846
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 848
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mCurrentListableView:Lcom/sec/android/app/myfiles/view/IDirectDragable;

    invoke-interface {v3, v5}, Lcom/sec/android/app/myfiles/view/IDirectDragable;->setDirectDragMode(Z)V

    :goto_1
    move v3, v5

    .line 857
    goto/16 :goto_0

    .line 854
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mCurrentListableView:Lcom/sec/android/app/myfiles/view/IDirectDragable;

    invoke-interface {v3, v4}, Lcom/sec/android/app/myfiles/view/IDirectDragable;->setDirectDragMode(Z)V

    goto :goto_1

    .line 862
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemPosition()Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 864
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v3

    if-nez v3, :cond_7

    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mSideSyncControl:Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;->isKMSRunning(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_7

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mSideSyncControl:Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;

    invoke-static {}, Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;->isPSSRunning()Z

    move-result v3

    if-nez v3, :cond_7

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 869
    :cond_7
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    # invokes: Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->verifySelectedItem()Z
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 870
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 871
    .local v1, "mBundle":Landroid/os/Bundle;
    const-string v3, "ITEM_INITIATING_DRAG_PATH"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 872
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    invoke-virtual {v3, p2, v1}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->startDragMode(Landroid/view/View;Landroid/os/Bundle;)V

    .end local v1    # "mBundle":Landroid/os/Bundle;
    :goto_2
    move v3, v5

    .line 879
    goto/16 :goto_0

    .line 876
    :cond_8
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v6, 0x7f0b005a

    invoke-static {v3, v6, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_2

    :cond_9
    move v3, v4

    .line 885
    goto/16 :goto_0
.end method
