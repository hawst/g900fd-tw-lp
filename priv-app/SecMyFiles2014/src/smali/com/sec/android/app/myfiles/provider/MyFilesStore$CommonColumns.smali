.class public interface abstract Lcom/sec/android/app/myfiles/provider/MyFilesStore$CommonColumns;
.super Ljava/lang/Object;
.source "MyFilesStore.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/provider/MyFilesStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CommonColumns"
.end annotation


# static fields
.field public static final COUNT_OPENED:Ljava/lang/String; = "count_opened"

.field public static final COUNT_USED:Ljava/lang/String; = "count_used"

.field public static final DATA:Ljava/lang/String; = "_data"

.field public static final DATE_LAST_USED:Ljava/lang/String; = "date_used"

.field public static final DATE_MODIFIED:Ljava/lang/String; = "date_modified"

.field public static final DEFAULT_SORT_ORDER:Ljava/lang/String; = "_data ASC"

.field public static final FILE_ID:Ljava/lang/String; = "file_id"

.field public static final TITLE:Ljava/lang/String; = "title"
