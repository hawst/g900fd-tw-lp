.class public abstract Lcom/sec/android/app/myfiles/fragment/AbsMultipleDetailFragment;
.super Landroid/app/DialogFragment;
.source "AbsMultipleDetailFragment.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/fragment/OnShowMultipleDetailsListener;


# static fields
.field private static final CONTENT:Ljava/lang/String; = "content"

.field private static final TITLE:Ljava/lang/String; = "title"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 24
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 49
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsMultipleDetailFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v8

    .line 51
    .local v8, "argument":Landroid/os/Bundle;
    if-nez v8, :cond_1

    .line 53
    invoke-super/range {p0 .. p1}, Landroid/app/DialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v11

    .line 177
    :cond_0
    :goto_0
    return-object v11

    .line 55
    :cond_1
    const-string v3, "detail_item_path"

    invoke-virtual {v8, v3}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v20

    .line 57
    .local v20, "pathes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v3, "detail_item_format"

    invoke-virtual {v8, v3}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v17

    .line 59
    .local v17, "formats":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz v20, :cond_2

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_2

    if-eqz v17, :cond_2

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_2

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-eq v3, v5, :cond_3

    .line 62
    :cond_2
    const/4 v11, 0x0

    goto :goto_0

    .line 64
    :cond_3
    const/16 v16, 0x0

    .line 66
    .local v16, "folderCnt":I
    const/4 v13, 0x0

    .line 68
    .local v13, "fileCnt":I
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_1
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v3

    move/from16 v0, v18

    if-ge v0, v3, :cond_5

    .line 70
    invoke-virtual/range {v17 .. v18}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/16 v5, 0x3001

    if-ne v3, v5, :cond_4

    .line 72
    add-int/lit8 v16, v16, 0x1

    .line 68
    :cond_4
    add-int/lit8 v18, v18, 0x1

    goto :goto_1

    .line 77
    :cond_5
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v3

    sub-int v13, v3, v16

    .line 79
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 81
    .local v4, "dataList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    const/4 v10, 0x0

    .line 93
    .local v10, "datas":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v10, Ljava/util/HashMap;

    .end local v10    # "datas":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 95
    .restart local v10    # "datas":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "title"

    const v5, 0x7f0b00aa

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/myfiles/fragment/AbsMultipleDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v10, v3, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    .line 99
    .local v21, "sb":Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsMultipleDetailFragment;->getFileSize(Ljava/util/ArrayList;)J

    move-result-wide v14

    .line 101
    .local v14, "fileLength":J
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsMultipleDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v14, v15}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 103
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-static {v3}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v19

    .line 105
    .local v19, "nf":Ljava/text/NumberFormat;
    const-string v3, " ("

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v14, v15}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v5, 0x20

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    const v5, 0x7f0b006f

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/myfiles/fragment/AbsMultipleDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v5, 0x29

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 107
    const-string v3, "content"

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v10, v3, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 112
    new-instance v10, Ljava/util/HashMap;

    .end local v10    # "datas":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 114
    .restart local v10    # "datas":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "title"

    const v5, 0x7f0b00ab

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/myfiles/fragment/AbsMultipleDetailFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v10, v3, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    const-string v12, ""

    .line 118
    .local v12, "fCnt":Ljava/lang/String;
    if-eqz v13, :cond_6

    .line 120
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsMultipleDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const/high16 v6, 0x7f0c0000

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/16 v22, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v7, v22

    invoke-virtual {v5, v6, v13, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 124
    :cond_6
    if-eqz v16, :cond_8

    .line 126
    const-string v3, ""

    invoke-virtual {v12, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 128
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 130
    :cond_7
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsMultipleDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c0001

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/16 v22, 0x0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v7, v22

    move/from16 v0, v16

    invoke-virtual {v5, v6, v0, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 134
    :cond_8
    const-string v3, "content"

    invoke-virtual {v10, v3, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    new-instance v2, Landroid/widget/SimpleAdapter;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsMultipleDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v5, 0x7f040024

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v22, "title"

    aput-object v22, v6, v7

    const/4 v7, 0x1

    const-string v22, "content"

    aput-object v22, v6, v7

    const/4 v7, 0x2

    new-array v7, v7, [I

    fill-array-data v7, :array_0

    invoke-direct/range {v2 .. v7}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 141
    .local v2, "adapter":Landroid/widget/SimpleAdapter;
    new-instance v9, Landroid/app/AlertDialog$Builder;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsMultipleDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v9, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 143
    .local v9, "builder":Landroid/app/AlertDialog$Builder;
    const v3, 0x7f0b0025

    invoke-virtual {v9, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 145
    const/4 v3, 0x0

    invoke-virtual {v9, v2, v3}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 147
    const v3, 0x7f0b0015

    new-instance v5, Lcom/sec/android/app/myfiles/fragment/AbsMultipleDetailFragment$1;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/sec/android/app/myfiles/fragment/AbsMultipleDetailFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsMultipleDetailFragment;)V

    invoke-virtual {v9, v3, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 157
    invoke-virtual {v9}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v11

    .line 159
    .local v11, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v11}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 161
    invoke-virtual {v11}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/widget/ListView;->setFocusable(Z)V

    .line 162
    invoke-virtual {v11}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v3

    new-instance v5, Lcom/sec/android/app/myfiles/fragment/AbsMultipleDetailFragment$2;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/sec/android/app/myfiles/fragment/AbsMultipleDetailFragment$2;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsMultipleDetailFragment;)V

    invoke-virtual {v3, v5}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto/16 :goto_0

    .line 138
    :array_0
    .array-data 4
        0x1020014
        0x1020015
    .end array-data
.end method
