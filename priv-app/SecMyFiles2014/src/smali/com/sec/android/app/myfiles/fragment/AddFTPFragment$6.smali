.class Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$6;
.super Ljava/lang/Object;
.source "AddFTPFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)V
    .locals 0

    .prologue
    .line 450
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # I
    .param p4, "arg3"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 453
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mOldParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    if-eqz v0, :cond_1

    .line 454
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mSpinnersCounter:I
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->access$600(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    .line 455
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->validate()V

    .line 457
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    # ++operator for: Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mSpinnersCounter:I
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->access$604(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)I

    .line 459
    :cond_1
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 464
    .local p1, "arg0":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
