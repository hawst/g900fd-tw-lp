.class Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;
.super Landroid/content/IRCPInterfaceCallback$Stub;
.source "KnoxFileOperationTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RCPInterfaceCallBack"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;)V
    .locals 0

    .prologue
    .line 544
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    invoke-direct {p0}, Landroid/content/IRCPInterfaceCallback$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;
    .param p2, "x1"    # Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$1;

    .prologue
    .line 544
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;-><init>(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;)V

    return-void
.end method


# virtual methods
.method public onComplete(Ljava/util/List;II)V
    .locals 3
    .param p2, "destinationUserId"    # I
    .param p3, "successCnt"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;II)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 551
    .local p1, "srcPathsOrig":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mFileRelayComplete:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->access$602(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;Z)Z

    .line 553
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mWaitKnoxReply:Ljava/lang/Runnable;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->access$800(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;)Ljava/lang/Runnable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 554
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mWaitKnoxReply:Ljava/lang/Runnable;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->access$800(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;)Ljava/lang/Runnable;

    move-result-object v1

    monitor-enter v1

    .line 556
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mWaitKnowReplyFlag:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->access$902(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;Z)Z

    .line 558
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mWaitKnoxReply:Ljava/lang/Runnable;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->access$800(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 560
    monitor-exit v1

    .line 562
    :cond_0
    return-void

    .line 560
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onDone(Ljava/lang/String;I)V
    .locals 6
    .param p1, "srcPathsOrig"    # Ljava/lang/String;
    .param p2, "destinationUserId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 567
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget v2, v1, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTarget:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTarget:I

    .line 569
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    new-array v2, v5, [Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    aput-object v4, v2, v3

    # invokes: Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->publishProgress([Ljava/lang/Object;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->access$1000(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;[Ljava/lang/Object;)V

    .line 571
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget v1, v1, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTarget:I

    mul-int/lit8 v1, v1, 0x64

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    div-int v0, v1, v2

    .line 575
    .local v0, "progressRate":I
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    # setter for: Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mFileRelayDone:Z
    invoke-static {v1, v5}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->access$302(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;Z)Z

    .line 577
    return-void
.end method

.method public onFail(Ljava/lang/String;II)V
    .locals 4
    .param p1, "srcPathsOrig"    # Ljava/lang/String;
    .param p2, "destinationUserId"    # I
    .param p3, "errorCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 582
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mFileRelayError:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->access$402(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;Z)Z

    .line 584
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    # operator++ for: Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mFailedCount:I
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->access$508(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;)I

    .line 586
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    # setter for: Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mErrorCode:I
    invoke-static {v0, p3}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->access$102(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;I)I

    .line 588
    const/4 v0, 0x0

    const-string v1, "KnoxFileOperationTask"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " file operation failed - mErrorCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 590
    return-void
.end method

.method public onProgress(Ljava/lang/String;II)V
    .locals 5
    .param p1, "srcPathsOrig"    # Ljava/lang/String;
    .param p2, "destinationUserId"    # I
    .param p3, "progress"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/16 v0, 0x64

    const/4 v4, 0x0

    .line 595
    const-string v1, "KnoxFileOperationTask"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACTION_KNOX_FILE_RELAY_PROGRESS curPercentage : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 597
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mFirstProgress:Z
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->access$1100(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 598
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    # setter for: Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mCurrentProgress:I
    invoke-static {v1, p3}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->access$1202(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;I)I

    .line 599
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    # setter for: Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mFirstProgress:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->access$1102(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;Z)Z

    .line 601
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mCurrentProgress:I
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->access$1200(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;)I

    move-result v1

    if-ge v1, p3, :cond_2

    .line 602
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    # setter for: Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mCurrentProgress:I
    invoke-static {v1, p3}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->access$1202(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;I)I

    .line 604
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    if-lt p3, v0, :cond_1

    move p3, v0

    .end local p3    # "progress":I
    :cond_1
    iput p3, v1, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mCurrentTargetPercentage:I

    .line 606
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    aput-object v2, v1, v4

    # invokes: Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->publishProgress([Ljava/lang/Object;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->access$1300(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;[Ljava/lang/Object;)V

    .line 608
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressBar:Landroid/widget/ProgressBar;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget v1, v1, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mCurrentTargetPercentage:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 610
    :cond_2
    return-void
.end method
