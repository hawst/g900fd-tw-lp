.class Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$1;
.super Ljava/lang/Object;
.source "DownloadedAppFragment.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)V
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0
    .param p1, "arg0"    # Landroid/widget/AbsListView;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 183
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1
    .param p1, "scrollState"    # Landroid/widget/AbsListView;
    .param p2, "state"    # I

    .prologue
    .line 157
    packed-switch p2, :pswitch_data_0

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 160
    :pswitch_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader;->SCROLL_DONE:Z

    goto :goto_0

    .line 166
    :pswitch_1
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader;->SCROLL_DONE:Z

    goto :goto_0

    .line 171
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    goto :goto_0

    .line 157
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
