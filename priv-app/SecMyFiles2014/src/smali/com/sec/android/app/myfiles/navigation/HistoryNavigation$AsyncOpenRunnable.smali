.class Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$AsyncOpenRunnable;
.super Ljava/lang/Object;
.source "HistoryNavigation.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AsyncOpenRunnable"
.end annotation


# instance fields
.field private mAsyncUIFrag:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

.field final synthetic this$0:Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;)V
    .locals 1
    .param p2, "asyncUi"    # Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    .prologue
    .line 223
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;

    .line 224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 221
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$AsyncOpenRunnable;->mAsyncUIFrag:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    .line 225
    iput-object p2, p0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$AsyncOpenRunnable;->mAsyncUIFrag:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    .line 226
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 231
    # getter for: Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->CACHE_DB_LOCK:Ljava/lang/Object;
    invoke-static {}, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->access$000()Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 232
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;

    # invokes: Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->loadDownloadedFiles()Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->access$100(Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;)Ljava/util/ArrayList;

    move-result-object v0

    .line 234
    .local v0, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/HistoryItem;>;"
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;

    # invokes: Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->removeUndesiredMimeTypeItems(Ljava/util/List;)V
    invoke-static {v1, v0}, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->access$200(Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;Ljava/util/List;)V

    .line 236
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;

    # invokes: Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->getCache()Lcom/sec/android/app/myfiles/db/CacheDB;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->access$300(Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;)Lcom/sec/android/app/myfiles/db/CacheDB;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->replaceHistoryCache(Ljava/util/List;)V

    .line 238
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$AsyncOpenRunnable;->mAsyncUIFrag:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    if-eqz v1, :cond_0

    .line 239
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$AsyncOpenRunnable;->mAsyncUIFrag:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->setCompletedCondition(Z)V

    .line 241
    :cond_0
    monitor-exit v2

    .line 243
    return-void

    .line 241
    .end local v0    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/HistoryItem;>;"
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
