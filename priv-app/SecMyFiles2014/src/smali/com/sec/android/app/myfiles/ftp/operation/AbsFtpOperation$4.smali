.class Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$4;
.super Ljava/lang/Object;
.source "AbsFtpOperation.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;)V
    .locals 0

    .prologue
    .line 947
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$4;->this$0:Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 952
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 966
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$4;->this$0:Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;

    # getter for: Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mProcessing:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->access$900(Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$4;->this$0:Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;

    # getter for: Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->access$800(Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b006b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 970
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 954
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$4;->this$0:Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;

    # getter for: Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mProcessing:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->access$300(Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$4;->this$0:Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;

    # getter for: Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->access$200(Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b00ac

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 958
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$4;->this$0:Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;

    # getter for: Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mProcessing:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->access$500(Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$4;->this$0:Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;

    # getter for: Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->access$400(Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b00c8

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 962
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$4;->this$0:Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;

    # getter for: Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mProcessing:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->access$700(Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$4;->this$0:Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;

    # getter for: Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->access$600(Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b00ca

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 952
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
