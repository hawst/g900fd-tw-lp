.class Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$9;
.super Ljava/lang/Object;
.source "DownloadedAppFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->onCreateTreeViewItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)V
    .locals 0

    .prologue
    .line 994
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/16 v4, 0x1f

    .line 999
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    invoke-virtual {v2, p3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->setCurrentPosition(I)V

    .line 1001
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    invoke-virtual {v2, p3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;

    .line 1003
    .local v1, "item":Lcom/sec/android/app/myfiles/element/TreeViewItem;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->saveTreeYPosition()V

    .line 1005
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->getSelectModeFrom()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->getSelectModeFrom()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->isSelectMode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1007
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->finishSelectMode()V

    .line 1010
    :cond_1
    iget v2, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    const/16 v3, 0xd

    if-ne v2, v3, :cond_2

    .line 1012
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->getFTPShortCutCount(Landroid/content/Context;)I

    move-result v2

    if-lez v2, :cond_2

    .line 1014
    const/16 v2, 0x11

    iput v2, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    .line 1018
    :cond_2
    iget v2, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    if-nez v2, :cond_4

    .line 1020
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1022
    .local v0, "argument":Landroid/os/Bundle;
    const-string v2, "FOLDERPATH"

    iget-object v3, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mPath:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1024
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v2, Lcom/sec/android/app/myfiles/MainActivity;

    const/16 v3, 0x201

    invoke-virtual {v2, v3, v0}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    .line 1047
    .end local v0    # "argument":Landroid/os/Bundle;
    :cond_3
    :goto_0
    return-void

    .line 1027
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v2, v2, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v2, :cond_3

    .line 1029
    iget-boolean v2, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mEnabled:Z

    if-eqz v2, :cond_3

    .line 1032
    iget v2, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    const/16 v3, 0x8

    if-eq v2, v3, :cond_5

    iget v2, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    if-ne v2, v4, :cond_7

    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isWifiOn(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 1034
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckDropBox()Z

    move-result v2

    if-nez v2, :cond_7

    .line 1036
    iget v2, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    if-ne v2, v4, :cond_6

    .line 1037
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->showDialog(I)V

    goto :goto_0

    .line 1039
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->showDialog(I)V

    goto :goto_0

    .line 1044
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v2, Lcom/sec/android/app/myfiles/MainActivity;

    iget v3, v1, Lcom/sec/android/app/myfiles/element/TreeViewItem;->mType:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto :goto_0
.end method
