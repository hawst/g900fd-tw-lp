.class Lcom/sec/android/app/myfiles/view/MyFilesListView$1;
.super Landroid/os/CountDownTimer;
.source "MyFilesListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/view/MyFilesListView;->triggerLongClick(ILandroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/view/MyFilesListView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/view/MyFilesListView;JJ)V
    .locals 0
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 167
    iput-object p1, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView$1;->this$0:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView$1;->this$0:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    # getter for: Lcom/sec/android/app/myfiles/view/MyFilesListView;->mTouchedRowView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->access$000(Lcom/sec/android/app/myfiles/view/MyFilesListView;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView$1;->this$0:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    # getter for: Lcom/sec/android/app/myfiles/view/MyFilesListView;->mTouchedRowView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->access$000(Lcom/sec/android/app/myfiles/view/MyFilesListView;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 190
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView$1;->this$0:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    # invokes: Lcom/sec/android/app/myfiles/view/MyFilesListView;->performDirectDrag()V
    invoke-static {v0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->access$200(Lcom/sec/android/app/myfiles/view/MyFilesListView;)V

    .line 191
    return-void
.end method

.method public onTick(J)V
    .locals 5
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView$1;->this$0:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    # getter for: Lcom/sec/android/app/myfiles/view/MyFilesListView;->mTouchedRowView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->access$000(Lcom/sec/android/app/myfiles/view/MyFilesListView;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView$1;->this$0:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    # getter for: Lcom/sec/android/app/myfiles/view/MyFilesListView;->mTouchedRowView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->access$000(Lcom/sec/android/app/myfiles/view/MyFilesListView;)Landroid/view/View;

    move-result-object v0

    const/high16 v1, 0x437f0000    # 255.0f

    const-wide/16 v2, 0x3e8

    sub-long/2addr v2, p1

    long-to-float v2, v2

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView$1;->this$0:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    # getter for: Lcom/sec/android/app/myfiles/view/MyFilesListView;->mDirectDragBackground:I
    invoke-static {v2}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->access$100(Lcom/sec/android/app/myfiles/view/MyFilesListView;)I

    move-result v2

    invoke-static {v2}, Landroid/graphics/Color;->red(I)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView$1;->this$0:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    # getter for: Lcom/sec/android/app/myfiles/view/MyFilesListView;->mDirectDragBackground:I
    invoke-static {v3}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->access$100(Lcom/sec/android/app/myfiles/view/MyFilesListView;)I

    move-result v3

    invoke-static {v3}, Landroid/graphics/Color;->green(I)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView$1;->this$0:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    # getter for: Lcom/sec/android/app/myfiles/view/MyFilesListView;->mDirectDragBackground:I
    invoke-static {v4}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->access$100(Lcom/sec/android/app/myfiles/view/MyFilesListView;)I

    move-result v4

    invoke-static {v4}, Landroid/graphics/Color;->blue(I)I

    move-result v4

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 179
    :cond_0
    return-void
.end method
