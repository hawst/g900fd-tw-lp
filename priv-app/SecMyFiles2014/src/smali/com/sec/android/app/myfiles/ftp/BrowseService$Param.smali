.class public final enum Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;
.super Ljava/lang/Enum;
.source "BrowseService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/ftp/BrowseService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Param"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;

.field public static final enum BROWSE_ITEM:Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;

.field public static final enum BROWSE_PATH:Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;

.field public static final enum FTP_PARAMS:Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;


# instance fields
.field private final mTag:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 158
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;

    const-string v1, "FTP_PARAMS"

    const-string v2, "FTP_PARAMS"

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;->FTP_PARAMS:Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;

    .line 160
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;

    const-string v1, "BROWSE_ITEM"

    const-string v2, "BROWSE_ITEM"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;->BROWSE_ITEM:Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;

    .line 162
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;

    const-string v1, "BROWSE_PATH"

    const-string v2, "BROWSE_PATH"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;->BROWSE_PATH:Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;

    .line 156
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;

    sget-object v1, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;->FTP_PARAMS:Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;->BROWSE_ITEM:Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;->BROWSE_PATH:Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;->$VALUES:[Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "tag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 164
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 166
    iput-object p3, p0, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;->mTag:Ljava/lang/String;

    .line 168
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 156
    const-class v0, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;
    .locals 1

    .prologue
    .line 156
    sget-object v0, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;->$VALUES:[Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;

    invoke-virtual {v0}, [Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;->mTag:Ljava/lang/String;

    return-object v0
.end method
