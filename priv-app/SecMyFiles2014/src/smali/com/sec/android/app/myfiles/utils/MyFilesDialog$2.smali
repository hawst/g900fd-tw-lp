.class Lcom/sec/android/app/myfiles/utils/MyFilesDialog$2;
.super Ljava/lang/Object;
.source "MyFilesDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->showWaitProgressDialog(Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/utils/MyFilesDialog;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog$2;->this$0:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 123
    const/16 v2, 0x52

    if-ne p2, v2, :cond_1

    .line 152
    :cond_0
    :goto_0
    return v1

    .line 127
    :cond_1
    const/16 v2, 0x54

    if-eq p2, v2, :cond_0

    .line 131
    const/4 v2, 0x4

    if-ne p2, v2, :cond_2

    .line 133
    iget-object v2, p0, Lcom/sec/android/app/myfiles/utils/MyFilesDialog$2;->this$0:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    # getter for: Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->mWaitProgressDlg:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->access$200(Lcom/sec/android/app/myfiles/utils/MyFilesDialog;)Landroid/app/ProgressDialog;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 137
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/FileUtils;->isRunningConvertThread()Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 142
    :catch_0
    move-exception v0

    .line 144
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 152
    .end local v0    # "ex":Ljava/lang/IllegalArgumentException;
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
