.class Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory$DisconnectHandlerTask;
.super Landroid/os/AsyncTask;
.source "FTPConnectionHandlerFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DisconnectHandlerTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 232
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory$1;

    .prologue
    .line 232
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory$DisconnectHandlerTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 232
    check-cast p1, [Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory$DisconnectHandlerTask;->doInBackground([Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;)Ljava/lang/Void;
    .locals 5
    .param p1, "args"    # [Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    .prologue
    .line 237
    if-eqz p1, :cond_0

    array-length v4, p1

    if-lez v4, :cond_0

    .line 239
    move-object v0, p1

    .local v0, "arr$":[Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 241
    .local v1, "handler":Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    invoke-interface {v1}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncLogOutAndDisconnect()Z

    .line 239
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 247
    .end local v0    # "arr$":[Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    .end local v1    # "handler":Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_0
    const/4 v4, 0x0

    return-object v4
.end method
