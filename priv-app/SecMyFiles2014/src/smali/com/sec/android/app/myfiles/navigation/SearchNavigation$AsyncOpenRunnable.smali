.class Lcom/sec/android/app/myfiles/navigation/SearchNavigation$AsyncOpenRunnable;
.super Ljava/lang/Object;
.source "SearchNavigation.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/navigation/SearchNavigation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AsyncOpenRunnable"
.end annotation


# instance fields
.field private final mAsyncFragment:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

.field final synthetic this$0:Lcom/sec/android/app/myfiles/navigation/SearchNavigation;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/myfiles/navigation/SearchNavigation;Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;)V
    .locals 0
    .param p2, "asyncUiFragment"    # Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    .prologue
    .line 1293
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/SearchNavigation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1295
    iput-object p2, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$AsyncOpenRunnable;->mAsyncFragment:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    .line 1297
    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    .line 1302
    const/4 v0, 0x0

    .line 1303
    .local v0, "CloudDeltaCursor":Ljava/lang/String;
    new-instance v4, Lcom/sec/android/app/myfiles/db/CacheDB;

    iget-object v11, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/SearchNavigation;

    iget-object v11, v11, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    invoke-direct {v4, v11}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    .line 1304
    .local v4, "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 1305
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/db/CacheDB;->getDropboxCursor()Ljava/lang/String;

    move-result-object v0

    .line 1306
    if-eqz v0, :cond_0

    const-string v11, ""

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1307
    :cond_0
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/db/CacheDB;->clearCloudData()J

    .line 1308
    :cond_1
    const/4 v9, 0x0

    .line 1310
    .local v9, "nodes":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<Lcom/samsung/scloud/data/SCloudNode;>;"
    const/4 v1, 0x0

    .line 1312
    .local v1, "SyncCount":I
    const/4 v2, 0x0

    .line 1314
    .local v2, "TotalCount":I
    move-object v10, v0

    .line 1316
    .local v10, "workingDelta":Ljava/lang/String;
    const/4 v3, 0x1

    .line 1319
    .local v3, "bUsingTransaction":Z
    :cond_2
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1320
    const/4 v6, 0x0

    .line 1321
    .local v6, "mBaiduAPIHelper":Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;
    iget-object v11, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/SearchNavigation;

    iget-object v11, v11, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    invoke-static {v11}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    move-result-object v6

    .line 1322
    invoke-virtual {v6, v10}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->getDeltaList(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;

    move-result-object v9

    .line 1329
    .end local v6    # "mBaiduAPIHelper":Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;
    :goto_0
    if-nez v9, :cond_4

    .line 1378
    :goto_1
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 1383
    return-void

    .line 1324
    :cond_3
    const/4 v7, 0x0

    .line 1325
    .local v7, "mDBAPIHelper":Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;
    iget-object v11, p0, Lcom/sec/android/app/myfiles/navigation/SearchNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/SearchNavigation;

    iget-object v11, v11, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mContext:Landroid/content/Context;

    invoke-static {v11}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    move-result-object v7

    .line 1326
    invoke-virtual {v7, v10}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->getDeltaList(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;

    move-result-object v9

    goto :goto_0

    .line 1332
    .end local v7    # "mDBAPIHelper":Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;
    :cond_4
    invoke-virtual {v9}, Lcom/samsung/scloud/data/SCloudNodeList;->getCursor()Ljava/lang/String;

    move-result-object v10

    .line 1334
    if-eqz v3, :cond_5

    .line 1336
    invoke-virtual {v9}, Lcom/samsung/scloud/data/SCloudNodeList;->getNodes()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    if-lez v11, :cond_5

    .line 1337
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/db/CacheDB;->beginTransaction()V

    .line 1340
    :cond_5
    if-eqz v9, :cond_7

    .line 1341
    invoke-virtual {v9}, Lcom/samsung/scloud/data/SCloudNodeList;->getNodes()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/scloud/data/SCloudNode;

    .line 1343
    .local v8, "node":Lcom/samsung/scloud/data/SCloudNode;
    invoke-virtual {v8}, Lcom/samsung/scloud/data/SCloudNode;->isDeleted()Z

    move-result v11

    if-eqz v11, :cond_6

    .line 1344
    invoke-virtual {v4, v8}, Lcom/sec/android/app/myfiles/db/CacheDB;->deleteCloudData(Lcom/samsung/scloud/data/SCloudNode;)I

    goto :goto_2

    .line 1346
    :cond_6
    invoke-virtual {v4, v8, v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->insertCloudData(Lcom/samsung/scloud/data/SCloudNode;Z)J

    goto :goto_2

    .line 1351
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v8    # "node":Lcom/samsung/scloud/data/SCloudNode;
    :cond_7
    if-eqz v3, :cond_8

    .line 1353
    invoke-virtual {v9}, Lcom/samsung/scloud/data/SCloudNodeList;->getNodes()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v1

    .line 1354
    invoke-virtual {v9}, Lcom/samsung/scloud/data/SCloudNodeList;->getNodes()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    add-int/2addr v2, v11

    .line 1358
    if-lez v1, :cond_8

    .line 1361
    const/4 v1, 0x0

    .line 1363
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/db/CacheDB;->setTransactionSuccessful()V

    .line 1364
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/db/CacheDB;->endTransaction()V

    .line 1371
    :cond_8
    move-object v0, v10

    .line 1372
    invoke-virtual {v4, v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->setDropboxCursor(Ljava/lang/String;)V

    .line 1375
    invoke-virtual {v9}, Lcom/samsung/scloud/data/SCloudNodeList;->getHasMore()Z

    move-result v11

    const/4 v12, 0x1

    if-eq v11, v12, :cond_2

    goto :goto_1
.end method
