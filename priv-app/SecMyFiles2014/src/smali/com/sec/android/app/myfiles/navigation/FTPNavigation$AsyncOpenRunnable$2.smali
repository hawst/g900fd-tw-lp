.class Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable$2;
.super Ljava/lang/Object;
.source "FTPNavigation.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;)V
    .locals 0

    .prologue
    .line 1228
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable$2;->this$1:Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1233
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable$2;->this$1:Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mAsyncUi:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->setCompletedCondition(Z)V

    .line 1235
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable$2;->this$1:Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1237
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable$2;->this$1:Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mGoPrevious:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1239
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable$2;->this$1:Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mGoPrevious:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1245
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable$2;->this$1:Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable$2;->this$1:Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mContext:Landroid/content/Context;

    const v2, 0x7f0b00c7

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1248
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable$2;->this$1:Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;

    # getter for: Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->mInitial:Z
    invoke-static {v0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->access$200(Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1250
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable$2;->this$1:Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->popItem()V

    .line 1252
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable$2;->this$1:Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;->this$0:Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->AsyncOpen(ZZ)V

    .line 1256
    :cond_1
    return-void
.end method
