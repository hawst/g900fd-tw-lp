.class public abstract Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;
.super Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
.source "AbsCursorAdapter.java"

# interfaces
.implements Landroid/widget/Filterable;
.implements Lcom/sec/android/app/myfiles/adapter/CursorFilter$CursorFilterClient;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter$1;,
        Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter$MyDataSetObserver;,
        Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter$ChangeObserver;
    }
.end annotation


# static fields
.field public static final FLAG_AUTO_REQUERY:I = 0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final FLAG_REGISTER_CONTENT_OBSERVER:I = 0x2

.field private static final MODULE:Ljava/lang/String; = "AbsCursorAdapter"


# instance fields
.field public isFirst:Z

.field protected mAbsCursorAdapterContext:Landroid/content/Context;

.field protected mAutoRequery:Z

.field protected mChangeObserver:Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter$ChangeObserver;

.field protected mCursor:Landroid/database/Cursor;

.field protected mCursorFilter:Lcom/sec/android/app/myfiles/adapter/CursorFilter;

.field protected mDataSetObserver:Landroid/database/DataSetObserver;

.field protected mDataValid:Z

.field protected mFilterQueryProvider:Landroid/widget/FilterQueryProvider;

.field protected mRowIDColumn:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "c"    # Landroid/database/Cursor;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 123
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;-><init>()V

    .line 93
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->isFirst:Z

    .line 124
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->init(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 125
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "c"    # Landroid/database/Cursor;
    .param p3, "flags"    # I

    .prologue
    .line 152
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;-><init>()V

    .line 93
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->isFirst:Z

    .line 153
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->init(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 154
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "c"    # Landroid/database/Cursor;
    .param p3, "autoRequery"    # Z

    .prologue
    .line 139
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;-><init>()V

    .line 93
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->isFirst:Z

    .line 140
    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->init(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 141
    return-void

    .line 140
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method


# virtual methods
.method public abstract bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
.end method

.method public changeCursor(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 346
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 347
    .local v0, "old":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 348
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 350
    :cond_0
    return-void
.end method

.method public closeCursor()V
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 203
    :cond_0
    return-void
.end method

.method public convertToString(Landroid/database/Cursor;)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 406
    if-nez p1, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 210
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mDataValid:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 213
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCursor()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mCursor:Landroid/database/Cursor;

    return-object v0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 293
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mDataValid:Z

    if-eqz v1, :cond_1

    .line 294
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 296
    if-nez p2, :cond_0

    .line 297
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mAbsCursorAdapterContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-virtual {p0, v1, v2, p3}, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->newDropDownView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 301
    .local v0, "v":Landroid/view/View;
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mAbsCursorAdapterContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 304
    .end local v0    # "v":Landroid/view/View;
    :goto_1
    return-object v0

    .line 299
    :cond_0
    move-object v0, p2

    .restart local v0    # "v":Landroid/view/View;
    goto :goto_0

    .line 304
    .end local v0    # "v":Landroid/view/View;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 443
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mCursorFilter:Lcom/sec/android/app/myfiles/adapter/CursorFilter;

    if-nez v0, :cond_0

    .line 444
    new-instance v0, Lcom/sec/android/app/myfiles/adapter/CursorFilter;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/adapter/CursorFilter;-><init>(Lcom/sec/android/app/myfiles/adapter/CursorFilter$CursorFilterClient;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mCursorFilter:Lcom/sec/android/app/myfiles/adapter/CursorFilter;

    .line 446
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mCursorFilter:Lcom/sec/android/app/myfiles/adapter/CursorFilter;

    return-object v0
.end method

.method public getFilterQueryProvider()Landroid/widget/FilterQueryProvider;
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mFilterQueryProvider:Landroid/widget/FilterQueryProvider;

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 221
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mDataValid:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mCursor:Landroid/database/Cursor;

    .line 225
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 3
    .param p1, "position"    # I

    .prologue
    const-wide/16 v0, 0x0

    .line 233
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mDataValid:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v2, :cond_0

    .line 234
    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mCursor:Landroid/database/Cursor;

    iget v1, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mRowIDColumn:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 240
    :cond_0
    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 253
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mDataValid:Z

    if-nez v1, :cond_0

    .line 254
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "this should only be called when the cursor is valid"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 256
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 257
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "couldn\'t move cursor to position "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 260
    :cond_1
    if-nez p2, :cond_2

    .line 261
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mAbsCursorAdapterContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-virtual {p0, v1, v2, p3}, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 266
    .local v0, "v":Landroid/view/View;
    :goto_0
    if-nez p1, :cond_3

    .line 267
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->isFirst:Z

    .line 271
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mAbsCursorAdapterContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 288
    return-object v0

    .line 263
    .end local v0    # "v":Landroid/view/View;
    :cond_2
    move-object v0, p2

    .restart local v0    # "v":Landroid/view/View;
    goto :goto_0

    .line 269
    :cond_3
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->isFirst:Z

    goto :goto_1
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 246
    const/4 v0, 0x1

    return v0
.end method

.method init(Landroid/content/Context;Landroid/database/Cursor;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "c"    # Landroid/database/Cursor;
    .param p3, "flags"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v0, 0x1

    .line 166
    and-int/lit8 v2, p3, 0x1

    if-ne v2, v0, :cond_2

    .line 167
    or-int/lit8 p3, p3, 0x2

    .line 168
    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mAutoRequery:Z

    .line 172
    :goto_0
    if-eqz p2, :cond_3

    .line 173
    .local v0, "cursorPresent":Z
    :goto_1
    iput-object p2, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mCursor:Landroid/database/Cursor;

    .line 174
    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mDataValid:Z

    .line 175
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mAbsCursorAdapterContext:Landroid/content/Context;

    .line 176
    if-eqz v0, :cond_4

    const-string v1, "_id"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    :goto_2
    iput v1, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mRowIDColumn:I

    .line 177
    and-int/lit8 v1, p3, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    .line 178
    new-instance v1, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter$ChangeObserver;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter$ChangeObserver;-><init>(Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mChangeObserver:Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter$ChangeObserver;

    .line 179
    new-instance v1, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter$MyDataSetObserver;

    invoke-direct {v1, p0, v3}, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter$MyDataSetObserver;-><init>(Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter$1;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mDataSetObserver:Landroid/database/DataSetObserver;

    .line 185
    :goto_3
    if-eqz v0, :cond_1

    .line 186
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mChangeObserver:Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter$ChangeObserver;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mChangeObserver:Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter$ChangeObserver;

    invoke-interface {p2, v1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 187
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mDataSetObserver:Landroid/database/DataSetObserver;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mDataSetObserver:Landroid/database/DataSetObserver;

    invoke-interface {p2, v1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 189
    :cond_1
    return-void

    .line 170
    .end local v0    # "cursorPresent":Z
    :cond_2
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mAutoRequery:Z

    goto :goto_0

    :cond_3
    move v0, v1

    .line 172
    goto :goto_1

    .line 176
    .restart local v0    # "cursorPresent":Z
    :cond_4
    const/4 v1, -0x1

    goto :goto_2

    .line 181
    :cond_5
    iput-object v3, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mChangeObserver:Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter$ChangeObserver;

    .line 182
    iput-object v3, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mDataSetObserver:Landroid/database/DataSetObserver;

    goto :goto_3
.end method

.method protected init(Landroid/content/Context;Landroid/database/Cursor;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "c"    # Landroid/database/Cursor;
    .param p3, "autoRequery"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 162
    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->init(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 163
    return-void

    .line 162
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public newDropDownView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 327
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public abstract newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method protected onContentChanged()V
    .locals 1

    .prologue
    .line 486
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mAutoRequery:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 488
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->requery()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mDataValid:Z

    .line 490
    :cond_0
    return-void
.end method

.method public runQueryOnBackgroundThread(Ljava/lang/CharSequence;)Landroid/database/Cursor;
    .locals 1
    .param p1, "constraint"    # Ljava/lang/CharSequence;

    .prologue
    .line 435
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mFilterQueryProvider:Landroid/widget/FilterQueryProvider;

    if-eqz v0, :cond_0

    .line 436
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mFilterQueryProvider:Landroid/widget/FilterQueryProvider;

    invoke-interface {v0, p1}, Landroid/widget/FilterQueryProvider;->runQuery(Ljava/lang/CharSequence;)Landroid/database/Cursor;

    move-result-object v0

    .line 439
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mCursor:Landroid/database/Cursor;

    goto :goto_0
.end method

.method public setFilterQueryProvider(Landroid/widget/FilterQueryProvider;)V
    .locals 0
    .param p1, "filterQueryProvider"    # Landroid/widget/FilterQueryProvider;

    .prologue
    .line 475
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mFilterQueryProvider:Landroid/widget/FilterQueryProvider;

    .line 476
    return-void
.end method

.method public swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 10
    .param p1, "newCursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v6, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 363
    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mCursor:Landroid/database/Cursor;

    if-ne p1, v7, :cond_0

    move-object v4, v6

    .line 393
    :goto_0
    return-object v4

    .line 366
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mCursor:Landroid/database/Cursor;

    .line 367
    .local v4, "oldCursor":Landroid/database/Cursor;
    if-eqz v4, :cond_2

    .line 368
    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mChangeObserver:Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter$ChangeObserver;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mChangeObserver:Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter$ChangeObserver;

    invoke-interface {v4, v7}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 369
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mDataSetObserver:Landroid/database/DataSetObserver;

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mDataSetObserver:Landroid/database/DataSetObserver;

    invoke-interface {v4, v7}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 371
    :cond_2
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mCursor:Landroid/database/Cursor;

    .line 372
    if-eqz p1, :cond_6

    .line 373
    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mChangeObserver:Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter$ChangeObserver;

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mChangeObserver:Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter$ChangeObserver;

    invoke-interface {p1, v7}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 374
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mDataSetObserver:Landroid/database/DataSetObserver;

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mDataSetObserver:Landroid/database/DataSetObserver;

    invoke-interface {p1, v7}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 376
    :cond_4
    :try_start_0
    const-string v7, "_id"

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mRowIDColumn:I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 383
    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mDataValid:Z

    .line 385
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 377
    :catch_0
    move-exception v1

    .line 378
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-interface {p1}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_5

    aget-object v5, v0, v2

    .line 379
    .local v5, "str":Ljava/lang/String;
    const-string v7, "AbsCursorAdapter"

    invoke-static {v9, v7, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 378
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .end local v5    # "str":Ljava/lang/String;
    :cond_5
    move-object v4, v6

    .line 381
    goto :goto_0

    .line 387
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_6
    const/4 v6, -0x1

    iput v6, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mRowIDColumn:I

    .line 388
    iput-boolean v8, p0, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->mDataValid:Z

    .line 390
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsCursorAdapter;->notifyDataSetInvalidated()V

    .line 391
    const-string v6, "AbsCursorAdapter"

    const-string v7, "swapCursor() newCursor is null"

    invoke-static {v8, v6, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
