.class public Lcom/sec/android/app/myfiles/FileComparator;
.super Ljava/lang/Object;
.source "FileComparator.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/FileComparator$1;,
        Lcom/sec/android/app/myfiles/FileComparator$Types;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# static fields
.field private static final MODULE:Ljava/lang/String; = "FileComparator"


# instance fields
.field private type:Lcom/sec/android/app/myfiles/FileComparator$Types;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/sec/android/app/myfiles/FileComparator$Types;->NAME:Lcom/sec/android/app/myfiles/FileComparator$Types;

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/FileComparator;-><init>(Lcom/sec/android/app/myfiles/FileComparator$Types;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/myfiles/FileComparator$Types;)V
    .locals 0
    .param p1, "type"    # Lcom/sec/android/app/myfiles/FileComparator$Types;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/sec/android/app/myfiles/FileComparator;->type:Lcom/sec/android/app/myfiles/FileComparator$Types;

    .line 40
    return-void
.end method


# virtual methods
.method public compare(Ljava/io/File;Ljava/io/File;)I
    .locals 5
    .param p1, "file"    # Ljava/io/File;
    .param p2, "compared"    # Ljava/io/File;

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 44
    sget-object v2, Lcom/sec/android/app/myfiles/FileComparator$1;->$SwitchMap$com$sec$android$app$myfiles$FileComparator$Types:[I

    iget-object v3, p0, Lcom/sec/android/app/myfiles/FileComparator;->type:Lcom/sec/android/app/myfiles/FileComparator$Types;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/FileComparator$Types;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 50
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    .line 52
    .local v0, "collator":Ljava/text/Collator;
    invoke-virtual {v0, v1}, Ljava/text/Collator;->setStrength(I)V

    .line 53
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p2}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 54
    invoke-virtual {p1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 62
    .end local v0    # "collator":Ljava/text/Collator;
    :goto_0
    return v1

    .line 48
    :pswitch_0
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result v1

    goto :goto_0

    .line 55
    .restart local v0    # "collator":Ljava/text/Collator;
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p2}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_1

    .line 56
    const-string v1, "FileComparator"

    const-string v2, "copmpare return -1"

    invoke-static {v4, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 57
    const/4 v1, -0x1

    goto :goto_0

    .line 58
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p2}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 59
    const-string v2, "FileComparator"

    const-string v3, "copmpare return -1"

    invoke-static {v4, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 62
    :cond_2
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    .line 44
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 25
    check-cast p1, Ljava/io/File;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/io/File;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/myfiles/FileComparator;->compare(Ljava/io/File;Ljava/io/File;)I

    move-result v0

    return v0
.end method
