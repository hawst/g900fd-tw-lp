.class public Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;
.super Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;
.source "DownLoadOperationTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;,
        Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$CancellationRunnable;,
        Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;,
        Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;,
        Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$FileUploadRunnable;,
        Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$FileDownloadRunnable;
    }
.end annotation


# static fields
.field private static final DOWNLOAD:I = 0x0

.field private static final INSIDE_COPY:I = 0x2

.field private static final MAX_PARALLEL_LOAD:I = 0x64

.field private static final MODULE:Ljava/lang/String; = "DownLoadOperationTask"

.field private static final UPLOAD:I = 0x1


# instance fields
.field private CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

.field private mCancelableDownloadList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/DropboxAPI$CancelableDownload;",
            ">;"
        }
    .end annotation
.end field

.field private mCancelableUploadList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/DropboxAPI$CancelableUpload;",
            ">;"
        }
    .end annotation
.end field

.field private mCancellactionThread:Ljava/lang/Thread;

.field private mCopySummarySize:J

.field private mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

.field private mErrorCode:I

.field private mFileLoadThreads:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Thread;",
            ">;"
        }
    .end annotation
.end field

.field private mFiles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;",
            ">;"
        }
    .end annotation
.end field

.field private mOperationDirection:I

.field private mTotalSize:J

.field private mUpFiles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;",
            ">;"
        }
    .end annotation
.end field

.field private mUsedFilesNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/view/View;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contentType"    # I
    .param p3, "progressView"    # Landroid/view/View;
    .param p4, "operation"    # I

    .prologue
    const-wide/16 v4, 0x0

    const/16 v3, 0x8

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 249
    invoke-direct {p0, p1, p4, p2, p3}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;-><init>(Landroid/content/Context;IILandroid/view/View;)V

    .line 59
    iput-wide v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTotalSize:J

    .line 61
    iput-wide v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mCopySummarySize:J

    .line 63
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mCancelableDownloadList:Ljava/util/ArrayList;

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mCancelableUploadList:Ljava/util/ArrayList;

    .line 69
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    .line 71
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    .line 73
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFileLoadThreads:Ljava/util/ArrayList;

    .line 75
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mCancellactionThread:Ljava/lang/Thread;

    .line 77
    iput v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mErrorCode:I

    .line 79
    iput v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOperationDirection:I

    .line 81
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    .line 250
    iput p2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mSourceFragmentId:I

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    .line 252
    iget v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mSourceFragmentId:I

    if-ne v0, v3, :cond_0

    .line 253
    const/16 v0, 0x201

    iput v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetFragmentId:I

    .line 256
    :goto_0
    return-void

    .line 255
    :cond_0
    iput v3, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetFragmentId:I

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/view/View;II)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contentType"    # I
    .param p3, "progressView"    # Landroid/view/View;
    .param p4, "contentTypeOfTarget"    # I
    .param p5, "operation"    # I

    .prologue
    .line 260
    invoke-direct {p0, p1, p2, p3, p5}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;-><init>(Landroid/content/Context;ILandroid/view/View;I)V

    .line 261
    iput p4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetFragmentId:I

    .line 262
    return-void
.end method

.method private CopyOrMoveFiles(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 7
    .param p2, "dstFolder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "srcFilesPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v6, 0x0

    .line 1084
    const-string v4, "DownLoadOperationTask"

    const-string v5, "Inside dropbox copy or move start"

    invoke-static {v6, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 1085
    const-string v4, "Dropbox/"

    invoke-virtual {p2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1086
    const/16 v4, 0x8

    invoke-virtual {p2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p2

    .line 1088
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v4, p1}, Lcom/sec/android/app/myfiles/db/CacheDB;->getCloudPathSizes(Ljava/util/ArrayList;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTotalSize:J

    .line 1089
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mCopySummarySize:J

    .line 1090
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    .line 1091
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1092
    .local v2, "filename":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetFolder:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v2, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1094
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1095
    iget v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOverwriteFileCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOverwriteFileCount:I

    goto :goto_0

    .line 1098
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "filename":Ljava/lang/String;
    :cond_2
    iget v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOverwriteFileCount:I

    iput v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTempOverwriteFileCount:I

    .line 1099
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1100
    .local v0, "f":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->isCancelled()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1108
    .end local v0    # "f":Ljava/lang/String;
    :goto_2
    return-void

    .line 1102
    .restart local v0    # "f":Ljava/lang/String;
    :cond_4
    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->insideDropboxCopy(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1103
    iget v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mRemainOverwriteFileCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mRemainOverwriteFileCount:I

    goto :goto_1

    .line 1106
    .end local v0    # "f":Ljava/lang/String;
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 1107
    const-string v4, "DownLoadOperationTask"

    const-string v5, "Inside dropbox copy or move finished"

    invoke-static {v6, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private DownloadFiles(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 8
    .param p2, "dstFolder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 677
    .local p1, "srcFilesPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->isCancelled()Z

    move-result v4

    if-nez v4, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_1

    .line 703
    :cond_0
    :goto_0
    return-void

    .line 679
    :cond_1
    const/4 v4, 0x0

    const-string v5, "DownLoadOperationTask"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Download start, "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " items"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 680
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mCancelableDownloadList:Ljava/util/ArrayList;

    .line 681
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v4, p1}, Lcom/sec/android/app/myfiles/db/CacheDB;->getCloudPathSizes(Ljava/util/ArrayList;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTotalSize:J

    .line 682
    invoke-static {p2}, Lcom/sec/android/app/myfiles/utils/Utils;->getAvailableSpace(Ljava/lang/String;)J

    move-result-wide v4

    iget-wide v6, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTotalSize:J

    cmp-long v4, v4, v6

    if-gtz v4, :cond_2

    .line 683
    const/4 v4, 0x6

    iput v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mErrorCode:I

    goto :goto_0

    .line 686
    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    .line 687
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 688
    .local v2, "filename":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetFolder:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v2, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 690
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 691
    iget v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOverwriteFileCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOverwriteFileCount:I

    goto :goto_1

    .line 694
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "filename":Ljava/lang/String;
    :cond_4
    iget v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOverwriteFileCount:I

    iput v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTempOverwriteFileCount:I

    .line 695
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 696
    .local v0, "f":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->isCancelled()Z

    move-result v4

    if-nez v4, :cond_0

    .line 698
    invoke-direct {p0, p2, v0}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->DropboxDownload(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 699
    iget v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mRemainOverwriteFileCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mRemainOverwriteFileCount:I

    goto :goto_2

    .line 702
    .end local v0    # "f":Ljava/lang/String;
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->startDownloadFiles()V

    goto/16 :goto_0
.end method

.method private DropboxCopyOrMove(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "fromPath"    # Ljava/lang/String;
    .param p2, "toPath"    # Ljava/lang/String;

    .prologue
    .line 958
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 979
    :cond_0
    :goto_0
    return-void

    .line 960
    :cond_1
    iget-boolean v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOverwrite:Z

    if-eqz v4, :cond_2

    .line 961
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v4, p2}, Lcom/sec/android/app/myfiles/db/CacheDB;->getIfDropboxFileExist(Ljava/lang/String;)Z

    move-result v1

    .line 962
    .local v1, "fileExists":Z
    if-eqz v1, :cond_2

    .line 963
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    invoke-virtual {v4, p2}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->deleteFile(Ljava/lang/String;)V

    .line 966
    .end local v1    # "fileExists":Z
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    iget v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOperationType:I

    invoke-virtual {v4, p1, p2, v5}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->fileCopyOrMove(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    .line 967
    .local v0, "copied":Z
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    if-eqz v4, :cond_3

    .line 968
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    invoke-virtual {v4, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 969
    :cond_3
    if-nez v0, :cond_4

    .line 970
    const/4 v4, 0x2

    iput v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mErrorCode:I

    goto :goto_0

    .line 973
    :cond_4
    iget-wide v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mCopySummarySize:J

    long-to-double v4, v4

    iget-wide v6, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTotalSize:J

    long-to-double v6, v6

    div-double/2addr v4, v6

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    mul-double v2, v4, v6

    .line 975
    .local v2, "progress":D
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    double-to-int v5, v2

    iput v5, v4, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mCurrentTargetPercentage:I

    .line 977
    const/4 v4, 0x1

    new-array v4, v4, [Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    aput-object v6, v4, v5

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->publishProgress([Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private DropboxDownload(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10
    .param p1, "dstFolder"    # Ljava/lang/String;
    .param p2, "serverPath"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 632
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->isCancelled()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 663
    :cond_0
    :goto_0
    return v7

    .line 636
    :cond_1
    const-string v0, ""

    .line 637
    .local v0, "fileName":Ljava/lang/String;
    const-string v5, "/"

    invoke-virtual {p2, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    .line 638
    .local v4, "sPos":I
    const/4 v5, -0x1

    if-eq v4, v5, :cond_2

    .line 639
    add-int/lit8 v5, v4, 0x1

    invoke-virtual {p2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 640
    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_4

    .line 641
    :cond_3
    move-object v0, p2

    .line 643
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v5, p2}, Lcom/sec/android/app/myfiles/db/CacheDB;->isDirectory(Ljava/lang/String;)Z

    move-result v1

    .line 644
    .local v1, "isDirectory":Z
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "/"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 645
    .local v3, "newFilePath":Ljava/lang/String;
    if-nez v1, :cond_6

    move v5, v6

    :goto_1
    invoke-virtual {p0, v3, v5}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->checkDirOrFileName(Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v2

    .line 646
    .local v2, "newFile":Ljava/io/File;
    if-eqz v2, :cond_0

    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mRenameCancelled:Z

    if-nez v5, :cond_0

    .line 648
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    .line 649
    if-eqz v1, :cond_7

    .line 650
    invoke-direct {p0, v3, p2}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->downloadFolder(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    :goto_2
    move v7, v6

    .line 663
    goto :goto_0

    .end local v2    # "newFile":Ljava/io/File;
    :cond_6
    move v5, v7

    .line 645
    goto :goto_1

    .line 652
    .restart local v2    # "newFile":Ljava/io/File;
    :cond_7
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    if-eqz v5, :cond_5

    .line 653
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    new-instance v7, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v8, p2}, Lcom/sec/android/app/myfiles/db/CacheDB;->getCloudFileSize(Ljava/lang/String;)J

    move-result-wide v8

    invoke-direct {v7, p2, v8, v9, v3}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 654
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 655
    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOverwrite:Z

    if-eqz v5, :cond_8

    .line 656
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_2

    .line 659
    :cond_8
    iput v6, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mErrorCode:I

    goto :goto_2
.end method

.method private DropboxFolderUpload(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V
    .locals 15
    .param p1, "localFile"    # Ljava/io/File;
    .param p2, "serverPath"    # Ljava/lang/String;
    .param p3, "NewFolderName"    # Ljava/lang/String;

    .prologue
    .line 830
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 869
    :cond_0
    :goto_0
    return-void

    .line 833
    :cond_1
    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 838
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_2

    .line 840
    const-string p2, "/"

    .line 843
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    move-object/from16 v0, p3

    move-object/from16 v1, p2

    invoke-virtual {v3, v0, v1}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->createFolder(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;

    move-result-object v9

    .line 844
    .local v9, "folder":Lcom/samsung/scloud/data/SCloudFolder;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "/"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 845
    if-nez v9, :cond_3

    .line 846
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->getFolderInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;

    move-result-object v9

    .line 847
    if-nez v9, :cond_3

    .line 848
    const/4 v3, 0x3

    iput v3, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mErrorCode:I

    goto :goto_0

    .line 852
    :cond_3
    if-eqz v9, :cond_4

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    if-eqz v3, :cond_4

    .line 853
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 855
    :cond_4
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v12

    .line 856
    .local v12, "subFiles":[Ljava/io/File;
    if-eqz v12, :cond_0

    array-length v3, v12

    if-eqz v3, :cond_0

    .line 857
    move-object v2, v12

    .local v2, "arr$":[Ljava/io/File;
    array-length v11, v2

    .local v11, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    :goto_1
    if-ge v10, v11, :cond_0

    aget-object v4, v2, v10

    .line 858
    .local v4, "sf":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 859
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-direct {p0, v4, v0, v3}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->DropboxFolderUpload(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    .line 857
    :cond_5
    :goto_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 861
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    if-eqz v3, :cond_5

    .line 862
    iget-object v13, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    new-instance v3, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v6

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v14, "/"

    invoke-virtual {v8, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;-><init>(Ljava/io/File;Ljava/lang/String;JLjava/lang/String;)V

    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 863
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    if-eqz v3, :cond_5

    .line 864
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method private DropboxUpload(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "localPath"    # Ljava/lang/String;
    .param p2, "serverPath"    # Ljava/lang/String;

    .prologue
    .line 806
    const-string v0, "Dropbox/"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 807
    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p2

    .line 809
    :cond_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 810
    .local v2, "localFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    .line 813
    .local v3, "newName":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v0

    invoke-direct {p0, p2, v3, v0}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->checkCloudDirOrFileName(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 814
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 815
    :cond_1
    const/4 v0, 0x0

    .line 825
    :goto_0
    return v0

    .line 816
    :cond_2
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 817
    invoke-direct {p0, v2, p2, v3}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->DropboxFolderUpload(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    .line 825
    :cond_3
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 819
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 820
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;-><init>(Ljava/io/File;Ljava/lang/String;JLjava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 821
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 822
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "/"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private StartUploadFiles()V
    .locals 20

    .prologue
    .line 873
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->isCancelled()Z

    move-result v4

    if-nez v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_1

    .line 900
    :cond_0
    return-void

    .line 876
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;

    .line 877
    .local v11, "file":Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v11, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mServerPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v11, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mNewFileName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 878
    .local v16, "newFilePath":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOverwrite:Z

    if-eqz v4, :cond_2

    .line 879
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->getIfDropboxFileExist(Ljava/lang/String;)Z

    move-result v12

    .line 880
    .local v12, "fileExist":Z
    if-eqz v12, :cond_2

    .line 881
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->deleteFile(Ljava/lang/String;)V

    .line 885
    .end local v12    # "fileExist":Z
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    iget-object v5, v11, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mLocalFile:Ljava/io/File;

    iget-object v6, v11, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mServerPath:Ljava/lang/String;

    iget-object v7, v11, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mNewFileName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOverwrite:Z

    const-string v9, ""

    new-instance v10, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTotalSize:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-direct {v10, v0, v1, v2, v11}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;-><init>(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;JLcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;)V

    invoke-virtual/range {v4 .. v10}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->uploadCancelable(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/DropboxAPI$CancelableUpload;

    move-result-object v4

    iput-object v4, v11, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mUpload:Lcom/samsung/scloud/DropboxAPI$CancelableUpload;

    .line 887
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mCancelableUploadList:Ljava/util/ArrayList;

    iget-object v5, v11, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mUpload:Lcom/samsung/scloud/DropboxAPI$CancelableUpload;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 890
    .end local v11    # "file":Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;
    .end local v16    # "newFilePath":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v15

    .line 891
    .local v15, "n":I
    const/16 v4, 0x64

    if-le v15, v4, :cond_4

    .line 892
    const/16 v15, 0x64

    .line 893
    :cond_4
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFileLoadThreads:Ljava/util/ArrayList;

    .line 895
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_1
    if-ge v13, v15, :cond_0

    .line 896
    new-instance v17, Ljava/lang/Thread;

    new-instance v4, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$FileUploadRunnable;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$FileUploadRunnable;-><init>(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;)V

    move-object/from16 v0, v17

    invoke-direct {v0, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 897
    .local v17, "thread":Ljava/lang/Thread;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFileLoadThreads:Ljava/util/ArrayList;

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 898
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Thread;->start()V

    .line 895
    add-int/lit8 v13, v13, 0x1

    goto :goto_1
.end method

.method private UploadFiles(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 9
    .param p2, "dstFolder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "srcFilesPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v8, 0x1

    .line 917
    const/4 v5, 0x0

    const-string v6, "DownLoadOperationTask"

    const-string v7, "Upload start"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 919
    iget v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mSourceFragmentId:I

    if-nez v5, :cond_1

    iget v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetFragmentId:I

    const/16 v6, 0x8

    if-ne v5, v6, :cond_1

    .line 921
    iput v8, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mErrorCode:I

    .line 954
    :cond_0
    :goto_0
    return-void

    .line 925
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mContext:Landroid/content/Context;

    invoke-static {v5, p1}, Lcom/sec/android/app/myfiles/utils/Utils;->getPathFilesSize(Landroid/content/Context;Ljava/util/ArrayList;)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTotalSize:J

    .line 926
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mCancelableUploadList:Ljava/util/ArrayList;

    .line 927
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    .line 928
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    .line 930
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 931
    .local v3, "filename":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetFolder:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v3, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v3, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 933
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 934
    iget v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOverwriteFileCount:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOverwriteFileCount:I

    goto :goto_1

    .line 937
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "filename":Ljava/lang/String;
    :cond_3
    iget v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOverwriteFileCount:I

    iput v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTempOverwriteFileCount:I

    .line 938
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 940
    .local v1, "f":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 941
    .local v0, "checkFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_5

    .line 943
    iput v8, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mErrorCode:I

    goto/16 :goto_0

    .line 947
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->isCancelled()Z

    move-result v5

    if-nez v5, :cond_0

    .line 949
    invoke-direct {p0, v1, p2}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->DropboxUpload(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 950
    iget v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mRemainOverwriteFileCount:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mRemainOverwriteFileCount:I

    goto :goto_2

    .line 953
    .end local v0    # "checkFile":Ljava/io/File;
    .end local v1    # "f":Ljava/lang/String;
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->StartUploadFiles()V

    goto/16 :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;)Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mCancelableDownloadList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mCancelableUploadList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->updateProgressBar()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;[Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 45
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->publishProgress([Ljava/lang/Object;)V

    return-void
.end method

.method private checkCloudDirOrFileName(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 8
    .param p1, "serverPath"    # Ljava/lang/String;
    .param p2, "newDirName"    # Ljava/lang/String;
    .param p3, "isFile"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 755
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ne v5, v3, :cond_5

    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mSameFolder:Z

    if-nez v5, :cond_5

    .line 756
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mPath:Ljava/lang/String;

    .line 757
    iput-boolean v7, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOverwrite:Z

    .line 758
    iput-boolean v7, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mRunRename:Z

    .line 759
    iput-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mRename:Ljava/lang/String;

    .line 760
    iput-boolean v7, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mRenameCancelled:Z

    .line 761
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/myfiles/db/CacheDB;->getIfDropboxFileExist(Ljava/lang/String;)Z

    move-result v2

    .line 762
    .local v2, "fileExist":Z
    if-eqz v2, :cond_1

    .line 763
    if-eqz p3, :cond_2

    :goto_0
    iput v3, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFormat:I

    .line 764
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->showRenameDialog()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 765
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "/"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3, p3}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->startRename(Ljava/lang/String;Z)V

    .line 768
    :cond_0
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mRunRename:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mRename:Ljava/lang/String;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mRename:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_3

    .line 769
    iget-object p2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mRename:Ljava/lang/String;

    .line 800
    .end local v2    # "fileExist":Z
    .end local p2    # "newDirName":Ljava/lang/String;
    :cond_1
    :goto_1
    return-object p2

    .line 763
    .restart local v2    # "fileExist":Z
    .restart local p2    # "newDirName":Ljava/lang/String;
    :cond_2
    const/16 v3, 0x3001

    goto :goto_0

    .line 770
    :cond_3
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOverwrite:Z

    if-eqz v3, :cond_4

    .line 771
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/storage/sdcard0/Android/data/com.dropbox.android/files/scratch"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 773
    .local v1, "cachePath":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 775
    .local v0, "cacheFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 777
    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteDropboxThumbnailCache(Ljava/io/File;)V

    goto :goto_1

    .end local v0    # "cacheFile":Ljava/io/File;
    .end local v1    # "cachePath":Ljava/lang/String;
    :cond_4
    move-object p2, v4

    .line 781
    goto :goto_1

    .line 799
    .end local v2    # "fileExist":Z
    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3, p3}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->startRename(Ljava/lang/String;Z)V

    .line 800
    iget-object p2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mRename:Ljava/lang/String;

    goto :goto_1
.end method

.method private checkIfFileNameInUse(Ljava/lang/String;)Z
    .locals 4
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 708
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    if-nez v3, :cond_1

    .line 714
    :cond_0
    :goto_0
    return v2

    .line 710
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 711
    .local v0, "fName":Ljava/lang/String;
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 712
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private checkProcessSuccess()Z
    .locals 8

    .prologue
    const-wide/16 v6, 0x64

    const/4 v2, 0x1

    .line 391
    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOperationDirection:I

    if-nez v3, :cond_2

    .line 392
    const/4 v2, 0x1

    .line 393
    .local v2, "success":Z
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    if-eqz v3, :cond_3

    .line 394
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 395
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;

    iget-wide v4, v3, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;->mDownloadPercent:J

    cmp-long v3, v4, v6

    if-gez v3, :cond_1

    .line 396
    new-instance v0, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;->mDestinationPath:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 397
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 398
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 399
    :cond_0
    const/4 v2, 0x0

    .line 394
    .end local v0    # "f":Ljava/io/File;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 404
    .end local v1    # "i":I
    .end local v2    # "success":Z
    :cond_2
    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOperationDirection:I

    if-ne v3, v2, :cond_3

    .line 405
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    if-eqz v3, :cond_3

    .line 406
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 407
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;

    iget-wide v4, v3, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mUploadPercent:J

    cmp-long v3, v4, v6

    if-gez v3, :cond_4

    .line 408
    const/4 v2, 0x0

    .line 412
    .end local v1    # "i":I
    :cond_3
    return v2

    .line 406
    .restart local v1    # "i":I
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private deleteDownloadableFiles(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 668
    .local p1, "srcFilesPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    .line 669
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 670
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->deleteFile(Ljava/lang/String;)V

    .line 669
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 673
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method private deleteUploadableFiles(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 904
    .local p1, "srcFilesPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 905
    new-instance v1, Ljava/io/File;

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 907
    .local v1, "file":Ljava/io/File;
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->deleteInFileSystem(Ljava/io/File;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 904
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 908
    :catch_0
    move-exception v0

    .line 909
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 910
    const/4 v3, 0x7

    iput v3, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mErrorCode:I

    goto :goto_1

    .line 913
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "file":Ljava/io/File;
    :cond_0
    return-void
.end method

.method private downloadFolder(Ljava/lang/String;Ljava/lang/String;)V
    .locals 16
    .param p1, "dstFolder"    # Ljava/lang/String;
    .param p2, "serverPath"    # Ljava/lang/String;

    .prologue
    .line 554
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->isCancelled()Z

    move-result v14

    if-eqz v14, :cond_1

    .line 628
    :cond_0
    :goto_0
    return-void

    .line 557
    :cond_1
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v14

    const/16 v15, 0xff

    if-le v14, v15, :cond_2

    .line 558
    const/16 v14, 0x8

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mErrorCode:I

    goto :goto_0

    .line 562
    :cond_2
    new-instance v8, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 564
    .local v8, "newFolder":Ljava/io/File;
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOverwrite:Z

    if-nez v14, :cond_3

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v14

    if-eqz v14, :cond_3

    .line 565
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mErrorCode:I

    goto :goto_0

    .line 569
    :cond_3
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v14

    if-nez v14, :cond_4

    .line 570
    invoke-virtual {v8}, Ljava/io/File;->mkdirs()Z

    move-result v14

    if-nez v14, :cond_4

    .line 571
    const/4 v14, 0x3

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mErrorCode:I

    goto :goto_0

    .line 576
    :cond_4
    invoke-virtual {v8}, Ljava/io/File;->canWrite()Z

    move-result v14

    if-nez v14, :cond_5

    .line 577
    const/16 v14, 0x9

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mErrorCode:I

    goto :goto_0

    .line 580
    :cond_5
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v11

    .line 583
    .local v11, "pathLen":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->getCloudPathItemList(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 584
    .local v2, "cursor":Landroid/database/Cursor;
    if-eqz v2, :cond_0

    .line 586
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v14

    if-gtz v14, :cond_6

    .line 587
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 590
    :cond_6
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 592
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->isCancelled()Z

    move-result v14

    if-eqz v14, :cond_8

    .line 627
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 595
    :cond_8
    const-string v14, "_data"

    invoke-interface {v2, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    invoke-interface {v2, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 596
    .local v9, "path":Ljava/lang/String;
    add-int/lit8 v14, v11, 0x1

    invoke-virtual {v9, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 597
    .local v10, "pathInFolder":Ljava/lang/String;
    const-string v14, "format"

    invoke-interface {v2, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    invoke-interface {v2, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 598
    .local v6, "format":I
    const-string v14, "_size"

    invoke-interface {v2, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    invoke-interface {v2, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    int-to-long v12, v14

    .line 599
    .local v12, "size":J
    invoke-virtual {v8}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 600
    .local v3, "dstPlace":Ljava/lang/String;
    if-eqz v10, :cond_9

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v14

    if-lez v14, :cond_9

    .line 601
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 602
    :cond_9
    const/16 v14, 0x3001

    if-ne v6, v14, :cond_c

    .line 603
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 604
    .local v5, "folder":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v14

    if-eqz v14, :cond_a

    .line 606
    :try_start_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->deleteInFileSystem(Ljava/io/File;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 612
    :cond_a
    :goto_2
    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    move-result v14

    if-nez v14, :cond_b

    .line 613
    const/4 v14, 0x3

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mErrorCode:I

    .line 626
    .end local v5    # "folder":Ljava/io/File;
    :cond_b
    :goto_3
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v14

    if-nez v14, :cond_7

    goto :goto_1

    .line 607
    .restart local v5    # "folder":Ljava/io/File;
    :catch_0
    move-exception v4

    .line 609
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 615
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v5    # "folder":Ljava/io/File;
    :cond_c
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    if-eqz v14, :cond_b

    .line 616
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    new-instance v15, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;

    invoke-direct {v15, v9, v12, v13, v3}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 617
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 618
    .local v7, "newFile":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v14

    if-eqz v14, :cond_b

    .line 619
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOverwrite:Z

    if-eqz v14, :cond_d

    .line 620
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    goto :goto_3

    .line 622
    :cond_d
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mErrorCode:I

    goto :goto_3
.end method

.method private insideDropboxCopy(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 24
    .param p1, "fromPath"    # Ljava/lang/String;
    .param p2, "toPath"    # Ljava/lang/String;

    .prologue
    .line 983
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->isDirectory(Ljava/lang/String;)Z

    move-result v11

    .line 986
    .local v11, "isDirectory":Z
    const-string v7, ""

    .line 987
    .local v7, "fileName":Ljava/lang/String;
    const-string v20, "/"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v17

    .line 988
    .local v17, "sPos":I
    const/16 v20, -0x1

    move/from16 v0, v17

    move/from16 v1, v20

    if-eq v0, v1, :cond_0

    .line 989
    add-int/lit8 v20, v17, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 994
    :goto_0
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 995
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOperationType:I

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_1

    .line 996
    const/16 v20, 0x4

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mErrorCode:I

    .line 997
    const/16 v20, 0x0

    .line 1079
    :goto_1
    return v20

    .line 991
    :cond_0
    move-object/from16 v7, p1

    goto :goto_0

    .line 999
    :cond_1
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mSameFolder:Z

    .line 1006
    :cond_2
    if-nez v11, :cond_5

    const/16 v20, 0x1

    :goto_2
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, v20

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->checkCloudDirOrFileName(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v12

    .line 1007
    .local v12, "newName":Ljava/lang/String;
    if-eqz v12, :cond_3

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v20

    if-nez v20, :cond_6

    .line 1008
    :cond_3
    const/16 v20, 0x0

    goto :goto_1

    .line 1000
    .end local v12    # "newName":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 1001
    const/16 v20, 0x5

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mErrorCode:I

    .line 1002
    const/16 v20, 0x0

    goto :goto_1

    .line 1006
    :cond_5
    const/16 v20, 0x0

    goto :goto_2

    .line 1011
    .restart local v12    # "newName":Ljava/lang/String;
    :cond_6
    if-eqz v11, :cond_13

    .line 1013
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    move-object/from16 v21, v0

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v20

    if-nez v20, :cond_9

    const-string v20, "/"

    :goto_3
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v12, v1}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->createFolder(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;

    move-result-object v9

    .line 1014
    .local v9, "folder":Lcom/samsung/scloud/data/SCloudFolder;
    if-nez v9, :cond_7

    .line 1015
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "/"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->getFolderInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;

    move-result-object v9

    .line 1016
    if-nez v9, :cond_7

    .line 1017
    const/16 v20, 0x3

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mErrorCode:I

    .line 1020
    :cond_7
    if-eqz v9, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    if-eqz v20, :cond_8

    .line 1021
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-virtual {v9}, Lcom/samsung/scloud/data/SCloudFolder;->getAbsolutePath()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1023
    :cond_8
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 1024
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v15

    .line 1027
    .local v15, "pathLen":I
    const/4 v4, 0x0

    .line 1028
    .local v4, "cursor":Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->getCloudPathItemList(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 1029
    if-nez v4, :cond_a

    .line 1030
    const/16 v20, 0x0

    goto/16 :goto_1

    .end local v4    # "cursor":Landroid/database/Cursor;
    .end local v9    # "folder":Lcom/samsung/scloud/data/SCloudFolder;
    .end local v15    # "pathLen":I
    :cond_9
    move-object/from16 v20, p2

    .line 1013
    goto/16 :goto_3

    .line 1031
    .restart local v4    # "cursor":Landroid/database/Cursor;
    .restart local v9    # "folder":Lcom/samsung/scloud/data/SCloudFolder;
    .restart local v15    # "pathLen":I
    :cond_a
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v20

    if-gtz v20, :cond_b

    .line 1032
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 1033
    const/16 v20, 0x0

    goto/16 :goto_1

    .line 1035
    :cond_b
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1037
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->isCancelled()Z

    move-result v20

    if-eqz v20, :cond_d

    .line 1038
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 1039
    const/16 v20, 0x0

    goto/16 :goto_1

    .line 1041
    :cond_d
    const-string v20, "_data"

    move-object/from16 v0, v20

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 1042
    .local v13, "path":Ljava/lang/String;
    add-int/lit8 v20, v15, 0x1

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    .line 1043
    .local v14, "pathInFolder":Ljava/lang/String;
    if-eqz v14, :cond_f

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v20

    if-lez v20, :cond_f

    .line 1044
    const-string v20, "format"

    move-object/from16 v0, v20

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 1045
    .local v10, "format":I
    const-string v20, "_size"

    move-object/from16 v0, v20

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 1046
    .local v18, "size":J
    const-string v20, "/"

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v16

    .line 1047
    .local v16, "pos":I
    const-string v5, ""

    .line 1048
    .local v5, "fName":Ljava/lang/String;
    const-string v6, ""

    .line 1049
    .local v6, "fPath":Ljava/lang/String;
    const/16 v20, -0x1

    move/from16 v0, v16

    move/from16 v1, v20

    if-eq v0, v1, :cond_11

    .line 1050
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const/16 v21, 0x0

    move/from16 v0, v21

    move/from16 v1, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1051
    add-int/lit8 v20, v16, 0x1

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 1055
    :goto_4
    const/16 v20, 0x3001

    move/from16 v0, v20

    if-ne v10, v0, :cond_12

    .line 1056
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v0, v5, v1}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->createFolder(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;

    move-result-object v8

    .line 1057
    .local v8, "fold":Lcom/samsung/scloud/data/SCloudFolder;
    if-nez v8, :cond_e

    .line 1058
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "/"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->getFolderInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;

    move-result-object v8

    .line 1059
    if-nez v8, :cond_e

    .line 1060
    const/16 v20, 0x3

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mErrorCode:I

    .line 1063
    :cond_e
    if-eqz v8, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    if-eqz v20, :cond_f

    .line 1064
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-virtual {v8}, Lcom/samsung/scloud/data/SCloudFolder;->getAbsolutePath()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1070
    .end local v5    # "fName":Ljava/lang/String;
    .end local v6    # "fPath":Ljava/lang/String;
    .end local v8    # "fold":Lcom/samsung/scloud/data/SCloudFolder;
    .end local v10    # "format":I
    .end local v16    # "pos":I
    .end local v18    # "size":J
    :cond_f
    :goto_5
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v20

    if-nez v20, :cond_c

    .line 1071
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 1076
    .end local v4    # "cursor":Landroid/database/Cursor;
    .end local v9    # "folder":Lcom/samsung/scloud/data/SCloudFolder;
    .end local v13    # "path":Ljava/lang/String;
    .end local v14    # "pathInFolder":Ljava/lang/String;
    .end local v15    # "pathLen":I
    :goto_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOperationType:I

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_10

    .line 1077
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->deleteFile(Ljava/lang/String;)V

    .line 1079
    :cond_10
    const/16 v20, 0x1

    goto/16 :goto_1

    .line 1053
    .restart local v4    # "cursor":Landroid/database/Cursor;
    .restart local v5    # "fName":Ljava/lang/String;
    .restart local v6    # "fPath":Ljava/lang/String;
    .restart local v9    # "folder":Lcom/samsung/scloud/data/SCloudFolder;
    .restart local v10    # "format":I
    .restart local v13    # "path":Ljava/lang/String;
    .restart local v14    # "pathInFolder":Ljava/lang/String;
    .restart local v15    # "pathLen":I
    .restart local v16    # "pos":I
    .restart local v18    # "size":J
    :cond_11
    move-object v5, v14

    goto/16 :goto_4

    .line 1066
    :cond_12
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mCopySummarySize:J

    move-wide/from16 v20, v0

    add-long v20, v20, v18

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mCopySummarySize:J

    .line 1067
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v13, v1}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->DropboxCopyOrMove(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 1073
    .end local v4    # "cursor":Landroid/database/Cursor;
    .end local v5    # "fName":Ljava/lang/String;
    .end local v6    # "fPath":Ljava/lang/String;
    .end local v9    # "folder":Lcom/samsung/scloud/data/SCloudFolder;
    .end local v10    # "format":I
    .end local v13    # "path":Ljava/lang/String;
    .end local v14    # "pathInFolder":Ljava/lang/String;
    .end local v15    # "pathLen":I
    .end local v16    # "pos":I
    .end local v18    # "size":J
    :cond_13
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mCopySummarySize:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->getCloudFileSize(Ljava/lang/String;)J

    move-result-wide v22

    add-long v20, v20, v22

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mCopySummarySize:J

    .line 1074
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->DropboxCopyOrMove(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6
.end method

.method private startDownloadFiles()V
    .locals 12

    .prologue
    .line 519
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->isCancelled()Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-nez v6, :cond_1

    .line 549
    :cond_0
    return-void

    .line 522
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;

    .line 524
    .local v0, "file":Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;
    new-instance v4, Ljava/io/File;

    iget-object v6, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;->mDestinationPath:Ljava/lang/String;

    invoke-direct {v4, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 526
    .local v4, "newFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    const/16 v7, 0xff

    if-le v6, v7, :cond_2

    .line 527
    const/16 v6, 0x8

    iput v6, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mErrorCode:I

    goto :goto_0

    .line 531
    :cond_2
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 532
    const/4 v6, 0x1

    iput v6, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mErrorCode:I

    goto :goto_0

    .line 535
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;->mServerPath:Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v9, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;

    iget-wide v10, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTotalSize:J

    invoke-direct {v9, p0, v10, v11, v0}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$ConcreteProgressListener;-><init>(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;JLcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;)V

    invoke-virtual {v6, v4, v7, v8, v9}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->downloadCancelable(Ljava/io/File;Ljava/lang/String;ZLcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/DropboxAPI$CancelableDownload;

    move-result-object v6

    iput-object v6, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;->mDownload:Lcom/samsung/scloud/DropboxAPI$CancelableDownload;

    .line 536
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mCancelableDownloadList:Ljava/util/ArrayList;

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;->mDownload:Lcom/samsung/scloud/DropboxAPI$CancelableDownload;

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 539
    .end local v0    # "file":Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;
    .end local v4    # "newFile":Ljava/io/File;
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 540
    .local v3, "n":I
    const/16 v6, 0x64

    if-le v3, v6, :cond_5

    .line 541
    const/16 v3, 0x64

    .line 542
    :cond_5
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFileLoadThreads:Ljava/util/ArrayList;

    .line 544
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v3, :cond_0

    .line 545
    new-instance v5, Ljava/lang/Thread;

    new-instance v6, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$FileDownloadRunnable;

    invoke-direct {v6, p0}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$FileDownloadRunnable;-><init>(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;)V

    invoke-direct {v5, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 546
    .local v5, "thread":Ljava/lang/Thread;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFileLoadThreads:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 547
    invoke-virtual {v5}, Ljava/lang/Thread;->start()V

    .line 544
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private declared-synchronized updateProgressBar()V
    .locals 10

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 444
    monitor-enter p0

    const-wide/16 v2, 0x0

    .line 445
    .local v2, "totalProgress":D
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 446
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 447
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;

    iget-wide v4, v1, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;->mSize:J

    long-to-double v4, v4

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;

    iget-wide v6, v1, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$DownloadableFile;->mDownloadPercent:J

    long-to-double v6, v6

    mul-double/2addr v4, v6

    mul-double/2addr v4, v8

    iget-wide v6, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTotalSize:J

    long-to-double v6, v6

    div-double/2addr v4, v6

    add-double/2addr v2, v4

    .line 446
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 450
    .end local v0    # "i":I
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 451
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 452
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;

    iget-wide v4, v1, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mSize:J

    long-to-double v4, v4

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;

    iget-wide v6, v1, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$UploadableFile;->mUploadPercent:J

    long-to-double v6, v6

    mul-double/2addr v4, v6

    mul-double/2addr v4, v8

    iget-wide v6, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTotalSize:J

    long-to-double v6, v6

    div-double/2addr v4, v6

    add-double/2addr v2, v4

    .line 451
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 455
    .end local v0    # "i":I
    :cond_1
    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    cmpl-double v1, v2, v4

    if-lez v1, :cond_2

    .line 456
    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    .line 457
    :cond_2
    const-string v1, "DownLoadOperationTask"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Total copy process: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    double-to-int v4, v2

    iput v4, v1, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mCurrentTargetPercentage:I

    .line 461
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    aput-object v5, v1, v4

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->publishProgress([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 462
    monitor-exit p0

    return-void

    .line 444
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method protected cancelOperation()V
    .locals 2

    .prologue
    .line 417
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 427
    :cond_0
    :goto_0
    return-void

    .line 419
    :cond_1
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->cancelOperation()V

    .line 420
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mCancellactionThread:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    .line 422
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mCancellactionThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/lang/IllegalThreadStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 423
    :catch_0
    move-exception v0

    .line 424
    .local v0, "ex":Ljava/lang/IllegalThreadStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalThreadStateException;->printStackTrace()V

    goto :goto_0
.end method

.method protected varargs doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;
    .locals 10
    .param p1, "params"    # [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .prologue
    const/4 v9, 0x2

    const/16 v8, 0x8

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 273
    aget-object v2, p1, v5

    .line 274
    .local v2, "param":Lcom/sec/android/app/myfiles/element/FileOperationParam;
    iget-object v4, v2, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    iput-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetFolder:Ljava/lang/String;

    .line 276
    iget v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetFragmentId:I

    if-ne v4, v8, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetFolder:Ljava/lang/String;

    if-nez v4, :cond_0

    .line 278
    const-string v4, "Dropbox/"

    iput-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetFolder:Ljava/lang/String;

    .line 280
    :cond_0
    iget-object v4, v2, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    iput-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    .line 281
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    if-nez v4, :cond_7

    move v4, v5

    :goto_0
    iput v4, v6, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mTotalTarget:I

    .line 282
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget v6, v2, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mStartIndex:I

    iput v6, v4, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTarget:I

    .line 283
    new-instance v4, Ljava/lang/Thread;

    new-instance v6, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$CancellationRunnable;

    invoke-direct {v6, p0}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$CancellationRunnable;-><init>(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;)V

    invoke-direct {v4, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mCancellactionThread:Ljava/lang/Thread;

    .line 284
    new-instance v4, Lcom/sec/android/app/myfiles/db/CacheDB;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mContext:Landroid/content/Context;

    invoke-direct {v4, v6}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    .line 285
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 286
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    if-eqz v4, :cond_1

    .line 287
    iget v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mSourceFragmentId:I

    if-ne v4, v8, :cond_9

    .line 288
    iget v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetFragmentId:I

    if-ne v4, v8, :cond_8

    .line 289
    iput v9, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOperationDirection:I

    .line 290
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetFolder:Ljava/lang/String;

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->CopyOrMoveFiles(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 300
    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 302
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFileLoadThreads:Ljava/util/ArrayList;

    if-eqz v4, :cond_3

    .line 303
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFileLoadThreads:Ljava/util/ArrayList;

    monitor-enter v5

    .line 305
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFileLoadThreads:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Thread;

    .line 306
    .local v3, "thread":Ljava/lang/Thread;
    invoke-virtual {v3}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 307
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "thread":Ljava/lang/Thread;
    :catch_0
    move-exception v0

    .line 308
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 310
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_2
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 313
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mCancellactionThread:Ljava/lang/Thread;

    if-eqz v4, :cond_4

    .line 314
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->isCancelled()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 315
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mCancellactionThread:Ljava/lang/Thread;

    monitor-enter v5

    .line 317
    :try_start_2
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mCancellactionThread:Ljava/lang/Thread;

    invoke-virtual {v4}, Ljava/lang/Thread;->join()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 321
    :goto_3
    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 324
    :cond_4
    iget-boolean v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mRenameCancelled:Z

    if-nez v4, :cond_6

    .line 325
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->checkProcessSuccess()Z

    move-result v4

    if-nez v4, :cond_5

    .line 326
    iput v9, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mErrorCode:I

    .line 327
    :cond_5
    iget v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mErrorCode:I

    if-nez v4, :cond_6

    iget v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOperationType:I

    if-ne v4, v7, :cond_6

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->isCancelled()Z

    move-result v4

    if-nez v4, :cond_6

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    if-eqz v4, :cond_6

    .line 328
    iget v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOperationDirection:I

    if-nez v4, :cond_a

    .line 329
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->deleteDownloadableFiles(Ljava/util/ArrayList;)V

    .line 335
    :cond_6
    :goto_4
    const/4 v4, 0x0

    return-object v4

    .line 281
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    goto/16 :goto_0

    .line 292
    :cond_8
    iput v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOperationDirection:I

    .line 293
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetFolder:Ljava/lang/String;

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->DownloadFiles(Ljava/util/ArrayList;Ljava/lang/String;)V

    goto :goto_1

    .line 296
    :cond_9
    iput v7, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOperationDirection:I

    .line 297
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetFolder:Ljava/lang/String;

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->UploadFiles(Ljava/util/ArrayList;Ljava/lang/String;)V

    goto :goto_1

    .line 310
    :catchall_0
    move-exception v4

    :try_start_4
    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v4

    .line 318
    :catch_1
    move-exception v0

    .line 319
    .restart local v0    # "e":Ljava/lang/InterruptedException;
    :try_start_5
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_3

    .line 321
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_1
    move-exception v4

    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v4

    .line 330
    :cond_a
    iget v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOperationDirection:I

    if-ne v4, v7, :cond_6

    .line 331
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->deleteUploadableFiles(Ljava/util/ArrayList;)V

    goto :goto_4
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 45
    check-cast p1, [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 2

    .prologue
    .line 431
    iget v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOperationType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 433
    const v0, 0x7f0b00cb

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->showToast(I)V

    .line 439
    :goto_0
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->onCancelled()V

    .line 440
    return-void

    .line 437
    :cond_0
    const v0, 0x7f0b00c9

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->showToast(I)V

    goto :goto_0
.end method

.method protected onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V
    .locals 6
    .param p1, "result"    # Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 356
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mRenameCancelled:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 357
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mErrorCode:I

    if-eqz v1, :cond_4

    .line 358
    const-string v1, "DownLoadOperationTask"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to copy, error code = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mErrorCode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOperationType:I

    if-ne v1, v5, :cond_3

    .line 360
    const v1, 0x7f0b00cb

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->showToast(I)V

    .line 374
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetFolder:Ljava/lang/String;

    const-string v2, "/storage/sdcard0/Android/data/com.dropbox.android/files/scratch"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 375
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetFolder:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/android/app/myfiles/utils/Utils;->sendScan(Landroid/content/Context;Ljava/lang/String;)V

    .line 376
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    if-eqz v1, :cond_1

    .line 377
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 378
    .local v0, "data":Landroid/os/Bundle;
    const-string v1, "/storage/sdcard0/Android/data/com.dropbox.android/files/scratch"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 379
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    invoke-interface {v1, v4, v4, v0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;->onCompleted(IILandroid/os/Bundle;)V

    .line 381
    .end local v0    # "data":Landroid/os/Bundle;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetName:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetName:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 382
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mTargetName:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 387
    :cond_2
    :goto_1
    return-void

    .line 362
    :cond_3
    const v1, 0x7f0b00c9

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->showToast(I)V

    goto :goto_0

    .line 364
    :cond_4
    iget v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOperationType:I

    if-ne v1, v5, :cond_5

    .line 365
    const v1, 0x7f0c0008

    iget v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mRemainOverwriteFileCount:I

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->showToastPrurals(II)V

    .line 366
    iput v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mRemainOverwriteFileCount:I

    goto :goto_0

    .line 369
    :cond_5
    const v1, 0x7f0c0005

    iget v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mRemainOverwriteFileCount:I

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->showToastPrurals(II)V

    .line 370
    iput v4, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mRemainOverwriteFileCount:I

    goto :goto_0

    .line 385
    :cond_6
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 45
    check-cast p1, Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Lcom/sec/android/app/myfiles/element/FileOperationProgress;)V
    .locals 4
    .param p1, "values"    # [Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    .prologue
    .line 341
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 343
    const/4 v1, 0x0

    aget-object v1, p1, v1

    iget v0, v1, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mCurrentTargetPercentage:I

    .line 344
    .local v0, "percent":I
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 346
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mPercentText:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 348
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mPercentText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 351
    :cond_0
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 45
    check-cast p1, [Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->onProgressUpdate([Lcom/sec/android/app/myfiles/element/FileOperationProgress;)V

    return-void
.end method

.method protected setupProgressView()V
    .locals 2

    .prologue
    .line 267
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 268
    return-void
.end method

.method protected startRename(Ljava/lang/String;Z)V
    .locals 10
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "isFile"    # Z

    .prologue
    const/4 v9, 0x0

    .line 720
    iget v6, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mOperationDirection:I

    if-nez v6, :cond_1

    .line 721
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->startRename(Ljava/lang/String;Z)V

    .line 751
    :cond_0
    :goto_0
    return-void

    .line 725
    :cond_1
    const-string v6, "/"

    invoke-virtual {p1, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 726
    .local v0, "dPos":I
    move-object v4, p1

    .line 727
    .local v4, "newDirName":Ljava/lang/String;
    const-string v5, "/"

    .line 728
    .local v5, "serverPath":Ljava/lang/String;
    if-ltz v0, :cond_2

    .line 729
    add-int/lit8 v6, v0, 0x1

    invoke-virtual {p1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 730
    invoke-virtual {p1, v9, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 732
    :cond_2
    const/4 v3, 0x1

    .line 733
    .local v3, "i":I
    const/4 v2, 0x0

    .line 734
    .local v2, "fileExist":Z
    move-object v1, v4

    .line 736
    .local v1, "dirName":Ljava/lang/String;
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/myfiles/db/CacheDB;->getIfDropboxFileExist(Ljava/lang/String;)Z

    move-result v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->checkIfFileNameInUse(Ljava/lang/String;)Z

    move-result v7

    or-int v2, v6, v7

    .line 737
    if-eqz v2, :cond_4

    .line 739
    invoke-virtual {p0, v4, v3, p2}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->addPostfix(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v1

    .line 740
    add-int/lit8 v3, v3, 0x1

    .line 742
    :cond_4
    if-nez v2, :cond_3

    .line 744
    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 745
    const-string v6, "DownLoadOperationTask"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "New file or folder name: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 747
    :cond_5
    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mRename:Ljava/lang/String;

    .line 748
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mHandler:Landroid/os/Handler;

    if-eqz v6, :cond_0

    .line 749
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mHandler:Landroid/os/Handler;

    invoke-virtual {v6, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0
.end method
