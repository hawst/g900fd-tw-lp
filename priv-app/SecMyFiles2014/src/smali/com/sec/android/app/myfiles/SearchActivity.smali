.class public Lcom/sec/android/app/myfiles/SearchActivity;
.super Lcom/sec/android/app/myfiles/AbsMainActivity;
.source "SearchActivity.java"

# interfaces
.implements Landroid/widget/SearchView$OnQueryTextListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/SearchActivity$SpinnerArrayAdapter;
    }
.end annotation


# static fields
.field public static FRAGMENT_LAYOUT_ID:I = 0x0

.field private static final MODULE:Ljava/lang/String; = "SearchActivity"

.field private static final SEARCH_ADVANCED:I = 0x5

.field private static final SEARCH_ADVANCE_FRAGMENT_TAG:Ljava/lang/String; = "SearchAdvanceFragment"

.field private static final SEARCH_ALL:I = 0x0

.field private static final SEARCH_DATE:I = 0x3

.field private static final SEARCH_EXTENSION:I = 0x4

.field private static final SEARCH_FILE_TYPE:I = 0x2

.field private static final SEARCH_LOCATION:I = 0x1


# instance fields
.field private final MAX_SEARCH_TEXT_LIMIT:I

.field private isSearchPeforming:Z

.field private mCalendar:Ljava/util/Calendar;

.field mConnectionChangeReceiver:Landroid/content/BroadcastReceiver;

.field private mContext:Landroid/content/Context;

.field protected mCurrentFileTypes:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/sec/android/app/myfiles/utils/FileType;",
            ">;"
        }
    .end annotation
.end field

.field protected mCurrentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

.field protected mCurrentFromDate:J

.field protected mCurrentSearchType:I

.field protected mCurrentToDate:J

.field protected mExtentionString:Ljava/lang/String;

.field private mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

.field protected mParameter:Lcom/sec/android/app/myfiles/utils/SearchParameter;

.field protected mSearchActionBar:Landroid/view/View;

.field protected mSearchFragmentManager:Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;

.field protected mSearchTypeButton:Lcom/sec/android/app/myfiles/view/MyFilesSpinner;

.field protected mSearchView:Landroid/widget/SearchView;

.field protected mSearchable:Lcom/sec/android/app/myfiles/fragment/ISearchable;

.field private mSelectionType:I

.field private mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

.field private mShowFolderFileType:I

.field private mStartFolder:Ljava/lang/String;

.field protected mStorageType:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mrefreshHandler:Landroid/os/Handler;

.field private preText:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 742
    const v0, 0x7f0f00f9

    sput v0, Lcom/sec/android/app/myfiles/SearchActivity;->FRAGMENT_LAYOUT_ID:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/AbsMainActivity;-><init>()V

    .line 85
    iput-object p0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mContext:Landroid/content/Context;

    .line 102
    const/16 v0, 0x32

    iput v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->MAX_SEARCH_TEXT_LIMIT:I

    .line 104
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->preText:Ljava/lang/String;

    .line 164
    new-instance v0, Lcom/sec/android/app/myfiles/SearchActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/SearchActivity$1;-><init>(Lcom/sec/android/app/myfiles/SearchActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mConnectionChangeReceiver:Landroid/content/BroadcastReceiver;

    .line 682
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/myfiles/SearchActivity$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/SearchActivity$4;-><init>(Lcom/sec/android/app/myfiles/SearchActivity;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mrefreshHandler:Landroid/os/Handler;

    .line 746
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/SearchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SearchActivity;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->refreshFragmentsLocation()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/SearchActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SearchActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mrefreshHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/SearchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SearchActivity;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->handleMenuItemAll()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/SearchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SearchActivity;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->handleMenuItemLocation()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/myfiles/SearchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SearchActivity;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->handleMenuItemExtension()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/myfiles/SearchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SearchActivity;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->handleMenuItemDate()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/myfiles/SearchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SearchActivity;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->handleMenuItemFileType()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/myfiles/SearchActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/SearchActivity;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->handleMenuItemAdvanced()V

    return-void
.end method

.method private cleanSearchableIfNeeded(II)V
    .locals 1
    .param p1, "currentSearchType"    # I
    .param p2, "searchOption"    # I

    .prologue
    .line 607
    if-eq p1, p2, :cond_0

    .line 608
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchable:Lcom/sec/android/app/myfiles/fragment/ISearchable;

    if-eqz v0, :cond_0

    .line 609
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchable:Lcom/sec/android/app/myfiles/fragment/ISearchable;

    invoke-interface {v0}, Lcom/sec/android/app/myfiles/fragment/ISearchable;->clean()V

    .line 612
    :cond_0
    return-void
.end method

.method private getFileType(Landroid/content/Intent;)Ljava/util/EnumSet;
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/sec/android/app/myfiles/utils/FileType;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 505
    const v2, 0x7f0b0001

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/SearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 506
    .local v0, "all":Z
    if-eqz v0, :cond_0

    .line 507
    sget-object v2, Lcom/sec/android/app/myfiles/utils/FileType;->IMAGE:Lcom/sec/android/app/myfiles/utils/FileType;

    sget-object v3, Lcom/sec/android/app/myfiles/utils/FileType;->DOCUMENT:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-static {v2, v3}, Ljava/util/EnumSet;->range(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v2

    .line 523
    :goto_0
    return-object v2

    .line 509
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 510
    .local v1, "fileTypes":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/utils/FileType;>;"
    const v2, 0x7f0b0003

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/SearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 511
    sget-object v2, Lcom/sec/android/app/myfiles/utils/FileType;->IMAGE:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 513
    :cond_1
    const v2, 0x7f0b0147

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/SearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 514
    sget-object v2, Lcom/sec/android/app/myfiles/utils/FileType;->AUDIO:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 516
    :cond_2
    const v2, 0x7f0b0004

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/SearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 517
    sget-object v2, Lcom/sec/android/app/myfiles/utils/FileType;->VIDEO:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 519
    :cond_3
    const v2, 0x7f0b0006

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/SearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 520
    sget-object v2, Lcom/sec/android/app/myfiles/utils/FileType;->DOCUMENT:Lcom/sec/android/app/myfiles/utils/FileType;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 522
    :cond_4
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x0

    goto :goto_0

    .line 523
    :cond_5
    invoke-static {v1}, Ljava/util/EnumSet;->copyOf(Ljava/util/Collection;)Ljava/util/EnumSet;

    move-result-object v2

    goto :goto_0
.end method

.method private handleMenuItemAdvanced()V
    .locals 5

    .prologue
    .line 596
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 597
    .local v1, "fragmentManager":Landroid/app/FragmentManager;
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    .line 598
    .local v2, "fragmentTransaction":Landroid/app/FragmentTransaction;
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;-><init>()V

    .line 599
    .local v0, "advancedFragment":Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;
    sget v3, Lcom/sec/android/app/myfiles/SearchActivity;->FRAGMENT_LAYOUT_ID:I

    const-string v4, "SearchAdvanceFragment"

    invoke-virtual {v2, v3, v0, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 600
    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 604
    return-void
.end method

.method private handleMenuItemAll()V
    .locals 2

    .prologue
    .line 530
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentSearchType:I

    .line 532
    const/16 v0, 0x12

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/myfiles/SearchActivity;->setFragment(ILandroid/os/Bundle;)V

    .line 533
    return-void
.end method

.method private handleMenuItemDate()V
    .locals 4

    .prologue
    .line 564
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_0

    .line 565
    invoke-static {}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->getInstance()Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;

    move-result-object v1

    .line 566
    .local v1, "dateFragment":Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 567
    .local v0, "dateArgs":Landroid/os/Bundle;
    const-string v2, "title"

    const v3, 0x7f0b008e

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 568
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 569
    invoke-virtual {v1, v0}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->setArguments(Landroid/os/Bundle;)V

    .line 570
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 572
    .end local v0    # "dateArgs":Landroid/os/Bundle;
    .end local v1    # "dateFragment":Lcom/sec/android/app/myfiles/fragment/SearchSetDateFragment;
    :cond_0
    return-void
.end method

.method private handleMenuItemExtension()V
    .locals 3

    .prologue
    .line 555
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchView:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    .line 556
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchView:Landroid/widget/SearchView;

    const-string v1, ""

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 558
    :cond_0
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentSearchType:I

    .line 559
    const/16 v0, 0x12

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/myfiles/SearchActivity;->setFragment(ILandroid/os/Bundle;)V

    .line 560
    return-void
.end method

.method private handleMenuItemFileType()V
    .locals 4

    .prologue
    .line 576
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_0

    .line 577
    invoke-static {}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->getInstance()Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    move-result-object v1

    .line 578
    .local v1, "fileTypeFragment":Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 579
    .local v0, "fileArgs":Landroid/os/Bundle;
    const-string v2, "title"

    const v3, 0x7f0b008d

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 580
    invoke-virtual {v1, v0}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->setArguments(Landroid/os/Bundle;)V

    .line 581
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 582
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 584
    .end local v0    # "fileArgs":Landroid/os/Bundle;
    .end local v1    # "fileTypeFragment":Lcom/sec/android/app/myfiles/fragment/SearchsSetFileTypeFragment;
    :cond_0
    return-void
.end method

.method private handleMenuItemLocation()V
    .locals 4

    .prologue
    .line 537
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_0

    .line 545
    invoke-static {}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getInstance()Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    move-result-object v1

    .line 546
    .local v1, "fragment":Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 547
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "title"

    const v3, 0x7f0b008c

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 548
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 549
    invoke-virtual {v1, v0}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->setArguments(Landroid/os/Bundle;)V

    .line 550
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 552
    .end local v0    # "args":Landroid/os/Bundle;
    .end local v1    # "fragment":Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;
    :cond_0
    return-void
.end method

.method private initSearchMenu()V
    .locals 4

    .prologue
    .line 255
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 257
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->handleMenuItemAll()V

    .line 316
    :goto_0
    return-void

    .line 261
    :cond_0
    const v2, 0x7f0f00f7

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/view/MyFilesSpinner;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchTypeButton:Lcom/sec/android/app/myfiles/view/MyFilesSpinner;

    .line 263
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070006

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 265
    .local v1, "search_arry":[Ljava/lang/String;
    new-instance v0, Lcom/sec/android/app/myfiles/SearchActivity$SpinnerArrayAdapter;

    const v2, 0x7f040043

    invoke-direct {v0, p0, v2, v1}, Lcom/sec/android/app/myfiles/SearchActivity$SpinnerArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/CharSequence;)V

    .line 267
    .local v0, "adapter":Lcom/sec/android/app/myfiles/SearchActivity$SpinnerArrayAdapter;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchTypeButton:Lcom/sec/android/app/myfiles/view/MyFilesSpinner;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/myfiles/view/MyFilesSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 269
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchTypeButton:Lcom/sec/android/app/myfiles/view/MyFilesSpinner;

    # getter for: Lcom/sec/android/app/myfiles/SearchActivity$SpinnerArrayAdapter;->mCurrentPosition:I
    invoke-static {v0}, Lcom/sec/android/app/myfiles/SearchActivity$SpinnerArrayAdapter;->access$200(Lcom/sec/android/app/myfiles/SearchActivity$SpinnerArrayAdapter;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/view/MyFilesSpinner;->setSelection(I)V

    .line 271
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchTypeButton:Lcom/sec/android/app/myfiles/view/MyFilesSpinner;

    new-instance v3, Lcom/sec/android/app/myfiles/SearchActivity$3;

    invoke-direct {v3, p0, v0}, Lcom/sec/android/app/myfiles/SearchActivity$3;-><init>(Lcom/sec/android/app/myfiles/SearchActivity;Lcom/sec/android/app/myfiles/SearchActivity$SpinnerArrayAdapter;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/view/MyFilesSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    goto :goto_0
.end method

.method private refreshFragmentsLocation()V
    .locals 4

    .prologue
    .line 188
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-class v3, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    .line 189
    .local v1, "searchSetLocationFragment":Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->isResumed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 190
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->refreshAdapter()V

    .line 192
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "SearchAdvanceFragment"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;

    .line 193
    .local v0, "advancedFragment":Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->isResumed()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 194
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->refreshLocation()V

    .line 195
    :cond_1
    return-void
.end method

.method private registerBroadcastReceiver()V
    .locals 2

    .prologue
    .line 208
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 210
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 220
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 222
    new-instance v1, Lcom/sec/android/app/myfiles/SearchActivity$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/SearchActivity$2;-><init>(Lcom/sec/android/app/myfiles/SearchActivity;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

    .line 250
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/myfiles/SearchActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 251
    return-void
.end method

.method private setFragment(ILandroid/os/Bundle;)V
    .locals 8
    .param p1, "id"    # I
    .param p2, "arg"    # Landroid/os/Bundle;

    .prologue
    .line 484
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchFragmentManager:Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;

    iget v3, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSelectionType:I

    iget v4, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mShowFolderFileType:I

    iget-object v5, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mStartFolder:Ljava/lang/String;

    move v2, p1

    move-object v6, p2

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;->setFragment(IIILjava/lang/String;Landroid/os/Bundle;)Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    move-result-object v7

    .line 487
    .local v7, "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    iput-object v7, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .line 491
    if-eqz v7, :cond_1

    .line 492
    :try_start_0
    move-object v0, v7

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/ISearchable;

    move-object v1, v0

    iput-object v1, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchable:Lcom/sec/android/app/myfiles/fragment/ISearchable;

    .line 494
    const/16 v1, 0x13

    if-ne p1, v1, :cond_0

    .line 495
    check-cast v7, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;

    .end local v7    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    const-string v1, "provider_id"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/sec/android/app/myfiles/fragment/NearByDevicesBrowserFragment;->changeProvider(Ljava/lang/String;)V

    .line 502
    :cond_0
    :goto_0
    return-void

    .line 499
    .restart local v7    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchable:Lcom/sec/android/app/myfiles/fragment/ISearchable;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 501
    .end local v7    # "fragment":Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method protected getMainLayoutResId()I
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 323
    const v0, 0x7f040040

    .line 327
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f04003f

    goto :goto_0
.end method

.method public initialActionBar()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 337
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 338
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 339
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 340
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f0b0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 342
    return-void
.end method

.method protected loadComponentFromView()V
    .locals 0

    .prologue
    .line 333
    return-void
.end method

.method public moveToFolderInResult(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 617
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    if-eqz v0, :cond_0

    .line 619
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->moveToFolder(Ljava/lang/String;)V

    .line 621
    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 8
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 377
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 378
    const/4 v4, -0x1

    if-ne p2, v4, :cond_8

    if-eqz p3, :cond_8

    .line 379
    iget-object v4, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v4}, Landroid/widget/SearchView;->requestFocus()Z

    .line 380
    const/4 v4, 0x2

    const-string v5, "SearchActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onActivityResult() resultCode : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " requestCode : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 381
    packed-switch p1, :pswitch_data_0

    .line 481
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 383
    :pswitch_1
    const/4 v4, 0x3

    iput v4, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentSearchType:I

    .line 384
    invoke-direct {p0, p3}, Lcom/sec/android/app/myfiles/SearchActivity;->getFileType(Landroid/content/Intent;)Ljava/util/EnumSet;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentFileTypes:Ljava/util/EnumSet;

    .line 386
    iget-object v4, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v4}, Landroid/widget/SearchView;->requestFocus()Z

    .line 387
    const/16 v4, 0x12

    const/4 v5, 0x0

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/myfiles/SearchActivity;->setFragment(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 390
    :pswitch_2
    const/4 v4, 0x2

    iput v4, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentSearchType:I

    .line 391
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "search_set_location_is_near_by_device_intent_key"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 392
    const-string v4, "provider_id"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 393
    .local v2, "deviceId":Ljava/lang/String;
    const-string v4, "provider_name"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 394
    .local v3, "deviceName":Ljava/lang/String;
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    .line 395
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 396
    .local v0, "arg":Landroid/os/Bundle;
    const-string v4, "provider_id"

    invoke-virtual {v0, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    const/16 v4, 0x13

    invoke-direct {p0, v4, v0}, Lcom/sec/android/app/myfiles/SearchActivity;->setFragment(ILandroid/os/Bundle;)V

    .line 398
    iget-object v4, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchable:Lcom/sec/android/app/myfiles/fragment/ISearchable;

    if-eqz v4, :cond_0

    .line 399
    iget-object v4, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchable:Lcom/sec/android/app/myfiles/fragment/ISearchable;

    invoke-interface {v4}, Lcom/sec/android/app/myfiles/fragment/ISearchable;->clean()V

    goto :goto_0

    .line 406
    .end local v0    # "arg":Landroid/os/Bundle;
    .end local v2    # "deviceId":Ljava/lang/String;
    .end local v3    # "deviceName":Ljava/lang/String;
    :cond_1
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "search_set_location_intent_key"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mStorageType:Ljava/util/ArrayList;

    .line 409
    iget-object v4, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mStorageType:Ljava/util/ArrayList;

    const v5, 0x7f0b00fa

    invoke-virtual {p0, v5}, Lcom/sec/android/app/myfiles/SearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 410
    const/16 v4, 0x20

    const/4 v5, 0x0

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/myfiles/SearchActivity;->setFragment(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 414
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mStorageType:Ljava/util/ArrayList;

    const v5, 0x7f0b0008

    invoke-virtual {p0, v5}, Lcom/sec/android/app/myfiles/SearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 415
    const/16 v4, 0x14

    const/4 v5, 0x0

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/myfiles/SearchActivity;->setFragment(ILandroid/os/Bundle;)V

    goto/16 :goto_0

    .line 419
    :cond_3
    const/16 v4, 0x12

    const/4 v5, 0x0

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/myfiles/SearchActivity;->setFragment(ILandroid/os/Bundle;)V

    goto/16 :goto_0

    .line 425
    :pswitch_3
    const/4 v4, 0x6

    iput v4, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentSearchType:I

    .line 426
    iget-object v4, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "search_advance_name_intent_key"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 427
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "search_from_date_intent_key"

    const-wide/16 v6, 0x0

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentFromDate:J

    .line 428
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "search_to_date_intent_key"

    const-wide/16 v6, 0x0

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentToDate:J

    .line 429
    invoke-direct {p0, p3}, Lcom/sec/android/app/myfiles/SearchActivity;->getFileType(Landroid/content/Intent;)Ljava/util/EnumSet;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentFileTypes:Ljava/util/EnumSet;

    .line 430
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "search_advance_extention_intent_key"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mExtentionString:Ljava/lang/String;

    .line 431
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "search_set_location_is_near_by_device_intent_key"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 432
    const-string v4, "provider_id"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 433
    .restart local v2    # "deviceId":Ljava/lang/String;
    const-string v4, "provider_name"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 434
    .restart local v3    # "deviceName":Ljava/lang/String;
    if-eqz v2, :cond_4

    if-eqz v3, :cond_4

    .line 435
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 436
    .restart local v0    # "arg":Landroid/os/Bundle;
    const-string v4, "provider_id"

    invoke-virtual {v0, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    const/16 v4, 0x13

    invoke-direct {p0, v4, v0}, Lcom/sec/android/app/myfiles/SearchActivity;->setFragment(ILandroid/os/Bundle;)V

    .line 464
    .end local v0    # "arg":Landroid/os/Bundle;
    .end local v2    # "deviceId":Ljava/lang/String;
    .end local v3    # "deviceName":Ljava/lang/String;
    :cond_4
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v4}, Landroid/widget/SearchView;->requestFocus()Z

    goto/16 :goto_0

    .line 440
    :cond_5
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "search_set_location_intent_key"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mStorageType:Ljava/util/ArrayList;

    .line 442
    iget-object v4, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mStorageType:Ljava/util/ArrayList;

    const v5, 0x7f0b00fa

    invoke-virtual {p0, v5}, Lcom/sec/android/app/myfiles/SearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 443
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 444
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v4, "search_option_type"

    const/4 v5, 0x6

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 445
    const-string v4, "search_keyword_intent_key"

    iget-object v5, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v5}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    const/16 v4, 0x20

    invoke-direct {p0, v4, v1}, Lcom/sec/android/app/myfiles/SearchActivity;->setFragment(ILandroid/os/Bundle;)V

    goto :goto_1

    .line 450
    .end local v1    # "bundle":Landroid/os/Bundle;
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mStorageType:Ljava/util/ArrayList;

    const v5, 0x7f0b0008

    invoke-virtual {p0, v5}, Lcom/sec/android/app/myfiles/SearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 451
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 452
    .restart local v1    # "bundle":Landroid/os/Bundle;
    const-string v4, "search_option_type"

    const/4 v5, 0x6

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 453
    const-string v4, "search_keyword_intent_key"

    iget-object v5, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v5}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    const/16 v4, 0x14

    invoke-direct {p0, v4, v1}, Lcom/sec/android/app/myfiles/SearchActivity;->setFragment(ILandroid/os/Bundle;)V

    goto :goto_1

    .line 458
    .end local v1    # "bundle":Landroid/os/Bundle;
    :cond_7
    const/16 v4, 0x12

    const/4 v5, 0x0

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/myfiles/SearchActivity;->setFragment(ILandroid/os/Bundle;)V

    .line 459
    iget-object v4, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mrefreshHandler:Landroid/os/Handler;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 468
    :pswitch_4
    const/4 v4, 0x4

    iput v4, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentSearchType:I

    .line 469
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "search_from_date_intent_key"

    const-wide/16 v6, 0x0

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentFromDate:J

    .line 470
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "search_to_date_intent_key"

    const-wide/16 v6, 0x0

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentToDate:J

    .line 472
    const/16 v4, 0x12

    const/4 v5, 0x0

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/myfiles/SearchActivity;->setFragment(ILandroid/os/Bundle;)V

    goto/16 :goto_0

    .line 475
    :cond_8
    if-nez p2, :cond_0

    .line 477
    iget-object v4, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v4}, Landroid/widget/SearchView;->requestFocus()Z

    .line 478
    iget-object v4, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchTypeButton:Lcom/sec/android/app/myfiles/view/MyFilesSpinner;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/android/app/myfiles/view/MyFilesSpinner;->setSelection(I)V

    .line 479
    const/16 v4, 0x12

    const/4 v5, 0x0

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/myfiles/SearchActivity;->setFragment(ILandroid/os/Bundle;)V

    goto/16 :goto_0

    .line 381
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 352
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-nez v0, :cond_1

    .line 363
    :cond_0
    :goto_0
    return-void

    .line 355
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "SearchAdvanceFragment"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 356
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchTypeButton:Lcom/sec/android/app/myfiles/view/MyFilesSpinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/MyFilesSpinner;->setSelection(I)V

    .line 357
    const/16 v0, 0x12

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/myfiles/SearchActivity;->setFragment(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 358
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v0, :cond_0

    .line 360
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onBackPressed()Z

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const-wide/16 v10, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 112
    new-instance v3, Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Lcom/sec/android/app/myfiles/SearchActivity;->FRAGMENT_LAYOUT_ID:I

    invoke-direct {v3, v4, v5, v6}, Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;-><init>(Landroid/app/FragmentManager;Landroid/content/Context;I)V

    iput-object v3, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchFragmentManager:Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;

    .line 114
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onCreate(Landroid/os/Bundle;)V

    .line 116
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->initSearchMenu()V

    .line 118
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    .line 119
    const v3, 0x7f0f00f8

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/SearchView;

    iput-object v3, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchView:Landroid/widget/SearchView;

    .line 120
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchView:Landroid/widget/SearchView;

    iget-object v3, v3, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    const/4 v4, 0x0

    invoke-static {p0, v4}, Lcom/sec/android/app/myfiles/utils/Utils;->getEditTextFilter(Landroid/content/Context;I)[Landroid/text/InputFilter;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/SearchView$SearchAutoComplete;->setFilters([Landroid/text/InputFilter;)V

    .line 121
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v3}, Landroid/widget/SearchView;->onActionViewExpanded()V

    .line 122
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v3, p0}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    .line 125
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v3}, Landroid/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const-string v4, "android:id/search_src_text"

    invoke-virtual {v3, v4, v8, v8}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 126
    .local v2, "editTextId":I
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v3, v2}, Landroid/widget/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 127
    .local v1, "editBoxView":Landroid/view/View;
    if-eqz v1, :cond_0

    instance-of v3, v1, Landroid/widget/EditText;

    if-eqz v3, :cond_0

    .line 129
    check-cast v1, Landroid/widget/EditText;

    .end local v1    # "editBoxView":Landroid/view/View;
    invoke-virtual {v1, v7}, Landroid/widget/EditText;->getInputExtras(Z)Landroid/os/Bundle;

    move-result-object v0

    .line 130
    .local v0, "b":Landroid/os/Bundle;
    const-string v3, "maxLength"

    const/16 v4, 0x32

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 134
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCalendar:Ljava/util/Calendar;

    .line 135
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCalendar:Ljava/util/Calendar;

    const/4 v4, 0x6

    const/4 v5, -0x7

    invoke-virtual {v3, v4, v5}, Ljava/util/Calendar;->add(II)V

    .line 137
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getSearchFromDate()J

    move-result-wide v4

    cmp-long v3, v4, v10

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getSearchToDate()J

    move-result-wide v4

    cmp-long v3, v4, v10

    if-nez v3, :cond_1

    .line 139
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchToDate(J)V

    .line 140
    iget-object v3, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFromDate(J)V

    .line 145
    :cond_1
    iput v7, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentSearchType:I

    .line 147
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->registerBroadcastReceiver()V

    .line 154
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 201
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f0e0003

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 202
    const/4 v1, 0x1

    return v1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 720
    invoke-super {p0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onDestroy()V

    .line 722
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->unregisterBroadcastReceiver()V

    .line 725
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->commit()Z

    .line 726
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 367
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 372
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 369
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->onBackPressed()V

    goto :goto_0

    .line 367
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 177
    invoke-super {p0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onPause()V

    .line 178
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mConnectionChangeReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 180
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mConnectionChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/SearchActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 185
    :cond_0
    :goto_0
    return-void

    .line 181
    :catch_0
    move-exception v0

    .line 182
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 5
    .param p1, "queryText"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 626
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x32

    if-gt v0, v1, :cond_0

    .line 628
    iput-object p1, p0, Lcom/sec/android/app/myfiles/SearchActivity;->preText:Ljava/lang/String;

    .line 636
    :goto_0
    new-instance v0, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mParameter:Lcom/sec/android/app/myfiles/utils/SearchParameter;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;-><init>(Lcom/sec/android/app/myfiles/utils/SearchParameter;)V

    iget v1, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentSearchType:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->searchType(I)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mStorageType:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->storageType(Ljava/util/ArrayList;)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentFileTypes:Ljava/util/EnumSet;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->fileTypes(Ljava/util/EnumSet;)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    move-result-object v0

    iget-wide v2, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentFromDate:J

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->dateFrom(J)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    move-result-object v0

    iget-wide v2, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentToDate:J

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->dateTo(J)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mExtentionString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->extentionString(Ljava/lang/String;)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->keyword(Ljava/lang/String;)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->build()Lcom/sec/android/app/myfiles/utils/SearchParameter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mParameter:Lcom/sec/android/app/myfiles/utils/SearchParameter;

    .line 644
    return v4

    .line 632
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/SearchActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0077

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 633
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchView:Landroid/widget/SearchView;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/SearchActivity;->preText:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    goto :goto_0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 5
    .param p1, "queryText"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 650
    new-instance v0, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mParameter:Lcom/sec/android/app/myfiles/utils/SearchParameter;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;-><init>(Lcom/sec/android/app/myfiles/utils/SearchParameter;)V

    iget v1, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentSearchType:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->searchType(I)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mStorageType:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->storageType(Ljava/util/ArrayList;)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentFileTypes:Ljava/util/EnumSet;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->fileTypes(Ljava/util/EnumSet;)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    move-result-object v0

    iget-wide v2, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentFromDate:J

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->dateFrom(J)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    move-result-object v0

    iget-wide v2, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mCurrentToDate:J

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->dateTo(J)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mExtentionString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->extentionString(Ljava/lang/String;)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->keyword(Ljava/lang/String;)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->build()Lcom/sec/android/app/myfiles/utils/SearchParameter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mParameter:Lcom/sec/android/app/myfiles/utils/SearchParameter;

    .line 658
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchable:Lcom/sec/android/app/myfiles/fragment/ISearchable;

    if-eqz v0, :cond_0

    .line 659
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/SearchActivity;->isSearchPeforming:Z

    .line 660
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchable:Lcom/sec/android/app/myfiles/fragment/ISearchable;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mParameter:Lcom/sec/android/app/myfiles/utils/SearchParameter;

    invoke-interface {v0, v1}, Lcom/sec/android/app/myfiles/fragment/ISearchable;->search(Lcom/sec/android/app/myfiles/utils/SearchParameter;)V

    .line 661
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->clearFocus()V

    .line 663
    :cond_0
    return v4
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 804
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 806
    const-string v0, "hasQueryText"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 807
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchView:Landroid/widget/SearchView;

    const-string v1, "hasQueryText"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 809
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 158
    invoke-super {p0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onResume()V

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mConnectionChangeReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/myfiles/SearchActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 162
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 794
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 796
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchView:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    .line 797
    const-string v0, "hasQueryText"

    iget-object v1, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 799
    :cond_0
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 4
    .param p1, "level"    # I

    .prologue
    .line 814
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->onTrimMemory(I)V

    .line 816
    const/4 v0, 0x0

    const-string v1, "SearchActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SearchActivity : onTrimMemory : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 817
    sparse-switch p1, :sswitch_data_0

    .line 833
    :sswitch_0
    return-void

    .line 817
    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0xa -> :sswitch_0
        0xf -> :sswitch_0
        0x14 -> :sswitch_0
        0x28 -> :sswitch_0
        0x3c -> :sswitch_0
        0x50 -> :sswitch_0
    .end sparse-switch
.end method

.method public setDefaultView()V
    .locals 2

    .prologue
    .line 346
    const/16 v0, 0x12

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/myfiles/SearchActivity;->setFragment(ILandroid/os/Bundle;)V

    .line 347
    return-void
.end method

.method public unregisterBroadcastReceiver()V
    .locals 1

    .prologue
    .line 707
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 709
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/SearchActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 711
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

    .line 714
    :cond_0
    return-void
.end method

.method public updateEmptyView()V
    .locals 2

    .prologue
    const v1, 0x7f0f00b0

    .line 668
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->isSearchPeforming:Z

    if-eqz v0, :cond_1

    .line 673
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 674
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0b0095

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/SearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 676
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->clearFocus()V

    .line 678
    :cond_1
    return-void
.end method
