.class public Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;
.super Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
.source "RemoteShareAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter$RemoteShareFolderInfo;
    }
.end annotation


# static fields
.field private static IMAGECACHE_SIZE:I


# instance fields
.field private mContext:Landroid/content/Context;

.field private mFolderExpiredDate:Ljava/lang/String;

.field private mFolderInfoArray:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter$RemoteShareFolderInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mRemoteShareNavigation:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

.field private mSelectedItemsSize:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83
    const/high16 v0, 0xa00000

    sput v0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->IMAGECACHE_SIZE:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "c"    # Landroid/database/Cursor;
    .param p4, "flags"    # I
    .param p5, "navigation"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .prologue
    .line 75
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 53
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mFolderInfoArray:Landroid/util/SparseArray;

    .line 76
    if-eqz p5, :cond_0

    instance-of v0, p5, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    if-eqz v0, :cond_0

    .line 77
    check-cast p5, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    .end local p5    # "navigation":Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
    iput-object p5, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mRemoteShareNavigation:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    .line 79
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mContext:Landroid/content/Context;

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->createImageCache(Landroid/content/Context;)V

    .line 81
    return-void
.end method

.method private createImageCache(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 86
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/RequestManager;->init(Landroid/content/Context;)V

    .line 87
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/ImageCacheManager;->getInstance()Lcom/sec/android/app/myfiles/utils/ImageCacheManager;

    move-result-object v0

    sget v1, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->IMAGECACHE_SIZE:I

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/myfiles/utils/ImageCacheManager;->init(Landroid/content/Context;I)V

    .line 88
    return-void
.end method

.method private fileExists(Ljava/lang/String;)Z
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 114
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/android/app/myfiles/utils/Constant;->RSHARE_DOWNLOAD_DIR_URI:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 115
    .local v1, "fileUri":Landroid/net/Uri;
    new-instance v0, Ljava/io/File;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 116
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 117
    const/4 v2, 0x0

    .line 119
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private isAvailableForDownload(Landroid/database/Cursor;)Z
    .locals 8
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 124
    const-string v7, "filename"

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 125
    .local v3, "name":Ljava/lang/String;
    const-string v7, "status"

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 126
    .local v4, "status":I
    const-string v7, "local_file"

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 127
    .local v1, "filePath":Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_1

    .line 128
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 129
    .local v2, "fileUri":Landroid/net/Uri;
    if-eqz v2, :cond_1

    .line 130
    new-instance v0, Ljava/io/File;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 131
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 142
    .end local v0    # "file":Ljava/io/File;
    .end local v2    # "fileUri":Landroid/net/Uri;
    :cond_0
    :goto_0
    return v5

    .line 136
    :cond_1
    const/4 v7, 0x3

    if-ne v4, v7, :cond_2

    .line 137
    invoke-direct {p0, v3}, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->fileExists(Ljava/lang/String;)Z

    move-result v5

    goto :goto_0

    .line 139
    :cond_2
    const/4 v7, 0x2

    if-eq v4, v7, :cond_0

    if-eq v4, v6, :cond_0

    move v5, v6

    .line 142
    goto :goto_0
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 37
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 221
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v36

    check-cast v36, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;

    .line 222
    .local v36, "vh":Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    if-eqz v6, :cond_0

    .line 223
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 224
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f080017

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 226
    :cond_0
    const/4 v11, 0x0

    .line 227
    .local v11, "currentFolder":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mRemoteShareNavigation:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    if-eqz v6, :cond_1

    .line 228
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mRemoteShareNavigation:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v11

    .line 231
    :cond_1
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDetailContainer:Landroid/view/ViewGroup;

    if-nez v6, :cond_2

    .line 232
    const v6, 0x7f0f0067

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    move-object/from16 v0, v36

    iput-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDetailContainer:Landroid/view/ViewGroup;

    .line 233
    const v6, 0x7f0f005a

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    move-object/from16 v0, v36

    iput-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    .line 234
    const v6, 0x7f0f0069

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    move-object/from16 v0, v36

    iput-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    .line 236
    :cond_2
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mViewMode:I

    if-nez v6, :cond_f

    .line 237
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDetailContainer:Landroid/view/ViewGroup;

    if-eqz v6, :cond_3

    .line 238
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDetailContainer:Landroid/view/ViewGroup;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 245
    :cond_3
    :goto_0
    const v6, 0x7f0f0036

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    move-object/from16 v0, v36

    iput-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDocExtnOverlay:Landroid/widget/ImageView;

    .line 246
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDocExtnOverlay:Landroid/widget/ImageView;

    if-eqz v6, :cond_4

    .line 247
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDocExtnOverlay:Landroid/widget/ImageView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 249
    :cond_4
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    if-eqz v6, :cond_5

    .line 250
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 252
    :cond_5
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v6, :cond_6

    .line 253
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 254
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 256
    :cond_6
    const v6, 0x7f0f003b

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    .line 257
    .local v16, "downloadedFlag":Landroid/view/View;
    if-eqz v16, :cond_7

    .line 258
    const/16 v6, 0x8

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 260
    :cond_7
    if-eqz v11, :cond_19

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_19

    .line 262
    const-string v6, "filename"

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 263
    .local v17, "fileName":Ljava/lang/String;
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    if-eqz v6, :cond_8

    .line 264
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 268
    :cond_8
    const v6, 0x7f0f003a

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v32

    check-cast v32, Lcom/android/volley/toolbox/NetworkImageView;

    .line 269
    .local v32, "networkThumb":Lcom/android/volley/toolbox/NetworkImageView;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/ImageCacheManager;->getInstance()Lcom/sec/android/app/myfiles/utils/ImageCacheManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/utils/ImageCacheManager;->getImageLoader()Lcom/android/volley/toolbox/ImageLoader;

    move-result-object v24

    .line 270
    .local v24, "imLoader":Lcom/android/volley/toolbox/ImageLoader;
    const-string v6, "thumbnail_uri"

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 271
    .local v25, "imageUrl":Ljava/lang/String;
    const-string v6, "local_file"

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 272
    .local v18, "filePath":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, v18

    invoke-static {v0, v6}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;Landroid/content/Context;)I

    move-result v21

    .line 273
    .local v21, "fileType":I
    const-string v6, "status"

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v33

    .line 274
    .local v33, "status":I
    const/4 v15, 0x0

    .line 275
    .local v15, "defaultIconResId":I
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mViewMode:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_11

    .line 276
    if-eqz v17, :cond_10

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-static {v0, v6, v1}, Lcom/sec/android/app/myfiles/MediaFile;->getLargeIcon(Ljava/lang/String;Landroid/content/Context;I)I

    move-result v15

    .line 280
    :goto_1
    if-eqz v25, :cond_13

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_13

    if-eqz v24, :cond_13

    if-eqz v32, :cond_13

    .line 281
    move-object/from16 v0, v32

    move-object/from16 v1, v25

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lcom/android/volley/toolbox/NetworkImageView;->setImageUrl(Ljava/lang/String;Lcom/android/volley/toolbox/ImageLoader;)V

    .line 282
    const/4 v6, 0x0

    move-object/from16 v0, v32

    invoke-virtual {v0, v6}, Lcom/android/volley/toolbox/NetworkImageView;->setVisibility(I)V

    .line 283
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    if-eqz v6, :cond_9

    .line 284
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 285
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v6, v15}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 305
    :cond_9
    :goto_2
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->isAvailableForDownload(Landroid/database/Cursor;)Z

    move-result v28

    .line 308
    .local v28, "isAvailableForDownload":Z
    if-nez v18, :cond_a

    if-eqz v16, :cond_a

    const/4 v6, 0x3

    move/from16 v0, v33

    if-ne v0, v6, :cond_a

    if-nez v28, :cond_a

    .line 309
    const/4 v6, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 312
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mIsSelectMode:Z

    if-eqz v6, :cond_17

    .line 313
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mSelectionType:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_b

    .line 314
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 315
    const/4 v6, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 316
    .local v22, "id":J
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 323
    .end local v22    # "id":J
    :cond_b
    :goto_3
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    if-eqz v6, :cond_c

    .line 324
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->getFolderExpiredDate()Ljava/lang/String;

    move-result-object v14

    .line 325
    .local v14, "dateExpiredString":Ljava/lang/String;
    if-eqz v14, :cond_18

    .line 326
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 327
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b0011

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 333
    .end local v14    # "dateExpiredString":Ljava/lang/String;
    :cond_c
    :goto_4
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    if-eqz v6, :cond_d

    .line 334
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 335
    const-string v6, "file_size"

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    .line 336
    .local v19, "fileSize":Ljava/lang/Long;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mContext:Landroid/content/Context;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v6, v8, v9}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v20

    .line 337
    .local v20, "fileSizeString":Ljava/lang/String;
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 339
    .end local v19    # "fileSize":Ljava/lang/Long;
    .end local v20    # "fileSizeString":Ljava/lang/String;
    :cond_d
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mHighlight:Landroid/widget/TextView;

    if-eqz v6, :cond_e

    .line 340
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mHighlight:Landroid/widget/TextView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 429
    .end local v15    # "defaultIconResId":I
    .end local v17    # "fileName":Ljava/lang/String;
    .end local v18    # "filePath":Ljava/lang/String;
    .end local v21    # "fileType":I
    .end local v24    # "imLoader":Lcom/android/volley/toolbox/ImageLoader;
    .end local v25    # "imageUrl":Ljava/lang/String;
    .end local v28    # "isAvailableForDownload":Z
    :cond_e
    :goto_5
    return-void

    .line 240
    .end local v16    # "downloadedFlag":Landroid/view/View;
    .end local v32    # "networkThumb":Lcom/android/volley/toolbox/NetworkImageView;
    .end local v33    # "status":I
    :cond_f
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mViewMode:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_3

    .line 241
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDetailContainer:Landroid/view/ViewGroup;

    if-eqz v6, :cond_3

    .line 242
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDetailContainer:Landroid/view/ViewGroup;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_0

    .line 276
    .restart local v15    # "defaultIconResId":I
    .restart local v16    # "downloadedFlag":Landroid/view/View;
    .restart local v17    # "fileName":Ljava/lang/String;
    .restart local v18    # "filePath":Ljava/lang/String;
    .restart local v21    # "fileType":I
    .restart local v24    # "imLoader":Lcom/android/volley/toolbox/ImageLoader;
    .restart local v25    # "imageUrl":Ljava/lang/String;
    .restart local v32    # "networkThumb":Lcom/android/volley/toolbox/NetworkImageView;
    .restart local v33    # "status":I
    :cond_10
    const v15, 0x7f0200ae

    goto/16 :goto_1

    .line 278
    :cond_11
    if-eqz v17, :cond_12

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-static {v0, v6, v1}, Lcom/sec/android/app/myfiles/MediaFile;->getSmallIcon(Ljava/lang/String;Landroid/content/Context;I)I

    move-result v15

    :goto_6
    goto/16 :goto_1

    :cond_12
    const v15, 0x7f0200ad

    goto :goto_6

    .line 288
    :cond_13
    if-eqz v18, :cond_16

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_16

    .line 289
    invoke-static/range {v18 .. v18}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v35

    .line 290
    .local v35, "uri":Landroid/net/Uri;
    invoke-virtual/range {v35 .. v35}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v18

    .line 291
    move-object/from16 v0, p0

    move-object/from16 v1, v36

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->loadThumbnail(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;Ljava/lang/String;)V

    .line 297
    .end local v35    # "uri":Landroid/net/Uri;
    :cond_14
    :goto_7
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    if-eqz v6, :cond_15

    .line 298
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 300
    :cond_15
    if-eqz v32, :cond_9

    .line 301
    const/16 v6, 0x8

    move-object/from16 v0, v32

    invoke-virtual {v0, v6}, Lcom/android/volley/toolbox/NetworkImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 293
    :cond_16
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    if-eqz v6, :cond_14

    .line 294
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v6, v15}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_7

    .line 319
    .restart local v28    # "isAvailableForDownload":Z
    :cond_17
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 320
    const/4 v6, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 321
    .restart local v22    # "id":J
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto/16 :goto_3

    .line 330
    .end local v22    # "id":J
    .restart local v14    # "dateExpiredString":Ljava/lang/String;
    :cond_18
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 343
    .end local v14    # "dateExpiredString":Ljava/lang/String;
    .end local v15    # "defaultIconResId":I
    .end local v17    # "fileName":Ljava/lang/String;
    .end local v18    # "filePath":Ljava/lang/String;
    .end local v21    # "fileType":I
    .end local v24    # "imLoader":Lcom/android/volley/toolbox/ImageLoader;
    .end local v25    # "imageUrl":Ljava/lang/String;
    .end local v28    # "isAvailableForDownload":Z
    .end local v32    # "networkThumb":Lcom/android/volley/toolbox/NetworkImageView;
    .end local v33    # "status":I
    :cond_19
    const-string v6, "recipient_ids"

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 344
    .local v10, "contactId":Ljava/lang/String;
    const-string v6, "date"

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 345
    .local v12, "date":J
    const-string v6, "expire_date"

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 346
    .local v4, "expireDate":J
    const-string v6, "status"

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v33

    .line 347
    .restart local v33    # "status":I
    const-string v6, "media_box"

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v30

    .line 349
    .local v30, "mediaBox":I
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    if-eqz v6, :cond_1a

    .line 350
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 351
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const v7, 0x7f0200af

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 353
    :cond_1a
    const v6, 0x7f0f003a

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v32

    check-cast v32, Lcom/android/volley/toolbox/NetworkImageView;

    .line 354
    .restart local v32    # "networkThumb":Lcom/android/volley/toolbox/NetworkImageView;
    if-eqz v32, :cond_1b

    .line 355
    const/16 v6, 0x8

    move-object/from16 v0, v32

    invoke-virtual {v0, v6}, Lcom/android/volley/toolbox/NetworkImageView;->setVisibility(I)V

    .line 357
    :cond_1b
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mContext:Landroid/content/Context;

    invoke-static {v6, v10}, Lcom/sec/android/app/myfiles/utils/Utils;->getContactNames(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    .line 358
    .local v31, "name":Ljava/lang/String;
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    if-eqz v6, :cond_1c

    .line 359
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    move-object/from16 v0, v31

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 362
    :cond_1c
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mViewMode:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_e

    .line 363
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDetailContainer:Landroid/view/ViewGroup;

    if-eqz v6, :cond_1d

    .line 364
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDetailContainer:Landroid/view/ViewGroup;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 366
    :cond_1d
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    if-eqz v6, :cond_1e

    .line 367
    const-string v6, "_id"

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    .line 368
    .local v22, "id":I
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mFolderInfoArray:Landroid/util/SparseArray;

    move/from16 v0, v22

    invoke-virtual {v6, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter$RemoteShareFolderInfo;

    .line 369
    .local v26, "info":Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter$RemoteShareFolderInfo;
    if-eqz v26, :cond_21

    .line 370
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b0070

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v29

    .line 371
    .local v29, "items":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter$RemoteShareFolderInfo;->getFilesCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v29

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " | "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mContext:Landroid/content/Context;

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter$RemoteShareFolderInfo;->getFilesSummarySize()J

    move-result-wide v8

    invoke-static {v7, v8, v9}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    .line 372
    .local v27, "infoString":Ljava/lang/String;
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 373
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 380
    .end local v22    # "id":I
    .end local v26    # "info":Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter$RemoteShareFolderInfo;
    .end local v27    # "infoString":Ljava/lang/String;
    .end local v29    # "items":Ljava/lang/String;
    :cond_1e
    :goto_8
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    if-eqz v6, :cond_20

    .line 381
    const/4 v6, 0x1

    move/from16 v0, v30

    if-eq v0, v6, :cond_1f

    const/4 v6, 0x3

    move/from16 v0, v30

    if-ne v0, v6, :cond_23

    const/16 v6, 0xc8

    move/from16 v0, v33

    if-ne v0, v6, :cond_23

    .line 385
    :cond_1f
    const-wide/16 v6, 0x0

    cmp-long v6, v12, v6

    if-nez v6, :cond_22

    const-string v6, "-"

    :goto_9
    check-cast v6, Ljava/lang/String;

    move-object v14, v6

    check-cast v14, Ljava/lang/String;

    .line 387
    .restart local v14    # "dateExpiredString":Ljava/lang/String;
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 388
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b0011

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 394
    .end local v14    # "dateExpiredString":Ljava/lang/String;
    :cond_20
    :goto_a
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mHighlight:Landroid/widget/TextView;

    if-eqz v6, :cond_e

    .line 395
    const/4 v6, 0x1

    move/from16 v0, v30

    if-eq v0, v6, :cond_24

    .line 396
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mHighlight:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 397
    const-string v34, ""

    .line 398
    .local v34, "statusString":Ljava/lang/String;
    sparse-switch v33, :sswitch_data_0

    .line 421
    :goto_b
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mHighlight:Landroid/widget/TextView;

    move-object/from16 v0, v34

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 375
    .end local v34    # "statusString":Ljava/lang/String;
    .restart local v22    # "id":I
    .restart local v26    # "info":Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter$RemoteShareFolderInfo;
    :cond_21
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_8

    .line 385
    .end local v22    # "id":I
    .end local v26    # "info":Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter$RemoteShareFolderInfo;
    :cond_22
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const/4 v8, 0x3

    const/4 v9, 0x3

    invoke-static/range {v4 .. v9}, Landroid/text/format/DateUtils;->formatSameDayTime(JJII)Ljava/lang/CharSequence;

    move-result-object v6

    goto :goto_9

    .line 390
    :cond_23
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_a

    .line 400
    .restart local v34    # "statusString":Ljava/lang/String;
    :sswitch_0
    const-string v34, "Preparing"

    .line 401
    goto :goto_b

    .line 403
    :sswitch_1
    const-string v34, "In progress"

    .line 404
    goto :goto_b

    .line 406
    :sswitch_2
    const-string v34, "Upload success"

    .line 407
    goto :goto_b

    .line 409
    :sswitch_3
    const-string v34, "Upload cancelled"

    .line 410
    goto :goto_b

    .line 412
    :sswitch_4
    const-string v34, "Paused"

    .line 413
    goto :goto_b

    .line 415
    :sswitch_5
    const-string v34, "Network error"

    .line 416
    goto :goto_b

    .line 418
    :sswitch_6
    const-string v34, "Unexpected error"

    goto :goto_b

    .line 423
    .end local v34    # "statusString":Ljava/lang/String;
    :cond_24
    move-object/from16 v0, v36

    iget-object v6, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mHighlight:Landroid/widget/TextView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_5

    .line 398
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x64 -> :sswitch_1
        0xc8 -> :sswitch_2
        0xc9 -> :sswitch_3
        0x12c -> :sswitch_4
        0x190 -> :sswitch_5
        0x191 -> :sswitch_6
    .end sparse-switch
.end method

.method public changeCursor(Landroid/database/Cursor;)V
    .locals 0
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 469
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 470
    return-void
.end method

.method protected getContentType()I
    .locals 1

    .prologue
    .line 447
    const/16 v0, 0x25

    return v0
.end method

.method public getFolderExpiredDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 463
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mFolderExpiredDate:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectableItemsCount()I
    .locals 4

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 147
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "status"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    .line 148
    :cond_0
    const/4 v1, 0x0

    .line 156
    :goto_0
    return v1

    .line 150
    :cond_1
    const/4 v1, 0x0

    .line 152
    .local v1, "cnt":I
    :cond_2
    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->isAvailableForDownload(Landroid/database/Cursor;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 153
    add-int/lit8 v1, v1, 0x1

    .line 155
    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_2

    goto :goto_0
.end method

.method public getSelectedItem()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 434
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 435
    .local v2, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    monitor-enter p0

    .line 436
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 437
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Long;>;"
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 438
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 439
    .local v0, "id":Ljava/lang/Integer;
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 441
    .end local v0    # "id":Ljava/lang/Integer;
    .end local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Long;>;"
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .restart local v1    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Long;>;"
    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 442
    return-object v2
.end method

.method public getSelectedItemsSize()J
    .locals 2

    .prologue
    .line 167
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mSelectedItemsSize:J

    return-wide v0
.end method

.method public isAvailableForDownload(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mCursor:Landroid/database/Cursor;

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->isAvailableForDownload(Landroid/database/Cursor;)Z

    move-result v0

    .line 110
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public selectAllItem()V
    .locals 8

    .prologue
    .line 195
    iget-boolean v6, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mIsSelectMode:Z

    if-eqz v6, :cond_0

    .line 196
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 197
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 216
    .end local v0    # "cursor":Landroid/database/Cursor;
    :cond_0
    :goto_0
    return-void

    .line 200
    .restart local v0    # "cursor":Landroid/database/Cursor;
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 204
    :cond_2
    const-string v6, "status"

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 205
    .local v3, "status":I
    const-string v6, "local_file"

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 207
    .local v2, "filePath":Ljava/lang/String;
    const/4 v6, 0x0

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 208
    .local v4, "id":J
    const/4 v6, 0x1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 209
    .local v1, "data":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-nez v6, :cond_2

    .line 212
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->notifyToSelectedItemChangeListener()V

    .line 213
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public selectItem(I)V
    .locals 10
    .param p1, "position"    # I

    .prologue
    .line 173
    iget-boolean v6, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mIsSelectMode:Z

    if-eqz v6, :cond_0

    .line 174
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 175
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 191
    .end local v0    # "cursor":Landroid/database/Cursor;
    :cond_0
    :goto_0
    return-void

    .line 178
    .restart local v0    # "cursor":Landroid/database/Cursor;
    :cond_1
    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 179
    const-string v6, "status"

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 180
    .local v3, "status":I
    const-string v6, "local_file"

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 181
    .local v2, "filePath":Ljava/lang/String;
    iget-wide v6, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mSelectedItemsSize:J

    const-string v8, "file_size"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    add-long/2addr v6, v8

    iput-wide v6, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mSelectedItemsSize:J

    .line 183
    const/4 v6, 0x0

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 184
    .local v4, "id":J
    const/4 v6, 0x1

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 185
    .local v1, "data":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->notifyToSelectedItemChangeListener()V

    .line 187
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public setFolderExpiredDate(J)V
    .locals 7
    .param p1, "expireDate"    # J

    .prologue
    const/4 v4, 0x3

    .line 452
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 454
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    move-wide v0, p1

    move v5, v4

    invoke-static/range {v0 .. v5}, Landroid/text/format/DateUtils;->formatSameDayTime(JJII)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mFolderExpiredDate:Ljava/lang/String;

    .line 459
    :goto_0
    return-void

    .line 458
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mFolderExpiredDate:Ljava/lang/String;

    goto :goto_0
.end method

.method public setFolderInfoArray(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter$RemoteShareFolderInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 101
    .local p1, "infoArray":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter$RemoteShareFolderInfo;>;"
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mFolderInfoArray:Landroid/util/SparseArray;

    .line 102
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->notifyDataSetChanged()V

    .line 103
    return-void
.end method

.method public setViewMode(II)V
    .locals 1
    .param p1, "mode"    # I
    .param p2, "layout"    # I

    .prologue
    .line 93
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 94
    :cond_0
    const v0, 0x7f040016

    invoke-super {p0, p1, v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->setViewMode(II)V

    .line 98
    :goto_0
    return-void

    .line 96
    :cond_1
    const v0, 0x7f040010

    invoke-super {p0, p1, v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->setViewMode(II)V

    goto :goto_0
.end method

.method public startSelectMode(II)V
    .locals 2
    .param p1, "selectionType"    # I
    .param p2, "from"    # I

    .prologue
    .line 162
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/adapter/RemoteShareAdapter;->mSelectedItemsSize:J

    .line 163
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->startSelectMode(II)V

    .line 164
    return-void
.end method
