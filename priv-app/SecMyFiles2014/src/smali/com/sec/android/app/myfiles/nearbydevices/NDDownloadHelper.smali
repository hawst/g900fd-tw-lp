.class public Lcom/sec/android/app/myfiles/nearbydevices/NDDownloadHelper;
.super Ljava/lang/Object;
.source "NDDownloadHelper.java"


# static fields
.field public static final DEFAULT_DOWNLOAD_DIR:Ljava/lang/String;

.field public static final MAX_DOWNLOAD_ITEMS:I = 0xc8

.field public static final TAG:Ljava/lang/String;

.field private static mDownloadManager:Landroid/app/DownloadManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    sget-object v0, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    sput-object v0, Lcom/sec/android/app/myfiles/nearbydevices/NDDownloadHelper;->DEFAULT_DOWNLOAD_DIR:Ljava/lang/String;

    .line 33
    const-class v0, Lcom/sec/android/app/myfiles/nearbydevices/NDDownloadHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/nearbydevices/NDDownloadHelper;->TAG:Ljava/lang/String;

    .line 86
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/myfiles/nearbydevices/NDDownloadHelper;->mDownloadManager:Landroid/app/DownloadManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static downloadItem(Landroid/content/Context;Lcom/samsung/android/allshare/Item;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "item"    # Lcom/samsung/android/allshare/Item;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "serverName"    # Ljava/lang/String;

    .prologue
    .line 37
    if-eqz p1, :cond_0

    if-nez p0, :cond_1

    .line 56
    :cond_0
    :goto_0
    return-void

    .line 41
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v2

    .line 42
    .local v2, "uri":Landroid/net/Uri;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/samsung/android/allshare/Item;->getExtension()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 44
    .local v1, "title":Ljava/lang/String;
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 48
    new-instance v0, Landroid/app/DownloadManager$Request;

    invoke-direct {v0, v2}, Landroid/app/DownloadManager$Request;-><init>(Landroid/net/Uri;)V

    .line 49
    .local v0, "request":Landroid/app/DownloadManager$Request;
    invoke-virtual {v0, v1}, Landroid/app/DownloadManager$Request;->setTitle(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    .line 50
    invoke-virtual {v0, p0, p2, v1}, Landroid/app/DownloadManager$Request;->setDestinationInExternalFilesDir(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    .line 52
    invoke-virtual {v0, p3}, Landroid/app/DownloadManager$Request;->setDescription(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    .line 53
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/app/DownloadManager$Request;->setNotificationVisibility(I)Landroid/app/DownloadManager$Request;

    .line 55
    invoke-static {p0, v0}, Lcom/sec/android/app/myfiles/nearbydevices/NDDownloadHelper;->postDownloadRequest(Landroid/content/Context;Landroid/app/DownloadManager$Request;)V

    goto :goto_0
.end method

.method public static downloadItems(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "serverName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/allshare/Item;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 60
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/allshare/Item;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/16 v1, 0xc8

    if-ge v0, v1, :cond_0

    .line 61
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/Item;

    invoke-static {p0, v1, p2, p3}, Lcom/sec/android/app/myfiles/nearbydevices/NDDownloadHelper;->downloadItem(Landroid/content/Context;Lcom/samsung/android/allshare/Item;Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 64
    :cond_0
    return-void
.end method

.method private static getDownloadManager(Landroid/content/Context;)Landroid/app/DownloadManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 75
    sget-object v0, Lcom/sec/android/app/myfiles/nearbydevices/NDDownloadHelper;->mDownloadManager:Landroid/app/DownloadManager;

    if-nez v0, :cond_1

    .line 76
    const-class v1, Lcom/sec/android/app/myfiles/nearbydevices/NDDownloadHelper;

    monitor-enter v1

    .line 77
    :try_start_0
    sget-object v0, Lcom/sec/android/app/myfiles/nearbydevices/NDDownloadHelper;->mDownloadManager:Landroid/app/DownloadManager;

    if-nez v0, :cond_0

    .line 78
    const-string v0, "download"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    sput-object v0, Lcom/sec/android/app/myfiles/nearbydevices/NDDownloadHelper;->mDownloadManager:Landroid/app/DownloadManager;

    .line 81
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    :cond_1
    sget-object v0, Lcom/sec/android/app/myfiles/nearbydevices/NDDownloadHelper;->mDownloadManager:Landroid/app/DownloadManager;

    return-object v0

    .line 81
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static postDownloadRequest(Landroid/content/Context;Landroid/app/DownloadManager$Request;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "request"    # Landroid/app/DownloadManager$Request;

    .prologue
    .line 67
    invoke-static {p0}, Lcom/sec/android/app/myfiles/nearbydevices/NDDownloadHelper;->getDownloadManager(Landroid/content/Context;)Landroid/app/DownloadManager;

    move-result-object v0

    .line 68
    .local v0, "dManager":Landroid/app/DownloadManager;
    const-class v2, Lcom/sec/android/app/myfiles/nearbydevices/NDDownloadHelper;

    monitor-enter v2

    .line 69
    :try_start_0
    invoke-virtual {v0, p1}, Landroid/app/DownloadManager;->enqueue(Landroid/app/DownloadManager$Request;)J

    .line 70
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    sget-object v1, Lcom/sec/android/app/myfiles/nearbydevices/NDDownloadHelper;->TAG:Ljava/lang/String;

    const-string v2, "Download request has been posted."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    return-void

    .line 70
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
