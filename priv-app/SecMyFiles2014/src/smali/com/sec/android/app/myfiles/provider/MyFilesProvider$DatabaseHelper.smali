.class Lcom/sec/android/app/myfiles/provider/MyFilesProvider$DatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "MyFilesProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/provider/MyFilesProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DatabaseHelper"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "factory"    # Landroid/database/sqlite/SQLiteDatabase$CursorFactory;
    .param p4, "version"    # I

    .prologue
    .line 949
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 950
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 956
    const-string v0, "CREATE TABLE IF NOT EXISTS folders ( _id INTEGER PRIMARY KEY,_data TEXT,file_id INTEGER,date_modified INTEGER,count_opened INTEGER,count_used INTEGER,date_used INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 967
    const-string v0, "CREATE TABLE IF NOT EXISTS files ( _id INTEGER PRIMARY KEY,_data TEXT,file_id INTEGER,_size INTEGER,date_modified INTEGER,count_opened INTEGER,count_used INTEGER,date_used INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 978
    const-string v0, "CREATE TABLE IF NOT EXISTS shortcuts ( _id INTEGER PRIMARY KEY,type INTEGER,shortcut_index INTEGER,_data TEXT,title TEXT,shortcut_drawable BLOB,f_sub_type INTEGER,f_server_name TEXT,f_port_no INTEGER,f_mode INTEGER,f_username TEXT,f_password TEXT,f_is_anonymous INTEGER,f_encodoing TEXT,f_encryption INTEGER,f_is_hidden_filed INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 998
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 1006
    const/16 v0, 0x65

    if-ge p2, v0, :cond_0

    .line 1008
    const-string v0, "DROP TABLE IF EXISTS folders"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1009
    const-string v0, "DROP TABLE IF EXISTS files"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1010
    const-string v0, "DROP TABLE IF EXISTS shortcuts"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1011
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/provider/MyFilesProvider$DatabaseHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1016
    :cond_0
    return-void
.end method
