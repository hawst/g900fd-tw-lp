.class public Lcom/sec/android/app/myfiles/fragment/RemoteShareDetailFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;
.source "RemoteShareDetailFragment.java"


# instance fields
.field mAdapter:Landroid/widget/BaseAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public getFileLastModifiedTime(Ljava/lang/String;I)J
    .locals 2
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 62
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getFileName(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 46
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareDetailFragment;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v2, p2}, Landroid/widget/BaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 47
    .local v0, "cursor":Landroid/database/Cursor;
    const-string v2, "filename"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 48
    .local v1, "fileName":Ljava/lang/String;
    return-object v1
.end method

.method public getFileSize(Ljava/lang/String;I)J
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 54
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareDetailFragment;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v2, p2}, Landroid/widget/BaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 55
    .local v0, "cursor":Landroid/database/Cursor;
    const-string v2, "file_size"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 56
    .local v1, "fileSize":Ljava/lang/Long;
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    return-wide v2
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v1, 0x12

    .line 34
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->onCreate(Landroid/os/Bundle;)V

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareDetailFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/MainActivity;->getCurrentFragmentID()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 37
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareDetailFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getAdapter(I)Landroid/widget/BaseAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareDetailFragment;->mAdapter:Landroid/widget/BaseAdapter;

    .line 41
    :goto_0
    return-void

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareDetailFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v0

    const/16 v1, 0x25

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getAdapter(I)Landroid/widget/BaseAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RemoteShareDetailFragment;->mAdapter:Landroid/widget/BaseAdapter;

    goto :goto_0
.end method
