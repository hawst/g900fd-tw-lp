.class public Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;
.super Ljava/lang/Object;
.source "CloudActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/cloud/CloudActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DropboxAPIHelper"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper$ConcreteProgressListener;
    }
.end annotation


# static fields
.field private static instance:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;


# instance fields
.field dAPI:Lcom/samsung/scloud/DropboxAPI;

.field mContext:Landroid/content/Context;

.field public mSignInRequested:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->instance:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->mContext:Landroid/content/Context;

    .line 51
    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->dAPI:Lcom/samsung/scloud/DropboxAPI;

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->mSignInRequested:Z

    .line 64
    iput-object p1, p0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->mContext:Landroid/content/Context;

    .line 65
    invoke-static {p1}, Lcom/samsung/scloud/DropboxAPI;->getInstance(Landroid/content/Context;)Lcom/samsung/scloud/DropboxAPI;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->dAPI:Lcom/samsung/scloud/DropboxAPI;

    .line 66
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    sget-object v0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->instance:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    if-nez v0, :cond_0

    .line 58
    new-instance v0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->instance:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    .line 60
    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->instance:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    return-object v0
.end method


# virtual methods
.method public createFolder(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;
    .locals 3
    .param p1, "folderName"    # Ljava/lang/String;
    .param p2, "serverPath"    # Ljava/lang/String;

    .prologue
    .line 217
    const/4 v1, 0x0

    .line 219
    .local v1, "folder":Lcom/samsung/scloud/data/SCloudFolder;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->dAPI:Lcom/samsung/scloud/DropboxAPI;

    invoke-virtual {v2, p1, p2}, Lcom/samsung/scloud/DropboxAPI;->makeFolder(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 223
    :goto_0
    return-object v1

    .line 220
    :catch_0
    move-exception v0

    .line 221
    .local v0, "e":Lcom/samsung/scloud/exception/SCloudException;
    invoke-virtual {v0}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V

    goto :goto_0
.end method

.method public deleteFile(Ljava/lang/String;)V
    .locals 3
    .param p1, "serverPath"    # Ljava/lang/String;

    .prologue
    .line 191
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->dAPI:Lcom/samsung/scloud/DropboxAPI;

    invoke-virtual {v2, p1}, Lcom/samsung/scloud/DropboxAPI;->delete(Ljava/lang/String;)Z

    .line 193
    new-instance v0, Lcom/sec/android/app/myfiles/db/CacheDB;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    .line 194
    .local v0, "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 195
    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/db/CacheDB;->deleteCloudData(Ljava/lang/String;)I

    .line 196
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    .line 200
    .end local v0    # "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    :goto_0
    return-void

    .line 197
    :catch_0
    move-exception v1

    .line 198
    .local v1, "e":Lcom/samsung/scloud/exception/SCloudException;
    invoke-virtual {v1}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V

    goto :goto_0
.end method

.method public download(Ljava/io/File;Ljava/lang/String;)V
    .locals 4
    .param p1, "localFile"    # Ljava/io/File;
    .param p2, "serverPath"    # Ljava/lang/String;

    .prologue
    .line 159
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->dAPI:Lcom/samsung/scloud/DropboxAPI;

    const/4 v2, 0x0

    new-instance v3, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper$ConcreteProgressListener;

    invoke-direct {v3}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper$ConcreteProgressListener;-><init>()V

    invoke-virtual {v1, p1, p2, v2, v3}, Lcom/samsung/scloud/DropboxAPI;->getFile(Ljava/io/File;Ljava/lang/String;ZLcom/samsung/scloud/response/ProgressListener;)V
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    :goto_0
    return-void

    .line 160
    :catch_0
    move-exception v0

    .line 161
    .local v0, "e":Lcom/samsung/scloud/exception/SCloudException;
    invoke-virtual {v0}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V

    goto :goto_0
.end method

.method public downloadCancelable(Ljava/io/File;Ljava/lang/String;ZLcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/DropboxAPI$CancelableDownload;
    .locals 6
    .param p1, "localFile"    # Ljava/io/File;
    .param p2, "serverFileIdOrPath"    # Ljava/lang/String;
    .param p3, "overwrite"    # Z
    .param p4, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;

    .prologue
    .line 185
    new-instance v0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->dAPI:Lcom/samsung/scloud/DropboxAPI;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;-><init>(Lcom/samsung/scloud/DropboxAPI;Ljava/io/File;Ljava/lang/String;ZLcom/samsung/scloud/response/ProgressListener;)V

    .line 186
    .local v0, "cancelabledownload":Lcom/samsung/scloud/DropboxAPI$CancelableDownload;
    return-object v0
.end method

.method public fileCopyOrMove(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 4
    .param p1, "fromPath"    # Ljava/lang/String;
    .param p2, "toPath"    # Ljava/lang/String;
    .param p3, "operationType"    # I

    .prologue
    const/4 v1, 0x1

    .line 204
    if-nez p3, :cond_0

    .line 205
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->dAPI:Lcom/samsung/scloud/DropboxAPI;

    const/4 v3, 0x1

    invoke-virtual {v2, p1, p2, v3}, Lcom/samsung/scloud/DropboxAPI;->copy(Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/scloud/data/SCloudNode;

    .line 212
    :goto_0
    return v1

    .line 207
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->dAPI:Lcom/samsung/scloud/DropboxAPI;

    const/4 v3, 0x1

    invoke-virtual {v2, p1, p2, v3}, Lcom/samsung/scloud/DropboxAPI;->move(Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/scloud/data/SCloudNode;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 208
    :catch_0
    move-exception v0

    .line 209
    .local v0, "e":Lcom/samsung/scloud/exception/SCloudException;
    invoke-virtual {v0}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V

    .line 210
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAuthToken()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->dAPI:Lcom/samsung/scloud/DropboxAPI;

    invoke-virtual {v0}, Lcom/samsung/scloud/DropboxAPI;->getAuthToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDeltaList(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;
    .locals 3
    .param p1, "cloudDeltaCursor"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/scloud/data/SCloudNodeList",
            "<",
            "Lcom/samsung/scloud/data/SCloudNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 238
    const/4 v1, 0x0

    .line 240
    .local v1, "nodes":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<Lcom/samsung/scloud/data/SCloudNode;>;"
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->dAPI:Lcom/samsung/scloud/DropboxAPI;

    invoke-virtual {v2, p1}, Lcom/samsung/scloud/DropboxAPI;->getDeltaList(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 244
    :goto_0
    return-object v1

    .line 241
    :catch_0
    move-exception v0

    .line 242
    .local v0, "e":Lcom/samsung/scloud/exception/SCloudException;
    invoke-virtual {v0}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V

    goto :goto_0
.end method

.method public getFileInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 3
    .param p1, "serverPath"    # Ljava/lang/String;

    .prologue
    .line 119
    const/4 v1, 0x0

    .line 122
    .local v1, "file":Lcom/samsung/scloud/data/SCloudFile;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->dAPI:Lcom/samsung/scloud/DropboxAPI;

    invoke-virtual {v2, p1}, Lcom/samsung/scloud/DropboxAPI;->getFileInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFile;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 127
    :goto_0
    return-object v1

    .line 123
    :catch_0
    move-exception v0

    .line 124
    .local v0, "e":Lcom/samsung/scloud/exception/SCloudException;
    invoke-virtual {v0}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V

    goto :goto_0
.end method

.method public getFileThumbnail(Ljava/io/File;Ljava/lang/String;IILcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 7
    .param p1, "thumbnailFile"    # Ljava/io/File;
    .param p2, "serverPath"    # Ljava/lang/String;
    .param p3, "thumbnailSizes"    # I
    .param p4, "thumbnailType"    # I
    .param p5, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;

    .prologue
    .line 249
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->dAPI:Lcom/samsung/scloud/DropboxAPI;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/scloud/DropboxAPI;->getThumbnail(Ljava/io/File;Ljava/lang/String;IILcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 252
    :goto_0
    return-object v0

    .line 250
    :catch_0
    move-exception v6

    .line 251
    .local v6, "e":Lcom/samsung/scloud/exception/SCloudException;
    invoke-virtual {v6}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V

    .line 252
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFolderInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;
    .locals 3
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 228
    const/4 v1, 0x0

    .line 230
    .local v1, "folder":Lcom/samsung/scloud/data/SCloudFolder;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->dAPI:Lcom/samsung/scloud/DropboxAPI;

    invoke-virtual {v2, p1}, Lcom/samsung/scloud/DropboxAPI;->getFolderInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 234
    :goto_0
    return-object v1

    .line 231
    :catch_0
    move-exception v0

    .line 232
    .local v0, "e":Lcom/samsung/scloud/exception/SCloudException;
    invoke-virtual {v0}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V

    goto :goto_0
.end method

.method public isAuthKeySaved()Z
    .locals 4

    .prologue
    .line 106
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v2

    .line 107
    .local v2, "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDropboxAuthKey()Ljava/lang/String;

    move-result-object v0

    .line 108
    .local v0, "authKey":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDropboxAuthSecret()Ljava/lang/String;

    move-result-object v1

    .line 109
    .local v1, "authSecret":Ljava/lang/String;
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 110
    :cond_0
    const/4 v3, 0x0

    .line 113
    :goto_0
    return v3

    .line 112
    :cond_1
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->setAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public isDropboxAccountIsSignin()Z
    .locals 5

    .prologue
    .line 93
    const/4 v1, 0x0

    .line 94
    .local v1, "bResult":Z
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .line 95
    .local v2, "manager":Landroid/accounts/AccountManager;
    const-string v3, "com.dropbox.android.account"

    invoke-virtual {v2, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 96
    .local v0, "accountAttr":[Landroid/accounts/Account;
    array-length v3, v0

    if-lez v3, :cond_0

    .line 97
    const-string v3, "DropboxAPIHelper"

    const-string v4, "dropbox account is login"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    const/4 v1, 0x1

    .line 102
    :goto_0
    return v1

    .line 100
    :cond_0
    const-string v3, "DropboxAPIHelper"

    const-string v4, "dropboxAccount is signout"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public renameFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "serverPath"    # Ljava/lang/String;
    .param p2, "newName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 167
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->dAPI:Lcom/samsung/scloud/DropboxAPI;

    const/4 v3, 0x0

    invoke-virtual {v2, p1, p2, v3}, Lcom/samsung/scloud/DropboxAPI;->rename(Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/scloud/data/SCloudNode;

    .line 168
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteThumbnail(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 169
    :catch_0
    move-exception v0

    .line 170
    .local v0, "e":Lcom/samsung/scloud/exception/SCloudException;
    invoke-virtual {v0}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V

    goto :goto_0
.end method

.method public requestGetToken()V
    .locals 3

    .prologue
    .line 69
    const-string v1, "DropboxAPIHelper"

    const-string v2, "requestGetToken"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.cloudagent.ACTION_REQUEST_TOKEN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 71
    .local v0, "gettokenintent":Landroid/content/Intent;
    const-string v1, "packageName"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 72
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 73
    return-void
.end method

.method public requestSignIn()V
    .locals 3

    .prologue
    .line 76
    const-string v1, "DropboxAPIHelper"

    const-string v2, "requestSignIn"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->mSignInRequested:Z

    .line 78
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.cloudagent.ACTION_REQUEST_SIGNIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 79
    .local v0, "loginIntent":Landroid/content/Intent;
    const-string v1, "packageName"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 80
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 81
    return-void
.end method

.method public setAuthToken(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "secret"    # Ljava/lang/String;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->dAPI:Lcom/samsung/scloud/DropboxAPI;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/scloud/DropboxAPI;->setAuthToken(Ljava/lang/String;)V

    .line 86
    return-void
.end method

.method public upload(Ljava/io/File;Ljava/lang/String;ZLjava/lang/String;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 8
    .param p1, "localFile"    # Ljava/io/File;
    .param p2, "serverPath"    # Ljava/lang/String;
    .param p3, "overwrite"    # Z
    .param p4, "revision"    # Ljava/lang/String;

    .prologue
    .line 146
    const/4 v6, 0x0

    .line 148
    .local v6, "cloudFile":Lcom/samsung/scloud/data/SCloudFile;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->dAPI:Lcom/samsung/scloud/DropboxAPI;

    new-instance v5, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper$ConcreteProgressListener;

    invoke-direct {v5}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper$ConcreteProgressListener;-><init>()V

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/scloud/DropboxAPI;->putFile(Ljava/io/File;Ljava/lang/String;ZLjava/lang/String;Lcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 153
    :goto_0
    return-object v6

    .line 149
    :catch_0
    move-exception v7

    .line 150
    .local v7, "e":Lcom/samsung/scloud/exception/SCloudException;
    invoke-virtual {v7}, Lcom/samsung/scloud/exception/SCloudException;->printStackTrace()V

    goto :goto_0
.end method

.method public uploadCancelable(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/DropboxAPI$CancelableUpload;
    .locals 7
    .param p1, "sourcefile"    # Ljava/io/File;
    .param p2, "serverPath"    # Ljava/lang/String;
    .param p3, "newFileName"    # Ljava/lang/String;
    .param p4, "overwrite"    # Z
    .param p5, "revision"    # Ljava/lang/String;
    .param p6, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;

    .prologue
    .line 179
    new-instance v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->dAPI:Lcom/samsung/scloud/DropboxAPI;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-object v2, p1

    move-object v3, p2

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;-><init>(Lcom/samsung/scloud/DropboxAPI;Ljava/io/File;Ljava/lang/String;ZLjava/lang/String;Lcom/samsung/scloud/response/ProgressListener;)V

    .line 180
    .local v0, "cancelableUpload":Lcom/samsung/scloud/DropboxAPI$CancelableUpload;
    return-object v0
.end method
