.class public interface abstract Lcom/sec/android/app/myfiles/provider/MyFilesStore$Shortcut$ShortcutColumns;
.super Ljava/lang/Object;
.source "MyFilesStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/provider/MyFilesStore$Shortcut;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ShortcutColumns"
.end annotation


# static fields
.field public static final SHORTCUT_DRAWABLE:Ljava/lang/String; = "shortcut_drawable"

.field public static final SHORTCUT_F_ENCODING:Ljava/lang/String; = "f_encodoing"

.field public static final SHORTCUT_F_ENCRYPTION:Ljava/lang/String; = "f_encryption"

.field public static final SHORTCUT_F_IS_ANONYMOUS:Ljava/lang/String; = "f_is_anonymous"

.field public static final SHORTCUT_F_IS_HIDDEN_FILES:Ljava/lang/String; = "f_is_hidden_filed"

.field public static final SHORTCUT_F_MODE:Ljava/lang/String; = "f_mode"

.field public static final SHORTCUT_F_PASSWORD:Ljava/lang/String; = "f_password"

.field public static final SHORTCUT_F_PORT_NO:Ljava/lang/String; = "f_port_no"

.field public static final SHORTCUT_F_SERVER_NAME:Ljava/lang/String; = "f_server_name"

.field public static final SHORTCUT_F_SUB_TYPE:Ljava/lang/String; = "f_sub_type"

.field public static final SHORTCUT_F_USERNAME:Ljava/lang/String; = "f_username"

.field public static final SHORTCUT_INDEX:Ljava/lang/String; = "shortcut_index"

.field public static final SHORTCUT_PATH:Ljava/lang/String; = "_data"

.field public static final SHORTCUT_TITLE:Ljava/lang/String; = "title"

.field public static final SHORTCUT_TYPE:Ljava/lang/String; = "type"

.field public static final _ID:Ljava/lang/String; = "_id"
