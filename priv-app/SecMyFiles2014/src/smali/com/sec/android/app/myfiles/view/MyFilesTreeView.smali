.class public Lcom/sec/android/app/myfiles/view/MyFilesTreeView;
.super Landroid/widget/ListView;
.source "MyFilesTreeView.java"


# instance fields
.field private mAdapter:Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;

.field private mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

.field private mNavigationObserver:Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 130
    new-instance v0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView$1;-><init>(Lcom/sec/android/app/myfiles/view/MyFilesTreeView;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->mNavigationObserver:Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;

    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->init()V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 130
    new-instance v0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView$1;-><init>(Lcom/sec/android/app/myfiles/view/MyFilesTreeView;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->mNavigationObserver:Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;

    .line 47
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->init()V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 130
    new-instance v0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView$1;-><init>(Lcom/sec/android/app/myfiles/view/MyFilesTreeView;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->mNavigationObserver:Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;

    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->init()V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/myfiles/navigation/AbsNavigation;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "navigation"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 130
    new-instance v0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView$1;-><init>(Lcom/sec/android/app/myfiles/view/MyFilesTreeView;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->mNavigationObserver:Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;

    .line 61
    iput-object p2, p0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .line 63
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->init()V

    .line 64
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/view/MyFilesTreeView;)Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->mAdapter:Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;

    return-object v0
.end method

.method private init()V
    .locals 0

    .prologue
    .line 68
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->mNavigationObserver:Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->removeObserver(Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;)V

    .line 107
    :cond_0
    return-void
.end method

.method public goTo(Ljava/lang/String;)Z
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 92
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v1, :cond_0

    .line 94
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z

    move-result v0

    .line 97
    :cond_0
    return v0
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0
    .param p1, "x0"    # Landroid/widget/Adapter;

    .prologue
    .line 29
    check-cast p1, Landroid/widget/ListAdapter;

    .end local p1    # "x0":Landroid/widget/Adapter;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 1
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    .line 73
    invoke-super {p0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 75
    instance-of v0, p1, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;

    if-eqz v0, :cond_0

    .line 77
    check-cast p1, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;

    .end local p1    # "adapter":Landroid/widget/ListAdapter;
    iput-object p1, p0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->mAdapter:Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;

    .line 79
    :cond_0
    return-void
.end method

.method public setNavigation(Lcom/sec/android/app/myfiles/navigation/AbsNavigation;)V
    .locals 2
    .param p1, "navigation"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->mNavigationObserver:Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->registerObserver(Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;)V

    .line 87
    return-void
.end method

.method public setPositionIndicatorPressed(Z)V
    .locals 1
    .param p1, "pressed"    # Z

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->mAdapter:Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->setPositionIndicatorPressed(Z)V

    .line 114
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->setVerticalScrollBarEnabled(Z)V

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->mAdapter:Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->notifyDataSetChanged()V

    .line 117
    return-void

    .line 114
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showPositionIndicator(Z)V
    .locals 1
    .param p1, "show"    # Z

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->mAdapter:Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->showPositionIndicator(Z)V

    .line 123
    return-void
.end method
