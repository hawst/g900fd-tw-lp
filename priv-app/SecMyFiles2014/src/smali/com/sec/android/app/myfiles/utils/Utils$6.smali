.class final Lcom/sec/android/app/myfiles/utils/Utils$6;
.super Ljava/lang/Object;
.source "Utils.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/utils/Utils;->getEditTextFilter(Landroid/content/Context;I)[Landroid/text/InputFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$ctx:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 3283
    iput-object p1, p0, Lcom/sec/android/app/myfiles/utils/Utils$6;->val$ctx:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 8
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .prologue
    const v7, 0x7f0b00a2

    const/4 v6, 0x0

    .line 3296
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3298
    .local v2, "inputString":Ljava/lang/String;
    const/4 v0, 0x0

    .line 3300
    .local v0, "containsInvalidChar":Z
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->hasEmojiString(Ljava/lang/CharSequence;)Z

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 3301
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$100()Landroid/widget/Toast;

    move-result-object v4

    if-nez v4, :cond_0

    .line 3303
    iget-object v4, p0, Lcom/sec/android/app/myfiles/utils/Utils$6;->val$ctx:Landroid/content/Context;

    invoke-static {v4, v7, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    # setter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->access$102(Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 3309
    :goto_0
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$100()Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 3310
    const-string v4, ""

    .line 3340
    :goto_1
    return-object v4

    .line 3306
    :cond_0
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$100()Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/widget/Toast;->setText(I)V

    goto :goto_0

    .line 3313
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->INVALID_CHAR:[Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$300()[Ljava/lang/String;

    move-result-object v4

    array-length v4, v4

    if-ge v1, v4, :cond_3

    .line 3315
    :goto_3
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->INVALID_CHAR:[Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$300()[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v2, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_2

    .line 3317
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->INVALID_CHAR:[Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$300()[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v2, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 3319
    .local v3, "pos":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, v3, 0x1

    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3321
    const/4 v0, 0x1

    .line 3322
    goto :goto_3

    .line 3313
    .end local v3    # "pos":I
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3325
    :cond_3
    if-eqz v0, :cond_5

    .line 3327
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$100()Landroid/widget/Toast;

    move-result-object v4

    if-nez v4, :cond_4

    .line 3329
    iget-object v4, p0, Lcom/sec/android/app/myfiles/utils/Utils$6;->val$ctx:Landroid/content/Context;

    invoke-static {v4, v7, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    # setter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->access$102(Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 3335
    :goto_4
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$100()Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 3337
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2, v6, v4}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v4

    goto :goto_1

    .line 3332
    :cond_4
    # getter for: Lcom/sec/android/app/myfiles/utils/Utils;->mToast:Landroid/widget/Toast;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->access$100()Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/widget/Toast;->setText(I)V

    goto :goto_4

    .line 3340
    :cond_5
    const/4 v4, 0x0

    goto :goto_1
.end method
