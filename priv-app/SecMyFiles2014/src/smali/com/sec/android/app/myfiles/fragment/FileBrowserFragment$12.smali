.class Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;
.super Ljava/lang/Object;
.source "FileBrowserFragment.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)V
    .locals 0

    .prologue
    .line 1793
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1798
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    move v3, v5

    .line 1915
    :goto_0
    return v3

    :pswitch_0
    move v3, v4

    .line 1802
    goto :goto_0

    .line 1807
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v3, :cond_0

    .line 1809
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mPathToMove:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->access$800(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->isNotMovingToPrivate(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1811
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->refreshNavigation()V

    .line 1826
    :cond_0
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iput-object v8, v3, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mTempCursor:Landroid/database/Cursor;

    .line 1827
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v3, v5}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->showWaitProgressDialog(Z)V

    .line 1828
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mFromFindoFile:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->access$900(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1830
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v5, v3, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v3, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mFromFindoFile:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->access$900(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->getPositionFromPath(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v5, v3}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setSelection(I)V

    :cond_1
    move v3, v4

    .line 1834
    goto :goto_0

    .line 1815
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->notifyUpdate(Z)V

    goto :goto_1

    .line 1837
    :pswitch_2
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    .line 1838
    .local v2, "path":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mIsBack:Z
    invoke-static {v6}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->access$700(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v3, v2, v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1841
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    instance-of v3, v3, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    if-eqz v3, :cond_3

    .line 1843
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v3, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    if-eqz v3, :cond_3

    .line 1845
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v3, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->removeDelayedMessage()V

    .line 1849
    const/4 v3, 0x1

    const/4 v6, -0x1

    :try_start_0
    invoke-static {v3, v6}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1859
    :cond_3
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iput-boolean v5, v3, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mIsSelector:Z

    .line 1875
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    # invokes: Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->setPositionZero()V
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->access$1000(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)V

    .line 1877
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getSelectModeFrom()I

    move-result v3

    if-nez v3, :cond_5

    .line 1879
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->isSelectMode()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1881
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->finishSelectMode()V

    .line 1902
    :cond_4
    :goto_3
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v6, v3, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v3, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mPreviousfolder:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->access$1100(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->getPositionFromPath(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v6, v3}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setSelection(I)V

    .line 1903
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    # setter for: Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mPreviousfolder:Ljava/lang/String;
    invoke-static {v3, v8}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->access$1102(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 1909
    :goto_4
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iput-object v8, v3, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mTempCursor:Landroid/database/Cursor;

    .line 1910
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v3, v5}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->showWaitProgressDialog(Z)V

    move v3, v4

    .line 1912
    goto/16 :goto_0

    .line 1852
    :catch_0
    move-exception v0

    .line 1854
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2

    .line 1886
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getSelectionType()I

    move-result v3

    if-eq v3, v9, :cond_4

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$12;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getSelectionType()I

    move-result v3

    const/4 v6, 0x3

    if-eq v3, v6, :cond_4

    .line 1889
    sget-object v3, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mOptionsMenu:Landroid/view/Menu;

    if-eqz v3, :cond_4

    .line 1891
    sget-object v3, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mOptionsMenu:Landroid/view/Menu;

    const v6, 0x7f0f0148

    invoke-interface {v3, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1893
    .local v1, "item":Landroid/view/MenuItem;
    if-eqz v1, :cond_4

    .line 1895
    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_3

    .line 1906
    .end local v1    # "item":Landroid/view/MenuItem;
    :cond_6
    const-string v3, "FileBrowserFragment"

    const-string v6, "Can\'t go to the folder"

    invoke-static {v5, v3, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1907
    const-string v3, "FileBrowserFragment"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "=> path = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v9, v3, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 1798
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
