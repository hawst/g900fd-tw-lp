.class public Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailLoaderThread;
.super Ljava/lang/Thread;
.source "ThumbnailLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/utils/ThumbnailLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ThumbnailLoaderThread"
.end annotation


# instance fields
.field public mBackThreadHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 67
    const/4 v0, 0x0

    const-string v1, "ThumbnailLoader"

    const-string v2, "ThumbnailLoaderThread"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 70
    return-void
.end method


# virtual methods
.method public quit()V
    .locals 1

    .prologue
    .line 136
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    .line 138
    .local v0, "looper":Landroid/os/Looper;
    if-eqz v0, :cond_0

    .line 140
    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 142
    :cond_0
    return-void
.end method

.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 76
    const-string v0, "ThumbnailLoader"

    const-string v1, "ThumbnailLoaderThread run..."

    invoke-static {v2, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 78
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 80
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailLoaderThread$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailLoaderThread$1;-><init>(Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailLoaderThread;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailLoaderThread;->mBackThreadHandler:Landroid/os/Handler;

    .line 128
    const-string v0, "ThumbnailLoader"

    const-string v1, "ThumbnailLoader back handler created"

    invoke-static {v2, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 130
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 131
    return-void
.end method
