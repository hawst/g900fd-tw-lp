.class public Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;
.super Landroid/os/AsyncTask;
.source "DownloadFolderOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/sec/android/app/myfiles/ftp/DownloadItem;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final MODULE:Ljava/lang/String;


# instance fields
.field private volatile mAbort:Z

.field private final mContext:Landroid/content/Context;

.field private final mDialogSync:Ljava/lang/Object;

.field protected mDownloadItem:Lcom/sec/android/app/myfiles/ftp/DownloadItem;

.field protected mDownloadItems:[Lcom/sec/android/app/myfiles/ftp/DownloadItem;

.field private mDownloadingPaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final mFetchingDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

.field private mFiles:[Ljava/lang/String;

.field private mFolders:[Ljava/lang/String;

.field private final mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

.field private mMove:Z

.field private final mOperationArg:Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;

.field private final mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

.field private volatile mTransferring:Z

.field private final mUploadSrc:Ljava/lang/String;

.field private final mUploadTarget:Ljava/lang/String;

.field private final mUploadTargetFtp:Lcom/sec/android/app/myfiles/ftp/FTPParams;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 343
    const-class v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->MODULE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "arg"    # Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;
    .param p2, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "subtitle"    # Ljava/lang/String;
    .param p5, "fileName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 76
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 328
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mTransferring:Z

    .line 329
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mDownloadingPaths:Ljava/util/ArrayList;

    .line 330
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mAbort:Z

    .line 342
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mMove:Z

    .line 77
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mOperationArg:Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;

    .line 78
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mContext:Landroid/content/Context;

    .line 79
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->getDialogSync()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mDialogSync:Ljava/lang/Object;

    .line 80
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->getFTPConnectionHandler()Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    .line 81
    iput-object p2, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    .line 83
    iput-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mUploadTarget:Ljava/lang/String;

    .line 84
    iput-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mUploadTargetFtp:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .line 85
    iput-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mUploadSrc:Ljava/lang/String;

    .line 86
    invoke-direct {p0, p3, p4, p5}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->getFetchingDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFetchingDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    .line 87
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "arg"    # Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;
    .param p2, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "subtitle"    # Ljava/lang/String;
    .param p5, "fileName"    # Ljava/lang/String;
    .param p6, "uploadTargetPath"    # Ljava/lang/String;
    .param p7, "uploadTarget"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p8, "uploadSrc"    # Ljava/lang/String;
    .param p9, "move"    # Z

    .prologue
    const/4 v1, 0x0

    .line 62
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 328
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mTransferring:Z

    .line 329
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mDownloadingPaths:Ljava/util/ArrayList;

    .line 330
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mAbort:Z

    .line 342
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mMove:Z

    .line 63
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mOperationArg:Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;

    .line 64
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mContext:Landroid/content/Context;

    .line 65
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->getDialogSync()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mDialogSync:Ljava/lang/Object;

    .line 66
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->getFTPConnectionHandler()Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    .line 67
    iput-object p2, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    .line 69
    iput-boolean p9, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mMove:Z

    .line 70
    iput-object p6, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mUploadTarget:Ljava/lang/String;

    .line 71
    iput-object p8, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mUploadSrc:Ljava/lang/String;

    .line 72
    iput-object p7, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mUploadTargetFtp:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .line 73
    invoke-direct {p0, p3, p4, p5}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->getFetchingDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFetchingDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    .line 74
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "arg"    # Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;
    .param p2, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "subtitle"    # Ljava/lang/String;
    .param p5, "fileName"    # Ljava/lang/String;
    .param p6, "uploadTarget"    # Ljava/lang/String;
    .param p7, "uploadSrc"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 328
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mTransferring:Z

    .line 329
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mDownloadingPaths:Ljava/util/ArrayList;

    .line 330
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mAbort:Z

    .line 342
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mMove:Z

    .line 49
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mOperationArg:Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;

    .line 50
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mContext:Landroid/content/Context;

    .line 51
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->getDialogSync()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mDialogSync:Ljava/lang/Object;

    .line 52
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->getFTPConnectionHandler()Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    .line 53
    iput-object p2, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    .line 55
    iput-object p6, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mUploadTarget:Ljava/lang/String;

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mUploadTargetFtp:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .line 57
    iput-object p7, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mUploadSrc:Ljava/lang/String;

    .line 58
    invoke-direct {p0, p3, p4, p5}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->getFetchingDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFetchingDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    .line 59
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "arg"    # Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;
    .param p2, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "subtitle"    # Ljava/lang/String;
    .param p5, "fileName"    # Ljava/lang/String;
    .param p6, "move"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 89
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 328
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mTransferring:Z

    .line 329
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mDownloadingPaths:Ljava/util/ArrayList;

    .line 330
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mAbort:Z

    .line 342
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mMove:Z

    .line 90
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mOperationArg:Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;

    .line 91
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mContext:Landroid/content/Context;

    .line 92
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->getDialogSync()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mDialogSync:Ljava/lang/Object;

    .line 93
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->getFTPConnectionHandler()Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    .line 94
    iput-object p2, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    .line 96
    iput-boolean p6, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mMove:Z

    .line 97
    iput-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mUploadTarget:Ljava/lang/String;

    .line 98
    iput-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mUploadTargetFtp:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .line 99
    iput-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mUploadSrc:Ljava/lang/String;

    .line 100
    invoke-direct {p0, p3, p4, p5}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->getFetchingDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFetchingDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    .line 101
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mAbort:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mTransferring:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;)Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;[Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->publishProgress([Ljava/lang/Object;)V

    return-void
.end method

.method private getFetchingDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;
    .locals 2
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "subtitle"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;

    .prologue
    .line 105
    new-instance v0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;-><init>()V

    .line 107
    .local v0, "dialog":Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;
    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->setTitle(Ljava/lang/String;)V

    .line 109
    invoke-virtual {v0, p2}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->setSubtitle(Ljava/lang/String;)V

    .line 111
    invoke-virtual {v0, p3}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->setText(Ljava/lang/String;)V

    .line 113
    new-instance v1, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation$1;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->setAbortAction(Ljava/lang/Runnable;)V

    .line 130
    return-object v0
.end method

.method private isFile(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)Z
    .locals 1
    .param p1, "item"    # Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .prologue
    .line 244
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isFile()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isFolder(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)Z
    .locals 1
    .param p1, "item"    # Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .prologue
    .line 240
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isSymbolicLink()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private makeDirectory(Ljava/lang/String;)Z
    .locals 4
    .param p1, "targetDir"    # Ljava/lang/String;

    .prologue
    .line 316
    const-string v1, "MyFiles"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AbsFTPSupplier makeDirectory: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 318
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 319
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 320
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    .line 325
    .end local v0    # "dir":Ljava/io/File;
    :goto_0
    return v1

    .line 322
    .restart local v0    # "dir":Ljava/io/File;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0

    .line 325
    .end local v0    # "dir":Ljava/io/File;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private setFilename(Ljava/lang/String;)V
    .locals 2
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 248
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFetchingDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation$3;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation$3;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 256
    :cond_0
    return-void
.end method

.method private uploadFromLocal([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 10
    .param p1, "folders"    # [Ljava/lang/String;
    .param p2, "files"    # [Ljava/lang/String;
    .param p3, "target"    # Ljava/lang/String;
    .param p4, "src"    # Ljava/lang/String;
    .param p5, "move"    # Z

    .prologue
    .line 290
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 291
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mOperationArg:Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    if-eqz p5, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0021

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_0
    const-string v4, ""

    const/4 v8, 0x0

    move-object v5, p3

    move-object v6, p4

    move v7, p5

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/FTPParams;)V

    .line 294
    .local v0, "uo":Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 295
    .local v9, "arg":Landroid/os/Bundle;
    const-string v1, "recur_disc_folders"

    invoke-virtual {v9, v1, p1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 296
    const-string v1, "recur_disc_files"

    invoke-virtual {v9, v1, p2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 297
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/os/Bundle;

    const/4 v3, 0x0

    aput-object v9, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 299
    .end local v0    # "uo":Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;
    .end local v9    # "arg":Landroid/os/Bundle;
    :cond_0
    return-void

    .line 291
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0022

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private uploadFromLocalToFtp(Lcom/sec/android/app/myfiles/ftp/FTPParams;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 10
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p2, "folders"    # [Ljava/lang/String;
    .param p3, "files"    # [Ljava/lang/String;
    .param p4, "target"    # Ljava/lang/String;
    .param p5, "src"    # Ljava/lang/String;
    .param p6, "move"    # Z

    .prologue
    .line 303
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 304
    const-string v1, "MyFiles"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AbsFTPSupplier uploadFromLocalToFtp target: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mOperationArg:Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    if-eqz p6, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0021

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_0
    const-string v4, ""

    move-object v5, p4

    move-object v6, p5

    move/from16 v7, p6

    move-object v8, p1

    invoke-direct/range {v0 .. v8}, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/FTPParams;)V

    .line 308
    .local v0, "uo":Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 309
    .local v9, "arg":Landroid/os/Bundle;
    const-string v1, "recur_disc_folders"

    invoke-virtual {v9, v1, p2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 310
    const-string v1, "recur_disc_files"

    invoke-virtual {v9, v1, p3}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 311
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/os/Bundle;

    const/4 v3, 0x0

    aput-object v9, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 313
    .end local v0    # "uo":Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;
    .end local v9    # "arg":Landroid/os/Bundle;
    :cond_0
    return-void

    .line 305
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b0022

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/sec/android/app/myfiles/ftp/DownloadItem;)Ljava/lang/Boolean;
    .locals 29
    .param p1, "params"    # [Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    .prologue
    .line 136
    const/16 v21, 0x0

    .line 137
    .local v21, "success":Z
    const/16 v20, 0x0

    .line 138
    .local v20, "succeeded":I
    if-nez p1, :cond_0

    .line 139
    const/16 v24, 0x0

    invoke-static/range {v24 .. v24}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v24

    .line 236
    :goto_0
    return-object v24

    .line 142
    :cond_0
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mDownloadItems:[Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    .line 143
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mDialogSync:Ljava/lang/Object;

    move-object/from16 v25, v0

    monitor-enter v25

    .line 145
    if-eqz p1, :cond_1

    :try_start_0
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v24, v0

    if-lez v24, :cond_1

    .line 146
    const/16 v24, 0x0

    aget-object v24, p1, v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mDownloadItem:Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    .line 148
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    check-cast v24, Landroid/app/Activity;

    invoke-virtual/range {v24 .. v24}, Landroid/app/Activity;->isResumed()Z

    move-result v24

    if-nez v24, :cond_3

    .line 149
    const/16 v24, 0x0

    invoke-static/range {v24 .. v24}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v24

    .line 231
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFetchingDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->isAdded()Z

    move-result v26

    if-eqz v26, :cond_2

    .line 232
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFetchingDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->dismissAllowingStateLoss()V

    :cond_2
    monitor-exit v25

    goto :goto_0

    .line 235
    :catchall_0
    move-exception v24

    monitor-exit v25
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v24

    .line 151
    :cond_3
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncPrepareConnection()Z

    move-result v24

    if-nez v24, :cond_5

    .line 152
    const/16 v24, 0x0

    invoke-static/range {v24 .. v24}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v24

    .line 231
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFetchingDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->isAdded()Z

    move-result v26

    if-eqz v26, :cond_4

    .line 232
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFetchingDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->dismissAllowingStateLoss()V

    :cond_4
    monitor-exit v25
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 155
    :cond_5
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFetchingDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    check-cast v24, Landroid/app/Activity;

    invoke-virtual/range {v24 .. v24}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v24

    const-string v27, "fetching_dialog"

    move-object/from16 v0, v26

    move-object/from16 v1, v24

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 160
    :try_start_5
    new-instance v17, Ljava/util/ArrayDeque;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayDeque;-><init>()V

    .line 161
    .local v17, "jobFolder":Ljava/util/ArrayDeque;, "Ljava/util/ArrayDeque<Lcom/sec/android/app/myfiles/ftp/DownloadItem;>;"
    new-instance v11, Ljava/util/Stack;

    invoke-direct {v11}, Ljava/util/Stack;-><init>()V

    .line 162
    .local v11, "folders":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/String;>;"
    new-instance v16, Ljava/util/ArrayDeque;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayDeque;-><init>()V

    .line 163
    .local v16, "jobFile":Ljava/util/ArrayDeque;, "Ljava/util/ArrayDeque<Lcom/sec/android/app/myfiles/ftp/DownloadItem;>;"
    new-instance v10, Ljava/util/Stack;

    invoke-direct {v10}, Ljava/util/Stack;-><init>()V

    .line 164
    .local v10, "files":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/String;>;"
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    .line 165
    .local v19, "sb":Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    const v26, 0x7f0b00b0

    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 166
    .local v8, "discovering":Ljava/lang/String;
    move-object/from16 v3, p1

    .local v3, "arr$":[Lcom/sec/android/app/myfiles/ftp/DownloadItem;
    array-length v0, v3

    move/from16 v18, v0

    .local v18, "len$":I
    const/4 v13, 0x0

    .local v13, "i$":I
    move v14, v13

    .end local v13    # "i$":I
    .local v14, "i$":I
    :goto_1
    move/from16 v0, v18

    if-ge v14, v0, :cond_6

    aget-object v12, v3, v14

    .line 167
    .local v12, "i":Lcom/sec/android/app/myfiles/ftp/DownloadItem;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mDownloadingPaths:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    invoke-virtual {v12}, Lcom/sec/android/app/myfiles/ftp/DownloadItem;->getFtpItem()Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-result-object v26

    invoke-interface/range {v26 .. v26}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 168
    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mDownloadItem:Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    .line 169
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mAbort:Z

    move/from16 v24, v0

    if-eqz v24, :cond_b

    .line 205
    .end local v12    # "i":Lcom/sec/android/app/myfiles/ftp/DownloadItem;
    :cond_6
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayDeque;->size()I

    move-result v23

    .line 206
    .local v23, "total":I
    const/4 v12, 0x0

    .line 207
    .local v12, "i":I
    invoke-virtual {v10}, Ljava/util/Stack;->size()I

    move-result v24

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v10, v0}, Ljava/util/Stack;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v24

    check-cast v24, [Ljava/lang/String;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFiles:[Ljava/lang/String;

    .line 208
    invoke-virtual {v11}, Ljava/util/Stack;->size()I

    move-result v24

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v11, v0}, Ljava/util/Stack;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v24

    check-cast v24, [Ljava/lang/String;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFolders:[Ljava/lang/String;

    .line 209
    :cond_7
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mAbort:Z

    move/from16 v24, v0

    if-nez v24, :cond_11

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayDeque;->size()I

    move-result v24

    if-lez v24, :cond_11

    .line 210
    add-int/lit8 v12, v12, 0x1

    move v6, v12

    .line 211
    .local v6, "current":I
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayDeque;->pop()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mDownloadItem:Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    .line 212
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mDownloadItem:Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/myfiles/ftp/DownloadItem;->getFtpItem()Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-result-object v15

    .line 213
    .local v15, "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mDownloadItem:Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/myfiles/ftp/DownloadItem;->getTarget()Ljava/lang/String;

    move-result-object v22

    .line 214
    .local v22, "target":Ljava/lang/String;
    invoke-interface {v15}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isFile()Z

    move-result v24

    if-eqz v24, :cond_7

    .line 215
    invoke-interface {v15}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getTitle()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->setFilename(Ljava/lang/String;)V

    .line 216
    const/16 v24, 0x1

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mTransferring:Z

    .line 217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    move-object/from16 v24, v0

    new-instance v26, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation$2;

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    move/from16 v2, v23

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation$2;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;II)V

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    move-object/from16 v2, v26

    invoke-interface {v0, v15, v1, v2}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncDownloadFileToLocal(Lcom/sec/android/app/myfiles/ftp/IFTPItem;Ljava/lang/String;Lcom/sec/android/app/myfiles/openoperation/IProgressUpdatable;)Z

    move-result v24

    if-eqz v24, :cond_8

    .line 224
    add-int/lit8 v20, v20, 0x1

    .line 226
    :cond_8
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mTransferring:Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_2

    .line 231
    .end local v3    # "arr$":[Lcom/sec/android/app/myfiles/ftp/DownloadItem;
    .end local v6    # "current":I
    .end local v8    # "discovering":Ljava/lang/String;
    .end local v10    # "files":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/String;>;"
    .end local v11    # "folders":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/String;>;"
    .end local v12    # "i":I
    .end local v14    # "i$":I
    .end local v15    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .end local v16    # "jobFile":Ljava/util/ArrayDeque;, "Ljava/util/ArrayDeque<Lcom/sec/android/app/myfiles/ftp/DownloadItem;>;"
    .end local v17    # "jobFolder":Ljava/util/ArrayDeque;, "Ljava/util/ArrayDeque<Lcom/sec/android/app/myfiles/ftp/DownloadItem;>;"
    .end local v18    # "len$":I
    .end local v19    # "sb":Ljava/lang/StringBuilder;
    .end local v22    # "target":Ljava/lang/String;
    .end local v23    # "total":I
    :catchall_1
    move-exception v24

    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFetchingDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->isAdded()Z

    move-result v26

    if-eqz v26, :cond_9

    .line 232
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFetchingDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->dismissAllowingStateLoss()V

    :cond_9
    throw v24
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 156
    :catch_0
    move-exception v9

    .line 157
    .local v9, "e":Ljava/lang/Exception;
    const/16 v24, 0x0

    :try_start_7
    sget-object v26, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->MODULE:Ljava/lang/String;

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "show has not been executed Exception: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual {v9}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move/from16 v0, v24

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 158
    const/16 v24, 0x0

    invoke-static/range {v24 .. v24}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-result-object v24

    .line 231
    :try_start_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFetchingDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->isAdded()Z

    move-result v26

    if-eqz v26, :cond_a

    .line 232
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFetchingDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->dismissAllowingStateLoss()V

    :cond_a
    monitor-exit v25
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0

    .line 172
    .end local v9    # "e":Ljava/lang/Exception;
    .restart local v3    # "arr$":[Lcom/sec/android/app/myfiles/ftp/DownloadItem;
    .restart local v8    # "discovering":Ljava/lang/String;
    .restart local v10    # "files":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/String;>;"
    .restart local v11    # "folders":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/String;>;"
    .local v12, "i":Lcom/sec/android/app/myfiles/ftp/DownloadItem;
    .restart local v14    # "i$":I
    .restart local v16    # "jobFile":Ljava/util/ArrayDeque;, "Ljava/util/ArrayDeque<Lcom/sec/android/app/myfiles/ftp/DownloadItem;>;"
    .restart local v17    # "jobFolder":Ljava/util/ArrayDeque;, "Ljava/util/ArrayDeque<Lcom/sec/android/app/myfiles/ftp/DownloadItem;>;"
    .restart local v18    # "len$":I
    .restart local v19    # "sb":Ljava/lang/StringBuilder;
    :cond_b
    :try_start_9
    invoke-virtual {v12}, Lcom/sec/android/app/myfiles/ftp/DownloadItem;->getFtpItem()Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-result-object v24

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->isFolder(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)Z

    move-result v24

    if-eqz v24, :cond_e

    .line 173
    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 174
    invoke-virtual {v12}, Lcom/sec/android/app/myfiles/ftp/DownloadItem;->getTarget()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v11, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    .end local v14    # "i$":I
    :cond_c
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mAbort:Z

    move/from16 v24, v0

    if-nez v24, :cond_10

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayDeque;->size()I

    move-result v24

    if-lez v24, :cond_10

    .line 180
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayDeque;->pop()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    .line 181
    .local v7, "currentItem":Lcom/sec/android/app/myfiles/ftp/DownloadItem;
    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/ftp/DownloadItem;->getFtpItem()Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-result-object v15

    .line 182
    .restart local v15    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/ftp/DownloadItem;->getTarget()Ljava/lang/String;

    move-result-object v22

    .line 183
    .restart local v22    # "target":Ljava/lang/String;
    if-eqz v15, :cond_c

    if-eqz v22, :cond_c

    .line 184
    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->isFolder(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)Z

    move-result v24

    if-eqz v24, :cond_c

    .line 185
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->makeDirectory(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_c

    .line 186
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v24

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-interface {v15}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getTitle()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->setFilename(Ljava/lang/String;)V

    .line 187
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-interface {v0, v15}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncBrowseForItemContent(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)Ljava/util/List;

    move-result-object v5

    .line 188
    .local v5, "children":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    if-eqz v5, :cond_c

    .line 189
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :cond_d
    :goto_4
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v24

    if-eqz v24, :cond_c

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .line 190
    .local v4, "child":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    const/16 v24, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v24, "/"

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v4}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getTitle()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->isFolder(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)Z

    move-result v24

    if-eqz v24, :cond_f

    .line 192
    new-instance v24, Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-direct {v0, v4, v1}, Lcom/sec/android/app/myfiles/ftp/DownloadItem;-><init>(Lcom/sec/android/app/myfiles/ftp/IFTPItem;Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 193
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v11, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 175
    .end local v4    # "child":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .end local v5    # "children":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    .end local v7    # "currentItem":Lcom/sec/android/app/myfiles/ftp/DownloadItem;
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v15    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .end local v22    # "target":Ljava/lang/String;
    .restart local v14    # "i$":I
    :cond_e
    invoke-virtual {v12}, Lcom/sec/android/app/myfiles/ftp/DownloadItem;->getFtpItem()Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-result-object v24

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->isFile(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)Z

    move-result v24

    if-eqz v24, :cond_c

    .line 176
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 177
    invoke-virtual {v12}, Lcom/sec/android/app/myfiles/ftp/DownloadItem;->getTarget()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 194
    .end local v14    # "i$":I
    .restart local v4    # "child":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .restart local v5    # "children":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    .restart local v7    # "currentItem":Lcom/sec/android/app/myfiles/ftp/DownloadItem;
    .restart local v13    # "i$":Ljava/util/Iterator;
    .restart local v15    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .restart local v22    # "target":Ljava/lang/String;
    :cond_f
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->isFile(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)Z

    move-result v24

    if-eqz v24, :cond_d

    .line 195
    new-instance v24, Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-direct {v0, v4, v1}, Lcom/sec/android/app/myfiles/ftp/DownloadItem;-><init>(Lcom/sec/android/app/myfiles/ftp/IFTPItem;Ljava/lang/String;)V

    move-object/from16 v0, v16

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 196
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v10, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_4

    .line 166
    .end local v4    # "child":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .end local v5    # "children":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    .end local v7    # "currentItem":Lcom/sec/android/app/myfiles/ftp/DownloadItem;
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v15    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .end local v22    # "target":Ljava/lang/String;
    :cond_10
    add-int/lit8 v13, v14, 0x1

    .local v13, "i$":I
    move v14, v13

    .end local v13    # "i$":I
    .restart local v14    # "i$":I
    goto/16 :goto_1

    .line 229
    .local v12, "i":I
    .restart local v23    # "total":I
    :cond_11
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mAbort:Z

    move/from16 v24, v0

    if-nez v24, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFiles:[Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    array-length v0, v0

    move/from16 v24, v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move/from16 v0, v20

    move/from16 v1, v24

    if-ne v0, v1, :cond_13

    const/16 v21, 0x1

    .line 231
    :goto_5
    :try_start_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFetchingDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->isAdded()Z

    move-result v24

    if-eqz v24, :cond_12

    .line 232
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFetchingDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->dismissAllowingStateLoss()V

    .line 235
    :cond_12
    monitor-exit v25
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 236
    invoke-static/range {v21 .. v21}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v24

    goto/16 :goto_0

    .line 229
    :cond_13
    const/16 v21, 0x0

    goto :goto_5
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 46
    check-cast p1, [Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->doInBackground([Lcom/sec/android/app/myfiles/ftp/DownloadItem;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 9
    .param p1, "success"    # Ljava/lang/Boolean;

    .prologue
    .line 270
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mUploadTarget:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mUploadSrc:Ljava/lang/String;

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mDownloadItem:Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    if-eqz v0, :cond_2

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mDownloadingPaths:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mDownloadingPaths:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mDownloadItem:Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/ftp/DownloadItem;->getTarget()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v4}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->getFtpParams()Lcom/sec/android/app/myfiles/ftp/FTPParams;

    move-result-object v4

    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mMove:Z

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;->onDownloadCompleted(Z[Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/FTPParams;Z)V

    .line 287
    :cond_1
    :goto_0
    return-void

    .line 275
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mUploadSrc:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mUploadTargetFtp:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    if-eqz v0, :cond_1

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mUploadTargetFtp:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFolders:[Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFiles:[Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFolders:[Ljava/lang/String;

    array-length v0, v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFiles:[Ljava/lang/String;

    array-length v1, v1

    add-int/2addr v0, v1

    if-lez v0, :cond_5

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mUploadTarget:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_3

    iget-object v8, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mUploadTarget:Ljava/lang/String;

    .line 278
    .local v8, "replaceStr":Ljava/lang/String;
    :goto_1
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFolders:[Ljava/lang/String;

    array-length v0, v0

    if-ge v7, v0, :cond_4

    .line 279
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFolders:[Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFolders:[Ljava/lang/String;

    aget-object v1, v1, v7

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mUploadSrc:Ljava/lang/String;

    invoke-virtual {v1, v2, v8}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    .line 278
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 277
    .end local v7    # "i":I
    .end local v8    # "replaceStr":Ljava/lang/String;
    :cond_3
    const-string v8, ""

    goto :goto_1

    .line 281
    .restart local v7    # "i":I
    .restart local v8    # "replaceStr":Ljava/lang/String;
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFolders:[Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFiles:[Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mUploadTarget:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mUploadSrc:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mMove:Z

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->uploadFromLocal([Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 282
    .end local v7    # "i":I
    .end local v8    # "replaceStr":Ljava/lang/String;
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mUploadTargetFtp:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    if-eqz v0, :cond_1

    .line 283
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mUploadTargetFtp:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFolders:[Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFiles:[Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mUploadTarget:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mUploadSrc:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mMove:Z

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->uploadFromLocalToFtp(Lcom/sec/android/app/myfiles/ftp/FTPParams;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 46
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 4
    .param p1, "values"    # [Ljava/lang/Integer;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 260
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 261
    array-length v0, p1

    if-ne v0, v3, :cond_1

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFetchingDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    aget-object v1, p1, v2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->updateProgress(I)V

    .line 266
    :cond_0
    :goto_0
    return-void

    .line 263
    :cond_1
    array-length v0, p1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 264
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFetchingDialog:Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    aget-object v1, p1, v2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aget-object v2, p1, v3

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x2

    aget-object v3, p1, v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->updateProgress(III)V

    goto :goto_0
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 46
    check-cast p1, [Ljava/lang/Integer;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
