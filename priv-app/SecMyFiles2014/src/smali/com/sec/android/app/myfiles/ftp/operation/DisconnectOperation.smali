.class public Lcom/sec/android/app/myfiles/ftp/operation/DisconnectOperation;
.super Landroid/os/AsyncTask;
.source "DisconnectOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final mDialogSync:Ljava/lang/Object;

.field private final mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

.field private final mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 1
    .param p1, "arg"    # Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;
    .param p2, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 26
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->getDialogSync()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DisconnectOperation;->mDialogSync:Ljava/lang/Object;

    .line 27
    iput-object p2, p0, Lcom/sec/android/app/myfiles/ftp/operation/DisconnectOperation;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    .line 28
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/operation/FTPOperationArg;->getFTPConnectionHandler()Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DisconnectOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    .line 29
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 3
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 33
    const/4 v0, 0x0

    .line 34
    .local v0, "success":Z
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/DisconnectOperation;->mDialogSync:Ljava/lang/Object;

    monitor-enter v2

    .line 35
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/DisconnectOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v1}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncLogOut()Z

    move-result v0

    .line 36
    if-eqz v0, :cond_0

    .line 37
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/DisconnectOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v1}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncDisconnect()Z

    move-result v0

    .line 39
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 40
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    return-object v1

    .line 39
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 24
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/DisconnectOperation;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "success"    # Ljava/lang/Boolean;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DisconnectOperation;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;->onCloseConnection(Z)V

    .line 46
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 24
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/DisconnectOperation;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
