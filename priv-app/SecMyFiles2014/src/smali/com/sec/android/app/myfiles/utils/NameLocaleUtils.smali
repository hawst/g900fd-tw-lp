.class public Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;
.super Ljava/lang/Object;
.source "NameLocaleUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/utils/NameLocaleUtils$1;,
        Lcom/sec/android/app/myfiles/utils/NameLocaleUtils$ChineseNameUtils;,
        Lcom/sec/android/app/myfiles/utils/NameLocaleUtils$FullNameStyle;,
        Lcom/sec/android/app/myfiles/utils/NameLocaleUtils$NameLocaleUtilsBase;
    }
.end annotation


# static fields
.field private static final CHINESE_LANGUAGE:Ljava/lang/String;

.field private static final DEBUG:Z = false

.field private static final JAPANESE_LANGUAGE:Ljava/lang/String;

.field private static final KOREAN_LANGUAGE:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "NameLocaleUtils"

.field private static sSingleton:Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;


# instance fields
.field private mBase:Lcom/sec/android/app/myfiles/utils/NameLocaleUtils$NameLocaleUtilsBase;

.field private mLanguage:Ljava/lang/String;

.field private mUtils:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/android/app/myfiles/utils/NameLocaleUtils$NameLocaleUtilsBase;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 152
    sget-object v0, Ljava/util/Locale;->CHINESE:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->CHINESE_LANGUAGE:Ljava/lang/String;

    .line 154
    sget-object v0, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->JAPANESE_LANGUAGE:Ljava/lang/String;

    .line 156
    sget-object v0, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->KOREAN_LANGUAGE:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->mUtils:Ljava/util/HashMap;

    .line 162
    new-instance v0, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils$NameLocaleUtilsBase;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils$NameLocaleUtilsBase;-><init>(Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->mBase:Lcom/sec/android/app/myfiles/utils/NameLocaleUtils$NameLocaleUtilsBase;

    .line 167
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->setLocale(Ljava/util/Locale;)V

    .line 168
    return-void
.end method

.method private declared-synchronized get(Ljava/lang/Integer;)Lcom/sec/android/app/myfiles/utils/NameLocaleUtils$NameLocaleUtilsBase;
    .locals 3
    .param p1, "nameStyle"    # Ljava/lang/Integer;

    .prologue
    .line 205
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->mUtils:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils$NameLocaleUtilsBase;

    .line 206
    .local v0, "utils":Lcom/sec/android/app/myfiles/utils/NameLocaleUtils$NameLocaleUtilsBase;
    if-nez v0, :cond_0

    .line 207
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 208
    new-instance v0, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils$ChineseNameUtils;

    .end local v0    # "utils":Lcom/sec/android/app/myfiles/utils/NameLocaleUtils$NameLocaleUtilsBase;
    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils$ChineseNameUtils;-><init>(Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;Lcom/sec/android/app/myfiles/utils/NameLocaleUtils$1;)V

    .line 209
    .restart local v0    # "utils":Lcom/sec/android/app/myfiles/utils/NameLocaleUtils$NameLocaleUtilsBase;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->mUtils:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    :cond_0
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->mBase:Lcom/sec/android/app/myfiles/utils/NameLocaleUtils$NameLocaleUtilsBase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .end local v0    # "utils":Lcom/sec/android/app/myfiles/utils/NameLocaleUtils$NameLocaleUtilsBase;
    :cond_1
    monitor-exit p0

    return-object v0

    .line 205
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private getAdjustedStyle(I)I
    .locals 2
    .param p1, "nameStyle"    # I

    .prologue
    .line 233
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    sget-object v0, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->JAPANESE_LANGUAGE:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->mLanguage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->KOREAN_LANGUAGE:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->mLanguage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 235
    const/4 p1, 0x3

    .line 237
    .end local p1    # "nameStyle":I
    :cond_0
    return p1
.end method

.method private getForNameLookup(Ljava/lang/Integer;)Lcom/sec/android/app/myfiles/utils/NameLocaleUtils$NameLocaleUtilsBase;
    .locals 4
    .param p1, "nameStyle"    # Ljava/lang/Integer;

    .prologue
    .line 196
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 197
    .local v1, "nameStyleInt":I
    invoke-direct {p0, v1}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->getAdjustedStyle(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 198
    .local v0, "adjustedUtil":Ljava/lang/Integer;
    sget-object v2, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->CHINESE_LANGUAGE:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->mLanguage:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 199
    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 201
    :cond_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->get(Ljava/lang/Integer;)Lcom/sec/android/app/myfiles/utils/NameLocaleUtils$NameLocaleUtilsBase;

    move-result-object v2

    return-object v2
.end method

.method private getForSort(Ljava/lang/Integer;)Lcom/sec/android/app/myfiles/utils/NameLocaleUtils$NameLocaleUtilsBase;
    .locals 1
    .param p1, "nameStyle"    # Ljava/lang/Integer;

    .prologue
    .line 222
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->getAdjustedStyle(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->get(Ljava/lang/Integer;)Lcom/sec/android/app/myfiles/utils/NameLocaleUtils$NameLocaleUtilsBase;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized getIntance()Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;
    .locals 2

    .prologue
    .line 226
    const-class v1, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->sSingleton:Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;

    if-nez v0, :cond_0

    .line 227
    new-instance v0, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->sSingleton:Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;

    .line 229
    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->sSingleton:Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 226
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private guessCJKNameStyle(Ljava/lang/String;I)I
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "offset"    # I

    .prologue
    .line 289
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 290
    .local v1, "length":I
    :goto_0
    if-ge p2, v1, :cond_2

    .line 291
    invoke-static {p1, p2}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v0

    .line 292
    .local v0, "codePoint":I
    invoke-static {v0}, Ljava/lang/Character;->isLetter(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 293
    invoke-static {v0}, Ljava/lang/Character$UnicodeBlock;->of(I)Ljava/lang/Character$UnicodeBlock;

    move-result-object v2

    .line 294
    .local v2, "unicodeBlock":Ljava/lang/Character$UnicodeBlock;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->isJapanesePhoneticUnicodeBlock(Ljava/lang/Character$UnicodeBlock;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 295
    const/4 v3, 0x4

    .line 304
    .end local v0    # "codePoint":I
    .end local v2    # "unicodeBlock":Ljava/lang/Character$UnicodeBlock;
    :goto_1
    return v3

    .line 297
    .restart local v0    # "codePoint":I
    .restart local v2    # "unicodeBlock":Ljava/lang/Character$UnicodeBlock;
    :cond_0
    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->isKoreanUnicodeBlock(Ljava/lang/Character$UnicodeBlock;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 298
    const/4 v3, 0x5

    goto :goto_1

    .line 301
    .end local v2    # "unicodeBlock":Ljava/lang/Character$UnicodeBlock;
    :cond_1
    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v3

    add-int/2addr p2, v3

    .line 302
    goto :goto_0

    .line 304
    .end local v0    # "codePoint":I
    :cond_2
    const/4 v3, 0x2

    goto :goto_1
.end method

.method private static isCJKUnicodeBlock(Ljava/lang/Character$UnicodeBlock;)Z
    .locals 1
    .param p0, "block"    # Ljava/lang/Character$UnicodeBlock;

    .prologue
    .line 316
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_UNIFIED_IDEOGRAPHS:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_SYMBOLS_AND_PUNCTUATION:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_RADICALS_SUPPLEMENT:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_COMPATIBILITY:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_COMPATIBILITY_FORMS:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_COMPATIBILITY_IDEOGRAPHS:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->CJK_COMPATIBILITY_IDEOGRAPHS_SUPPLEMENT:Ljava/lang/Character$UnicodeBlock;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isJapanesePhoneticUnicodeBlock(Ljava/lang/Character$UnicodeBlock;)Z
    .locals 1
    .param p0, "unicodeBlock"    # Ljava/lang/Character$UnicodeBlock;

    .prologue
    .line 334
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->KATAKANA:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->KATAKANA_PHONETIC_EXTENSIONS:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->HALFWIDTH_AND_FULLWIDTH_FORMS:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->HIRAGANA:Ljava/lang/Character$UnicodeBlock;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isKoreanUnicodeBlock(Ljava/lang/Character$UnicodeBlock;)Z
    .locals 1
    .param p0, "unicodeBlock"    # Ljava/lang/Character$UnicodeBlock;

    .prologue
    .line 328
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->HANGUL_SYLLABLES:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->HANGUL_JAMO:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->HANGUL_COMPATIBILITY_JAMO:Ljava/lang/Character$UnicodeBlock;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isLatinUnicodeBlock(Ljava/lang/Character$UnicodeBlock;)Z
    .locals 1
    .param p0, "unicodeBlock"    # Ljava/lang/Character$UnicodeBlock;

    .prologue
    .line 308
    sget-object v0, Ljava/lang/Character$UnicodeBlock;->BASIC_LATIN:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->LATIN_1_SUPPLEMENT:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->LATIN_EXTENDED_A:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->LATIN_EXTENDED_B:Ljava/lang/Character$UnicodeBlock;

    if-eq p0, v0, :cond_0

    sget-object v0, Ljava/lang/Character$UnicodeBlock;->LATIN_EXTENDED_ADDITIONAL:Ljava/lang/Character$UnicodeBlock;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getNameLookupKeys(Ljava/lang/String;I)Ljava/util/Iterator;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "nameStyle"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/Iterator",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 184
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->getForNameLookup(Ljava/lang/Integer;)Lcom/sec/android/app/myfiles/utils/NameLocaleUtils$NameLocaleUtilsBase;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils$NameLocaleUtilsBase;->getNameLookupKeys(Ljava/lang/String;)Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public getSortKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "displayName"    # Ljava/lang/String;

    .prologue
    .line 180
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->guessNameStyle(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->getForSort(Ljava/lang/Integer;)Lcom/sec/android/app/myfiles/utils/NameLocaleUtils$NameLocaleUtilsBase;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils$NameLocaleUtilsBase;->getSortKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public guessNameStyle(Ljava/lang/String;)I
    .locals 6
    .param p1, "displayName"    # Ljava/lang/String;

    .prologue
    .line 242
    if-nez p1, :cond_1

    .line 243
    const/4 v2, 0x0

    .line 285
    :cond_0
    :goto_0
    return v2

    .line 246
    :cond_1
    const/4 v2, 0x0

    .line 247
    .local v2, "nameStyle":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    .line 248
    .local v1, "length":I
    const/4 v3, 0x0

    .line 250
    .local v3, "offset":I
    :goto_1
    if-ge v3, v1, :cond_0

    .line 251
    invoke-static {p1, v3}, Ljava/lang/Character;->codePointAt(Ljava/lang/CharSequence;I)I

    move-result v0

    .line 252
    .local v0, "codePoint":I
    invoke-static {v0}, Ljava/lang/Character;->isLetter(I)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 253
    invoke-static {v0}, Ljava/lang/Character$UnicodeBlock;->of(I)Ljava/lang/Character$UnicodeBlock;

    move-result-object v4

    .line 255
    .local v4, "unicodeBlock":Ljava/lang/Character$UnicodeBlock;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->isLatinUnicodeBlock(Ljava/lang/Character$UnicodeBlock;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 256
    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->isCJKUnicodeBlock(Ljava/lang/Character$UnicodeBlock;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 261
    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v5

    add-int/2addr v5, v3

    invoke-direct {p0, p1, v5}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->guessCJKNameStyle(Ljava/lang/String;I)I

    move-result v2

    goto :goto_0

    .line 265
    :cond_2
    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->isJapanesePhoneticUnicodeBlock(Ljava/lang/Character$UnicodeBlock;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 268
    const/4 v2, 0x4

    goto :goto_0

    .line 271
    :cond_3
    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->isKoreanUnicodeBlock(Ljava/lang/Character$UnicodeBlock;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 274
    const/4 v2, 0x5

    goto :goto_0

    .line 277
    :cond_4
    const/4 v2, 0x1

    .line 279
    .end local v4    # "unicodeBlock":Ljava/lang/Character$UnicodeBlock;
    :cond_5
    invoke-static {v0}, Ljava/lang/Character;->charCount(I)I

    move-result v5

    add-int/2addr v3, v5

    .line 280
    goto :goto_1
.end method

.method public setLocale(Ljava/util/Locale;)V
    .locals 1
    .param p1, "currentLocale"    # Ljava/util/Locale;

    .prologue
    .line 171
    if-nez p1, :cond_0

    .line 172
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->mLanguage:Ljava/lang/String;

    .line 176
    :goto_0
    return-void

    .line 174
    :cond_0
    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/utils/NameLocaleUtils;->mLanguage:Ljava/lang/String;

    goto :goto_0
.end method
