.class public Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;
.source "AddFTPSFragment.java"


# instance fields
.field private mEncodingView:Landroid/widget/Spinner;

.field private mPasswordView:Landroid/widget/TextView;

.field private mPortView:Landroid/widget/TextView;

.field private mServerView:Landroid/widget/TextView;

.field private mUsernameView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected checkData()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 60
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->mServerView:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->mPortView:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->mUsernameView:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->mPasswordView:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->mEncodingView:Landroid/widget/Spinner;

    if-eqz v1, :cond_0

    .line 63
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->mServerView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->checkServer(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->mPortView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->checkPort(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->mUsernameView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->checkUsername(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->mPasswordView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->checkPassword(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 68
    :cond_0
    return v0
.end method

.method protected gatherParameters()Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .locals 2

    .prologue
    .line 49
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;-><init>()V

    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPType;->FTPS:Lcom/sec/android/app/myfiles/ftp/FTPType;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->type(Lcom/sec/android/app/myfiles/ftp/FTPType;)Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->mServerView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->host(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->mPortView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->getPort(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->port(I)Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->mUsernameView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->getUsername(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->username(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->mPasswordView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->getPassword(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->password(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->mEncodingView:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->getEncoding(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->encoding(Lcom/sec/android/app/myfiles/ftp/FTPEncoding;)Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/ftp/FTPParams$Builder;->build()Lcom/sec/android/app/myfiles/ftp/FTPParams;

    move-result-object v0

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 34
    const v1, 0x7f04000d

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 36
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f0f001a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->mServerView:Landroid/widget/TextView;

    .line 37
    const v1, 0x7f0f001d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->mPortView:Landroid/widget/TextView;

    .line 38
    const v1, 0x7f0f0023

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->mUsernameView:Landroid/widget/TextView;

    .line 39
    const v1, 0x7f0f0026

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->mPasswordView:Landroid/widget/TextView;

    .line 40
    const v1, 0x7f0f002c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->mEncodingView:Landroid/widget/Spinner;

    .line 42
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->runRestore()V

    .line 44
    return-object v0
.end method

.method protected restoreState(Lcom/sec/android/app/myfiles/ftp/FTPParams;)V
    .locals 2
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .prologue
    .line 74
    if-eqz p1, :cond_4

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->mServerView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->mServerView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->mPortView:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->mPortView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getPort()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->mUsernameView:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->mUsernameView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getUsername()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->mPasswordView:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->mPasswordView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getPassword()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->mEncodingView:Landroid/widget/Spinner;

    if-eqz v0, :cond_4

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->mEncodingView:Landroid/widget/Spinner;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getEncoding()Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/AddFTPSFragment;->getEncodingPosition(Lcom/sec/android/app/myfiles/ftp/FTPEncoding;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 91
    :cond_4
    return-void
.end method
