.class Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment$4;
.super Landroid/os/AsyncTask;
.source "AsyncOpenOperationUiFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->onActivityCreated(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;)V
    .locals 0

    .prologue
    .line 288
    iput-object p1, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment$4;->this$0:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 288
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment$4;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 4
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 291
    iget-object v1, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment$4;->this$0:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->AsyncOpenExcute()V

    .line 293
    iget-object v1, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment$4;->this$0:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    # getter for: Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mSync:Ljava/lang/Object;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->access$100(Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 294
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment$4;->this$0:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->getCompletedCondition()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    .line 296
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment$4;->this$0:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    # getter for: Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mSync:Ljava/lang/Object;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->access$100(Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 297
    :catch_0
    move-exception v0

    .line 298
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    # getter for: Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->access$200()Ljava/lang/String;

    move-result-object v1

    const-string v3, "Wait has been interrupted."

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 301
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_0
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 303
    const/4 v1, 0x0

    return-object v1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 288
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment$4;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 308
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 309
    iget-object v0, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment$4;->this$0:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mParentFrgment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment$4;->this$0:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mParentFrgment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->AsyncOpenCompleted()V

    .line 311
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment$4;->this$0:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 312
    iget-object v0, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment$4;->this$0:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->dismissAllowingStateLoss()V

    .line 313
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment$4;->this$0:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->mWifiDisconnect:Z

    if-eqz v0, :cond_2

    .line 314
    iget-object v0, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment$4;->this$0:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/MainActivity;->setDefaultView()V

    .line 315
    iget-object v0, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment$4;->this$0:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment$4;->this$0:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    const v2, 0x7f0b0083

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 317
    :cond_2
    return-void
.end method
