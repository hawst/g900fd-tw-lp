.class public abstract Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
.super Landroid/widget/BaseAdapter;
.source "AbsBaseAdapter.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/adapter/ISelectModeAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;
    }
.end annotation


# instance fields
.field protected mCurrentInOrder:I

.field protected mCurrentSortBy:I

.field protected mDropListener:Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;

.field protected mIsDragMode:Z

.field protected mIsSelectMode:Z

.field private mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

.field protected mSelectedItemChangeListener:Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;

.field protected mSelectionType:I

.field protected mViewMode:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 110
    return-void
.end method


# virtual methods
.method public finishDrag()V
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;->mIsDragMode:Z

    .line 89
    return-void
.end method

.method public getListView()Lcom/sec/android/app/myfiles/view/MyFilesListView;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    return-object v0
.end method

.method public isDragMode()Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;->mIsDragMode:Z

    return v0
.end method

.method public onRefresh()V
    .locals 0

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;->notifyDataSetChanged()V

    .line 65
    return-void
.end method

.method public setListView(Lcom/sec/android/app/myfiles/view/MyFilesListView;)V
    .locals 0
    .param p1, "listView"    # Lcom/sec/android/app/myfiles/view/MyFilesListView;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    .line 71
    return-void
.end method

.method public setOnDropListener(Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;->mDropListener:Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;

    .line 101
    return-void
.end method

.method public setSelectedItemChangeListener(Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;->mSelectedItemChangeListener:Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;

    .line 51
    return-void
.end method

.method public setSortBy(II)V
    .locals 0
    .param p1, "sortBy"    # I
    .param p2, "inOrder"    # I

    .prologue
    .line 56
    iput p1, p0, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;->mCurrentSortBy:I

    .line 58
    iput p2, p0, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;->mCurrentInOrder:I

    .line 59
    return-void
.end method

.method public startDrag()V
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;->mIsDragMode:Z

    .line 83
    return-void
.end method
