.class public Lcom/sec/android/app/myfiles/fragment/NeedWifiFragment;
.super Landroid/app/DialogFragment;
.source "NeedWifiFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 76
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NeedWifiFragment;->dismiss()V

    .line 78
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 39
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NeedWifiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 41
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    const v4, 0x7f0b013a

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 43
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isChinaModel()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 44
    const v4, 0x7f0b0139

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 47
    :cond_0
    const v4, 0x7f0b0015

    new-instance v5, Lcom/sec/android/app/myfiles/fragment/NeedWifiFragment$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/myfiles/fragment/NeedWifiFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/NeedWifiFragment;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 55
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NeedWifiFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 56
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f040036

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 58
    .local v3, "view":Landroid/view/View;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isChinaModel()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 59
    const v4, 0x7f0f00ce

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 60
    .local v2, "noWifiText":Landroid/widget/TextView;
    const v4, 0x7f0b0085

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    .line 62
    .end local v2    # "noWifiText":Landroid/widget/TextView;
    :cond_1
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 63
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    return-object v4
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 69
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/NeedWifiFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const-string v1, "#1C8BA6"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 71
    return-void
.end method
