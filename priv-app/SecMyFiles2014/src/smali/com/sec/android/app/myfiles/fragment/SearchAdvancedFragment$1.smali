.class Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment$1;
.super Ljava/lang/Object;
.source "SearchAdvancedFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const-wide/16 v2, 0x0

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mDateSummary:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;

    const-string v1, ""

    iput-object v1, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mStartDate:Ljava/lang/String;

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;

    # setter for: Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mCurrentToDate:J
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->access$002(Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;J)J

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;

    # setter for: Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mCurrentFromDate:J
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->access$102(Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;J)J

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchToDate(J)V

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFromDate(J)V

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchAdvancedFragment;->mDateDelete:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 167
    return-void
.end method
