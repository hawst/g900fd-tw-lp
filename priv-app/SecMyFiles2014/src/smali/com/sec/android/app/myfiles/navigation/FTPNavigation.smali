.class public Lcom/sec/android/app/myfiles/navigation/FTPNavigation;
.super Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
.source "FTPNavigation.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/navigation/FTPNavigation$FtpAsyncOpenOperationFragment;,
        Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;
    }
.end annotation


# static fields
.field private static final MODULE:Ljava/lang/String;

.field private static final OPEN_OPERATION_TAG:Ljava/lang/String; = "OpenOperation"


# instance fields
.field protected final VISIBLE_ITEMS_OFFSET:I

.field protected mAsyncUi:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

.field protected final mBrowseHandler:Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;

.field protected final mCache:Lcom/sec/android/app/myfiles/ftp/FTPCache;

.field protected mCloseFragment:Ljava/lang/Runnable;

.field protected mCurrItemsIdProp:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;"
        }
    .end annotation
.end field

.field protected mCurrentItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;"
        }
    .end annotation
.end field

.field protected mCurrentPathItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;"
        }
    .end annotation
.end field

.field protected mEmptyCurrentFolder:Z

.field protected final mFileCache:Lcom/sec/android/app/myfiles/ftp/FtpFileCache;

.field protected mGoPrevious:Ljava/lang/Runnable;

.field protected final mHandlers:Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;

.field protected mItemsToOpen:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

.field protected mPreviousPath:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;"
        }
    .end annotation
.end field

.field protected mRootItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

.field protected mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1087
    const-class v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->MODULE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I

    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;-><init>(Landroid/content/Context;I)V

    .line 1063
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mEmptyCurrentFolder:Z

    .line 1071
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentItems:Ljava/util/List;

    .line 1072
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrItemsIdProp:Ljava/util/Map;

    .line 1073
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    .line 1074
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mItemsToOpen:Ljava/util/Set;

    .line 1077
    invoke-static {}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->getInstance()Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mHandlers:Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;

    .line 1079
    invoke-static {}, Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;->getInstance()Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mBrowseHandler:Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;

    .line 1085
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->VISIBLE_ITEMS_OFFSET:I

    .line 72
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPCache;

    invoke-direct {v0, p1}, Lcom/sec/android/app/myfiles/ftp/FTPCache;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCache:Lcom/sec/android/app/myfiles/ftp/FTPCache;

    .line 74
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FtpFileCache;

    invoke-direct {v0, p1}, Lcom/sec/android/app/myfiles/ftp/FtpFileCache;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mFileCache:Lcom/sec/android/app/myfiles/ftp/FtpFileCache;

    .line 76
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/navigation/FTPNavigation;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getPreviousPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->MODULE:Ljava/lang/String;

    return-object v0
.end method

.method public static deleteDir(Ljava/io/File;)V
    .locals 6
    .param p0, "dir"    # Ljava/io/File;

    .prologue
    .line 898
    if-eqz p0, :cond_2

    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 899
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 900
    .local v1, "chld":[Ljava/io/File;
    if-eqz v1, :cond_1

    .line 901
    move-object v0, v1

    .local v0, "arr$":[Ljava/io/File;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    .line 902
    .local v4, "temp":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 903
    invoke-static {v4}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->deleteDir(Ljava/io/File;)V

    .line 901
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 905
    :cond_0
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    goto :goto_1

    .line 909
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "temp":Ljava/io/File;
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    .line 911
    .end local v1    # "chld":[Ljava/io/File;
    :cond_2
    return-void
.end method

.method protected static getDefaultPath(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)Ljava/lang/String;
    .locals 2
    .param p0, "item"    # Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .prologue
    .line 1042
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/myfiles/utils/Constant;->DEFAULT_TEMP_DOWNLOAD_DIR:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p0}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getPreviousPath()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1025
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mPreviousPath:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mPreviousPath:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 1027
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mPreviousPath:Ljava/util/List;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mPreviousPath:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .line 1029
    .local v0, "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    if-eqz v0, :cond_0

    .line 1031
    invoke-interface {v0}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v1

    .line 1037
    .end local v0    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    goto :goto_0
.end method

.method protected static getTargetPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "original"    # Ljava/lang/String;
    .param p1, "targetFolder"    # Ljava/lang/String;

    .prologue
    .line 649
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 651
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 652
    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 653
    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 655
    :cond_0
    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 656
    .local v1, "split":[Ljava/lang/String;
    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    aget-object v2, v1, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 658
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static getTempPath(Lcom/sec/android/app/myfiles/ftp/IFTPItem;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "item"    # Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .param p1, "sessionName"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1046
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1047
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "/storage/sdcard0/Android/data/com.sec.android.app.myfiles/ftp/cache"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1048
    const-string v1, "/."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1049
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1050
    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1051
    invoke-interface {p0}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/myfiles/ftp/FTPUtils;->getParentDir(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1052
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1053
    invoke-interface {p0}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1054
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public AsyncOpen()V
    .locals 1

    .prologue
    .line 663
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->AsyncOpen(Z)V

    .line 664
    return-void
.end method

.method public AsyncOpen(Z)V
    .locals 1
    .param p1, "initial"    # Z

    .prologue
    .line 668
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->AsyncOpen(ZZ)V

    .line 670
    return-void
.end method

.method public AsyncOpen(ZZ)V
    .locals 9
    .param p1, "initial"    # Z
    .param p2, "doNotCheckConnection"    # Z

    .prologue
    const/4 v4, 0x1

    .line 674
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v1, :cond_0

    .line 676
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getCurrentItem()Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-result-object v2

    .line 678
    .local v2, "currentItem":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    .line 680
    .local v7, "fm":Landroid/app/FragmentManager;
    if-eqz v7, :cond_0

    .line 682
    new-instance v3, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$FtpAsyncOpenOperationFragment;

    invoke-direct {v3}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$FtpAsyncOpenOperationFragment;-><init>()V

    .line 684
    .local v3, "asyncUi":Lcom/sec/android/app/myfiles/navigation/FTPNavigation$FtpAsyncOpenOperationFragment;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$FtpAsyncOpenOperationFragment;->setParentFragment(Lcom/sec/android/app/myfiles/fragment/AbsFragment;)V

    .line 686
    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$FtpAsyncOpenOperationFragment;->setCancelButtonActive(Z)V

    .line 688
    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$FtpAsyncOpenOperationFragment;->setWaitOnCancel(Z)V

    .line 690
    new-instance v1, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$1;-><init>(Lcom/sec/android/app/myfiles/navigation/FTPNavigation;)V

    invoke-virtual {v3, v1}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$FtpAsyncOpenOperationFragment;->setCancelOperation(Ljava/lang/Runnable;)V

    .line 701
    new-instance v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;

    move-object v1, p0

    move v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;-><init>(Lcom/sec/android/app/myfiles/navigation/FTPNavigation;Lcom/sec/android/app/myfiles/ftp/IFTPItem;Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;ZZ)V

    .line 703
    .local v0, "asyncOpen":Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;
    invoke-virtual {v3, v0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation$FtpAsyncOpenOperationFragment;->setAsyncOpenRunnable(Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;)V

    .line 705
    iput-object v3, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mAsyncUi:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    .line 709
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mAsyncUi:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    const-string v4, "OpenOperation"

    invoke-virtual {v1, v7, v4}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 721
    .end local v0    # "asyncOpen":Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;
    .end local v2    # "currentItem":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .end local v3    # "asyncUi":Lcom/sec/android/app/myfiles/navigation/FTPNavigation$FtpAsyncOpenOperationFragment;
    .end local v7    # "fm":Landroid/app/FragmentManager;
    :cond_0
    :goto_0
    return-void

    .line 711
    .restart local v0    # "asyncOpen":Lcom/sec/android/app/myfiles/navigation/FTPNavigation$AsyncOpenRunnable;
    .restart local v2    # "currentItem":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .restart local v3    # "asyncUi":Lcom/sec/android/app/myfiles/navigation/FTPNavigation$FtpAsyncOpenOperationFragment;
    .restart local v7    # "fm":Landroid/app/FragmentManager;
    :catch_0
    move-exception v6

    .line 713
    .local v6, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    sget-object v4, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->MODULE:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "dialog has not been shown Exception: "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected abortCurrentOperation()V
    .locals 3

    .prologue
    .line 931
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    if-eqz v1, :cond_2

    .line 932
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->abortCurrentOperation()V

    .line 933
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mPreviousPath:Ljava/util/List;

    if-nez v1, :cond_3

    .line 934
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mGoPrevious:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    .line 935
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mGoPrevious:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 943
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    .line 944
    .local v0, "fr":Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 945
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    invoke-interface {v1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->restorePathToPrevious(Ljava/lang/String;)V

    .line 947
    :cond_1
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mAbortBrowsing:Z

    .line 949
    .end local v0    # "fr":Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;
    :cond_2
    return-void

    .line 938
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mGoPrevious:Ljava/lang/Runnable;

    if-eqz v1, :cond_4

    .line 939
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mGoPrevious:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 941
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->restorePreviousList()V

    goto :goto_0
.end method

.method public cacheFile(Lcom/sec/android/app/myfiles/ftp/FTPItem;)V
    .locals 3
    .param p1, "item"    # Lcom/sec/android/app/myfiles/ftp/FTPItem;

    .prologue
    .line 520
    if-eqz p1, :cond_0

    .line 522
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mFileCache:Lcom/sec/android/app/myfiles/ftp/FtpFileCache;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/FtpFileCache;->cacheFile(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;)V

    .line 526
    :cond_0
    return-void
.end method

.method protected cleanInternalStructure()V
    .locals 1

    .prologue
    .line 893
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 894
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 895
    return-void
.end method

.method public clearCacheDB()V
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCache:Lcom/sec/android/app/myfiles/ftp/FTPCache;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/ftp/FTPCache;->clearCache()V

    .line 377
    return-void
.end method

.method public clearCacheDBBuffer()V
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCache:Lcom/sec/android/app/myfiles/ftp/FTPCache;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/ftp/FTPCache;->clearBuffer()V

    .line 383
    return-void
.end method

.method public closeConnection()V
    .locals 2

    .prologue
    .line 887
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    if-eqz v0, :cond_0

    .line 888
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->closeConnection(Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    .line 890
    :cond_0
    return-void
.end method

.method public copyFromLocalToFtp([Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 1
    .param p1, "srcPaths"    # [Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    .prologue
    .line 393
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->uploadLocal([Ljava/lang/String;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    .line 395
    return-void
.end method

.method public createDirectory(Ljava/lang/String;)V
    .locals 1
    .param p1, "newDirPath"    # Ljava/lang/String;

    .prologue
    .line 617
    if-eqz p1, :cond_0

    .line 618
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->createDirectory(Ljava/lang/String;)V

    .line 620
    :cond_0
    return-void
.end method

.method public deleteFiles(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 609
    .local p1, "itemsToDelete":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 610
    .local v1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 611
    .local v2, "path":Ljava/lang/String;
    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getItemOfPath(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 613
    .end local v2    # "path":Ljava/lang/String;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->deleteFiles(Ljava/util/List;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    .line 614
    return-void
.end method

.method public downloadAndSelect(Ljava/lang/String;)V
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 562
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getItemOfPath(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-result-object v0

    .line 564
    .local v0, "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    if-eqz v0, :cond_0

    .line 566
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->getParams()Lcom/sec/android/app/myfiles/ftp/FTPParams;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v1

    .line 568
    .local v1, "sessionName":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mContext:Landroid/content/Context;

    invoke-static {v0, v1, v3}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getTempPath(Lcom/sec/android/app/myfiles/ftp/IFTPItem;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 570
    .local v2, "targetDir":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    invoke-virtual {v3, v0, v2}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->selectItem(Lcom/sec/android/app/myfiles/ftp/IFTPItem;Ljava/lang/String;)V

    .line 572
    .end local v1    # "sessionName":Ljava/lang/String;
    .end local v2    # "targetDir":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public downloadAndSelect(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 576
    .local p1, "paths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_2

    .line 578
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 580
    .local v2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 582
    .local v5, "targets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->getParams()Lcom/sec/android/app/myfiles/ftp/FTPParams;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v4

    .line 584
    .local v4, "sessionName":Ljava/lang/String;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 586
    .local v3, "path":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 588
    invoke-static {v3}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getItem(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPItem;

    move-result-object v1

    .line 590
    .local v1, "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    if-eqz v1, :cond_0

    .line 592
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 594
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mContext:Landroid/content/Context;

    invoke-static {v1, v4, v6}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getTempPath(Lcom/sec/android/app/myfiles/ftp/IFTPItem;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 602
    .end local v1    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .end local v3    # "path":Ljava/lang/String;
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    invoke-virtual {v6, v2, v5}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->selectItems(Ljava/util/List;Ljava/util/ArrayList;)V

    .line 606
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    .end local v4    # "sessionName":Ljava/lang/String;
    .end local v5    # "targets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    return-void
.end method

.method public downloadToDefault(Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 3
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    .prologue
    .line 504
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getItemOfPath(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-result-object v0

    .line 505
    .local v0, "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    if-eqz v0, :cond_0

    .line 506
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getDefaultPath(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2, p2}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->copyFromFtpToLocal(Lcom/sec/android/app/myfiles/ftp/IFTPItem;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    .line 508
    :cond_0
    return-void
.end method

.method public downloadToDefault(Ljava/util/List;Z)V
    .locals 1
    .param p2, "move"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 511
    .local p1, "filePath":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->downloadToDefault(Ljava/util/List;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    .line 512
    return-void
.end method

.method public downloadToDefault(Ljava/util/List;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 1
    .param p2, "move"    # Z
    .param p3, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z",
            "Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 515
    .local p1, "filePath":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Constant;->DEFAULT_TEMP_DOWNLOAD_DIR:Ljava/lang/String;

    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->downloadToDir(Ljava/util/List;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    .line 516
    return-void
.end method

.method public downloadToDir(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "targetDir"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    .prologue
    .line 479
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getItemOfPath(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-result-object v0

    .line 480
    .local v0, "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    if-eqz v0, :cond_0

    .line 481
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2, p3}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->copyFromFtpToLocal(Lcom/sec/android/app/myfiles/ftp/IFTPItem;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    .line 483
    :cond_0
    return-void
.end method

.method public downloadToDir(Ljava/util/List;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 5
    .param p2, "targetDir"    # Ljava/lang/String;
    .param p3, "move"    # Z
    .param p4, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 487
    .local p1, "filePaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 489
    .local v2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 490
    .local v3, "path":Ljava/lang/String;
    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getItemOfPath(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-result-object v1

    .line 491
    .local v1, "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    if-eqz v1, :cond_0

    .line 492
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 496
    .end local v1    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .end local v3    # "path":Ljava/lang/String;
    :cond_1
    if-eqz p3, :cond_2

    .line 497
    iget-object v4, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    invoke-virtual {v4, v2, p2, p4}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->moveFromFtpToLocal(Ljava/util/Collection;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    .line 501
    :goto_1
    return-void

    .line 499
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    invoke-virtual {v4, v2, p2, p4}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->copyFromFtpToLocal(Ljava/util/Collection;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    goto :goto_1
.end method

.method public getCurFolder()Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .locals 1

    .prologue
    .line 471
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getCurrentItem()Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-result-object v0

    return-object v0
.end method

.method public getCurFolderItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 467
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentItems:Ljava/util/List;

    return-object v0
.end method

.method public getCurItem(I)Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 459
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    if-ltz p1, :cond_0

    .line 460
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .line 462
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentFolder()Ljava/lang/String;
    .locals 4

    .prologue
    .line 954
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isNetworkOn(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 955
    const-string v3, ""

    .line 969
    :goto_0
    return-object v3

    .line 958
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 959
    .local v2, "sb":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .line 960
    .local v1, "s":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    invoke-interface {v1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isRoot()Z

    move-result v3

    if-nez v3, :cond_1

    .line 963
    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 964
    invoke-interface {v1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 966
    .end local v1    # "s":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-nez v3, :cond_3

    .line 967
    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 969
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public getCurrentItem()Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .locals 3

    .prologue
    .line 973
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 974
    .local v0, "size":I
    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    add-int/lit8 v2, v0, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected getCurrentPathString()[Ljava/lang/String;
    .locals 6

    .prologue
    .line 994
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 995
    .local v2, "size":I
    new-array v1, v2, [Ljava/lang/String;

    .line 996
    .local v1, "names":[Ljava/lang/String;
    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getRootName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    .line 997
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 998
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    invoke-interface {v3}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getTitle()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x2f

    const/16 v5, 0x5c

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    .line 997
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1000
    :cond_0
    return-object v1
.end method

.method public getFileCount()I
    .locals 1

    .prologue
    .line 475
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getFilesInCurrentFolder()Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 298
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v5

    .line 300
    .local v5, "currentPath":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->isEnableFiltering()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 302
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 304
    .local v7, "exts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 306
    .local v6, "ext":Ljava/lang/String;
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 324
    .end local v6    # "ext":Ljava/lang/String;
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mEmptyCurrentFolder:Z

    if-nez v1, :cond_1

    .line 326
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCache:Lcom/sec/android/app/myfiles/ftp/FTPCache;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mFilteringExtension:Ljava/util/HashSet;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentSortBy:I

    iget v3, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentInOrder:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/myfiles/ftp/FTPCache;->getBufferContent(Ljava/util/ArrayList;IILcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 358
    .end local v7    # "exts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v8    # "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    return-object v0

    .line 352
    :cond_2
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mEmptyCurrentFolder:Z

    if-nez v1, :cond_1

    .line 354
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCache:Lcom/sec/android/app/myfiles/ftp/FTPCache;

    iget v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentSortBy:I

    iget v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentInOrder:I

    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v0, v1, v2, v3, v5}, Lcom/sec/android/app/myfiles/ftp/FTPCache;->getBufferContent(IILcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_1
.end method

.method public getFilesInFolder(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 247
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->goTo(Ljava/lang/String;Z)Z

    .line 249
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getFilesInFolder(Ljava/lang/String;I)Landroid/database/Cursor;
    .locals 2
    .param p1, "folderPath"    # Ljava/lang/String;
    .param p2, "showFolderFileType"    # I

    .prologue
    .line 284
    iget v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mShowFolderFileType:I

    .line 286
    .local v1, "prevShowFolderFileType":I
    iput p2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mShowFolderFileType:I

    .line 288
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getFilesInFolder(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 290
    .local v0, "cursor":Landroid/database/Cursor;
    iput v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mShowFolderFileType:I

    .line 292
    return-object v0
.end method

.method public getItemOfId(J)Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 454
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrItemsIdProp:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrItemsIdProp:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemOfPath(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 1013
    if-nez p1, :cond_0

    .line 1015
    const/4 v0, 0x0

    .line 1019
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getItem(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPItem;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemsInFolder(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 254
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->isEnableFiltering()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 256
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 258
    .local v7, "exts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mFilteringExtension:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 260
    .local v6, "ext":Ljava/lang/String;
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 264
    .end local v6    # "ext":Ljava/lang/String;
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCache:Lcom/sec/android/app/myfiles/ftp/FTPCache;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mFilteringExtension:Ljava/util/HashSet;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentSortBy:I

    iget v3, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentInOrder:I

    iget-object v4, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/myfiles/ftp/FTPCache;->getBufferContent(Ljava/util/ArrayList;IILcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 268
    .end local v7    # "exts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v8    # "i$":Ljava/util/Iterator;
    :goto_1
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCache:Lcom/sec/android/app/myfiles/ftp/FTPCache;

    iget v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentSortBy:I

    iget v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentInOrder:I

    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/sec/android/app/myfiles/ftp/FTPCache;->getBufferContent(IILcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_1
.end method

.method public getItemsOfId(Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 446
    .local p1, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 447
    .local v2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 448
    .local v1, "id":Ljava/lang/Long;
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getItemOfId(J)Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 450
    .end local v1    # "id":Ljava/lang/Long;
    :cond_0
    return-object v2
.end method

.method protected getRoot()Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .locals 3

    .prologue
    .line 985
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mRootItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    if-nez v1, :cond_0

    .line 986
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 987
    .local v0, "size":I
    if-lez v0, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    :goto_0
    iput-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mRootItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .line 990
    .end local v0    # "size":I
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mRootItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    return-object v1

    .line 987
    .restart local v0    # "size":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected getRootName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1004
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mRootItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mRootItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    invoke-interface {v0}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public goBack()Z
    .locals 1

    .prologue
    .line 216
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->goUp()Z

    move-result v0

    return v0
.end method

.method public goTo(J)Z
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 110
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrItemsIdProp:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 111
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->rememberCurrentList()V

    .line 113
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrItemsIdProp:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .line 114
    .local v0, "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 116
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->AsyncOpen()V

    .line 118
    const/4 v1, 0x1

    .line 120
    .end local v0    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z
    .locals 1
    .param p1, "label"    # Ljava/lang/String;
    .param p2, "isBack"    # Ljava/lang/Boolean;

    .prologue
    .line 126
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->goTo(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public goTo(Ljava/lang/String;Z)Z
    .locals 15
    .param p1, "label"    # Ljava/lang/String;
    .param p2, "initial"    # Z

    .prologue
    .line 130
    if-nez p1, :cond_0

    .line 131
    const/4 v12, 0x0

    .line 199
    :goto_0
    return v12

    .line 134
    :cond_0
    const/4 v1, 0x0

    .line 136
    .local v1, "doNotCheckConnection":Z
    sget-object v12, Ljava/io/File;->separator:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 138
    const/4 v12, 0x0

    invoke-virtual {p0, v12}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->goToRoot(Z)Z

    move-result v12

    goto :goto_0

    .line 142
    :cond_1
    if-eqz p2, :cond_2

    .line 143
    iget-object v12, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->clear()V

    .line 146
    :cond_2
    sget-object v12, Ljava/io/File;->separator:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 147
    .local v11, "split":[Ljava/lang/String;
    array-length v7, v11

    .line 148
    .local v7, "len":I
    if-nez p2, :cond_3

    .line 149
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->rememberCurrentList()V

    .line 151
    :cond_3
    iget-object v12, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    if-le v7, v12, :cond_a

    .line 152
    const/4 v6, 0x0

    .line 153
    .local v6, "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    iget-object v12, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentItems:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .line 154
    .local v4, "i":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    if-eqz v4, :cond_4

    .line 155
    invoke-interface {v4}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v3

    .line 156
    .local v3, "fullPath":Ljava/lang/String;
    if-eqz v3, :cond_4

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 157
    move-object v6, v4

    .line 162
    .end local v3    # "fullPath":Ljava/lang/String;
    .end local v4    # "i":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    :cond_5
    if-eqz v6, :cond_7

    if-nez p2, :cond_7

    .line 163
    iget-object v12, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v12, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 197
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    :cond_6
    :goto_1
    move/from16 v0, p2

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->AsyncOpen(ZZ)V

    .line 199
    const/4 v12, 0x1

    goto :goto_0

    .line 165
    .restart local v5    # "i$":Ljava/util/Iterator;
    .restart local v6    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    :cond_7
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 166
    .local v10, "sb":Ljava/lang/StringBuilder;
    iget-object v12, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->clear()V

    .line 167
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    array-length v12, v11

    if-ge v4, v12, :cond_6

    .line 168
    if-nez v4, :cond_9

    .line 169
    iget-object v12, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    new-instance v13, Lcom/sec/android/app/myfiles/ftp/FTPRootItem;

    sget-object v14, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-direct {v13, v14}, Lcom/sec/android/app/myfiles/ftp/FTPRootItem;-><init>(Ljava/lang/String;)V

    invoke-interface {v12, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170
    sget-object v12, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    :cond_8
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 172
    :cond_9
    aget-object v9, v11, v4

    .line 173
    .local v9, "s":Ljava/lang/String;
    new-instance v2, Lorg/apache/commons/net/ftp/FTPFile;

    invoke-direct {v2}, Lorg/apache/commons/net/ftp/FTPFile;-><init>()V

    .line 174
    .local v2, "f":Lorg/apache/commons/net/ftp/FTPFile;
    const/4 v12, 0x1

    invoke-virtual {v2, v12}, Lorg/apache/commons/net/ftp/FTPFile;->setType(I)V

    .line 175
    invoke-virtual {v2, v9}, Lorg/apache/commons/net/ftp/FTPFile;->setName(Ljava/lang/String;)V

    .line 176
    iget-object v12, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v2, v13}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getItem(Lorg/apache/commons/net/ftp/FTPFile;Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPItem;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 177
    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 178
    const/4 v12, 0x1

    if-lt v4, v12, :cond_8

    .line 179
    sget-object v12, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 184
    .end local v2    # "f":Lorg/apache/commons/net/ftp/FTPFile;
    .end local v4    # "i":I
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .end local v9    # "s":Ljava/lang/String;
    .end local v10    # "sb":Ljava/lang/StringBuilder;
    :cond_a
    const/4 v12, 0x1

    if-le v7, v12, :cond_b

    .line 185
    const/4 v1, 0x1

    .line 186
    :goto_4
    iget-object v12, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v12

    if-ge v7, v12, :cond_6

    .line 187
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->popItem()V

    goto :goto_4

    .line 189
    :cond_b
    const/4 v12, 0x1

    if-ne v7, v12, :cond_6

    .line 190
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getRoot()Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-result-object v8

    .line 191
    .local v8, "root":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    iget-object v12, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->clear()V

    .line 192
    iget-object v12, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentItems:Ljava/util/List;

    invoke-interface {v12}, Ljava/util/List;->clear()V

    .line 193
    iget-object v12, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrItemsIdProp:Ljava/util/Map;

    invoke-interface {v12}, Ljava/util/Map;->clear()V

    .line 194
    iget-object v12, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v12, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public goToRoot(Ljava/lang/Boolean;)Z
    .locals 1
    .param p1, "isBack"    # Ljava/lang/Boolean;

    .prologue
    .line 222
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->goToRoot(Z)Z

    move-result v0

    return v0
.end method

.method public goToRoot(Z)Z
    .locals 4
    .param p1, "initial"    # Z

    .prologue
    const/4 v3, 0x1

    .line 227
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 228
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->rememberCurrentList()V

    .line 230
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v3, :cond_1

    move v0, v3

    .line 231
    .local v0, "doNotCheckConnection":Z
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 234
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mRootItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    if-nez v2, :cond_2

    new-instance v1, Lcom/sec/android/app/myfiles/ftp/FTPRootItem;

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/sec/android/app/myfiles/ftp/FTPRootItem;-><init>(Ljava/lang/String;)V

    .line 237
    .local v1, "root":Lcom/sec/android/app/myfiles/ftp/FTPRootItem;
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 238
    iput-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mRootItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .line 240
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->AsyncOpen(ZZ)V

    .line 242
    return v3

    .line 230
    .end local v0    # "doNotCheckConnection":Z
    .end local v1    # "root":Lcom/sec/android/app/myfiles/ftp/FTPRootItem;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 234
    .restart local v0    # "doNotCheckConnection":Z
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mRootItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    check-cast v2, Lcom/sec/android/app/myfiles/ftp/FTPRootItem;

    move-object v1, v2

    goto :goto_1
.end method

.method public goUp()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 204
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->rememberCurrentList()V

    .line 205
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->isCurrentFolderRoot()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 211
    :goto_0
    return v0

    .line 209
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->popItem()V

    .line 210
    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->AsyncOpen(ZZ)V

    move v0, v1

    .line 211
    goto :goto_0
.end method

.method public initializeSupplier(Lcom/sec/android/app/myfiles/ftp/FTPParams;)V
    .locals 3
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    if-eqz v0, :cond_1

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->getParams()Lcom/sec/android/app/myfiles/ftp/FTPParams;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->getParams()Lcom/sec/android/app/myfiles/ftp/FTPParams;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->equals(Lcom/sec/android/app/myfiles/ftp/FTPParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    :goto_0
    return-void

    .line 87
    :cond_0
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCache:Lcom/sec/android/app/myfiles/ftp/FTPCache;

    invoke-direct {v0, v1, p1, p0, v2}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;-><init>(Landroid/content/Context;Lcom/sec/android/app/myfiles/ftp/FTPParams;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    .line 89
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    goto :goto_0

    .line 95
    :cond_1
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCache:Lcom/sec/android/app/myfiles/ftp/FTPCache;

    invoke-direct {v0, v1, p1, p0, v2}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;-><init>(Landroid/content/Context;Lcom/sec/android/app/myfiles/ftp/FTPParams;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    .line 97
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    goto :goto_0
.end method

.method public isCurrentFolderRoot()Z
    .locals 2

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getCurrentItem()Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-result-object v0

    .line 106
    .local v0, "currentItem":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-interface {v0}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isRoot()Z

    move-result v1

    goto :goto_0
.end method

.method public isFileCached(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)Z
    .locals 4
    .param p1, "item"    # Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .prologue
    .line 546
    if-eqz p1, :cond_0

    .line 548
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getSessionName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mContext:Landroid/content/Context;

    invoke-static {p1, v1, v2}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getTempPath(Lcom/sec/android/app/myfiles/ftp/IFTPItem;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 550
    .local v0, "cachedPath":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mFileCache:Lcom/sec/android/app/myfiles/ftp/FtpFileCache;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-interface {p1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/android/app/myfiles/ftp/FtpFileCache;->isFileCached(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 554
    .end local v0    # "cachedPath":Ljava/lang/String;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public moveFilesOverSelf([Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 4
    .param p1, "from"    # [Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    .prologue
    .line 629
    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    array-length v3, p1

    if-lez v3, :cond_2

    .line 630
    array-length v3, p1

    new-array v2, v3, [Ljava/lang/String;

    .line 631
    .local v2, "to":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_1

    .line 632
    aget-object v3, p1, v0

    invoke-static {v3}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getItem(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPItem;

    move-result-object v1

    .line 633
    .local v1, "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    if-eqz v1, :cond_0

    .line 634
    invoke-interface {v1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v3

    aput-object v3, p1, v0

    .line 636
    :cond_0
    aget-object v3, p1, v0

    invoke-static {v3, p2}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getTargetPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 631
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 638
    .end local v1    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    invoke-virtual {v3, p1, v2, p3}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->moveFiles([Ljava/lang/String;[Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    .line 640
    .end local v0    # "i":I
    .end local v2    # "to":[Ljava/lang/String;
    :cond_2
    return-void
.end method

.method public moveFromLocal([Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 1
    .param p1, "sourceFiles"    # [Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    .prologue
    .line 399
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->uploadLocal([Ljava/lang/String;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    .line 400
    return-void
.end method

.method public moveFromLocalToFtp([Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 1
    .param p1, "srcPaths"    # [Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    .prologue
    .line 387
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->uploadLocal([Ljava/lang/String;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    .line 389
    return-void
.end method

.method public onBrowseOperationFailed(ZZI)V
    .locals 4
    .param p1, "connectionProblem"    # Z
    .param p2, "initialBrowse"    # Z
    .param p3, "errorCode"    # I

    .prologue
    const/4 v3, 0x1

    .line 770
    const-string v0, "MyFiles"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AbsFTPNavigation.onOperationFailed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", initialBrowse: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 771
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mAsyncUi:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    if-eqz v0, :cond_0

    .line 772
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mAsyncUi:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->setCompletedCondition(Z)V

    .line 773
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mGoPrevious:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 774
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mGoPrevious:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 777
    :cond_0
    if-eqz p1, :cond_3

    .line 778
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->popItem()V

    .line 779
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->AsyncOpen()V

    .line 780
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mContext:Landroid/content/Context;

    const v2, 0x7f0b00c7

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 787
    :cond_1
    :goto_0
    if-eqz p2, :cond_2

    .line 788
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCloseFragment:Ljava/lang/Runnable;

    if-eqz v0, :cond_2

    .line 789
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCloseFragment:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 792
    :cond_2
    return-void

    .line 783
    :cond_3
    if-nez p3, :cond_1

    .line 784
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mContext:Landroid/content/Context;

    const v1, 0x7f0b00f0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onBrowseResponseReceived(Ljava/util/List;Lcom/sec/android/app/myfiles/ftp/IFTPItem;)V
    .locals 5
    .param p2, "requestedFolderItem"    # Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ")V"
        }
    .end annotation

    .prologue
    .line 738
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    const-string v2, "MyFiles"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "AbsFTPNavigation.onBrowseResponseReceived: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " items"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 742
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 745
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .line 746
    .local v1, "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentItems:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 750
    .end local v1    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    :cond_0
    sget-boolean v2, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mAbortBrowsing:Z

    if-eqz v2, :cond_1

    .line 752
    const/4 v2, 0x2

    const-string v3, "ftp"

    const-string v4, "mAbortBrowsing ture onBrowseResponseReceived !!! mCurrentItems clear"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 753
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 757
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mAsyncUi:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    if-eqz v2, :cond_2

    .line 758
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mAsyncUi:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->setCompletedCondition(Z)V

    .line 760
    :cond_2
    return-void
.end method

.method public onCloseConnection(Z)V
    .locals 3
    .param p1, "success"    # Z

    .prologue
    .line 796
    const-string v0, "MyFiles"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AbsFTPNavigation.onCloseConnection: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 798
    return-void
.end method

.method public onDiscoveryCompleted(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "result"    # Landroid/os/Bundle;

    .prologue
    .line 822
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->deleteFiles(Landroid/os/Bundle;)V

    .line 823
    return-void
.end method

.method public onDiscoveryCompleted(Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;)V
    .locals 0
    .param p1, "visitor"    # Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;

    .prologue
    .line 827
    if-eqz p1, :cond_0

    .line 828
    invoke-interface {p1}, Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;->done()V

    .line 830
    :cond_0
    return-void
.end method

.method public onDownloadCompleted(Z[Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/FTPParams;Z)V
    .locals 0
    .param p1, "success"    # Z
    .param p2, "ftpPaths"    # [Ljava/lang/String;
    .param p3, "downloadedPath"    # Ljava/lang/String;
    .param p4, "srcParrams"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p5, "move"    # Z

    .prologue
    .line 858
    return-void
.end method

.method public onFileDeleted()V
    .locals 0

    .prologue
    .line 807
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->AsyncOpen()V

    .line 808
    return-void
.end method

.method public onFileRenamed()V
    .locals 0

    .prologue
    .line 812
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->AsyncOpen()V

    .line 813
    return-void
.end method

.method public onFolderCreated()V
    .locals 0

    .prologue
    .line 802
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->AsyncOpen()V

    .line 803
    return-void
.end method

.method public onMoveFilesSelfCompleted()V
    .locals 0

    .prologue
    .line 817
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->AsyncOpen()V

    .line 818
    return-void
.end method

.method public onOpenConnection(Z)V
    .locals 3
    .param p1, "success"    # Z

    .prologue
    .line 764
    const-string v0, "MyFiles"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AbsFTPNavigation.onOpenConnection: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 766
    return-void
.end method

.method public onSelectCompleted(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "success"    # Z
    .param p2, "ftpPath"    # Ljava/lang/String;
    .param p3, "downloadedPath"    # Ljava/lang/String;

    .prologue
    .line 863
    if-eqz p1, :cond_0

    .line 864
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mContext:Landroid/content/Context;

    invoke-static {v1, p3}, Lcom/sec/android/app/myfiles/utils/Utils;->sendScan(Landroid/content/Context;Ljava/lang/String;)V

    .line 875
    :goto_0
    return-void

    .line 866
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 867
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 868
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 872
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mContext:Landroid/content/Context;

    const v3, 0x7f0b00ad

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onUploadCompleted(ZLjava/lang/String;)V
    .locals 4
    .param p1, "success"    # Z
    .param p2, "moveSrc"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 835
    const/4 v0, -0x1

    .line 836
    .local v0, "result":I
    if-nez p1, :cond_0

    .line 837
    const/4 v0, 0x0

    .line 839
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v1, :cond_1

    .line 840
    if-eqz p2, :cond_3

    .line 841
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    const/16 v2, 0xe

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 846
    :cond_1
    :goto_0
    if-eqz p2, :cond_2

    .line 847
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mContext:Landroid/content/Context;

    invoke-static {v1, p2}, Lcom/sec/android/app/myfiles/utils/Utils;->sendScan(Landroid/content/Context;Ljava/lang/String;)V

    .line 849
    :cond_2
    new-instance v1, Ljava/io/File;

    sget-object v2, Lcom/sec/android/app/myfiles/utils/Constant;->DEFAULT_TEMP_UPLOAD_DIR:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->deleteDir(Ljava/io/File;)V

    .line 850
    return-void

    .line 843
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    const/16 v2, 0xc

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method protected openItem(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)V
    .locals 1
    .param p1, "item"    # Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .prologue
    .line 1009
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->openItem(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)V

    .line 1010
    return-void
.end method

.method protected popItem()V
    .locals 3

    .prologue
    .line 978
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    .line 979
    .local v0, "size":I
    if-lez v0, :cond_0

    .line 980
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    add-int/lit8 v2, v0, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 982
    :cond_0
    return-void
.end method

.method public refreshNavigation()V
    .locals 1

    .prologue
    .line 370
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mRootItem:Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .line 371
    return-void
.end method

.method protected rememberCurrentList()V
    .locals 3

    .prologue
    .line 914
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mPreviousPath:Ljava/util/List;

    if-nez v2, :cond_0

    .line 915
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mPreviousPath:Ljava/util/List;

    .line 917
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mPreviousPath:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 918
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .line 919
    .local v1, "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mPreviousPath:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 921
    .end local v1    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    :cond_1
    return-void
.end method

.method public removeFileFromCache(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "targetPath"    # Ljava/lang/String;
    .param p2, "downloadItemStr"    # Ljava/lang/String;

    .prologue
    .line 530
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 532
    invoke-static {p2}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getItem(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPItem;

    move-result-object v0

    .line 534
    .local v0, "item":Lcom/sec/android/app/myfiles/ftp/FTPItem;
    if-eqz v0, :cond_0

    .line 536
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mFileCache:Lcom/sec/android/app/myfiles/ftp/FtpFileCache;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, p1}, Lcom/sec/android/app/myfiles/ftp/FtpFileCache;->removeFromCache(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Ljava/lang/String;)V

    .line 542
    .end local v0    # "item":Lcom/sec/android/app/myfiles/ftp/FTPItem;
    :cond_0
    return-void
.end method

.method public renameFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "from"    # Ljava/lang/String;
    .param p2, "to"    # Ljava/lang/String;

    .prologue
    .line 624
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->renameFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 625
    return-void
.end method

.method protected restorePreviousList()V
    .locals 3

    .prologue
    .line 924
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 925
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mPreviousPath:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .line 926
    .local v1, "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 928
    .end local v1    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    :cond_0
    return-void
.end method

.method public setCloseFragmentCallback(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "cleanCallback"    # Ljava/lang/Runnable;

    .prologue
    .line 883
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCloseFragment:Ljava/lang/Runnable;

    .line 884
    return-void
.end method

.method public setCurrentPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 277
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentFolderPath:Ljava/lang/String;

    .line 279
    return-void
.end method

.method public setEmptyCurrentFolder(Z)V
    .locals 0
    .param p1, "emptyCurrentFolder"    # Z

    .prologue
    .line 1059
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mEmptyCurrentFolder:Z

    .line 1061
    return-void
.end method

.method public setGoPreviousCallback(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "goUpCallback"    # Ljava/lang/Runnable;

    .prologue
    .line 878
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 879
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mGoPrevious:Ljava/lang/Runnable;

    .line 880
    return-void
.end method

.method public syncIsConnectionUp()Z
    .locals 1

    .prologue
    .line 725
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    if-eqz v0, :cond_0

    .line 727
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->syncPrepareConnection()Z

    move-result v0

    .line 731
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public uploadFtp(Lcom/sec/android/app/myfiles/ftp/FTPParams;[Ljava/lang/String;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 12
    .param p1, "targetFtp"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p2, "files"    # [Ljava/lang/String;
    .param p3, "target"    # Ljava/lang/String;
    .param p4, "move"    # Z
    .param p5, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    .prologue
    .line 433
    sget-object v4, Lcom/sec/android/app/myfiles/utils/Constant;->DEFAULT_TEMP_UPLOAD_DIR:Ljava/lang/String;

    .line 435
    .local v4, "src":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 436
    .local v1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/ftp/DownloadItem;>;"
    move-object v7, p2

    .local v7, "arr$":[Ljava/lang/String;
    array-length v11, v7

    .local v11, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    :goto_0
    if-ge v9, v11, :cond_1

    aget-object v8, v7, v9

    .line 437
    .local v8, "file":Ljava/lang/String;
    invoke-virtual {p0, v8}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getItemOfPath(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-result-object v10

    .line 438
    .local v10, "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    if-eqz v10, :cond_0

    .line 439
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v10}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v10, v2}, Lcom/sec/android/app/myfiles/ftp/DownloadItem;-><init>(Lcom/sec/android/app/myfiles/ftp/IFTPItem;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 436
    :cond_0
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 442
    .end local v8    # "file":Ljava/lang/String;
    .end local v10    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    move-object v2, p1

    move-object v3, p3

    move/from16 v5, p4

    move-object/from16 v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->copyToFtp(Ljava/util/List;Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    .line 443
    return-void
.end method

.method public uploadLocal([Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 1
    .param p1, "sourceFiles"    # [Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    .prologue
    .line 403
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->uploadLocal([Ljava/lang/String;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    .line 404
    return-void
.end method

.method protected uploadLocal([Ljava/lang/String;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 1
    .param p1, "sourceFiles"    # [Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;
    .param p3, "move"    # Z
    .param p4, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    .prologue
    .line 408
    if-eqz p1, :cond_0

    array-length v0, p1

    if-lez v0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 410
    if-eqz p3, :cond_1

    .line 411
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    invoke-virtual {v0, p1, p2, p4}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->moveFromLocalToFtp([Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    .line 416
    :cond_0
    :goto_0
    return-void

    .line 413
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    invoke-virtual {v0, p1, p2, p4}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->copyFromLocalToFtp([Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    goto :goto_0
.end method

.method public uploadSelf([Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V
    .locals 10
    .param p1, "files"    # [Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    .prologue
    .line 419
    sget-object v6, Lcom/sec/android/app/myfiles/utils/Constant;->DEFAULT_TEMP_UPLOAD_DIR:Ljava/lang/String;

    .line 421
    .local v6, "src":Ljava/lang/String;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 422
    .local v4, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/ftp/DownloadItem;>;"
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v1, v0, v2

    .line 423
    .local v1, "file":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getItemOfPath(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-result-object v3

    .line 424
    .local v3, "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    if-eqz v3, :cond_0

    .line 425
    new-instance v7, Lcom/sec/android/app/myfiles/ftp/DownloadItem;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v3}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getTitle()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v3, v8}, Lcom/sec/android/app/myfiles/ftp/DownloadItem;-><init>(Lcom/sec/android/app/myfiles/ftp/IFTPItem;Ljava/lang/String;)V

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 422
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 428
    .end local v1    # "file":Ljava/lang/String;
    .end local v3    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    invoke-virtual {v7, v4, p2, v6, p3}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->copySelf(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    .line 429
    return-void
.end method

.method public visitItemsRecursively(Ljava/util/List;Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;)V
    .locals 2
    .param p2, "visitor"    # Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;",
            "Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 644
    .local p1, "ftpItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->mSupplier:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->visitItemsRecursively(Ljava/util/List;Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    .line 646
    return-void
.end method
