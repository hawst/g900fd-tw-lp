.class public Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;
.super Ljava/lang/Object;
.source "ContentSearchConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/utils/ContentSearchConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DocExtensions"
.end annotation


# static fields
.field public static final DOC:Ljava/lang/String; = ".doc"

.field public static final DOCX:Ljava/lang/String; = ".docx"

.field public static final DOCX_DOCM:Ljava/lang/String; = ".docm"

.field public static final DOCX_DOTM:Ljava/lang/String; = ".dotm"

.field public static final DOCX_DOTX:Ljava/lang/String; = ".dotx"

.field public static final DOC_DOT:Ljava/lang/String; = ".dot"

.field public static final HWP:Ljava/lang/String; = ".hwp"

.field public static final PDF:Ljava/lang/String; = ".pdf"

.field public static final PPT:Ljava/lang/String; = ".ppt"

.field public static final PPTX:Ljava/lang/String; = ".pptx"

.field public static final PPTX_POTM:Ljava/lang/String; = ".potm"

.field public static final PPTX_POTX:Ljava/lang/String; = ".potx"

.field public static final PPTX_PPAM:Ljava/lang/String; = ".ppam"

.field public static final PPTX_PPSM:Ljava/lang/String; = ".ppsm"

.field public static final PPTX_PPSX:Ljava/lang/String; = ".ppsx"

.field public static final PPTX_PPTM:Ljava/lang/String; = ".pptm"

.field public static final PPTX_SLDM:Ljava/lang/String; = ".sldm"

.field public static final PPTX_SLDX:Ljava/lang/String; = ".sldx"

.field public static final PPT_POT:Ljava/lang/String; = ".pot"

.field public static final PPT_PPS:Ljava/lang/String; = ".pps"

.field public static final SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final TXT:Ljava/lang/String; = ".txt"

.field public static final XLS:Ljava/lang/String; = ".xls"

.field public static final XLSX:Ljava/lang/String; = ".xlsx"

.field public static final XLSX_XLSM:Ljava/lang/String; = ".xlsm"

.field public static final XLSX_XLTM:Ljava/lang/String; = ".xltm"

.field public static final XLSX_XLTX:Ljava/lang/String; = ".xltx"

.field public static final XLS_XLM:Ljava/lang/String; = ".xlm"

.field public static final XLS_XLT:Ljava/lang/String; = ".xlt"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 69
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;

    .line 72
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;

    const-string v1, ".txt"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;

    const-string v1, ".doc"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;

    const-string v1, ".docx"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 75
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;

    const-string v1, ".pdf"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 76
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;

    const-string v1, ".xls"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 77
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;

    const-string v1, ".xlsx"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;

    const-string v1, ".ppt"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;

    const-string v1, ".pptx"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;

    const-string v1, ".dot"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;

    const-string v1, ".docm"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 82
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;

    const-string v1, ".dotx"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 83
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;

    const-string v1, ".dotm"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 84
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;

    const-string v1, ".pot"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;

    const-string v1, ".pps"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 86
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;

    const-string v1, ".pptm"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 87
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;

    const-string v1, ".potx"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 88
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;

    const-string v1, ".potm"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;

    const-string v1, ".ppam"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;

    const-string v1, ".ppsx"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;

    const-string v1, ".ppsm"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;

    const-string v1, ".sldx"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 93
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;

    const-string v1, ".sldm"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 94
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;

    const-string v1, ".xlt"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;

    const-string v1, ".xlm"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;

    const-string v1, ".xlsm"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;

    const-string v1, ".xltx"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 98
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;->SUPPORTED_FILE_TYPES:Ljava/util/ArrayList;

    const-string v1, ".xltm"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 99
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 102
    const-string v2, "."

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 103
    .local v1, "mid":I
    const/4 v0, 0x0

    .line 104
    .local v0, "ext":Ljava/lang/String;
    if-lez v1, :cond_0

    .line 105
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v0

    .line 108
    :cond_0
    return-object v0
.end method
