.class public Lcom/sec/android/app/myfiles/fragment/CloudFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;
.source "CloudFragment.java"


# static fields
.field private static final MODULE:Ljava/lang/String; = "DropboxFragment"

.field private static final REQUEST_CREATE_FOLDER:I = 0x4

.field private static final REQUEST_EXTRACT:I = 0x6

.field private static final REQUEST_ZIP:I = 0x5


# instance fields
.field mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

.field private mDropboxCopyMoveSourceFolderPath:Ljava/lang/String;

.field private mDropboxPathIndicator:Ljava/lang/String;

.field private mDropboxRoot:Ljava/lang/String;

.field private mDropboxRootReplace:Ljava/lang/String;

.field mFromBackKey:Z

.field private mFromSelector:Z

.field private mPathChangeListener:Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;

.field private mSearchFromIndex:I

.field private mShortcutIndex:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 98
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;-><init>()V

    .line 82
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxPathIndicator:Ljava/lang/String;

    .line 84
    const-string v0, "/Dropbox"

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxRoot:Ljava/lang/String;

    .line 85
    const-string v0, "Dropbox/"

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxRootReplace:Ljava/lang/String;

    .line 87
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    .line 93
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mFromSelector:Z

    .line 95
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mFromBackKey:Z

    .line 1344
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment$6;-><init>(Lcom/sec/android/app/myfiles/fragment/CloudFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mPathChangeListener:Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;

    .line 99
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/fragment/CloudFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxPathIndicator:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/myfiles/fragment/CloudFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CloudFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxPathIndicator:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/fragment/CloudFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CloudFragment;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxRoot:Ljava/lang/String;

    return-object v0
.end method

.method private startCompress(Ljava/lang/String;)V
    .locals 9
    .param p1, "targetZipFile"    # Ljava/lang/String;

    .prologue
    .line 924
    const/4 v6, 0x3

    invoke-static {v6}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(I)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v1

    .line 926
    .local v1, "fragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    const/4 v6, 0x2

    invoke-virtual {v1, p0, v6}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 928
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v4

    .line 930
    .local v4, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 932
    .local v5, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 934
    .local v3, "selectedItem":Ljava/lang/Object;
    check-cast v3, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .end local v3    # "selectedItem":Ljava/lang/Object;
    iget-object v6, v3, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 937
    :cond_0
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 939
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v6, "src_folder"

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 941
    const-string v6, "dst_folder"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 943
    const-string v6, "target_datas"

    invoke-virtual {v0, v6, v5}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 945
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "compress"

    invoke-virtual {v1, v6, v7}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 946
    return-void
.end method

.method private startExtract(Ljava/lang/String;)V
    .locals 8
    .param p1, "targetFoler"    # Ljava/lang/String;

    .prologue
    .line 951
    const/4 v6, 0x4

    invoke-static {v6}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(I)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v1

    .line 953
    .local v1, "fragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    const/4 v6, 0x2

    invoke-virtual {v1, p0, v6}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 955
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v4

    .line 957
    .local v4, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 959
    .local v5, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 961
    .local v3, "selectedItem":Ljava/lang/Object;
    check-cast v3, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .end local v3    # "selectedItem":Ljava/lang/Object;
    iget-object v6, v3, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 964
    :cond_0
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 966
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v6, "src_folder"

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 968
    const-string v6, "dst_folder"

    invoke-virtual {v0, v6, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 970
    const-string v6, "target_datas"

    invoke-virtual {v0, v6, v5}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 972
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "extract"

    invoke-virtual {v1, v6, v7}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 973
    return-void
.end method


# virtual methods
.method public AsyncOpenCompleted()V
    .locals 2

    .prologue
    .line 1409
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v0

    .line 1411
    .local v0, "c":Landroid/database/Cursor;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 1413
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->invalidateOptionsMenu()V

    .line 1415
    return-void
.end method

.method public addCurrentFolderToShortcut()V
    .locals 9

    .prologue
    .line 862
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->addCurrentFolderToShortcut()V

    .line 863
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v0, :cond_6

    .line 864
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mAddShortcutFromActionmode:Z

    if-eqz v0, :cond_b

    .line 865
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    .line 866
    .local v8, "item":Ljava/lang/Object;
    check-cast v8, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .end local v8    # "item":Ljava/lang/Object;
    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 867
    .local v1, "path":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 869
    :cond_0
    const-string v1, "Dropbox/"

    .line 871
    :cond_1
    const-string v0, "Dropbox/"

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 873
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Dropbox/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 875
    :cond_2
    sget-char v0, Ljava/io/File;->separatorChar:C

    invoke-virtual {v1, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 877
    .local v2, "shortcutName":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_3

    .line 878
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/16 v3, 0x8

    const/4 v4, -0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/myfiles/utils/Utils;->addShortCut(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/drawable/Drawable;Z)V

    goto :goto_0

    .line 880
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/16 v3, 0x8

    const/4 v4, -0x1

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/myfiles/utils/Utils;->addShortCut(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/drawable/Drawable;Z)V

    goto :goto_0

    .line 883
    .end local v1    # "path":Ljava/lang/String;
    .end local v2    # "shortcutName":Ljava/lang/String;
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v0

    const/4 v3, 0x1

    if-le v0, v3, :cond_5

    .line 884
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v0

    sget v3, Lcom/sec/android/app/myfiles/utils/Utils;->mAddShortcutSuccess:I

    if-ne v0, v3, :cond_8

    .line 885
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v3, 0x7f0b0060

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 898
    :cond_5
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v0

    const/16 v3, 0x1a

    invoke-virtual {v0, v3}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->refreshAdapter(I)V

    .line 916
    .end local v7    # "i$":Ljava/util/Iterator;
    :cond_6
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 917
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->finishSelectMode()V

    .line 919
    :cond_7
    return-void

    .line 887
    .restart local v7    # "i$":Ljava/util/Iterator;
    :cond_8
    sget v0, Lcom/sec/android/app/myfiles/utils/Utils;->mAddShortcutSuccess:I

    const/4 v3, 0x1

    if-ne v0, v3, :cond_9

    .line 888
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v3, "1 shortcut added, Existing shortcuts were excluded"

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 890
    :cond_9
    sget v0, Lcom/sec/android/app/myfiles/utils/Utils;->mAddShortcutSuccess:I

    if-nez v0, :cond_a

    .line 891
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v3, "Shortcuts already exist."

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 893
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget v4, Lcom/sec/android/app/myfiles/utils/Utils;->mAddShortcutSuccess:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " shortcuts added, Existing shortcuts were excluded"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 900
    .end local v7    # "i$":Ljava/util/Iterator;
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v1

    .line 901
    .restart local v1    # "path":Ljava/lang/String;
    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_d

    .line 903
    :cond_c
    const-string v1, "Dropbox/"

    .line 905
    :cond_d
    const-string v0, "Dropbox/"

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 907
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Dropbox/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 909
    :cond_e
    sget-char v0, Ljava/io/File;->separatorChar:C

    invoke-virtual {v1, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 911
    .restart local v2    # "shortcutName":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/16 v3, 0x8

    const/4 v4, -0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/myfiles/utils/Utils;->addShortCut(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/drawable/Drawable;Z)V

    .line 913
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v0

    const/16 v3, 0x1a

    invoke-virtual {v0, v3}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->refreshAdapter(I)V

    goto/16 :goto_2
.end method

.method public addShortcutToHome()V
    .locals 6

    .prologue
    .line 1510
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->addShortcutToHome()V

    .line 1511
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v4, v4, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v4, :cond_7

    .line 1512
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v4, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v4, v4, Lcom/sec/android/app/myfiles/MainActivity;->mAddShortcutFromActionmode:Z

    if-eqz v4, :cond_3

    .line 1513
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 1514
    .local v1, "item":Ljava/lang/Object;
    check-cast v1, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .end local v1    # "item":Ljava/lang/Object;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 1515
    .local v2, "path":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 1517
    :cond_0
    const-string v2, "Dropbox/"

    .line 1519
    :cond_1
    const-string v4, "Dropbox/"

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1521
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Dropbox/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1523
    :cond_2
    sget-char v4, Ljava/io/File;->separatorChar:C

    invoke-virtual {v2, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 1525
    .local v3, "shortcutName":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v4, v2, v3}, Lcom/sec/android/app/myfiles/utils/Utils;->addShortcutToHome(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1528
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "path":Ljava/lang/String;
    .end local v3    # "shortcutName":Ljava/lang/String;
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v2

    .line 1529
    .restart local v2    # "path":Ljava/lang/String;
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_5

    .line 1531
    :cond_4
    const-string v2, "Dropbox/"

    .line 1533
    :cond_5
    const-string v4, "Dropbox/"

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 1535
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Dropbox/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1537
    :cond_6
    sget-char v4, Ljava/io/File;->separatorChar:C

    invoke-virtual {v2, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 1539
    .restart local v3    # "shortcutName":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v4, v2, v3}, Lcom/sec/android/app/myfiles/utils/Utils;->addShortcutToHome(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1542
    .end local v2    # "path":Ljava/lang/String;
    .end local v3    # "shortcutName":Ljava/lang/String;
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->isSelectMode()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1543
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->finishSelectMode()V

    .line 1545
    :cond_8
    return-void
.end method

.method public getOptionsMenuResId(Z)I
    .locals 1
    .param p1, "isSelectMode"    # Z

    .prologue
    .line 979
    if-eqz p1, :cond_1

    .line 981
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getSelectModeFrom()I

    move-result v0

    if-nez v0, :cond_0

    .line 983
    const v0, 0x7f0e0005

    .line 992
    :goto_0
    return v0

    .line 987
    :cond_0
    const v0, 0x7f0e0018

    goto :goto_0

    .line 992
    :cond_1
    const v0, 0x7f0e0004

    goto :goto_0
.end method

.method protected initTreeView()V
    .locals 2

    .prologue
    .line 831
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->initTreeView()V

    .line 833
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->setNavigation(Lcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 834
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 9
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v8, -0x1

    .line 702
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 704
    sparse-switch p1, :sswitch_data_0

    .line 826
    :cond_0
    :goto_0
    return-void

    .line 708
    :sswitch_0
    if-ne p2, v8, :cond_0

    .line 710
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "result_value"

    invoke-virtual {p3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".zip"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 712
    .local v6, "targetZipFile":Ljava/lang/String;
    invoke-direct {p0, v6}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->startCompress(Ljava/lang/String;)V

    goto :goto_0

    .line 720
    .end local v6    # "targetZipFile":Ljava/lang/String;
    :sswitch_1
    if-ne p2, v8, :cond_0

    .line 722
    const-string v7, "result_value"

    invoke-virtual {p3, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 724
    .local v5, "targetFolder":Ljava/lang/String;
    invoke-direct {p0, v5}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->startExtract(Ljava/lang/String;)V

    goto :goto_0

    .line 731
    .end local v5    # "targetFolder":Ljava/lang/String;
    :sswitch_2
    if-ne p2, v8, :cond_0

    .line 733
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-result-object v7

    if-eqz v7, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 735
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->AsyncOpen()V

    .line 739
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v1

    .line 741
    .local v1, "c":Landroid/database/Cursor;
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v7, :cond_2

    .line 742
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->updateAdapter(Landroid/database/Cursor;)V

    goto :goto_0

    .line 744
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 753
    .end local v1    # "c":Landroid/database/Cursor;
    :sswitch_3
    if-ne p2, v8, :cond_0

    .line 755
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 757
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->AsyncOpen()V

    goto :goto_0

    .line 765
    :sswitch_4
    const/4 v4, 0x0

    .line 766
    .local v4, "path":Ljava/lang/String;
    const/4 v3, -0x1

    .line 767
    .local v3, "operationType":I
    const/4 v0, 0x0

    .line 769
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz p3, :cond_3

    .line 770
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 771
    if-eqz v0, :cond_3

    .line 772
    const-string v7, "target_folder"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 773
    const-string v7, "FILE_OPERATION"

    invoke-virtual {p3, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 777
    :cond_3
    if-eq p2, v8, :cond_4

    const/4 v7, 0x2

    if-ne v3, v7, :cond_7

    .line 779
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-result-object v7

    if-eqz v7, :cond_5

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v7

    if-eqz v7, :cond_5

    .line 781
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->AsyncOpen()V

    .line 785
    :cond_5
    const/4 v7, 0x1

    if-eq v3, v7, :cond_6

    if-nez v3, :cond_0

    .line 786
    :cond_6
    const-string v7, "dest_fragment_id"

    invoke-virtual {p3, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 787
    .local v2, "dstFragmentId":I
    sget-object v7, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 789
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getFragmentId()I

    move-result v7

    invoke-virtual {p0, v7, v2, v4, v0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->switchFragments(IILjava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 791
    .end local v2    # "dstFragmentId":I
    :cond_7
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxCopyMoveSourceFolderPath:Ljava/lang/String;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v7, :cond_0

    .line 793
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxCopyMoveSourceFolderPath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_8

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goBack()Z

    move-result v7

    if-eqz v7, :cond_d

    .line 794
    :cond_8
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxCopyMoveSourceFolderPath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_9

    .line 795
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goBack()Z

    move-result v7

    if-nez v7, :cond_8

    .line 799
    :cond_9
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxCopyMoveSourceFolderPath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 801
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_a

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 802
    :cond_a
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxRoot:Ljava/lang/String;

    iput-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxPathIndicator:Ljava/lang/String;

    .line 804
    :cond_b
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxPathIndicator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 805
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxPathIndicator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setCurrentDirectory(Ljava/lang/String;)V

    .line 806
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_c

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v7, :cond_c

    .line 808
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->RemoveAllPaddingThumbnailMessage()V

    .line 810
    :cond_c
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v1

    .line 811
    .restart local v1    # "c":Landroid/database/Cursor;
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v7, :cond_e

    .line 812
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 821
    .end local v1    # "c":Landroid/database/Cursor;
    :cond_d
    :goto_1
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxCopyMoveSourceFolderPath:Ljava/lang/String;

    goto/16 :goto_0

    .line 814
    .restart local v1    # "c":Landroid/database/Cursor;
    :cond_e
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 818
    .end local v1    # "c":Landroid/database/Cursor;
    :cond_f
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goToRoot(Ljava/lang/Boolean;)Z

    goto :goto_1

    .line 704
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_4
        0x4 -> :sswitch_2
        0x5 -> :sswitch_0
        0x6 -> :sswitch_1
        0x12 -> :sswitch_3
    .end sparse-switch
.end method

.method public onBackPressed()Z
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, -0x1

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 285
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getSelectModeFrom()I

    move-result v5

    if-eq v5, v9, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getSelectModeFrom()I

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->isSelectMode()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 288
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->finishSelectMode()V

    .line 407
    :goto_0
    return v3

    .line 292
    :cond_1
    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mIsHomeBtnSelected:Z

    if-nez v5, :cond_2

    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mIsSelector:Z

    if-nez v5, :cond_12

    .line 293
    :cond_2
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mIsSelector:Z

    .line 294
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v5, :cond_f

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    if-eqz v5, :cond_f

    .line 295
    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mIsHomeBtnSelected:Z

    if-nez v5, :cond_3

    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mIsShortcutFolder:Z

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mShortcutRootFolder:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxRootReplace:Ljava/lang/String;

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mShortcutRootFolder:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    :cond_3
    iget v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mSearchFromIndex:I

    const/4 v6, 0x6

    if-ne v5, v6, :cond_9

    .line 298
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getSelectModeFrom()I

    move-result v5

    if-nez v5, :cond_5

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-nez v5, :cond_6

    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getSelectModeFrom()I

    move-result v5

    if-ne v5, v8, :cond_8

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v5, :cond_8

    .line 300
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 302
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 303
    .local v1, "b":Landroid/os/Bundle;
    const-string v4, "shortcut_working_index_intent_key"

    iget v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mShortcutIndex:I

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 304
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v4, v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView(Landroid/os/Bundle;)V

    goto :goto_0

    .line 308
    .end local v1    # "b":Landroid/os/Bundle;
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView()V

    goto :goto_0

    .line 312
    :cond_8
    const-string v3, "DropboxFragment"

    const-string v5, " failed to go up"

    invoke-static {v4, v3, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    move v3, v4

    .line 313
    goto :goto_0

    .line 316
    :cond_9
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goUp()Z

    move-result v5

    if-eqz v5, :cond_f

    .line 319
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->isSelectMode()Z

    move-result v4

    if-eqz v4, :cond_a

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v4, :cond_a

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v4

    if-lez v4, :cond_a

    .line 321
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->unselectAllItem()V

    .line 325
    :cond_a
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/view/PathIndicator;->goUp()V

    .line 327
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_b

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 329
    :cond_b
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxRoot:Ljava/lang/String;

    iput-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxPathIndicator:Ljava/lang/String;

    .line 330
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxPathIndicator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 331
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxPathIndicator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setCurrentDirectory(Ljava/lang/String;)V

    .line 339
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_c

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v4, :cond_c

    .line 342
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->RemoveAllPaddingThumbnailMessage()V

    .line 345
    :cond_c
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v2

    .line 346
    .local v2, "c":Landroid/database/Cursor;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v4, :cond_e

    .line 348
    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->updateAdapter(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 334
    .end local v2    # "c":Landroid/database/Cursor;
    :cond_d
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxRoot:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxPathIndicator:Ljava/lang/String;

    .line 335
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxPathIndicator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 336
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxPathIndicator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setCurrentDirectory(Ljava/lang/String;)V

    goto :goto_1

    .line 352
    .restart local v2    # "c":Landroid/database/Cursor;
    :cond_e
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 357
    .end local v2    # "c":Landroid/database/Cursor;
    :cond_f
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mIsHomeBtnSelected:Z

    .line 393
    :cond_10
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getSelectModeFrom()I

    move-result v5

    if-eq v5, v9, :cond_11

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getSelectModeFrom()I

    move-result v5

    if-eq v5, v8, :cond_11

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getSelectModeFrom()I

    move-result v5

    if-nez v5, :cond_19

    :cond_11
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v5, :cond_19

    .line 396
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView()V

    goto/16 :goto_0

    .line 358
    :cond_12
    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mIsSelector:Z

    if-eqz v5, :cond_10

    .line 359
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mIsSelector:Z

    .line 360
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "ISSELECTOR"

    iget-boolean v6, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mIsSelector:Z

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 361
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goBack()Z

    move-result v4

    if-eqz v4, :cond_17

    .line 363
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_13

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 364
    :cond_13
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxRoot:Ljava/lang/String;

    iput-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxPathIndicator:Ljava/lang/String;

    .line 366
    :cond_14
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxPathIndicator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 367
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxPathIndicator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setCurrentDirectory(Ljava/lang/String;)V

    .line 368
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_15

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v4, :cond_15

    .line 370
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->RemoveAllPaddingThumbnailMessage()V

    .line 372
    :cond_15
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v2

    .line 373
    .restart local v2    # "c":Landroid/database/Cursor;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v4, :cond_16

    .line 374
    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->updateAdapter(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 376
    :cond_16
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 380
    .end local v2    # "c":Landroid/database/Cursor;
    :cond_17
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 381
    .local v0, "argument":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "RETURNPATH"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_18

    .line 382
    const-string v4, "FOLDERPATH"

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxRoot:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    :goto_2
    const-string v4, "RETURNPATH"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->clearHistoricalPaths()V

    .line 388
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mCallback:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$onCopyMoveSelectedListener;

    const-string v5, "FOLDERPATH"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v0, v5}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$onCopyMoveSelectedListener;->onCopyMoveSelected(Landroid/os/Bundle;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 384
    :cond_18
    const-string v4, "FOLDERPATH"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "RETURNPATH"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 399
    .end local v0    # "argument":Landroid/os/Bundle;
    :cond_19
    const-string v3, "DropboxFragment"

    const-string v5, " failed to go up"

    invoke-static {v4, v3, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    move v3, v4

    .line 401
    goto/16 :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1434
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1435
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mRunFrom:I

    const/16 v1, 0x15

    if-ne v0, v1, :cond_0

    .line 1436
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->selectmodeActionbar()V

    .line 1438
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 104
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onCreate(Landroid/os/Bundle;)V

    .line 106
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 108
    .local v0, "argument":Landroid/os/Bundle;
    const-string v3, "back_key_intent"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mFromBackKey:Z

    .line 110
    const/16 v3, 0x8

    iput v3, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mCategoryType:I

    .line 112
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 115
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .line 118
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3, p0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->setSupportAsync(Lcom/sec/android/app/myfiles/fragment/AbsFragment;)V

    .line 120
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mFromBackKey:Z

    if-nez v3, :cond_1

    .line 121
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goToRoot(Ljava/lang/Boolean;)Z

    .line 124
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mSelectedItemChangeListener:Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;

    if-eqz v3, :cond_2

    .line 126
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mSelectedItemChangeListener:Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->setSelectedItemChangeListener(Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;)V

    .line 132
    :cond_2
    const/4 v2, 0x0

    .line 134
    .local v2, "startFolder":Ljava/lang/String;
    if-eqz v0, :cond_4

    .line 136
    const-string v3, "shortcut_working_index_intent_key"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mShortcutIndex:I

    .line 138
    const-string v3, "run_from"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    const/16 v4, 0x15

    if-ne v3, v4, :cond_3

    .line 140
    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mFromSelector:Z

    .line 142
    const-string v3, "FILE_OPERATION"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_3

    .line 144
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    const-string v4, "audio/*;video/*"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->addExceptFilterMimeType(Ljava/lang/String;)V

    .line 146
    const-string v3, "CONTENT_TYPE"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_3

    .line 148
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->setEnableFiltering(Z)V

    .line 150
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->setAllAttachMode(Z)V

    .line 156
    :cond_3
    const-string v3, "search_option_type"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mSearchFromIndex:I

    .line 158
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "FOLDERPATH"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 159
    if-eqz v2, :cond_4

    .line 161
    const-string v3, "/Dropbox"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 162
    const-string v2, ""

    .line 167
    :goto_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 169
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z

    .line 171
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxRoot:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxPathIndicator:Ljava/lang/String;

    .line 175
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 176
    .local v1, "context":Landroid/content/Context;
    if-eqz v1, :cond_5

    .line 177
    invoke-static {v1}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    .line 178
    :cond_5
    return-void

    .line 164
    .end local v1    # "context":Landroid/content/Context;
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxRootReplace:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method protected onCreateDetailFragmentListener()Landroid/app/DialogFragment;
    .locals 1

    .prologue
    .line 1422
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/DropboxDetailFragment;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/fragment/DropboxDetailFragment;-><init>()V

    return-object v0
.end method

.method protected onCreateFileOperationListener()Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;
    .locals 1

    .prologue
    .line 1323
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment$5;-><init>(Lcom/sec/android/app/myfiles/fragment/CloudFragment;)V

    return-object v0
.end method

.method protected onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 1000
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment$3;-><init>(Lcom/sec/android/app/myfiles/fragment/CloudFragment;)V

    return-object v0
.end method

.method protected onCreateItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;
    .locals 1

    .prologue
    .line 1238
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment$4;-><init>(Lcom/sec/android/app/myfiles/fragment/CloudFragment;)V

    return-object v0
.end method

.method protected onCreateMultipleDetailFragmentListener()Landroid/app/DialogFragment;
    .locals 1

    .prologue
    .line 1428
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/MultipleDropboxDetailFragment;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/fragment/MultipleDropboxDetailFragment;-><init>()V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 184
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 186
    .local v0, "view":Landroid/view/View;
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mFromSelector:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getSelectionType()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 187
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mCreateFolderContainer:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 190
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mPathChangeListener:Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setOnPathChangeListener(Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;)V

    .line 193
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxPathIndicator:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 195
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxRoot:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxPathIndicator:Ljava/lang/String;

    .line 202
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxPathIndicator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 203
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxPathIndicator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setCurrentDirectory(Ljava/lang/String;)V

    .line 205
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    if-eqz v1, :cond_2

    .line 206
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    invoke-virtual {v1, p0}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->setOnDropListener(Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;)V

    .line 210
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 212
    return-object v0

    .line 199
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxRoot:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxPathIndicator:Ljava/lang/String;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 412
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onDestroy()V

    .line 415
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v0, :cond_0

    .line 417
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->closeCursor()V

    .line 421
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 9
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v6, -0x1

    const/4 v4, 0x1

    .line 575
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    .line 695
    :goto_0
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v4

    :cond_0
    :goto_1
    return v4

    .line 579
    :sswitch_0
    new-instance v1, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    invoke-direct {v1}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;-><init>()V

    .line 581
    .local v1, "createFolder":Landroid/app/DialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 583
    .local v0, "argument":Landroid/os/Bundle;
    const-string v5, "title"

    const v6, 0x7f0b0019

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 585
    const-string v5, "kind_of_operation"

    invoke-virtual {v0, v5, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 588
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v2

    .line 590
    .local v2, "currentfolder":Ljava/lang/String;
    if-nez v2, :cond_1

    .line 591
    const-string v2, "/"

    .line 593
    :cond_1
    const-string v5, "current_folder"

    invoke-virtual {v0, v5, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 595
    const-string v5, "src_fragment_id"

    invoke-virtual {v0, v5, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 597
    invoke-virtual {v1, v0}, Landroid/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 599
    invoke-virtual {v1, p0, v7}, Landroid/app/DialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 601
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const-string v6, "createFolder"

    invoke-virtual {v1, v5, v6}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1

    .line 608
    .end local v0    # "argument":Landroid/os/Bundle;
    .end local v1    # "createFolder":Landroid/app/DialogFragment;
    .end local v2    # "currentfolder":Ljava/lang/String;
    :sswitch_1
    const/4 v5, 0x0

    invoke-virtual {p0, v4, v6, v5}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->startSelectMode(III)V

    goto :goto_1

    .line 616
    :sswitch_2
    const/4 v5, 0x5

    invoke-virtual {p0, v4, v6, v5}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->startSelectMode(III)V

    goto :goto_1

    .line 623
    :sswitch_3
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-le v5, v4, :cond_2

    .line 624
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->addCurrentFolderToShortcut()V

    goto :goto_1

    .line 626
    :cond_2
    invoke-virtual {p0, v8}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->showDialog(I)V

    goto :goto_1

    .line 631
    :sswitch_4
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    if-eqz v5, :cond_0

    .line 632
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->requestSignIn()V

    goto :goto_1

    .line 637
    :sswitch_5
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretMode(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 639
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->moveDropboxToSecretBox()V

    goto :goto_1

    .line 643
    :cond_3
    :try_start_0
    new-instance v5, Lcom/sec/android/app/myfiles/fragment/CloudFragment$2;

    invoke-direct {v5, p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment$2;-><init>(Lcom/sec/android/app/myfiles/fragment/CloudFragment;)V

    iput-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mPrivateModeClient:Lcom/samsung/android/privatemode/IPrivateModeClient;

    .line 681
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mPrivateModeClient:Lcom/samsung/android/privatemode/IPrivateModeClient;

    invoke-static {v5, v6}, Lcom/samsung/android/privatemode/PrivateModeManager;->getInstance(Landroid/content/Context;Lcom/samsung/android/privatemode/IPrivateModeClient;)Lcom/samsung/android/privatemode/PrivateModeManager;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 684
    :catch_0
    move-exception v3

    .line 685
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 693
    .end local v3    # "e":Ljava/lang/Exception;
    :sswitch_6
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxCopyMoveSourceFolderPath:Ljava/lang/String;

    goto/16 :goto_0

    .line 575
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0f0122 -> :sswitch_6
        0x7f0f0123 -> :sswitch_6
        0x7f0f0124 -> :sswitch_5
        0x7f0f0129 -> :sswitch_3
        0x7f0f0137 -> :sswitch_1
        0x7f0f0138 -> :sswitch_2
        0x7f0f013a -> :sswitch_0
        0x7f0f013e -> :sswitch_4
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 16
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 427
    const/4 v4, 0x0

    .line 429
    .local v4, "bResult":Z
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    if-eqz v14, :cond_0

    .line 430
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->isDropboxAccountIsSignin()Z

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;

    invoke-virtual {v15}, Lcom/sec/android/app/myfiles/cloud/CloudActivity$DropboxAPIHelper;->isAuthKeySaved()Z

    move-result v15

    and-int v4, v14, v15

    .line 433
    :cond_0
    invoke-super/range {p0 .. p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 435
    if-eqz v4, :cond_9

    .line 437
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCount()I

    move-result v14

    if-eqz v14, :cond_2

    .line 439
    const v14, 0x7f0f0137

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v8

    .line 441
    .local v8, "selectFileMenu":Landroid/view/MenuItem;
    if-eqz v8, :cond_1

    .line 443
    const/4 v14, 0x1

    invoke-interface {v8, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 446
    :cond_1
    const v14, 0x7f0f0138

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    .line 448
    .local v7, "normalDeleteMenu":Landroid/view/MenuItem;
    if-eqz v7, :cond_2

    .line 451
    const/4 v14, 0x1

    invoke-interface {v7, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 457
    .end local v7    # "normalDeleteMenu":Landroid/view/MenuItem;
    .end local v8    # "selectFileMenu":Landroid/view/MenuItem;
    :cond_2
    const v14, 0x7f0f013d

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v9

    .line 459
    .local v9, "settingMenu":Landroid/view/MenuItem;
    if-eqz v9, :cond_3

    .line 461
    const/4 v14, 0x0

    invoke-interface {v9, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 465
    :cond_3
    const v14, 0x7f0f012b

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v13

    .line 467
    .local v13, "zipMenu":Landroid/view/MenuItem;
    if-eqz v13, :cond_4

    .line 469
    const/4 v14, 0x0

    invoke-interface {v13, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 472
    :cond_4
    const v14, 0x7f0f012c

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 474
    .local v1, "UnzipMenu":Landroid/view/MenuItem;
    if-eqz v1, :cond_5

    .line 476
    const/4 v14, 0x0

    invoke-interface {v1, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 480
    :cond_5
    const v14, 0x7f0f012d

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 482
    .local v2, "UnzipMenuHere":Landroid/view/MenuItem;
    if-eqz v2, :cond_6

    .line 484
    const/4 v14, 0x0

    invoke-interface {v2, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 487
    :cond_6
    const v14, 0x7f0f013a

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    .line 489
    .local v5, "createFolderMenu":Landroid/view/MenuItem;
    if-eqz v5, :cond_7

    .line 491
    const/4 v14, 0x1

    invoke-interface {v5, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 564
    :cond_7
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mFromSelector:Z

    if-eqz v14, :cond_8

    const v14, 0x7f0f013a

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v14

    if-eqz v14, :cond_8

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v14}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v14

    if-nez v14, :cond_8

    .line 565
    const v14, 0x7f0f013a

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    .line 566
    .local v6, "item":Landroid/view/MenuItem;
    const/4 v14, 0x0

    invoke-interface {v6, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 569
    .end local v6    # "item":Landroid/view/MenuItem;
    :cond_8
    return-void

    .line 496
    .end local v1    # "UnzipMenu":Landroid/view/MenuItem;
    .end local v2    # "UnzipMenuHere":Landroid/view/MenuItem;
    .end local v5    # "createFolderMenu":Landroid/view/MenuItem;
    .end local v9    # "settingMenu":Landroid/view/MenuItem;
    .end local v13    # "zipMenu":Landroid/view/MenuItem;
    :cond_9
    const v14, 0x7f0f013d

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v9

    .line 498
    .restart local v9    # "settingMenu":Landroid/view/MenuItem;
    if-eqz v9, :cond_a

    .line 500
    const/4 v14, 0x0

    invoke-interface {v9, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 504
    :cond_a
    const v14, 0x7f0f012b

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v13

    .line 506
    .restart local v13    # "zipMenu":Landroid/view/MenuItem;
    if-eqz v13, :cond_b

    .line 508
    const/4 v14, 0x0

    invoke-interface {v13, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 511
    :cond_b
    const v14, 0x7f0f012c

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 513
    .restart local v1    # "UnzipMenu":Landroid/view/MenuItem;
    if-eqz v1, :cond_c

    .line 515
    const/4 v14, 0x0

    invoke-interface {v1, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 519
    :cond_c
    const v14, 0x7f0f012d

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 521
    .restart local v2    # "UnzipMenuHere":Landroid/view/MenuItem;
    if-eqz v2, :cond_d

    .line 523
    const/4 v14, 0x0

    invoke-interface {v2, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 526
    :cond_d
    const v14, 0x7f0f013a

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    .line 528
    .restart local v5    # "createFolderMenu":Landroid/view/MenuItem;
    if-eqz v5, :cond_e

    .line 530
    const/4 v14, 0x0

    invoke-interface {v5, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 534
    :cond_e
    const v14, 0x7f0f013b

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v12

    .line 536
    .local v12, "viewAsMenu":Landroid/view/MenuItem;
    if-eqz v12, :cond_f

    .line 538
    const/4 v14, 0x0

    invoke-interface {v12, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 541
    :cond_f
    const v14, 0x7f0f013c

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v11

    .line 543
    .local v11, "sortByMenu":Landroid/view/MenuItem;
    if-eqz v11, :cond_10

    .line 545
    const/4 v14, 0x0

    invoke-interface {v11, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 548
    :cond_10
    const v14, 0x7f0f0129

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 550
    .local v3, "addShortcutMenu":Landroid/view/MenuItem;
    if-eqz v3, :cond_11

    .line 552
    const/4 v14, 0x0

    invoke-interface {v3, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 555
    :cond_11
    const v14, 0x7f0f013e

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v10

    .line 557
    .local v10, "signInMenu":Landroid/view/MenuItem;
    if-eqz v10, :cond_7

    .line 559
    const/4 v14, 0x1

    invoke-interface {v10, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_0
.end method

.method public onRefresh()V
    .locals 3

    .prologue
    .line 274
    const/4 v0, 0x0

    const-string v1, "DropboxFragment"

    const-string v2, "refresh"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->refreshNavigation()V

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 279
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 263
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onResume()V

    .line 265
    const/4 v0, 0x0

    const-string v1, "DropboxFragment"

    const-string v2, "onResume"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 267
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->showPathIndicator(Z)V

    .line 268
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 217
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 219
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 221
    .local v0, "argument":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    const-string v1, "run_from"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const/16 v2, 0x15

    if-ne v1, v2, :cond_0

    .line 224
    const-string v1, "selection_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->startSelectMode(III)V

    .line 229
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    if-eqz v1, :cond_1

    .line 231
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    const-string v2, "8"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->setCategoryItemListSelection(Ljava/lang/String;)I

    .line 233
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    new-instance v2, Lcom/sec/android/app/myfiles/fragment/CloudFragment$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/CloudFragment;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->post(Ljava/lang/Runnable;)Z

    .line 259
    :cond_1
    return-void
.end method

.method protected selectAllItem()V
    .locals 2

    .prologue
    .line 839
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->selectAllItem()V

    .line 840
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 841
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getSelectModeFrom()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 842
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->selectAllFileItem()V

    .line 847
    :cond_0
    :goto_0
    return-void

    .line 844
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->selectAllItem()V

    goto :goto_0
.end method

.method public selectmodeActionbar()V
    .locals 7

    .prologue
    const v6, 0x7f0b0014

    const v5, 0x7f0b0013

    const/4 v3, 0x1

    const/4 v2, 0x0

    const v4, 0x7f0b0018

    .line 1442
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 1443
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 1444
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 1445
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 1447
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getSelectionType()I

    move-result v1

    if-eq v1, v3, :cond_3

    .line 1448
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040008

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1450
    .local v0, "customview":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 1451
    const v1, 0x7f0f0008

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mActionbar_cancel:Landroid/widget/TextView;

    .line 1452
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mActionbar_cancel:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0b0017

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1453
    const v1, 0x7f0f0009

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mActionbar_ok:Landroid/widget/TextView;

    .line 1454
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mActionbar_ok:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0b0016

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1457
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/SelectorActivity;

    if-eqz v1, :cond_0

    .line 1458
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->getmFileOperation()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 1459
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->getmFileOperation()I

    move-result v1

    if-nez v1, :cond_2

    .line 1460
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mActionbar_ok:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1461
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mActionbar_ok:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v5}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1468
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mActionbar_cancel:Landroid/widget/TextView;

    new-instance v2, Lcom/sec/android/app/myfiles/fragment/CloudFragment$7;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment$7;-><init>(Lcom/sec/android/app/myfiles/fragment/CloudFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1490
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mActionbar_ok:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 1491
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mActionbar_ok:Landroid/widget/TextView;

    new-instance v2, Lcom/sec/android/app/myfiles/fragment/CloudFragment$8;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment$8;-><init>(Lcom/sec/android/app/myfiles/fragment/CloudFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1503
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v1, :cond_1

    .line 1504
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mActionbar_ok:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1507
    :cond_1
    return-void

    .line 1463
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mActionbar_ok:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(I)V

    .line 1464
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mActionbar_ok:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v6}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1485
    .end local v0    # "customview":Landroid/view/View;
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/high16 v2, 0x7f040000

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1486
    .restart local v0    # "customview":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 1487
    const v1, 0x7f0f0003

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mSelectAllButton:Landroid/widget/TextView;

    goto :goto_1
.end method

.method public setPathIndicator(Ljava/lang/String;)V
    .locals 2
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 1548
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxPathIndicator:Ljava/lang/String;

    .line 1549
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    if-eqz v0, :cond_0

    .line 1550
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mDropboxPathIndicator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 1551
    :cond_0
    return-void
.end method

.method protected unselectAllItem()V
    .locals 1

    .prologue
    .line 852
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->unselectAllItem()V

    .line 854
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 856
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CloudFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->unselectAllItem()V

    .line 858
    :cond_0
    return-void
.end method
