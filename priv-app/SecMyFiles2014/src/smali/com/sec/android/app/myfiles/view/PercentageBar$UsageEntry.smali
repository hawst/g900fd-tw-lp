.class public Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;
.super Ljava/lang/Object;
.source "PercentageBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/view/PercentageBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UsageEntry"
.end annotation


# instance fields
.field public final mBlockDrawable:Landroid/graphics/drawable/Drawable;

.field public final mPercentage:F


# direct methods
.method protected constructor <init>(FLandroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "percentage"    # F
    .param p2, "blockDrawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 489
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 491
    iput p1, p0, Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;->mPercentage:F

    .line 493
    iput-object p2, p0, Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;->mBlockDrawable:Landroid/graphics/drawable/Drawable;

    .line 494
    return-void
.end method
