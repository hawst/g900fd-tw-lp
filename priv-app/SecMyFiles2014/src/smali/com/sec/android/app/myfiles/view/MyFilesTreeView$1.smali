.class Lcom/sec/android/app/myfiles/view/MyFilesTreeView$1;
.super Ljava/lang/Object;
.source "MyFilesTreeView.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/view/MyFilesTreeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/view/MyFilesTreeView;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView$1;->this$0:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public refresh(Lcom/sec/android/app/myfiles/navigation/AbsNavigation;)V
    .locals 1
    .param p1, "navigation"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .prologue
    .line 156
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView$1;->this$0:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    # getter for: Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->mAdapter:Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->access$000(Lcom/sec/android/app/myfiles/view/MyFilesTreeView;)Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;->refresh(Lcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 158
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView$1;->update(Lcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 159
    return-void
.end method

.method public update(Lcom/sec/android/app/myfiles/navigation/AbsNavigation;)V
    .locals 4
    .param p1, "navigation"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .prologue
    .line 135
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView$1;->this$0:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    # getter for: Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->mAdapter:Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->access$000(Lcom/sec/android/app/myfiles/view/MyFilesTreeView;)Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;

    move-result-object v2

    instance-of v2, v2, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    if-eqz v2, :cond_1

    .line 137
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v0

    .line 139
    .local v0, "currentPath":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView$1;->this$0:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    # getter for: Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->mAdapter:Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->access$000(Lcom/sec/android/app/myfiles/view/MyFilesTreeView;)Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->goTo(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 141
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView$1;->this$0:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    # getter for: Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->mAdapter:Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->access$000(Lcom/sec/android/app/myfiles/view/MyFilesTreeView;)Lcom/sec/android/app/myfiles/adapter/TreeViewAdapter;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->getCurrentItemIndex()I

    move-result v1

    .line 143
    .local v1, "index":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView$1;->this$0:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->getFirstVisiblePosition()I

    move-result v2

    if-lt v1, v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView$1;->this$0:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->getLastVisiblePosition()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-le v1, v2, :cond_1

    .line 146
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/MyFilesTreeView$1;->this$0:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->setSelection(I)V

    .line 150
    .end local v0    # "currentPath":Ljava/lang/String;
    .end local v1    # "index":I
    :cond_1
    return-void
.end method
