.class Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$2;
.super Ljava/lang/Object;
.source "SelectorActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->finishUpdate(Landroid/view/ViewGroup;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;)V
    .locals 0

    .prologue
    .line 1357
    iput-object p1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$2;->this$1:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1362
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$2;->this$1:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllCheckBox:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1364
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$2;->this$1:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllCheckBox:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 1365
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$2;->this$1:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->selectItemControl(I)V

    .line 1371
    :goto_0
    return-void

    .line 1368
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$2;->this$1:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllCheckBox:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 1369
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter$2;->this$1:Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity$TabsAdapter;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->selectItemControl(I)V

    goto :goto_0
.end method
