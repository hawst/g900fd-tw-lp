.class Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$14;
.super Ljava/lang/Object;
.source "AbsListableFragment.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;)V
    .locals 0

    .prologue
    .line 1004
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$14;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1009
    const/4 v3, 0x0

    .line 1011
    .local v3, "result":Z
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    if-nez v4, :cond_0

    const/16 v4, 0x3e

    if-ne p2, v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$14;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->isSelectMode()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1013
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$14;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getSelectedView()Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$14;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getSelectedView()Landroid/view/View;

    move-result-object v1

    .line 1015
    .local v1, "child":Landroid/view/View;
    :goto_0
    if-eqz v1, :cond_0

    .line 1017
    const v4, 0x7f0f0037

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1019
    .local v0, "check_container":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1021
    invoke-virtual {v0, v5}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    .line 1023
    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    .line 1025
    invoke-virtual {v0, v6}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    .line 1027
    const/4 v3, 0x1

    .line 1047
    .end local v0    # "check_container":Landroid/view/View;
    .end local v1    # "child":Landroid/view/View;
    :cond_0
    :goto_1
    return v3

    .line 1013
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment$14;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->getSelectedView()Landroid/view/View;

    move-result-object v1

    goto :goto_0

    .line 1031
    .restart local v0    # "check_container":Landroid/view/View;
    .restart local v1    # "child":Landroid/view/View;
    :cond_2
    const v4, 0x7f0f0038

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1033
    .local v2, "radio_container":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 1035
    invoke-virtual {v2, v5}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    .line 1037
    invoke-virtual {v2}, Landroid/view/View;->performClick()Z

    .line 1039
    invoke-virtual {v2, v6}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    .line 1041
    const/4 v3, 0x1

    goto :goto_1
.end method
