.class public Lcom/sec/android/app/myfiles/utils/MyFilesDB;
.super Ljava/lang/Object;
.source "MyFilesDB.java"


# static fields
.field private static final EXTENSION_FILTER:Ljava/lang/String; = "\"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890;!@#$%^&()-_=+\""

.field private static final FILE_NAME_FILTER:Ljava/lang/String; = "\"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890\' ;.!@#$%^&()-_=+\""

.field private static final MODULE:Ljava/lang/String; = "MyFilesDB"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getIntentForQueryInformation(Landroid/content/Context;I)Landroid/content/Intent;
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "category"    # I

    .prologue
    .line 104
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 106
    .local v1, "intent":Landroid/content/Intent;
    const/4 v8, 0x0

    .line 108
    .local v8, "uri":Landroid/net/Uri;
    const/4 v2, 0x0

    .line 110
    .local v2, "projection":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 112
    .local v5, "selection":Ljava/lang/String;
    const/4 v6, 0x0

    .line 114
    .local v6, "selectionArgs":[Ljava/lang/String;
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v4

    .line 116
    .local v4, "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentSortBy()I

    move-result v9

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentInOrder()I

    move-result v10

    const/4 v11, 0x1

    const/4 v12, 0x0

    invoke-static {v9, v10, v11, v12}, Lcom/sec/android/app/myfiles/utils/MyFilesDB;->getOrderBy(IIIZ)Ljava/lang/String;

    move-result-object v7

    .line 119
    .local v7, "sortOrder":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 161
    :goto_0
    if-eqz v8, :cond_0

    .line 163
    const-string v9, "query_uri"

    invoke-virtual {v1, v9, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 165
    const-string v9, "query_projection"

    invoke-virtual {v1, v9, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 167
    const-string v9, "query_selection"

    invoke-virtual {v1, v9, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 169
    const-string v9, "query_selectionArgs"

    invoke-virtual {v1, v9, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 171
    const-string v9, "query_sortOrder"

    invoke-virtual {v1, v9, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 174
    :cond_0
    return-object v1

    .line 122
    :pswitch_0
    sget-object v8, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 123
    goto :goto_0

    .line 126
    :pswitch_1
    sget-object v8, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 127
    goto :goto_0

    .line 130
    :pswitch_2
    sget-object v8, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 131
    goto :goto_0

    .line 134
    :pswitch_3
    const-string v9, "external"

    invoke-static {v9}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 136
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 138
    .local v3, "sb":Ljava/lang/StringBuilder;
    invoke-static {}, Lcom/sec/android/app/myfiles/MediaFile;->getDocumentExtensions()[Ljava/lang/String;

    move-result-object v6

    .line 140
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v9, v6

    if-ge v0, v9, :cond_1

    .line 142
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "%."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    aget-object v10, v6, v0

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v0

    .line 140
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 145
    :cond_1
    const/4 v0, 0x0

    :goto_2
    array-length v9, v6

    if-ge v0, v9, :cond_3

    .line 147
    array-length v9, v6

    add-int/lit8 v9, v9, -0x1

    if-ge v0, v9, :cond_2

    .line 149
    const-string v9, "_data LIKE ? OR "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 153
    :cond_2
    const-string v9, "_data LIKE ?"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 157
    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 119
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getOrderBy(IIIZ)Ljava/lang/String;
    .locals 5
    .param p0, "sortBy"    # I
    .param p1, "inOrder"    # I
    .param p2, "target"    # I
    .param p3, "ismusic"    # Z

    .prologue
    const/4 v3, 0x1

    .line 43
    const-string v0, "_data"

    .line 45
    .local v0, "orderBy":Ljava/lang/String;
    if-nez p1, :cond_0

    const-string v1, " ASC"

    .line 48
    .local v1, "strInOrder":Ljava/lang/String;
    :goto_0
    if-eqz p0, :cond_1

    const/4 v2, 0x2

    if-ne p2, v2, :cond_1

    .line 50
    const/4 v2, 0x0

    const-string v3, "MyFilesDB"

    const-string v4, "The type of sort and target is mismatch"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 52
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 98
    :goto_1
    return-object v2

    .line 45
    .end local v1    # "strInOrder":Ljava/lang/String;
    :cond_0
    const-string v1, " DESC"

    goto :goto_0

    .line 56
    .restart local v1    # "strInOrder":Ljava/lang/String;
    :cond_1
    packed-switch p0, :pswitch_data_0

    :goto_2
    move-object v2, v0

    .line 98
    goto :goto_1

    .line 59
    :pswitch_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "date_modified"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 60
    goto :goto_2

    .line 63
    :pswitch_1
    if-ne p2, v3, :cond_2

    .line 65
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SUBSTR(LOWER(_data),LENGTH(RTRIM(LOWER(_data),\"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890;!@#$%^&()-_=+\"))+1) COLLATE LOCALIZED"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 69
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " COLLATE LOCALIZED"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 71
    goto :goto_2

    .line 75
    :pswitch_2
    if-eqz p3, :cond_3

    .line 77
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_display_name COLLATE LOCALIZED"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 81
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "title COLLATE LOCALIZED"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 84
    goto :goto_2

    .line 87
    :pswitch_3
    if-ne p2, v3, :cond_4

    .line 89
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_size"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 93
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " COLLATE LOCALIZED ASC"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    .line 56
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
