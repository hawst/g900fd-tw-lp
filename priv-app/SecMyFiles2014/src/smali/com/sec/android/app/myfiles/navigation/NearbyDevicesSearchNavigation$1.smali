.class Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$1;
.super Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;
.source "NearbyDevicesSearchNavigation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->AsyncSearch()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;Lcom/sec/android/app/myfiles/fragment/AbsFragment;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    invoke-direct {p0, p2}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsFragment;)V

    return-void
.end method


# virtual methods
.method public AsyncOpenExcute()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 113
    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$1;->setCompletedCondition(Z)V

    .line 114
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mSearchParameter:Lcom/sec/android/app/myfiles/utils/SearchParameter;

    if-eqz v1, :cond_0

    .line 115
    new-instance v1, Lcom/samsung/android/allshare/media/SearchCriteria$Builder;

    invoke-direct {v1}, Lcom/samsung/android/allshare/media/SearchCriteria$Builder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mSearchParameter:Lcom/sec/android/app/myfiles/utils/SearchParameter;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getKeyword()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/media/SearchCriteria$Builder;->setKeyword(Ljava/lang/String;)Lcom/samsung/android/allshare/media/SearchCriteria$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/media/SearchCriteria$Builder;->build()Lcom/samsung/android/allshare/media/SearchCriteria;

    move-result-object v0

    .line 118
    .local v0, "sc":Lcom/samsung/android/allshare/media/SearchCriteria;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    const/16 v2, 0x32

    invoke-virtual {v1, v0, v3, v2}, Lcom/samsung/android/allshare/media/Provider;->search(Lcom/samsung/android/allshare/media/SearchCriteria;II)V

    .line 120
    .end local v0    # "sc":Lcom/samsung/android/allshare/media/SearchCriteria;
    :cond_0
    return-void
.end method
