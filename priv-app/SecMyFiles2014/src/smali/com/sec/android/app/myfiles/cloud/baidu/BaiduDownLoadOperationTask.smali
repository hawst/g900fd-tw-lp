.class public Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;
.super Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;
.source "BaiduDownLoadOperationTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;,
        Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;,
        Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;,
        Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$FileUploadRunnable;,
        Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$FileDownloadRunnable;
    }
.end annotation


# static fields
.field private static final DEBUG:Z

.field private static final DOWNLOAD:I = 0x0

.field private static final INSIDE_COPY:I = 0x2

.field private static final MAX_PARALLEL_LOAD:I = 0x64

.field private static final MODULE:Ljava/lang/String; = "BaiduDownLoadOperationTask"

.field private static final UPLOAD:I = 0x1


# instance fields
.field private CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

.field private mCopySummarySize:J

.field private mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

.field private mErrorCode:I

.field private mFileLoadThreads:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Thread;",
            ">;"
        }
    .end annotation
.end field

.field private mFiles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;",
            ">;"
        }
    .end annotation
.end field

.field private mOperationDirection:I

.field private mTotalSize:J

.field private mUpFiles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;",
            ">;"
        }
    .end annotation
.end field

.field private mUsedFilesNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 78
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/view/View;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contentType"    # I
    .param p3, "progressView"    # Landroid/view/View;
    .param p4, "operation"    # I

    .prologue
    const-wide/16 v4, 0x0

    const/16 v1, 0x1f

    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 200
    invoke-direct {p0, p1, p4, p2, p3}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;-><init>(Landroid/content/Context;IILandroid/view/View;)V

    .line 60
    iput-wide v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTotalSize:J

    .line 62
    iput-wide v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mCopySummarySize:J

    .line 64
    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    .line 66
    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    .line 68
    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    .line 70
    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFileLoadThreads:Ljava/util/ArrayList;

    .line 72
    iput v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mErrorCode:I

    .line 74
    iput v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOperationDirection:I

    .line 76
    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    .line 201
    iput p2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mSourceFragmentId:I

    .line 202
    invoke-static {p1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    .line 203
    iget v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mSourceFragmentId:I

    if-ne v0, v1, :cond_0

    .line 204
    const/16 v0, 0x201

    iput v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetFragmentId:I

    .line 207
    :goto_0
    const-string v0, "BaiduDownLoadOperationTask"

    const-string v1, "1. BaiduDownLoadOperationTask"

    invoke-static {v2, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->setToContinue(Z)V

    .line 210
    return-void

    .line 206
    :cond_0
    iput v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetFragmentId:I

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/view/View;II)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contentType"    # I
    .param p3, "progressView"    # Landroid/view/View;
    .param p4, "contentTypeOfTarget"    # I
    .param p5, "operation"    # I

    .prologue
    .line 214
    invoke-direct {p0, p1, p2, p3, p5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;-><init>(Landroid/content/Context;ILandroid/view/View;I)V

    .line 215
    iput p4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetFragmentId:I

    .line 216
    const/4 v0, 0x0

    const-string v1, "BaiduDownLoadOperationTask"

    const-string v2, "2. BaiduDownLoadOperationTask"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 218
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->setToContinue(Z)V

    .line 219
    return-void
.end method

.method private BaiduCopyOrMove(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "fromPath"    # Ljava/lang/String;
    .param p2, "toPath"    # Ljava/lang/String;

    .prologue
    .line 902
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 923
    :cond_0
    :goto_0
    return-void

    .line 904
    :cond_1
    iget-boolean v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOverwrite:Z

    if-eqz v4, :cond_2

    .line 905
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v4, p2}, Lcom/sec/android/app/myfiles/db/CacheDB;->getIfDropboxFileExist(Ljava/lang/String;)Z

    move-result v1

    .line 906
    .local v1, "fileExists":Z
    if-eqz v1, :cond_2

    .line 907
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    invoke-virtual {v4, p2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->deleteFile(Ljava/lang/String;)V

    .line 910
    .end local v1    # "fileExists":Z
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    iget v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOperationType:I

    invoke-virtual {v4, p1, p2, v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->fileCopyOrMove(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    .line 911
    .local v0, "copied":Z
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    if-eqz v4, :cond_3

    .line 912
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    invoke-virtual {v4, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 913
    :cond_3
    if-nez v0, :cond_4

    .line 914
    const/4 v4, 0x2

    iput v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mErrorCode:I

    goto :goto_0

    .line 917
    :cond_4
    iget-wide v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mCopySummarySize:J

    long-to-double v4, v4

    iget-wide v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTotalSize:J

    long-to-double v6, v6

    div-double/2addr v4, v6

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    mul-double v2, v4, v6

    .line 919
    .local v2, "progress":D
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    double-to-int v5, v2

    iput v5, v4, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mCurrentTargetPercentage:I

    .line 921
    const/4 v4, 0x1

    new-array v4, v4, [Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    aput-object v6, v4, v5

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->publishProgress([Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private BaiduDownload(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10
    .param p1, "dstFolder"    # Ljava/lang/String;
    .param p2, "serverPath"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 575
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->isCancelled()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 606
    :cond_0
    :goto_0
    return v7

    .line 579
    :cond_1
    const-string v0, ""

    .line 580
    .local v0, "fileName":Ljava/lang/String;
    const-string v5, "/"

    invoke-virtual {p2, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    .line 581
    .local v4, "sPos":I
    const/4 v5, -0x1

    if-eq v4, v5, :cond_2

    .line 582
    add-int/lit8 v5, v4, 0x1

    invoke-virtual {p2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 583
    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_4

    .line 584
    :cond_3
    move-object v0, p2

    .line 586
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v5, p2}, Lcom/sec/android/app/myfiles/db/CacheDB;->isDirectory(Ljava/lang/String;)Z

    move-result v1

    .line 587
    .local v1, "isDirectory":Z
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "/"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 588
    .local v3, "newFilePath":Ljava/lang/String;
    if-nez v1, :cond_6

    move v5, v6

    :goto_1
    invoke-virtual {p0, v3, v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->checkDirOrFileName(Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v2

    .line 589
    .local v2, "newFile":Ljava/io/File;
    if-eqz v2, :cond_0

    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mRenameCancelled:Z

    if-nez v5, :cond_0

    .line 591
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    .line 592
    if-eqz v1, :cond_7

    .line 593
    invoke-direct {p0, v3, p2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->downloadFolder(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    :goto_2
    move v7, v6

    .line 606
    goto :goto_0

    .end local v2    # "newFile":Ljava/io/File;
    :cond_6
    move v5, v7

    .line 588
    goto :goto_1

    .line 595
    .restart local v2    # "newFile":Ljava/io/File;
    :cond_7
    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    if-eqz v5, :cond_5

    .line 596
    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    new-instance v7, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v8, p2}, Lcom/sec/android/app/myfiles/db/CacheDB;->getCloudFileSize(Ljava/lang/String;)J

    move-result-wide v8

    invoke-direct {v7, p2, v8, v9, v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 597
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 598
    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOverwrite:Z

    if-eqz v5, :cond_8

    .line 599
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_2

    .line 602
    :cond_8
    iput v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mErrorCode:I

    goto :goto_2
.end method

.method private BaiduFolderUpload(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V
    .locals 15
    .param p1, "localFile"    # Ljava/io/File;
    .param p2, "serverPath"    # Ljava/lang/String;
    .param p3, "NewFolderName"    # Ljava/lang/String;

    .prologue
    .line 776
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 815
    :cond_0
    :goto_0
    return-void

    .line 779
    :cond_1
    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 784
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_2

    .line 786
    const-string p2, "/"

    .line 789
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    move-object/from16 v0, p3

    move-object/from16 v1, p2

    invoke-virtual {v3, v0, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->createFolder(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;

    move-result-object v9

    .line 790
    .local v9, "folder":Lcom/samsung/scloud/data/SCloudFolder;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "/"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 791
    if-nez v9, :cond_3

    .line 792
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->getFolderInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;

    move-result-object v9

    .line 793
    if-nez v9, :cond_3

    .line 794
    const/4 v3, 0x3

    iput v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mErrorCode:I

    goto :goto_0

    .line 798
    :cond_3
    if-eqz v9, :cond_4

    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    if-eqz v3, :cond_4

    .line 799
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 801
    :cond_4
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v12

    .line 802
    .local v12, "subFiles":[Ljava/io/File;
    if-eqz v12, :cond_0

    array-length v3, v12

    if-eqz v3, :cond_0

    .line 803
    move-object v2, v12

    .local v2, "arr$":[Ljava/io/File;
    array-length v11, v2

    .local v11, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    :goto_1
    if-ge v10, v11, :cond_0

    aget-object v4, v2, v10

    .line 804
    .local v4, "sf":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 805
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-direct {p0, v4, v0, v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->BaiduFolderUpload(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    .line 803
    :cond_5
    :goto_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 807
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    if-eqz v3, :cond_5

    .line 808
    iget-object v13, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    new-instance v3, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v6

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v14, "/"

    invoke-virtual {v8, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;-><init>(Ljava/io/File;Ljava/lang/String;JLjava/lang/String;)V

    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 809
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    if-eqz v3, :cond_5

    .line 810
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method private BaiduUpload(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "localPath"    # Ljava/lang/String;
    .param p2, "serverPath"    # Ljava/lang/String;

    .prologue
    .line 752
    const-string v0, "Baidu/"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 753
    const/4 v0, 0x6

    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p2

    .line 755
    :cond_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 756
    .local v2, "localFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    .line 759
    .local v3, "newName":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v0

    invoke-direct {p0, p2, v3, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->checkCloudDirOrFileName(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    .line 760
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 761
    :cond_1
    const/4 v0, 0x0

    .line 771
    :goto_0
    return v0

    .line 762
    :cond_2
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 763
    invoke-direct {p0, v2, p2, v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->BaiduFolderUpload(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    :cond_3
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 765
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 766
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;-><init>(Ljava/io/File;Ljava/lang/String;JLjava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 767
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 768
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "/"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private CopyOrMoveFiles(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 7
    .param p2, "dstFolder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "srcFilesPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v6, 0x0

    .line 1028
    const-string v4, "BaiduDownLoadOperationTask"

    const-string v5, "Inside baidu copy or move start"

    invoke-static {v6, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 1029
    const-string v4, "Baidu/"

    invoke-virtual {p2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1030
    const/4 v4, 0x6

    invoke-virtual {p2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p2

    .line 1032
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v4, p1}, Lcom/sec/android/app/myfiles/db/CacheDB;->getCloudPathSizes(Ljava/util/ArrayList;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTotalSize:J

    .line 1033
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mCopySummarySize:J

    .line 1034
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    .line 1035
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1036
    .local v2, "filename":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetFolder:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v2, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1038
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1039
    iget v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOverwriteFileCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOverwriteFileCount:I

    goto :goto_0

    .line 1042
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "filename":Ljava/lang/String;
    :cond_2
    iget v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOverwriteFileCount:I

    iput v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTempOverwriteFileCount:I

    .line 1043
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1044
    .local v0, "f":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->isCancelled()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1052
    .end local v0    # "f":Ljava/lang/String;
    :goto_2
    return-void

    .line 1046
    .restart local v0    # "f":Ljava/lang/String;
    :cond_4
    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->insideBaiduCopy(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1047
    iget v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mRemainOverwriteFileCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mRemainOverwriteFileCount:I

    goto :goto_1

    .line 1050
    .end local v0    # "f":Ljava/lang/String;
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 1051
    const-string v4, "BaiduDownLoadOperationTask"

    const-string v5, "Inside baidu copy or move finished"

    invoke-static {v6, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private DownloadFiles(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 8
    .param p2, "dstFolder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 620
    .local p1, "srcFilesPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->isCancelled()Z

    move-result v4

    if-nez v4, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_1

    .line 645
    :cond_0
    :goto_0
    return-void

    .line 622
    :cond_1
    const/4 v4, 0x0

    const-string v5, "BaiduDownLoadOperationTask"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Download start, "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " items"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 623
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v4, p1}, Lcom/sec/android/app/myfiles/db/CacheDB;->getCloudPathSizes(Ljava/util/ArrayList;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTotalSize:J

    .line 624
    invoke-static {p2}, Lcom/sec/android/app/myfiles/utils/Utils;->getAvailableSpace(Ljava/lang/String;)J

    move-result-wide v4

    iget-wide v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTotalSize:J

    cmp-long v4, v4, v6

    if-gtz v4, :cond_2

    .line 625
    const/4 v4, 0x6

    iput v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mErrorCode:I

    goto :goto_0

    .line 628
    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    .line 629
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 630
    .local v2, "filename":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetFolder:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v2, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 632
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 633
    iget v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOverwriteFileCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOverwriteFileCount:I

    goto :goto_1

    .line 636
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "filename":Ljava/lang/String;
    :cond_4
    iget v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOverwriteFileCount:I

    iput v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTempOverwriteFileCount:I

    .line 637
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 638
    .local v0, "f":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->isCancelled()Z

    move-result v4

    if-nez v4, :cond_0

    .line 640
    invoke-direct {p0, p2, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->BaiduDownload(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 641
    iget v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mRemainOverwriteFileCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mRemainOverwriteFileCount:I

    goto :goto_2

    .line 644
    .end local v0    # "f":Ljava/lang/String;
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->startDownloadFiles()V

    goto/16 :goto_0
.end method

.method private StartUploadFiles()V
    .locals 20

    .prologue
    .line 819
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->isCancelled()Z

    move-result v4

    if-nez v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_1

    .line 845
    :cond_0
    return-void

    .line 822
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;

    .line 823
    .local v10, "file":Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v10, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;->mServerPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v10, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;->mNewFileName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 824
    .local v15, "newFilePath":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOverwrite:Z

    if-eqz v4, :cond_2

    .line 825
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v4, v15}, Lcom/sec/android/app/myfiles/db/CacheDB;->getIfDropboxFileExist(Ljava/lang/String;)Z

    move-result v11

    .line 826
    .local v11, "fileExist":Z
    if-eqz v11, :cond_2

    .line 827
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    invoke-virtual {v4, v15}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->deleteFile(Ljava/lang/String;)V

    .line 831
    .end local v11    # "fileExist":Z
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    iget-object v5, v10, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;->mLocalFile:Ljava/io/File;

    iget-object v6, v10, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;->mServerPath:Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v8, v10, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;->mNewFileName:Ljava/lang/String;

    new-instance v9, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTotalSize:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-direct {v9, v0, v1, v2, v10}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;JLcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;)V

    invoke-virtual/range {v4 .. v9}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->upload(Ljava/io/File;Ljava/lang/String;ZLjava/lang/String;Lcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;

    goto :goto_0

    .line 835
    .end local v10    # "file":Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;
    .end local v15    # "newFilePath":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v14

    .line 836
    .local v14, "n":I
    const/16 v4, 0x64

    if-le v14, v4, :cond_4

    .line 837
    const/16 v14, 0x64

    .line 838
    :cond_4
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFileLoadThreads:Ljava/util/ArrayList;

    .line 840
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_1
    if-ge v12, v14, :cond_0

    .line 841
    new-instance v16, Ljava/lang/Thread;

    new-instance v4, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$FileUploadRunnable;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$FileUploadRunnable;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;)V

    move-object/from16 v0, v16

    invoke-direct {v0, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 842
    .local v16, "thread":Ljava/lang/Thread;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFileLoadThreads:Ljava/util/ArrayList;

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 843
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Thread;->start()V

    .line 840
    add-int/lit8 v12, v12, 0x1

    goto :goto_1
.end method

.method private UploadFiles(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 9
    .param p2, "dstFolder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "srcFilesPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v8, 0x1

    .line 862
    const/4 v5, 0x0

    const-string v6, "BaiduDownLoadOperationTask"

    const-string v7, "Upload start"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 864
    iget v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mSourceFragmentId:I

    if-nez v5, :cond_1

    iget v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetFragmentId:I

    const/16 v6, 0x1f

    if-ne v5, v6, :cond_1

    .line 866
    iput v8, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mErrorCode:I

    .line 898
    :cond_0
    :goto_0
    return-void

    .line 870
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mContext:Landroid/content/Context;

    invoke-static {v5, p1}, Lcom/sec/android/app/myfiles/utils/Utils;->getPathFilesSize(Landroid/content/Context;Ljava/util/ArrayList;)J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTotalSize:J

    .line 871
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    .line 872
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    .line 874
    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 875
    .local v3, "filename":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetFolder:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v3, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v3, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 877
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 878
    iget v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOverwriteFileCount:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOverwriteFileCount:I

    goto :goto_1

    .line 881
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "filename":Ljava/lang/String;
    :cond_3
    iget v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOverwriteFileCount:I

    iput v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTempOverwriteFileCount:I

    .line 882
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 884
    .local v1, "f":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 885
    .local v0, "checkFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_5

    .line 887
    iput v8, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mErrorCode:I

    goto/16 :goto_0

    .line 891
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->isCancelled()Z

    move-result v5

    if-nez v5, :cond_0

    .line 893
    invoke-direct {p0, v1, p2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->BaiduUpload(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 894
    iget v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mRemainOverwriteFileCount:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mRemainOverwriteFileCount:I

    goto :goto_2

    .line 897
    .end local v0    # "checkFile":Ljava/io/File;
    .end local v1    # "f":Ljava/lang/String;
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->StartUploadFiles()V

    goto/16 :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;)Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->updateProgressBar()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;)Lcom/sec/android/app/myfiles/element/FileOperationProgress;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;)Lcom/sec/android/app/myfiles/element/FileOperationProgress;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;[Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->publishProgress([Ljava/lang/Object;)V

    return-void
.end method

.method private checkCloudDirOrFileName(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 8
    .param p1, "serverPath"    # Ljava/lang/String;
    .param p2, "newDirName"    # Ljava/lang/String;
    .param p3, "isFile"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 697
    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ne v5, v3, :cond_5

    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mSameFolder:Z

    if-nez v5, :cond_5

    .line 698
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mPath:Ljava/lang/String;

    .line 699
    iput-boolean v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOverwrite:Z

    .line 700
    iput-boolean v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mRunRename:Z

    .line 701
    iput-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mRename:Ljava/lang/String;

    .line 702
    iput-boolean v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mRenameCancelled:Z

    .line 703
    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/myfiles/db/CacheDB;->getIfDropboxFileExist(Ljava/lang/String;)Z

    move-result v2

    .line 704
    .local v2, "fileExist":Z
    if-eqz v2, :cond_1

    .line 705
    if-eqz p3, :cond_2

    :goto_0
    iput v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFormat:I

    .line 708
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->isAllOverwriteCheck:Z

    if-nez v3, :cond_0

    .line 709
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->showRenameDialog()Z

    .line 713
    :cond_0
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mRunRename:Z

    if-eqz v3, :cond_3

    .line 714
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3, p3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->startRename(Ljava/lang/String;Z)V

    .line 715
    iget-object p2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mRename:Ljava/lang/String;

    .line 746
    .end local v2    # "fileExist":Z
    .end local p2    # "newDirName":Ljava/lang/String;
    :cond_1
    :goto_1
    return-object p2

    .line 705
    .restart local v2    # "fileExist":Z
    .restart local p2    # "newDirName":Ljava/lang/String;
    :cond_2
    const/16 v3, 0x3001

    goto :goto_0

    .line 716
    :cond_3
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOverwrite:Z

    if-eqz v3, :cond_4

    .line 717
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/storage/sdcard0/Android/data/com.baidu.netdisk/files/scratch"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 719
    .local v1, "cachePath":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 721
    .local v0, "cacheFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 723
    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteDropboxThumbnailCache(Ljava/io/File;)V

    goto :goto_1

    .end local v0    # "cacheFile":Ljava/io/File;
    .end local v1    # "cachePath":Ljava/lang/String;
    :cond_4
    move-object p2, v4

    .line 727
    goto :goto_1

    .line 745
    .end local v2    # "fileExist":Z
    :cond_5
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3, p3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->startRename(Ljava/lang/String;Z)V

    .line 746
    iget-object p2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mRename:Ljava/lang/String;

    goto :goto_1
.end method

.method private checkIfFileNameInUse(Ljava/lang/String;)Z
    .locals 4
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 650
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    if-nez v3, :cond_1

    .line 656
    :cond_0
    :goto_0
    return v2

    .line 652
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 653
    .local v0, "fName":Ljava/lang/String;
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 654
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private checkProcessSuccess()Z
    .locals 8

    .prologue
    const-wide/16 v6, 0x64

    const/4 v2, 0x1

    .line 335
    iget v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOperationType:I

    if-nez v3, :cond_2

    .line 336
    const/4 v2, 0x1

    .line 337
    .local v2, "success":Z
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    if-eqz v3, :cond_3

    .line 338
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 339
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;

    iget-wide v4, v3, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;->mDownloadPercent:J

    cmp-long v3, v4, v6

    if-gez v3, :cond_1

    .line 340
    new-instance v0, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;->mDestinationPath:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 341
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 342
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 343
    :cond_0
    const/4 v2, 0x0

    .line 338
    .end local v0    # "f":Ljava/io/File;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 348
    .end local v1    # "i":I
    .end local v2    # "success":Z
    :cond_2
    iget v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOperationType:I

    if-ne v3, v2, :cond_3

    .line 349
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    if-eqz v3, :cond_3

    .line 350
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 351
    iget-object v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;

    iget-wide v4, v3, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;->mUploadPercent:J

    cmp-long v3, v4, v6

    if-gez v3, :cond_4

    .line 352
    const/4 v2, 0x0

    .line 356
    .end local v1    # "i":I
    :cond_3
    return v2

    .line 350
    .restart local v1    # "i":I
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private deleteDownloadableFiles(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 611
    .local p1, "srcFilesPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    .line 612
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 613
    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->deleteFile(Ljava/lang/String;)V

    .line 612
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 616
    .end local v0    # "i":I
    :cond_0
    return-void
.end method

.method private deleteUploadableFiles(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 849
    .local p1, "srcFilesPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 850
    new-instance v1, Ljava/io/File;

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 852
    .local v1, "file":Ljava/io/File;
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->deleteInFileSystem(Ljava/io/File;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 849
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 853
    :catch_0
    move-exception v0

    .line 854
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 855
    const/4 v3, 0x7

    iput v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mErrorCode:I

    goto :goto_1

    .line 858
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "file":Ljava/io/File;
    :cond_0
    return-void
.end method

.method private downloadFolder(Ljava/lang/String;Ljava/lang/String;)V
    .locals 16
    .param p1, "dstFolder"    # Ljava/lang/String;
    .param p2, "serverPath"    # Ljava/lang/String;

    .prologue
    .line 497
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->isCancelled()Z

    move-result v14

    if-eqz v14, :cond_1

    .line 571
    :cond_0
    :goto_0
    return-void

    .line 500
    :cond_1
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v14

    const/16 v15, 0xff

    if-le v14, v15, :cond_2

    .line 501
    const/16 v14, 0x8

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mErrorCode:I

    goto :goto_0

    .line 505
    :cond_2
    new-instance v8, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 507
    .local v8, "newFolder":Ljava/io/File;
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOverwrite:Z

    if-nez v14, :cond_3

    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v14

    if-eqz v14, :cond_3

    .line 508
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mErrorCode:I

    goto :goto_0

    .line 512
    :cond_3
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v14

    if-nez v14, :cond_4

    .line 513
    invoke-virtual {v8}, Ljava/io/File;->mkdirs()Z

    move-result v14

    if-nez v14, :cond_4

    .line 514
    const/4 v14, 0x3

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mErrorCode:I

    goto :goto_0

    .line 519
    :cond_4
    invoke-virtual {v8}, Ljava/io/File;->canWrite()Z

    move-result v14

    if-nez v14, :cond_5

    .line 520
    const/16 v14, 0x9

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mErrorCode:I

    goto :goto_0

    .line 523
    :cond_5
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v11

    .line 526
    .local v11, "pathLen":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->getCloudPathItemList(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 527
    .local v2, "cursor":Landroid/database/Cursor;
    if-eqz v2, :cond_0

    .line 529
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v14

    if-gtz v14, :cond_6

    .line 530
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 533
    :cond_6
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 535
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->isCancelled()Z

    move-result v14

    if-eqz v14, :cond_8

    .line 570
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 538
    :cond_8
    const-string v14, "_data"

    invoke-interface {v2, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    invoke-interface {v2, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 539
    .local v9, "path":Ljava/lang/String;
    add-int/lit8 v14, v11, 0x1

    invoke-virtual {v9, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 540
    .local v10, "pathInFolder":Ljava/lang/String;
    const-string v14, "format"

    invoke-interface {v2, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    invoke-interface {v2, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 541
    .local v6, "format":I
    const-string v14, "_size"

    invoke-interface {v2, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    invoke-interface {v2, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    int-to-long v12, v14

    .line 542
    .local v12, "size":J
    invoke-virtual {v8}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 543
    .local v3, "dstPlace":Ljava/lang/String;
    if-eqz v10, :cond_9

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v14

    if-lez v14, :cond_9

    .line 544
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 545
    :cond_9
    const/16 v14, 0x3001

    if-ne v6, v14, :cond_c

    .line 546
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 547
    .local v5, "folder":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v14

    if-eqz v14, :cond_a

    .line 549
    :try_start_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->deleteInFileSystem(Ljava/io/File;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 555
    :cond_a
    :goto_2
    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    move-result v14

    if-nez v14, :cond_b

    .line 556
    const/4 v14, 0x3

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mErrorCode:I

    .line 569
    .end local v5    # "folder":Ljava/io/File;
    :cond_b
    :goto_3
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v14

    if-nez v14, :cond_7

    goto :goto_1

    .line 550
    .restart local v5    # "folder":Ljava/io/File;
    :catch_0
    move-exception v4

    .line 552
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 558
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v5    # "folder":Ljava/io/File;
    :cond_c
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    if-eqz v14, :cond_b

    .line 559
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    new-instance v15, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;

    invoke-direct {v15, v9, v12, v13, v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    invoke-virtual {v14, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 560
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 561
    .local v7, "newFile":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v14

    if-eqz v14, :cond_b

    .line 562
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOverwrite:Z

    if-eqz v14, :cond_d

    .line 563
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    goto :goto_3

    .line 565
    :cond_d
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mErrorCode:I

    goto :goto_3
.end method

.method private insideBaiduCopy(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 24
    .param p1, "fromPath"    # Ljava/lang/String;
    .param p2, "toPath"    # Ljava/lang/String;

    .prologue
    .line 927
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->isDirectory(Ljava/lang/String;)Z

    move-result v11

    .line 930
    .local v11, "isDirectory":Z
    const-string v7, ""

    .line 931
    .local v7, "fileName":Ljava/lang/String;
    const-string v20, "/"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v17

    .line 932
    .local v17, "sPos":I
    const/16 v20, -0x1

    move/from16 v0, v17

    move/from16 v1, v20

    if-eq v0, v1, :cond_0

    .line 933
    add-int/lit8 v20, v17, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 938
    :goto_0
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 939
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOperationType:I

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_1

    .line 940
    const/16 v20, 0x4

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mErrorCode:I

    .line 941
    const/16 v20, 0x0

    .line 1023
    :goto_1
    return v20

    .line 935
    :cond_0
    move-object/from16 v7, p1

    goto :goto_0

    .line 943
    :cond_1
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mSameFolder:Z

    .line 950
    :cond_2
    if-nez v11, :cond_5

    const/16 v20, 0x1

    :goto_2
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, v20

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->checkCloudDirOrFileName(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v12

    .line 951
    .local v12, "newName":Ljava/lang/String;
    if-eqz v12, :cond_3

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v20

    if-nez v20, :cond_6

    .line 952
    :cond_3
    const/16 v20, 0x0

    goto :goto_1

    .line 944
    .end local v12    # "newName":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 945
    const/16 v20, 0x5

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mErrorCode:I

    .line 946
    const/16 v20, 0x0

    goto :goto_1

    .line 950
    :cond_5
    const/16 v20, 0x0

    goto :goto_2

    .line 955
    .restart local v12    # "newName":Ljava/lang/String;
    :cond_6
    if-eqz v11, :cond_13

    .line 957
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    move-object/from16 v21, v0

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v20

    if-nez v20, :cond_9

    const-string v20, "/"

    :goto_3
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v12, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->createFolder(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;

    move-result-object v9

    .line 958
    .local v9, "folder":Lcom/samsung/scloud/data/SCloudFolder;
    if-nez v9, :cond_7

    .line 959
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "/"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->getFolderInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;

    move-result-object v9

    .line 960
    if-nez v9, :cond_7

    .line 961
    const/16 v20, 0x3

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mErrorCode:I

    .line 964
    :cond_7
    if-eqz v9, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    if-eqz v20, :cond_8

    .line 965
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-virtual {v9}, Lcom/samsung/scloud/data/SCloudFolder;->getAbsolutePath()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 967
    :cond_8
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 968
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v15

    .line 971
    .local v15, "pathLen":I
    const/4 v4, 0x0

    .line 972
    .local v4, "cursor":Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->getCloudPathItemList(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 973
    if-nez v4, :cond_a

    .line 974
    const/16 v20, 0x0

    goto/16 :goto_1

    .end local v4    # "cursor":Landroid/database/Cursor;
    .end local v9    # "folder":Lcom/samsung/scloud/data/SCloudFolder;
    .end local v15    # "pathLen":I
    :cond_9
    move-object/from16 v20, p2

    .line 957
    goto/16 :goto_3

    .line 975
    .restart local v4    # "cursor":Landroid/database/Cursor;
    .restart local v9    # "folder":Lcom/samsung/scloud/data/SCloudFolder;
    .restart local v15    # "pathLen":I
    :cond_a
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v20

    if-gtz v20, :cond_b

    .line 976
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 977
    const/16 v20, 0x0

    goto/16 :goto_1

    .line 979
    :cond_b
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    .line 981
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->isCancelled()Z

    move-result v20

    if-eqz v20, :cond_d

    .line 982
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 983
    const/16 v20, 0x0

    goto/16 :goto_1

    .line 985
    :cond_d
    const-string v20, "_data"

    move-object/from16 v0, v20

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 986
    .local v13, "path":Ljava/lang/String;
    add-int/lit8 v20, v15, 0x1

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    .line 987
    .local v14, "pathInFolder":Ljava/lang/String;
    if-eqz v14, :cond_f

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v20

    if-lez v20, :cond_f

    .line 988
    const-string v20, "format"

    move-object/from16 v0, v20

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 989
    .local v10, "format":I
    const-string v20, "_size"

    move-object/from16 v0, v20

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 990
    .local v18, "size":J
    const-string v20, "/"

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v16

    .line 991
    .local v16, "pos":I
    const-string v5, ""

    .line 992
    .local v5, "fName":Ljava/lang/String;
    const-string v6, ""

    .line 993
    .local v6, "fPath":Ljava/lang/String;
    const/16 v20, -0x1

    move/from16 v0, v16

    move/from16 v1, v20

    if-eq v0, v1, :cond_11

    .line 994
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const/16 v21, 0x0

    move/from16 v0, v21

    move/from16 v1, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 995
    add-int/lit8 v20, v16, 0x1

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 999
    :goto_4
    const/16 v20, 0x3001

    move/from16 v0, v20

    if-ne v10, v0, :cond_12

    .line 1000
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v0, v5, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->createFolder(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;

    move-result-object v8

    .line 1001
    .local v8, "fold":Lcom/samsung/scloud/data/SCloudFolder;
    if-nez v8, :cond_e

    .line 1002
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "/"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->getFolderInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;

    move-result-object v8

    .line 1003
    if-nez v8, :cond_e

    .line 1004
    const/16 v20, 0x3

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mErrorCode:I

    .line 1007
    :cond_e
    if-eqz v8, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    if-eqz v20, :cond_f

    .line 1008
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUsedFilesNames:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-virtual {v8}, Lcom/samsung/scloud/data/SCloudFolder;->getAbsolutePath()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1014
    .end local v5    # "fName":Ljava/lang/String;
    .end local v6    # "fPath":Ljava/lang/String;
    .end local v8    # "fold":Lcom/samsung/scloud/data/SCloudFolder;
    .end local v10    # "format":I
    .end local v16    # "pos":I
    .end local v18    # "size":J
    :cond_f
    :goto_5
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v20

    if-nez v20, :cond_c

    .line 1015
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 1020
    .end local v4    # "cursor":Landroid/database/Cursor;
    .end local v9    # "folder":Lcom/samsung/scloud/data/SCloudFolder;
    .end local v13    # "path":Ljava/lang/String;
    .end local v14    # "pathInFolder":Ljava/lang/String;
    .end local v15    # "pathLen":I
    :goto_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOperationType:I

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_10

    .line 1021
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->deleteFile(Ljava/lang/String;)V

    .line 1023
    :cond_10
    const/16 v20, 0x1

    goto/16 :goto_1

    .line 997
    .restart local v4    # "cursor":Landroid/database/Cursor;
    .restart local v5    # "fName":Ljava/lang/String;
    .restart local v6    # "fPath":Ljava/lang/String;
    .restart local v9    # "folder":Lcom/samsung/scloud/data/SCloudFolder;
    .restart local v10    # "format":I
    .restart local v13    # "path":Ljava/lang/String;
    .restart local v14    # "pathInFolder":Ljava/lang/String;
    .restart local v15    # "pathLen":I
    .restart local v16    # "pos":I
    .restart local v18    # "size":J
    :cond_11
    move-object v5, v14

    goto/16 :goto_4

    .line 1010
    :cond_12
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mCopySummarySize:J

    move-wide/from16 v20, v0

    add-long v20, v20, v18

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mCopySummarySize:J

    .line 1011
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v13, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->BaiduCopyOrMove(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 1017
    .end local v4    # "cursor":Landroid/database/Cursor;
    .end local v5    # "fName":Ljava/lang/String;
    .end local v6    # "fPath":Ljava/lang/String;
    .end local v9    # "folder":Lcom/samsung/scloud/data/SCloudFolder;
    .end local v10    # "format":I
    .end local v13    # "path":Ljava/lang/String;
    .end local v14    # "pathInFolder":Ljava/lang/String;
    .end local v15    # "pathLen":I
    .end local v16    # "pos":I
    .end local v18    # "size":J
    :cond_13
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mCopySummarySize:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->getCloudFileSize(Ljava/lang/String;)J

    move-result-wide v22

    add-long v20, v20, v22

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mCopySummarySize:J

    .line 1018
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "/"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->BaiduCopyOrMove(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6
.end method

.method private startDownloadFiles()V
    .locals 12

    .prologue
    .line 460
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->isCancelled()Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-nez v6, :cond_1

    .line 492
    :cond_0
    return-void

    .line 463
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;

    .line 465
    .local v0, "file":Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;
    new-instance v4, Ljava/io/File;

    iget-object v6, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;->mDestinationPath:Ljava/lang/String;

    invoke-direct {v4, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 467
    .local v4, "newFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    const/16 v7, 0xff

    if-le v6, v7, :cond_2

    .line 468
    const/16 v6, 0x8

    iput v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mErrorCode:I

    goto :goto_0

    .line 472
    :cond_2
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 473
    const/4 v6, 0x1

    iput v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mErrorCode:I

    goto :goto_0

    .line 476
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mDBAPIHelper:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    iget-object v7, v0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;->mServerPath:Ljava/lang/String;

    new-instance v8, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;

    iget-wide v10, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTotalSize:J

    invoke-direct {v8, p0, v10, v11, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$ConcreteProgressListener;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;JLcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;)V

    invoke-virtual {v6, v4, v7, v8}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->download(Ljava/io/File;Ljava/lang/String;Lcom/samsung/scloud/response/ProgressListener;)V

    goto :goto_0

    .line 479
    .end local v0    # "file":Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;
    .end local v4    # "newFile":Ljava/io/File;
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->isCancelled()Z

    move-result v6

    if-nez v6, :cond_0

    .line 482
    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 483
    .local v3, "n":I
    const/16 v6, 0x64

    if-le v3, v6, :cond_5

    .line 484
    const/16 v3, 0x64

    .line 485
    :cond_5
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFileLoadThreads:Ljava/util/ArrayList;

    .line 487
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v3, :cond_0

    .line 488
    new-instance v5, Ljava/lang/Thread;

    new-instance v6, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$FileDownloadRunnable;

    invoke-direct {v6, p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$FileDownloadRunnable;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;)V

    invoke-direct {v5, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 489
    .local v5, "thread":Ljava/lang/Thread;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFileLoadThreads:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 490
    invoke-virtual {v5}, Ljava/lang/Thread;->start()V

    .line 487
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private declared-synchronized updateProgressBar()V
    .locals 10

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 381
    monitor-enter p0

    const-wide/16 v2, 0x0

    .line 382
    .local v2, "totalProgress":D
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 383
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 384
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;

    iget-wide v4, v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;->mSize:J

    long-to-double v4, v4

    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFiles:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;

    iget-wide v6, v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;->mDownloadPercent:J

    long-to-double v6, v6

    mul-double/2addr v4, v6

    mul-double/2addr v4, v8

    iget-wide v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTotalSize:J

    long-to-double v6, v6

    div-double/2addr v4, v6

    add-double/2addr v2, v4

    .line 383
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 387
    .end local v0    # "i":I
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 388
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 389
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;

    iget-wide v4, v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;->mSize:J

    long-to-double v4, v4

    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mUpFiles:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;

    iget-wide v6, v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$UploadableFile;->mUploadPercent:J

    long-to-double v6, v6

    mul-double/2addr v4, v6

    mul-double/2addr v4, v8

    iget-wide v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTotalSize:J

    long-to-double v6, v6

    div-double/2addr v4, v6

    add-double/2addr v2, v4

    .line 388
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 392
    .end local v0    # "i":I
    :cond_1
    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    cmpl-double v1, v2, v4

    if-lez v1, :cond_2

    .line 393
    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    .line 394
    :cond_2
    const-string v1, "BaiduDownLoadOperationTask"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Total copy process: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    double-to-int v4, v2

    iput v4, v1, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mCurrentTargetPercentage:I

    .line 398
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    aput-object v5, v1, v4

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->publishProgress([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 399
    monitor-exit p0

    return-void

    .line 381
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method protected cancelOperation()V
    .locals 1

    .prologue
    .line 361
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 364
    :goto_0
    return-void

    .line 363
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->cancelOperation()V

    goto :goto_0
.end method

.method protected varargs doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;
    .locals 10
    .param p1, "params"    # [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .prologue
    const/4 v9, 0x2

    const/16 v8, 0x1f

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 230
    aget-object v2, p1, v5

    .line 231
    .local v2, "param":Lcom/sec/android/app/myfiles/element/FileOperationParam;
    iget-object v4, v2, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    iput-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetFolder:Ljava/lang/String;

    .line 233
    iget v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetFragmentId:I

    if-ne v4, v8, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetFolder:Ljava/lang/String;

    if-nez v4, :cond_0

    .line 235
    const-string v4, "Baidu/"

    iput-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetFolder:Ljava/lang/String;

    .line 237
    :cond_0
    iget-object v4, v2, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    iput-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    .line 238
    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    if-nez v4, :cond_6

    move v4, v5

    :goto_0
    iput v4, v6, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mTotalTarget:I

    .line 239
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget v6, v2, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mStartIndex:I

    iput v6, v4, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTarget:I

    .line 240
    new-instance v4, Lcom/sec/android/app/myfiles/db/CacheDB;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mContext:Landroid/content/Context;

    invoke-direct {v4, v6}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    .line 241
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 242
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    if-eqz v4, :cond_1

    .line 243
    iget v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mSourceFragmentId:I

    if-ne v4, v8, :cond_8

    .line 244
    iget v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetFragmentId:I

    if-ne v4, v8, :cond_7

    .line 245
    iput v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOperationDirection:I

    .line 246
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetFolder:Ljava/lang/String;

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->CopyOrMoveFiles(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 256
    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 258
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFileLoadThreads:Ljava/util/ArrayList;

    if-eqz v4, :cond_3

    .line 259
    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFileLoadThreads:Ljava/util/ArrayList;

    monitor-enter v5

    .line 261
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFileLoadThreads:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Thread;

    .line 262
    .local v3, "thread":Ljava/lang/Thread;
    invoke-virtual {v3}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 263
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "thread":Ljava/lang/Thread;
    :catch_0
    move-exception v0

    .line 264
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 266
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_2
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 268
    :cond_3
    iget-boolean v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mRenameCancelled:Z

    if-nez v4, :cond_5

    .line 269
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->checkProcessSuccess()Z

    move-result v4

    if-nez v4, :cond_4

    .line 270
    iput v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mErrorCode:I

    .line 271
    :cond_4
    iget v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mErrorCode:I

    if-nez v4, :cond_5

    iget v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOperationType:I

    if-ne v4, v7, :cond_5

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->isCancelled()Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    if-eqz v4, :cond_5

    .line 272
    iget v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOperationDirection:I

    if-nez v4, :cond_9

    .line 273
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->deleteDownloadableFiles(Ljava/util/ArrayList;)V

    .line 279
    :cond_5
    :goto_3
    const/4 v4, 0x0

    return-object v4

    .line 238
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    goto/16 :goto_0

    .line 248
    :cond_7
    iput v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOperationDirection:I

    .line 249
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetFolder:Ljava/lang/String;

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->DownloadFiles(Ljava/util/ArrayList;Ljava/lang/String;)V

    goto :goto_1

    .line 252
    :cond_8
    iput v7, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOperationDirection:I

    .line 253
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetFolder:Ljava/lang/String;

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->UploadFiles(Ljava/util/ArrayList;Ljava/lang/String;)V

    goto :goto_1

    .line 266
    :catchall_0
    move-exception v4

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 274
    :cond_9
    iget v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOperationDirection:I

    if-ne v4, v7, :cond_5

    .line 275
    iget-object v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->deleteUploadableFiles(Ljava/util/ArrayList;)V

    goto :goto_3
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 46
    check-cast p1, [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 2

    .prologue
    .line 368
    iget v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOperationType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 370
    const v0, 0x7f0b00cb

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->showToast(I)V

    .line 376
    :goto_0
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->onCancelled()V

    .line 377
    return-void

    .line 374
    :cond_0
    const v0, 0x7f0b00c9

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->showToast(I)V

    goto :goto_0
.end method

.method protected onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V
    .locals 6
    .param p1, "result"    # Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 300
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mRenameCancelled:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 301
    iget v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mErrorCode:I

    if-eqz v1, :cond_4

    .line 302
    const-string v1, "BaiduDownLoadOperationTask"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to copy, error code = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mErrorCode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    iget v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOperationType:I

    if-ne v1, v5, :cond_3

    .line 304
    const v1, 0x7f0b00cb

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->showToast(I)V

    .line 318
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetFolder:Ljava/lang/String;

    const-string v2, "/storage/sdcard0/Android/data/com.baidu.netdisk/files/scratch"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 319
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetFolder:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/sec/android/app/myfiles/utils/Utils;->sendScan(Landroid/content/Context;Ljava/lang/String;)V

    .line 320
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    if-eqz v1, :cond_1

    .line 321
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 322
    .local v0, "data":Landroid/os/Bundle;
    const-string v1, "/storage/sdcard0/Android/data/com.baidu.netdisk/files/scratch"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 323
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    invoke-interface {v1, v4, v4, v0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;->onCompleted(IILandroid/os/Bundle;)V

    .line 325
    .end local v0    # "data":Landroid/os/Bundle;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetName:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetName:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 326
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mTargetName:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 331
    :cond_2
    :goto_1
    return-void

    .line 306
    :cond_3
    const v1, 0x7f0b00c9

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->showToast(I)V

    goto :goto_0

    .line 308
    :cond_4
    iget v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOperationType:I

    if-ne v1, v5, :cond_5

    .line 309
    const v1, 0x7f0c0008

    iget v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mRemainOverwriteFileCount:I

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->showToastPrurals(II)V

    .line 310
    iput v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mRemainOverwriteFileCount:I

    goto :goto_0

    .line 313
    :cond_5
    const v1, 0x7f0c0005

    iget v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mRemainOverwriteFileCount:I

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->showToastPrurals(II)V

    .line 314
    iput v4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mRemainOverwriteFileCount:I

    goto :goto_0

    .line 329
    :cond_6
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 46
    check-cast p1, Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Lcom/sec/android/app/myfiles/element/FileOperationProgress;)V
    .locals 4
    .param p1, "values"    # [Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    .prologue
    .line 285
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 287
    const/4 v1, 0x0

    aget-object v1, p1, v1

    iget v0, v1, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mCurrentTargetPercentage:I

    .line 288
    .local v0, "percent":I
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 290
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mPercentText:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 292
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mPercentText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 295
    :cond_0
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 46
    check-cast p1, [Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->onProgressUpdate([Lcom/sec/android/app/myfiles/element/FileOperationProgress;)V

    return-void
.end method

.method protected setupProgressView()V
    .locals 2

    .prologue
    .line 224
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 225
    return-void
.end method

.method protected startRename(Ljava/lang/String;Z)V
    .locals 10
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "isFile"    # Z

    .prologue
    const/4 v9, 0x0

    .line 662
    iget v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mOperationDirection:I

    if-nez v6, :cond_1

    .line 663
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->startRename(Ljava/lang/String;Z)V

    .line 693
    :cond_0
    :goto_0
    return-void

    .line 667
    :cond_1
    const-string v6, "/"

    invoke-virtual {p1, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 668
    .local v0, "dPos":I
    move-object v4, p1

    .line 669
    .local v4, "newDirName":Ljava/lang/String;
    const-string v5, "/"

    .line 670
    .local v5, "serverPath":Ljava/lang/String;
    if-ltz v0, :cond_2

    .line 671
    add-int/lit8 v6, v0, 0x1

    invoke-virtual {p1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 672
    invoke-virtual {p1, v9, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 674
    :cond_2
    const/4 v3, 0x1

    .line 675
    .local v3, "i":I
    const/4 v2, 0x0

    .line 676
    .local v2, "fileExist":Z
    move-object v1, v4

    .line 678
    .local v1, "dirName":Ljava/lang/String;
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->CloudDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/myfiles/db/CacheDB;->getIfDropboxFileExist(Ljava/lang/String;)Z

    move-result v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->checkIfFileNameInUse(Ljava/lang/String;)Z

    move-result v7

    or-int v2, v6, v7

    .line 679
    if-eqz v2, :cond_4

    .line 681
    invoke-virtual {p0, v4, v3, p2}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->addPostfix(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v1

    .line 682
    add-int/lit8 v3, v3, 0x1

    .line 684
    :cond_4
    if-nez v2, :cond_3

    .line 686
    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 687
    const-string v6, "BaiduDownLoadOperationTask"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "New file or folder name: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 689
    :cond_5
    iput-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mRename:Ljava/lang/String;

    .line 690
    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mHandler:Landroid/os/Handler;

    if-eqz v6, :cond_0

    .line 691
    iget-object v6, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;->mHandler:Landroid/os/Handler;

    invoke-virtual {v6, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0
.end method
