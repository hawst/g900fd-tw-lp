.class final enum Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;
.super Ljava/lang/Enum;
.source "SelectorActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/SelectorActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ShortcutTabMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

.field public static final enum BROWSING:Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

.field public static final enum SHORTCUT:Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 135
    new-instance v0, Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    const-string v1, "SHORTCUT"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;->SHORTCUT:Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    .line 136
    new-instance v0, Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    const-string v1, "BROWSING"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;->BROWSING:Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    .line 134
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    sget-object v1, Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;->SHORTCUT:Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;->BROWSING:Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;->$VALUES:[Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 134
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 134
    const-class v0, Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;
    .locals 1

    .prologue
    .line 134
    sget-object v0, Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;->$VALUES:[Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    invoke-virtual {v0}, [Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/myfiles/SelectorActivity$ShortcutTabMode;

    return-object v0
.end method
