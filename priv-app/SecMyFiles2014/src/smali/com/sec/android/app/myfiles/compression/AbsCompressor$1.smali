.class Lcom/sec/android/app/myfiles/compression/AbsCompressor$1;
.super Landroid/os/Handler;
.source "AbsCompressor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/compression/AbsCompressor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/compression/AbsCompressor;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/compression/AbsCompressor;)V
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor$1;->this$0:Lcom/sec/android/app/myfiles/compression/AbsCompressor;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 185
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 204
    :goto_0
    return-void

    .line 189
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor$1;->this$0:Lcom/sec/android/app/myfiles/compression/AbsCompressor;

    iget-wide v0, v0, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mCurrentSize:J

    iget-object v2, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor$1;->this$0:Lcom/sec/android/app/myfiles/compression/AbsCompressor;

    iget-wide v2, v2, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mTotalSize:J

    div-long/2addr v0, v2

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor$1;->this$0:Lcom/sec/android/app/myfiles/compression/AbsCompressor;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mProgressBarPercent:Landroid/widget/TextView;

    const-string v1, "100 %"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor$1;->this$0:Lcom/sec/android/app/myfiles/compression/AbsCompressor;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mProgressBarCounter:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor$1;->this$0:Lcom/sec/android/app/myfiles/compression/AbsCompressor;

    iget v2, v2, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mCurrentCounter:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor$1;->this$0:Lcom/sec/android/app/myfiles/compression/AbsCompressor;

    iget v2, v2, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mTotalFilesCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor$1;->this$0:Lcom/sec/android/app/myfiles/compression/AbsCompressor;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mProgressBarPercent:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor$1;->this$0:Lcom/sec/android/app/myfiles/compression/AbsCompressor;

    iget-wide v2, v2, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mCurrentSize:J

    long-to-double v2, v2

    iget-object v4, p0, Lcom/sec/android/app/myfiles/compression/AbsCompressor$1;->this$0:Lcom/sec/android/app/myfiles/compression/AbsCompressor;

    iget-wide v4, v4, Lcom/sec/android/app/myfiles/compression/AbsCompressor;->mTotalSize:J

    long-to-double v4, v4

    div-double/2addr v2, v4

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    mul-double/2addr v2, v4

    double-to-int v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " %"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 185
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
