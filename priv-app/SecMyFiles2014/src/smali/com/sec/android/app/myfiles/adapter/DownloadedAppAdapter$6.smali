.class Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$6;
.super Ljava/lang/Object;
.source "DownloadedAppAdapter.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;)V
    .locals 0

    .prologue
    .line 691
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$6;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;)I
    .locals 8
    .param p1, "lhs"    # Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    .param p2, "rhs"    # Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 696
    const/4 v0, 0x0

    .line 698
    .local v0, "result":I
    if-eqz p1, :cond_5

    if-eqz p2, :cond_5

    .line 699
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$6;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    iget v4, v4, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mCurrentInOrder:I

    if-nez v4, :cond_3

    .line 700
    iget-wide v4, p1, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->date:J

    iget-wide v6, p2, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->date:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_1

    .line 705
    :cond_0
    :goto_0
    return v1

    .line 700
    :cond_1
    iget-wide v4, p1, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->date:J

    iget-wide v6, p2, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->date:J

    cmp-long v1, v4, v6

    if-lez v1, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_0

    .line 702
    :cond_3
    iget-wide v4, p1, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->date:J

    iget-wide v6, p2, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->date:J

    cmp-long v4, v4, v6

    if-eqz v4, :cond_0

    iget-wide v4, p2, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->date:J

    iget-wide v6, p1, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->date:J

    cmp-long v1, v4, v6

    if-lez v1, :cond_4

    move v1, v2

    goto :goto_0

    :cond_4
    move v1, v3

    goto :goto_0

    :cond_5
    move v1, v0

    .line 705
    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 691
    check-cast p1, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$6;->compare(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;)I

    move-result v0

    return v0
.end method
