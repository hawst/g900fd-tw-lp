.class Lcom/sec/android/app/myfiles/view/MyFilesListView$3;
.super Landroid/view/animation/Animation;
.source "MyFilesListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/view/MyFilesListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/view/MyFilesListView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/view/MyFilesListView;)V
    .locals 0

    .prologue
    .line 322
    iput-object p1, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView$3;->this$0:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 3
    .param p1, "interpolatedTime"    # F
    .param p2, "t"    # Landroid/view/animation/Transformation;

    .prologue
    .line 327
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    .line 329
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView$3;->this$0:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setVisibility(I)V

    .line 331
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView$3;->this$0:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView$3;->this$0:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    # getter for: Lcom/sec/android/app/myfiles/view/MyFilesListView;->mCollapsedHeight:I
    invoke-static {v1}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->access$400(Lcom/sec/android/app/myfiles/view/MyFilesListView;)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 338
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView$3;->this$0:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->requestLayout()V

    .line 339
    return-void

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView$3;->this$0:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView$3;->this$0:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    # getter for: Lcom/sec/android/app/myfiles/view/MyFilesListView;->mLastHeight:I
    invoke-static {v1}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->access$500(Lcom/sec/android/app/myfiles/view/MyFilesListView;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView$3;->this$0:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    # getter for: Lcom/sec/android/app/myfiles/view/MyFilesListView;->mLastHeight:I
    invoke-static {v2}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->access$500(Lcom/sec/android/app/myfiles/view/MyFilesListView;)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, p1

    float-to-int v2, v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0
.end method

.method public willChangeBounds()Z
    .locals 1

    .prologue
    .line 343
    const/4 v0, 0x1

    return v0
.end method
