.class public Lcom/sec/android/app/myfiles/ftp/operation/DeleteFromFtpOperation;
.super Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;
.source "DeleteFromFtpOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/ftp/operation/DeleteFromFtpOperation$1;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "operation"    # I
    .param p3, "contentType"    # I
    .param p4, "progressView"    # Landroid/view/View;
    .param p5, "ftpHandlerParams"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;-><init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;)V

    .line 40
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;
    .locals 8
    .param p1, "args"    # [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .prologue
    const/4 v7, 0x0

    .line 52
    if-eqz p1, :cond_3

    array-length v6, p1

    if-lez v6, :cond_3

    aget-object v6, p1, v7

    if-eqz v6, :cond_3

    aget-object v6, p1, v7

    iget-object v6, v6, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    if-eqz v6, :cond_3

    aget-object v6, p1, v7

    iget-object v6, v6, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_3

    .line 55
    aget-object v6, p1, v7

    iget-object v4, v6, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    .line 57
    .local v4, "itemsToDelete":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/sec/android/app/myfiles/ftp/operation/DeleteFromFtpOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v6}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncPrepareConnection()Z

    .line 59
    iget-boolean v6, p0, Lcom/sec/android/app/myfiles/ftp/operation/DeleteFromFtpOperation;->isCancelled:Z

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/myfiles/ftp/operation/DeleteFromFtpOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v6}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->isReady()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 61
    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/ftp/operation/DeleteFromFtpOperation;->discoverFtp(Ljava/util/List;)Landroid/util/Pair;

    move-result-object v5

    .line 63
    .local v5, "res":Landroid/util/Pair;, "Landroid/util/Pair<[Ljava/lang/String;[Ljava/lang/String;>;"
    iget-object v1, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, [Ljava/lang/String;

    .line 65
    .local v1, "filesToDelete":[Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/ftp/operation/DeleteFromFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    sget-object v7, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    if-ne v6, v7, :cond_0

    if-eqz v1, :cond_0

    .line 67
    iget-object v6, p0, Lcom/sec/android/app/myfiles/ftp/operation/DeleteFromFtpOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v6, v1}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncDeleteFiles([Ljava/lang/String;)I

    move-result v0

    .line 69
    .local v0, "filesDeleted":I
    array-length v6, v1

    if-eq v6, v0, :cond_0

    .line 71
    sget-object v6, Lcom/sec/android/app/myfiles/ftp/FTPErr;->PARTIALLY_DONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    iput-object v6, p0, Lcom/sec/android/app/myfiles/ftp/operation/DeleteFromFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 77
    .end local v0    # "filesDeleted":I
    :cond_0
    iget-object v3, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, [Ljava/lang/String;

    .line 79
    .local v3, "foldersToDelete":[Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/ftp/operation/DeleteFromFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    sget-object v7, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    if-ne v6, v7, :cond_1

    if-eqz v3, :cond_1

    .line 81
    iget-object v6, p0, Lcom/sec/android/app/myfiles/ftp/operation/DeleteFromFtpOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v6, v3}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncDeleteFolders([Ljava/lang/String;)I

    move-result v2

    .line 83
    .local v2, "foldersDeleted":I
    array-length v6, v3

    if-eq v6, v2, :cond_1

    .line 85
    sget-object v6, Lcom/sec/android/app/myfiles/ftp/FTPErr;->PARTIALLY_DONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    iput-object v6, p0, Lcom/sec/android/app/myfiles/ftp/operation/DeleteFromFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 103
    .end local v1    # "filesToDelete":[Ljava/lang/String;
    .end local v2    # "foldersDeleted":I
    .end local v3    # "foldersToDelete":[Ljava/lang/String;
    .end local v4    # "itemsToDelete":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v5    # "res":Landroid/util/Pair;, "Landroid/util/Pair<[Ljava/lang/String;[Ljava/lang/String;>;"
    :cond_1
    :goto_0
    const/4 v6, 0x0

    return-object v6

    .line 93
    .restart local v4    # "itemsToDelete":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    sget-object v6, Lcom/sec/android/app/myfiles/ftp/FTPErr;->DELETE_FAILED:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    iput-object v6, p0, Lcom/sec/android/app/myfiles/ftp/operation/DeleteFromFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    goto :goto_0

    .line 99
    .end local v4    # "itemsToDelete":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_3
    sget-object v6, Lcom/sec/android/app/myfiles/ftp/FTPErr;->WRONG_ARGUMENT:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    iput-object v6, p0, Lcom/sec/android/app/myfiles/ftp/operation/DeleteFromFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 33
    check-cast p1, [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/DeleteFromFtpOperation;->doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V
    .locals 4
    .param p1, "result"    # Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .prologue
    const/4 v3, 0x1

    .line 110
    sget-object v1, Lcom/sec/android/app/myfiles/ftp/operation/DeleteFromFtpOperation$1;->$SwitchMap$com$sec$android$app$myfiles$ftp$FTPErr:[I

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/DeleteFromFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPErr;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 128
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/DeleteFromFtpOperation;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    if-eqz v1, :cond_0

    .line 130
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/DeleteFromFtpOperation;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;->onCancelled(Ljava/util/ArrayList;I)V

    .line 134
    :cond_0
    const v1, 0x7f0b009f

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/ftp/operation/DeleteFromFtpOperation;->showToast(I)V

    .line 140
    :cond_1
    :goto_0
    return-void

    .line 114
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/DeleteFromFtpOperation;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    if-eqz v1, :cond_1

    .line 116
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 118
    .local v0, "arg":Landroid/os/Bundle;
    const-string v1, "FILE_OPERATION"

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 120
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/DeleteFromFtpOperation;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    invoke-interface {v1, v3, v3, v0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;->onCompleted(IILandroid/os/Bundle;)V

    goto :goto_0

    .line 110
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 33
    check-cast p1, Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/DeleteFromFtpOperation;->onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V

    return-void
.end method

.method protected setupProgressView()V
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DeleteFromFtpOperation;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 47
    return-void
.end method
