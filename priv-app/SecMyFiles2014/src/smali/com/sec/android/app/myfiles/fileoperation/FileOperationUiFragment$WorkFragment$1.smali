.class Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment$1;
.super Ljava/lang/Object;
.source "FileOperationUiFragment.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;)V
    .locals 0

    .prologue
    .line 1158
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancelled(Ljava/util/ArrayList;I)V
    .locals 4
    .param p2, "totalCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 1213
    .local p1, "remainList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getTargetRequestCode()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1214
    return-void
.end method

.method public onCompleted(IILandroid/os/Bundle;)V
    .locals 4
    .param p1, "totalCount"    # I
    .param p2, "doneCount"    # I
    .param p3, "data"    # Landroid/os/Bundle;

    .prologue
    .line 1163
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    iget v1, v1, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mOperationType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 1165
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    # invokes: Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->updateShortcutInHomescreen(Landroid/os/Bundle;)V
    invoke-static {v1, p3}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->access$200(Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;Landroid/os/Bundle;)V

    .line 1168
    :cond_0
    if-ne p1, p2, :cond_5

    .line 1170
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1172
    .local v0, "intent":Landroid/content/Intent;
    if-eqz p3, :cond_1

    .line 1174
    invoke-virtual {v0, p3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1177
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1179
    const-string v1, "src-ftp-param"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mSrcFtpParams:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1182
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mDstFtpParams:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mDstFtpParams:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1184
    const-string v1, "dst-ftp-param"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mDstFtpParams:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1186
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    iget v1, v1, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mOperationType:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_4

    .line 1187
    const-string v1, "FILE_OPERATION"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    iget v2, v2, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mOperationType:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1189
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->mFileOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->isError()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1190
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getTargetRequestCode()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1207
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_5
    :goto_0
    return-void

    .line 1192
    .restart local v0    # "intent":Landroid/content/Intent;
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment$1;->this$0:Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment$WorkFragment;->getTargetRequestCode()I

    move-result v2

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method
