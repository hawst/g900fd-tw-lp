.class public interface abstract Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
.super Ljava/lang/Object;
.source "IFTPConnectionHandler.java"


# virtual methods
.method public abstract abortBrowse()V
.end method

.method public abstract abortTransfer()V
.end method

.method public abstract getFtpParams()Lcom/sec/android/app/myfiles/ftp/FTPParams;
.end method

.method public abstract getLastErrorCode()I
.end method

.method public abstract getStat()I
.end method

.method public abstract getTimestamp(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract isConnected()Z
.end method

.method public abstract isConnectedAndLoggedIn()Z
.end method

.method public abstract isLoggedIn()Z
.end method

.method public abstract isReady()Z
.end method

.method public abstract setFtpClientOperationListener(Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;)V
.end method

.method public abstract syncBrowseForItemContent(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;"
        }
    .end annotation
.end method

.method public abstract syncConnect()Z
.end method

.method public abstract syncConnectAndLogIn()Z
.end method

.method public abstract syncCreateFolder(Ljava/lang/String;)Z
.end method

.method public abstract syncDeleteFile(Ljava/lang/String;)Z
.end method

.method public abstract syncDeleteFiles([Ljava/lang/String;)I
.end method

.method public abstract syncDeleteFolder(Ljava/lang/String;)Z
.end method

.method public abstract syncDeleteFolders([Ljava/lang/String;)I
.end method

.method public abstract syncDisconnect()Z
.end method

.method public abstract syncDownloadFileToLocal(Lcom/sec/android/app/myfiles/ftp/IFTPItem;Ljava/lang/String;Lcom/sec/android/app/myfiles/openoperation/IProgressUpdatable;)Z
.end method

.method public abstract syncFileExists(Ljava/lang/String;)Z
.end method

.method public abstract syncLogIn()Z
.end method

.method public abstract syncLogOut()Z
.end method

.method public abstract syncLogOutAndDisconnect()Z
.end method

.method public abstract syncPrepareConnection()Z
.end method

.method public abstract syncReconnect()Z
.end method

.method public abstract syncRenameFile(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method public abstract syncSupportsMdtm()Z
.end method

.method public abstract syncUploadFileFromLocal(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/openoperation/IProgressUpdatable;)Z
.end method
