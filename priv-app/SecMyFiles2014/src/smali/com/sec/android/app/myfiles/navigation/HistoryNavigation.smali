.class public Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;
.super Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
.source "HistoryNavigation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$HistoryAsyncOpenOperationFragment;,
        Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$AsyncOpenRunnable;
    }
.end annotation


# static fields
.field private static final CACHE_DB_LOCK:Ljava/lang/Object;

.field private static final HISTORY_ASYNC_TAG:Ljava/lang/String; = "history-async"


# instance fields
.field private mAsyncUi:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

.field private final mCache:Lcom/sec/android/app/myfiles/db/CacheDB;

.field private final mDownloadManager:Landroid/app/DownloadManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 217
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->CACHE_DB_LOCK:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;-><init>(Landroid/content/Context;I)V

    .line 45
    new-instance v0, Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-direct {v0, p1}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->mCache:Lcom/sec/android/app/myfiles/db/CacheDB;

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->mCache:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 49
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->setupDownloadManager(Landroid/content/Context;)Landroid/app/DownloadManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->mDownloadManager:Landroid/app/DownloadManager;

    .line 51
    return-void
.end method

.method static synthetic access$000()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->CACHE_DB_LOCK:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->loadDownloadedFiles()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->removeUndesiredMimeTypeItems(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;)Lcom/sec/android/app/myfiles/db/CacheDB;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->getCache()Lcom/sec/android/app/myfiles/db/CacheDB;

    move-result-object v0

    return-object v0
.end method

.method private getCache()Lcom/sec/android/app/myfiles/db/CacheDB;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->mCache:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->mCache:Lcom/sec/android/app/myfiles/db/CacheDB;

    return-object v0
.end method

.method private loadDownloadedFiles()Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/HistoryItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 148
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 150
    .local v3, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/HistoryItem;>;"
    new-instance v7, Landroid/app/DownloadManager$Query;

    invoke-direct {v7}, Landroid/app/DownloadManager$Query;-><init>()V

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/app/DownloadManager$Query;->setOnlyIncludeVisibleInDownloadsUi(Z)Landroid/app/DownloadManager$Query;

    move-result-object v6

    .line 152
    .local v6, "query":Landroid/app/DownloadManager$Query;
    iget-object v7, p0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->mDownloadManager:Landroid/app/DownloadManager;

    invoke-virtual {v7, v6}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v0

    .line 154
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_5

    .line 158
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 162
    :cond_0
    invoke-static {v0}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getFromDownloadManagerCursor(Landroid/database/Cursor;)Lcom/sec/android/app/myfiles/element/HistoryItem;

    move-result-object v2

    .line 164
    .local v2, "item":Lcom/sec/android/app/myfiles/element/HistoryItem;
    if-eqz v2, :cond_3

    .line 166
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getData()Ljava/lang/String;

    move-result-object v4

    .line 168
    .local v4, "path":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getStatus()I

    move-result v7

    const/16 v8, 0x10

    if-ne v7, v8, :cond_3

    .line 170
    :cond_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 172
    .local v1, "historicalFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_2

    .line 173
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getFileNameInPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 174
    .local v5, "pathInPrivate":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    .end local v1    # "historicalFile":Ljava/io/File;
    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 175
    .restart local v1    # "historicalFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretMode(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 176
    invoke-virtual {v2, v5}, Lcom/sec/android/app/myfiles/element/HistoryItem;->setData(Ljava/lang/String;)V

    .line 179
    .end local v5    # "pathInPrivate":Ljava/lang/String;
    :cond_2
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 185
    .end local v1    # "historicalFile":Ljava/io/File;
    .end local v4    # "path":Ljava/lang/String;
    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    if-nez v7, :cond_0

    .line 191
    .end local v2    # "item":Lcom/sec/android/app/myfiles/element/HistoryItem;
    :cond_4
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 197
    :cond_5
    return-object v3

    .line 191
    :catchall_0
    move-exception v7

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v7
.end method

.method private removeUndesiredMimeTypeItems(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/element/HistoryItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 251
    .local p1, "historyItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/element/HistoryItem;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 252
    .local v1, "historyIterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/myfiles/element/HistoryItem;>;"
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 253
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/element/HistoryItem;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getMimeType()Ljava/lang/String;

    move-result-object v0

    .line 254
    .local v0, "currentItemMimeType":Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/element/HistoryItem$UndesiredMimetypes;->containsMimeType(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 255
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 258
    .end local v0    # "currentItemMimeType":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private setupDownloadManager(Landroid/content/Context;)Landroid/app/DownloadManager;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 140
    const-string v1, "download"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    .line 142
    .local v0, "downloadManager":Landroid/app/DownloadManager;
    return-object v0
.end method


# virtual methods
.method public AsyncOpen()V
    .locals 4

    .prologue
    .line 66
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 68
    .local v1, "fm":Landroid/app/FragmentManager;
    if-eqz v1, :cond_0

    .line 70
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->mAsyncUi:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->mAsyncUi:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->isAdded()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 82
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    new-instance v0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$HistoryAsyncOpenOperationFragment;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$HistoryAsyncOpenOperationFragment;-><init>()V

    .line 74
    .local v0, "asyncUi":Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$HistoryAsyncOpenOperationFragment;
    new-instance v2, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$AsyncOpenRunnable;

    invoke-direct {v2, p0, v0}, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$AsyncOpenRunnable;-><init>(Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;)V

    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$HistoryAsyncOpenOperationFragment;->initialize(Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$AsyncOpenRunnable;Lcom/sec/android/app/myfiles/fragment/AbsFragment;)V

    .line 76
    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->mAsyncUi:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    .line 78
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    const-string v3, "history-async"

    invoke-virtual {v2, v0, v3}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0
.end method

.method public getFilesInCurrentFolder()Landroid/database/Cursor;
    .locals 3

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->getCache()Lcom/sec/android/app/myfiles/db/CacheDB;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->mCurrentSortBy:I

    iget v2, p0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->mCurrentInOrder:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/db/CacheDB;->getHistoryCacheCursor(II)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getFilesInFolder(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getFilesInFolder(Ljava/lang/String;I)Landroid/database/Cursor;
    .locals 1
    .param p1, "folderPath"    # Ljava/lang/String;
    .param p2, "showFolderFileType"    # I

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public goBack()Z
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    return v0
.end method

.method public goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z
    .locals 1
    .param p1, "folderPath"    # Ljava/lang/String;
    .param p2, "isBack"    # Ljava/lang/Boolean;

    .prologue
    .line 115
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->goToRoot(Ljava/lang/Boolean;)Z

    move-result v0

    return v0
.end method

.method public goToRoot(Ljava/lang/Boolean;)Z
    .locals 1
    .param p1, "isBack"    # Ljava/lang/Boolean;

    .prologue
    .line 122
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;->AsyncOpen()V

    .line 124
    const/4 v0, 0x1

    return v0
.end method

.method public goUp()Z
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x0

    return v0
.end method

.method public isCurrentFolderRoot()Z
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x1

    return v0
.end method

.method public refreshNavigation()V
    .locals 0

    .prologue
    .line 61
    return-void
.end method

.method public setCurrentPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 136
    return-void
.end method
