.class Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment$3;
.super Ljava/lang/Object;
.source "AbsAddFTPFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->changeActionbar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;)V
    .locals 0

    .prologue
    .line 692
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 696
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->checkData()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 697
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-nez v2, :cond_1

    .line 710
    :cond_0
    :goto_0
    return-void

    .line 700
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;

    const/4 v3, 0x1

    # setter for: Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mBackFromOK:Z
    invoke-static {v2, v3}, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->access$002(Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;Z)Z

    .line 701
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v3, "FTPA"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/Utils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 702
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->gatherParameters()Lcom/sec/android/app/myfiles/ftp/FTPParams;

    move-result-object v0

    .line 703
    .local v0, "params":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;

    # invokes: Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->storeFTPToDataStore(Lcom/sec/android/app/myfiles/ftp/FTPParams;)V
    invoke-static {v2, v0}, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;Lcom/sec/android/app/myfiles/ftp/FTPParams;)V

    .line 704
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mFTPSessionListener:Lcom/sec/android/app/myfiles/ftp/IFTPSessionListener;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;)Lcom/sec/android/app/myfiles/ftp/IFTPSessionListener;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 705
    const/4 v1, 0x0

    .line 706
    .local v1, "sItem":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->mFTPSessionListener:Lcom/sec/android/app/myfiles/ftp/IFTPSessionListener;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;)Lcom/sec/android/app/myfiles/ftp/IFTPSessionListener;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/sec/android/app/myfiles/ftp/IFTPSessionListener;->onFTPSessionAdded(Lcom/sec/android/app/myfiles/element/ShortcutItem;)V

    .line 708
    .end local v1    # "sItem":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/AbsAddFTPFragment;->onBackPressed()Z

    goto :goto_0
.end method
