.class Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$28;
.super Ljava/lang/Object;
.source "AbsBrowserFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnTwMultiSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field dragSelectEnable:Z

.field private mSelectedPosition:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V
    .locals 1

    .prologue
    .line 6005
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$28;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6007
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$28;->dragSelectEnable:Z

    .line 6009
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$28;->mSelectedPosition:Ljava/util/TreeSet;

    return-void
.end method


# virtual methods
.method public OnTwMultiSelectStart(II)V
    .locals 4
    .param p1, "startX"    # I
    .param p2, "startY"    # I

    .prologue
    const/4 v3, 0x1

    .line 6033
    const/4 v0, 0x0

    const-string v1, "AbsBrowserFragment"

    const-string v2, "MultiSelect Start = "

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 6035
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$28;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectionType()I

    move-result v0

    if-nez v0, :cond_1

    .line 6053
    :cond_0
    :goto_0
    return-void

    .line 6038
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$28;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;

    if-eqz v0, :cond_2

    .line 6039
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$28;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v0, Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/SelectorActivity;->getmFileOperation()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$28;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v0, Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/SelectorActivity;->getmFileOperation()I

    move-result v0

    if-eq v0, v3, :cond_0

    .line 6046
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$28;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->setEnableDragBlock(Z)V

    .line 6048
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$28;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setEnableDragBlock(Z)V

    .line 6050
    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$28;->dragSelectEnable:Z

    .line 6052
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$28;->mSelectedPosition:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->clear()V

    goto :goto_0
.end method

.method public OnTwMultiSelectStop(II)V
    .locals 5
    .param p1, "endX"    # I
    .param p2, "endY"    # I

    .prologue
    const/4 v4, 0x0

    .line 6058
    const-string v1, "AbsBrowserFragment"

    const-string v2, "MultiSelect end = "

    invoke-static {v4, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 6060
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$28;->dragSelectEnable:Z

    if-eqz v1, :cond_2

    .line 6062
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$28;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->isSelectMode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 6064
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$28;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    const/4 v2, 0x1

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startSelectMode(III)V

    .line 6067
    :cond_0
    const/4 v0, 0x0

    .line 6068
    .local v0, "skipFolders":Z
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$28;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/SelectorActivity;

    if-eqz v1, :cond_1

    .line 6069
    const/4 v0, 0x1

    .line 6072
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$28;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$28;->mSelectedPosition:Ljava/util/TreeSet;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->penRangeSelect(Ljava/util/TreeSet;Z)V

    .line 6074
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$28;->dragSelectEnable:Z

    .line 6077
    .end local v0    # "skipFolders":Z
    :cond_2
    return-void
.end method

.method public onTwMultiSelected(Landroid/widget/AdapterView;Landroid/view/View;IJZZZ)V
    .locals 4
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .param p6, "isShiftpress"    # Z
    .param p7, "isCtrlpress"    # Z
    .param p8, "isPentpress"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJZZZ)V"
        }
    .end annotation

    .prologue
    .line 6013
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v0, 0x0

    const-string v1, "AbsBrowserFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onTwMultiSelected call position = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  PenPress is = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 6015
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$28;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;

    if-eqz v0, :cond_0

    .line 6017
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$28;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v0, Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/SelectorActivity;->getmSelectionType()I

    move-result v0

    if-nez v0, :cond_0

    .line 6028
    :goto_0
    return-void

    .line 6023
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$28;->mSelectedPosition:Ljava/util/TreeSet;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6024
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$28;->mSelectedPosition:Ljava/util/TreeSet;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 6026
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$28;->mSelectedPosition:Ljava/util/TreeSet;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
