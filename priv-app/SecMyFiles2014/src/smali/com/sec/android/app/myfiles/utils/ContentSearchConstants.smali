.class public Lcom/sec/android/app/myfiles/utils/ContentSearchConstants;
.super Ljava/lang/Object;
.source "ContentSearchConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/utils/ContentSearchConstants$DocExtensions;
    }
.end annotation


# static fields
.field public static final EXT_SD_PATH:Ljava/lang/String; = "/storage/extSdCard/"

.field public static final PROJ_CONTENT:Ljava/lang/String; = "content_search"

.field public static final ROOT_PATH:Ljava/lang/String;

.field public static final STORAGE:Ljava/lang/String; = "/storage/"

.field private static final STRING_FOR_DOT:Ljava/lang/String; = "."


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/utils/ContentSearchConstants;->ROOT_PATH:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    return-void
.end method
