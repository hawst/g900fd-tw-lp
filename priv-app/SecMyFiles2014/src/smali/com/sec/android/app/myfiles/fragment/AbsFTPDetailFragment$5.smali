.class Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$5;
.super Ljava/lang/Object;
.source "AbsFTPDetailFragment.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mAbort:Ljava/lang/Runnable;

.field private volatile mTotalSize:J

.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;)V
    .locals 2

    .prologue
    .line 159
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 198
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$5;->mTotalSize:J

    return-void
.end method


# virtual methods
.method public abort()V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$5;->mAbort:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$5;->mAbort:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 191
    :cond_0
    return-void
.end method

.method public done()V
    .locals 6

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->mDetailsAdapter:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$5;->mTotalSize:J

    invoke-static {v2, v4, v5}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->getSizeString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->updateSize(Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;Ljava/lang/String;)V

    .line 179
    :cond_0
    return-void
.end method

.method public getResult()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 183
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    return-object v0
.end method

.method public setAbortHandler(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "abort"    # Ljava/lang/Runnable;

    .prologue
    .line 195
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$5;->mAbort:Ljava/lang/Runnable;

    .line 196
    return-void
.end method

.method public visitFile(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)V
    .locals 6
    .param p1, "item"    # Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .prologue
    .line 165
    if-eqz p1, :cond_0

    .line 166
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$5;->mTotalSize:J

    invoke-interface {p1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getSize()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$5;->mTotalSize:J

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->mDetailsAdapter:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    iget-wide v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$5;->mTotalSize:J

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->getSizeString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->updateSize(Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;Ljava/lang/String;)V

    .line 172
    :cond_0
    return-void
.end method

.method public visitFolder(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)V
    .locals 0
    .param p1, "item"    # Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .prologue
    .line 161
    return-void
.end method
