.class public Lcom/sec/android/app/myfiles/fragment/MultipleDropboxDetailFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsMultipleDetailFragment;
.source "MultipleDropboxDetailFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsMultipleDetailFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public getFileSize(Ljava/util/ArrayList;)J
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 27
    .local p1, "pathes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/MultipleDropboxDetailFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/sec/android/app/myfiles/utils/Utils;->getPathDropboxFilesSize(Landroid/content/Context;Ljava/util/ArrayList;)J

    move-result-wide v0

    return-wide v0
.end method
