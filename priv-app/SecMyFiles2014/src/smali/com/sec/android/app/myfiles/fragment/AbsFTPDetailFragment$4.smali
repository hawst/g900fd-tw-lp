.class Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$4;
.super Ljava/lang/Object;
.source "AbsFTPDetailFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->updateSize(Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;

.field final synthetic val$adapter:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;

.field final synthetic val$size:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;

    iput-object p2, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$4;->val$adapter:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;

    iput-object p3, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$4;->val$size:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 132
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$4;->val$adapter:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$FTPDetailsDialogAdapter;->getSizeValueTextView()Landroid/widget/TextView;

    move-result-object v0

    .line 133
    .local v0, "sizeTxtView":Landroid/widget/TextView;
    const-string v1, "MyFiles"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "tum: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", size: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$4;->val$size:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    if-eqz v0, :cond_0

    .line 135
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 136
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$4;->val$size:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->setSize(Ljava/lang/String;)V

    .line 137
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment$4;->val$size:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 140
    :cond_0
    return-void
.end method
