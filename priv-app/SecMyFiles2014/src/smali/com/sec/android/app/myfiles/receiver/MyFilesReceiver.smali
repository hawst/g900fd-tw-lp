.class public Lcom/sec/android/app/myfiles/receiver/MyFilesReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MyFilesReceiver.java"


# static fields
.field private static final CLOUD_ACTION_SIGNED_OUT:Ljava/lang/String; = "com.sec.android.cloudagent.ACTION_ACCOUNT_SIGNED_OUT"

.field private static final MODULE:Ljava/lang/String; = "MyFilesReceiver"

.field private static final mFolderProjection:[Ljava/lang/String;


# instance fields
.field private LAUNCH_MODE:Ljava/lang/String;

.field private LAUNCH_MODE_USB_STORAGE:Ljava/lang/String;

.field private START_FOLDER:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 73
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "_data"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/myfiles/receiver/MyFilesReceiver;->mFolderProjection:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 65
    const-string v0, "LAUNCH_MODE"

    iput-object v0, p0, Lcom/sec/android/app/myfiles/receiver/MyFilesReceiver;->LAUNCH_MODE:Ljava/lang/String;

    .line 67
    const-string v0, "LAUNCH_FROM_USB_STORAGE"

    iput-object v0, p0, Lcom/sec/android/app/myfiles/receiver/MyFilesReceiver;->LAUNCH_MODE_USB_STORAGE:Ljava/lang/String;

    .line 69
    const-string v0, "START_FOLDER"

    iput-object v0, p0, Lcom/sec/android/app/myfiles/receiver/MyFilesReceiver;->START_FOLDER:Ljava/lang/String;

    return-void
.end method

.method private isFactoryMode(Landroid/content/Context;)Z
    .locals 12
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x1

    const/4 v11, 0x0

    .line 384
    const/4 v4, 0x0

    .line 386
    .local v4, "isFactoryMode":Z
    const/4 v3, 0x0

    .line 388
    .local v3, "imeiBlocked":Ljava/lang/String;
    const-string v8, "phone"

    invoke-virtual {p1, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/telephony/TelephonyManager;

    .line 391
    .local v5, "mTelephonyManager":Landroid/telephony/TelephonyManager;
    if-nez v4, :cond_0

    .line 393
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "SHOULD_SHUT_DOWN"

    invoke-static {v8, v9, v11}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v8

    if-ne v8, v7, :cond_0

    .line 395
    const-string v8, "MyFilesReceiver"

    const-string v9, "Factory mode is enabled by Case #1"

    invoke-static {v11, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 397
    const/4 v4, 0x1

    .line 402
    :cond_0
    if-nez v4, :cond_1

    .line 403
    const-string v8, "999999999999999"

    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 405
    const-string v8, "MyFilesReceiver"

    const-string v9, "Factory mode is enabled by Case #2"

    invoke-static {v11, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 407
    const/4 v4, 0x1

    .line 412
    :cond_1
    if-nez v4, :cond_2

    .line 416
    :try_start_0
    new-instance v8, Ljava/io/File;

    const-string v9, "/efs/FactoryApp/factorymode"

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/16 v9, 0x20

    const/4 v10, 0x0

    invoke-direct {p0, v8, v9, v10}, Lcom/sec/android/app/myfiles/receiver/MyFilesReceiver;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 425
    :goto_0
    if-eqz v3, :cond_3

    const-string v8, "ON"

    invoke-virtual {v3, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 427
    const-string v8, "MyFilesReceiver"

    const-string v9, "Not factory mode"

    invoke-static {v11, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 484
    :cond_2
    :goto_1
    if-nez v4, :cond_6

    .line 486
    const-string v8, "activity"

    invoke-virtual {p1, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    .line 488
    .local v1, "am":Landroid/app/ActivityManager;
    const/4 v8, 0x2

    invoke-virtual {v1, v7, v8}, Landroid/app/ActivityManager;->getRecentTasks(II)Ljava/util/List;

    move-result-object v0

    .line 490
    .local v0, "Info":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RecentTaskInfo;>;"
    if-nez v0, :cond_4

    .line 510
    .end local v0    # "Info":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RecentTaskInfo;>;"
    .end local v1    # "am":Landroid/app/ActivityManager;
    :goto_2
    return v7

    .line 418
    :catch_0
    move-exception v2

    .line 420
    .local v2, "e":Ljava/io/IOException;
    const-string v3, "OFF"

    .line 422
    sget v8, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->CURRENT_LOG_LEVEL:I

    const-string v9, "MyFilesReceiver"

    const-string v10, "cannot open file factorymode"

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 431
    .end local v2    # "e":Ljava/io/IOException;
    :cond_3
    const-string v8, "MyFilesReceiver"

    const-string v9, "Factory mode is enabled by Case #3"

    invoke-static {v11, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 433
    const/4 v4, 0x1

    goto :goto_1

    .line 495
    .restart local v0    # "Info":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RecentTaskInfo;>;"
    .restart local v1    # "am":Landroid/app/ActivityManager;
    :cond_4
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_6

    .line 497
    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/ActivityManager$RecentTaskInfo;

    iget-object v7, v7, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-virtual {v7}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    .line 499
    .local v6, "topactivityname":Ljava/lang/String;
    const-string v7, "com.sec.factory"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_5

    const-string v7, "com.sec.android.app.factorytest"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 501
    :cond_5
    const-string v7, "MyFilesReceiver"

    const-string v8, "It\'s Factory Test Mode!!! So don\'t lauch the MyFiles!!!"

    invoke-static {v11, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 503
    const/4 v4, 0x1

    .line 508
    .end local v0    # "Info":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RecentTaskInfo;>;"
    .end local v1    # "am":Landroid/app/ActivityManager;
    .end local v6    # "topactivityname":Ljava/lang/String;
    :cond_6
    const-string v7, "MyFilesReceiver"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "isFactoryMode = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v11, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    move v7, v4

    .line 510
    goto :goto_2
.end method

.method private readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;
    .locals 16
    .param p1, "file"    # Ljava/io/File;
    .param p2, "max"    # I
    .param p3, "ellipsis"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 515
    new-instance v5, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 520
    .local v5, "input":Ljava/io/InputStream;
    new-instance v2, Ljava/io/BufferedInputStream;

    invoke-direct {v2, v5}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 523
    .local v2, "bis":Ljava/io/BufferedInputStream;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v10

    .line 524
    .local v10, "size":J
    if-gtz p2, :cond_0

    const-wide/16 v14, 0x0

    cmp-long v13, v10, v14

    if-lez v13, :cond_6

    if-nez p2, :cond_6

    .line 526
    :cond_0
    const-wide/16 v14, 0x0

    cmp-long v13, v10, v14

    if-lez v13, :cond_2

    if-eqz p2, :cond_1

    move/from16 v0, p2

    int-to-long v14, v0

    cmp-long v13, v10, v14

    if-gez v13, :cond_2

    :cond_1
    long-to-int v0, v10

    move/from16 p2, v0

    .line 527
    :cond_2
    add-int/lit8 v13, p2, 0x1

    new-array v4, v13, [B

    .line 528
    .local v4, "data":[B
    invoke-virtual {v2, v4}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v8

    .line 529
    .local v8, "length":I
    if-gtz v8, :cond_3

    const-string v13, ""
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 579
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V

    .line 580
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .end local v8    # "length":I
    :goto_0
    return-object v13

    .line 530
    .restart local v8    # "length":I
    :cond_3
    move/from16 v0, p2

    if-gt v8, v0, :cond_4

    :try_start_1
    new-instance v13, Ljava/lang/String;

    const/4 v14, 0x0

    invoke-direct {v13, v4, v14, v8}, Ljava/lang/String;-><init>([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 579
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V

    .line 580
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    goto :goto_0

    .line 531
    :cond_4
    if-nez p3, :cond_5

    :try_start_2
    new-instance v13, Ljava/lang/String;

    const/4 v14, 0x0

    move/from16 v0, p2

    invoke-direct {v13, v4, v14, v0}, Ljava/lang/String;-><init>([BII)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 579
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V

    .line 580
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    goto :goto_0

    .line 532
    :cond_5
    :try_start_3
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v14, Ljava/lang/String;

    const/4 v15, 0x0

    move/from16 v0, p2

    invoke-direct {v14, v4, v15, v0}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p3

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v13

    .line 579
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V

    .line 580
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    goto :goto_0

    .line 534
    .end local v4    # "data":[B
    .end local v8    # "length":I
    :cond_6
    if-gez p2, :cond_f

    .line 537
    const/4 v9, 0x0

    .line 538
    .local v9, "rolled":Z
    const/4 v6, 0x0

    .line 539
    .local v6, "last":[B
    const/4 v4, 0x0

    .line 543
    .restart local v4    # "data":[B
    :cond_7
    if-eqz v6, :cond_8

    const/4 v9, 0x1

    .line 544
    :cond_8
    move-object v12, v6

    .local v12, "tmp":[B
    move-object v6, v4

    move-object v4, v12

    .line 545
    if-nez v4, :cond_9

    move/from16 v0, p2

    neg-int v13, v0

    :try_start_4
    new-array v4, v13, [B

    .line 546
    :cond_9
    invoke-virtual {v2, v4}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v7

    .line 548
    .local v7, "len":I
    array-length v13, v4

    if-eq v7, v13, :cond_7

    .line 550
    if-nez v6, :cond_a

    if-gtz v7, :cond_a

    const-string v13, ""
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 579
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V

    .line 580
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    goto :goto_0

    .line 551
    :cond_a
    if-nez v6, :cond_b

    :try_start_5
    new-instance v13, Ljava/lang/String;

    const/4 v14, 0x0

    invoke-direct {v13, v4, v14, v7}, Ljava/lang/String;-><init>([BII)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 579
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V

    .line 580
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    goto :goto_0

    .line 552
    :cond_b
    if-lez v7, :cond_c

    .line 554
    const/4 v9, 0x1

    .line 555
    const/4 v13, 0x0

    :try_start_6
    array-length v14, v6

    sub-int/2addr v14, v7

    invoke-static {v6, v7, v6, v13, v14}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 556
    const/4 v13, 0x0

    array-length v14, v6

    sub-int/2addr v14, v7

    invoke-static {v4, v13, v6, v14, v7}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 559
    :cond_c
    if-eqz p3, :cond_d

    if-nez v9, :cond_e

    :cond_d
    new-instance v13, Ljava/lang/String;

    invoke-direct {v13, v6}, Ljava/lang/String;-><init>([B)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 579
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V

    .line 580
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    goto/16 :goto_0

    .line 560
    :cond_e
    :try_start_7
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p3

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    new-instance v14, Ljava/lang/String;

    invoke-direct {v14, v6}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v13

    .line 579
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V

    .line 580
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    goto/16 :goto_0

    .line 564
    .end local v4    # "data":[B
    .end local v6    # "last":[B
    .end local v7    # "len":I
    .end local v9    # "rolled":Z
    .end local v12    # "tmp":[B
    :cond_f
    :try_start_8
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 566
    .local v3, "contents":Ljava/io/ByteArrayOutputStream;
    const/16 v13, 0x400

    new-array v4, v13, [B

    .line 570
    .restart local v4    # "data":[B
    :cond_10
    invoke-virtual {v2, v4}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v7

    .line 571
    .restart local v7    # "len":I
    if-lez v7, :cond_11

    const/4 v13, 0x0

    invoke-virtual {v3, v4, v13, v7}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 572
    :cond_11
    array-length v13, v4

    if-eq v7, v13, :cond_10

    .line 574
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result-object v13

    .line 579
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V

    .line 580
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    goto/16 :goto_0

    .line 579
    .end local v3    # "contents":Ljava/io/ByteArrayOutputStream;
    .end local v4    # "data":[B
    .end local v7    # "len":I
    .end local v10    # "size":J
    :catchall_0
    move-exception v13

    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V

    .line 580
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    throw v13
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 19
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 83
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 85
    .local v2, "action":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 87
    const/4 v15, 0x0

    const-string v16, "MyFilesReceiver"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Received the "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 89
    const-string v15, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v2, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_0

    const-string v15, "android.intent.action.MEDIA_SCAN"

    invoke-virtual {v2, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 92
    :cond_0
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/myfiles/receiver/MyFilesReceiver;->isFactoryMode(Landroid/content/Context;)Z

    move-result v15

    if-nez v15, :cond_2

    .line 94
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v14

    .line 96
    .local v14, "uri":Landroid/net/Uri;
    invoke-virtual {v14}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v15

    const-string v16, "file"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 98
    invoke-virtual {v14}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v12

    .line 100
    .local v12, "path":Ljava/lang/String;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isMountedStateCheck(Landroid/content/Context;)V

    .line 102
    if-eqz v12, :cond_2

    invoke-static {v12}, Lcom/sec/android/app/myfiles/utils/Utils;->isUsbStorageRootFolder(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 104
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 106
    .local v7, "extras":Landroid/os/Bundle;
    new-instance v3, Landroid/content/ComponentName;

    const-string v15, "com.sec.android.app.myfiles"

    const-string v16, "com.sec.android.app.myfiles.MainActivity"

    move-object/from16 v0, v16

    invoke-direct {v3, v15, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    .local v3, "compName":Landroid/content/ComponentName;
    new-instance v8, Landroid/content/Intent;

    const-string v15, "android.intent.action.MAIN"

    invoke-direct {v8, v15}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 111
    .local v8, "newIntent":Landroid/content/Intent;
    const-string v15, "android.intent.category.LAUNCHER"

    invoke-virtual {v8, v15}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 113
    const v15, 0x10008000

    invoke-virtual {v8, v15}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 115
    invoke-virtual {v8, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 117
    if-eqz v7, :cond_1

    .line 119
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/myfiles/receiver/MyFilesReceiver;->LAUNCH_MODE:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/receiver/MyFilesReceiver;->LAUNCH_MODE_USB_STORAGE:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v7, v15, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/myfiles/receiver/MyFilesReceiver;->START_FOLDER:Ljava/lang/String;

    invoke-virtual {v7, v15, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    invoke-virtual {v8, v7}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 125
    const/4 v15, 0x0

    const-string v16, "MyFilesReceiver"

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "launch mode = "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/receiver/MyFilesReceiver;->LAUNCH_MODE_USB_STORAGE:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "start folder"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 131
    :cond_1
    :try_start_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 379
    .end local v3    # "compName":Landroid/content/ComponentName;
    .end local v7    # "extras":Landroid/os/Bundle;
    .end local v8    # "newIntent":Landroid/content/Intent;
    .end local v12    # "path":Ljava/lang/String;
    .end local v14    # "uri":Landroid/net/Uri;
    :cond_2
    :goto_0
    return-void

    .line 132
    .restart local v3    # "compName":Landroid/content/ComponentName;
    .restart local v7    # "extras":Landroid/os/Bundle;
    .restart local v8    # "newIntent":Landroid/content/Intent;
    .restart local v12    # "path":Ljava/lang/String;
    .restart local v14    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v6

    .line 134
    .local v6, "e":Landroid/content/ActivityNotFoundException;
    const/4 v15, 0x2

    const-string v16, "MyFilesReceiver"

    const-string v17, "com.sec.android.app.myfiles.MainActivity not found - ACTION_MEDIA_MOUNTED | ACTION_MEDIA_SCAN"

    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 296
    .end local v3    # "compName":Landroid/content/ComponentName;
    .end local v6    # "e":Landroid/content/ActivityNotFoundException;
    .end local v7    # "extras":Landroid/os/Bundle;
    .end local v8    # "newIntent":Landroid/content/Intent;
    .end local v12    # "path":Ljava/lang/String;
    .end local v14    # "uri":Landroid/net/Uri;
    :cond_3
    const-string v15, "com.samsung.android.intent.action.PRIVATE_MODE_ON"

    invoke-virtual {v2, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 298
    const/4 v15, 0x1

    invoke-static {v15}, Lcom/sec/android/app/myfiles/utils/Utils;->setSecretModeState(Z)V

    goto :goto_0

    .line 300
    :cond_4
    const-string v15, "com.samsung.android.intent.action.PRIVATE_MODE_OFF"

    invoke-virtual {v2, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 302
    const/4 v15, 0x0

    invoke-static {v15}, Lcom/sec/android/app/myfiles/utils/Utils;->setSecretModeState(Z)V

    goto :goto_0

    .line 304
    :cond_5
    const-string v15, "com.sec.knox.containeragent.klms.created.b2c"

    invoke-virtual {v2, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 306
    const/4 v15, 0x1

    invoke-static {v15}, Lcom/sec/android/app/myfiles/utils/Utils;->setKNOXInstalled(Z)V

    .line 307
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v13

    .line 308
    .local v13, "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    const/4 v15, 0x1

    invoke-virtual {v13, v15}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setKNOXInstalled(Z)V

    .line 309
    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->commit()Z

    goto :goto_0

    .line 311
    .end local v13    # "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    :cond_6
    const-string v15, "com.sec.knox.containeragent.klms.removed.b2c"

    invoke-virtual {v2, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 313
    const/4 v15, 0x0

    invoke-static {v15}, Lcom/sec/android/app/myfiles/utils/Utils;->setKNOXInstalled(Z)V

    .line 314
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v13

    .line 315
    .restart local v13    # "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    const/4 v15, 0x0

    invoke-virtual {v13, v15}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setKNOXInstalled(Z)V

    .line 316
    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->commit()Z

    goto :goto_0

    .line 318
    .end local v13    # "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    :cond_7
    const-string v15, "com.sec.android.cloudagent.ACTION_ACCOUNT_SIGNED_OUT"

    invoke-virtual {v2, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 320
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v13

    .line 321
    .restart local v13    # "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    const-string v15, ""

    invoke-virtual {v13, v15}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setDropboxAuthKey(Ljava/lang/String;)V

    .line 322
    const-string v15, ""

    invoke-virtual {v13, v15}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setDropboxAuthSecret(Ljava/lang/String;)V

    .line 323
    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->commit()Z

    .line 326
    :try_start_1
    new-instance v5, Lcom/sec/android/app/myfiles/db/CacheDB;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    .line 328
    .local v5, "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 330
    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/db/CacheDB;->clearCloudData()J

    .line 332
    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 334
    .end local v5    # "db":Lcom/sec/android/app/myfiles/db/CacheDB;
    :catch_1
    move-exception v6

    .line 336
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 339
    .end local v6    # "e":Ljava/lang/Exception;
    .end local v13    # "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    :cond_8
    const-string v15, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v2, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 341
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v15

    invoke-virtual {v15}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v10

    .line 343
    .local v10, "packageName":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    .line 345
    .local v9, "packageManager":Landroid/content/pm/PackageManager;
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v4

    .line 350
    .local v4, "currentUser":I
    new-instance v11, Lcom/sec/android/app/myfiles/receiver/MyFilesReceiver$1;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v11, v0, v1}, Lcom/sec/android/app/myfiles/receiver/MyFilesReceiver$1;-><init>(Lcom/sec/android/app/myfiles/receiver/MyFilesReceiver;Landroid/content/Context;)V

    .line 373
    .local v11, "packageStatsObserver":Landroid/content/pm/IPackageStatsObserver$Stub;
    invoke-virtual {v9, v10, v4, v11}, Landroid/content/pm/PackageManager;->getPackageSizeInfo(Ljava/lang/String;ILandroid/content/pm/IPackageStatsObserver;)V

    goto/16 :goto_0
.end method
