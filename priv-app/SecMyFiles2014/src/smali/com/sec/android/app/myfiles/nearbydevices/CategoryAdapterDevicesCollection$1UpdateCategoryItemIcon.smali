.class Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$1UpdateCategoryItemIcon;
.super Ljava/lang/Object;
.source "CategoryAdapterDevicesCollection.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/nearbydevices/DrawableUpdatable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->getShortcutItem(Lcom/samsung/android/allshare/Device;)Lcom/sec/android/app/myfiles/element/ShortcutItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "UpdateCategoryItemIcon"
.end annotation


# instance fields
.field private final mShortcutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

.field final synthetic this$0:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;Lcom/sec/android/app/myfiles/element/ShortcutItem;)V
    .locals 1
    .param p2, "shortcutItem"    # Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .prologue
    .line 246
    iput-object p1, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$1UpdateCategoryItemIcon;->this$0:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247
    if-nez p2, :cond_0

    .line 248
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 249
    :cond_0
    iput-object p2, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$1UpdateCategoryItemIcon;->mShortcutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 250
    return-void
.end method


# virtual methods
.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$1UpdateCategoryItemIcon;->mShortcutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getIconDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public setDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 5
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 254
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 255
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v3, "shortcut_drawable"

    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->convertDrawableToByteArray(Landroid/graphics/drawable/Drawable;)[B

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 256
    const-string v1, "_data = ? "

    .line 257
    .local v1, "where":Ljava/lang/String;
    const/4 v3, 0x1

    new-array v2, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$1UpdateCategoryItemIcon;->mShortcutItem:Lcom/sec/android/app/myfiles/element/ShortcutItem;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getPath()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 258
    .local v2, "whereArgs":[Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$1UpdateCategoryItemIcon;->this$0:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    # getter for: Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->access$300(Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/myfiles/provider/MyFilesStore$Shortcut;->getContentUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4, v0, v1, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 260
    iget-object v3, p0, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$1UpdateCategoryItemIcon;->this$0:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;->mDevicesCollectionUpdatedListener:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;

    invoke-interface {v3}, Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection$OnDevicesCollectionUpdatedListener;->notifyDataSetChanged()V

    .line 266
    return-void
.end method
