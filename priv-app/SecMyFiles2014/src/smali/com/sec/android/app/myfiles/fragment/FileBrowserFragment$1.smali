.class Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$1;
.super Landroid/content/BroadcastReceiver;
.source "FileBrowserFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    .line 135
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 147
    :goto_0
    return-void

    .line 142
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->notifyDataSetChanged()V

    .line 146
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->onRefresh()V

    goto :goto_0
.end method
