.class Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$2;
.super Landroid/content/BroadcastReceiver;
.source "CategoryHomeFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V
    .locals 0

    .prologue
    .line 286
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 291
    const/4 v1, 0x0

    const-string v2, "CategoryHomeFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "receive the broadcast message : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 297
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->isSelectMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 299
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->dismissDialog(I)V

    .line 300
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getOtherConfirmDeleteInstance(Landroid/app/Dialog;)Landroid/app/Dialog;

    move-result-object v0

    .line 301
    .local v0, "confirmDeleteInstance":Landroid/app/Dialog;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 302
    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 304
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->finishSelectMode()V

    .line 306
    .end local v0    # "confirmDeleteInstance":Landroid/app/Dialog;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "category_home"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getmShortcutListAdapter()Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 307
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "category_home"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getmShortcutListAdapter()Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->onRefresh()V

    .line 310
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "category_home"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getmCloudListAdapter()Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 311
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "category_home"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getmCloudListAdapter()Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->onRefresh()V

    .line 313
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$2;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListView:Landroid/widget/ListView;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Landroid/widget/ListView;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/myfiles/ListViewHeightRefresh;->getListViewSize(Landroid/widget/ListView;)V

    .line 314
    return-void
.end method
