.class public Lcom/sec/android/app/myfiles/utils/MyFilesFeatures;
.super Ljava/lang/Object;
.source "MyFilesFeatures.java"


# static fields
.field public static final ADD_SHORTCUT_ON_HOMESCREEN:Z = false

.field public static final ATT_BASE_MODEL:Z

.field private static ATT_MODEL:Z = false

.field public static final COPY_MOVE_PROGRESS_TYPE:I = 0x1

.field public static final DISPLAY_EACH_FILE_PROGRESS:I = 0x1

.field public static final DISPLAY_TOTAL_DONE_PROGRESS:I = 0x0

.field public static final FEATURE_DISPLAY_FTP_MENU:Z = true

.field public static final FEATURE_DISPLAY_NEARBYDEVICE_MENU:Z = true

.field public static final FEATURE_SUPPORT_FINGER_HOVER:Z = true

.field public static final FEATURE_SUPPORT_HOVER_OPEREATIONS:Z = false

.field public static final FEATURE_SUPPORT_LIST_ANIMATION:Z

.field public static final FEATURE_SUPPORT_NORMAL_DELETE_MENU:Z = true

.field public static final PAGE_ARROW_IN_STORAGE_USAGE_VIEW:Z

.field public static final PAGE_INDICATOR_IN_STORAGE_USAGE_VIEW:Z

.field public static US_MODEL:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/MyFilesFeatures;->isUSModel()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/MyFilesFeatures;->US_MODEL:Z

    .line 37
    const-string v0, "ATT"

    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/MyFilesFeatures;->ATT_MODEL:Z

    .line 39
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/MyFilesFeatures;->ATT_MODEL:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/myfiles/utils/MyFilesFeatures;->isFlagShipModel()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/MyFilesFeatures;->ATT_BASE_MODEL:Z

    .line 65
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/MyFilesFeatures;->isFlagShipModel()Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/myfiles/utils/MyFilesFeatures;->FEATURE_SUPPORT_LIST_ANIMATION:Z

    return-void

    .line 39
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static isFlagShipModel()Z
    .locals 2

    .prologue
    .line 42
    const-string v1, "ro.product.name"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 43
    .local v0, "productName":Ljava/lang/String;
    const-string v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "tb3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "trlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "tre3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "tbhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "trhlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "trhp0lte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "trhplte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "trelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "trwifi"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SC-01G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SCL24"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static isUSModel()Z
    .locals 2

    .prologue
    .line 53
    const-string v1, "ro.product.locale.region"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 55
    .local v0, "LocaleName":Ljava/lang/String;
    const-string v1, "US"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method
