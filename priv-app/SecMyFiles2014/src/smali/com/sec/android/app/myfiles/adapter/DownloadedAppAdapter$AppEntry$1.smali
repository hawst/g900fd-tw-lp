.class Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry$1;
.super Landroid/content/pm/IPackageStatsObserver$Stub;
.source "DownloadedAppAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;)V
    .locals 0

    .prologue
    .line 1001
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry$1;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    invoke-direct {p0}, Landroid/content/pm/IPackageStatsObserver$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetStatsCompleted(Landroid/content/pm/PackageStats;Z)V
    .locals 14
    .param p1, "stats"    # Landroid/content/pm/PackageStats;
    .param p2, "succeeded"    # Z

    .prologue
    .line 1005
    sget-object v8, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mEntriesMap:Ljava/util/HashMap;

    monitor-enter v8

    .line 1007
    :try_start_0
    sget-object v1, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->mEntriesMap:Ljava/util/HashMap;

    iget-object v9, p1, Landroid/content/pm/PackageStats;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .line 1009
    .local v0, "entry":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    if-eqz v0, :cond_2

    .line 1011
    monitor-enter v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1013
    const/4 v1, 0x0

    :try_start_1
    iput-boolean v1, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->sizeStale:Z

    .line 1014
    const-wide/16 v10, 0x0

    iput-wide v10, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->sizeLoadStart:J

    .line 1015
    iget-wide v10, p1, Landroid/content/pm/PackageStats;->externalCodeSize:J

    iget-wide v12, p1, Landroid/content/pm/PackageStats;->externalObbSize:J

    add-long v2, v10, v12

    .line 1016
    .local v2, "externalCodeSize":J
    iget-wide v10, p1, Landroid/content/pm/PackageStats;->externalDataSize:J

    iget-wide v12, p1, Landroid/content/pm/PackageStats;->externalMediaSize:J

    add-long v4, v10, v12

    .line 1017
    .local v4, "externalDataSize":J
    add-long v10, v2, v4

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry$1;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    # invokes: Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->getTotalInternalSize(Landroid/content/pm/PackageStats;)J
    invoke-static {v1, p1}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->access$300(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;Landroid/content/pm/PackageStats;)J

    move-result-wide v12

    add-long v6, v10, v12

    .line 1019
    .local v6, "newSize":J
    iget-wide v10, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->size:J

    cmp-long v1, v10, v6

    if-nez v1, :cond_0

    iget-wide v10, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->cacheSize:J

    iget-wide v12, p1, Landroid/content/pm/PackageStats;->cacheSize:J

    cmp-long v1, v10, v12

    if-nez v1, :cond_0

    iget-wide v10, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->codeSize:J

    iget-wide v12, p1, Landroid/content/pm/PackageStats;->codeSize:J

    cmp-long v1, v10, v12

    if-nez v1, :cond_0

    iget-wide v10, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->dataSize:J

    iget-wide v12, p1, Landroid/content/pm/PackageStats;->dataSize:J

    cmp-long v1, v10, v12

    if-nez v1, :cond_0

    iget-wide v10, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->externalCodeSize:J

    cmp-long v1, v10, v2

    if-nez v1, :cond_0

    iget-wide v10, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->externalDataSize:J

    cmp-long v1, v10, v4

    if-nez v1, :cond_0

    iget-wide v10, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->externalCacheSize:J

    iget-wide v12, p1, Landroid/content/pm/PackageStats;->externalCacheSize:J

    cmp-long v1, v10, v12

    if-eqz v1, :cond_1

    .line 1027
    :cond_0
    iput-wide v6, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->size:J

    .line 1028
    iget-wide v10, p1, Landroid/content/pm/PackageStats;->cacheSize:J

    iput-wide v10, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->cacheSize:J

    .line 1029
    iget-wide v10, p1, Landroid/content/pm/PackageStats;->codeSize:J

    iput-wide v10, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->codeSize:J

    .line 1030
    iget-wide v10, p1, Landroid/content/pm/PackageStats;->dataSize:J

    iput-wide v10, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->dataSize:J

    .line 1031
    iput-wide v2, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->externalCodeSize:J

    .line 1032
    iput-wide v4, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->externalDataSize:J

    .line 1033
    iget-wide v10, p1, Landroid/content/pm/PackageStats;->externalCacheSize:J

    iput-wide v10, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->externalCacheSize:J

    .line 1034
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry$1;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    iget-wide v10, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->size:J

    # invokes: Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->getSizeStr(J)Ljava/lang/String;
    invoke-static {v1, v10, v11}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->access$400(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->sizeStr:Ljava/lang/String;

    .line 1035
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry$1;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    iget-object v9, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry$1;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    iget-wide v10, v9, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->codeSize:J

    # invokes: Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->getSizeStr(J)Ljava/lang/String;
    invoke-static {v1, v10, v11}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->access$400(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->codeSizeStr:Ljava/lang/String;

    .line 1036
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry$1;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    iget-wide v10, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->dataSize:J

    # invokes: Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->getSizeStr(J)Ljava/lang/String;
    invoke-static {v1, v10, v11}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->access$400(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->dataSizeStr:Ljava/lang/String;

    .line 1037
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry$1;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    # invokes: Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->getTotalInternalSize(Landroid/content/pm/PackageStats;)J
    invoke-static {v1, p1}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->access$300(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;Landroid/content/pm/PackageStats;)J

    move-result-wide v10

    iput-wide v10, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->internalSize:J

    .line 1038
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry$1;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    iget-wide v10, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->internalSize:J

    # invokes: Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->getSizeStr(J)Ljava/lang/String;
    invoke-static {v1, v10, v11}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->access$400(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->internalSizeStr:Ljava/lang/String;

    .line 1039
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry$1;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    # invokes: Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->getTotalExternalSize(Landroid/content/pm/PackageStats;)J
    invoke-static {v1, p1}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->access$500(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;Landroid/content/pm/PackageStats;)J

    move-result-wide v10

    iput-wide v10, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->externalSize:J

    .line 1040
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry$1;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    iget-wide v10, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->externalSize:J

    # invokes: Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->getSizeStr(J)Ljava/lang/String;
    invoke-static {v1, v10, v11}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->access$400(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->externalSizeStr:Ljava/lang/String;

    .line 1041
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry$1;->this$0:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    iget-wide v10, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->cacheSize:J

    # invokes: Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->getSizeStr(J)Ljava/lang/String;
    invoke-static {v1, v10, v11}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->access$400(Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->cacheSizeStr:Ljava/lang/String;

    .line 1043
    :cond_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1045
    .end local v2    # "externalCodeSize":J
    .end local v4    # "externalDataSize":J
    .end local v6    # "newSize":J
    :cond_2
    :try_start_2
    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1046
    return-void

    .line 1043
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1

    .line 1045
    .end local v0    # "entry":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    :catchall_1
    move-exception v1

    monitor-exit v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v1
.end method
