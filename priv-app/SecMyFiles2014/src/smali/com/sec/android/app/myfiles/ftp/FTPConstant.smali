.class public Lcom/sec/android/app/myfiles/ftp/FTPConstant;
.super Ljava/lang/Object;
.source "FTPConstant.java"


# static fields
.field public static final FETCHING_DIALOG_TAG:Ljava/lang/String; = "fetching_dialog"

.field public static final REQUEST_CREATE_DIR:Ljava/lang/String; = "create_dir"

.field public static final REQUEST_RECURSIVE_DISCOVER_FILES:Ljava/lang/String; = "recur_disc_files"

.field public static final REQUEST_RECURSIVE_DISCOVER_FOLDERS:Ljava/lang/String; = "recur_disc_folders"

.field public static final REQUEST_RENAME_FROM:Ljava/lang/String; = "rename_from"

.field public static final REQUEST_RENAME_TO:Ljava/lang/String; = "rename_to"

.field public static final REQUEST_TITLE:Ljava/lang/String; = "title"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
