.class public Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;
.super Ljava/lang/Object;
.source "SideSyncControl.java"


# static fields
.field private static final MODULE:Ljava/lang/String; = "SideSyncControlService"

.field public static final SIDESYNC_STATE_CONNECTING:I = 0x2

.field public static final SIDESYNC_STATE_HOLD:I = 0x5

.field public static final SIDESYNC_STATE_PREPARE:I = 0x4

.field public static final SIDESYNC_STATE_READY:I = 0x1

.field public static final SIDESYNC_STATE_UNKNOWN:I = 0x0

.field public static final SIDESYNC_STATE_WORKING:I = 0x3

.field private static mContentResolver:Landroid/content/ContentResolver;


# instance fields
.field private isConnect:Z

.field protected mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/ContentResolver;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cr"    # Landroid/content/ContentResolver;

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;->isConnect:Z

    .line 53
    iput-object p1, p0, Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;->mContext:Landroid/content/Context;

    .line 55
    sput-object p2, Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;->mContentResolver:Landroid/content/ContentResolver;

    .line 56
    return-void
.end method

.method public static isKMSRunning(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 158
    const-string v5, "activity"

    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 160
    .local v0, "am":Landroid/app/ActivityManager;
    const v5, 0x7fffffff

    invoke-virtual {v0, v5}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 162
    .local v3, "serviceInfo":Landroid/app/ActivityManager$RunningServiceInfo;
    iget-object v5, v3, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v5}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    .line 164
    .local v2, "mServiceName":Ljava/lang/String;
    const-string v5, "com.sec.android.sidesync.kms.sink.service.SideSyncServerService"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "com.sec.android.sidesync.kms.source.service.SideSyncService"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 167
    :cond_1
    const-string v5, "SideSyncControlService"

    const-string v6, "isKMSRunning - return true "

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 168
    const/4 v4, 0x1

    .line 175
    .end local v2    # "mServiceName":Ljava/lang/String;
    .end local v3    # "serviceInfo":Landroid/app/ActivityManager$RunningServiceInfo;
    :goto_0
    return v4

    .line 174
    :cond_2
    const-string v5, "SideSyncControlService"

    const-string v6, "isKMSRunning - return false "

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static isPSSRunning()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 180
    sget-object v2, Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;->mContentResolver:Landroid/content/ContentResolver;

    const-string v3, "sidesync_source_connect"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 182
    const-string v2, "SideSyncControlService"

    const-string v3, "isPSSRunning - return true "

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 188
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
