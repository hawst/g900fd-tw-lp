.class public Lcom/sec/android/app/myfiles/ftp/FTPCache;
.super Ljava/lang/Object;
.source "FTPCache.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;


# static fields
.field private static final mSync:Ljava/util/concurrent/locks/ReentrantLock;


# instance fields
.field private final mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 457
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mSync:Ljava/util/concurrent/locks/ReentrantLock;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-direct {v0, p1}, Lcom/sec/android/app/myfiles/db/CacheDB;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    .line 39
    return-void
.end method


# virtual methods
.method public clearBuffer()V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->clearFtpBuffer()V

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 151
    return-void
.end method

.method public clearCache()V
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 332
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->clearFTPData()V

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 336
    return-void
.end method

.method public getBufferContent(II)Landroid/database/Cursor;
    .locals 2
    .param p1, "sortBy"    # I
    .param p2, "order"    # I

    .prologue
    .line 67
    const/4 v0, 0x0

    .line 69
    .local v0, "cursor":Landroid/database/Cursor;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 71
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFtpBufferContent(II)Landroid/database/Cursor;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 75
    return-object v0
.end method

.method public getBufferContent(IILcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 3
    .param p1, "sortBy"    # I
    .param p2, "order"    # I
    .param p3, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p4, "path"    # Ljava/lang/String;

    .prologue
    .line 95
    const/4 v0, 0x0

    .line 97
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz p3, :cond_0

    .line 99
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 101
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {p3}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p1, p2, v2, p4}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFtpBufferContent(IILjava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 103
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 107
    :cond_0
    return-object v0
.end method

.method public getBufferContent(Ljava/util/ArrayList;II)Landroid/database/Cursor;
    .locals 2
    .param p2, "sortBy"    # I
    .param p3, "order"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;II)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .line 81
    .local p1, "exts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 83
    .local v0, "cursor":Landroid/database/Cursor;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 85
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1, p1, p2, p3}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFtpBufferContent(Ljava/util/ArrayList;II)Landroid/database/Cursor;

    move-result-object v0

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 89
    return-object v0
.end method

.method public getBufferContent(Ljava/util/ArrayList;IILcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p2, "sortBy"    # I
    .param p3, "order"    # I
    .param p4, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p5, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;II",
            "Lcom/sec/android/app/myfiles/ftp/FTPParams;",
            "Ljava/lang/String;",
            ")",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .line 113
    .local p1, "exts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v6, 0x0

    .line 115
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz p4, :cond_0

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {p4}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v4

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFtpBufferContent(Ljava/util/ArrayList;IILjava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 125
    :cond_0
    return-object v6
.end method

.method public getBufferItems(II)Ljava/util/List;
    .locals 9
    .param p1, "sortBy"    # I
    .param p2, "order"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 290
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 292
    .local v3, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    iget-object v8, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 294
    iget-object v8, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v8, p1, p2}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFtpBufferContent(II)Landroid/database/Cursor;

    move-result-object v0

    .line 296
    .local v0, "cr":Landroid/database/Cursor;
    if-eqz v0, :cond_2

    .line 298
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 302
    :cond_0
    const-string v8, "title"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 304
    .local v6, "title":Ljava/lang/String;
    const-string v8, "is_directory"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 306
    .local v7, "type":I
    const-string v8, "file_size"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 308
    .local v4, "size":J
    const-string v8, "_data"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 310
    .local v2, "fullPath":Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/ftp/FTPUtils;->getParentDir(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 312
    .local v1, "directory":Ljava/lang/String;
    invoke-static {v6, v7, v1, v4, v5}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getItem(Ljava/lang/String;ILjava/lang/String;J)Lcom/sec/android/app/myfiles/ftp/FTPItem;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 314
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-nez v8, :cond_0

    .line 318
    .end local v1    # "directory":Ljava/lang/String;
    .end local v2    # "fullPath":Ljava/lang/String;
    .end local v4    # "size":J
    .end local v6    # "title":Ljava/lang/String;
    .end local v7    # "type":I
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 322
    :cond_2
    iget-object v8, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 324
    return-object v3
.end method

.method public getBufferItems(IILcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;)Ljava/util/List;
    .locals 10
    .param p1, "sortBy"    # I
    .param p2, "order"    # I
    .param p3, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p4, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/sec/android/app/myfiles/ftp/FTPParams;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 250
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 252
    .local v3, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    iget-object v8, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 254
    iget-object v8, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {p3}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, p1, p2, v9, p4}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFtpBufferContent(IILjava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 256
    .local v0, "cr":Landroid/database/Cursor;
    if-eqz v0, :cond_2

    .line 258
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 262
    :cond_0
    const-string v8, "title"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 264
    .local v6, "title":Ljava/lang/String;
    const-string v8, "is_directory"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 266
    .local v7, "type":I
    const-string v8, "file_size"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 268
    .local v4, "size":J
    const-string v8, "_data"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 270
    .local v2, "fullPath":Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/ftp/FTPUtils;->getParentDir(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 272
    .local v1, "directory":Ljava/lang/String;
    invoke-static {v6, v7, v1, v4, v5}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getItem(Ljava/lang/String;ILjava/lang/String;J)Lcom/sec/android/app/myfiles/ftp/FTPItem;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 274
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-nez v8, :cond_0

    .line 278
    .end local v1    # "directory":Ljava/lang/String;
    .end local v2    # "fullPath":Ljava/lang/String;
    .end local v4    # "size":J
    .end local v6    # "title":Ljava/lang/String;
    .end local v7    # "type":I
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 282
    :cond_2
    iget-object v8, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 284
    return-object v3
.end method

.method public getCurrentDBContent(Ljava/util/ArrayList;II)Landroid/database/Cursor;
    .locals 3
    .param p2, "sortBy"    # I
    .param p3, "sortOrder"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;II)",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    .line 43
    .local p1, "exts":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 45
    .local v0, "cursor":Landroid/database/Cursor;
    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mSync:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 49
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 51
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1, p1, p2, p3}, Lcom/sec/android/app/myfiles/db/CacheDB;->getFTPList(Ljava/util/ArrayList;II)Landroid/database/Cursor;

    move-result-object v0

    .line 53
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mSync:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 61
    return-object v0

    .line 57
    :catchall_0
    move-exception v1

    sget-object v2, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mSync:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v1
.end method

.method public getLastUpdateTime(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;)J
    .locals 4
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 232
    const-wide/16 v0, -0x1

    .line 234
    .local v0, "lastUpdateTime":J
    if-eqz p1, :cond_0

    .line 236
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 238
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, p2}, Lcom/sec/android/app/myfiles/db/CacheDB;->getLastUpdateTime(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    .line 240
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 244
    :cond_0
    return-wide v0
.end method

.method public insertItems(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 2
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p2, "timestamp"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/myfiles/ftp/FTPParams;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 155
    .local p4, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    if-eqz p1, :cond_0

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p4, p3}, Lcom/sec/android/app/myfiles/db/CacheDB;->insertFtpData(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 165
    :cond_0
    return-void
.end method

.method public insertItemsAndLoadBuffer(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;II)V
    .locals 3
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p2, "timestamp"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;
    .param p5, "sortBy"    # I
    .param p6, "order"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/myfiles/ftp/FTPParams;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .local p4, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    const/4 v2, 0x2

    .line 170
    if-eqz p1, :cond_0

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 175
    sget-boolean v0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mAbortBrowsing:Z

    if-eqz v0, :cond_1

    .line 177
    const-string v0, "ftp"

    const-string v1, "mAbortBrowsing ture insertFtpData !!! RETURN"

    invoke-static {v2, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 181
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p4, p3}, Lcom/sec/android/app/myfiles/db/CacheDB;->insertFtpData(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    .line 183
    sget-boolean v0, Lcom/sec/android/app/myfiles/ftp/operation/BrowseOperation;->mAbortBrowsing:Z

    if-eqz v0, :cond_2

    .line 184
    const-string v0, "ftp"

    const-string v1, "mAbortBrowsing ture loadFtpBufferWithContent !!! RETURN "

    invoke-static {v2, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    goto :goto_0

    .line 195
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    goto :goto_0
.end method

.method public isPathCached(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;)Z
    .locals 2
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 218
    const/4 v0, 0x0

    .line 220
    .local v0, "result":Z
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 222
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/app/myfiles/db/CacheDB;->isFtpPathCached(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;)Z

    move-result v0

    .line 224
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 226
    return v0
.end method

.method public isPathCached(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p2, "timestamp"    # Ljava/lang/String;
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 131
    const/4 v0, 0x0

    .line 133
    .local v0, "result":Z
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 135
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1, p1, p2, p3}, Lcom/sec/android/app/myfiles/db/CacheDB;->isFtpPathCached(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 137
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 139
    return v0
.end method

.method public loadBuffer(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;II)V
    .locals 0
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "sortBy"    # I
    .param p4, "order"    # I

    .prologue
    .line 214
    return-void
.end method

.method public notifyCreateFolder(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;)V
    .locals 2
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 356
    if-eqz p1, :cond_0

    .line 358
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 360
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/myfiles/db/CacheDB;->insertFtpNewFolder(Ljava/lang/String;Ljava/lang/String;)J

    .line 362
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 366
    :cond_0
    return-void
.end method

.method public notifyDeleteFile(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;)V
    .locals 2
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 371
    if-eqz p1, :cond_0

    .line 373
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 375
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/myfiles/db/CacheDB;->deleteFtpFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 381
    :cond_0
    return-void
.end method

.method public notifyDeleteFiles(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/util/Collection;)V
    .locals 4
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/myfiles/ftp/FTPParams;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 386
    .local p2, "paths":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 388
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v2

    .line 390
    .local v2, "server":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 392
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 394
    .local v1, "path":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v3, v2, v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->deleteFtpFile(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 398
    .end local v1    # "path":Ljava/lang/String;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 402
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "server":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public notifyDeleteFolder(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;)V
    .locals 2
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 407
    if-eqz p1, :cond_0

    .line 409
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 411
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/myfiles/db/CacheDB;->deleteFtpFolder(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 417
    :cond_0
    return-void
.end method

.method public notifyDeleteFolders(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/util/Collection;)V
    .locals 4
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/myfiles/ftp/FTPParams;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 422
    .local p2, "paths":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 424
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v2

    .line 426
    .local v2, "server":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 428
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 430
    .local v1, "path":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v3, v2, v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->deleteFtpFolder(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 434
    .end local v1    # "path":Ljava/lang/String;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 438
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "server":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public notifyRenameItem(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p2, "from"    # Ljava/lang/String;
    .param p3, "to"    # Ljava/lang/String;

    .prologue
    .line 443
    if-eqz p1, :cond_0

    .line 445
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 447
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3}, Lcom/sec/android/app/myfiles/db/CacheDB;->renameFtpItem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 453
    :cond_0
    return-void
.end method

.method public notifyUploadFile(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p2, "from"    # Ljava/lang/String;
    .param p3, "to"    # Ljava/lang/String;

    .prologue
    .line 341
    if-eqz p1, :cond_0

    .line 343
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 345
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getFullLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3}, Lcom/sec/android/app/myfiles/db/CacheDB;->uploadFtpFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPCache;->mCacheDb:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 351
    :cond_0
    return-void
.end method
