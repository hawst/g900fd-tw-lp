.class public Lcom/sec/android/app/myfiles/adapter/FrequentlyUsedAdapter;
.super Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
.source "FrequentlyUsedAdapter.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "c"    # Landroid/database/Cursor;
    .param p4, "flags"    # I
    .param p5, "navigation"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .prologue
    .line 30
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 31
    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 86
    return-void
.end method

.method public finishSelectMode()V
    .locals 0

    .prologue
    .line 41
    return-void
.end method

.method protected getContentType()I
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x0

    return v0
.end method

.method public getSelectedItem()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSelectedItemCount()I
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    return v0
.end method

.method public getSelectedItemPosition()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    const/4 v0, 0x0

    return-object v0
.end method

.method public isSelectMode()Z
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    return v0
.end method

.method public selectAllItem()V
    .locals 0

    .prologue
    .line 57
    return-void
.end method

.method public selectItem(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 52
    return-void
.end method

.method public setSelectedItemChangeListener(Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;

    .prologue
    .line 67
    return-void
.end method

.method public startSelectMode(II)V
    .locals 0
    .param p1, "selectType"    # I
    .param p2, "from"    # I

    .prologue
    .line 36
    return-void
.end method

.method public unselectAllItem()V
    .locals 0

    .prologue
    .line 62
    return-void
.end method
