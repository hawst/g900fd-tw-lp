.class public Lcom/sec/android/app/myfiles/ftp/BrowseService;
.super Landroid/app/IntentService;
.source "BrowseService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/ftp/BrowseService$1;,
        Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;
    }
.end annotation


# static fields
.field private static final MODULE:Ljava/lang/String;


# instance fields
.field private mBrowseHandler:Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;

.field private final mFactory:Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 154
    const-class v0, Lcom/sec/android/app/myfiles/ftp/BrowseService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/BrowseService;->MODULE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/sec/android/app/myfiles/ftp/BrowseService;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 152
    invoke-static {}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->getInstance()Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/BrowseService;->mFactory:Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;

    .line 195
    invoke-static {}, Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;->getInstance()Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/BrowseService;->mBrowseHandler:Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;

    .line 35
    return-void
.end method

.method private broadcastResult(Lcom/sec/android/app/myfiles/ftp/FTPParams;Lcom/sec/android/app/myfiles/ftp/IFTPItem;)V
    .locals 3
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p2, "browseItem"    # Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .prologue
    .line 95
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.myfiles.ftp.BACKGROUND_BROWSE_DONE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 97
    .local v0, "resultIntent":Landroid/content/Intent;
    sget-object v1, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;->FTP_PARAMS:Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 99
    sget-object v1, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;->BROWSE_PATH:Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/ftp/BrowseService;->trimPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 101
    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/ftp/BrowseService;->sendBroadcast(Landroid/content/Intent;)V

    .line 103
    return-void
.end method

.method private static getFtpBrowseItem(Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/FTPParams;)Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .locals 3
    .param p0, "requestedItemStr"    # Ljava/lang/String;
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .prologue
    .line 121
    if-eqz p1, :cond_0

    .line 125
    sget-object v1, Lcom/sec/android/app/myfiles/ftp/BrowseService$1;->$SwitchMap$com$sec$android$app$myfiles$ftp$FTPType:[I

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getType()Lcom/sec/android/app/myfiles/ftp/FTPType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 136
    const/4 v0, 0x0

    .line 146
    :goto_0
    return-object v0

    .line 130
    :pswitch_0
    invoke-static {p0}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getItem(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPItem;

    move-result-object v0

    .line 132
    .local v0, "browseItem":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    goto :goto_0

    .line 146
    .end local v0    # "browseItem":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 125
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private storeBrowseResult(Lcom/sec/android/app/myfiles/ftp/FTPParams;Lcom/sec/android/app/myfiles/ftp/IFTPItem;Ljava/util/List;)V
    .locals 7
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p2, "browseItem"    # Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/myfiles/ftp/FTPParams;",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "children":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    const/4 v5, 0x0

    .line 78
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPCache;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/ftp/FTPCache;-><init>(Landroid/content/Context;)V

    .line 80
    .local v0, "cache":Lcom/sec/android/app/myfiles/ftp/FTPCache;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/BrowseService;->mBrowseHandler:Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;

    invoke-interface {p2}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/ftp/BrowseService;->trimFtpPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;->isBrowsePending(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 82
    invoke-static {}, Lcom/sec/android/app/myfiles/ftp/FTPUtils;->getCurrentTimeStr()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v3

    move-object v1, p1

    move-object v4, p3

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/myfiles/ftp/FTPCache;->insertItemsAndLoadBuffer(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;II)V

    .line 91
    :goto_0
    return-void

    .line 87
    :cond_0
    invoke-static {}, Lcom/sec/android/app/myfiles/ftp/FTPUtils;->getCurrentTimeStr()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2, p3}, Lcom/sec/android/app/myfiles/ftp/FTPCache;->insertItems(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0
.end method

.method private static final trimFtpPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 183
    if-eqz p0, :cond_0

    .line 185
    const-string v0, "//"

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 189
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Ljava/io/File;->separator:Ljava/lang/String;

    goto :goto_0
.end method

.method private static trimPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 107
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    sget-object v0, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 113
    .end local p0    # "path":Ljava/lang/String;
    :cond_0
    return-object p0
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 10
    .param p1, "workIntent"    # Landroid/content/Intent;

    .prologue
    .line 40
    sget-object v6, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;->FTP_PARAMS:Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 42
    .local v4, "ftpParamsStr":Ljava/lang/String;
    sget-object v6, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;->BROWSE_ITEM:Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 44
    .local v5, "requestedItemStr":Ljava/lang/String;
    const/4 v6, 0x1

    sget-object v7, Lcom/sec/android/app/myfiles/ftp/BrowseService;->MODULE:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Got the browse request for the item: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    .line 46
    new-instance v3, Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-direct {v3, v4}, Lcom/sec/android/app/myfiles/ftp/FTPParams;-><init>(Ljava/lang/String;)V

    .line 48
    .local v3, "ftpParams":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/ftp/BrowseService;->mFactory:Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;

    invoke-virtual {v6, v3}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->getFTPConnectionHandlerForService(Lcom/sec/android/app/myfiles/ftp/FTPParams;)Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    move-result-object v2

    .line 50
    .local v2, "ftpHandler":Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    if-eqz v2, :cond_0

    .line 52
    invoke-static {v5, v3}, Lcom/sec/android/app/myfiles/ftp/BrowseService;->getFtpBrowseItem(Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/FTPParams;)Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-result-object v0

    .line 54
    .local v0, "browseItem":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    if-eqz v0, :cond_0

    .line 56
    invoke-interface {v2}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncPrepareConnection()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 58
    invoke-interface {v2, v0}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncBrowseForItemContent(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)Ljava/util/List;

    move-result-object v1

    .line 60
    .local v1, "folderContent":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    if-eqz v1, :cond_0

    .line 62
    invoke-direct {p0, v3, v0, v1}, Lcom/sec/android/app/myfiles/ftp/BrowseService;->storeBrowseResult(Lcom/sec/android/app/myfiles/ftp/FTPParams;Lcom/sec/android/app/myfiles/ftp/IFTPItem;Ljava/util/List;)V

    .line 64
    invoke-direct {p0, v3, v0}, Lcom/sec/android/app/myfiles/ftp/BrowseService;->broadcastResult(Lcom/sec/android/app/myfiles/ftp/FTPParams;Lcom/sec/android/app/myfiles/ftp/IFTPItem;)V

    .line 74
    .end local v0    # "browseItem":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .end local v1    # "folderContent":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    :cond_0
    return-void
.end method
