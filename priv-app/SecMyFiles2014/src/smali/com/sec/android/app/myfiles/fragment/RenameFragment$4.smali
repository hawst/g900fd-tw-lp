.class Lcom/sec/android/app/myfiles/fragment/RenameFragment$4;
.super Ljava/lang/Object;
.source "RenameFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/RenameFragment;->setupInputEditText(Landroid/widget/EditText;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/RenameFragment;)V
    .locals 0

    .prologue
    .line 231
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 257
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 253
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mOkButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 235
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result p4

    .line 236
    if-ge p4, v3, :cond_1

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mOkButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 249
    :cond_0
    :goto_0
    return-void

    .line 240
    :cond_1
    if-lez p4, :cond_2

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mSrcFileName:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/RenameFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 242
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mOkButton:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 246
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/RenameFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->mOkButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method
