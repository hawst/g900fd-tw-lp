.class Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$25;
.super Ljava/lang/Object;
.source "CategoryHomeFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V
    .locals 0

    .prologue
    .line 2347
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$25;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/16 v6, 0x1f

    .line 2352
    invoke-virtual {p2}, Landroid/view/View;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_1

    .line 2435
    :cond_0
    :goto_0
    return-void

    .line 2355
    :cond_1
    long-to-int v1, p4

    .line 2357
    .local v1, "categoryType":I
    const/4 v0, 0x0

    .line 2359
    .local v0, "argument":Landroid/os/Bundle;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$25;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->isSelectMode()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2361
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$25;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->finishSelectMode()V

    .line 2364
    :cond_2
    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 2380
    :sswitch_0
    const/16 v3, 0x8

    if-eq v1, v3, :cond_3

    if-ne v1, v6, :cond_5

    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$25;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isWifiOn(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 2382
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$25;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iput v1, v3, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mModeForWarningDialog:I

    .line 2383
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$25;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckDropBox()Z

    move-result v3

    if-nez v3, :cond_5

    .line 2385
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$25;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setDropBoxFromShortcut(Z)V

    .line 2386
    if-ne v1, v6, :cond_4

    .line 2387
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$25;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->showDialog(I)V

    goto :goto_0

    .line 2389
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$25;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->showDialog(I)V

    goto :goto_0

    .line 2395
    :cond_5
    sget-boolean v3, Lcom/sec/android/app/myfiles/utils/Utils;->mIsActiveWarningUsingWLAN:Z

    if-eqz v3, :cond_6

    const-string v3, "ChinaNalSecurity"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v4

    const-string v5, "CscFeature_Common_ConfigLocalSecurityPolicy"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2397
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$25;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isWifiOn(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_6

    if-ne v1, v6, :cond_6

    .line 2399
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$25;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iput v1, v3, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mModeForWarningDialog:I

    .line 2400
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$25;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckWLANOn()Z

    move-result v3

    if-nez v3, :cond_6

    .line 2401
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$25;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/16 v4, 0xb

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->showDialog(I)V

    goto/16 :goto_0

    .line 2410
    :sswitch_1
    const/16 v1, 0x201

    .line 2412
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/element/CategoryItem;

    .line 2414
    .local v2, "item":Lcom/sec/android/app/myfiles/element/CategoryItem;
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "argument":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2416
    .restart local v0    # "argument":Landroid/os/Bundle;
    const-string v3, "FOLDERPATH"

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/CategoryItem;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2431
    .end local v2    # "item":Lcom/sec/android/app/myfiles/element/CategoryItem;
    :cond_6
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$25;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v3, v3, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v3, :cond_0

    .line 2432
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$25;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setStartBrowserState(Z)V

    .line 2433
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$25;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v3, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v3, v1, v0}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto/16 :goto_0

    .line 2419
    :sswitch_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$25;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->getFTPShortCutCount(Landroid/content/Context;)I

    move-result v3

    if-nez v3, :cond_7

    .line 2420
    const/16 v1, 0xd

    goto :goto_1

    .line 2422
    :cond_7
    const/16 v1, 0x11

    .line 2423
    new-instance v0, Landroid/os/Bundle;

    .end local v0    # "argument":Landroid/os/Bundle;
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2426
    .restart local v0    # "argument":Landroid/os/Bundle;
    goto :goto_1

    .line 2364
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_0
        0x4 -> :sswitch_0
        0x5 -> :sswitch_0
        0x6 -> :sswitch_0
        0x7 -> :sswitch_0
        0x8 -> :sswitch_0
        0xd -> :sswitch_2
        0x1a -> :sswitch_1
        0x1f -> :sswitch_0
        0x28 -> :sswitch_0
        0x201 -> :sswitch_0
    .end sparse-switch
.end method
