.class Lcom/sec/android/app/myfiles/MainActivity$8;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Landroid/widget/SearchView$OnQueryTextListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/MainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/MainActivity;)V
    .locals 0

    .prologue
    .line 2823
    iput-object p1, p0, Lcom/sec/android/app/myfiles/MainActivity$8;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 5
    .param p1, "newText"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 2847
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x32

    if-gt v0, v1, :cond_0

    .line 2849
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$8;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # setter for: Lcom/sec/android/app/myfiles/MainActivity;->preText:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/sec/android/app/myfiles/MainActivity;->access$1302(Lcom/sec/android/app/myfiles/MainActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 2857
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$8;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    new-instance v1, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$8;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mParameter:Lcom/sec/android/app/myfiles/utils/SearchParameter;

    invoke-direct {v1, v2}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;-><init>(Lcom/sec/android/app/myfiles/utils/SearchParameter;)V

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$8;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentSearchType:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->searchType(I)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$8;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mStorageType:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->storageType(Ljava/util/ArrayList;)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$8;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFileTypes:Ljava/util/EnumSet;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->fileTypes(Ljava/util/EnumSet;)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$8;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-wide v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFromDate:J

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->dateFrom(J)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$8;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-wide v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentToDate:J

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->dateTo(J)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$8;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mExtentionString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->extentionString(Ljava/lang/String;)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->keyword(Ljava/lang/String;)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->build()Lcom/sec/android/app/myfiles/utils/SearchParameter;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/myfiles/MainActivity;->mParameter:Lcom/sec/android/app/myfiles/utils/SearchParameter;

    .line 2865
    return v4

    .line 2853
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$8;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/MainActivity;->access$300(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0077

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2854
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$8;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity$8;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->preText:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/MainActivity;->access$1300(Lcom/sec/android/app/myfiles/MainActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    goto :goto_0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 4
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 2828
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$8;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mSelectionTypeChange:Z
    invoke-static {v0}, Lcom/sec/android/app/myfiles/MainActivity;->access$1100(Lcom/sec/android/app/myfiles/MainActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2829
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$8;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity$8;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->fragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/MainActivity;->access$1200(Lcom/sec/android/app/myfiles/MainActivity;)Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/MainActivity;->SearchFragmentCommit(Lcom/sec/android/app/myfiles/fragment/AbsFragment;)V

    .line 2830
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$8;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/myfiles/MainActivity;->mSelectionTypeChange:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/MainActivity;->access$1102(Lcom/sec/android/app/myfiles/MainActivity;Z)Z

    .line 2832
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$8;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    new-instance v1, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$8;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mParameter:Lcom/sec/android/app/myfiles/utils/SearchParameter;

    invoke-direct {v1, v2}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;-><init>(Lcom/sec/android/app/myfiles/utils/SearchParameter;)V

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$8;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentSearchType:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->searchType(I)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$8;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mStorageType:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->storageType(Ljava/util/ArrayList;)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$8;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFileTypes:Ljava/util/EnumSet;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->fileTypes(Ljava/util/EnumSet;)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$8;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-wide v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFromDate:J

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->dateFrom(J)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$8;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-wide v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentToDate:J

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->dateTo(J)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$8;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mExtentionString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->extentionString(Ljava/lang/String;)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->keyword(Ljava/lang/String;)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->build()Lcom/sec/android/app/myfiles/utils/SearchParameter;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/app/myfiles/MainActivity;->mParameter:Lcom/sec/android/app/myfiles/utils/SearchParameter;

    .line 2840
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$8;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/MainActivity;->access$000(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2841
    const/4 v0, 0x1

    return v0
.end method
