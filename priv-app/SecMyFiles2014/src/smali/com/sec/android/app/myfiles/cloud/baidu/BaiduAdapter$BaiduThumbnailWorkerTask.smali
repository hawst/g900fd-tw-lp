.class Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;
.super Landroid/os/AsyncTask;
.source "BaiduAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BaiduThumbnailWorkerTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$IdPath;",
        "Ljava/lang/Integer;",
        "Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;",
        ">;"
    }
.end annotation


# instance fields
.field private mContentsType:I

.field private mFilePath:Ljava/lang/String;

.field private mImageView:Landroid/widget/ImageView;

.field public mStarted:Z

.field private mThumbnailCacheFile:Ljava/io/File;

.field final synthetic this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;Landroid/widget/ImageView;I)V
    .locals 1
    .param p2, "imageView"    # Landroid/widget/ImageView;
    .param p3, "type"    # I

    .prologue
    .line 654
    iput-object p1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 650
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->mThumbnailCacheFile:Ljava/io/File;

    .line 651
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->mFilePath:Ljava/lang/String;

    .line 652
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->mStarted:Z

    .line 655
    iput-object p2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->mImageView:Landroid/widget/ImageView;

    .line 656
    iput p3, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->mContentsType:I

    .line 657
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;Landroid/widget/ImageView;ILjava/lang/String;)V
    .locals 0
    .param p2, "imageView"    # Landroid/widget/ImageView;
    .param p3, "type"    # I
    .param p4, "filePath"    # Ljava/lang/String;

    .prologue
    .line 660
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;-><init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;Landroid/widget/ImageView;I)V

    .line 661
    iput-object p4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->mFilePath:Ljava/lang/String;

    .line 662
    return-void
.end method

.method private postExecuteActions(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;)V
    .locals 4
    .param p1, "result"    # Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;

    .prologue
    .line 724
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mThumbnailWorkerTasks:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 725
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mThumbnailWorkerTasks:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne v1, p0, :cond_2

    .line 726
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mThumbnailWorkerTasks:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 730
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mThumbnailWorkerTasks:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 731
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mThumbnailWorkerTasks:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;

    iget-boolean v1, v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->mStarted:Z

    if-nez v1, :cond_3

    .line 732
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mThumbnailWorkerTasks:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->mStarted:Z

    .line 733
    iget-object v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mThumbnailWorkerTasks:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;

    sget-object v2, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x0

    new-array v3, v3, [Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$IdPath;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 739
    :cond_1
    return-void

    .line 724
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 730
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$IdPath;)Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;
    .locals 12
    .param p1, "params"    # [Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$IdPath;

    .prologue
    const/4 v8, 0x0

    .line 666
    iget-object v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->mFilePath:Ljava/lang/String;

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->mFilePath:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    if-nez v9, :cond_1

    .line 720
    :cond_0
    :goto_0
    return-object v8

    .line 668
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->isCancelled()Z

    move-result v9

    if-nez v9, :cond_0

    .line 672
    iget-object v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->access$000(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;)Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    move-result-object v2

    .line 674
    .local v2, "baiduAPIHelper":Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;
    iget v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->mContentsType:I

    packed-switch v9, :pswitch_data_0

    goto :goto_0

    .line 676
    :pswitch_0
    iget-object v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->mFilePath:Ljava/lang/String;

    invoke-static {v9}, Lcom/sec/android/app/myfiles/utils/Utils;->getFileThumbnailFolderName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 677
    .local v4, "serverFolderName":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 678
    .local v1, "ThumbnailCacheFolder":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_2

    .line 679
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 681
    :cond_2
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "large.jpg"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 683
    .local v0, "ThumbnailCacheFilePath":Ljava/lang/String;
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->mThumbnailCacheFile:Ljava/io/File;

    .line 685
    iget-object v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->mThumbnailCacheFile:Ljava/io/File;

    iget-object v10, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->mFilePath:Ljava/lang/String;

    new-instance v11, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$ConcreteProgressListener;

    invoke-direct {v11}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$ConcreteProgressListener;-><init>()V

    invoke-virtual {v2, v9, v10, v11}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->getThumbnail(Ljava/io/File;Ljava/lang/String;Lcom/samsung/scloud/response/ProgressListener;)V

    .line 688
    iget-object v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->mThumbnailCacheFile:Ljava/io/File;

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 691
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 692
    .local v3, "bitmapImage":Landroid/graphics/Bitmap;
    new-instance v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;

    iget-object v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->mFilePath:Ljava/lang/String;

    invoke-direct {v8, v9, v3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;-><init>(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 701
    .end local v0    # "ThumbnailCacheFilePath":Ljava/lang/String;
    .end local v1    # "ThumbnailCacheFolder":Ljava/io/File;
    .end local v3    # "bitmapImage":Landroid/graphics/Bitmap;
    .end local v4    # "serverFolderName":Ljava/lang/String;
    :pswitch_1
    iget-object v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->mFilePath:Ljava/lang/String;

    invoke-static {v9}, Lcom/sec/android/app/myfiles/utils/Utils;->getFileThumbnailFolderName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 702
    .local v5, "videoServerFolderName":Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 703
    .local v7, "videoThumbnailCacheFolder":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_3

    .line 704
    invoke-virtual {v7}, Ljava/io/File;->mkdirs()Z

    .line 706
    :cond_3
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "large.jpg"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 707
    .local v6, "videoThumbnailCacheFilePath":Ljava/lang/String;
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->mThumbnailCacheFile:Ljava/io/File;

    .line 708
    iget-object v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->mThumbnailCacheFile:Ljava/io/File;

    iget-object v10, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->mFilePath:Ljava/lang/String;

    new-instance v11, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$ConcreteProgressListener;

    invoke-direct {v11}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$ConcreteProgressListener;-><init>()V

    invoke-virtual {v2, v9, v10, v11}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->getThumbnail(Ljava/io/File;Ljava/lang/String;Lcom/samsung/scloud/response/ProgressListener;)V

    .line 710
    iget-object v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->mThumbnailCacheFile:Ljava/io/File;

    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 711
    invoke-static {v6}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 712
    .restart local v3    # "bitmapImage":Landroid/graphics/Bitmap;
    new-instance v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;

    iget-object v9, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->mFilePath:Ljava/lang/String;

    invoke-direct {v8, v9, v3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;-><init>(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 674
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 647
    check-cast p1, [Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$IdPath;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->doInBackground([Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$IdPath;)Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;)V
    .locals 0
    .param p1, "result"    # Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;

    .prologue
    .line 743
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->postExecuteActions(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;)V

    .line 744
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onCancelled(Ljava/lang/Object;)V

    .line 745
    return-void
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 647
    check-cast p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->onCancelled(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;)V

    return-void
.end method

.method protected onPostExecute(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;)V
    .locals 3
    .param p1, "result"    # Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;

    .prologue
    .line 749
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 750
    iget-object v0, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;->mPath:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 751
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->mThumbnailCache:Landroid/util/LruCache;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;->access$200(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter;)Landroid/util/LruCache;

    move-result-object v0

    iget-object v1, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;->mPath:Ljava/lang/String;

    iget-object v2, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 752
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->mImageView:Landroid/widget/ImageView;

    iget-object v1, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 755
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->postExecuteActions(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;)V

    .line 756
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 757
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 647
    check-cast p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduAdapter$BaiduThumbnailWorkerTask;->onPostExecute(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$AsyncResult;)V

    return-void
.end method
