.class public Lcom/sec/android/app/myfiles/drm/DrmUtils;
.super Ljava/lang/Object;
.source "DrmUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;
    }
.end annotation


# static fields
.field private static final MODULE:Ljava/lang/String; = "DrmUtils"

.field private static final OMADRM_MIMETYPE:Ljava/lang/String; = "application/vnd.oma.drm.content"

.field private static final PLAYREADY_PYA_MIMETYPE:Ljava/lang/String; = "audio/vnd.ms-playready.media.pya"

.field private static final PLAYREADY_PYV_MIMETYPE:Ljava/lang/String; = "video/vnd.ms-playready.media.pyv"

.field private static final PLAYREADY_WMA_MIMETYPE:Ljava/lang/String; = "audio/x-ms-wma"

.field private static final PLAYREADY_WMV_MIMETYPE:Ljava/lang/String; = "video/x-ms-wmv"

.field private static mDrmManager:Landroid/drm/DrmManagerClient;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/myfiles/drm/DrmUtils;->mDrmManager:Landroid/drm/DrmManagerClient;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 209
    return-void
.end method

.method private static getDrmManager(Landroid/content/Context;)Landroid/drm/DrmManagerClient;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 155
    sget-object v0, Lcom/sec/android/app/myfiles/drm/DrmUtils;->mDrmManager:Landroid/drm/DrmManagerClient;

    if-nez v0, :cond_0

    .line 156
    new-instance v0, Landroid/drm/DrmManagerClient;

    invoke-direct {v0, p0}, Landroid/drm/DrmManagerClient;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/myfiles/drm/DrmUtils;->mDrmManager:Landroid/drm/DrmManagerClient;

    .line 159
    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/drm/DrmUtils;->mDrmManager:Landroid/drm/DrmManagerClient;

    return-object v0
.end method

.method private static getPermissionType(Landroid/content/Context;Ljava/lang/String;)I
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v4, -0x1

    const/4 v7, 0x0

    .line 526
    new-instance v1, Landroid/drm/DrmInfoRequest;

    const/16 v5, 0xf

    const-string v6, "application/vnd.oma.drm.content"

    invoke-direct {v1, v5, v6}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    .line 528
    .local v1, "infoRequest":Landroid/drm/DrmInfoRequest;
    const-string v5, "drm_path"

    invoke-virtual {v1, v5, p1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 530
    invoke-static {p0}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->getDrmManager(Landroid/content/Context;)Landroid/drm/DrmManagerClient;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v0

    .line 532
    .local v0, "drmInfo":Landroid/drm/DrmInfo;
    if-eqz v0, :cond_1

    .line 533
    const-string v5, "status"

    invoke-virtual {v0, v5}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 535
    .local v3, "status":Ljava/lang/String;
    const-string v5, "success"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 536
    const-string v5, "DrmUtils"

    const-string v6, "getRightInfo: acquireDrmInfo Success"

    invoke-static {v7, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 546
    const-string v5, "permission"

    invoke-virtual {v0, v5}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 548
    .local v2, "objPermType":Ljava/lang/Object;
    if-nez v2, :cond_2

    .line 549
    const-string v5, "DrmUtils"

    const-string v6, "getRightInfo: objPermType is null"

    invoke-static {v7, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 553
    .end local v2    # "objPermType":Ljava/lang/Object;
    .end local v3    # "status":Ljava/lang/String;
    :goto_0
    return v4

    .line 538
    .restart local v3    # "status":Ljava/lang/String;
    :cond_0
    const-string v5, "DrmUtils"

    const-string v6, "getRightInfo: acquireDrmInfo Fail"

    invoke-static {v7, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 542
    .end local v3    # "status":Ljava/lang/String;
    :cond_1
    const-string v5, "DrmUtils"

    const-string v6, "getRightInfo: acquireDrmInfo Fail"

    invoke-static {v7, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 553
    .restart local v2    # "objPermType":Ljava/lang/Object;
    .restart local v3    # "status":Ljava/lang/String;
    :cond_2
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    goto :goto_0
.end method

.method public static getPlayReadyRightInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const v8, 0x7f0b0052

    const v7, 0x7f0b004d

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 223
    if-nez p1, :cond_0

    .line 224
    const-string v4, "DrmUtils"

    const-string v5, "getPlayreadyRightInfo : filePath is null. "

    invoke-static {v9, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 225
    const/4 v0, 0x0

    .line 325
    :goto_0
    return-object v0

    .line 228
    :cond_0
    invoke-static {p0, p1}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isPlayReadyFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 229
    const-string v4, "DrmUtils"

    const-string v5, "getPlayReadyRightInfo : filePath is not PlayReady. "

    invoke-static {v9, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 230
    const/4 v0, 0x0

    goto :goto_0

    .line 233
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 235
    .local v0, "details":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;>;"
    new-instance v3, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;

    invoke-direct {v3}, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;-><init>()V

    .line 237
    .local v3, "rightInfo":Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;
    invoke-static {p0}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->getDrmManager(Landroid/content/Context;)Landroid/drm/DrmManagerClient;

    move-result-object v4

    invoke-virtual {v4, p1, v10}, Landroid/drm/DrmManagerClient;->getConstraints(Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v2

    .line 240
    .local v2, "rightDetails":Landroid/content/ContentValues;
    const-string v4, "license_category"

    invoke-virtual {v2, v4}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 243
    .local v1, "licenseCategory":Ljava/lang/String;
    const-string v4, "DrmUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "licenseCategory = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v9, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 244
    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    iput-object v4, v3, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->validityStr:[Ljava/lang/String;

    .line 246
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 317
    const/4 v4, 0x0

    iput-object v4, v3, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->typeStr:Ljava/lang/String;

    .line 320
    :goto_1
    const-string v4, "DrmUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "rightInfo.typeStr="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v3, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->typeStr:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "rightInfo.validityStr="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v3, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->validityStr:[Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v9, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 323
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 248
    :pswitch_0
    const-string v4, "DrmUtils"

    const-string v5, "getPlayReadyRightInfo: no right => "

    invoke-static {v9, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 249
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 251
    :pswitch_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b004c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->typeStr:Ljava/lang/String;

    goto :goto_1

    .line 254
    :pswitch_2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b004f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->typeStr:Ljava/lang/String;

    .line 255
    iget-object v4, v3, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->validityStr:[Ljava/lang/String;

    const-string v5, "%s/%s"

    new-array v6, v11, [Ljava/lang/Object;

    const-string v7, "remaining_repeat_count"

    invoke-virtual {v2, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v9

    const-string v7, "max_repeat_count"

    invoke-virtual {v2, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    goto :goto_1

    .line 260
    :pswitch_3
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->typeStr:Ljava/lang/String;

    .line 261
    iget-object v4, v3, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->validityStr:[Ljava/lang/String;

    const-string v5, "%s:%s"

    new-array v6, v11, [Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    const-string v7, "license_start_time"

    invoke-virtual {v2, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    goto/16 :goto_1

    .line 266
    :pswitch_4
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->typeStr:Ljava/lang/String;

    .line 267
    iget-object v4, v3, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->validityStr:[Ljava/lang/String;

    const-string v5, "%s:%s"

    new-array v6, v11, [Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0053

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    const-string v7, "license_expiry_time"

    invoke-virtual {v2, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    goto/16 :goto_1

    .line 272
    :pswitch_5
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->typeStr:Ljava/lang/String;

    .line 273
    iget-object v4, v3, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->validityStr:[Ljava/lang/String;

    const-string v5, "%s:%s"

    new-array v6, v11, [Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    const-string v7, "license_start_time"

    invoke-virtual {v2, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    .line 276
    iget-object v4, v3, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->validityStr:[Ljava/lang/String;

    const-string v5, "%s:%s"

    new-array v6, v11, [Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0053

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    const-string v7, "license_expiry_time"

    invoke-virtual {v2, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v10

    goto/16 :goto_1

    .line 281
    :pswitch_6
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->typeStr:Ljava/lang/String;

    .line 282
    iget-object v4, v3, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->validityStr:[Ljava/lang/String;

    const-string v5, "%s/%s"

    new-array v6, v11, [Ljava/lang/Object;

    const-string v7, "remaining_repeat_count"

    invoke-virtual {v2, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v9

    const-string v7, "max_repeat_count"

    invoke-virtual {v2, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    .line 285
    iget-object v4, v3, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->validityStr:[Ljava/lang/String;

    const-string v5, "%s:%s"

    new-array v6, v11, [Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    const-string v7, "license_start_time"

    invoke-virtual {v2, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v10

    goto/16 :goto_1

    .line 290
    :pswitch_7
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->typeStr:Ljava/lang/String;

    .line 291
    iget-object v4, v3, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->validityStr:[Ljava/lang/String;

    const-string v5, "%s/%s"

    new-array v6, v11, [Ljava/lang/Object;

    const-string v7, "max_repeat_count"

    invoke-virtual {v2, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v9

    const-string v7, "remaining_repeat_count"

    invoke-virtual {v2, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    .line 294
    iget-object v4, v3, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->validityStr:[Ljava/lang/String;

    const-string v5, "%s:%s"

    new-array v6, v11, [Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0053

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    const-string v7, "license_expiry_time"

    invoke-virtual {v2, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v10

    goto/16 :goto_1

    .line 299
    :pswitch_8
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->typeStr:Ljava/lang/String;

    .line 300
    iget-object v4, v3, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->validityStr:[Ljava/lang/String;

    const-string v5, "%s/%s"

    new-array v6, v11, [Ljava/lang/Object;

    const-string v7, "max_repeat_count"

    invoke-virtual {v2, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v9

    const-string v7, "remaining_repeat_count"

    invoke-virtual {v2, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    .line 303
    iget-object v4, v3, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->validityStr:[Ljava/lang/String;

    const-string v5, "%s:%s"

    new-array v6, v11, [Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    const-string v7, "license_start_time"

    invoke-virtual {v2, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v10

    .line 306
    iget-object v4, v3, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->validityStr:[Ljava/lang/String;

    const-string v5, "%s:%s"

    new-array v6, v11, [Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0053

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    const-string v7, "license_expiry_time"

    invoke-virtual {v2, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v11

    goto/16 :goto_1

    .line 311
    :pswitch_9
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b004e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->typeStr:Ljava/lang/String;

    .line 312
    iget-object v4, v3, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->validityStr:[Ljava/lang/String;

    const-string v5, "%s/%s"

    new-array v6, v11, [Ljava/lang/Object;

    const-string v7, "license_original_interval"

    invoke-virtual {v2, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v9

    const-string v7, "license_available_time"

    invoke-virtual {v2, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v9

    goto/16 :goto_1

    .line 246
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static getRealMimeTypeOfDRM(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 658
    if-nez p1, :cond_1

    .line 659
    const-string v1, "DrmUtils"

    const-string v2, "getRealMimeTypeOfDRM : filePath is null."

    invoke-static {v4, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 660
    const/4 v0, 0x0

    .line 682
    :cond_0
    :goto_0
    return-object v0

    .line 663
    :cond_1
    invoke-static {p1}, Lcom/sec/android/app/myfiles/MediaFile;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 665
    .local v0, "mimeType":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "application/vnd.oma.drm.content"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 666
    invoke-static {p0}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->getDrmManager(Landroid/content/Context;)Landroid/drm/DrmManagerClient;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/drm/DrmManagerClient;->getOriginalMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 667
    const-string v1, "DrmUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getRealMimeTypeOfDRM : real mime type = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 676
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/webkit/MimeTypeMap;->getExtensionFromMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 677
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/webkit/MimeTypeMap;->getExtensionFromMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/myfiles/MediaFile;->getMimeTypeForExtention(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 678
    :cond_2
    const-string v1, "DrmUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getRealMimeTypeOfDRM : reMapped to  = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getRightInfo(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 354
    if-nez p1, :cond_1

    .line 355
    const/4 v7, 0x0

    const-string v8, "DrmUtils"

    const-string v9, "getRightInfo : filePath is null."

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 356
    const/4 v1, 0x0

    .line 480
    :cond_0
    :goto_0
    return-object v1

    .line 359
    :cond_1
    invoke-static {p0, p1}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isOMADrmFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 360
    const/4 v7, 0x0

    const-string v8, "DrmUtils"

    const-string v9, "getRightInfo : filePath is not DRM."

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 361
    const/4 v1, 0x0

    goto :goto_0

    .line 365
    :cond_2
    invoke-static {p0, p1}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isForwardLockType(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 366
    const/4 v7, 0x0

    const-string v8, "DrmUtils"

    const-string v9, "getRightInfo: this is FL"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 367
    const/4 v1, 0x0

    goto :goto_0

    .line 370
    :cond_3
    const/4 v0, 0x1

    .line 372
    .local v0, "RoCount":I
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 374
    .local v1, "details":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;>;"
    new-instance v6, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;

    invoke-direct {v6}, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;-><init>()V

    .line 376
    .local v6, "rightInfo":Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;
    const/4 v4, -0x1

    .line 378
    .local v4, "permissionType":I
    invoke-static {p0, p1}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->getPermissionType(Landroid/content/Context;Ljava/lang/String;)I

    move-result v4

    .line 380
    const/4 v7, 0x0

    const-string v8, "DrmUtils"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getRightInfo: permissionType = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 382
    sparse-switch v4, :sswitch_data_0

    .line 414
    :goto_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    if-ge v2, v0, :cond_0

    .line 416
    sget-object v7, Lcom/sec/android/app/myfiles/drm/DrmUtils;->mDrmManager:Landroid/drm/DrmManagerClient;

    invoke-virtual {v7, p1, v4}, Landroid/drm/DrmManagerClient;->getConstraints(Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v5

    .line 418
    .local v5, "rightDetails":Landroid/content/ContentValues;
    if-eqz v5, :cond_4

    .line 420
    const-string v7, "license_category"

    invoke-virtual {v5, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 423
    .local v3, "licenseCategory":Ljava/lang/String;
    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/String;

    iput-object v7, v6, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->validityStr:[Ljava/lang/String;

    .line 425
    const/4 v7, 0x0

    const-string v8, "DrmUtils"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "licenseCategory = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 427
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    packed-switch v7, :pswitch_data_0

    .line 469
    :pswitch_0
    const/4 v7, 0x0

    iput-object v7, v6, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->typeStr:Ljava/lang/String;

    .line 477
    .end local v3    # "licenseCategory":Ljava/lang/String;
    :cond_4
    :goto_3
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 414
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 384
    .end local v2    # "i":I
    .end local v5    # "rightDetails":Landroid/content/ContentValues;
    :sswitch_0
    const v7, 0x7f0b0054

    iput v7, v6, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->permissionType:I

    goto :goto_1

    .line 387
    :sswitch_1
    const v7, 0x7f0b0055

    iput v7, v6, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->permissionType:I

    goto :goto_1

    .line 390
    :sswitch_2
    const v7, 0x7f0b0056

    iput v7, v6, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->permissionType:I

    goto :goto_1

    .line 429
    .restart local v2    # "i":I
    .restart local v3    # "licenseCategory":Ljava/lang/String;
    .restart local v5    # "rightDetails":Landroid/content/ContentValues;
    :pswitch_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b004c

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->typeStr:Ljava/lang/String;

    goto :goto_3

    .line 433
    :pswitch_2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b004f

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->typeStr:Ljava/lang/String;

    .line 434
    iget-object v7, v6, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->validityStr:[Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "%s/%s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "remaining_repeat_count"

    invoke-virtual {v5, v12}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const-string v12, "max_repeat_count"

    invoke-virtual {v5, v12}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    goto :goto_3

    .line 442
    :pswitch_3
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0050

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->typeStr:Ljava/lang/String;

    .line 443
    iget-object v7, v6, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->validityStr:[Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "%s~%s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "license_start_time"

    invoke-virtual {v5, v12}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const-string v12, "license_expiry_time"

    invoke-virtual {v5, v12}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    goto/16 :goto_3

    .line 448
    :pswitch_4
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b004e

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->typeStr:Ljava/lang/String;

    .line 450
    iget-object v7, v6, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->validityStr:[Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "%s/%s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "license_original_interval"

    invoke-virtual {v5, v12}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const-string v12, "license_available_time"

    invoke-virtual {v5, v12}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    goto/16 :goto_3

    .line 456
    :pswitch_5
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b004d

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->typeStr:Ljava/lang/String;

    .line 457
    iget-object v7, v6, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->validityStr:[Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "%s/%s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "license_original_interval"

    invoke-virtual {v5, v12}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const-string v12, "license_available_time"

    invoke-virtual {v5, v12}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    .line 461
    iget-object v7, v6, Lcom/sec/android/app/myfiles/drm/DrmUtils$DrmRightInfo;->validityStr:[Ljava/lang/String;

    const/4 v8, 0x1

    const-string v9, "%s/%s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "remaining_repeat_count"

    invoke-virtual {v5, v12}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const-string v12, "max_repeat_count"

    invoke-virtual {v5, v12}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    goto/16 :goto_3

    .line 382
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x6 -> :sswitch_2
        0x7 -> :sswitch_1
    .end sparse-switch

    .line 427
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public static isAvailableToSet(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 687
    invoke-static {p0, p1}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isOMADrmFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 688
    const-string v1, "bRingtone"

    invoke-static {p0, p1, v1}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isPossibleOptMenu(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 689
    const-string v1, "DrmUtils"

    const-string v2, "isAvailableToSet => this is not possible for bRingtone."

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 693
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isDRMFile(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 700
    invoke-static {p0, p1}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isOMADrmFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 705
    :cond_0
    :goto_0
    return v0

    .line 702
    :cond_1
    invoke-static {p0, p1}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isPlayReadyFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 705
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isForwardLockType(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 485
    new-instance v2, Landroid/drm/DrmInfoRequest;

    const/16 v6, 0xe

    const-string v7, "application/vnd.oma.drm.content"

    invoke-direct {v2, v6, v7}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    .line 487
    .local v2, "infoRequest":Landroid/drm/DrmInfoRequest;
    const-string v6, "drm_path"

    invoke-virtual {v2, v6, p1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 489
    invoke-static {p0}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->getDrmManager(Landroid/content/Context;)Landroid/drm/DrmManagerClient;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v0

    .line 491
    .local v0, "drmInfo":Landroid/drm/DrmInfo;
    if-eqz v0, :cond_2

    .line 492
    const-string v6, "status"

    invoke-virtual {v0, v6}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 494
    .local v4, "status":Ljava/lang/String;
    const-string v6, "success"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 495
    const-string v6, "DrmUtils"

    const-string v7, "isForwardLockType: acquireDrmInfo Success"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 505
    const-string v6, "type"

    invoke-virtual {v0, v6}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 507
    .local v3, "objType":Ljava/lang/Object;
    if-nez v3, :cond_3

    .line 508
    const-string v6, "DrmUtils"

    const-string v7, "isForwardLockType: objType is null"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 521
    .end local v3    # "objType":Ljava/lang/Object;
    .end local v4    # "status":Ljava/lang/String;
    :cond_0
    :goto_0
    return v5

    .line 497
    .restart local v4    # "status":Ljava/lang/String;
    :cond_1
    const-string v6, "DrmUtils"

    const-string v7, "isForwardLockType: acquireDrmInfo Fail"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 501
    .end local v4    # "status":Ljava/lang/String;
    :cond_2
    const-string v6, "DrmUtils"

    const-string v7, "isForwardLockType: acquireDrmInfo Fail"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 512
    .restart local v3    # "objType":Ljava/lang/Object;
    .restart local v4    # "status":Ljava/lang/String;
    :cond_3
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 514
    .local v1, "drmType":I
    const-string v6, "DrmUtils"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "drmType = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 516
    if-nez v1, :cond_0

    .line 517
    const-string v6, "DrmUtils"

    const-string v7, "isForwardLockType: this is FL"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 518
    const/4 v5, 0x1

    goto :goto_0
.end method

.method public static isForwardable(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 558
    if-nez p1, :cond_0

    .line 559
    const-string v1, "DrmUtils"

    const-string v2, "isForwardable : filePath is null."

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 572
    :goto_0
    return v0

    .line 563
    :cond_0
    invoke-static {p0, p1}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isPlayReadyFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 564
    const-string v1, "DrmUtils"

    const-string v2, "isForwardable => this is not forwardable."

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 566
    :cond_1
    invoke-static {p0, p1}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isOMADrmFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 567
    const-string v1, "bSendAs"

    invoke-static {p0, p1, v1}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isPossibleOptMenu(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 568
    const-string v1, "DrmUtils"

    const-string v2, "isForwardable => this is not forwardable."

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 572
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isOMADrmFile(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 188
    if-nez p1, :cond_1

    .line 189
    const-string v3, "DrmUtils"

    const-string v4, "isOMADrmFile : filePath is null."

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 206
    :cond_0
    :goto_0
    return v2

    .line 193
    :cond_1
    invoke-static {p1}, Lcom/sec/android/app/myfiles/MediaFile;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 195
    .local v1, "mimeType":Ljava/lang/String;
    const-string v3, "application/vnd.oma.drm.content"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 197
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->getDrmManager(Landroid/content/Context;)Landroid/drm/DrmManagerClient;

    move-result-object v3

    invoke-virtual {v3, p1, v1}, Landroid/drm/DrmManagerClient;->canHandle(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 198
    const/4 v3, 0x0

    const-string v4, "DrmUtils"

    const-string v5, "isOMADrmFile : this file is DRM. => "

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 199
    const/4 v2, 0x1

    goto :goto_0

    .line 201
    :catch_0
    move-exception v0

    .line 202
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public static isPlayReadyFile(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 108
    if-nez p1, :cond_1

    .line 110
    const-string v3, "DrmUtils"

    const-string v4, "isPlayReadyFile : filePath is null."

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 150
    :cond_0
    :goto_0
    return v2

    .line 119
    :cond_1
    invoke-static {p1}, Lcom/sec/android/app/myfiles/MediaFile;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 121
    .local v1, "mimeType":Ljava/lang/String;
    const-string v3, "audio/vnd.ms-playready.media.pya"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "video/vnd.ms-playready.media.pyv"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "audio/x-ms-wma"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "video/x-ms-wmv"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 126
    :cond_2
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->getDrmManager(Landroid/content/Context;)Landroid/drm/DrmManagerClient;

    move-result-object v3

    invoke-virtual {v3, p1, v1}, Landroid/drm/DrmManagerClient;->canHandle(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 127
    const/4 v3, 0x2

    const-string v4, "DrmUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isPlayReadyFile : this file is PlayReady. => "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    const/4 v2, 0x1

    goto :goto_0

    .line 130
    :catch_0
    move-exception v0

    .line 131
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method private static isPossibleOptMenu(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "option"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 577
    if-nez p1, :cond_0

    .line 578
    const-string v9, "DrmUtils"

    const-string v10, "isPossibleOptMenu : filePath is null."

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    move v3, v8

    .line 645
    :goto_0
    return v3

    .line 582
    :cond_0
    new-instance v5, Landroid/drm/DrmInfoRequest;

    const/16 v9, 0x10

    const-string v10, "application/vnd.oma.drm.content"

    invoke-direct {v5, v9, v10}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    .line 584
    .local v5, "optMenu":Landroid/drm/DrmInfoRequest;
    const-string v9, "drm_path"

    invoke-virtual {v5, v9, p1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 586
    invoke-static {p0}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->getDrmManager(Landroid/content/Context;)Landroid/drm/DrmManagerClient;

    move-result-object v9

    invoke-virtual {v9, v5}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v4

    .line 588
    .local v4, "drmInfo":Landroid/drm/DrmInfo;
    if-nez v4, :cond_1

    .line 589
    const-string v9, "DrmUtils"

    const-string v10, "isPossibleOptMenu : drmInfo == null return false"

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    move v3, v8

    .line 590
    goto :goto_0

    .line 593
    :cond_1
    const-string v9, "status"

    invoke-virtual {v4, v9}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 595
    .local v6, "status":Ljava/lang/String;
    if-nez v6, :cond_2

    .line 596
    const-string v9, "DrmUtils"

    const-string v10, "isPossibleOptMenu : status == null return false"

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    move v3, v8

    .line 597
    goto :goto_0

    .line 600
    :cond_2
    const-string v0, "1"

    .line 602
    .local v0, "TRUE":Ljava/lang/String;
    const-string v9, "success"

    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 603
    const-string v9, "DrmUtils"

    const-string v10, "isPossibleOptMenu : acquireDrmInfo Success"

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 608
    :goto_1
    const-string v9, "bSendAs"

    invoke-virtual {p2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 609
    const/4 v3, 0x0

    .line 610
    .local v3, "bSendAs":Z
    const-string v9, "status"

    invoke-virtual {v4, v9}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    .line 611
    .local v7, "status_req":Ljava/lang/String;
    const-string v9, "fail"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 612
    const/4 v3, 0x0

    .line 617
    :goto_2
    const-string v9, "DrmUtils"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "isPossibleOptMenu : bSendAs = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 605
    .end local v3    # "bSendAs":Z
    .end local v7    # "status_req":Ljava/lang/String;
    :cond_3
    const-string v9, "DrmUtils"

    const-string v10, "isPossibleOptMenu : acquireDrmInfo Fail"

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 614
    .restart local v3    # "bSendAs":Z
    .restart local v7    # "status_req":Ljava/lang/String;
    :cond_4
    const-string v9, "bSendAs"

    invoke-virtual {v4, v9}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "1"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    goto :goto_2

    .line 619
    .end local v3    # "bSendAs":Z
    .end local v7    # "status_req":Ljava/lang/String;
    :cond_5
    const-string v9, "bPrint"

    invoke-virtual {p2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 620
    const/4 v1, 0x0

    .line 622
    .local v1, "bPrint":Z
    const-string v9, "status"

    invoke-virtual {v4, v9}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    .line 623
    .restart local v7    # "status_req":Ljava/lang/String;
    const-string v9, "fail"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 624
    const/4 v1, 0x0

    .line 629
    :goto_3
    const-string v9, "DrmUtils"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "isPossibleOptMenu : bPrint = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    move v3, v1

    .line 630
    goto/16 :goto_0

    .line 626
    :cond_6
    const-string v9, "bPrint"

    invoke-virtual {v4, v9}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "1"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_3

    .line 631
    .end local v1    # "bPrint":Z
    .end local v7    # "status_req":Ljava/lang/String;
    :cond_7
    const-string v9, "bRingtone"

    invoke-virtual {p2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 632
    const/4 v2, 0x0

    .line 634
    .local v2, "bRingtone":Z
    const-string v9, "status"

    invoke-virtual {v4, v9}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    .line 635
    .restart local v7    # "status_req":Ljava/lang/String;
    const-string v9, "fail"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 636
    const/4 v2, 0x0

    .line 641
    :goto_4
    const-string v9, "DrmUtils"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "isPossibleOptMenu : bRingtone = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 642
    goto/16 :goto_0

    .line 638
    :cond_8
    const-string v9, "bRingtone"

    invoke-virtual {v4, v9}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "1"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_4

    .end local v2    # "bRingtone":Z
    .end local v7    # "status_req":Ljava/lang/String;
    :cond_9
    move v3, v8

    .line 645
    goto/16 :goto_0
.end method

.method public static isValidDrmFile(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 330
    if-nez p1, :cond_0

    .line 331
    const-string v2, "DrmUtils"

    const-string v3, "isValidDrmFile : filePath is null."

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 348
    :goto_0
    return v1

    .line 335
    :cond_0
    invoke-static {p0, p1}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isPlayReadyFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {p0, p1}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isOMADrmFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 336
    const-string v3, "DrmUtils"

    const-string v4, "isValidDrmFile => this file is not DRM. - "

    invoke-static {v1, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    move v1, v2

    .line 337
    goto :goto_0

    .line 340
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->getDrmManager(Landroid/content/Context;)Landroid/drm/DrmManagerClient;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/drm/DrmManagerClient;->checkRightsStatus(Ljava/lang/String;)I

    move-result v0

    .line 342
    .local v0, "rightStatus":I
    if-nez v0, :cond_2

    .line 343
    const-string v3, "DrmUtils"

    const-string v4, "isValidDrmFile => this file is valid."

    invoke-static {v1, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    move v1, v2

    .line 344
    goto :goto_0

    .line 346
    :cond_2
    const-string v2, "DrmUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isValidDrmFile => this file is not valid. rightStatus = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static isValidPlayReadyFile(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 164
    if-nez p1, :cond_0

    .line 165
    const-string v2, "DrmUtils"

    const-string v3, "isValidPlayReadyFile : filePath is null."

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 182
    :goto_0
    return v1

    .line 169
    :cond_0
    invoke-static {p0, p1}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isPlayReadyFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {p0, p1}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isOMADrmFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 170
    const/4 v1, 0x2

    const-string v3, "DrmUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isValidPlayReadyFile => this file is not DRM. - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    move v1, v2

    .line 171
    goto :goto_0

    .line 174
    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->getDrmManager(Landroid/content/Context;)Landroid/drm/DrmManagerClient;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/drm/DrmManagerClient;->checkRightsStatus(Ljava/lang/String;)I

    move-result v0

    .line 176
    .local v0, "rightStatus":I
    if-nez v0, :cond_2

    .line 177
    const-string v3, "DrmUtils"

    const-string v4, "isValidDrmFile => this file is valid. "

    invoke-static {v1, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    move v1, v2

    .line 178
    goto :goto_0

    .line 180
    :cond_2
    const-string v2, "DrmUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DrmManager.isValidDrmFile => this file is not valid. rightStatus = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
