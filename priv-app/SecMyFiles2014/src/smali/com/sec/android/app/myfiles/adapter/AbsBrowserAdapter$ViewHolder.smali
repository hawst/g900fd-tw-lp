.class public Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;
.super Ljava/lang/Object;
.source "AbsBrowserAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ViewHolder"
.end annotation


# instance fields
.field public mData:Landroid/os/Bundle;

.field public mDate:Landroid/widget/TextView;

.field public mDescriptionTextView:Landroid/widget/TextView;

.field public mDescriptionViewGroup:Landroid/view/ViewGroup;

.field public mDetailContainer:Landroid/view/ViewGroup;

.field public mDivider:Landroid/widget/ImageView;

.field public mDocExtnOverlay:Landroid/widget/ImageView;

.field public mHighlight:Landroid/widget/TextView;

.field public mIcon:Landroid/widget/ImageView;

.field public mIconContainer:Landroid/widget/RelativeLayout;

.field public mOverlay:Landroid/widget/ImageView;

.field public mPersonalContentsIcon:Landroid/widget/ImageView;

.field public mProgressBar:Lcom/sec/android/app/myfiles/view/ListPlayerProgressView;

.field public mSelectCheckBox:Landroid/widget/CheckBox;

.field public mSelectRadioButton:Landroid/widget/RadioButton;

.field public mSizeOrItems:Landroid/widget/TextView;

.field public mThumbnail:Landroid/widget/ImageView;

.field public mThumbnailWorker:Landroid/os/AsyncTask;

.field public mTitle:Landroid/widget/TextView;

.field public mTitleSearchHeader:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1411
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
