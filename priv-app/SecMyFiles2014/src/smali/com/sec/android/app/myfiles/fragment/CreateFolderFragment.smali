.class public Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;
.source "CreateFolderFragment.java"


# static fields
.field private static final MESSAGE_CREATEFOLDER_SUCCESS:I = 0x0

.field private static final MODULE:Ljava/lang/String; = "CreateFolderFragment"


# instance fields
.field private isAlreadyEnterName:Z

.field private mHandler:Landroid/os/Handler;

.field private mToast:Landroid/widget/Toast;

.field private mWaitingDialog:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;-><init>()V

    .line 57
    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->mWaitingDialog:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    .line 59
    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->mToast:Landroid/widget/Toast;

    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->isAlreadyEnterName:Z

    .line 62
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->clickOK()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->mToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->mToast:Landroid/widget/Toast;

    return-object p1
.end method

.method private clickOK()V
    .locals 14

    .prologue
    .line 189
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v10

    const-string v11, "current_folder"

    invoke-virtual {v10, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 191
    .local v0, "currentPath":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->mInputEditText:Landroid/widget/EditText;

    invoke-virtual {v10}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 193
    .local v6, "newFolderName":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v11, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v5, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 195
    .local v5, "newFolder":Ljava/io/File;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v10

    const-string v11, "src_fragment_id"

    invoke-virtual {v10, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    .line 198
    .local v9, "targetFragmentID":I
    const/16 v10, 0x201

    if-eq v9, v10, :cond_0

    const/16 v10, 0x8

    if-eq v9, v10, :cond_0

    const/16 v10, 0x1f

    if-eq v9, v10, :cond_0

    const/16 v10, 0x24

    if-ne v9, v10, :cond_5

    .line 201
    :cond_0
    invoke-direct {p0, v9, v5}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->verifyNewFolder(ILjava/io/File;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 203
    sparse-switch v9, :sswitch_data_0

    .line 362
    const v10, 0x7f0b009e

    invoke-direct {p0, v10}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->showToast(I)V

    .line 403
    :cond_1
    :goto_0
    const/4 v10, 0x0

    const-string v11, "CreateFolderFragment"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "dismiss() targetFragmentID="

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 404
    const/16 v10, 0x1f

    if-eq v9, v10, :cond_2

    iget-boolean v10, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->isAlreadyEnterName:Z

    if-eqz v10, :cond_2

    .line 405
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->dismissAllowingStateLoss()V

    .line 407
    :cond_2
    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->isAlreadyEnterName:Z

    .line 408
    return-void

    .line 207
    :sswitch_0
    invoke-virtual {v5}, Ljava/io/File;->mkdir()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 209
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/myfiles/utils/Utils;->sendScanFile(Landroid/content/Context;Ljava/lang/String;)V

    .line 213
    :try_start_0
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->insertInDatabase(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    :goto_1
    iget v10, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->cur_operation:I

    const/4 v11, 0x4

    if-ne v10, v11, :cond_3

    .line 226
    :cond_3
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 228
    .local v1, "data":Landroid/content/Intent;
    const-string v10, "result_value"

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 230
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v10

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->getTargetRequestCode()I

    move-result v11

    const/4 v12, -0x1

    invoke-virtual {v10, v11, v12, v1}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    .line 215
    .end local v1    # "data":Landroid/content/Intent;
    :catch_0
    move-exception v2

    .line 217
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 235
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_4
    const v10, 0x7f0b009e

    invoke-direct {p0, v10}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->showToast(I)V

    goto :goto_0

    .line 243
    :sswitch_1
    new-instance v8, Ljava/lang/Thread;

    new-instance v10, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$4;

    invoke-direct {v10, p0, v0, v6}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$4;-><init>(Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v8, v10}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 296
    .local v8, "t":Ljava/lang/Thread;
    invoke-virtual {v8}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 304
    .end local v8    # "t":Ljava/lang/Thread;
    :sswitch_2
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 306
    new-instance v8, Ljava/lang/Thread;

    new-instance v10, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$5;

    invoke-direct {v10, p0, v0, v6}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$5;-><init>(Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v8, v10}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 354
    .restart local v8    # "t":Ljava/lang/Thread;
    invoke-virtual {v8}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    .line 371
    .end local v8    # "t":Ljava/lang/Thread;
    :cond_5
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->mInputEditText:Landroid/widget/EditText;

    invoke-virtual {v10}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 373
    .local v3, "input":Ljava/lang/String;
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_1

    .line 375
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 377
    .local v4, "intent":Landroid/content/Intent;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 379
    .local v7, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 381
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v10

    if-eqz v10, :cond_6

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v10

    const/4 v11, 0x1

    if-le v10, v11, :cond_7

    .line 383
    :cond_6
    const-string v10, "/"

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 386
    :cond_7
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 388
    const-string v10, "result_value"

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v4, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 390
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    if-eqz v10, :cond_8

    .line 392
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v10}, Landroid/view/inputmethod/InputMethodManager;->dismissAndShowAgainInputMethodPicker()V

    .line 396
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->dismiss()V

    .line 398
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v10

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->getTargetRequestCode()I

    move-result v11

    const/4 v12, -0x1

    invoke-virtual {v10, v11, v12, v4}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    goto/16 :goto_0

    .line 203
    nop

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_1
        0x1f -> :sswitch_2
        0x24 -> :sswitch_0
        0x201 -> :sswitch_0
    .end sparse-switch
.end method

.method private insertInDatabase(Ljava/lang/String;)Z
    .locals 6
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 490
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 492
    .local v0, "contentValues":Landroid/content/ContentValues;
    const-string v3, "external"

    invoke-static {v3}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 494
    .local v1, "uri":Landroid/net/Uri;
    const-string v3, "_data"

    invoke-virtual {v0, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 496
    const-string v3, "format"

    const-wide/16 v4, 0x3001

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 498
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, v1, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 500
    const/4 v2, 0x1

    .line 505
    :goto_0
    return v2

    .line 503
    :cond_0
    const-string v3, "CreateFolderFragment"

    const-string v4, "insertInDatabase() return false"

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private showToast(I)V
    .locals 2
    .param p1, "resId"    # I

    .prologue
    .line 511
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 513
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$7;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$7;-><init>(Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;I)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 541
    :cond_0
    return-void
.end method

.method private verifyNewFolder(ILjava/io/File;)Z
    .locals 6
    .param p1, "srcFragment"    # I
    .param p2, "folder"    # Ljava/io/File;

    .prologue
    const/4 v3, 0x0

    .line 114
    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 116
    .local v1, "folderName":Ljava/lang/String;
    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 118
    .local v2, "folderPath":Ljava/lang/String;
    const/16 v4, 0x8

    if-eq p1, v4, :cond_0

    const/16 v4, 0x1f

    if-ne p1, v4, :cond_2

    .line 135
    :cond_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 137
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-gtz v4, :cond_3

    .line 139
    const v4, 0x7f0b009c

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->showToast(I)V

    .line 183
    :cond_1
    :goto_0
    return v3

    .line 127
    :cond_2
    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->getStorage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 128
    .local v0, "checkStorageExist":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 130
    const v4, 0x7f0b009e

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->showToast(I)V

    goto :goto_0

    .line 143
    .end local v0    # "checkStorageExist":Ljava/lang/String;
    :cond_3
    const-string v4, "."

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, ".."

    invoke-virtual {v4, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 145
    :cond_4
    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->isAlreadyEnterName:Z

    .line 146
    const v4, 0x7f0b009d

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->showToast(I)V

    goto :goto_0

    .line 150
    :cond_5
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 152
    const v4, 0x7f0b012a

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->showToast(I)V

    goto :goto_0

    .line 156
    :cond_6
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0xff

    if-le v4, v5, :cond_7

    .line 158
    const v4, 0x7f0b0077

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->showToast(I)V

    goto :goto_0

    .line 164
    :cond_7
    packed-switch p1, :pswitch_data_0

    .line 183
    :cond_8
    const/4 v3, 0x1

    goto :goto_0

    .line 168
    :pswitch_0
    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->hasAvailableSpace(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 170
    const v4, 0x7f0b0067

    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->showToast(I)V

    goto :goto_0

    .line 164
    :pswitch_data_0
    .packed-switch 0x201
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public onResume()V
    .locals 2

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 105
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->mOkButton:Landroid/widget/Button;

    .line 106
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->mCancelButton:Landroid/widget/Button;

    .line 108
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->onResume()V

    .line 109
    return-void
.end method

.method protected setupDialog(Landroid/app/AlertDialog$Builder;)V
    .locals 2
    .param p1, "builder"    # Landroid/app/AlertDialog$Builder;

    .prologue
    .line 83
    const v0, 0x7f0b016a

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$2;-><init>(Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 92
    const v0, 0x7f0b0017

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$3;-><init>(Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 99
    return-void
.end method

.method protected setupInputEditText(Landroid/widget/EditText;)V
    .locals 5
    .param p1, "editText"    # Landroid/widget/EditText;

    .prologue
    const/4 v4, 0x1

    .line 416
    if-nez p1, :cond_0

    .line 485
    :goto_0
    return-void

    .line 421
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "src_fragment_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "src_fragment_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const/16 v2, 0x1f

    if-ne v1, v2, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->mCur_folder:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 425
    const-string v1, "/"

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->mCur_folder:Ljava/lang/String;

    .line 428
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "file_name"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 430
    .local v0, "defaultName":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 432
    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 434
    invoke-virtual {p1}, Landroid/widget/EditText;->selectAll()V

    .line 436
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->mOkButton:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 439
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->mCur_folder:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Lcom/sec/android/app/myfiles/utils/Utils;->getEditTextFilter(Landroid/content/Context;I)[Landroid/text/InputFilter;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 441
    invoke-virtual {p1, v4}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 443
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 445
    const-string v1, "inputType=PredictionOff;inputType=filename;disableEmoticonInput=true"

    invoke-virtual {p1, v1}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 451
    const v1, 0x84001

    invoke-virtual {p1, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 454
    new-instance v1, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$6;-><init>(Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;)V

    invoke-virtual {p1, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    goto :goto_0
.end method
