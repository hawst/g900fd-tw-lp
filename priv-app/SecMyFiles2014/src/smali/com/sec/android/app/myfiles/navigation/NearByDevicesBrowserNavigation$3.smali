.class Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$3;
.super Ljava/lang/Object;
.source "NearByDevicesBrowserNavigation.java"

# interfaces
.implements Lcom/samsung/android/allshare/media/Provider$IProviderBrowseResponseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;)V
    .locals 0

    .prologue
    .line 518
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBrowseResponseReceived(Ljava/util/ArrayList;IILcom/samsung/android/allshare/Item;ZLcom/samsung/android/allshare/ERROR;)V
    .locals 6
    .param p2, "requestedStartIndex"    # I
    .param p3, "requestedCout"    # I
    .param p4, "requestedFolderItem"    # Lcom/samsung/android/allshare/Item;
    .param p5, "endOfItems"    # Z
    .param p6, "err"    # Lcom/samsung/android/allshare/ERROR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Item;",
            ">;II",
            "Lcom/samsung/android/allshare/Item;",
            "Z",
            "Lcom/samsung/android/allshare/ERROR;",
            ")V"
        }
    .end annotation

    .prologue
    .line 524
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    if-eqz p2, :cond_0

    if-eqz p5, :cond_0

    .line 525
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mContext:Landroid/content/Context;

    const v4, 0x7f0b008a

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 529
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    if-eqz v3, :cond_4

    .line 530
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 532
    if-nez p2, :cond_1

    .line 533
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentItems:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 534
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrItemsIdProp:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 535
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->clearNearByData()J

    .line 538
    :cond_1
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_3

    if-eqz p4, :cond_3

    .line 539
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->getCurrentItem()Lcom/samsung/android/allshare/Item;

    move-result-object v3

    invoke-virtual {p4, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 540
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/Item;

    .line 541
    .local v2, "item":Lcom/samsung/android/allshare/Item;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrentItems:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 542
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/media/Provider;->getID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Lcom/sec/android/app/myfiles/db/CacheDB;->insertNearByData(Ljava/lang/String;Lcom/samsung/android/allshare/Item;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 543
    .local v1, "id":Ljava/lang/Long;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mCurrItemsIdProp:Ljava/util/Map;

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 545
    .end local v1    # "id":Ljava/lang/Long;
    .end local v2    # "item":Lcom/samsung/android/allshare/Item;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 546
    if-lez p2, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v3, :cond_3

    .line 547
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/app/myfiles/AbsMainActivity;

    new-instance v4, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$3$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$3$1;-><init>(Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$3;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/AbsMainActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 559
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    if-nez p2, :cond_4

    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mAsyncUi:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    if-eqz v3, :cond_4

    .line 560
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mAsyncUi:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->setCompletedCondition(Z)V

    .line 564
    :cond_4
    return-void

    .line 555
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation$3;->this$0:Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    goto :goto_1
.end method
