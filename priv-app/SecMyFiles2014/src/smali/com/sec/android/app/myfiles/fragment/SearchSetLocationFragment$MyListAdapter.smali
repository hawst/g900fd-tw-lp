.class Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;
.super Landroid/widget/BaseAdapter;
.source "SearchSetLocationFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyListAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;)V
    .locals 0

    .prologue
    .line 614
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 616
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 620
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 625
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 630
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "arg2"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v9, 0x1

    const v8, 0x3ecccccd    # 0.4f

    .line 636
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    if-nez v6, :cond_2

    .line 637
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mActivity:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    .line 641
    .local v4, "inflater":Landroid/view/LayoutInflater;
    :goto_0
    if-nez p2, :cond_0

    .line 642
    const v6, 0x7f040041

    const/4 v7, 0x0

    invoke-virtual {v4, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 644
    :cond_0
    const v6, 0x7f0f00fa

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 645
    .local v1, "container":Landroid/widget/RelativeLayout;
    const v6, 0x7f0f00fc

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 646
    .local v0, "checkBox":Landroid/widget/CheckBox;
    const v6, 0x7f0f00fb

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 647
    .local v5, "text":Landroid/widget/TextView;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->getText()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 648
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->isChecked()Z

    move-result v6

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 649
    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 650
    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {p2, v6}, Landroid/view/View;->setAlpha(F)V

    .line 652
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mNearbyPosition:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_1

    .line 653
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    iget-boolean v6, v6, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mIsNearbyCheck:Z

    if-eqz v6, :cond_3

    .line 662
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mData:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$ListItem;->isChecked()Z

    move-result v6

    if-nez v6, :cond_1

    .line 663
    invoke-virtual {v0, v9}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 664
    invoke-virtual {p2, v8}, Landroid/view/View;->setAlpha(F)V

    .line 676
    :cond_1
    return-object p2

    .line 639
    .end local v0    # "checkBox":Landroid/widget/CheckBox;
    .end local v1    # "container":Landroid/widget/RelativeLayout;
    .end local v4    # "inflater":Landroid/view/LayoutInflater;
    .end local v5    # "text":Landroid/widget/TextView;
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    .restart local v4    # "inflater":Landroid/view/LayoutInflater;
    goto/16 :goto_0

    .line 666
    .restart local v0    # "checkBox":Landroid/widget/CheckBox;
    .restart local v1    # "container":Landroid/widget/RelativeLayout;
    .restart local v5    # "text":Landroid/widget/TextView;
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    iget-boolean v6, v6, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mIsOtherCheck:Z

    if-eqz v6, :cond_1

    .line 667
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment$MyListAdapter;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->mNearbyPosition:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/SearchSetLocationFragment;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 668
    .local v2, "i":I
    if-ne p1, v2, :cond_4

    .line 669
    invoke-virtual {v0, v9}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 670
    invoke-virtual {p2, v8}, Landroid/view/View;->setAlpha(F)V

    goto :goto_1
.end method
