.class Lcom/sec/android/app/myfiles/MainActivity$10;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/MainActivity;->updateEmptySearchView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/MainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/MainActivity;)V
    .locals 0

    .prologue
    .line 2932
    iput-object p1, p0, Lcom/sec/android/app/myfiles/MainActivity$10;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 2935
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$10;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mEmptyTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/MainActivity;->access$1400(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity$10;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/MainActivity;->access$300(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b0095

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2936
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$10;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mEmptyTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/MainActivity;->access$1400(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/widget/TextView;

    move-result-object v0

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2937
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$10;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mEmptyTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/MainActivity;->access$1400(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2938
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$10;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mEmptyTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/MainActivity;->access$1400(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    .line 2939
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$10;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mEmptyTextView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/MainActivity;->access$1400(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x40

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    .line 2940
    return-void
.end method
