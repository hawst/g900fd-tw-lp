.class public Lcom/sec/android/app/myfiles/compression/ZipCompressor;
.super Lcom/sec/android/app/myfiles/compression/AbsCompressor;
.source "ZipCompressor.java"


# static fields
.field private static final BUFFER_SIZE:I = 0x400

.field private static final MODULE:Ljava/lang/String; = "ZipCompressor"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/compression/AbsCompressor;-><init>()V

    return-void
.end method

.method private addFileToZip(Ljava/lang/String;Ljava/io/File;Ljava/util/zip/ZipOutputStream;)V
    .locals 8
    .param p1, "zipEntryName"    # Ljava/lang/String;
    .param p2, "srcFile"    # Ljava/io/File;
    .param p3, "zos"    # Ljava/util/zip/ZipOutputStream;

    .prologue
    .line 257
    const/4 v2, 0x0

    .line 259
    .local v2, "fileInputStream":Ljava/io/FileInputStream;
    const/16 v6, 0x400

    new-array v0, v6, [B

    .line 265
    .local v0, "buf":[B
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 267
    .end local v2    # "fileInputStream":Ljava/io/FileInputStream;
    .local v3, "fileInputStream":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v5, Ljava/util/zip/ZipEntry;

    invoke-direct {v5, p1}, Ljava/util/zip/ZipEntry;-><init>(Ljava/lang/String;)V

    .line 269
    .local v5, "zipEntry":Ljava/util/zip/ZipEntry;
    invoke-virtual {p2}, Ljava/io/File;->lastModified()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/util/zip/ZipEntry;->setTime(J)V

    .line 271
    invoke-virtual {p3, v5}, Ljava/util/zip/ZipOutputStream;->putNextEntry(Ljava/util/zip/ZipEntry;)V

    .line 273
    :goto_0
    invoke-virtual {v3, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v4

    .local v4, "length":I
    if-lez v4, :cond_1

    .line 275
    const/4 v6, 0x0

    invoke-virtual {p3, v0, v6, v4}, Ljava/util/zip/ZipOutputStream;->write([BII)V

    .line 276
    int-to-long v6, v4

    invoke-virtual {p0, v6, v7}, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->setProgress(J)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 283
    .end local v4    # "length":I
    .end local v5    # "zipEntry":Ljava/util/zip/ZipEntry;
    :catch_0
    move-exception v1

    move-object v2, v3

    .line 285
    .end local v3    # "fileInputStream":Ljava/io/FileInputStream;
    .local v1, "e":Ljava/io/FileNotFoundException;
    .restart local v2    # "fileInputStream":Ljava/io/FileInputStream;
    :goto_1
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 295
    if-eqz v2, :cond_0

    .line 296
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 306
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :cond_0
    :goto_2
    return-void

    .line 279
    .end local v2    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v3    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v4    # "length":I
    .restart local v5    # "zipEntry":Ljava/util/zip/ZipEntry;
    :cond_1
    :try_start_4
    iget v6, p0, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->mCurrentCounter:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->mCurrentCounter:I

    .line 281
    invoke-virtual {p3}, Ljava/util/zip/ZipOutputStream;->closeEntry()V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 295
    if-eqz v3, :cond_2

    .line 296
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    :cond_2
    move-object v2, v3

    .line 302
    .end local v3    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v2    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_2

    .line 299
    .end local v2    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v3    # "fileInputStream":Ljava/io/FileInputStream;
    :catch_1
    move-exception v1

    .line 301
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    move-object v2, v3

    .line 304
    .end local v3    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v2    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_2

    .line 299
    .end local v4    # "length":I
    .end local v5    # "zipEntry":Ljava/util/zip/ZipEntry;
    .local v1, "e":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v1

    .line 301
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 287
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 289
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_3
    :try_start_6
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 295
    if-eqz v2, :cond_0

    .line 296
    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_2

    .line 299
    :catch_4
    move-exception v1

    .line 301
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 293
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 295
    :goto_4
    if-eqz v2, :cond_3

    .line 296
    :try_start_8
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 302
    :cond_3
    :goto_5
    throw v6

    .line 299
    :catch_5
    move-exception v1

    .line 301
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 293
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v3    # "fileInputStream":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v6

    move-object v2, v3

    .end local v3    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v2    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_4

    .line 287
    .end local v2    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v3    # "fileInputStream":Ljava/io/FileInputStream;
    :catch_6
    move-exception v1

    move-object v2, v3

    .end local v3    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v2    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_3

    .line 283
    :catch_7
    move-exception v1

    goto :goto_1
.end method

.method private isUTF8Encoded(Lnet/lingala/zip4j/core/ZipFile;)Z
    .locals 5
    .param p1, "zipFile"    # Lnet/lingala/zip4j/core/ZipFile;

    .prologue
    .line 638
    const/4 v0, 0x0

    .line 639
    .local v0, "bRet":Z
    if-eqz p1, :cond_0

    .line 641
    :try_start_0
    invoke-virtual {p1}, Lnet/lingala/zip4j/core/ZipFile;->getFileHeaders()Ljava/util/List;

    move-result-object v3

    .line 643
    .local v3, "headersList":Ljava/util/List;
    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 644
    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/lingala/zip4j/model/FileHeader;

    .line 645
    .local v2, "fileHeader":Lnet/lingala/zip4j/model/FileHeader;
    invoke-virtual {v2}, Lnet/lingala/zip4j/model/FileHeader;->isFileNameUTF8Encoded()Z
    :try_end_0
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 651
    .end local v2    # "fileHeader":Lnet/lingala/zip4j/model/FileHeader;
    .end local v3    # "headersList":Ljava/util/List;
    :cond_0
    :goto_0
    return v0

    .line 647
    :catch_0
    move-exception v1

    .line 648
    .local v1, "e":Lnet/lingala/zip4j/exception/ZipException;
    invoke-virtual {v1}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    goto :goto_0
.end method

.method private setCharset(Landroid/content/Context;Lnet/lingala/zip4j/core/ZipFile;)Lnet/lingala/zip4j/core/ZipFile;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "zipFile"    # Lnet/lingala/zip4j/core/ZipFile;

    .prologue
    .line 655
    move-object v3, p2

    .line 656
    .local v3, "ret":Lnet/lingala/zip4j/core/ZipFile;
    if-eqz p2, :cond_2

    .line 657
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget-object v5, v5, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    .line 660
    .local v1, "curLocale":Ljava/lang/String;
    const/4 v0, 0x0

    .line 662
    .local v0, "charset":Ljava/lang/String;
    :try_start_0
    sget-object v5, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    sget-object v5, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 664
    :cond_0
    const-string v0, "Shift_JIS"

    .line 671
    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    .line 672
    new-instance v4, Lnet/lingala/zip4j/core/ZipFile;

    invoke-virtual {p2}, Lnet/lingala/zip4j/core/ZipFile;->getFile()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_0 .. :try_end_0} :catch_0

    .line 673
    .end local v3    # "ret":Lnet/lingala/zip4j/core/ZipFile;
    .local v4, "ret":Lnet/lingala/zip4j/core/ZipFile;
    :try_start_1
    invoke-virtual {v4, v0}, Lnet/lingala/zip4j/core/ZipFile;->setFileNameCharset(Ljava/lang/String;)V
    :try_end_1
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v3, v4

    .line 680
    .end local v0    # "charset":Ljava/lang/String;
    .end local v1    # "curLocale":Ljava/lang/String;
    .end local v4    # "ret":Lnet/lingala/zip4j/core/ZipFile;
    .restart local v3    # "ret":Lnet/lingala/zip4j/core/ZipFile;
    :cond_2
    :goto_1
    return-object v3

    .line 665
    .restart local v0    # "charset":Ljava/lang/String;
    .restart local v1    # "curLocale":Ljava/lang/String;
    :cond_3
    :try_start_2
    sget-object v5, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    sget-object v5, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 667
    :cond_4
    const-string v0, "EUC-KR"
    :try_end_2
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 675
    :catch_0
    move-exception v2

    .line 676
    .local v2, "e":Lnet/lingala/zip4j/exception/ZipException;
    :goto_2
    invoke-virtual {v2}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    goto :goto_1

    .line 675
    .end local v2    # "e":Lnet/lingala/zip4j/exception/ZipException;
    .end local v3    # "ret":Lnet/lingala/zip4j/core/ZipFile;
    .restart local v4    # "ret":Lnet/lingala/zip4j/core/ZipFile;
    :catch_1
    move-exception v2

    move-object v3, v4

    .end local v4    # "ret":Lnet/lingala/zip4j/core/ZipFile;
    .restart local v3    # "ret":Lnet/lingala/zip4j/core/ZipFile;
    goto :goto_2
.end method


# virtual methods
.method public doCompress(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 24
    .param p1, "curFolder"    # Ljava/lang/String;
    .param p3, "dst"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 57
    .local p2, "srcList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v22, "ZipCompressor"

    const-string v23, "Compressing\'s started"

    invoke-static/range {v22 .. v23}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    new-instance v6, Ljava/io/File;

    move-object/from16 v0, p3

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 61
    .local v6, "dstFile":Ljava/io/File;
    const/4 v8, 0x0

    .line 63
    .local v8, "fileOutputStream":Ljava/io/FileOutputStream;
    const/16 v20, 0x0

    .line 65
    .local v20, "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    const/16 v17, 0x0

    .line 67
    .local v17, "zipEntry":Ljava/util/zip/ZipEntry;
    const/4 v14, 0x0

    .line 69
    .local v14, "srcFile":Ljava/io/File;
    const/16 v19, 0x0

    .line 71
    .local v19, "zipEntryName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->mProgressBar:Landroid/widget/ProgressBar;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/widget/ProgressBar;->getContext()Landroid/content/Context;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getPathFilesSize(Landroid/content/Context;Ljava/util/ArrayList;)J

    move-result-wide v22

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->mTotalSize:J

    .line 73
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->getTotalItemCount(Ljava/util/ArrayList;)I

    move-result v22

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->mTotalFilesCount:I

    .line 75
    new-instance v12, Ljava/util/LinkedList;

    invoke-direct {v12}, Ljava/util/LinkedList;-><init>()V

    .line 79
    .local v12, "queue":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/io/File;>;"
    :try_start_0
    new-instance v9, Ljava/io/FileOutputStream;

    invoke-direct {v9, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    .end local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    .local v9, "fileOutputStream":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v21, Ljava/util/zip/ZipOutputStream;

    move-object/from16 v0, v21

    invoke-direct {v0, v9}, Ljava/util/zip/ZipOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_12
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_e
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 85
    .end local v20    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    .local v21, "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    :try_start_2
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_13
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_f
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    move-object v15, v14

    .end local v14    # "srcFile":Ljava/io/File;
    .local v15, "srcFile":Ljava/io/File;
    move-object/from16 v18, v17

    .end local v17    # "zipEntry":Ljava/util/zip/ZipEntry;
    .local v18, "zipEntry":Ljava/util/zip/ZipEntry;
    :goto_0
    :try_start_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_17

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 87
    .local v13, "src":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->isCancelled()Z

    move-result v22

    if-eqz v22, :cond_1

    .line 89
    invoke-virtual {v12}, Ljava/util/LinkedList;->clear()V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_14
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_10
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move-object v14, v15

    .end local v15    # "srcFile":Ljava/io/File;
    .restart local v14    # "srcFile":Ljava/io/File;
    move-object/from16 v17, v18

    .line 122
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v13    # "src":Ljava/lang/String;
    .end local v18    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v17    # "zipEntry":Ljava/util/zip/ZipEntry;
    :cond_0
    :goto_1
    :try_start_4
    invoke-virtual {v12}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v22

    if-nez v22, :cond_8

    .line 124
    invoke-virtual {v12}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v22

    move-object/from16 v0, v22

    check-cast v0, Ljava/io/File;

    move-object v14, v0

    .line 126
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->mCurrentCounter:I

    move/from16 v22, v0

    add-int/lit8 v22, v22, 0x1

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->mCurrentCounter:I

    .line 128
    const-wide/16 v22, 0x0

    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->setProgress(J)V

    .line 130
    if-eqz v14, :cond_0

    .line 132
    invoke-virtual {v14}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v16

    .line 135
    .local v16, "subFiles":[Ljava/io/File;
    if-eqz v16, :cond_0

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v22, v0

    if-eqz v22, :cond_0

    .line 137
    move-object/from16 v4, v16

    .local v4, "arr$":[Ljava/io/File;
    array-length v11, v4
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_13
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_f
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .local v11, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    move-object/from16 v18, v17

    .end local v17    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v18    # "zipEntry":Ljava/util/zip/ZipEntry;
    :goto_2
    if-ge v10, v11, :cond_16

    :try_start_5
    aget-object v5, v4, v10

    .line 139
    .local v5, "child":Ljava/io/File;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->isCancelled()Z
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_15
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_11
    .catchall {:try_start_5 .. :try_end_5} :catchall_4

    move-result v22

    if-eqz v22, :cond_4

    move-object/from16 v17, v18

    .line 141
    .end local v18    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v17    # "zipEntry":Ljava/util/zip/ZipEntry;
    goto :goto_1

    .line 95
    .end local v4    # "arr$":[Ljava/io/File;
    .end local v5    # "child":Ljava/io/File;
    .end local v11    # "len$":I
    .end local v14    # "srcFile":Ljava/io/File;
    .end local v16    # "subFiles":[Ljava/io/File;
    .end local v17    # "zipEntry":Ljava/util/zip/ZipEntry;
    .local v10, "i$":Ljava/util/Iterator;
    .restart local v13    # "src":Ljava/lang/String;
    .restart local v15    # "srcFile":Ljava/io/File;
    .restart local v18    # "zipEntry":Ljava/util/zip/ZipEntry;
    :cond_1
    :try_start_6
    new-instance v14, Ljava/io/File;

    invoke-direct {v14, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_14
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_10
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 97
    .end local v15    # "srcFile":Ljava/io/File;
    .restart local v14    # "srcFile":Ljava/io/File;
    :try_start_7
    invoke-virtual {v14}, Ljava/io/File;->isDirectory()Z

    move-result v22

    if-eqz v22, :cond_3

    .line 99
    invoke-virtual {v12, v14}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 101
    const-string v22, "/"

    move-object/from16 v0, v22

    invoke-virtual {v13, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_2

    move-object/from16 v19, v13

    .line 103
    :goto_3
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "/"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    const-string v23, ""

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v19

    .line 105
    new-instance v17, Ljava/util/zip/ZipEntry;

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Ljava/util/zip/ZipEntry;-><init>(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_15
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_11
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    .line 107
    .end local v18    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v17    # "zipEntry":Ljava/util/zip/ZipEntry;
    :try_start_8
    invoke-virtual {v14}, Ljava/io/File;->lastModified()J

    move-result-wide v22

    move-object/from16 v0, v17

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/util/zip/ZipEntry;->setTime(J)V

    .line 109
    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/zip/ZipOutputStream;->putNextEntry(Ljava/util/zip/ZipEntry;)V
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_13
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_f
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :goto_4
    move-object v15, v14

    .end local v14    # "srcFile":Ljava/io/File;
    .restart local v15    # "srcFile":Ljava/io/File;
    move-object/from16 v18, v17

    .line 120
    .end local v17    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v18    # "zipEntry":Ljava/util/zip/ZipEntry;
    goto/16 :goto_0

    .line 101
    .end local v15    # "srcFile":Ljava/io/File;
    .restart local v14    # "srcFile":Ljava/io/File;
    :cond_2
    :try_start_9
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "/"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    goto :goto_3

    .line 113
    :cond_3
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "/"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    const-string v23, ""

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v19

    .line 115
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v14, v2}, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->addFileToZip(Ljava/lang/String;Ljava/io/File;Ljava/util/zip/ZipOutputStream;)V

    move-object/from16 v17, v18

    .end local v18    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v17    # "zipEntry":Ljava/util/zip/ZipEntry;
    goto :goto_4

    .line 145
    .end local v13    # "src":Ljava/lang/String;
    .end local v17    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v4    # "arr$":[Ljava/io/File;
    .restart local v5    # "child":Ljava/io/File;
    .local v10, "i$":I
    .restart local v11    # "len$":I
    .restart local v16    # "subFiles":[Ljava/io/File;
    .restart local v18    # "zipEntry":Ljava/util/zip/ZipEntry;
    :cond_4
    if-nez v5, :cond_5

    move-object/from16 v17, v18

    .line 146
    .end local v18    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v17    # "zipEntry":Ljava/util/zip/ZipEntry;
    goto/16 :goto_1

    .line 148
    .end local v17    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v18    # "zipEntry":Ljava/util/zip/ZipEntry;
    :cond_5
    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v19

    .line 150
    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v22

    if-eqz v22, :cond_7

    .line 152
    invoke-virtual {v12, v5}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 154
    const-string v22, "/"

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v22

    if-eqz v22, :cond_6

    .line 156
    :goto_5
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "/"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    const-string v23, ""

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v19

    .line 158
    new-instance v17, Ljava/util/zip/ZipEntry;

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Ljava/util/zip/ZipEntry;-><init>(Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_9} :catch_15
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_11
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    .line 160
    .end local v18    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v17    # "zipEntry":Ljava/util/zip/ZipEntry;
    :try_start_a
    invoke-virtual {v5}, Ljava/io/File;->lastModified()J

    move-result-wide v22

    move-object/from16 v0, v17

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/util/zip/ZipEntry;->setTime(J)V

    .line 162
    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/zip/ZipOutputStream;->putNextEntry(Ljava/util/zip/ZipEntry;)V
    :try_end_a
    .catch Ljava/io/FileNotFoundException; {:try_start_a .. :try_end_a} :catch_13
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_f
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 137
    :goto_6
    add-int/lit8 v10, v10, 0x1

    move-object/from16 v18, v17

    .end local v17    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v18    # "zipEntry":Ljava/util/zip/ZipEntry;
    goto/16 :goto_2

    .line 154
    :cond_6
    :try_start_b
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "/"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    goto :goto_5

    .line 166
    :cond_7
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "/"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    const-string v23, ""

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v19

    .line 168
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->addFileToZip(Ljava/lang/String;Ljava/io/File;Ljava/util/zip/ZipOutputStream;)V
    :try_end_b
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_b} :catch_15
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_11
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    move-object/from16 v17, v18

    .end local v18    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v17    # "zipEntry":Ljava/util/zip/ZipEntry;
    goto :goto_6

    .line 193
    .end local v4    # "arr$":[Ljava/io/File;
    .end local v5    # "child":Ljava/io/File;
    .end local v10    # "i$":I
    .end local v11    # "len$":I
    .end local v16    # "subFiles":[Ljava/io/File;
    :cond_8
    if-eqz v21, :cond_9

    .line 195
    :try_start_c
    invoke-virtual/range {v21 .. v21}, Ljava/util/zip/ZipOutputStream;->flush()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_0

    .line 207
    :cond_9
    :goto_7
    if-eqz v21, :cond_a

    .line 209
    :try_start_d
    invoke-virtual/range {v21 .. v21}, Ljava/util/zip/ZipOutputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_1

    .line 221
    :cond_a
    :goto_8
    if-eqz v9, :cond_b

    .line 223
    :try_start_e
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_2

    :cond_b
    move-object/from16 v20, v21

    .end local v21    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    .restart local v20    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    move-object v8, v9

    .line 235
    .end local v9    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    :cond_c
    :goto_9
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->isCancelled()Z

    move-result v22

    if-eqz v22, :cond_15

    .line 237
    if-eqz v6, :cond_d

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v22

    if-eqz v22, :cond_d

    .line 239
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 243
    :cond_d
    const-string v22, "ZipCompressor"

    const-string v23, "Compressing\'s been cancelled"

    invoke-static/range {v22 .. v23}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    :goto_a
    return-void

    .line 199
    .end local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    .end local v20    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    .restart local v9    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v21    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    :catch_0
    move-exception v7

    .line 201
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 213
    .end local v7    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v7

    .line 215
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 227
    .end local v7    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v7

    .line 229
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    move-object/from16 v20, v21

    .end local v21    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    .restart local v20    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    move-object v8, v9

    .line 233
    .end local v9    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto :goto_9

    .line 181
    .end local v7    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v7

    .line 183
    .local v7, "e":Ljava/io/FileNotFoundException;
    :goto_b
    :try_start_f
    invoke-virtual {v7}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 193
    if-eqz v20, :cond_e

    .line 195
    :try_start_10
    invoke-virtual/range {v20 .. v20}, Ljava/util/zip/ZipOutputStream;->flush()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_5

    .line 207
    .end local v7    # "e":Ljava/io/FileNotFoundException;
    :cond_e
    :goto_c
    if-eqz v20, :cond_f

    .line 209
    :try_start_11
    invoke-virtual/range {v20 .. v20}, Ljava/util/zip/ZipOutputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_6

    .line 221
    :cond_f
    :goto_d
    if-eqz v8, :cond_c

    .line 223
    :try_start_12
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_4

    goto :goto_9

    .line 227
    :catch_4
    move-exception v7

    .line 229
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 199
    .local v7, "e":Ljava/io/FileNotFoundException;
    :catch_5
    move-exception v7

    .line 201
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_c

    .line 213
    .end local v7    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v7

    .line 215
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_d

    .line 185
    .end local v7    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v7

    .line 187
    .restart local v7    # "e":Ljava/io/IOException;
    :goto_e
    :try_start_13
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    .line 193
    if-eqz v20, :cond_10

    .line 195
    :try_start_14
    invoke-virtual/range {v20 .. v20}, Ljava/util/zip/ZipOutputStream;->flush()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_9

    .line 207
    :cond_10
    :goto_f
    if-eqz v20, :cond_11

    .line 209
    :try_start_15
    invoke-virtual/range {v20 .. v20}, Ljava/util/zip/ZipOutputStream;->close()V
    :try_end_15
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_a

    .line 221
    :cond_11
    :goto_10
    if-eqz v8, :cond_c

    .line 223
    :try_start_16
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_8

    goto :goto_9

    .line 227
    :catch_8
    move-exception v7

    .line 229
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 199
    :catch_9
    move-exception v7

    .line 201
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_f

    .line 213
    :catch_a
    move-exception v7

    .line 215
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_10

    .line 191
    .end local v7    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v22

    .line 193
    :goto_11
    if-eqz v20, :cond_12

    .line 195
    :try_start_17
    invoke-virtual/range {v20 .. v20}, Ljava/util/zip/ZipOutputStream;->flush()V
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_b

    .line 207
    :cond_12
    :goto_12
    if-eqz v20, :cond_13

    .line 209
    :try_start_18
    invoke-virtual/range {v20 .. v20}, Ljava/util/zip/ZipOutputStream;->close()V
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_c

    .line 221
    :cond_13
    :goto_13
    if-eqz v8, :cond_14

    .line 223
    :try_start_19
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_19 .. :try_end_19} :catch_d

    .line 231
    :cond_14
    :goto_14
    throw v22

    .line 199
    :catch_b
    move-exception v7

    .line 201
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_12

    .line 213
    .end local v7    # "e":Ljava/io/IOException;
    :catch_c
    move-exception v7

    .line 215
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_13

    .line 227
    .end local v7    # "e":Ljava/io/IOException;
    :catch_d
    move-exception v7

    .line 229
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_14

    .line 247
    .end local v7    # "e":Ljava/io/IOException;
    :cond_15
    const-string v22, "ZipCompressor"

    const-string v23, "Compressing\'s finished"

    invoke-static/range {v22 .. v23}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a

    .line 191
    .end local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v9    # "fileOutputStream":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v22

    move-object v8, v9

    .end local v9    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto :goto_11

    .end local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    .end local v20    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    .restart local v9    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v21    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    :catchall_2
    move-exception v22

    move-object/from16 v20, v21

    .end local v21    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    .restart local v20    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    move-object v8, v9

    .end local v9    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto :goto_11

    .end local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    .end local v14    # "srcFile":Ljava/io/File;
    .end local v17    # "zipEntry":Ljava/util/zip/ZipEntry;
    .end local v20    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    .restart local v9    # "fileOutputStream":Ljava/io/FileOutputStream;
    .local v10, "i$":Ljava/util/Iterator;
    .restart local v15    # "srcFile":Ljava/io/File;
    .restart local v18    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v21    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    :catchall_3
    move-exception v22

    move-object v14, v15

    .end local v15    # "srcFile":Ljava/io/File;
    .restart local v14    # "srcFile":Ljava/io/File;
    move-object/from16 v17, v18

    .end local v18    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v17    # "zipEntry":Ljava/util/zip/ZipEntry;
    move-object/from16 v20, v21

    .end local v21    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    .restart local v20    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    move-object v8, v9

    .end local v9    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto :goto_11

    .end local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v17    # "zipEntry":Ljava/util/zip/ZipEntry;
    .end local v20    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    .restart local v9    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v18    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v21    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    :catchall_4
    move-exception v22

    move-object/from16 v17, v18

    .end local v18    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v17    # "zipEntry":Ljava/util/zip/ZipEntry;
    move-object/from16 v20, v21

    .end local v21    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    .restart local v20    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    move-object v8, v9

    .end local v9    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto :goto_11

    .line 185
    .end local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v9    # "fileOutputStream":Ljava/io/FileOutputStream;
    :catch_e
    move-exception v7

    move-object v8, v9

    .end local v9    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto :goto_e

    .end local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    .end local v20    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    .restart local v9    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v21    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    :catch_f
    move-exception v7

    move-object/from16 v20, v21

    .end local v21    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    .restart local v20    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    move-object v8, v9

    .end local v9    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto :goto_e

    .end local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    .end local v14    # "srcFile":Ljava/io/File;
    .end local v17    # "zipEntry":Ljava/util/zip/ZipEntry;
    .end local v20    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    .restart local v9    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v10    # "i$":Ljava/util/Iterator;
    .restart local v15    # "srcFile":Ljava/io/File;
    .restart local v18    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v21    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    :catch_10
    move-exception v7

    move-object v14, v15

    .end local v15    # "srcFile":Ljava/io/File;
    .restart local v14    # "srcFile":Ljava/io/File;
    move-object/from16 v17, v18

    .end local v18    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v17    # "zipEntry":Ljava/util/zip/ZipEntry;
    move-object/from16 v20, v21

    .end local v21    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    .restart local v20    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    move-object v8, v9

    .end local v9    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto :goto_e

    .end local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v17    # "zipEntry":Ljava/util/zip/ZipEntry;
    .end local v20    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    .restart local v9    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v18    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v21    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    :catch_11
    move-exception v7

    move-object/from16 v17, v18

    .end local v18    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v17    # "zipEntry":Ljava/util/zip/ZipEntry;
    move-object/from16 v20, v21

    .end local v21    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    .restart local v20    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    move-object v8, v9

    .end local v9    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto :goto_e

    .line 181
    .end local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v9    # "fileOutputStream":Ljava/io/FileOutputStream;
    :catch_12
    move-exception v7

    move-object v8, v9

    .end local v9    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto/16 :goto_b

    .end local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    .end local v20    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    .restart local v9    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v21    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    :catch_13
    move-exception v7

    move-object/from16 v20, v21

    .end local v21    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    .restart local v20    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    move-object v8, v9

    .end local v9    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto/16 :goto_b

    .end local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    .end local v14    # "srcFile":Ljava/io/File;
    .end local v17    # "zipEntry":Ljava/util/zip/ZipEntry;
    .end local v20    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    .restart local v9    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v10    # "i$":Ljava/util/Iterator;
    .restart local v15    # "srcFile":Ljava/io/File;
    .restart local v18    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v21    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    :catch_14
    move-exception v7

    move-object v14, v15

    .end local v15    # "srcFile":Ljava/io/File;
    .restart local v14    # "srcFile":Ljava/io/File;
    move-object/from16 v17, v18

    .end local v18    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v17    # "zipEntry":Ljava/util/zip/ZipEntry;
    move-object/from16 v20, v21

    .end local v21    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    .restart local v20    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    move-object v8, v9

    .end local v9    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto/16 :goto_b

    .end local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v17    # "zipEntry":Ljava/util/zip/ZipEntry;
    .end local v20    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    .restart local v9    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v18    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v21    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    :catch_15
    move-exception v7

    move-object/from16 v17, v18

    .end local v18    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v17    # "zipEntry":Ljava/util/zip/ZipEntry;
    move-object/from16 v20, v21

    .end local v21    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    .restart local v20    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    move-object v8, v9

    .end local v9    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto/16 :goto_b

    .end local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    .end local v17    # "zipEntry":Ljava/util/zip/ZipEntry;
    .end local v20    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    .restart local v4    # "arr$":[Ljava/io/File;
    .restart local v9    # "fileOutputStream":Ljava/io/FileOutputStream;
    .local v10, "i$":I
    .restart local v11    # "len$":I
    .restart local v16    # "subFiles":[Ljava/io/File;
    .restart local v18    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v21    # "zipOutputStream":Ljava/util/zip/ZipOutputStream;
    :cond_16
    move-object/from16 v17, v18

    .end local v18    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v17    # "zipEntry":Ljava/util/zip/ZipEntry;
    goto/16 :goto_1

    .end local v4    # "arr$":[Ljava/io/File;
    .end local v11    # "len$":I
    .end local v14    # "srcFile":Ljava/io/File;
    .end local v16    # "subFiles":[Ljava/io/File;
    .end local v17    # "zipEntry":Ljava/util/zip/ZipEntry;
    .local v10, "i$":Ljava/util/Iterator;
    .restart local v15    # "srcFile":Ljava/io/File;
    .restart local v18    # "zipEntry":Ljava/util/zip/ZipEntry;
    :cond_17
    move-object v14, v15

    .end local v15    # "srcFile":Ljava/io/File;
    .restart local v14    # "srcFile":Ljava/io/File;
    move-object/from16 v17, v18

    .end local v18    # "zipEntry":Ljava/util/zip/ZipEntry;
    .restart local v17    # "zipEntry":Ljava/util/zip/ZipEntry;
    goto/16 :goto_1
.end method

.method public doExtract(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 22
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "src"    # Ljava/lang/String;
    .param p3, "dst"    # Ljava/lang/String;
    .param p4, "type"    # I

    .prologue
    .line 311
    const-string v18, "ZipCompressor"

    const-string v19, "Extracting\'s started"

    invoke-static/range {v18 .. v19}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    const/4 v9, -0x1

    .line 315
    .local v9, "errorCode":I
    new-instance v14, Ljava/io/File;

    move-object/from16 v0, p2

    invoke-direct {v14, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 317
    .local v14, "srcFile":Ljava/io/File;
    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    move-result v18

    if-nez v18, :cond_0

    .line 635
    :goto_0
    return-void

    .line 322
    :cond_0
    const/4 v15, 0x0

    .line 326
    .local v15, "srcZipFile":Ljava/util/zip/ZipFile;
    :try_start_0
    new-instance v16, Ljava/util/zip/ZipFile;

    move-object/from16 v0, v16

    invoke-direct {v0, v14}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/util/zip/ZipException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 342
    .end local v15    # "srcZipFile":Ljava/util/zip/ZipFile;
    .local v16, "srcZipFile":Ljava/util/zip/ZipFile;
    if-eqz v16, :cond_1

    .line 346
    :try_start_1
    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipFile;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 354
    :cond_1
    :goto_1
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v9, v0, :cond_a

    .line 356
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->mOperationListener:Lcom/sec/android/app/myfiles/compression/AbsCompressor$OnOperationListener;

    move-object/from16 v18, v0

    if-eqz v18, :cond_2

    .line 358
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->mOperationListener:Lcom/sec/android/app/myfiles/compression/AbsCompressor$OnOperationListener;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-interface {v0, v1, v9}, Lcom/sec/android/app/myfiles/compression/AbsCompressor$OnOperationListener;->onCancelled(II)V

    .line 361
    :cond_2
    const/16 v18, 0x0

    const-string v19, "ZipCompressor"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "There is an error : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 348
    :catch_0
    move-exception v8

    .line 350
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 328
    .end local v8    # "e":Ljava/io/IOException;
    .end local v16    # "srcZipFile":Ljava/util/zip/ZipFile;
    .restart local v15    # "srcZipFile":Ljava/util/zip/ZipFile;
    :catch_1
    move-exception v8

    .line 330
    .local v8, "e":Ljava/util/zip/ZipException;
    :try_start_2
    invoke-virtual {v8}, Ljava/util/zip/ZipException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 332
    const/4 v9, 0x1

    .line 342
    if-eqz v15, :cond_3

    .line 346
    :try_start_3
    invoke-virtual {v15}, Ljava/util/zip/ZipFile;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 354
    .end local v8    # "e":Ljava/util/zip/ZipException;
    :cond_3
    :goto_2
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v9, v0, :cond_b

    .line 356
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->mOperationListener:Lcom/sec/android/app/myfiles/compression/AbsCompressor$OnOperationListener;

    move-object/from16 v18, v0

    if-eqz v18, :cond_4

    .line 358
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->mOperationListener:Lcom/sec/android/app/myfiles/compression/AbsCompressor$OnOperationListener;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-interface {v0, v1, v9}, Lcom/sec/android/app/myfiles/compression/AbsCompressor$OnOperationListener;->onCancelled(II)V

    .line 361
    :cond_4
    const/16 v18, 0x0

    const-string v19, "ZipCompressor"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "There is an error : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 348
    .restart local v8    # "e":Ljava/util/zip/ZipException;
    :catch_2
    move-exception v8

    .line 350
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 334
    .end local v8    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v8

    .line 336
    .restart local v8    # "e":Ljava/io/IOException;
    :try_start_4
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 338
    const/4 v9, 0x1

    .line 342
    if-eqz v15, :cond_5

    .line 346
    :try_start_5
    invoke-virtual {v15}, Ljava/util/zip/ZipFile;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 354
    :cond_5
    :goto_3
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v9, v0, :cond_b

    .line 356
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->mOperationListener:Lcom/sec/android/app/myfiles/compression/AbsCompressor$OnOperationListener;

    move-object/from16 v18, v0

    if-eqz v18, :cond_6

    .line 358
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->mOperationListener:Lcom/sec/android/app/myfiles/compression/AbsCompressor$OnOperationListener;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-interface {v0, v1, v9}, Lcom/sec/android/app/myfiles/compression/AbsCompressor$OnOperationListener;->onCancelled(II)V

    .line 361
    :cond_6
    const/16 v18, 0x0

    const-string v19, "ZipCompressor"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "There is an error : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 348
    :catch_4
    move-exception v8

    .line 350
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 342
    .end local v8    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v18

    if-eqz v15, :cond_7

    .line 346
    :try_start_6
    invoke-virtual {v15}, Ljava/util/zip/ZipFile;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 354
    :cond_7
    :goto_4
    const/16 v19, -0x1

    move/from16 v0, v19

    if-eq v9, v0, :cond_9

    .line 356
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->mOperationListener:Lcom/sec/android/app/myfiles/compression/AbsCompressor$OnOperationListener;

    move-object/from16 v18, v0

    if-eqz v18, :cond_8

    .line 358
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->mOperationListener:Lcom/sec/android/app/myfiles/compression/AbsCompressor$OnOperationListener;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-interface {v0, v1, v9}, Lcom/sec/android/app/myfiles/compression/AbsCompressor$OnOperationListener;->onCancelled(II)V

    .line 361
    :cond_8
    const/16 v18, 0x0

    const-string v19, "ZipCompressor"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "There is an error : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 348
    :catch_5
    move-exception v8

    .line 350
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 363
    .end local v8    # "e":Ljava/io/IOException;
    :cond_9
    throw v18

    .end local v15    # "srcZipFile":Ljava/util/zip/ZipFile;
    .restart local v16    # "srcZipFile":Ljava/util/zip/ZipFile;
    :cond_a
    move-object/from16 v15, v16

    .line 367
    .end local v16    # "srcZipFile":Ljava/util/zip/ZipFile;
    .restart local v15    # "srcZipFile":Ljava/util/zip/ZipFile;
    :cond_b
    invoke-virtual {v14}, Ljava/io/File;->length()J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->mTotalSize:J

    .line 381
    const/4 v12, 0x0

    .line 383
    .local v12, "headersList":Ljava/util/List;
    const/4 v10, 0x0

    .line 385
    .local v10, "fileHeader":Lnet/lingala/zip4j/model/FileHeader;
    new-instance v7, Ljava/io/File;

    move-object/from16 v0, p3

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 387
    .local v7, "dstFolder":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v18

    if-nez v18, :cond_11

    .line 389
    invoke-virtual {v7}, Ljava/io/File;->mkdir()Z

    .line 401
    :cond_c
    :goto_5
    :try_start_7
    new-instance v17, Lnet/lingala/zip4j/core/ZipFile;

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lnet/lingala/zip4j/core/ZipFile;-><init>(Ljava/lang/String;)V

    .line 403
    .local v17, "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->isUTF8Encoded(Lnet/lingala/zip4j/core/ZipFile;)Z

    move-result v18

    if-nez v18, :cond_d

    .line 404
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->setCharset(Landroid/content/Context;Lnet/lingala/zip4j/core/ZipFile;)Lnet/lingala/zip4j/core/ZipFile;

    move-result-object v17

    .line 407
    :cond_d
    invoke-virtual/range {v17 .. v17}, Lnet/lingala/zip4j/core/ZipFile;->getFileHeaders()Ljava/util/List;

    move-result-object v12

    .line 409
    if-eqz v12, :cond_e

    .line 410
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->mTotalFilesCount:I

    .line 412
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_6
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    if-ge v13, v0, :cond_e

    .line 414
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->isCancelled()Z
    :try_end_7
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_7 .. :try_end_7} :catch_6

    move-result v18

    if-eqz v18, :cond_12

    .line 614
    .end local v13    # "i":I
    .end local v17    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :cond_e
    :goto_7
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v9, v0, :cond_16

    .line 615
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->mOperationListener:Lcom/sec/android/app/myfiles/compression/AbsCompressor$OnOperationListener;

    move-object/from16 v18, v0

    if-eqz v18, :cond_f

    .line 617
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->mOperationListener:Lcom/sec/android/app/myfiles/compression/AbsCompressor$OnOperationListener;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-interface {v0, v1, v9}, Lcom/sec/android/app/myfiles/compression/AbsCompressor$OnOperationListener;->onCancelled(II)V

    .line 619
    :cond_f
    const/16 v18, 0x0

    const-string v19, "ZipCompressor"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "There is an error : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 627
    :cond_10
    :goto_8
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->isCancelled()Z

    move-result v18

    if-eqz v18, :cond_17

    .line 629
    const/16 v18, 0x0

    const-string v19, "ZipCompressor"

    const-string v20, "Extracting\'s been cancelled"

    invoke-static/range {v18 .. v20}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 391
    :cond_11
    const/16 v18, 0x4

    move/from16 v0, p4

    move/from16 v1, v18

    if-ne v0, v1, :cond_c

    .line 392
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->mFilesOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p3

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->checkDirOrFileName(Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v7

    .line 393
    if-eqz v7, :cond_c

    .line 394
    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p3

    goto/16 :goto_5

    .line 419
    .restart local v13    # "i":I
    .restart local v17    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :cond_12
    :try_start_8
    invoke-interface {v12, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, v18

    check-cast v0, Lnet/lingala/zip4j/model/FileHeader;

    move-object v10, v0

    .line 421
    add-int/lit8 v18, v13, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->mCurrentCounter:I

    .line 423
    invoke-virtual {v10}, Lnet/lingala/zip4j/model/FileHeader;->getFileName()Ljava/lang/String;

    move-result-object v11

    .line 425
    .local v11, "fileName":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 427
    .local v6, "dstFilePathWithName":Ljava/lang/StringBuilder;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 429
    .local v5, "dstFilePath":Ljava/lang/StringBuilder;
    move-object/from16 v0, p3

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    sget-object v19, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 431
    const/16 v18, 0x0

    sget-object v19, Ljava/io/File;->separator:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->lastIndexOf(Ljava/lang/String;)I

    move-result v19

    add-int/lit8 v19, v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 433
    const/4 v4, 0x0

    .line 435
    .local v4, "dstFile":Ljava/io/File;
    invoke-virtual {v10}, Lnet/lingala/zip4j/model/FileHeader;->isDirectory()Z

    move-result v18

    if-eqz v18, :cond_14

    .line 437
    new-instance v4, Ljava/io/File;

    .end local v4    # "dstFile":Ljava/io/File;
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 439
    .restart local v4    # "dstFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v18

    if-nez v18, :cond_13

    .line 441
    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    .line 412
    :cond_13
    :goto_9
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_6

    .line 448
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->mFilesOperationTask:Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;

    move-object/from16 v18, v0

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x1

    invoke-virtual/range {v18 .. v20}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->checkDirOrFileName(Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v4

    .line 450
    if-eqz v4, :cond_13

    invoke-virtual {v10}, Lnet/lingala/zip4j/model/FileHeader;->isDirectory()Z

    move-result v18

    if-nez v18, :cond_13

    .line 455
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x0

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v0, v10, v1, v2, v3}, Lnet/lingala/zip4j/core/ZipFile;->extractFile(Lnet/lingala/zip4j/model/FileHeader;Ljava/lang/String;Lnet/lingala/zip4j/model/UnzipParameters;Ljava/lang/String;)V

    .line 457
    invoke-virtual {v10}, Lnet/lingala/zip4j/model/FileHeader;->getLastModFileTime()I

    move-result v18

    if-gez v18, :cond_15

    .line 459
    invoke-virtual {v10}, Lnet/lingala/zip4j/model/FileHeader;->getLastModFileTime()I

    move-result v18

    move/from16 v0, v18

    neg-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v4, v0, v1}, Ljava/io/File;->setLastModified(J)Z

    .line 467
    :goto_a
    invoke-virtual {v10}, Lnet/lingala/zip4j/model/FileHeader;->getCompressedSize()J

    move-result-wide v18

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->setProgress(J)V
    :try_end_8
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_8 .. :try_end_8} :catch_6

    goto :goto_9

    .line 473
    .end local v4    # "dstFile":Ljava/io/File;
    .end local v5    # "dstFilePath":Ljava/lang/StringBuilder;
    .end local v6    # "dstFilePathWithName":Ljava/lang/StringBuilder;
    .end local v11    # "fileName":Ljava/lang/String;
    .end local v13    # "i":I
    .end local v17    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :catch_6
    move-exception v8

    .line 475
    .local v8, "e":Lnet/lingala/zip4j/exception/ZipException;
    invoke-virtual {v8}, Lnet/lingala/zip4j/exception/ZipException;->printStackTrace()V

    .line 476
    const/4 v9, 0x1

    goto/16 :goto_7

    .line 463
    .end local v8    # "e":Lnet/lingala/zip4j/exception/ZipException;
    .restart local v4    # "dstFile":Ljava/io/File;
    .restart local v5    # "dstFilePath":Ljava/lang/StringBuilder;
    .restart local v6    # "dstFilePathWithName":Ljava/lang/StringBuilder;
    .restart local v11    # "fileName":Ljava/lang/String;
    .restart local v13    # "i":I
    .restart local v17    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :cond_15
    :try_start_9
    invoke-virtual {v10}, Lnet/lingala/zip4j/model/FileHeader;->getLastModFileTime()I

    move-result v18

    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v4, v0, v1}, Ljava/io/File;->setLastModified(J)Z
    :try_end_9
    .catch Lnet/lingala/zip4j/exception/ZipException; {:try_start_9 .. :try_end_9} :catch_6

    goto :goto_a

    .line 621
    .end local v4    # "dstFile":Ljava/io/File;
    .end local v5    # "dstFilePath":Ljava/lang/StringBuilder;
    .end local v6    # "dstFilePathWithName":Ljava/lang/StringBuilder;
    .end local v11    # "fileName":Ljava/lang/String;
    .end local v13    # "i":I
    .end local v17    # "zipFile":Lnet/lingala/zip4j/core/ZipFile;
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->mOperationListener:Lcom/sec/android/app/myfiles/compression/AbsCompressor$OnOperationListener;

    move-object/from16 v18, v0

    if-eqz v18, :cond_10

    .line 623
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/compression/ZipCompressor;->mOperationListener:Lcom/sec/android/app/myfiles/compression/AbsCompressor$OnOperationListener;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    invoke-interface/range {v18 .. v19}, Lcom/sec/android/app/myfiles/compression/AbsCompressor$OnOperationListener;->onCompleted(I)V

    goto/16 :goto_8

    .line 633
    :cond_17
    const/16 v18, 0x0

    const-string v19, "ZipCompressor"

    const-string v20, "Extracting\'s finished"

    invoke-static/range {v18 .. v20}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
