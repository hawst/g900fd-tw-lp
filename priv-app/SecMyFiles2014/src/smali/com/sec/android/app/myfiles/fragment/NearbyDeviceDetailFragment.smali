.class public Lcom/sec/android/app/myfiles/fragment/NearbyDeviceDetailFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;
.source "NearbyDeviceDetailFragment.java"


# static fields
.field private static final FILENAME_TAG:Ljava/lang/String; = "filename"

.field private static final FILESIZE_TAG:Ljava/lang/String; = "filesize"

.field private static final MODTIME_TAG:Ljava/lang/String; = "modtime"


# instance fields
.field mAdapter:Landroid/widget/BaseAdapter;

.field private mFileName:Ljava/lang/String;

.field private mFileSize:J

.field private mModificationTime:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public getFileLastModifiedTime(Ljava/lang/String;I)J
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 126
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDeviceDetailFragment;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v1}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v1

    if-le v1, p2, :cond_0

    .line 128
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDeviceDetailFragment;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v1, p2}, Landroid/widget/BaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 130
    .local v0, "cursor":Landroid/database/Cursor;
    const-string v1, "Date"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 132
    .local v2, "modificationTime":J
    iput-wide v2, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDeviceDetailFragment;->mModificationTime:J

    .line 138
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v2    # "modificationTime":J
    :goto_0
    return-wide v2

    :cond_0
    iget-wide v2, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDeviceDetailFragment;->mModificationTime:J

    goto :goto_0
.end method

.method public getFileName(Ljava/lang/String;I)Ljava/lang/String;
    .locals 5
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 78
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDeviceDetailFragment;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v4}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v4

    if-le v4, p2, :cond_0

    .line 80
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDeviceDetailFragment;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v4, p2}, Landroid/widget/BaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 82
    .local v0, "cursor":Landroid/database/Cursor;
    const-string v4, "title"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 84
    .local v2, "fileName":Ljava/lang/String;
    const-string v4, "Extension"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 86
    .local v1, "fileExtension":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 88
    .local v3, "fullName":Ljava/lang/String;
    iput-object v3, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDeviceDetailFragment;->mFileName:Ljava/lang/String;

    .line 94
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v1    # "fileExtension":Ljava/lang/String;
    .end local v2    # "fileName":Ljava/lang/String;
    .end local v3    # "fullName":Ljava/lang/String;
    :goto_0
    return-object v3

    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDeviceDetailFragment;->mFileName:Ljava/lang/String;

    goto :goto_0
.end method

.method public getFileSize(Ljava/lang/String;I)J
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 104
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDeviceDetailFragment;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v1}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v1

    if-le v1, p2, :cond_0

    .line 106
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDeviceDetailFragment;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v1, p2}, Landroid/widget/BaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 108
    .local v0, "cursor":Landroid/database/Cursor;
    const-string v1, "FileSize"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 110
    .local v2, "size":J
    iput-wide v2, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDeviceDetailFragment;->mFileSize:J

    .line 116
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v2    # "size":J
    :goto_0
    return-wide v2

    :cond_0
    iget-wide v2, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDeviceDetailFragment;->mFileSize:J

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const-wide/16 v2, 0x0

    .line 46
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->onCreate(Landroid/os/Bundle;)V

    .line 48
    if-eqz p1, :cond_0

    .line 50
    const-string v0, "filename"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDeviceDetailFragment;->mFileName:Ljava/lang/String;

    .line 52
    const-string v0, "filesize"

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDeviceDetailFragment;->mFileSize:J

    .line 54
    const-string v0, "modtime"

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDeviceDetailFragment;->mModificationTime:J

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDeviceDetailFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getmAdapter()Landroid/widget/BaseAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDeviceDetailFragment;->mAdapter:Landroid/widget/BaseAdapter;

    .line 60
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 65
    const-string v0, "filename"

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDeviceDetailFragment;->mFileName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const-string v0, "filesize"

    iget-wide v2, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDeviceDetailFragment;->mFileSize:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 69
    const-string v0, "modtime"

    iget-wide v2, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDeviceDetailFragment;->mModificationTime:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 71
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsDetailFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 73
    return-void
.end method
