.class public Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;
.super Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;
.source "CopyMoveFtpToFtpOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation$1;
    }
.end annotation


# instance fields
.field private mDoneCount:I

.field private final mDstFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

.field private final mMove:Z

.field private mParam:Lcom/sec/android/app/myfiles/element/FileOperationParam;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "operation"    # I
    .param p3, "contentType"    # I
    .param p4, "progressView"    # Landroid/view/View;
    .param p5, "srcFtpHandlerParams"    # Ljava/lang/String;
    .param p6, "dstFtpHandlerParams"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 38
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;-><init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;)V

    .line 227
    iput v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mDoneCount:I

    .line 40
    invoke-static {}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->getInstance()Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;

    move-result-object v0

    .line 42
    .local v0, "factory":Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;
    if-nez v0, :cond_0

    .line 44
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "FTPConnectionHandlerFactory cannot be null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 47
    :cond_0
    new-instance v3, Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-direct {v3, p6}, Lcom/sec/android/app/myfiles/ftp/FTPParams;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->getFTPConnectionHandler(Lcom/sec/android/app/myfiles/ftp/FTPParams;)Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mDstFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    .line 49
    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mDstFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    if-nez v3, :cond_1

    .line 51
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "FTPConnectionHandler cannot be null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 54
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mDstFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mClientOperationListener:Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;

    invoke-interface {v3, v4}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->setFtpClientOperationListener(Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;)V

    .line 56
    if-ne p2, v1, :cond_2

    :goto_0
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mMove:Z

    .line 57
    return-void

    :cond_2
    move v1, v2

    .line 56
    goto :goto_0
.end method


# virtual methods
.method protected cancelOperation()V
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v0}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->abortTransfer()V

    .line 221
    invoke-super {p0}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->cancelOperation()V

    .line 223
    return-void
.end method

.method protected varargs doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;
    .locals 13
    .param p1, "args"    # [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .prologue
    const/4 v11, 0x0

    .line 70
    invoke-static {v11, p1}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->checkFileOperationParam(Z[Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/ftp/FTPErr;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 72
    iget-object v9, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    sget-object v10, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    if-ne v9, v10, :cond_0

    .line 74
    iget-object v9, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-virtual {p0, v9}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->checkConnection(Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;)Lcom/sec/android/app/myfiles/ftp/FTPErr;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 78
    :cond_0
    iget-object v9, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    sget-object v10, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    if-ne v9, v10, :cond_1

    .line 80
    iget-object v9, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mDstFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-virtual {p0, v9}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->checkConnection(Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;)Lcom/sec/android/app/myfiles/ftp/FTPErr;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 84
    :cond_1
    iget-object v9, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    sget-object v10, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    if-ne v9, v10, :cond_7

    .line 86
    aget-object v7, p1, v11

    .line 88
    .local v7, "param":Lcom/sec/android/app/myfiles/element/FileOperationParam;
    iput-object v7, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mParam:Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .line 90
    iget-object v9, v7, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    iget-object v10, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mDownloadTempDir:Ljava/lang/String;

    iget-object v11, v7, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mSrcFolder:Ljava/lang/String;

    invoke-virtual {p0, v9, v10, v11}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->discoverFtpAndMakeLocalDirectories(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;

    move-result-object v8

    .line 93
    .local v8, "res":Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;
    iget-object v9, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    sget-object v10, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    if-ne v9, v10, :cond_7

    .line 95
    iget-object v9, v8, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mFiles:Ljava/util/Stack;

    iget-object v10, v8, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mFiles:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->size()I

    move-result v10

    new-array v10, v10, [Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/util/Stack;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    .line 97
    .local v2, "files":[Ljava/lang/String;
    iget-object v9, v8, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mFolders:Ljava/util/Stack;

    iget-object v10, v8, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mFolders:Ljava/util/Stack;

    invoke-virtual {v10}, Ljava/util/Stack;->size()I

    move-result v10

    new-array v10, v10, [Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/util/Stack;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    .line 99
    .local v5, "folders":[Ljava/lang/String;
    array-length v3, v2

    .line 101
    .local v3, "filesToDownload":I
    array-length v6, v5

    .line 103
    .local v6, "foldersToCreate":I
    iget-object v9, v8, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mJobFile:Ljava/util/ArrayDeque;

    iget-object v10, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mDownloadTempDir:Ljava/lang/String;

    iget-object v11, v7, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mSrcFolder:Ljava/lang/String;

    invoke-virtual {p0, v9, v10, v11}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->downloadItems(Ljava/util/ArrayDeque;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 105
    .local v1, "downloadedItems":I
    if-eq v1, v3, :cond_2

    .line 107
    sget-object v9, Lcom/sec/android/app/myfiles/ftp/FTPErr;->PARTIALLY_DONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    iput-object v9, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 111
    :cond_2
    iget-object v9, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    sget-object v10, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    if-ne v9, v10, :cond_7

    .line 113
    iget-object v9, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mDstFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    iget-object v10, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mDownloadTempDir:Ljava/lang/String;

    iget-object v11, v7, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    invoke-virtual {p0, v9, v5, v10, v11}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->createDirectoriesOnFtp(Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 115
    .local v0, "dirCreated":I
    if-eq v0, v6, :cond_3

    .line 117
    sget-object v9, Lcom/sec/android/app/myfiles/ftp/FTPErr;->PARTIALLY_DONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    iput-object v9, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 121
    :cond_3
    iget-object v9, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    sget-object v10, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    if-ne v9, v10, :cond_7

    .line 123
    iget-object v9, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mDstFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    iget-object v10, v7, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    iget-object v11, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mDownloadTempDir:Ljava/lang/String;

    invoke-virtual {p0, v9, v2, v10, v11}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->uploadFiles(Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 125
    .local v4, "filesUploaded":I
    invoke-static {v5, v2}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->deleteItemsOnLocal([Ljava/lang/String;[Ljava/lang/String;)I

    .line 127
    iget-object v9, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    sget-object v10, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    if-ne v9, v10, :cond_4

    iget-boolean v9, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mMove:Z

    if-eqz v9, :cond_4

    .line 129
    iget-object v9, v8, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mFolders:Ljava/util/Stack;

    iget-object v10, v8, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mFiles:Ljava/util/Stack;

    iget-object v11, v7, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mSrcFolder:Ljava/lang/String;

    iget-object v12, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mDownloadTempDir:Ljava/lang/String;

    invoke-virtual {p0, v9, v10, v11, v12}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->deleteItemsOnFtp(Ljava/util/Stack;Ljava/util/Stack;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 133
    :cond_4
    array-length v9, v2

    if-ne v4, v9, :cond_5

    if-eq v0, v6, :cond_6

    .line 135
    :cond_5
    sget-object v9, Lcom/sec/android/app/myfiles/ftp/FTPErr;->PARTIALLY_DONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    iput-object v9, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 139
    :cond_6
    iget-object v9, v7, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    iput v9, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mDoneCount:I

    .line 149
    .end local v0    # "dirCreated":I
    .end local v1    # "downloadedItems":I
    .end local v2    # "files":[Ljava/lang/String;
    .end local v3    # "filesToDownload":I
    .end local v4    # "filesUploaded":I
    .end local v5    # "folders":[Ljava/lang/String;
    .end local v6    # "foldersToCreate":I
    .end local v7    # "param":Lcom/sec/android/app/myfiles/element/FileOperationParam;
    .end local v8    # "res":Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;
    :cond_7
    const/4 v9, 0x0

    return-object v9
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 33
    check-cast p1, [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V
    .locals 4
    .param p1, "result"    # Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .prologue
    const/16 v3, 0xa

    .line 156
    sget-object v1, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation$1;->$SwitchMap$com$sec$android$app$myfiles$ftp$FTPErr:[I

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPErr;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 194
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    if-eqz v1, :cond_0

    .line 196
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mDoneCount:I

    invoke-interface {v1, v2, v3}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;->onCancelled(Ljava/util/ArrayList;I)V

    .line 200
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mMove:Z

    if-eqz v1, :cond_5

    .line 202
    const v1, 0x7f0b00cb

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->showToast(I)V

    .line 214
    :goto_0
    return-void

    .line 160
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    if-eqz v1, :cond_2

    .line 162
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 164
    .local v0, "arg":Landroid/os/Bundle;
    const-string v2, "FILE_OPERATION"

    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mMove:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 166
    const-string v1, "src_fragment_id"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 168
    const-string v1, "dest_fragment_id"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 170
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mParam:Lcom/sec/android/app/myfiles/element/FileOperationParam;

    if-eqz v1, :cond_1

    .line 172
    const-string v1, "target_folder"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mParam:Lcom/sec/android/app/myfiles/element/FileOperationParam;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    iget v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mDoneCount:I

    iget v3, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mDoneCount:I

    invoke-interface {v1, v2, v3, v0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;->onCompleted(IILandroid/os/Bundle;)V

    .line 180
    .end local v0    # "arg":Landroid/os/Bundle;
    :cond_2
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mMove:Z

    if-eqz v1, :cond_4

    .line 182
    const v1, 0x7f0c0008

    iget v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mDoneCount:I

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->showToastPrurals(II)V

    goto :goto_0

    .line 164
    .restart local v0    # "arg":Landroid/os/Bundle;
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 186
    .end local v0    # "arg":Landroid/os/Bundle;
    :cond_4
    const v1, 0x7f0c0005

    iget v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mDoneCount:I

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->showToastPrurals(II)V

    goto :goto_0

    .line 206
    :cond_5
    const v1, 0x7f0b00c9

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->showToast(I)V

    goto :goto_0

    .line 156
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 33
    check-cast p1, Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V

    return-void
.end method

.method protected setupProgressView()V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveFtpToFtpOperation;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 64
    return-void
.end method
