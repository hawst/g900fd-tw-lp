.class public Lcom/sec/android/app/myfiles/utils/Security;
.super Ljava/lang/Object;
.source "Security.java"


# static fields
.field private static final ALGORITHM:Ljava/lang/String; = "PBEWithSHA256And256BitAES-CBC-BC"

.field private static final ITERATION_COUNT:I = 0xc8

.field private static final MODULE:Ljava/lang/String;

.field private static final RELAY:[C

.field private static final UTF8:Ljava/lang/String; = "UTF8"

.field private static volatile instance:Lcom/sec/android/app/myfiles/utils/Security;


# instance fields
.field private mDCipher:Ljavax/crypto/Cipher;

.field private mECipher:Ljavax/crypto/Cipher;

.field private mSalt:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 138
    const-string v0, "hI*&thI87g6o7AP(*D&2"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Security;->RELAY:[C

    .line 148
    const-class v0, Lcom/sec/android/app/myfiles/utils/Security;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Security;->MODULE:Ljava/lang/String;

    .line 150
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Security;->instance:Lcom/sec/android/app/myfiles/utils/Security;

    return-void
.end method

.method private constructor <init>()V
    .locals 8

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    const/16 v5, 0x8

    new-array v5, v5, [B

    fill-array-data v5, :array_0

    iput-object v5, p0, Lcom/sec/android/app/myfiles/utils/Security;->mSalt:[B

    .line 56
    :try_start_0
    const-string v5, "PBEWithSHA256And256BitAES-CBC-BC"

    invoke-static {v5}, Ljavax/crypto/SecretKeyFactory;->getInstance(Ljava/lang/String;)Ljavax/crypto/SecretKeyFactory;

    move-result-object v4

    .line 58
    .local v4, "secretKeyFactory":Ljavax/crypto/SecretKeyFactory;
    new-instance v2, Ljavax/crypto/spec/PBEKeySpec;

    sget-object v5, Lcom/sec/android/app/myfiles/utils/Security;->RELAY:[C

    invoke-direct {v2, v5}, Ljavax/crypto/spec/PBEKeySpec;-><init>([C)V

    .line 60
    .local v2, "keySpec":Ljavax/crypto/spec/PBEKeySpec;
    invoke-virtual {v4, v2}, Ljavax/crypto/SecretKeyFactory;->generateSecret(Ljava/security/spec/KeySpec;)Ljavax/crypto/SecretKey;

    move-result-object v1

    .line 62
    .local v1, "key":Ljavax/crypto/SecretKey;
    invoke-interface {v1}, Ljavax/crypto/SecretKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/myfiles/utils/Security;->mECipher:Ljavax/crypto/Cipher;

    .line 64
    invoke-interface {v1}, Ljavax/crypto/SecretKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/myfiles/utils/Security;->mDCipher:Ljavax/crypto/Cipher;

    .line 66
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/utils/Security;->getAlgorithmParameterSpec()Ljava/security/spec/AlgorithmParameterSpec;

    move-result-object v3

    .line 68
    .local v3, "paramSpec":Ljava/security/spec/AlgorithmParameterSpec;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/utils/Security;->mECipher:Ljavax/crypto/Cipher;

    const/4 v6, 0x1

    invoke-virtual {v5, v6, v1, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 70
    iget-object v5, p0, Lcom/sec/android/app/myfiles/utils/Security;->mDCipher:Ljavax/crypto/Cipher;

    const/4 v6, 0x2

    invoke-virtual {v5, v6, v1, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    .end local v1    # "key":Ljavax/crypto/SecretKey;
    .end local v2    # "keySpec":Ljavax/crypto/spec/PBEKeySpec;
    .end local v3    # "paramSpec":Ljava/security/spec/AlgorithmParameterSpec;
    .end local v4    # "secretKeyFactory":Ljavax/crypto/SecretKeyFactory;
    :goto_0
    return-void

    .line 72
    :catch_0
    move-exception v0

    .line 74
    .local v0, "e":Ljava/lang/Exception;
    const/4 v5, 0x0

    sget-object v6, Lcom/sec/android/app/myfiles/utils/Security;->MODULE:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 141
    :array_0
    .array-data 1
        0x1at
        -0x3ct
        -0x1dt
        0x66t
        -0x51t
        0x23t
        -0x1dt
        0x55t
    .end array-data
.end method

.method private getAlgorithmParameterSpec()Ljava/security/spec/AlgorithmParameterSpec;
    .locals 3

    .prologue
    .line 126
    new-instance v0, Ljavax/crypto/spec/PBEParameterSpec;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/utils/Security;->mSalt:[B

    const/16 v2, 0xc8

    invoke-direct {v0, v1, v2}, Ljavax/crypto/spec/PBEParameterSpec;-><init>([BI)V

    return-object v0
.end method

.method public static getInstance()Lcom/sec/android/app/myfiles/utils/Security;
    .locals 2

    .prologue
    .line 34
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Security;->instance:Lcom/sec/android/app/myfiles/utils/Security;

    if-nez v0, :cond_1

    .line 36
    const-class v1, Lcom/sec/android/app/myfiles/utils/Security;

    monitor-enter v1

    .line 38
    :try_start_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Security;->instance:Lcom/sec/android/app/myfiles/utils/Security;

    if-nez v0, :cond_0

    .line 40
    new-instance v0, Lcom/sec/android/app/myfiles/utils/Security;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/utils/Security;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/Security;->instance:Lcom/sec/android/app/myfiles/utils/Security;

    .line 44
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    :cond_1
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Security;->instance:Lcom/sec/android/app/myfiles/utils/Security;

    return-object v0

    .line 44
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public decrypt(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "phrase"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 106
    if-eqz p1, :cond_0

    const/4 v2, 0x0

    :try_start_0
    invoke-static {p1, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 108
    .local v0, "bytes":[B
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/utils/Security;->mDCipher:Ljavax/crypto/Cipher;

    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    :try_start_1
    new-instance v2, Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/utils/Security;->mDCipher:Ljavax/crypto/Cipher;

    invoke-virtual {v4, v0}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v4

    const-string v5, "UTF8"

    invoke-direct {v2, v4, v5}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v2

    .line 106
    .end local v0    # "bytes":[B
    :cond_0
    const/4 v2, 0x0

    :try_start_2
    new-array v0, v2, [B
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 112
    .restart local v0    # "bytes":[B
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 114
    .end local v0    # "bytes":[B
    :catch_0
    move-exception v1

    .line 116
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/android/app/myfiles/utils/Security;->MODULE:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 118
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public encrypt(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "phrase"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 84
    if-eqz p1, :cond_0

    :try_start_0
    const-string v2, "UTF8"

    invoke-virtual {p1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 86
    .local v0, "bytes":[B
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/utils/Security;->mECipher:Ljavax/crypto/Cipher;

    monitor-enter v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    :try_start_1
    new-instance v2, Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/utils/Security;->mECipher:Ljavax/crypto/Cipher;

    invoke-virtual {v4, v0}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v4

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Base64;->encode([BI)[B

    move-result-object v4

    const-string v5, "UTF8"

    invoke-direct {v2, v4, v5}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v2

    .line 84
    .end local v0    # "bytes":[B
    :cond_0
    const/4 v2, 0x0

    :try_start_2
    new-array v0, v2, [B
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 90
    .restart local v0    # "bytes":[B
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 92
    .end local v0    # "bytes":[B
    :catch_0
    move-exception v1

    .line 94
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/android/app/myfiles/utils/Security;->MODULE:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 96
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method
