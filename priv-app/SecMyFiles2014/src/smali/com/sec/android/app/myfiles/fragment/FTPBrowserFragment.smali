.class public Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;
.source "FTPBrowserFragment.java"


# static fields
.field private static final DISCONNECT_ON_CLOSE:Z = false

.field private static final DOWNLOAD_FOLDER:Ljava/lang/String; = "\"Download\""

.field private static final MODULE:Ljava/lang/String;

.field private static final SHORTCUT_DELIMITER:Ljava/lang/String; = ":"


# instance fields
.field private final mBrowseDoneReceiver:Landroid/content/BroadcastReceiver;

.field private final mBrowseHandler:Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;

.field private mFromSelector:Z

.field private mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

.field private mPathChangeListener:Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;

.field private mPrefix:Ljava/lang/String;

.field private mPreviousPath:Ljava/lang/String;

.field private mPreviousSelectedItemPositions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSelector:Z

.field private mSettingDefaultView:Z

.field private mShortcutWorkingIndex:I

.field private volatile mSwitchOnBack:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2176
    const-class v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->MODULE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 87
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;-><init>()V

    .line 89
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mFromSelector:Z

    .line 2064
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$9;-><init>(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPathChangeListener:Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;

    .line 2093
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$10;-><init>(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mBrowseDoneReceiver:Landroid/content/BroadcastReceiver;

    .line 2167
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSwitchOnBack:Z

    .line 2168
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSettingDefaultView:Z

    .line 2170
    const-string v0, "rroot"

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrefix:Ljava/lang/String;

    .line 2172
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mShortcutWorkingIndex:I

    .line 2173
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSelector:Z

    .line 2177
    invoke-static {}, Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;->getInstance()Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mBrowseHandler:Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPreviousPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPreviousPath:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->setParentActDefaultView()V

    return-void
.end method

.method static synthetic access$201(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;ILjava/lang/String;Ljava/util/ArrayList;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/util/ArrayList;
    .param p4, "x4"    # Z

    .prologue
    .line 87
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->handleOnDropFromFtp(ILjava/lang/String;Ljava/util/ArrayList;Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrefix:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->MODULE:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSelector:Z

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 87
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSwitchOnBack:Z

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;Lcom/sec/android/app/myfiles/ftp/FTPItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;
    .param p1, "x1"    # Lcom/sec/android/app/myfiles/ftp/FTPItem;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->downloadAndOpen(Lcom/sec/android/app/myfiles/ftp/FTPItem;)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mBrowseHandler:Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;

    return-object v0
.end method

.method private close()V
    .locals 0

    .prologue
    .line 1990
    return-void
.end method

.method private downloadAndOpen(Lcom/sec/android/app/myfiles/ftp/FTPItem;)V
    .locals 7
    .param p1, "item"    # Lcom/sec/android/app/myfiles/ftp/FTPItem;

    .prologue
    const/4 v6, 0x1

    .line 1634
    if-eqz p1, :cond_0

    .line 1636
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getSessionName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {p1, v4, v5}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getTempPath(Lcom/sec/android/app/myfiles/ftp/IFTPItem;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 1639
    .local v3, "targetDir":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v4, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    invoke-virtual {v4, p1}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->isFileCached(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1641
    const/4 v4, 0x0

    invoke-static {v4, v6, v6}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(IZZ)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v1

    .line 1643
    .local v1, "downloadFragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    const/16 v4, 0xb

    invoke-virtual {v1, p0, v4}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 1645
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1647
    .local v2, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1650
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 1652
    .local v0, "args":Landroid/os/Bundle;
    const-string v4, "dst_folder"

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1654
    const-string v4, "target_datas"

    invoke-virtual {v0, v4, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1656
    const-string v4, "src_fragment_id"

    const/16 v5, 0xa

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1658
    const-string v4, "dest_fragment_id"

    const/16 v5, 0x201

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1660
    const-string v4, "src-ftp-param"

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1663
    const-string v4, "cacheDownload"

    invoke-virtual {p0, v1, v4}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->showDialogFragment(Landroid/app/DialogFragment;Ljava/lang/String;)V

    .line 1674
    .end local v0    # "args":Landroid/os/Bundle;
    .end local v1    # "downloadFragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    .end local v2    # "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v3    # "targetDir":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 1668
    .restart local v3    # "targetDir":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->openFile(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getDetailsFragment(I)Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;
    .locals 1
    .param p1, "itemsSelected"    # I

    .prologue
    const/4 v0, 0x1

    .line 696
    if-ne p1, v0, :cond_0

    .line 697
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->onCreateDetailFragmentListener()Landroid/app/DialogFragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;

    .line 701
    :goto_0
    return-object v0

    .line 698
    :cond_0
    if-le p1, v0, :cond_1

    .line 699
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->onCreateMultipleDetailFragmentListener()Landroid/app/DialogFragment;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;

    goto :goto_0

    .line 701
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private refreshCurrentFolderContent(II)V
    .locals 1
    .param p1, "resultCode"    # I
    .param p2, "fileOperation"    # I

    .prologue
    .line 1122
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v0, :cond_0

    .line 1124
    packed-switch p1, :pswitch_data_0

    .line 1151
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->AsyncOpen()V

    .line 1163
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1128
    :pswitch_1
    packed-switch p2, :pswitch_data_1

    .line 1137
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->AsyncOpen()V

    goto :goto_0

    .line 1124
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
    .end packed-switch

    .line 1128
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private setParentActDefaultView()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1251
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1252
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1253
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "shortcut_working_index_intent_key"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "shortcut_working_index_intent_key"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    :cond_0
    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1254
    const-string v1, "id"

    const/16 v2, 0x1c

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1255
    const-string v1, "FOLDERPATH"

    const-string v2, "/"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1256
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView(Landroid/os/Bundle;)V

    .line 1261
    .end local v0    # "b":Landroid/os/Bundle;
    :goto_0
    return-void

    .line 1259
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView()V

    goto :goto_0
.end method

.method protected static trimSessionName(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "raw"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 1955
    if-eqz p0, :cond_3

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    .line 1956
    const-string v3, ":"

    invoke-virtual {p0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1957
    .local v2, "split":[Ljava/lang/String;
    array-length v3, v2

    if-gt v3, v4, :cond_0

    .line 1970
    .end local v2    # "split":[Ljava/lang/String;
    .end local p0    # "raw":Ljava/lang/String;
    :goto_0
    return-object p0

    .line 1960
    .restart local v2    # "split":[Ljava/lang/String;
    .restart local p0    # "raw":Ljava/lang/String;
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1961
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_2

    .line 1962
    aget-object v3, v2, v0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1963
    aget-object v3, v2, v0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v4, :cond_1

    .line 1964
    const-string v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1961
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1967
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 1970
    .end local v0    # "i":I
    .end local v1    # "sb":Ljava/lang/StringBuilder;
    .end local v2    # "split":[Ljava/lang/String;
    :cond_3
    const-string p0, ""

    goto :goto_0
.end method


# virtual methods
.method public AsyncOpenCompleted()V
    .locals 2

    .prologue
    .line 1786
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v0

    .line 1787
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 1788
    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 1789
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->updatePathIndicator()V

    .line 1790
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->invalidateOptionsMenu()V

    .line 1791
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->restorePreviousSelection()V

    .line 1793
    :cond_0
    return-void
.end method

.method public addShortcutToHome()V
    .locals 10

    .prologue
    .line 2257
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->addShortcutToHome()V

    .line 2259
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v8, v8, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v8, :cond_6

    .line 2260
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v8, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v8, v8, Lcom/sec/android/app/myfiles/MainActivity;->mAddShortcutFromActionmode:Z

    if-eqz v8, :cond_2

    .line 2261
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v8, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->getSelectedItemForAddshortcut()Ljava/util/ArrayList;

    move-result-object v2

    .line 2262
    .local v2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .local v1, "item":Ljava/lang/Object;
    move-object v8, v1

    .line 2263
    check-cast v8, Lcom/sec/android/app/myfiles/ftp/FTPItem;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v3

    .line 2264
    .local v3, "path":Ljava/lang/String;
    check-cast v1, Lcom/sec/android/app/myfiles/ftp/FTPItem;

    .end local v1    # "item":Ljava/lang/Object;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getTitle()Ljava/lang/String;

    move-result-object v7

    .line 2265
    .local v7, "title":Ljava/lang/String;
    sget-char v8, Ljava/io/File;->separatorChar:C

    invoke-virtual {v3, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v3, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 2267
    .local v6, "shortcutName":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 2269
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 2284
    .local v4, "sb":Ljava/lang/StringBuilder;
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v8, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getCurFolder()Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 2285
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v8, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getCurFolder()Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-result-object v8

    invoke-interface {v8}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2290
    :cond_1
    new-instance v5, Lcom/sec/android/app/myfiles/ftp/FTPParams;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-direct {v5, v8, v7, v3}, Lcom/sec/android/app/myfiles/ftp/FTPParams;-><init>(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Ljava/lang/String;)V

    .line 2292
    .local v5, "shortcut":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2293
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v8, v3, v6}, Lcom/sec/android/app/myfiles/utils/Utils;->addShortcutToHome(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2298
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v3    # "path":Ljava/lang/String;
    .end local v4    # "sb":Ljava/lang/StringBuilder;
    .end local v5    # "shortcut":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .end local v6    # "shortcutName":Ljava/lang/String;
    .end local v7    # "title":Ljava/lang/String;
    :cond_2
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v3

    .line 2300
    .restart local v3    # "path":Ljava/lang/String;
    sget-char v8, Ljava/io/File;->separatorChar:C

    invoke-virtual {v3, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v3, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 2302
    .restart local v6    # "shortcutName":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 2304
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 2319
    .restart local v4    # "sb":Ljava/lang/StringBuilder;
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v8, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getCurFolder()Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-result-object v8

    if-eqz v8, :cond_3

    .line 2320
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v8, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getCurFolder()Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-result-object v8

    invoke-interface {v8}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getTitle()Ljava/lang/String;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 2321
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getSessionName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2329
    :cond_3
    :goto_1
    new-instance v5, Lcom/sec/android/app/myfiles/ftp/FTPParams;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v5, v8, v9, v3}, Lcom/sec/android/app/myfiles/ftp/FTPParams;-><init>(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Ljava/lang/String;)V

    .line 2330
    .restart local v5    # "shortcut":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    const-string v8, ""

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2331
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getSessionName()Ljava/lang/String;

    move-result-object v6

    .line 2334
    :cond_4
    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2335
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v8, v3, v6}, Lcom/sec/android/app/myfiles/utils/Utils;->addShortcutToHome(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 2342
    .end local v3    # "path":Ljava/lang/String;
    .end local v4    # "sb":Ljava/lang/StringBuilder;
    .end local v5    # "shortcut":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .end local v6    # "shortcutName":Ljava/lang/String;
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v8

    if-eqz v8, :cond_6

    .line 2344
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v8

    const/16 v9, 0x1a

    invoke-virtual {v8, v9}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->refreshAdapter(I)V

    .line 2349
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->isSelectMode()Z

    move-result v8

    if-eqz v8, :cond_7

    .line 2350
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->finishSelectMode()V

    .line 2352
    :cond_7
    return-void

    .line 2323
    .restart local v3    # "path":Ljava/lang/String;
    .restart local v4    # "sb":Ljava/lang/StringBuilder;
    .restart local v6    # "shortcutName":Ljava/lang/String;
    :cond_8
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v8, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getCurFolder()Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-result-object v8

    invoke-interface {v8}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public addShortcutToMyfiles(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 9
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "size"    # I

    .prologue
    const/4 v5, 0x0

    const/16 v3, 0x1c

    const/4 v6, 0x1

    .line 2354
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v0, :cond_2

    .line 2356
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2358
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 2373
    .local v7, "sb":Ljava/lang/StringBuilder;
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getCurFolder()Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2374
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getCurFolder()Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2375
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getSessionName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2384
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mAddShortcutFromActionmode:Z

    if-eqz v0, :cond_4

    .line 2385
    new-instance v8, Lcom/sec/android/app/myfiles/ftp/FTPParams;

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-direct {v8, v0, p2, p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;-><init>(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Ljava/lang/String;)V

    .line 2390
    .local v8, "shortcut":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    :goto_1
    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getSessionName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2391
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getSessionName()Ljava/lang/String;

    move-result-object v2

    .line 2396
    .local v2, "shortcutName":Ljava/lang/String;
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mAddShortcutFromActionmode:Z

    if-eqz v0, :cond_6

    if-le p3, v6, :cond_6

    .line 2397
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->toString()Ljava/lang/String;

    move-result-object v1

    iget v4, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mShortcutWorkingIndex:I

    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/myfiles/utils/Utils;->addShortCut(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/drawable/Drawable;Z)V

    .line 2403
    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2405
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v0

    const/16 v1, 0x1a

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->refreshAdapter(I)V

    .line 2408
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;

    if-eqz v0, :cond_2

    .line 2415
    .end local v2    # "shortcutName":Ljava/lang/String;
    .end local v7    # "sb":Ljava/lang/StringBuilder;
    .end local v8    # "shortcut":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    :cond_2
    return-void

    .line 2377
    .restart local v7    # "sb":Ljava/lang/StringBuilder;
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getCurFolder()Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 2387
    :cond_4
    new-instance v8, Lcom/sec/android/app/myfiles/ftp/FTPParams;

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v8, v0, v1, p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;-><init>(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Ljava/lang/String;)V

    .restart local v8    # "shortcut":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    goto :goto_1

    .line 2393
    :cond_5
    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getSessionName()Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "shortcutName":Ljava/lang/String;
    goto :goto_2

    .line 2400
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->toString()Ljava/lang/String;

    move-result-object v1

    iget v4, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mShortcutWorkingIndex:I

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/myfiles/utils/Utils;->addShortCut(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IILandroid/graphics/drawable/Drawable;Z)V

    goto :goto_3
.end method

.method protected appendCustomArgsToStartCopyOrMoveArgs(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "arg"    # Landroid/os/Bundle;

    .prologue
    .line 1508
    if-eqz p1, :cond_0

    .line 1510
    const-string v0, "src-ftp-param"

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1514
    :cond_0
    return-void
.end method

.method public deleteSelectedItem()V
    .locals 11

    .prologue
    const/4 v8, 0x2

    const/4 v10, 0x0

    .line 1459
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v6

    .line 1461
    .local v6, "selectedItems":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1463
    .local v4, "itemsToDelete":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 1465
    .local v5, "o":Ljava/lang/Object;
    check-cast v5, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .end local v5    # "o":Ljava/lang/Object;
    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1469
    :cond_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_1

    .line 1471
    invoke-static {v8, v10}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(IZ)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v1

    .line 1473
    .local v1, "deleteFragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    invoke-virtual {v1, p0, v8}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 1475
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 1477
    .local v0, "args":Landroid/os/Bundle;
    const-string v7, "target_datas"

    invoke-virtual {v0, v7, v4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1479
    const-string v7, "src_fragment_id"

    const/16 v8, 0xa

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1481
    const-string v7, "src-ftp-param"

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1485
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    const-string v8, "createFolder"

    invoke-virtual {v1, v7, v8}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1497
    .end local v0    # "args":Landroid/os/Bundle;
    .end local v1    # "deleteFragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    :cond_1
    :goto_1
    iget-boolean v7, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSelector:Z

    if-nez v7, :cond_2

    .line 1499
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->finishSelectMode()V

    .line 1503
    :cond_2
    return-void

    .line 1487
    .restart local v0    # "args":Landroid/os/Bundle;
    .restart local v1    # "deleteFragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    :catch_0
    move-exception v2

    .line 1491
    .local v2, "e":Ljava/lang/Exception;
    sget-object v7, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->MODULE:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "dialog has not been shown Exception: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v10, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public finishSelectMode()V
    .locals 1

    .prologue
    .line 1334
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getSelectionType()I

    move-result v0

    .line 1336
    .local v0, "selectionType":I
    packed-switch v0, :pswitch_data_0

    .line 1344
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->finishSelectMode()V

    .line 1350
    :pswitch_0
    return-void

    .line 1336
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected getDragFileIcon(Ljava/lang/String;)I
    .locals 2
    .param p1, "itemPath"    # Ljava/lang/String;

    .prologue
    .line 1858
    invoke-static {p1}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getItem(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPItem;

    move-result-object v0

    .line 1860
    .local v0, "item":Lcom/sec/android/app/myfiles/ftp/FTPItem;
    if-eqz v0, :cond_0

    .line 1862
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/myfiles/MediaFile;->getSmallIcon(Ljava/lang/String;)I

    move-result v1

    .line 1866
    :goto_0
    return v1

    :cond_0
    const v1, 0x7f0200bd

    goto :goto_0
.end method

.method protected getDragFileText(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "itemPath"    # Ljava/lang/String;

    .prologue
    .line 1875
    invoke-static {p1}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getItem(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPItem;

    move-result-object v0

    .line 1877
    .local v0, "item":Lcom/sec/android/app/myfiles/ftp/FTPItem;
    if-eqz v0, :cond_0

    .line 1879
    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 1883
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public getOptionsMenuResId(Z)I
    .locals 2
    .param p1, "isSelectMode"    # Z

    .prologue
    const v0, 0x7f0e0018

    .line 1769
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSelector:Z

    if-eqz v1, :cond_1

    .line 1779
    :cond_0
    :goto_0
    return v0

    .line 1772
    :cond_1
    if-eqz p1, :cond_2

    .line 1773
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getSelectModeFrom()I

    move-result v1

    if-nez v1, :cond_0

    .line 1774
    const v0, 0x7f0e000d

    goto :goto_0

    .line 1779
    :cond_2
    const v0, 0x7f0e000c

    goto :goto_0
.end method

.method protected getSelectedItemsPathAsIntent(Ljava/util/ArrayList;)Landroid/content/Intent;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 1266
    .local p1, "selectedItemPos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectedItemsPathAsIntent(Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    .line 1268
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 1270
    const-string v1, "dst-ftp-param"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1274
    :cond_0
    return-object v0
.end method

.method protected getSelectedPaths()[Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1517
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v1

    .line 1518
    .local v1, "selectedItems":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    if-nez v1, :cond_1

    .line 1528
    :cond_0
    return-object v2

    .line 1520
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    .line 1521
    .local v3, "size":I
    if-lez v3, :cond_0

    .line 1522
    new-array v2, v3, [Ljava/lang/String;

    .line 1523
    .local v2, "selectedPaths":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 1524
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/myfiles/element/BrowserItem;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getPath()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v0

    .line 1523
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public gotoFolder(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .param p2, "arg"    # Landroid/os/Bundle;

    .prologue
    const/16 v8, 0x15

    const/4 v3, 0x0

    const/4 v5, -0x1

    const/4 v7, 0x1

    .line 103
    const/4 v1, 0x0

    .line 105
    .local v1, "returnPath":Ljava/lang/String;
    if-eqz p2, :cond_0

    const-string v2, "selection_type"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v7, :cond_0

    .line 106
    const-string v2, "run_from"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v8, :cond_0

    .line 108
    const-string v2, "selection_type"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p0, v2, v5, v7}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->startSelectMode(III)V

    .line 113
    :cond_0
    if-eqz p2, :cond_3

    .line 115
    const-string v2, "from_back"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "from_back"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 118
    .local v0, "fromBackPress":Z
    :goto_0
    if-eqz v0, :cond_a

    .line 120
    const-string v2, "src-ftp-param"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 122
    new-instance v2, Lcom/sec/android/app/myfiles/ftp/FTPParams;

    const-string v4, "src-ftp-param"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/sec/android/app/myfiles/ftp/FTPParams;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .line 124
    const-string v2, "FOLDERPATH"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 150
    :goto_1
    const-string v2, "selection_type"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 151
    iput-boolean v7, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSelector:Z

    .line 154
    :cond_1
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSelector:Z

    if-eqz v2, :cond_2

    .line 156
    const-string v2, "shortcut_working_index_intent_key"

    invoke-virtual {p2, v2, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mShortcutWorkingIndex:I

    .line 161
    :cond_2
    invoke-virtual {p0, p2}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->setPathPrefix(Landroid/os/Bundle;)V

    .line 165
    .end local v0    # "fromBackPress":Z
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSelectedItemChangeListener:Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;

    if-eqz v2, :cond_4

    .line 166
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSelectedItemChangeListener:Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->setSelectedItemChangeListener(Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;)V

    .line 169
    :cond_4
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPreviousPath:Ljava/lang/String;

    .line 171
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 172
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .line 175
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mBrowseDoneReceiver:Landroid/content/BroadcastReceiver;

    new-instance v5, Landroid/content/IntentFilter;

    const-string v6, "com.sec.android.app.myfiles.ftp.BACKGROUND_BROWSE_DONE"

    invoke-direct {v5, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/app/myfiles/AbsMainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 178
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v2, p0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->setSupportAsync(Lcom/sec/android/app/myfiles/fragment/AbsFragment;)V

    .line 179
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v2, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->initializeSupplier(Lcom/sec/android/app/myfiles/ftp/FTPParams;)V

    .line 180
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v2, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->setEmptyCurrentFolder(Z)V

    .line 181
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v2, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    new-instance v3, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->setGoPreviousCallback(Ljava/lang/Runnable;)V

    .line 191
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v2, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    new-instance v3, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$2;-><init>(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->setCloseFragmentCallback(Ljava/lang/Runnable;)V

    .line 200
    if-nez p1, :cond_6

    .line 202
    if-nez v1, :cond_d

    .line 203
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getStartPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-gt v2, v7, :cond_c

    .line 204
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v2, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    invoke-virtual {v2, v7}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->goToRoot(Z)Z

    .line 215
    :cond_6
    :goto_2
    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mRunFrom:I

    if-ne v2, v8, :cond_7

    .line 216
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->selectmodeActionbar()V

    .line 218
    :cond_7
    return-void

    :cond_8
    move v0, v3

    .line 115
    goto/16 :goto_0

    .line 128
    .restart local v0    # "fromBackPress":Z
    :cond_9
    new-instance v2, Lcom/sec/android/app/myfiles/ftp/FTPParams;

    const-string v4, "FOLDERPATH"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/sec/android/app/myfiles/ftp/FTPParams;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    goto/16 :goto_1

    .line 134
    :cond_a
    const-string v2, "dst-ftp-param"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 136
    new-instance v2, Lcom/sec/android/app/myfiles/ftp/FTPParams;

    const-string v4, "dst-ftp-param"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/sec/android/app/myfiles/ftp/FTPParams;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    .line 138
    const-string v2, "FOLDERPATH"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 140
    iput-boolean v7, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSwitchOnBack:Z

    goto/16 :goto_1

    .line 144
    :cond_b
    new-instance v2, Lcom/sec/android/app/myfiles/ftp/FTPParams;

    const-string v4, "FOLDERPATH"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/sec/android/app/myfiles/ftp/FTPParams;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    goto/16 :goto_1

    .line 206
    .end local v0    # "fromBackPress":Z
    :cond_c
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v2, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getStartPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v7}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->goTo(Ljava/lang/String;Z)Z

    goto :goto_2

    .line 210
    :cond_d
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v2, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    invoke-virtual {v2, v1, v7}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->goTo(Ljava/lang/String;Z)Z

    goto :goto_2
.end method

.method public handleAddShortcutRequest()V
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 1891
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->handleAddShortcutRequest()V

    .line 1893
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v5, v5, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v5, :cond_7

    .line 1894
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v5, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mAddShortcutFromActionmode:Z

    if-eqz v5, :cond_6

    .line 1895
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v5, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->getSelectedItemForAddshortcut()Ljava/util/ArrayList;

    move-result-object v2

    .line 1896
    .local v2, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .local v1, "item":Ljava/lang/Object;
    move-object v5, v1

    .line 1897
    check-cast v5, Lcom/sec/android/app/myfiles/ftp/FTPItem;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v3

    .line 1898
    .local v3, "path":Ljava/lang/String;
    check-cast v1, Lcom/sec/android/app/myfiles/ftp/FTPItem;

    .end local v1    # "item":Ljava/lang/Object;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getTitle()Ljava/lang/String;

    move-result-object v4

    .line 1899
    .local v4, "title":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {p0, v3, v4, v5}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->addShortcutToMyfiles(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 1901
    .end local v3    # "path":Ljava/lang/String;
    .end local v4    # "title":Ljava/lang/String;
    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-le v5, v7, :cond_1

    .line 1902
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v5

    sget v6, Lcom/sec/android/app/myfiles/utils/Utils;->mAddShortcutSuccess:I

    if-ne v5, v6, :cond_3

    .line 1903
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v6, 0x7f0b0060

    invoke-static {v5, v6, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    .line 1922
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_1
    :goto_1
    const-string v5, "MyFiles"

    const-string v6, "FTPBrowserFragment: add shortcut request"

    invoke-static {v8, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 1924
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->isSelectMode()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1925
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->finishSelectMode()V

    .line 1928
    :cond_2
    return-void

    .line 1905
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v2    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_3
    sget v5, Lcom/sec/android/app/myfiles/utils/Utils;->mAddShortcutSuccess:I

    if-ne v5, v7, :cond_4

    .line 1906
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v6, "1 shortcut added, Existing shortcuts were excluded"

    invoke-static {v5, v6, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 1907
    :cond_4
    sget v5, Lcom/sec/android/app/myfiles/utils/Utils;->mAddShortcutSuccess:I

    if-nez v5, :cond_5

    .line 1908
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v6, "Shortcuts already exist."

    invoke-static {v5, v6, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 1910
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget v7, Lcom/sec/android/app/myfiles/utils/Utils;->mAddShortcutSuccess:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " shortcuts added, Existing shortcuts were excluded"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 1915
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v3

    .line 1916
    .restart local v3    # "path":Ljava/lang/String;
    const-string v5, ""

    invoke-virtual {p0, v3, v5, v8}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->addShortcutToMyfiles(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_1

    .line 1918
    .end local v3    # "path":Ljava/lang/String;
    :cond_7
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v5, v5, Lcom/sec/android/app/myfiles/SelectorActivity;

    if-eqz v5, :cond_1

    .line 1919
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v3

    .line 1920
    .restart local v3    # "path":Ljava/lang/String;
    const-string v5, ""

    invoke-virtual {p0, v3, v5, v8}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->addShortcutToMyfiles(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_1
.end method

.method protected handleCopyMoveOperations(IIZ[Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/FTPParams;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;Z)V
    .locals 14
    .param p1, "fromFragmentId"    # I
    .param p2, "toFragmentId"    # I
    .param p3, "move"    # Z
    .param p4, "srcPaths"    # [Ljava/lang/String;
    .param p5, "targetPath"    # Ljava/lang/String;
    .param p6, "srcParams"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p7, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;
    .param p8, "fromMultiWindow"    # Z

    .prologue
    .line 727
    sparse-switch p1, :sswitch_data_0

    .line 782
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v4, "Unsupported yet."

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 785
    :cond_0
    :goto_0
    return-void

    .line 734
    :sswitch_0
    const/16 v3, 0xa

    move/from16 v0, p2

    if-ne v0, v3, :cond_0

    .line 735
    if-eqz p3, :cond_1

    .line 736
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v3, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    move-object/from16 v0, p4

    move-object/from16 v1, p5

    move-object/from16 v2, p7

    invoke-virtual {v3, v0, v1, v2}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->moveFromLocalToFtp([Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    goto :goto_0

    .line 738
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v3, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    move-object/from16 v0, p4

    move-object/from16 v1, p5

    move-object/from16 v2, p7

    invoke-virtual {v3, v0, v1, v2}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->copyFromLocalToFtp([Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    goto :goto_0

    .line 743
    :sswitch_1
    sparse-switch p2, :sswitch_data_1

    goto :goto_0

    .line 750
    :sswitch_2
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 751
    .local v13, "srcPathsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v9, p4

    .local v9, "arr$":[Ljava/lang/String;
    array-length v11, v9

    .local v11, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    :goto_1
    if-ge v10, v11, :cond_2

    aget-object v12, v9, v10

    .line 752
    .local v12, "path":Ljava/lang/String;
    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 751
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 754
    .end local v12    # "path":Ljava/lang/String;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v3, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    move-object/from16 v0, p5

    move/from16 v1, p3

    move-object/from16 v2, p7

    invoke-virtual {v3, v13, v0, v1, v2}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->downloadToDir(Ljava/util/List;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    goto :goto_0

    .line 757
    .end local v9    # "arr$":[Ljava/lang/String;
    .end local v10    # "i$":I
    .end local v11    # "len$":I
    .end local v13    # "srcPathsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :sswitch_3
    if-eqz p6, :cond_0

    .line 758
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    move-object/from16 v0, p6

    invoke-virtual {v0, v3}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->equals(Lcom/sec/android/app/myfiles/ftp/FTPParams;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 759
    if-eqz p3, :cond_3

    .line 760
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v3, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    move-object/from16 v0, p4

    move-object/from16 v1, p5

    move-object/from16 v2, p7

    invoke-virtual {v3, v0, v1, v2}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->moveFilesOverSelf([Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    goto :goto_0

    .line 762
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v3, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    move-object/from16 v0, p4

    move-object/from16 v1, p5

    move-object/from16 v2, p7

    invoke-virtual {v3, v0, v1, v2}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->uploadSelf([Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    goto :goto_0

    .line 766
    :cond_4
    if-eqz p8, :cond_5

    .line 768
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v3, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move/from16 v7, p3

    move-object/from16 v8, p7

    invoke-virtual/range {v3 .. v8}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->uploadFtp(Lcom/sec/android/app/myfiles/ftp/FTPParams;[Ljava/lang/String;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    goto/16 :goto_0

    .line 772
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v3, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    move-object/from16 v4, p6

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move/from16 v7, p3

    move-object/from16 v8, p7

    invoke-virtual/range {v3 .. v8}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->uploadFtp(Lcom/sec/android/app/myfiles/ftp/FTPParams;[Ljava/lang/String;Ljava/lang/String;ZLcom/sec/android/app/myfiles/ftp/IFTPResponseListener;)V

    goto/16 :goto_0

    .line 727
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_0
        0x4 -> :sswitch_0
        0x5 -> :sswitch_0
        0x7 -> :sswitch_0
        0xa -> :sswitch_1
        0x201 -> :sswitch_0
    .end sparse-switch

    .line 743
    :sswitch_data_1
    .sparse-switch
        0x2 -> :sswitch_2
        0x3 -> :sswitch_2
        0x4 -> :sswitch_2
        0x5 -> :sswitch_2
        0x7 -> :sswitch_2
        0xa -> :sswitch_3
        0x201 -> :sswitch_2
    .end sparse-switch
.end method

.method protected handleCopyMoveOperations(IIZ[Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/FTPParams;Z)V
    .locals 9
    .param p1, "fromFragmentId"    # I
    .param p2, "toFragmentId"    # I
    .param p3, "move"    # Z
    .param p4, "srcPaths"    # [Ljava/lang/String;
    .param p5, "targetPath"    # Ljava/lang/String;
    .param p6, "srcParams"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p7, "fromMultiwindow"    # Z

    .prologue
    .line 715
    const/4 v7, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move/from16 v8, p7

    invoke-virtual/range {v0 .. v8}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->handleCopyMoveOperations(IIZ[Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/FTPParams;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;Z)V

    .line 717
    return-void
.end method

.method protected handleCopyMoveOperations(IIZ[Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;Lcom/sec/android/app/myfiles/ftp/FTPParams;Z)V
    .locals 9
    .param p1, "fromFragmentId"    # I
    .param p2, "toFragmentId"    # I
    .param p3, "move"    # Z
    .param p4, "srcPaths"    # [Ljava/lang/String;
    .param p5, "targetPath"    # Ljava/lang/String;
    .param p6, "listener"    # Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;
    .param p7, "srcParams"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p8, "fromMultiwindow"    # Z

    .prologue
    .line 721
    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p7

    move-object v7, p6

    move/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->handleCopyMoveOperations(IIZ[Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/FTPParams;Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;Z)V

    .line 723
    return-void
.end method

.method protected handleCopyMoveOperations(IIZ[Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 8
    .param p1, "fromFragmentId"    # I
    .param p2, "toFragmentId"    # I
    .param p3, "move"    # Z
    .param p4, "srcPaths"    # [Ljava/lang/String;
    .param p5, "targetPath"    # Ljava/lang/String;
    .param p6, "fromMultiwindow"    # Z

    .prologue
    .line 708
    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->handleCopyMoveOperations(IIZ[Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/FTPParams;Z)V

    .line 710
    return-void
.end method

.method protected handleCreateFolderRequest()V
    .locals 11

    .prologue
    const/4 v9, 0x4

    .line 1818
    new-instance v2, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    invoke-direct {v2}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;-><init>()V

    .line 1819
    .local v2, "createFolderDialog":Landroid/app/DialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1820
    .local v0, "arg":Landroid/os/Bundle;
    const-string v7, "title"

    const v8, 0x7f0b0019

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1821
    const-string v7, "kind_of_operation"

    invoke-virtual {v0, v7, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1822
    const-string v7, "src_fragment_id"

    const/16 v8, 0xa

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1823
    const-string v7, "current_folder"

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1825
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v7, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    .line 1826
    .local v1, "c":Landroid/database/Cursor;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1828
    .local v6, "nameList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v7

    if-lez v7, :cond_2

    .line 1829
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1830
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v7

    if-ge v4, v7, :cond_2

    .line 1831
    const-string v7, "is_directory"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 1832
    .local v5, "isDir":I
    const/4 v7, 0x1

    if-eq v5, v7, :cond_0

    const/4 v7, 0x2

    if-ne v5, v7, :cond_1

    .line 1833
    :cond_0
    const-string v7, "title"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1835
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    .line 1830
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1839
    .end local v4    # "i":I
    .end local v5    # "isDir":I
    :cond_2
    const-string v7, "ftp_folder_name"

    invoke-virtual {v0, v7, v6}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1840
    invoke-virtual {v2, v0}, Landroid/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 1841
    invoke-virtual {v2, p0, v9}, Landroid/app/DialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 1844
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    const-string v8, "createFolder"

    invoke-virtual {v2, v7, v8}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1853
    :goto_1
    return-void

    .line 1846
    :catch_0
    move-exception v3

    .line 1850
    .local v3, "e":Ljava/lang/Exception;
    const/4 v7, 0x0

    sget-object v8, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->MODULE:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "dialog has not been shown Exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method protected handleDownloadRequest()V
    .locals 6

    .prologue
    .line 1993
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v2

    .line 1994
    .local v2, "selectedItems":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1995
    .local v3, "targetItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 1996
    .local v1, "o":Ljava/lang/Object;
    check-cast v1, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .end local v1    # "o":Ljava/lang/Object;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1998
    :cond_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 1999
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v4, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->downloadToDefault(Ljava/util/List;Z)V

    .line 2001
    :cond_1
    return-void
.end method

.method protected handleDownloadRequest(Ljava/lang/String;Z)V
    .locals 9
    .param p1, "targetDirectory"    # Ljava/lang/String;
    .param p2, "move"    # Z

    .prologue
    .line 1974
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v8

    .line 1975
    .local v8, "selectedItems":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 1976
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    new-array v4, v0, [Ljava/lang/String;

    .line 1977
    .local v4, "paths":[Ljava/lang/String;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    if-ge v7, v0, :cond_0

    .line 1978
    invoke-interface {v8, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/element/BrowserItem;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getPath()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    .line 1977
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 1980
    :cond_0
    const/16 v1, 0xa

    const/16 v2, 0x201

    const/4 v6, 0x0

    move-object v0, p0

    move v3, p2

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->handleCopyMoveOperations(IIZ[Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1982
    .end local v4    # "paths":[Ljava/lang/String;
    .end local v7    # "i":I
    :cond_1
    return-void
.end method

.method protected handleSelectItemsRequest()V
    .locals 6

    .prologue
    .line 2007
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getSelectionType()I

    move-result v3

    .line 2008
    .local v3, "selectionType":I
    packed-switch v3, :pswitch_data_0

    .line 2041
    :cond_0
    :goto_0
    return-void

    .line 2012
    :pswitch_0
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v2

    .line 2014
    .local v2, "selectedItems":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 2016
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2018
    .local v1, "result":Landroid/content/Intent;
    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/myfiles/element/BrowserItem;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 2020
    .local v0, "path":Ljava/lang/String;
    const-string v4, "FILE"

    invoke-virtual {v1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2022
    const-string v4, "FILE_URI"

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v5, v0}, Lcom/sec/android/app/myfiles/utils/FileUtils;->pathToUri(Landroid/app/Activity;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2024
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2026
    const-string v4, "CONTENT_TYPE"

    invoke-static {v0}, Lcom/sec/android/app/myfiles/MediaFile;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2028
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v4, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    invoke-virtual {v4, v0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->downloadAndSelect(Ljava/lang/String;)V

    goto :goto_0

    .line 2008
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected handleShowDetails()V
    .locals 29

    .prologue
    .line 591
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v23

    .line 593
    .local v23, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    if-eqz v23, :cond_2

    .line 594
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v20

    .line 598
    .local v20, "selSize":I
    :goto_0
    if-eqz v23, :cond_5

    if-lez v20, :cond_5

    .line 599
    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getDetailsFragment(I)Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;

    move-result-object v7

    .line 600
    .local v7, "details":Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;
    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->getDiscoveryVisitor()Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;

    move-result-object v24

    .line 601
    .local v24, "visitor":Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 602
    .local v4, "arg":Landroid/os/Bundle;
    const/4 v9, 0x0

    .local v9, "files":I
    const/4 v11, 0x0

    .line 603
    .local v11, "folders":I
    const-wide/16 v12, 0x0

    .line 605
    .local v12, "filesSize":J
    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    .line 607
    .local v22, "selectedFtpItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-eqz v25, :cond_4

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    .local v18, "item":Ljava/lang/Object;
    move-object/from16 v5, v18

    .line 608
    check-cast v5, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .line 609
    .local v5, "bItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    iget-object v0, v5, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-static/range {v25 .. v25}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getItem(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPItem;

    move-result-object v15

    .line 610
    .local v15, "ftpItem":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    if-eqz v15, :cond_0

    .line 611
    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 612
    invoke-interface {v15}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isDirectory()Z

    move-result v25

    if-nez v25, :cond_1

    invoke-interface {v15}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isSymbolicLink()Z

    move-result v25

    if-eqz v25, :cond_3

    .line 613
    :cond_1
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 596
    .end local v4    # "arg":Landroid/os/Bundle;
    .end local v5    # "bItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    .end local v7    # "details":Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;
    .end local v9    # "files":I
    .end local v11    # "folders":I
    .end local v12    # "filesSize":J
    .end local v15    # "ftpItem":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .end local v16    # "i$":Ljava/util/Iterator;
    .end local v18    # "item":Ljava/lang/Object;
    .end local v20    # "selSize":I
    .end local v22    # "selectedFtpItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    .end local v24    # "visitor":Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;
    :cond_2
    const/16 v20, 0x0

    .restart local v20    # "selSize":I
    goto :goto_0

    .line 615
    .restart local v4    # "arg":Landroid/os/Bundle;
    .restart local v5    # "bItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    .restart local v7    # "details":Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;
    .restart local v9    # "files":I
    .restart local v11    # "folders":I
    .restart local v12    # "filesSize":J
    .restart local v15    # "ftpItem":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .restart local v16    # "i$":Ljava/util/Iterator;
    .restart local v18    # "item":Ljava/lang/Object;
    .restart local v22    # "selectedFtpItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    .restart local v24    # "visitor":Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;
    :cond_3
    add-int/lit8 v9, v9, 0x1

    .line 616
    invoke-interface {v15}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getSize()J

    move-result-wide v26

    add-long v12, v12, v26

    goto :goto_1

    .line 621
    .end local v5    # "bItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    .end local v15    # "ftpItem":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .end local v18    # "item":Ljava/lang/Object;
    :cond_4
    const/16 v25, 0x1

    move/from16 v0, v20

    move/from16 v1, v25

    if-ne v0, v1, :cond_a

    .line 622
    const/16 v25, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .line 623
    .local v21, "selected":Lcom/sec/android/app/myfiles/element/BrowserItem;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemPosition()Ljava/util/ArrayList;

    move-result-object v19

    .line 624
    .local v19, "selItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v25

    if-nez v25, :cond_6

    .line 693
    .end local v4    # "arg":Landroid/os/Bundle;
    .end local v7    # "details":Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;
    .end local v9    # "files":I
    .end local v11    # "folders":I
    .end local v12    # "filesSize":J
    .end local v16    # "i$":Ljava/util/Iterator;
    .end local v19    # "selItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v21    # "selected":Lcom/sec/android/app/myfiles/element/BrowserItem;
    .end local v22    # "selectedFtpItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    .end local v24    # "visitor":Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;
    :cond_5
    :goto_2
    return-void

    .line 628
    .restart local v4    # "arg":Landroid/os/Bundle;
    .restart local v7    # "details":Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;
    .restart local v9    # "files":I
    .restart local v11    # "folders":I
    .restart local v12    # "filesSize":J
    .restart local v16    # "i$":Ljava/util/Iterator;
    .restart local v19    # "selItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v21    # "selected":Lcom/sec/android/app/myfiles/element/BrowserItem;
    .restart local v22    # "selectedFtpItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    .restart local v24    # "visitor":Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;
    :cond_6
    const-string v25, "detail_item_path"

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 629
    const-string v26, "detail_item_index"

    const/16 v25, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/lang/Integer;

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Integer;->intValue()I

    move-result v25

    move-object/from16 v0, v26

    move/from16 v1, v25

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 630
    const/16 v25, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    invoke-interface/range {v25 .. v25}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isDirectory()Z

    move-result v25

    if-eqz v25, :cond_a

    .line 631
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v25, v0

    check-cast v25, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    const/16 v26, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    invoke-interface/range {v26 .. v26}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getItemsInFolder(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 632
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v10, 0x0

    .line 633
    .local v10, "filesInFolder":I
    const/4 v14, 0x0

    .line 635
    .local v14, "foldersInFolder":I
    if-eqz v6, :cond_9

    .line 637
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v25

    if-eqz v25, :cond_8

    .line 639
    :cond_7
    const-string v25, "is_directory"

    move-object/from16 v0, v25

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    move/from16 v0, v25

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 641
    .local v17, "is_directory":I
    if-nez v17, :cond_c

    .line 642
    add-int/lit8 v10, v10, 0x1

    .line 646
    :goto_3
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v25

    if-nez v25, :cond_7

    .line 647
    .end local v17    # "is_directory":I
    :cond_8
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 650
    :cond_9
    const-string v25, "detail_files_count"

    move-object/from16 v0, v25

    invoke-virtual {v4, v0, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 651
    const-string v25, "detail_folder_count"

    move-object/from16 v0, v25

    invoke-virtual {v4, v0, v14}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 655
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v10    # "filesInFolder":I
    .end local v14    # "foldersInFolder":I
    .end local v19    # "selItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v21    # "selected":Lcom/sec/android/app/myfiles/element/BrowserItem;
    :cond_a
    const/16 v25, 0x1

    move/from16 v0, v20

    move/from16 v1, v25

    if-le v0, v1, :cond_b

    .line 656
    const-string v25, "detail_files_count"

    move-object/from16 v0, v25

    invoke-virtual {v4, v0, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 657
    const-string v25, "detail_folder_count"

    move-object/from16 v0, v25

    invoke-virtual {v4, v0, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 660
    :cond_b
    if-nez v11, :cond_d

    .line 662
    const-string v25, "detail_items_size"

    move-object/from16 v0, v25

    invoke-virtual {v4, v0, v12, v13}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 670
    :goto_4
    invoke-virtual {v7, v4}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->setArguments(Landroid/os/Bundle;)V

    .line 671
    const/16 v25, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-virtual {v7, v0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 675
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v25

    const-string v26, "detail"

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v7, v0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsFTPDetailFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 685
    :goto_5
    if-lez v11, :cond_5

    .line 687
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v25, v0

    check-cast v25, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    move-object/from16 v0, v25

    move-object/from16 v1, v22

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->visitItemsRecursively(Ljava/util/List;Lcom/sec/android/app/myfiles/ftp/IDiscoveryVisitor;)V

    goto/16 :goto_2

    .line 644
    .restart local v6    # "cursor":Landroid/database/Cursor;
    .restart local v10    # "filesInFolder":I
    .restart local v14    # "foldersInFolder":I
    .restart local v17    # "is_directory":I
    .restart local v19    # "selItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v21    # "selected":Lcom/sec/android/app/myfiles/element/BrowserItem;
    :cond_c
    add-int/lit8 v14, v14, 0x1

    goto :goto_3

    .line 666
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v10    # "filesInFolder":I
    .end local v14    # "foldersInFolder":I
    .end local v17    # "is_directory":I
    .end local v19    # "selItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v21    # "selected":Lcom/sec/android/app/myfiles/element/BrowserItem;
    :cond_d
    const-string v25, "detail_items_size"

    const-wide/16 v26, 0x0

    move-object/from16 v0, v25

    move-wide/from16 v1, v26

    invoke-virtual {v4, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_4

    .line 677
    :catch_0
    move-exception v8

    .line 681
    .local v8, "e":Ljava/lang/Exception;
    const/16 v25, 0x0

    sget-object v26, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->MODULE:Ljava/lang/String;

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "dialog has not been shown Exception: "

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v25 .. v27}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_5
.end method

.method public moveFTPToSecretBox()V
    .locals 8

    .prologue
    .line 2458
    const/4 v6, 0x1

    invoke-static {v6}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(I)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v1

    .line 2460
    .local v1, "fragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    const/4 v6, 0x2

    invoke-virtual {v1, p0, v6}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 2462
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v4

    .line 2464
    .local v4, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2466
    .local v5, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 2468
    .local v3, "selectedItem":Ljava/lang/Object;
    check-cast v3, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .end local v3    # "selectedItem":Ljava/lang/Object;
    iget-object v6, v3, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2471
    :cond_0
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 2473
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v6, "src_fragment_id"

    const/16 v7, 0xa

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2475
    const-string v6, "dest_fragment_id"

    const/16 v7, 0x201

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2477
    const-string v6, "src-ftp-param"

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2479
    const-string v6, "dst_folder"

    sget-object v7, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2481
    const-string v6, "target_datas"

    invoke-virtual {v0, v6, v5}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2483
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "delete"

    invoke-virtual {v1, v6, v7}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 2484
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 24
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 790
    sparse-switch p1, :sswitch_data_0

    .line 1079
    :cond_0
    :goto_0
    invoke-super/range {p0 .. p3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1080
    :cond_1
    :goto_1
    return-void

    .line 794
    :sswitch_0
    const/16 v20, -0x1

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_1

    if-eqz p3, :cond_1

    .line 796
    const-string v20, "result_value"

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 798
    .local v12, "newDirPath":Ljava/lang/String;
    const/16 v20, 0x8

    const/16 v21, 0x0

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(IZ)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v4

    .line 800
    .local v4, "createDirFragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    const/16 v20, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 802
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    .line 804
    .local v3, "args":Landroid/os/Bundle;
    const-string v20, "dst_folder"

    move-object/from16 v0, v20

    invoke-virtual {v3, v0, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 806
    const-string v20, "src_fragment_id"

    const/16 v21, 0xa

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 808
    const-string v20, "src-ftp-param"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 812
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v20

    const-string v21, "createFolder"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 814
    :catch_0
    move-exception v7

    .line 818
    .local v7, "e":Ljava/lang/Exception;
    const/16 v20, 0x0

    sget-object v21, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->MODULE:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "dialog has not been shown Exception: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 828
    .end local v3    # "args":Landroid/os/Bundle;
    .end local v4    # "createDirFragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    .end local v7    # "e":Ljava/lang/Exception;
    .end local v12    # "newDirPath":Ljava/lang/String;
    :sswitch_1
    if-eqz p3, :cond_1

    .line 830
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v14

    .line 832
    .local v14, "requestCurrentPath":Ljava/lang/String;
    if-eqz v14, :cond_1

    .line 834
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_2

    .line 836
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    sget-object v21, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 840
    :cond_2
    const-string v20, "from"

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 842
    .local v9, "from":Ljava/lang/String;
    const-string v20, "to"

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 844
    .local v19, "to":Ljava/lang/String;
    if-eqz v9, :cond_1

    if-eqz v19, :cond_1

    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    move-result v20

    if-nez v20, :cond_1

    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->isEmpty()Z

    move-result v20

    if-nez v20, :cond_1

    .line 846
    const/16 v20, 0x7

    const/16 v21, 0x0

    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(IZ)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v13

    .line 848
    .local v13, "renameItemFragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    const/16 v20, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v13, v0, v1}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 850
    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    .line 852
    .restart local v3    # "args":Landroid/os/Bundle;
    const-string v20, "src_folder"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 854
    const-string v20, "dst_folder"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 856
    const-string v20, "src_fragment_id"

    const/16 v21, 0xa

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 858
    const-string v20, "src-ftp-param"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 862
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v20

    const-string v21, "createFolder"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v13, v0, v1}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    .line 864
    :catch_1
    move-exception v7

    .line 868
    .restart local v7    # "e":Ljava/lang/Exception;
    const/16 v20, 0x0

    sget-object v21, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->MODULE:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "dialog has not been shown Exception: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 878
    .end local v3    # "args":Landroid/os/Bundle;
    .end local v7    # "e":Ljava/lang/Exception;
    .end local v9    # "from":Ljava/lang/String;
    .end local v13    # "renameItemFragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    .end local v14    # "requestCurrentPath":Ljava/lang/String;
    .end local v19    # "to":Ljava/lang/String;
    :sswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v20, v0

    if-eqz v20, :cond_0

    .line 880
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->AsyncOpen()V

    .line 882
    if-eqz p3, :cond_3

    move-object/from16 v10, p3

    .line 884
    .local v10, "intent":Landroid/content/Intent;
    :goto_2
    const-string v20, "id"

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getFragmentId()I

    move-result v21

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 886
    const-string v20, "target_folder"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 888
    const-string v20, "com.sec.android.action.FILE_OPERATION_DONE"

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 890
    packed-switch p2, :pswitch_data_0

    .line 900
    const-string v20, "success"

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_0

    .line 882
    .end local v10    # "intent":Landroid/content/Intent;
    :cond_3
    new-instance v10, Landroid/content/Intent;

    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    goto :goto_2

    .line 894
    .restart local v10    # "intent":Landroid/content/Intent;
    :pswitch_0
    const-string v20, "success"

    const/16 v21, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_0

    .line 911
    .end local v10    # "intent":Landroid/content/Intent;
    :sswitch_3
    if-eqz p3, :cond_1

    .line 913
    const-string v20, "FILE_OPERATION"

    const/16 v21, -0x1

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 915
    .local v8, "fileOperation":I
    const/16 v20, -0x1

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_b

    .line 917
    packed-switch v8, :pswitch_data_1

    .line 1001
    :cond_4
    :goto_3
    const/16 v20, 0x8

    move/from16 v0, v20

    if-eq v8, v0, :cond_5

    .line 1003
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->finishSelectMode()V

    .line 1006
    :cond_5
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1, v8}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->refreshCurrentFolderContent(II)V

    goto/16 :goto_1

    .line 922
    :pswitch_1
    const-string v20, "target_folder"

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 924
    .local v18, "targetPath":Ljava/lang/String;
    if-eqz v18, :cond_4

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->isEmpty()Z

    move-result v20

    if-nez v20, :cond_4

    .line 926
    const-string v20, "dest_fragment_id"

    const/16 v21, -0x1

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 928
    .local v5, "dstFragment":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    move-object/from16 v20, v0

    if-eqz v20, :cond_6

    const/16 v20, 0x201

    move/from16 v0, v20

    if-ne v5, v0, :cond_6

    .line 929
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-virtual/range {v20 .. v22}, Lcom/samsung/android/privatemode/PrivateModeManager;->unregisterClient(Landroid/os/IBinder;Z)Z

    .line 932
    :cond_6
    sparse-switch v5, :sswitch_data_1

    goto :goto_3

    .line 938
    :sswitch_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    .line 940
    .restart local v3    # "args":Landroid/os/Bundle;
    if-eqz p3, :cond_8

    .line 942
    const-string v20, "src-ftp-param"

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 944
    .local v17, "srcFtpParams":Ljava/lang/String;
    if-eqz v17, :cond_7

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->isEmpty()Z

    move-result v20

    if-nez v20, :cond_7

    .line 946
    const-string v20, "src-ftp-param"

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 950
    :cond_7
    const-string v20, "dst-ftp-param"

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 952
    .local v6, "dstFtpParams":Ljava/lang/String;
    if-eqz v6, :cond_8

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v20

    if-nez v20, :cond_8

    .line 954
    const-string v20, "dst-ftp-param"

    move-object/from16 v0, v20

    invoke-virtual {v3, v0, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 960
    .end local v6    # "dstFtpParams":Ljava/lang/String;
    .end local v17    # "srcFtpParams":Ljava/lang/String;
    :cond_8
    if-eqz v3, :cond_a

    .line 962
    const-string v20, "from_back"

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_9

    .line 964
    const-string v20, "from_back"

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 968
    :cond_9
    const-string v20, "from_back"

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 972
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getFragmentId()I

    move-result v20

    move-object/from16 v0, p0

    move/from16 v1, v20

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v5, v2, v3}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->switchFragments(IILjava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_3

    .line 988
    .end local v3    # "args":Landroid/os/Bundle;
    .end local v5    # "dstFragment":I
    .end local v18    # "targetPath":Ljava/lang/String;
    :cond_b
    if-nez p2, :cond_4

    .line 990
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    move-object/from16 v20, v0

    if-eqz v20, :cond_c

    .line 992
    const/16 v20, 0x0

    sget-object v21, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->MODULE:Ljava/lang/String;

    const-string v22, "ftp file operation is cancelled. unregister private mode"

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 993
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    invoke-virtual/range {v20 .. v22}, Lcom/samsung/android/privatemode/PrivateModeManager;->unregisterClient(Landroid/os/IBinder;Z)Z

    goto/16 :goto_3

    .line 997
    :cond_c
    const/16 v20, 0x0

    sget-object v21, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->MODULE:Ljava/lang/String;

    const-string v22, "ftp file operation is cancelled. private binder is null"

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 1013
    .end local v8    # "fileOperation":I
    :sswitch_5
    if-nez p2, :cond_d

    .line 1014
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v21, v0

    const v22, 0x7f0b00c9

    invoke-virtual/range {v21 .. v22}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x0

    invoke-static/range {v20 .. v22}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/widget/Toast;->show()V

    .line 1016
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    instance-of v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;

    move/from16 v20, v0

    if-eqz v20, :cond_1

    .line 1017
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setResult(I)V

    .line 1018
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/myfiles/AbsMainActivity;->finish()V

    goto/16 :goto_1

    .line 1022
    :sswitch_6
    if-nez p2, :cond_e

    .line 1023
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v21, v0

    const v22, 0x7f0b00cb

    invoke-virtual/range {v21 .. v22}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x0

    invoke-static/range {v20 .. v22}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/widget/Toast;->show()V

    .line 1025
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    instance-of v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;

    move/from16 v20, v0

    if-eqz v20, :cond_1

    .line 1026
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setResult(I)V

    .line 1027
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/myfiles/AbsMainActivity;->finish()V

    goto/16 :goto_1

    .line 1037
    :sswitch_7
    const/16 v20, -0x1

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_f

    .line 1039
    const-string v20, "target_folder"

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 1041
    .restart local v18    # "targetPath":Ljava/lang/String;
    const-string v20, "target_datas"

    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v15

    .line 1043
    .local v15, "serverDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static/range {v18 .. v18}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_1

    if-eqz v15, :cond_1

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_1

    .line 1045
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 1047
    .local v16, "serverPath":Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_1

    .line 1049
    invoke-static/range {v16 .. v16}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getItem(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPItem;

    move-result-object v11

    .line 1051
    .local v11, "item":Lcom/sec/android/app/myfiles/ftp/FTPItem;
    if-eqz v11, :cond_1

    .line 1053
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v20, v0

    check-cast v20, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->cacheFile(Lcom/sec/android/app/myfiles/ftp/FTPItem;)V

    .line 1055
    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->openFile(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1067
    .end local v11    # "item":Lcom/sec/android/app/myfiles/ftp/FTPItem;
    .end local v15    # "serverDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v16    # "serverPath":Ljava/lang/String;
    .end local v18    # "targetPath":Ljava/lang/String;
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->AsyncOpen()V

    .line 1069
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v21, v0

    const v22, 0x7f0b00ad

    invoke-virtual/range {v21 .. v22}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x0

    invoke-static/range {v20 .. v22}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 790
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_3
        0x4 -> :sswitch_0
        0xb -> :sswitch_7
        0xc -> :sswitch_5
        0xe -> :sswitch_6
        0x12 -> :sswitch_2
        0x13 -> :sswitch_1
    .end sparse-switch

    .line 890
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch

    .line 917
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 932
    :sswitch_data_1
    .sparse-switch
        0xa -> :sswitch_4
        0x24 -> :sswitch_4
        0x201 -> :sswitch_4
    .end sparse-switch
.end method

.method public onBackPressed()Z
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 1168
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mIsHomeBtnSelected:Z

    if-eqz v3, :cond_0

    .line 1170
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSwitchOnBack:Z

    .line 1174
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "RETURNPATH"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1176
    .local v2, "retPath":Ljava/lang/String;
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSwitchOnBack:Z

    if-eqz v3, :cond_2

    if-eqz v2, :cond_2

    .line 1178
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1180
    .local v0, "args":Landroid/os/Bundle;
    const-string v3, "FOLDERPATH"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1182
    const-string v3, "from_back"

    invoke-virtual {v0, v3, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1184
    const-string v3, "src-ftp-param"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "src-ftp-param"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1186
    const-string v3, "src_fragment_id"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "dest_fragment_id"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1188
    const-string v3, "dest_fragment_id"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "src_fragment_id"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1190
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mCallback:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$onCopyMoveSelectedListener;

    invoke-interface {v3, v0, v2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$onCopyMoveSelectedListener;->onCopyMoveSelected(Landroid/os/Bundle;Ljava/lang/String;)V

    .line 1243
    .end local v0    # "args":Landroid/os/Bundle;
    :cond_1
    :goto_0
    return v6

    .line 1196
    :cond_2
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSelector:Z

    if-nez v3, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->isSelectMode()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1198
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->finishSelectMode()V

    goto :goto_0

    .line 1203
    :cond_3
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSettingDefaultView:Z

    if-nez v3, :cond_1

    .line 1207
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v1

    .line 1208
    .local v1, "previousPath":Ljava/lang/String;
    :goto_1
    if-eqz v1, :cond_6

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getStartPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mIsHomeBtnSelected:Z

    if-eqz v3, :cond_4

    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mIsShortcutFolder:Z

    if-nez v3, :cond_6

    .line 1212
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->close()V

    .line 1213
    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSettingDefaultView:Z

    .line 1214
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->setParentActDefaultView()V

    goto :goto_0

    .line 1207
    .end local v1    # "previousPath":Ljava/lang/String;
    :cond_5
    const/4 v1, 0x0

    goto :goto_1

    .line 1218
    .restart local v1    # "previousPath":Ljava/lang/String;
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goUp()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 1221
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->isSelectMode()Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v3

    if-lez v3, :cond_7

    .line 1223
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->unselectAllItem()V

    .line 1225
    :cond_7
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/view/PathIndicator;->goUp()V

    .line 1227
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 1229
    :cond_8
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrefix:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 1235
    :goto_2
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mIsHomeBtnSelected:Z

    goto/16 :goto_0

    .line 1232
    :cond_9
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrefix:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPreviousPath:Ljava/lang/String;

    .line 1233
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPreviousPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    goto :goto_2

    .line 1240
    :cond_a
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->close()V

    .line 1241
    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSettingDefaultView:Z

    .line 1242
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->setParentActDefaultView()V

    goto/16 :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 2183
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2184
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mRunFrom:I

    const/16 v1, 0x15

    if-ne v0, v1, :cond_0

    .line 2185
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->selectmodeActionbar()V

    .line 2187
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 93
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onCreate(Landroid/os/Bundle;)V

    .line 95
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 97
    .local v0, "arg":Landroid/os/Bundle;
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->gotoFolder(Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 99
    return-void
.end method

.method protected onCreateDetailFragmentListener()Landroid/app/DialogFragment;
    .locals 1

    .prologue
    .line 1534
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/fragment/FtpDetailFragment;-><init>()V

    return-object v0
.end method

.method protected onCreateFileOperationListener()Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;
    .locals 1

    .prologue
    .line 1544
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$6;-><init>(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)V

    return-object v0
.end method

.method protected onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 1561
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$7;-><init>(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)V

    return-object v0
.end method

.method protected onCreateItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;
    .locals 1

    .prologue
    .line 1699
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$8;-><init>(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)V

    return-object v0
.end method

.method protected onCreateMultipleDetailFragmentListener()Landroid/app/DialogFragment;
    .locals 1

    .prologue
    .line 1539
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/FTPMultipleDetailFragment;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/fragment/FTPMultipleDetailFragment;-><init>()V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 230
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 232
    .local v0, "view":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPathChangeListener:Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setOnPathChangeListener(Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;)V

    .line 233
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrefix:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 234
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSelector:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getSelectionType()I

    move-result v1

    if-le v1, v5, :cond_0

    .line 235
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mCreateFolderContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 236
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mFromSelector:Z

    .line 239
    :cond_0
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSettingDefaultView:Z

    .line 240
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    if-eqz v1, :cond_1

    .line 241
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    new-instance v2, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$3;-><init>(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->setOnDropListener(Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;)V

    .line 250
    :cond_1
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 327
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onDestroy()V

    .line 329
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v0, :cond_0

    .line 331
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->setEmptyCurrentFolder(Z)V

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v0, :cond_1

    .line 337
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->closeCursor()V

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 342
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mBrowseDoneReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 344
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 314
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onDestroyView()V

    .line 316
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;

    if-nez v0, :cond_0

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v0, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->clearCacheDBBuffer()V

    .line 322
    :cond_0
    return-void
.end method

.method public onDrop(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 13
    .param p1, "targetFolder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/16 v12, 0x12

    const/4 v10, 0x1

    const/16 v9, 0xa

    const/4 v11, 0x0

    .line 1361
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDragSourceFragmentId()I

    move-result v7

    .line 1363
    .local v7, "srcFragment":I
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 1365
    .local v6, "size":I
    if-lez v6, :cond_0

    .line 1367
    sparse-switch v7, :sswitch_data_0

    .line 1454
    :cond_0
    :goto_0
    return-void

    .line 1371
    :sswitch_0
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getFtpParamsString()Ljava/lang/String;

    move-result-object v3

    .line 1373
    .local v3, "ftpParamsString":Ljava/lang/String;
    invoke-static {v11, v10, v10}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(IZZ)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v4

    .line 1376
    .local v4, "ftpToFtpFragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    invoke-virtual {v4, p0, v12}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 1378
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    .line 1380
    .local v2, "ftpArgs":Landroid/os/Bundle;
    const-string v8, "dst_folder"

    invoke-virtual {v2, v8, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1382
    const-string v8, "target_datas"

    invoke-virtual {v2, v8, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1384
    const-string v8, "src_fragment_id"

    invoke-virtual {v2, v8, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1386
    const-string v8, "dest_fragment_id"

    invoke-virtual {v2, v8, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1388
    const-string v8, "src-ftp-param"

    invoke-virtual {v2, v8, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1390
    const-string v8, "dst-ftp-param"

    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1394
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v8

    const-string v9, "createFolder"

    invoke-virtual {v4, v8, v9}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1396
    :catch_0
    move-exception v1

    .line 1400
    .local v1, "e":Ljava/lang/Exception;
    sget-object v8, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->MODULE:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "dialog has not been shown Exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v11, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1416
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "ftpArgs":Landroid/os/Bundle;
    .end local v3    # "ftpParamsString":Ljava/lang/String;
    .end local v4    # "ftpToFtpFragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    :sswitch_1
    invoke-static {v11}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(I)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v5

    .line 1418
    .local v5, "localToFtpFragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    invoke-virtual {v5, p0, v12}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 1420
    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 1422
    .local v0, "args":Landroid/os/Bundle;
    const-string v8, "dst_folder"

    invoke-virtual {v0, v8, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1424
    const-string v8, "target_datas"

    invoke-virtual {v0, v8, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1426
    const-string v8, "src_fragment_id"

    invoke-virtual {v0, v8, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1428
    const-string v8, "dest_fragment_id"

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1430
    const-string v8, "dst-ftp-param"

    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1434
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v8

    const-string v9, "createFolder"

    invoke-virtual {v5, v8, v9}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 1436
    :catch_1
    move-exception v1

    .line 1440
    .restart local v1    # "e":Ljava/lang/Exception;
    sget-object v8, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->MODULE:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "dialog has not been shown Exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v11, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1447
    .end local v0    # "args":Landroid/os/Bundle;
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v5    # "localToFtpFragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    :sswitch_2
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v9, 0x7f0b00cb

    invoke-static {v8, v9, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1367
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x3 -> :sswitch_1
        0x4 -> :sswitch_1
        0x5 -> :sswitch_1
        0x7 -> :sswitch_1
        0x8 -> :sswitch_2
        0xa -> :sswitch_0
        0x28 -> :sswitch_1
        0x201 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onDropFinished(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1281
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onDropFinished(Landroid/content/Intent;)V

    .line 1283
    if-eqz p1, :cond_0

    .line 1285
    const-string v3, "id"

    const/4 v4, -0x1

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 1287
    .local v2, "targetId":I
    sparse-switch v2, :sswitch_data_0

    .line 1317
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v3, :cond_0

    .line 1319
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->AsyncOpen()V

    .line 1329
    .end local v2    # "targetId":I
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 1291
    .restart local v2    # "targetId":I
    :sswitch_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v3, :cond_0

    .line 1293
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v0

    .line 1295
    .local v0, "currentFolder":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1297
    const-string v3, "target_folder"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1299
    .local v1, "targetFolder":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1301
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->AsyncOpen()V

    goto :goto_0

    .line 1287
    nop

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 21
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 383
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->pathForSearch()V

    .line 384
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v18

    sparse-switch v18, :sswitch_data_0

    .line 584
    invoke-super/range {p0 .. p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v18

    :goto_0
    return v18

    .line 386
    :sswitch_0
    const/16 v18, 0x1

    const/16 v19, -0x1

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->startSelectMode(III)V

    .line 388
    const/16 v18, 0x1

    goto :goto_0

    .line 391
    :sswitch_1
    const/16 v18, 0x1

    const/16 v19, -0x1

    const/16 v20, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->startSelectMode(III)V

    .line 393
    const/16 v18, 0x1

    goto :goto_0

    .line 396
    :sswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->handleCreateFolderRequest()V

    .line 397
    const/16 v18, 0x1

    goto :goto_0

    .line 400
    :sswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_0

    .line 401
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->handleAddShortcutRequest()V

    .line 405
    :goto_1
    const/16 v18, 0x1

    goto :goto_0

    .line 403
    :cond_0
    const/16 v18, 0x8

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->showDialog(I)V

    goto :goto_1

    .line 408
    :sswitch_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretMode(Landroid/content/Context;)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 409
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->moveFTPToSecretBox()V

    .line 455
    :goto_2
    const/16 v18, 0x1

    goto :goto_0

    .line 412
    :cond_1
    :try_start_0
    new-instance v18, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$5;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$5;-><init>(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrivateModeClient:Lcom/samsung/android/privatemode/IPrivateModeClient;

    .line 449
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrivateModeClient:Lcom/samsung/android/privatemode/IPrivateModeClient;

    move-object/from16 v19, v0

    invoke-static/range {v18 .. v19}, Lcom/samsung/android/privatemode/PrivateModeManager;->getInstance(Landroid/content/Context;Lcom/samsung/android/privatemode/IPrivateModeClient;)Lcom/samsung/android/privatemode/PrivateModeManager;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 451
    :catch_0
    move-exception v5

    .line 452
    .local v5, "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 459
    .end local v5    # "e":Ljava/lang/Exception;
    :sswitch_5
    new-instance v8, Landroid/content/Intent;

    const-string v18, "com.sec.android.app.myfiles.PICK_SELECT_PATH"

    move-object/from16 v0, v18

    invoke-direct {v8, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 460
    .local v8, "intent":Landroid/content/Intent;
    const-string v18, "SELECTOR_SOURCE_TYPE"

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mFragmentId:I

    move/from16 v19, v0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 461
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v18, v0

    if-eqz v18, :cond_3

    .line 462
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v9

    .line 463
    .local v9, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    if-eqz v9, :cond_3

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v18

    if-lez v18, :cond_3

    .line 464
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v18

    move/from16 v0, v18

    new-array v13, v0, [Ljava/lang/String;

    .line 465
    .local v13, "paths":[Ljava/lang/String;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_3
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v18

    move/from16 v0, v18

    if-ge v6, v0, :cond_2

    .line 466
    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/app/myfiles/element/BrowserItem;

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getPath()Ljava/lang/String;

    move-result-object v18

    aput-object v18, v13, v6

    .line 465
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 468
    :cond_2
    const-string v18, "SELECTOR_SOURCE_PATH"

    move-object/from16 v0, v18

    invoke-virtual {v8, v0, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 471
    .end local v6    # "i":I
    .end local v9    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v13    # "paths":[Ljava/lang/String;
    :cond_3
    const-string v18, "SELECTOR_SAME_FTP"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 472
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v18

    const v19, 0x7f0f0123

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_4

    .line 473
    const-string v18, "SELECTOR_FTP_MOVE"

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 474
    const-string v18, "FILE_OPERATION"

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 478
    const/16 v18, 0x0

    :try_start_1
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v8, v1}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 495
    :goto_4
    const/16 v18, 0x1

    goto/16 :goto_0

    .line 479
    :catch_1
    move-exception v5

    .line 481
    .local v5, "e":Landroid/content/ActivityNotFoundException;
    const/16 v18, 0x2

    sget-object v19, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->MODULE:Ljava/lang/String;

    const-string v20, "com.sec.android.app.myfiles.PICK_SELECT_PATH not found - copy"

    invoke-static/range {v18 .. v20}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 484
    .end local v5    # "e":Landroid/content/ActivityNotFoundException;
    :cond_4
    const-string v18, "SELECTOR_FTP_MOVE"

    const/16 v19, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 485
    const-string v18, "FILE_OPERATION"

    const/16 v19, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 489
    const/16 v18, 0x1

    :try_start_2
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v8, v1}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_4

    .line 490
    :catch_2
    move-exception v5

    .line 492
    .restart local v5    # "e":Landroid/content/ActivityNotFoundException;
    const/16 v18, 0x2

    sget-object v19, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->MODULE:Ljava/lang/String;

    const-string v20, "com.sec.android.app.myfiles.PICK_SELECT_PATH not found - move"

    invoke-static/range {v18 .. v20}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 497
    .end local v5    # "e":Landroid/content/ActivityNotFoundException;
    .end local v8    # "intent":Landroid/content/Intent;
    :sswitch_6
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->handleShowDetails()V

    .line 498
    const/16 v18, 0x1

    goto/16 :goto_0

    .line 502
    :sswitch_7
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getSelectionType()I

    move-result v17

    .line 504
    .local v17, "selectionType":I
    packed-switch v17, :pswitch_data_0

    .line 574
    :cond_5
    invoke-super/range {p0 .. p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 580
    :cond_6
    :goto_5
    const/16 v18, 0x1

    goto/16 :goto_0

    .line 508
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v16

    .line 510
    .local v16, "selectedItems":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_6

    .line 512
    new-instance v15, Landroid/content/Intent;

    invoke-direct {v15}, Landroid/content/Intent;-><init>()V

    .line 514
    .local v15, "result":Landroid/content/Intent;
    const/16 v18, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/app/myfiles/element/BrowserItem;

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getPath()Ljava/lang/String;

    move-result-object v12

    .line 516
    .local v12, "path":Ljava/lang/String;
    const-string v18, "FILE"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 518
    const-string v18, "FILE_URI"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-static {v0, v12}, Lcom/sec/android/app/myfiles/utils/FileUtils;->pathToUri(Landroid/app/Activity;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 520
    new-instance v18, Ljava/io/File;

    move-object/from16 v0, v18

    invoke-direct {v0, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static/range {v18 .. v18}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 522
    const-string v18, "CONTENT_TYPE"

    invoke-static {v12}, Lcom/sec/android/app/myfiles/MediaFile;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 524
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v18, v0

    check-cast v18, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->downloadAndSelect(Ljava/lang/String;)V

    goto :goto_5

    .line 532
    .end local v12    # "path":Ljava/lang/String;
    .end local v15    # "result":Landroid/content/Intent;
    .end local v16    # "selectedItems":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v11

    .line 534
    .local v11, "multiSelectedItems":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    if-eqz v11, :cond_6

    .line 536
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 538
    .local v14, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    .line 540
    .local v10, "multiItem":Ljava/lang/Object;
    check-cast v10, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .end local v10    # "multiItem":Ljava/lang/Object;
    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getPath()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 544
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v18, v0

    check-cast v18, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->downloadAndSelect(Ljava/util/List;)V

    goto/16 :goto_5

    .line 552
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v11    # "multiSelectedItems":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    .end local v14    # "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :pswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    .line 554
    .local v4, "args":Landroid/os/Bundle;
    if-eqz v4, :cond_5

    .line 556
    const-string v18, "from_add_shortcut"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_5

    .line 558
    const-string v18, "from_add_shortcut"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_5

    .line 560
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->handleAddShortcutRequest()V

    .line 562
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/myfiles/AbsMainActivity;->finish()V

    goto/16 :goto_5

    .line 384
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0f0122 -> :sswitch_5
        0x7f0f0123 -> :sswitch_5
        0x7f0f0124 -> :sswitch_4
        0x7f0f0129 -> :sswitch_3
        0x7f0f0130 -> :sswitch_6
        0x7f0f0137 -> :sswitch_0
        0x7f0f0138 -> :sswitch_1
        0x7f0f013a -> :sswitch_2
        0x7f0f0148 -> :sswitch_7
    .end sparse-switch

    .line 504
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 10
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v9, 0x7f0f013a

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 255
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 256
    const v6, 0x7f0f0128

    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 257
    .local v4, "renameItem":Landroid/view/MenuItem;
    const v6, 0x7f0f0129

    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 258
    .local v0, "addFTPItem":Landroid/view/MenuItem;
    const v6, 0x7f0f0137

    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    .line 259
    .local v5, "selectItem":Landroid/view/MenuItem;
    invoke-interface {p1, v9}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 260
    .local v1, "createFolderItem":Landroid/view/MenuItem;
    const v6, 0x7f0f0138

    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 261
    .local v3, "normalDeleteItem":Landroid/view/MenuItem;
    if-eqz v4, :cond_0

    .line 262
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->isSelectMode()Z

    move-result v6

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v6

    if-ne v6, v7, :cond_5

    .line 263
    invoke-interface {v4, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 269
    :cond_0
    :goto_0
    if-eqz v5, :cond_1

    .line 271
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCount()I

    move-result v6

    if-lez v6, :cond_1

    .line 273
    invoke-interface {v5, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 279
    :cond_1
    if-eqz v3, :cond_2

    .line 281
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCount()I

    move-result v6

    if-lez v6, :cond_2

    .line 284
    invoke-interface {v3, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 291
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v6, v6, Lcom/sec/android/app/myfiles/SelectorActivity;

    if-eqz v6, :cond_3

    .line 293
    if-eqz v1, :cond_3

    .line 295
    invoke-interface {v1, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 304
    :cond_3
    iget-boolean v6, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mFromSelector:Z

    if-eqz v6, :cond_4

    invoke-interface {p1, v9}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v6}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 305
    invoke-interface {p1, v9}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 306
    .local v2, "item":Landroid/view/MenuItem;
    invoke-interface {v2, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 309
    .end local v2    # "item":Landroid/view/MenuItem;
    :cond_4
    return-void

    .line 265
    :cond_5
    invoke-interface {v4, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onRefresh()V
    .locals 0

    .prologue
    .line 349
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 353
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onResume()V

    .line 354
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->showPathIndicator(Z)V

    .line 355
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    if-eqz v0, :cond_0

    .line 357
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    const-string v1, "13"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->setCategoryItemListSelection(Ljava/lang/String;)I

    .line 359
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$4;-><init>(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->post(Ljava/lang/Runnable;)Z

    .line 379
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 224
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onStart()V

    .line 225
    return-void
.end method

.method public openFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "serverPath"    # Ljava/lang/String;

    .prologue
    const v6, 0x7f0b007f

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 1084
    if-eqz p1, :cond_0

    .line 1086
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1088
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/myfiles/MediaFile;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1090
    .local v3, "mimeType":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 1092
    new-instance v2, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1094
    .local v2, "intent":Landroid/content/Intent;
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 1098
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v4, v2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1118
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v3    # "mimeType":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 1100
    .restart local v1    # "file":Ljava/io/File;
    .restart local v2    # "intent":Landroid/content/Intent;
    .restart local v3    # "mimeType":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1102
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 1104
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v4, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    invoke-virtual {v4, p2, p1}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->removeFileFromCache(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1110
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 1112
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v6, 0x7f0b00ae

    new-array v7, v10, [Ljava/lang/Object;

    const-string v8, "\"Download\""

    aput-object v8, v7, v9

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public pathForSearch()V
    .locals 6

    .prologue
    .line 2418
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v0

    .line 2420
    .local v0, "path":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2422
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2437
    .local v1, "sb":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v3, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getCurFolder()Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 2438
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v3, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/FTPNavigation;->getCurFolder()Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2443
    :cond_0
    new-instance v2, Lcom/sec/android/app/myfiles/ftp/FTPParams;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v0}, Lcom/sec/android/app/myfiles/ftp/FTPParams;-><init>(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Ljava/lang/String;)V

    .line 2445
    .local v2, "shortcut":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2447
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setCurrentDirectory(Ljava/lang/String;)V

    .line 2454
    .end local v1    # "sb":Ljava/lang/StringBuilder;
    .end local v2    # "shortcut":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    :cond_1
    :goto_0
    return-void

    .line 2449
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 2450
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "FOLDERPATH"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setCurrentDirectory(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected restorePathToPrevious()V
    .locals 2

    .prologue
    .line 2053
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPreviousPath:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2054
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPreviousPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 2056
    :cond_0
    return-void
.end method

.method public restorePathToPrevious(Ljava/lang/String;)V
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 2059
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2060
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrefix:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 2062
    :cond_0
    return-void
.end method

.method public restorePreviousSelection()V
    .locals 5

    .prologue
    .line 1932
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPreviousSelectedItemPositions:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v3, :cond_1

    .line 1934
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCount()I

    move-result v0

    .line 1936
    .local v0, "count":I
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->unselectAllItem()V

    .line 1938
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPreviousSelectedItemPositions:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 1940
    .local v2, "position":Ljava/lang/Integer;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ge v3, v0, :cond_0

    .line 1942
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->selectItem(I)V

    goto :goto_0

    .line 1950
    .end local v0    # "count":I
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "position":Ljava/lang/Integer;
    :cond_1
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPreviousSelectedItemPositions:Ljava/util/ArrayList;

    .line 1952
    return-void
.end method

.method protected selectAllItem()V
    .locals 2

    .prologue
    .line 1797
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->selectAllItem()V

    .line 1798
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1799
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getSelectModeFrom()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1800
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->selectAllFileItem()V

    .line 1805
    :cond_0
    :goto_0
    return-void

    .line 1802
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->selectAllItem()V

    goto :goto_0
.end method

.method public selectmodeActionbar()V
    .locals 7

    .prologue
    const v6, 0x7f0b0014

    const v5, 0x7f0b0013

    const/4 v3, 0x1

    const/4 v2, 0x0

    const v4, 0x7f0b0018

    .line 2191
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 2192
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 2193
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 2194
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 2196
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getSelectionType()I

    move-result v1

    if-eq v1, v3, :cond_3

    .line 2197
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040008

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 2199
    .local v0, "customview":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 2200
    const v1, 0x7f0f0008

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mActionbar_cancel:Landroid/widget/TextView;

    .line 2201
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mActionbar_cancel:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0b0017

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2202
    const v1, 0x7f0f0009

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    .line 2203
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0b0016

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2206
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/SelectorActivity;

    if-eqz v1, :cond_0

    .line 2207
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->getmFileOperation()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 2208
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->getmFileOperation()I

    move-result v1

    if-nez v1, :cond_2

    .line 2209
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    .line 2210
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v5}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2217
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mActionbar_cancel:Landroid/widget/TextView;

    new-instance v2, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$11;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$11;-><init>(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2232
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    new-instance v2, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$12;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$12;-><init>(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2244
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v1, :cond_1

    .line 2245
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2254
    :cond_1
    :goto_1
    return-void

    .line 2212
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(I)V

    .line 2213
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mActionbar_ok:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v6}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2249
    .end local v0    # "customview":Landroid/view/View;
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/high16 v2, 0x7f040000

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 2250
    .restart local v0    # "customview":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 2251
    const v1, 0x7f0f0003

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSelectAllButton:Landroid/widget/TextView;

    goto :goto_1
.end method

.method protected setPathPrefix(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstance"    # Landroid/os/Bundle;

    .prologue
    .line 2044
    sget-object v0, Ljava/io/File;->separator:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrefix:Ljava/lang/String;

    .line 2045
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    if-eqz v0, :cond_0

    .line 2046
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrefix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->getRootSessionName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrefix:Ljava/lang/String;

    .line 2050
    :goto_0
    return-void

    .line 2048
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrefix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ftp://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrefix:Ljava/lang/String;

    goto :goto_0
.end method

.method protected showDialogFragment(Landroid/app/DialogFragment;Ljava/lang/String;)V
    .locals 3
    .param p1, "df"    # Landroid/app/DialogFragment;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    .line 1678
    if-eqz p1, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1680
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 1682
    .local v0, "fm":Landroid/app/FragmentManager;
    if-eqz v0, :cond_0

    .line 1684
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 1686
    .local v1, "ft":Landroid/app/FragmentTransaction;
    invoke-virtual {v1, p1, p2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 1688
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1694
    .end local v0    # "fm":Landroid/app/FragmentManager;
    .end local v1    # "ft":Landroid/app/FragmentTransaction;
    :cond_0
    return-void
.end method

.method protected unselectAllItem()V
    .locals 1

    .prologue
    .line 1809
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->unselectAllItem()V

    .line 1810
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1811
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->unselectAllItem()V

    .line 1813
    :cond_0
    return-void
.end method

.method protected updatePathIndicator()V
    .locals 0

    .prologue
    .line 1815
    return-void
.end method
