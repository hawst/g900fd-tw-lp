.class Lcom/sec/android/app/myfiles/fragment/InputNameFragment$1;
.super Ljava/lang/Object;
.source "InputNameFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->setupDialog(Landroid/app/AlertDialog$Builder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/InputNameFragment;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v5, -0x1

    .line 75
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    iget v1, v1, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->cur_operation:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_1

    .line 76
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mInputName:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/InputNameFragment;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 77
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mCur_folder:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mInputName:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/InputNameFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".zip"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    # setter for: Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mExistZipFile:Ljava/io/File;
    invoke-static {v1, v2}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->access$102(Lcom/sec/android/app/myfiles/fragment/InputNameFragment;Ljava/io/File;)Ljava/io/File;

    .line 78
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mExistZipFile:Ljava/io/File;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/InputNameFragment;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 79
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    const v2, 0x7f0b012a

    # invokes: Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->showToast(I)V
    invoke-static {v1, v2}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->access$200(Lcom/sec/android/app/myfiles/fragment/InputNameFragment;I)V

    .line 112
    :cond_0
    :goto_0
    return-void

    .line 85
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 87
    .local v0, "data":Landroid/content/Intent;
    const-string v1, "result_value"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mInputEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 89
    const-string v1, "FILE"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mPath:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/InputNameFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 91
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    if-eqz v1, :cond_2

    .line 93
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->dismissAndShowAgainInputMethodPicker()V

    .line 97
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->dismissAllowingStateLoss()V

    .line 98
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 99
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->getTargetRequestCode()I

    move-result v2

    invoke-virtual {v1, v2, v5, v0}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    .line 101
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "rename_shortcut_request_intent_key"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 103
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    instance-of v1, v1, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v1, :cond_0

    .line 105
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "rename_shortcut_request_intent_key"

    const/16 v4, -0x64

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2, v5, v0}, Lcom/sec/android/app/myfiles/MainActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto/16 :goto_0
.end method
