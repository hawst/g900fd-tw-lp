.class Lcom/sec/android/app/myfiles/ftp/FTPSupplier$4;
.super Ljava/lang/Object;
.source "FTPSupplier.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/ftp/FTPAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->renameFile(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/ftp/FTPSupplier;)V
    .locals 0

    .prologue
    .line 375
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier$4;->this$0:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getWorkElement()Ljava/lang/String;
    .locals 1

    .prologue
    .line 397
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDone(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "result"    # Landroid/os/Bundle;

    .prologue
    .line 391
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier$4;->this$0:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    invoke-interface {v0}, Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;->onFileRenamed()V

    .line 392
    return-void
.end method

.method public work(Landroid/os/Bundle;)Z
    .locals 4
    .param p1, "arg"    # Landroid/os/Bundle;

    .prologue
    .line 378
    const/4 v0, 0x0

    .line 379
    .local v0, "result":Z
    if-eqz p1, :cond_0

    .line 380
    const-string v3, "rename_from"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 381
    .local v1, "targetFrom":Ljava/lang/String;
    const-string v3, "rename_to"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 382
    .local v2, "targetTo":Ljava/lang/String;
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 383
    iget-object v3, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier$4;->this$0:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    # getter for: Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->access$000(Lcom/sec/android/app/myfiles/ftp/FTPSupplier;)Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    move-result-object v3

    invoke-interface {v3, v1, v2}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncRenameFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 386
    .end local v1    # "targetFrom":Ljava/lang/String;
    .end local v2    # "targetTo":Ljava/lang/String;
    :cond_0
    return v0
.end method
