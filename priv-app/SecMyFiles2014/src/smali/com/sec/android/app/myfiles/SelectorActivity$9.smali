.class Lcom/sec/android/app/myfiles/SelectorActivity$9;
.super Ljava/lang/Object;
.source "SelectorActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/SelectorActivity;->onPrepareDialog(ILandroid/app/Dialog;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

.field final synthetic val$checkDoNotShow:Landroid/widget/CheckBox;

.field final synthetic val$dialogId:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/SelectorActivity;ILandroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 1884
    iput-object p1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$9;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iput p2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$9;->val$dialogId:I

    iput-object p3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$9;->val$checkDoNotShow:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1889
    iget v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$9;->val$dialogId:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$9;->val$dialogId:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 1891
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$9;->val$checkDoNotShow:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1892
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$9;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # setter for: Lcom/sec/android/app/myfiles/SelectorActivity;->bDoNotCheckDropBox:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$1502(Lcom/sec/android/app/myfiles/SelectorActivity;Z)Z

    .line 1903
    :cond_1
    :goto_0
    return-void

    .line 1894
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$9;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # setter for: Lcom/sec/android/app/myfiles/SelectorActivity;->bDoNotCheckDropBox:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$1502(Lcom/sec/android/app/myfiles/SelectorActivity;Z)Z

    goto :goto_0

    .line 1896
    :cond_3
    iget v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$9;->val$dialogId:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 1897
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$9;->val$checkDoNotShow:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1898
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$9;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # setter for: Lcom/sec/android/app/myfiles/SelectorActivity;->bDoNotCheckFTP:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$1602(Lcom/sec/android/app/myfiles/SelectorActivity;Z)Z

    goto :goto_0

    .line 1900
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$9;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # setter for: Lcom/sec/android/app/myfiles/SelectorActivity;->bDoNotCheckFTP:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$1602(Lcom/sec/android/app/myfiles/SelectorActivity;Z)Z

    goto :goto_0
.end method
