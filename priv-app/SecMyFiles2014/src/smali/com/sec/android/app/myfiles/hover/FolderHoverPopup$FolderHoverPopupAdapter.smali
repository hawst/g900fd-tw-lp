.class Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;
.super Landroid/widget/ArrayAdapter;
.source "FolderHoverPopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FolderHoverPopupAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;",
        ">;"
    }
.end annotation


# instance fields
.field private mFileThumb:Landroid/widget/ImageView;

.field private mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;",
            ">;"
        }
    .end annotation
.end field

.field private mOverlayIcon:Landroid/widget/ImageView;

.field final synthetic this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;>;"
    const/4 v1, 0x0

    .line 900
    iput-object p1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    .line 902
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 896
    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;->mFileThumb:Landroid/widget/ImageView;

    .line 897
    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;->mOverlayIcon:Landroid/widget/ImageView;

    .line 903
    iput-object p3, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;->mItems:Ljava/util/ArrayList;

    .line 904
    return-void
.end method

.method private setOverlayIconVisibility(Z)V
    .locals 2
    .param p1, "visibility"    # Z

    .prologue
    .line 1006
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;->mOverlayIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 1008
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 1009
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;->mOverlayIcon:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1013
    :cond_0
    :goto_0
    return-void

    .line 1011
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;->mOverlayIcon:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public getItem(I)Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 910
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 911
    const/4 p1, 0x0

    .line 913
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 892
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;->getItem(I)Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;

    move-result-object v0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 919
    move-object v7, p2

    .line 921
    .local v7, "adapterView":Landroid/view/View;
    if-nez v7, :cond_0

    .line 923
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/LayoutInflater;

    .line 925
    .local v9, "vi":Landroid/view/LayoutInflater;
    const v1, 0x7f04002a

    const/4 v2, 0x0

    invoke-virtual {v9, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 928
    .end local v9    # "vi":Landroid/view/LayoutInflater;
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;->getItem(I)Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;

    move-result-object v8

    .line 930
    .local v8, "fileItem":Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;
    if-eqz v8, :cond_3

    .line 932
    const v1, 0x7f0f00b6

    invoke-virtual {v7, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 933
    .local v3, "fileIcon":Landroid/widget/ImageView;
    const v1, 0x7f0f00b7

    invoke-virtual {v7, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 934
    .local v5, "fileName":Landroid/widget/TextView;
    const v1, 0x7f0f00b8

    invoke-virtual {v7, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;->mFileThumb:Landroid/widget/ImageView;

    .line 935
    const v1, 0x7f0f00b9

    invoke-virtual {v7, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;->mOverlayIcon:Landroid/widget/ImageView;

    .line 937
    if-eqz v3, :cond_1

    .line 939
    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->getFileIconID()I

    move-result v1

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 942
    :cond_1
    if-eqz v5, :cond_2

    .line 944
    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->getFileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 945
    const/4 v1, 0x0

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setHoverPopupType(I)V

    .line 948
    :cond_2
    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->checkHasThumbnail()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 950
    new-instance v0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;->mFileThumb:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;->mOverlayIcon:Landroid/widget/ImageView;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->isVidoeFile()Z

    move-result v6

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;-><init>(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/TextView;Z)V

    .line 951
    .local v0, "getThumbnail":Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;

    const/4 v2, 0x0

    aput-object v8, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 953
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;->mFileThumb:Landroid/widget/ImageView;

    if-eqz v1, :cond_5

    .line 955
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;->mFileThumb:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 957
    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->isVidoeFile()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 959
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;->setOverlayIconVisibility(Z)V

    .line 1001
    .end local v0    # "getThumbnail":Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;
    .end local v3    # "fileIcon":Landroid/widget/ImageView;
    .end local v5    # "fileName":Landroid/widget/TextView;
    :cond_3
    :goto_0
    return-object v7

    .line 961
    .restart local v0    # "getThumbnail":Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;
    .restart local v3    # "fileIcon":Landroid/widget/ImageView;
    .restart local v5    # "fileName":Landroid/widget/TextView;
    :cond_4
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;->setOverlayIconVisibility(Z)V

    goto :goto_0

    .line 965
    :cond_5
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;->setOverlayIconVisibility(Z)V

    .line 967
    if-eqz v3, :cond_6

    .line 969
    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->getFileIconID()I

    move-result v1

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 972
    :cond_6
    if-eqz v5, :cond_3

    .line 974
    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->getFileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 981
    .end local v0    # "getThumbnail":Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$GetThumbnailTask;
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;->mFileThumb:Landroid/widget/ImageView;

    if-eqz v1, :cond_8

    .line 982
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;->mFileThumb:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 983
    :cond_8
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FolderHoverPopupAdapter;->setOverlayIconVisibility(Z)V

    .line 985
    if-eqz v3, :cond_9

    .line 987
    const/4 v1, 0x0

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 988
    const/4 v1, 0x0

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 989
    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->getFileIconID()I

    move-result v1

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 992
    :cond_9
    if-eqz v5, :cond_3

    .line 994
    const/4 v1, 0x0

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 995
    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->getFileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
