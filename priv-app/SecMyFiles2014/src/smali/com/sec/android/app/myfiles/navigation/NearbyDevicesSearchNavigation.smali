.class public Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;
.super Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;
.source "NearbyDevicesSearchNavigation.java"


# instance fields
.field private final mProviderSearchResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;

.field protected mSearchParameter:Lcom/sec/android/app/myfiles/utils/SearchParameter;

.field protected mSearchRequests:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;-><init>(Landroid/content/Context;I)V

    .line 44
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mSearchRequests:Ljava/util/Set;

    .line 196
    new-instance v0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$3;-><init>(Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mProviderSearchResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;

    .line 48
    return-void
.end method


# virtual methods
.method public AsyncSearch()V
    .locals 3

    .prologue
    .line 110
    new-instance v0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$1;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$1;-><init>(Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;Lcom/sec/android/app/myfiles/fragment/AbsFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mAsyncUi:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mAsyncUi:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "SearchOperation"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 124
    :cond_0
    return-void
.end method

.method public getFilesInCurrentFolder()Landroid/database/Cursor;
    .locals 6

    .prologue
    .line 97
    const/4 v0, 0x0

    .line 99
    .local v0, "cursor":Landroid/database/Cursor;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mSearchParameter:Lcom/sec/android/app/myfiles/utils/SearchParameter;

    if-eqz v1, :cond_0

    .line 100
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 101
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/media/Provider;->getID()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mCurrentSortBy:I

    iget v4, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mCurrentInOrder:I

    iget-object v5, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mSearchParameter:Lcom/sec/android/app/myfiles/utils/SearchParameter;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/app/myfiles/db/CacheDB;->getNearbyFilteredContent(Ljava/lang/String;IILcom/sec/android/app/myfiles/utils/SearchParameter;)Landroid/database/Cursor;

    move-result-object v0

    .line 103
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 106
    :cond_0
    return-object v0
.end method

.method public goToRoot(Ljava/lang/Boolean;)Z
    .locals 1
    .param p1, "isBack"    # Ljava/lang/Boolean;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mSearchParameter:Lcom/sec/android/app/myfiles/utils/SearchParameter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mSearchParameter:Lcom/sec/android/app/myfiles/utils/SearchParameter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getKeyword()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mCurrentItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mCurrItemsIdProp:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mSearchRequests:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mAsyncUi:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    if-nez v0, :cond_1

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->AsyncSearch()V

    .line 74
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 70
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mAsyncUi:Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;->getCompletedCondition()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->AsyncSearch()V

    goto :goto_0
.end method

.method public goUp()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 79
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->isCurrentFolderRoot()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 80
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->open()V

    .line 81
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->clearNearByData()J

    .line 82
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mNearByDeviceDB:Lcom/sec/android/app/myfiles/db/CacheDB;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/db/CacheDB;->close()V

    .line 90
    :goto_0
    return v0

    .line 85
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->popItem()V

    .line 86
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->isCurrentFolderRoot()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 87
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->goToRoot(Ljava/lang/Boolean;)Z

    move-result v0

    goto :goto_0

    .line 89
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->AsyncOpen()V

    .line 90
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isCurrentFolderRoot()Z
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mCurrentPathItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected searchMoreItems(I)V
    .locals 5
    .param p1, "lastPosition"    # I

    .prologue
    .line 127
    add-int/lit8 v1, p1, 0x1

    div-int/lit8 v1, v1, 0x32

    mul-int/lit8 v1, v1, 0x32

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 128
    .local v0, "startIndex":Ljava/lang/Integer;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mSearchRequests:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 129
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mSearchRequests:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 130
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mSearchParameter:Lcom/sec/android/app/myfiles/utils/SearchParameter;

    if-eqz v1, :cond_0

    .line 131
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    new-instance v2, Lcom/samsung/android/allshare/media/SearchCriteria$Builder;

    invoke-direct {v2}, Lcom/samsung/android/allshare/media/SearchCriteria$Builder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mSearchParameter:Lcom/sec/android/app/myfiles/utils/SearchParameter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getKeyword()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/media/SearchCriteria$Builder;->setKeyword(Ljava/lang/String;)Lcom/samsung/android/allshare/media/SearchCriteria$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/allshare/media/SearchCriteria$Builder;->build()Lcom/samsung/android/allshare/media/SearchCriteria;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/16 v4, 0x32

    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/android/allshare/media/Provider;->search(Lcom/samsung/android/allshare/media/SearchCriteria;II)V

    .line 136
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lez v1, :cond_0

    .line 137
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mContext:Landroid/content/Context;

    const v3, 0x7f0b0089

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 143
    :cond_0
    return-void
.end method

.method protected setOnScrollListener()V
    .locals 1

    .prologue
    .line 147
    new-instance v0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation$2;-><init>(Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mOnScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    .line 185
    return-void
.end method

.method public setProvider(Lcom/samsung/android/allshare/media/Provider;)V
    .locals 2
    .param p1, "provider"    # Lcom/samsung/android/allshare/media/Provider;

    .prologue
    .line 189
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/navigation/NearByDevicesBrowserNavigation;->setProvider(Lcom/samsung/android/allshare/media/Provider;)V

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mProvider:Lcom/samsung/android/allshare/media/Provider;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mProviderSearchResponseListener:Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/media/Provider;->setSearchResponseListener(Lcom/samsung/android/allshare/media/Provider$IProviderSearchResponseListener;)V

    .line 194
    :cond_0
    return-void
.end method

.method public setSearchParameters(Lcom/sec/android/app/myfiles/utils/SearchParameter;)V
    .locals 0
    .param p1, "param"    # Lcom/sec/android/app/myfiles/utils/SearchParameter;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/NearbyDevicesSearchNavigation;->mSearchParameter:Lcom/sec/android/app/myfiles/utils/SearchParameter;

    .line 52
    return-void
.end method
