.class Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;
.super Ljava/lang/Object;
.source "FolderHoverPopup.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->initViews()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)V
    .locals 0

    .prologue
    .line 228
    iput-object p1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 284
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p2, v2}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v2

    if-ne v2, v6, :cond_0

    .line 286
    const/4 v2, 0x1

    const/4 v3, -0x1

    invoke-static {v2, v3}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 295
    :cond_0
    :goto_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 390
    :cond_1
    :goto_1
    :pswitch_0
    return v5

    .line 290
    :catch_0
    move-exception v0

    .line 292
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 299
    .end local v0    # "e":Landroid/os/RemoteException;
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    # setter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverMoved:Z
    invoke-static {v2, v5}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$002(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;Z)Z

    .line 301
    const-string v2, "FolderHoverPopup"

    const-string v3, "MotionEvent.ACTION_HOVER_ENTER"

    invoke-static {v4, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    .line 302
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->getHandler()Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 304
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->getHandler()Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 307
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    # getter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mArrayList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$100(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    # getter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mArrayList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$100(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/16 v3, 0x10

    if-le v2, v3, :cond_1

    .line 309
    const-string v2, "FolderHoverPopup"

    const-string v3, "MotionEvent.MSG_FOLDER_SHOW_THREAD_START"

    invoke-static {v4, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    .line 310
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    iput-boolean v4, v2, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mDismissFolderHover:Z

    .line 311
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->getHandler()Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 318
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    # setter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverMoved:Z
    invoke-static {v2, v5}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$002(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;Z)Z

    .line 320
    const-string v2, "FolderHoverPopup"

    const-string v3, "MotionEvent.ACTION_HOVER_MOVE"

    invoke-static {v4, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    .line 322
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->getHandler()Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 324
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->getHandler()Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 332
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    iput-boolean v4, v2, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mDismissFolderHover:Z

    goto :goto_1

    .line 338
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    # getter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mExitingHovering:Z
    invoke-static {v2}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$200(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 342
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    # setter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mExitingHovering:Z
    invoke-static {v2, v5}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$202(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;Z)Z

    .line 344
    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    # setter for: Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->mHoverMoved:Z
    invoke-static {v2, v4}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;->access$002(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;Z)Z

    .line 346
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1$1;-><init>(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$1;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 373
    .local v1, "thread":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 375
    const-string v2, "FolderHoverPopup"

    const-string v3, "MotionEvent.ACTION_HOVER_EXIT"

    invoke-static {v4, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 295
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
