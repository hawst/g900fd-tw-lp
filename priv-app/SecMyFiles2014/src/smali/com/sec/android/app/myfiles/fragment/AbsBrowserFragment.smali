.class public abstract Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;
.source "AbsBrowserFragment.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;
.implements Lcom/sec/android/app/myfiles/view/IDirectDragable$OnDirectDragListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$DropboxDownloadURL;,
        Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$onCopyMoveSelectedListener;
    }
.end annotation


# static fields
.field private static final MODULE:Ljava/lang/String; = "AbsBrowserFragment"

.field private static final MSG_PRIVATE_MODE_ON:I = 0x2

.field protected static final REQUEST_DROPBOX_SHAREVIA:I = 0x11

.field protected static final REQUEST_DROP_FILE_OPERATION:I = 0x12

.field protected static final REQUEST_FILES_DOWNLOAD_DROPBOX_SHARE:I = 0xf

.field protected static final REQUEST_FILE_DOWNLOAD:I = 0xb

.field protected static final REQUEST_FILE_OPERATION:I = 0x2

.field protected static final REQUEST_REMOTE_SHARE_DOWNLOAD:I = 0x1b

.field protected static final REQUEST_SELECT_PATH_FOR_COPY:I = 0x0

.field protected static final REQUEST_SELECT_PATH_FOR_MOVE:I = 0x1

.field private static final REQUEST_SETTING:I = 0xd

.field private static final USE_DRAG_AND_DROP_WITH_THUMBS:Z = true

.field private static mCheckBox:Landroid/widget/CheckBox;


# instance fields
.field private final AIRBUTTON_COPY:I

.field private final AIRBUTTON_DELETE:I

.field private final AIRBUTTON_MOVE:I

.field private final AIRBUTTON_RENAME:I

.field private final AIRBUTTON_SHARE:I

.field private bDoNotCheckDropBox:Z

.field private bFolderFromAirButton:Z

.field private downloadPath:Ljava/lang/String;

.field protected mActionModeCustom:Landroid/widget/LinearLayout;

.field private mActivity:Lcom/sec/android/app/myfiles/MainActivity;

.field protected mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

.field private mBackupSelectedItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field protected mCallback:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$onCopyMoveSelectedListener;

.field protected mCategoryType:I

.field protected mContextMenuPosition:I

.field private mContextpath:Ljava/lang/String;

.field private mConvertHandler:Landroid/os/Handler;

.field protected mCurrentorientation:I

.field private mDropbox:Ljava/lang/String;

.field private mDropboxRoot:Ljava/lang/String;

.field protected mEmptyImage:Landroid/widget/ImageView;

.field protected mEmptyTextView:Landroid/widget/TextView;

.field protected mFileObserver:Landroid/os/FileObserver;

.field private mFileOperationDoneReceiver:Landroid/content/BroadcastReceiver;

.field private mFolderHoverFilePath:Ljava/lang/String;

.field protected mImageView:Landroid/widget/ImageView;

.field protected mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

.field public mIsConverting:Z

.field protected mIsDragMode:Z

.field protected mIsHomeBtnSelected:Z

.field protected mIsSelector:Z

.field protected mIsShortcutFolder:Z

.field protected mJustSelectMode:Z

.field private mKnoxItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/PersonaItem;",
            ">;"
        }
    .end annotation
.end field

.field private mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

.field private mLockFileThread:Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;

.field protected mMoveToKnoxFileList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mMoveToKnoxFolderList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

.field private mNavigationObserver:Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;

.field protected mNeedToRefresh:Z

.field private mPersona:Landroid/os/PersonaManager;

.field protected mPrivateModeBinder:Landroid/os/IBinder;

.field protected mPrivateModeClient:Lcom/samsung/android/privatemode/IPrivateModeClient;

.field private mPrivateModeHandler:Landroid/os/Handler;

.field protected mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

.field private mProgressHandler:Landroid/os/Handler;

.field private mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

.field private mReplyByBroadcast:Z

.field protected mSelectActionBarView:Landroid/view/View;

.field protected mSelectAllButton:Landroid/widget/TextView;

.field mSelectedItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectedItemsDragListener:Landroid/view/View$OnDragListener;

.field private mSelectorCategoryType:I

.field private mSharePathList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mShortcutCurrentIndex:I

.field protected mShortcutRootFolder:Ljava/lang/String;

.field protected mSideSyncControl:Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;

.field private mSoundShotDialog:Landroid/app/AlertDialog;

.field protected mSplitPoint:I

.field protected mStartMoveToKnox:Z

.field protected mTempCursor:Landroid/database/Cursor;

.field private mTwMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;

.field private mWaitProgressDlg:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

.field private mbSettingChanged:Z

.field protected mbSplitInitialize:Z

.field private personas:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/PersonaInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 158
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;-><init>()V

    .line 208
    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mbSplitInitialize:Z

    .line 210
    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mIsDragMode:Z

    .line 217
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mWaitProgressDlg:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    .line 224
    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mbSettingChanged:Z

    .line 230
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSideSyncControl:Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;

    .line 234
    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mIsSelector:Z

    .line 244
    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->bDoNotCheckDropBox:Z

    .line 248
    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNeedToRefresh:Z

    .line 250
    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mIsConverting:Z

    .line 254
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPersona:Landroid/os/PersonaManager;

    .line 255
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->personas:Ljava/util/List;

    .line 256
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mKnoxItems:Ljava/util/ArrayList;

    .line 260
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 261
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    .line 264
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mBackupSelectedItems:Ljava/util/ArrayList;

    .line 265
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mTempCursor:Landroid/database/Cursor;

    .line 266
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectedItems:Ljava/util/ArrayList;

    .line 268
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    .line 269
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPrivateModeClient:Lcom/samsung/android/privatemode/IPrivateModeClient;

    .line 270
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

    .line 278
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFileObserver:Landroid/os/FileObserver;

    .line 301
    const-string v0, "/Dropbox"

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mDropboxRoot:Ljava/lang/String;

    .line 302
    const-string v0, "Dropbox/"

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mDropbox:Ljava/lang/String;

    .line 306
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSharePathList:Ljava/util/ArrayList;

    .line 308
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectorCategoryType:I

    .line 3860
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$23;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$23;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPrivateModeHandler:Landroid/os/Handler;

    .line 5856
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$27;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$27;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectedItemsDragListener:Landroid/view/View$OnDragListener;

    .line 6005
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$28;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$28;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mTwMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;

    .line 6082
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$29;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigationObserver:Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;

    .line 6169
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$30;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$30;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFileOperationDoneReceiver:Landroid/content/BroadcastReceiver;

    .line 6197
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSoundShotDialog:Landroid/app/AlertDialog;

    .line 6272
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$33;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$33;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mConvertHandler:Landroid/os/Handler;

    .line 6312
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$34;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$34;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mProgressHandler:Landroid/os/Handler;

    .line 6405
    iput v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->AIRBUTTON_COPY:I

    .line 6406
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->AIRBUTTON_MOVE:I

    .line 6407
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->AIRBUTTON_DELETE:I

    .line 6408
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->AIRBUTTON_RENAME:I

    .line 6409
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->AIRBUTTON_SHARE:I

    .line 6411
    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->bFolderFromAirButton:Z

    .line 6412
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFolderHoverFilePath:Ljava/lang/String;

    .line 6519
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/MainActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPrivateModeHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Lcom/sec/android/app/myfiles/utils/MyFilesDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mWaitProgressDlg:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;Lcom/sec/android/app/myfiles/utils/MyFilesDialog;)Lcom/sec/android/app/myfiles/utils/MyFilesDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;
    .param p1, "x1"    # Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    .prologue
    .line 158
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mWaitProgressDlg:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mProgressHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200()Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 158
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    .prologue
    .line 158
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->bDoNotCheckDropBox:Z

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 158
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->bDoNotCheckDropBox:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mKnoxItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 158
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->deleteHoveredItem(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFileOperationDoneReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    .prologue
    .line 158
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->dismissDialog()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mConvertHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;Ljava/io/File;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;
    .param p1, "x1"    # Ljava/io/File;

    .prologue
    .line 158
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->deleteGolfTempFiles(Ljava/io/File;)V

    return-void
.end method

.method private addSingleFileShortcutToHome()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    .line 2291
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->isSelectMode()Z

    move-result v9

    if-eqz v9, :cond_5

    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v9

    if-ne v9, v11, :cond_5

    .line 2293
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .line 2294
    .local v2, "item":Lcom/sec/android/app/myfiles/element/BrowserItem;
    if-nez v2, :cond_1

    .line 2368
    .end local v2    # "item":Lcom/sec/android/app/myfiles/element/BrowserItem;
    :cond_0
    :goto_0
    return-void

    .line 2296
    .restart local v2    # "item":Lcom/sec/android/app/myfiles/element/BrowserItem;
    :cond_1
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getPath()Ljava/lang/String;

    move-result-object v4

    .line 2297
    .local v4, "path":Ljava/lang/String;
    sget-char v9, Ljava/io/File;->separatorChar:C

    invoke-virtual {v4, v9}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    invoke-virtual {v4, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 2302
    .local v7, "shortcutName":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/BrowserItem;->isFile()Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-nez v9, :cond_2

    .line 2304
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v9, v4, v7}, Lcom/sec/android/app/myfiles/utils/Utils;->addShortcutToHome(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 2355
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->finishSelectMode()V

    goto :goto_0

    .line 2308
    :cond_2
    const/4 v6, 0x0

    .line 2309
    .local v6, "shortcutIcon":Landroid/graphics/Bitmap;
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 2310
    .local v5, "returnIntent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090012

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    float-to-int v1, v9

    .line 2312
    .local v1, "iconWidthHeight":I
    const-string v9, "android.intent.action.VIEW"

    invoke-virtual {v5, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2313
    const/4 v3, 0x0

    .line 2314
    .local v3, "mimeType":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v9, v4}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isOMADrmFile(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 2315
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v9, v4}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->getRealMimeTypeOfDRM(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2321
    :goto_2
    const/4 v8, 0x0

    .line 2328
    .local v8, "shortcutURI":Landroid/net/Uri;
    if-nez v8, :cond_3

    .line 2329
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v9}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v8

    .line 2331
    :cond_3
    invoke-virtual {v5, v8, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 2333
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v9, v1, v1, v4, v11}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->getHomeShortcutBitmap(Landroid/content/Context;IILjava/lang/String;Z)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 2336
    new-instance v0, Landroid/content/Intent;

    const-string v9, "com.android.launcher.action.INSTALL_SHORTCUT"

    invoke-direct {v0, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2339
    .local v0, "addShortcutToHomescreenIntent":Landroid/content/Intent;
    const-string v9, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v0, v9, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2342
    const-string v9, "android.intent.extra.shortcut.NAME"

    invoke-virtual {v0, v9, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2345
    const-string v9, "android.intent.extra.shortcut.ICON"

    invoke-virtual {v0, v9, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2348
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v9, v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 2351
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v9, v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->saveSetHomescreenPath(Ljava/lang/String;)V

    goto :goto_1

    .line 2318
    .end local v0    # "addShortcutToHomescreenIntent":Landroid/content/Intent;
    .end local v8    # "shortcutURI":Landroid/net/Uri;
    :cond_4
    invoke-static {v4}, Lcom/sec/android/app/myfiles/MediaFile;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 2357
    .end local v1    # "iconWidthHeight":I
    .end local v2    # "item":Lcom/sec/android/app/myfiles/element/BrowserItem;
    .end local v3    # "mimeType":Ljava/lang/String;
    .end local v4    # "path":Ljava/lang/String;
    .end local v5    # "returnIntent":Landroid/content/Intent;
    .end local v6    # "shortcutIcon":Landroid/graphics/Bitmap;
    .end local v7    # "shortcutName":Ljava/lang/String;
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->isSelectMode()Z

    move-result v9

    if-nez v9, :cond_0

    .line 2359
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v4

    .line 2361
    .restart local v4    # "path":Ljava/lang/String;
    sget-char v9, Ljava/io/File;->separatorChar:C

    invoke-virtual {v4, v9}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    invoke-virtual {v4, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 2363
    .restart local v7    # "shortcutName":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v9, v4, v7}, Lcom/sec/android/app/myfiles/utils/Utils;->addShortcutToHome(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private deleteGolfTempFiles(Ljava/io/File;)V
    .locals 9
    .param p1, "golfTempFiles"    # Ljava/io/File;

    .prologue
    .line 6367
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v6

    if-nez v6, :cond_0

    .line 6370
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v5

    .line 6372
    .local v5, "subFiles":[Ljava/io/File;
    if-eqz v5, :cond_0

    array-length v6, v5

    if-nez v6, :cond_1

    .line 6392
    .end local v5    # "subFiles":[Ljava/io/File;
    :cond_0
    :goto_0
    return-void

    .line 6378
    .restart local v5    # "subFiles":[Ljava/io/File;
    :cond_1
    move-object v0, v5

    .local v0, "arr$":[Ljava/io/File;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v4, v0, v2

    .line 6380
    .local v4, "sf":Ljava/io/File;
    invoke-direct {p0, v4}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->deleteGolfTempFiles(Ljava/io/File;)V

    .line 6378
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 6384
    .end local v4    # "sf":Ljava/io/File;
    :cond_2
    invoke-virtual {p1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 6388
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v5    # "subFiles":[Ljava/io/File;
    :catch_0
    move-exception v1

    .line 6390
    .local v1, "ex":Ljava/lang/SecurityException;
    const/4 v6, 0x0

    const-string v7, "AbsBrowserFragment"

    const-string v8, "deleteGolfTempFiles() - DeleteGolfTempFiles is failed"

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private deleteHoveredItem(Ljava/lang/String;)V
    .locals 7
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x2

    .line 3795
    invoke-static {v4}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(I)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v2

    .line 3797
    .local v2, "fragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    invoke-virtual {v2, p0, v4}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 3799
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 3801
    .local v3, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3803
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 3805
    .local v0, "arguments":Landroid/os/Bundle;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v4, :cond_0

    .line 3807
    const-string v4, "dst_folder"

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3810
    :cond_0
    const-string v4, "target_datas"

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 3812
    const-string v4, "src_fragment_id"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3814
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 3815
    .local v1, "fm":Landroid/app/FragmentManager;
    if-eqz v1, :cond_1

    .line 3816
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v4

    const-string v5, "delete"

    invoke-virtual {v4, v2, v5}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 3821
    :goto_0
    return-void

    .line 3818
    :cond_1
    const/4 v4, 0x0

    const-string v5, "AbsBrowserFragment"

    const-string v6, "deleteSelectedItem() : getFragmentManager() is null"

    invoke-static {v4, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private dismissDialog()V
    .locals 1

    .prologue
    .line 6396
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSoundShotDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 6398
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSoundShotDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 6400
    :cond_0
    return-void
.end method

.method protected static getParentDir(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 1250
    if-eqz p0, :cond_2

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1252
    const-string v3, "/"

    invoke-virtual {p0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1254
    .local v2, "split":[Ljava/lang/String;
    array-length v3, v2

    const/4 v4, 0x2

    if-le v3, v4, :cond_1

    .line 1256
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1258
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_0

    .line 1260
    const-string v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1262
    aget-object v3, v2, v0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1258
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1266
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1275
    .end local v0    # "i":I
    .end local v1    # "sb":Ljava/lang/StringBuilder;
    .end local v2    # "split":[Ljava/lang/String;
    :goto_1
    return-object v3

    .line 1270
    .restart local v2    # "split":[Ljava/lang/String;
    :cond_1
    const-string v3, "/"

    goto :goto_1

    .line 1275
    .end local v2    # "split":[Ljava/lang/String;
    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private getTotalListItemHeight()I
    .locals 5

    .prologue
    .line 624
    const/4 v2, 0x0

    .line 626
    .local v2, "listviewElementsheight":I
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getFirstVisiblePosition()I

    move-result v1

    .local v1, "idx":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getLastVisiblePosition()I

    move-result v3

    if-gt v1, v3, :cond_1

    .line 628
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getFirstVisiblePosition()I

    move-result v4

    sub-int v4, v1, v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 630
    .local v0, "childView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 632
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v2, v3

    .line 626
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 636
    .end local v0    # "childView":Landroid/view/View;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getHeight()I

    move-result v3

    if-le v2, v3, :cond_2

    .line 638
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getHeight()I

    move-result v2

    .line 641
    :cond_2
    return v2
.end method

.method private lock(Ljava/lang/String;)V
    .locals 12
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x2

    .line 2468
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->isEncryptionFile(Ljava/lang/String;)Z

    move-result v4

    .line 2470
    .local v4, "isLock":Z
    const-string v8, "AbsBrowserFragment"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "lock result = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v11, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 2473
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowLockFilesStatus()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 2474
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-direct {v0, v8}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2475
    .local v0, "DialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-static {v8}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 2476
    .local v2, "factory":Landroid/view/LayoutInflater;
    const v8, 0x7f040023

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 2477
    .local v5, "mCustomDialog":Landroid/view/View;
    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 2479
    const v8, 0x7f0f00a7

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/CheckBox;

    sput-object v8, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mCheckBox:Landroid/widget/CheckBox;

    .line 2480
    const v8, 0x7f0f00a5

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 2481
    .local v6, "mNotiTxt":Landroid/widget/TextView;
    const v8, 0x7f0f00a8

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 2483
    .local v7, "textDoNotShow":Landroid/widget/TextView;
    if-nez v4, :cond_0

    .line 2484
    const v8, 0x7f0b010a

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(I)V

    .line 2485
    const v8, 0x7f0b0106

    invoke-virtual {v0, v8}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 2491
    :goto_0
    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setSoundEffectsEnabled(Z)V

    .line 2492
    new-instance v8, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$8;

    invoke-direct {v8, p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$8;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2500
    const v8, 0x7f0b0015

    new-instance v9, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$9;

    invoke-direct {v9, p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$9;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;Ljava/lang/String;)V

    invoke-virtual {v0, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2550
    const v8, 0x7f0b0017

    new-instance v9, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$10;

    invoke-direct {v9, p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$10;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V

    invoke-virtual {v0, v8, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2558
    new-instance v8, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$11;

    invoke-direct {v8, p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$11;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V

    invoke-virtual {v0, v8}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 2568
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 2569
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/AlertDialog;->show()V

    .line 2597
    .end local v0    # "DialogBuilder":Landroid/app/AlertDialog$Builder;
    .end local v2    # "factory":Landroid/view/LayoutInflater;
    .end local v5    # "mCustomDialog":Landroid/view/View;
    .end local v6    # "mNotiTxt":Landroid/widget/TextView;
    .end local v7    # "textDoNotShow":Landroid/widget/TextView;
    :goto_1
    return-void

    .line 2487
    .restart local v0    # "DialogBuilder":Landroid/app/AlertDialog$Builder;
    .restart local v2    # "factory":Landroid/view/LayoutInflater;
    .restart local v5    # "mCustomDialog":Landroid/view/View;
    .restart local v6    # "mNotiTxt":Landroid/widget/TextView;
    .restart local v7    # "textDoNotShow":Landroid/widget/TextView;
    :cond_0
    const v8, 0x7f0b010b

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(I)V

    .line 2488
    const v8, 0x7f0b0107

    invoke-virtual {v0, v8}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 2571
    .end local v0    # "DialogBuilder":Landroid/app/AlertDialog$Builder;
    .end local v2    # "factory":Landroid/view/LayoutInflater;
    .end local v5    # "mCustomDialog":Landroid/view/View;
    .end local v6    # "mNotiTxt":Landroid/widget/TextView;
    .end local v7    # "textDoNotShow":Landroid/widget/TextView;
    :cond_1
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils;->isEncryptionFile(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 2572
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 2573
    .local v3, "intent":Landroid/content/Intent;
    const-string v8, "com.sec.android.app.myfiles"

    const-string v9, "com.sec.android.app.myfiles.activity.ChangePasswordsActivity"

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2578
    const/16 v8, 0x19

    :try_start_0
    invoke-virtual {p0, v3, v8}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2579
    :catch_0
    move-exception v1

    .line 2581
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    const-string v8, "AbsBrowserFragment"

    const-string v9, "om.sec.android.app.myfiles.activity.ChangePasswordsActivity not found - notShowLockFilesStatus unlock"

    invoke-static {v11, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2584
    .end local v1    # "e":Landroid/content/ActivityNotFoundException;
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_2
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 2585
    .restart local v3    # "intent":Landroid/content/Intent;
    const-string v8, "com.sec.android.app.myfiles"

    const-string v9, "com.sec.android.app.myfiles.activity.ChangePasswordsActivity"

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2590
    const/16 v8, 0x17

    :try_start_1
    invoke-virtual {p0, v3, v8}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 2591
    :catch_1
    move-exception v1

    .line 2593
    .restart local v1    # "e":Landroid/content/ActivityNotFoundException;
    const-string v8, "AbsBrowserFragment"

    const-string v9, "om.sec.android.app.myfiles.activity.ChangePasswordsActivity not found - notShowLockFilesStatus lock"

    invoke-static {v11, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private openDownloadedFile()V
    .locals 5

    .prologue
    .line 3474
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->downloadPath:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 3481
    :goto_0
    return-void

    .line 3478
    :cond_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->downloadPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3480
    .local v0, "file":Ljava/io/File;
    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mCategoryType:I

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getCurrentSortBy()I

    move-result v4

    invoke-static {v1, v0, v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/FileUtils;->openFile(ILjava/io/File;Landroid/app/Activity;ZI)V

    goto :goto_0
.end method

.method private setActionBarIcon(I)V
    .locals 5
    .param p1, "orientation"    # I

    .prologue
    const/4 v4, 0x0

    const v3, 0x7f0f0148

    const v2, 0x7f0f0147

    .line 683
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectModeFrom()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    if-eqz v0, :cond_1

    .line 685
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 687
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 692
    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 710
    :cond_1
    :goto_0
    return-void

    .line 699
    :cond_2
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 701
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v0, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 704
    :cond_3
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 706
    sget-object v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    invoke-interface {v0, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method private setResultAndFinish(ILandroid/content/Intent;)V
    .locals 4
    .param p1, "resultCode"    # I
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    .line 3446
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mReplyByBroadcast:Z

    if-eqz v0, :cond_1

    .line 3448
    if-nez p2, :cond_0

    .line 3450
    new-instance p2, Landroid/content/Intent;

    .end local p2    # "data":Landroid/content/Intent;
    invoke-direct {p2}, Landroid/content/Intent;-><init>()V

    .line 3453
    .restart local p2    # "data":Landroid/content/Intent;
    :cond_0
    const-string v0, "com.sec.android.app.myfiles.PICK_DATA_FINISHED"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 3455
    const-string v0, "RESULT_CODE"

    invoke-virtual {p2, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3457
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 3459
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->finish()V

    .line 3469
    :goto_0
    return-void

    .line 3463
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setResult(ILandroid/content/Intent;)V

    .line 3465
    const/4 v0, 0x2

    const-string v1, "AbsBrowserFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mParentActivity "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " finished result code "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 3467
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->finish()V

    goto :goto_0
.end method

.method private showDialogForSoundShot(Ljava/lang/String;)V
    .locals 6
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 6201
    const/4 v4, 0x2

    new-array v2, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const v5, 0x7f0b00d9

    invoke-virtual {p0, v5}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const v5, 0x7f0b00da

    invoke-virtual {p0, v5}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    .line 6203
    .local v2, "strItems":[Ljava/lang/String;
    const v4, 0x7f0b001f

    invoke-virtual {p0, v4}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 6205
    .local v3, "title":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/sec/android/app/myfiles/MediaFile;->getShareMimeType(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 6208
    .local v0, "MIMEType":Ljava/lang/String;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 6210
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 6212
    const v4, 0x7f0b0017

    new-instance v5, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$31;

    invoke-direct {v5, p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$31;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V

    invoke-virtual {v1, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 6220
    new-instance v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$32;

    invoke-direct {v4, p0, p1, v0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$32;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v4}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 6268
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSoundShotDialog:Landroid/app/AlertDialog;

    .line 6269
    return-void
.end method

.method private startCopyOrMove(IILjava/lang/String;IILandroid/content/Intent;)V
    .locals 13
    .param p1, "srcFragmentId"    # I
    .param p2, "dstFragmentId"    # I
    .param p3, "dstFolder"    # Ljava/lang/String;
    .param p4, "operation"    # I
    .param p5, "requestCode"    # I
    .param p6, "data"    # Landroid/content/Intent;

    .prologue
    .line 4099
    if-eqz p4, :cond_0

    const/4 v10, 0x1

    move/from16 v0, p4

    if-eq v0, v10, :cond_0

    .line 4208
    :goto_0
    return-void

    .line 4106
    :cond_0
    const/16 v10, 0xa

    if-eq p1, v10, :cond_1

    const/16 v10, 0xa

    if-ne p2, v10, :cond_2

    .line 4108
    :cond_1
    const/4 v10, 0x1

    const/4 v11, 0x1

    move/from16 v0, p4

    invoke-static {v0, v10, v11}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(IZZ)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v4

    .line 4116
    .local v4, "fragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    :goto_1
    move/from16 v0, p5

    invoke-virtual {v4, p0, v0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 4118
    const/4 v7, 0x0

    .line 4125
    .local v7, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    instance-of v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    if-eqz v10, :cond_3

    .line 4126
    new-instance v7, Ljava/util/ArrayList;

    .end local v7    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 4127
    .restart local v7    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mContextMenuPosition:I

    invoke-virtual {p0, v10}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getBrowserItem(I)Lcom/sec/android/app/myfiles/element/BrowserItem;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4134
    :goto_2
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 4136
    .local v9, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 4138
    .local v6, "selectedItem":Ljava/lang/Object;
    check-cast v6, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .end local v6    # "selectedItem":Ljava/lang/Object;
    iget-object v10, v6, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 4112
    .end local v4    # "fragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v7    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v9    # "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    invoke-static/range {p4 .. p4}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(I)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v4

    .restart local v4    # "fragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    goto :goto_1

    .line 4128
    .restart local v7    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_3
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mBackupSelectedItems:Ljava/util/ArrayList;

    if-eqz v10, :cond_4

    .line 4129
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mBackupSelectedItems:Ljava/util/ArrayList;

    goto :goto_2

    .line 4131
    :cond_4
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v7

    goto :goto_2

    .line 4142
    .restart local v5    # "i$":Ljava/util/Iterator;
    .restart local v9    # "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_5
    invoke-direct {p0, v9, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->verifySelectedFiles(Ljava/util/ArrayList;I)Z

    move-result v10

    if-nez v10, :cond_7

    .line 4144
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->finishSelectMode()V

    .line 4146
    if-nez p4, :cond_6

    .line 4148
    const/4 v10, 0x2

    const-string v11, "AbsBrowserFragment"

    const-string v12, "startCopyOrMove verifySelectedFiles:: Copy Failed(1)"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 4149
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v11, 0x7f0b00c9

    const/4 v12, 0x0

    invoke-static {v10, v11, v12}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v10

    invoke-virtual {v10}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 4153
    :cond_6
    const/4 v10, 0x2

    const-string v11, "AbsBrowserFragment"

    const-string v12, "startCopyOrMove verifySelectedFiles:: Move Failed(1)"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 4154
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v11, 0x7f0b00cb

    const/4 v12, 0x0

    invoke-static {v10, v11, v12}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v10

    invoke-virtual {v10}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 4160
    :cond_7
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 4162
    .local v1, "arguments":Landroid/os/Bundle;
    const-string v10, "dst_folder"

    move-object/from16 v0, p3

    invoke-virtual {v1, v10, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4164
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v10, :cond_8

    .line 4166
    const-string v10, "src_folder"

    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4170
    :cond_8
    const-string v10, "target_datas"

    invoke-virtual {v1, v10, v9}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 4172
    const-string v10, "src_fragment_id"

    invoke-virtual {v1, v10, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 4174
    const-string v10, "dest_fragment_id"

    invoke-virtual {v1, v10, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 4176
    if-eqz p6, :cond_a

    .line 4178
    const-string v10, "dst-ftp-param"

    move-object/from16 v0, p6

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4180
    .local v2, "dstFtpParams":Ljava/lang/String;
    if-eqz v2, :cond_9

    .line 4182
    const-string v10, "dst-ftp-param"

    invoke-virtual {v1, v10, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4186
    :cond_9
    const-string v10, "src-ftp-param"

    move-object/from16 v0, p6

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 4188
    .local v8, "srcFtpParams":Ljava/lang/String;
    if-eqz v8, :cond_a

    .line 4190
    const-string v10, "src-ftp-param"

    invoke-virtual {v1, v10, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4196
    .end local v2    # "dstFtpParams":Ljava/lang/String;
    .end local v8    # "srcFtpParams":Ljava/lang/String;
    :cond_a
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->appendCustomArgsToStartCopyOrMoveArgs(Landroid/os/Bundle;)V

    .line 4198
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    .line 4199
    .local v3, "fm":Landroid/app/FragmentManager;
    if-eqz v3, :cond_b

    .line 4200
    const-string v10, "createFolder"

    invoke-virtual {v4, v3, v10}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4202
    :cond_b
    const/4 v10, 0x0

    const-string v11, "AbsBrowserFragment"

    const-string v12, "Fragment manager startCopyOrMove was null!!!!"

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private startCopyOrMove(IILjava/lang/String;ILandroid/content/Intent;)V
    .locals 7
    .param p1, "srcFragmentId"    # I
    .param p2, "dstFragmentId"    # I
    .param p3, "dstFolder"    # Ljava/lang/String;
    .param p4, "operation"    # I
    .param p5, "data"    # Landroid/content/Intent;

    .prologue
    .line 4092
    const/4 v5, 0x2

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startCopyOrMove(IILjava/lang/String;IILandroid/content/Intent;)V

    .line 4093
    return-void
.end method

.method private verifySelectedFiles(Ljava/util/ArrayList;I)Z
    .locals 6
    .param p2, "srcFragmentId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I)Z"
        }
    .end annotation

    .prologue
    .local p1, "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v4, 0x0

    .line 4454
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-nez v5, :cond_1

    .line 4505
    :cond_0
    :goto_0
    return v4

    .line 4459
    :cond_1
    const-string v5, "/storage/UsbDrive"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v3, v5, 0x1

    .line 4461
    .local v3, "usbStoragePathLength":I
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 4463
    .local v1, "file":Ljava/lang/String;
    sparse-switch p2, :sswitch_data_0

    .line 4489
    :cond_3
    const-string v5, "/storage/extSdCard"

    invoke-virtual {v1, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    sget-boolean v5, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v5, :cond_0

    :cond_4
    sget-object v5, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    sget-boolean v5, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxMounted:Z

    if-eqz v5, :cond_0

    .line 4496
    :cond_5
    const-string v5, "/storage/UsbDrive"

    invoke-virtual {v1, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 4498
    invoke-virtual {v1, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/Utils;->isMountedUsbStorageFolder(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    goto :goto_0

    .line 4472
    :sswitch_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 4474
    .local v0, "checkExistFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_3

    goto :goto_0

    .line 4505
    .end local v0    # "checkExistFile":Ljava/io/File;
    .end local v1    # "file":Ljava/lang/String;
    :cond_6
    const/4 v4, 0x1

    goto :goto_0

    .line 4463
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_0
        0x4 -> :sswitch_0
        0x5 -> :sswitch_0
        0x7 -> :sswitch_0
        0x201 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method protected appendCustomArgsToStartCopyOrMoveArgs(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "arg"    # Landroid/os/Bundle;

    .prologue
    .line 4212
    return-void
.end method

.method protected checkShortcutValidationAfterMoveToKnox()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2257
    const-string v3, "AbsBrowserFragment"

    const-string v4, "check shortcut."

    invoke-static {v5, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 2260
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mStartMoveToKnox:Z

    if-eqz v3, :cond_4

    .line 2262
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mStartMoveToKnox:Z

    .line 2264
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mMoveToKnoxFileList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2266
    .local v1, "path":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSetHoemScreenPath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2268
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteFileShortcutFromHome(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 2272
    .end local v1    # "path":Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mMoveToKnoxFolderList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2274
    .restart local v1    # "path":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->isSetHoemScreenPath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2276
    sget-char v3, Ljava/io/File;->separatorChar:C

    invoke-virtual {v1, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 2278
    .local v2, "shortcutName":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v1, v2}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteShortcutFromHome(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2282
    .end local v1    # "path":Ljava/lang/String;
    .end local v2    # "shortcutName":Ljava/lang/String;
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mMoveToKnoxFileList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 2284
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mMoveToKnoxFolderList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 2286
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_4
    return-void
.end method

.method public clearHistoricalPaths()V
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->clearHistoricalPaths()V

    .line 327
    :cond_0
    return-void
.end method

.method protected deleteSelectedItem()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    .line 3731
    invoke-static {v9}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(I)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v2

    .line 3733
    .local v2, "fragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    invoke-virtual {v2, p0, v9}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 3735
    const/4 v5, 0x0

    .line 3743
    .local v5, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    instance-of v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    if-eqz v7, :cond_2

    .line 3744
    new-instance v5, Ljava/util/ArrayList;

    .end local v5    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 3745
    .restart local v5    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget v7, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mContextMenuPosition:I

    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getBrowserItem(I)Lcom/sec/android/app/myfiles/element/BrowserItem;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3752
    :cond_0
    :goto_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 3754
    .local v6, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v5, :cond_3

    .line 3756
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 3758
    .local v4, "selectedItem":Ljava/lang/Object;
    if-eqz v4, :cond_1

    .line 3761
    check-cast v4, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .end local v4    # "selectedItem":Ljava/lang/Object;
    iget-object v7, v4, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 3747
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v6    # "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v7, :cond_0

    .line 3748
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v5

    goto :goto_0

    .line 3765
    .restart local v6    # "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_3
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 3767
    .local v0, "arguments":Landroid/os/Bundle;
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v7, :cond_4

    .line 3769
    const-string v7, "dst_folder"

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3772
    :cond_4
    const-string v7, "target_datas"

    invoke-virtual {v0, v7, v6}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 3774
    const-string v7, "src_fragment_id"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3776
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    if-eqz v7, :cond_5

    .line 3780
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    const-string v8, "delete"

    invoke-virtual {v2, v7, v8}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3790
    :goto_2
    return-void

    .line 3782
    :catch_0
    move-exception v1

    .line 3784
    .local v1, "ex":Ljava/lang/IllegalStateException;
    const-string v7, "AbsBrowserFragment"

    const-string v8, "deleteSelectedItem: IllegalStateException."

    invoke-static {v9, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 3787
    .end local v1    # "ex":Ljava/lang/IllegalStateException;
    :cond_5
    const/4 v7, 0x0

    const-string v8, "AbsBrowserFragment"

    const-string v9, "deleteSelectedItem() : getFragmentManager() is null"

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public finishDragMode()V
    .locals 1

    .prologue
    .line 1283
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mIsDragMode:Z

    .line 1284
    return-void
.end method

.method public finishSelectMode()V
    .locals 0

    .prologue
    .line 6616
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->finishSelectMode()V

    .line 6617
    return-void
.end method

.method protected bridge synthetic getAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter;
    .locals 1

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-result-object v0

    return-object v0
.end method

.method protected getAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
    .locals 2

    .prologue
    .line 3695
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-nez v0, :cond_0

    .line 3697
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getAdapter(I)Landroid/widget/BaseAdapter;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    .line 3700
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    return-object v0
.end method

.method public getBrowserItem(I)Lcom/sec/android/app/myfiles/element/BrowserItem;
    .locals 7
    .param p1, "position"    # I

    .prologue
    .line 2372
    const/4 v4, 0x0

    .line 2373
    .local v4, "item":Lcom/sec/android/app/myfiles/element/BrowserItem;
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v5, :cond_2

    .line 2374
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v5, p1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 2376
    .local v0, "cursor":Landroid/database/Cursor;
    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-gt v5, v6, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    if-ltz v5, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-nez v5, :cond_1

    .line 2377
    :cond_0
    const/4 v5, 0x0

    .line 2386
    .end local v0    # "cursor":Landroid/database/Cursor;
    :goto_0
    return-object v5

    .line 2380
    .restart local v0    # "cursor":Landroid/database/Cursor;
    :cond_1
    const-string v5, "_id"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 2382
    .local v2, "id":J
    const-string v5, "_data"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2384
    .local v1, "data":Ljava/lang/String;
    new-instance v4, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .end local v4    # "item":Lcom/sec/android/app/myfiles/element/BrowserItem;
    const/16 v5, 0x11

    invoke-direct {v4, v5, v2, v3, v1}, Lcom/sec/android/app/myfiles/element/BrowserItem;-><init>(IJLjava/lang/String;)V

    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v1    # "data":Ljava/lang/String;
    .end local v2    # "id":J
    .restart local v4    # "item":Lcom/sec/android/app/myfiles/element/BrowserItem;
    :cond_2
    move-object v5, v4

    .line 2386
    goto :goto_0
.end method

.method public getCurrentSortBy()I
    .locals 1

    .prologue
    .line 3706
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mCurrentSortBy:I

    return v0
.end method

.method protected getDragFileIcon(Ljava/lang/String;)I
    .locals 1
    .param p1, "itemPath"    # Ljava/lang/String;

    .prologue
    .line 1169
    invoke-static {p1}, Lcom/sec/android/app/myfiles/MediaFile;->getSmallIcon(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method protected getDragFileText(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "itemPath"    # Ljava/lang/String;

    .prologue
    .line 1175
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getFileNameInPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getSelectedItemStringForRename(Lcom/sec/android/app/myfiles/element/BrowserItem;)Ljava/lang/String;
    .locals 1
    .param p1, "selectedItem"    # Lcom/sec/android/app/myfiles/element/BrowserItem;

    .prologue
    .line 4441
    if-eqz p1, :cond_0

    .line 4443
    iget-object v0, p1, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    .line 4447
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getSelectedItemsPathAsIntent(Ljava/util/ArrayList;)Landroid/content/Intent;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 4648
    .local p1, "selectedItemPos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v5, 0x0

    .line 4650
    .local v5, "result":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectionType()I

    move-result v7

    .line 4652
    .local v7, "selectionType":I
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 4654
    .local v6, "selectedItemCount":I
    packed-switch v7, :pswitch_data_0

    :cond_0
    :goto_0
    move-object v8, v5

    .line 4795
    :goto_1
    return-object v8

    .line 4658
    :pswitch_0
    const/4 v8, 0x1

    if-ne v6, v8, :cond_0

    .line 4660
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    const/4 v8, 0x0

    invoke-virtual {p1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v9, v8}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 4662
    .local v0, "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_1

    .line 4664
    const/4 v8, 0x0

    goto :goto_1

    .line 4667
    :cond_1
    const-string v8, "_data"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 4669
    .local v3, "path":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v8

    const/16 v9, 0x1f

    if-ne v8, v9, :cond_3

    .line 4671
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/storage/sdcard0/Android/data/com.baidu.netdisk/files/scratch"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4678
    :cond_2
    :goto_2
    new-instance v5, Landroid/content/Intent;

    .end local v5    # "result":Landroid/content/Intent;
    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 4680
    .restart local v5    # "result":Landroid/content/Intent;
    const-string v8, "FILE"

    invoke-virtual {v5, v8, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4682
    const-string v8, "FILE_URI"

    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v9, v3}, Lcom/sec/android/app/myfiles/utils/FileUtils;->pathToUri(Landroid/app/Activity;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 4684
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v8}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 4686
    const-string v8, "CONTENT_TYPE"

    invoke-static {v3}, Lcom/sec/android/app/myfiles/MediaFile;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 4673
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v8

    const/16 v9, 0x8

    if-ne v8, v9, :cond_2

    .line 4675
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/storage/sdcard0/Android/data/com.dropbox.android/files/scratch"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 4694
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v3    # "path":Ljava/lang/String;
    :pswitch_1
    const/16 v8, 0x64

    if-le v6, v8, :cond_4

    .line 4696
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b0066

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const/16 v12, 0x64

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 4700
    .local v2, "msg":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/4 v9, 0x1

    invoke-static {v8, v2, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 4704
    .end local v2    # "msg":Ljava/lang/String;
    :cond_4
    new-array v4, v6, [Ljava/lang/String;

    .line 4706
    .local v4, "pathList":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_3
    if-ge v1, v6, :cond_8

    .line 4708
    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v9, v8}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 4710
    .restart local v0    # "cursor":Landroid/database/Cursor;
    if-nez v0, :cond_5

    .line 4712
    const/4 v8, 0x0

    goto/16 :goto_1

    .line 4715
    :cond_5
    const-string v8, "_data"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 4717
    .restart local v3    # "path":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v8

    const/16 v9, 0x1f

    if-ne v8, v9, :cond_7

    .line 4719
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/storage/sdcard0/Android/data/com.baidu.netdisk/files/scratch"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4726
    :cond_6
    :goto_4
    aput-object v3, v4, v1

    .line 4706
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 4721
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v8

    const/16 v9, 0x8

    if-ne v8, v9, :cond_6

    .line 4723
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/storage/sdcard0/Android/data/com.dropbox.android/files/scratch"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_4

    .line 4729
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v3    # "path":Ljava/lang/String;
    :cond_8
    new-instance v5, Landroid/content/Intent;

    .end local v5    # "result":Landroid/content/Intent;
    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 4731
    .restart local v5    # "result":Landroid/content/Intent;
    const-string v8, "FILE"

    invoke-virtual {v5, v8, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 4739
    .end local v1    # "i":I
    .end local v4    # "pathList":[Ljava/lang/String;
    :pswitch_2
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v3

    .line 4741
    .restart local v3    # "path":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_b

    .line 4743
    new-instance v5, Landroid/content/Intent;

    .end local v5    # "result":Landroid/content/Intent;
    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 4745
    .restart local v5    # "result":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v8

    const/16 v9, 0x8

    if-ne v8, v9, :cond_a

    .line 4747
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Dropbox/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 4754
    :cond_9
    :goto_5
    const-string v8, "FILE"

    invoke-virtual {v5, v8, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 4749
    :cond_a
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v8

    const/16 v9, 0x1f

    if-ne v8, v9, :cond_9

    .line 4751
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Baidu/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_5

    .line 4756
    :cond_b
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v8

    const/16 v9, 0x8

    if-eq v8, v9, :cond_c

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v8

    const/16 v9, 0x1f

    if-ne v8, v9, :cond_0

    .line 4758
    :cond_c
    new-instance v5, Landroid/content/Intent;

    .end local v5    # "result":Landroid/content/Intent;
    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 4760
    .restart local v5    # "result":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v8

    const/16 v9, 0x1f

    if-ne v8, v9, :cond_d

    .line 4762
    const-string v3, "Baidu/"

    .line 4769
    :goto_6
    const-string v8, "FILE"

    invoke-virtual {v5, v8, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 4766
    :cond_d
    const-string v3, "Dropbox/"

    goto :goto_6

    .line 4778
    .end local v3    # "path":Ljava/lang/String;
    :pswitch_3
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v3

    .line 4780
    .restart local v3    # "path":Ljava/lang/String;
    const/4 v8, 0x2

    const-string v9, "AbsBrowserFragment"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "path="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", getFragmentId()="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 4782
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 4784
    const/4 v8, 0x2

    const-string v9, "AbsBrowserFragment"

    const-string v10, "isEmpty()"

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 4786
    new-instance v5, Landroid/content/Intent;

    .end local v5    # "result":Landroid/content/Intent;
    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 4788
    .restart local v5    # "result":Landroid/content/Intent;
    const-string v8, "uri"

    invoke-virtual {v5, v8, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_0

    .line 4654
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getSelectorCategoryType()I
    .locals 1

    .prologue
    .line 6620
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectorCategoryType:I

    return v0
.end method

.method public getmAdapter()Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
    .locals 1

    .prologue
    .line 6602
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    return-object v0
.end method

.method public getmCallback()Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$onCopyMoveSelectedListener;
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mCallback:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$onCopyMoveSelectedListener;

    return-object v0
.end method

.method public getmIsSelector()Z
    .locals 1

    .prologue
    .line 330
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mIsSelector:Z

    return v0
.end method

.method protected handleOnDropFromFtp(ILjava/lang/String;Ljava/util/ArrayList;Z)V
    .locals 12
    .param p1, "targetFragmentId"    # I
    .param p2, "targetFolder"    # Ljava/lang/String;
    .param p4, "move"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1182
    .local p3, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v9

    .line 1184
    .local v9, "size":I
    if-lez v9, :cond_2

    .line 1186
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getFtpParamsString()Ljava/lang/String;

    move-result-object v4

    .line 1188
    .local v4, "ftpParamsString":Ljava/lang/String;
    const/4 v2, 0x0

    .line 1192
    .local v2, "ftpParams":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    :try_start_0
    new-instance v3, Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-direct {v3, v4}, Lcom/sec/android/app/myfiles/ftp/FTPParams;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "ftpParams":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .local v3, "ftpParams":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    move-object v2, v3

    .line 1196
    .end local v3    # "ftpParams":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .restart local v2    # "ftpParams":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    :goto_0
    if-eqz v2, :cond_2

    .line 1198
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1200
    .local v7, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 1202
    .local v8, "json":Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getItem(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPItem;

    move-result-object v6

    .line 1204
    .local v6, "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    if-eqz v6, :cond_0

    .line 1206
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1210
    .end local v6    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .end local v8    # "json":Ljava/lang/String;
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 1245
    .end local v2    # "ftpParams":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .end local v4    # "ftpParamsString":Ljava/lang/String;
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v7    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    :cond_2
    :goto_2
    return-void

    .line 1216
    .restart local v2    # "ftpParams":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .restart local v4    # "ftpParamsString":Ljava/lang/String;
    .restart local v5    # "i$":Ljava/util/Iterator;
    .restart local v7    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    :pswitch_0
    const/4 v10, 0x0

    invoke-static {v10}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(I)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v1

    .line 1218
    .local v1, "frag":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    const/16 v10, 0x12

    invoke-virtual {v1, p0, v10}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 1220
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 1222
    .local v0, "args":Landroid/os/Bundle;
    const-string v10, "dst_folder"

    invoke-virtual {v0, v10, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1224
    const-string v10, "target_datas"

    invoke-virtual {v0, v10, p3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1226
    const-string v10, "src_fragment_id"

    const/16 v11, 0xa

    invoke-virtual {v0, v10, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1228
    const-string v10, "dest_fragment_id"

    invoke-virtual {v0, v10, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1230
    const-string v10, "src-ftp-param"

    invoke-virtual {v0, v10, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1232
    invoke-virtual {v1, v0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setArguments(Landroid/os/Bundle;)V

    .line 1234
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v10

    const-string v11, "createFolder"

    invoke-virtual {v1, v10, v11}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_2

    .line 1194
    .end local v0    # "args":Landroid/os/Bundle;
    .end local v1    # "frag":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v7    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    :catch_0
    move-exception v10

    goto :goto_0

    .line 1210
    :pswitch_data_0
    .packed-switch 0x201
        :pswitch_0
    .end packed-switch
.end method

.method protected initFileObserver()V
    .locals 1

    .prologue
    .line 615
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFileObserver:Landroid/os/FileObserver;

    if-eqz v0, :cond_0

    .line 617
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFileObserver:Landroid/os/FileObserver;

    invoke-virtual {v0}, Landroid/os/FileObserver;->stopWatching()V

    .line 618
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFileObserver:Landroid/os/FileObserver;

    .line 620
    :cond_0
    return-void
.end method

.method public initiateToDeleteHoveredItem(Ljava/lang/String;)V
    .locals 8
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 3825
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 3826
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    const v3, 0x7f0b0020

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 3828
    const/4 v2, 0x1

    .line 3830
    .local v2, "selectedItemCount":I
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0004

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v2, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 3833
    .local v1, "selectText":Ljava/lang/String;
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 3836
    const v3, 0x7f0b0015

    new-instance v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$21;

    invoke-direct {v4, p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$21;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;Ljava/lang/String;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 3848
    const v3, 0x7f0b0017

    new-instance v4, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$22;

    invoke-direct {v4, p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$22;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 3856
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 3857
    return-void
.end method

.method protected isNotMovingToPrivate(Ljava/lang/String;)Z
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 3441
    sget-object v0, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ismIsHomeBtnSelected()Z
    .locals 1

    .prologue
    .line 297
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mIsHomeBtnSelected:Z

    return v0
.end method

.method protected moveBaiduToSecretBox()V
    .locals 8

    .prologue
    .line 3934
    const/4 v6, 0x1

    invoke-static {v6}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(I)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v1

    .line 3936
    .local v1, "fragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    const/4 v6, 0x2

    invoke-virtual {v1, p0, v6}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 3938
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v4

    .line 3940
    .local v4, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 3942
    .local v5, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 3944
    .local v3, "selectedItem":Ljava/lang/Object;
    check-cast v3, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .end local v3    # "selectedItem":Ljava/lang/Object;
    iget-object v6, v3, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 3947
    :cond_0
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 3949
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v6, "src_fragment_id"

    const/16 v7, 0x1f

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3951
    const-string v6, "dest_fragment_id"

    const/16 v7, 0x201

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3953
    const-string v6, "dst_folder"

    sget-object v7, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3955
    const-string v6, "target_datas"

    invoke-virtual {v0, v6, v5}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 3957
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "delete"

    invoke-virtual {v1, v6, v7}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 3958
    return-void
.end method

.method protected moveDropboxToSecretBox()V
    .locals 8

    .prologue
    .line 3906
    const/4 v6, 0x1

    invoke-static {v6}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(I)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v1

    .line 3908
    .local v1, "fragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    const/4 v6, 0x2

    invoke-virtual {v1, p0, v6}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 3910
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v4

    .line 3912
    .local v4, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 3914
    .local v5, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 3916
    .local v3, "selectedItem":Ljava/lang/Object;
    check-cast v3, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .end local v3    # "selectedItem":Ljava/lang/Object;
    iget-object v6, v3, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 3919
    :cond_0
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 3921
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v6, "src_fragment_id"

    const/16 v7, 0x8

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3923
    const-string v6, "dest_fragment_id"

    const/16 v7, 0x201

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3925
    const-string v6, "dst_folder"

    sget-object v7, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3927
    const-string v6, "target_datas"

    invoke-virtual {v0, v6, v5}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 3929
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "delete"

    invoke-virtual {v1, v6, v7}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 3930
    return-void
.end method

.method protected moveSecretBox()V
    .locals 8

    .prologue
    .line 3880
    const/4 v6, 0x1

    invoke-static {v6}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(I)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v1

    .line 3882
    .local v1, "fragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    const/4 v6, 0x2

    invoke-virtual {v1, p0, v6}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 3884
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v4

    .line 3886
    .local v4, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 3888
    .local v5, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 3890
    .local v3, "selectedItem":Ljava/lang/Object;
    check-cast v3, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .end local v3    # "selectedItem":Ljava/lang/Object;
    iget-object v6, v3, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 3893
    :cond_0
    if-eqz v1, :cond_1

    .line 3894
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 3896
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v6, "dst_folder"

    sget-object v7, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 3898
    const-string v6, "target_datas"

    invoke-virtual {v0, v6, v5}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 3900
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "delete"

    invoke-virtual {v1, v6, v7}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 3902
    .end local v0    # "arguments":Landroid/os/Bundle;
    :cond_1
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 27
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 3035
    invoke-super/range {p0 .. p3}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 3037
    if-nez p3, :cond_0

    .line 3039
    const/4 v2, 0x0

    const-string v3, "AbsBrowserFragment"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onActivityResult() requestCode : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p1

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", data is null"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v3, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 3042
    :cond_0
    const/16 v18, 0x0

    .line 3043
    .local v18, "mSelectedList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    const/16 v2, 0x17

    move/from16 v0, p1

    if-eq v0, v2, :cond_1

    const/16 v2, 0x19

    move/from16 v0, p1

    if-ne v0, v2, :cond_2

    .line 3044
    :cond_1
    move-object/from16 v0, p0

    instance-of v2, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    if-eqz v2, :cond_4

    .line 3045
    new-instance v18, Ljava/util/ArrayList;

    .end local v18    # "mSelectedList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 3046
    .restart local v18    # "mSelectedList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mContextMenuPosition:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getBrowserItem(I)Lcom/sec/android/app/myfiles/element/BrowserItem;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 3052
    :cond_2
    :goto_0
    packed-switch p1, :pswitch_data_0

    .line 3437
    :cond_3
    :goto_1
    :pswitch_0
    return-void

    .line 3049
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v18

    goto :goto_0

    .line 3056
    :pswitch_1
    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_3

    .line 3058
    const/4 v2, 0x2

    const-string v3, "AbsBrowserFragment"

    const-string v6, "RESULT_OK for REQUEST_SELECT_PATH_FOR_MOVE"

    invoke-static {v2, v3, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 3060
    if-eqz p3, :cond_3

    .line 3062
    const-string v2, "FILE"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 3064
    .local v5, "path":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 3066
    const-string v2, "dest_fragment_id"

    const/4 v3, -0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 3068
    .local v4, "dstFragmentId":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v3

    const/4 v6, 0x1

    move-object/from16 v2, p0

    move-object/from16 v7, p3

    invoke-direct/range {v2 .. v7}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startCopyOrMove(IILjava/lang/String;ILandroid/content/Intent;)V

    goto :goto_1

    .line 3078
    .end local v4    # "dstFragmentId":I
    .end local v5    # "path":Ljava/lang/String;
    :pswitch_2
    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_3

    .line 3080
    const/4 v2, 0x2

    const-string v3, "AbsBrowserFragment"

    const-string v6, "RESULT_OK for REQUEST_SELECT_PATH_FOR_COPY"

    invoke-static {v2, v3, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 3082
    if-eqz p3, :cond_3

    .line 3084
    const-string v2, "FILE"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 3086
    .restart local v5    # "path":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 3088
    const-string v2, "dest_fragment_id"

    const/4 v3, -0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 3090
    .restart local v4    # "dstFragmentId":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v3

    const/4 v6, 0x0

    move-object/from16 v2, p0

    move-object/from16 v7, p3

    invoke-direct/range {v2 .. v7}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startCopyOrMove(IILjava/lang/String;ILandroid/content/Intent;)V

    goto :goto_1

    .line 3101
    .end local v4    # "dstFragmentId":I
    .end local v5    # "path":Ljava/lang/String;
    :pswitch_3
    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_13

    .line 3102
    if-eqz p3, :cond_7

    .line 3103
    const-string v2, "FILE_OPERATION"

    const/4 v3, -0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v19

    .line 3105
    .local v19, "operationType":I
    const/4 v2, 0x2

    move/from16 v0, v19

    if-ne v0, v2, :cond_5

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v3, 0x12

    if-ne v2, v3, :cond_5

    .line 3106
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onRefresh()V

    goto/16 :goto_1

    .line 3109
    :cond_5
    move-object/from16 v0, p0

    instance-of v2, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    if-eqz v2, :cond_7

    if-eqz v19, :cond_6

    const/4 v2, 0x1

    move/from16 v0, v19

    if-eq v0, v2, :cond_6

    const/4 v2, 0x4

    move/from16 v0, v19

    if-ne v0, v2, :cond_7

    .line 3112
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->finishSelectMode()V

    .line 3117
    .end local v19    # "operationType":I
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectModeFrom()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_8

    move-object/from16 v0, p0

    instance-of v2, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    if-nez v2, :cond_8

    .line 3119
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->finishSelectMode()V

    .line 3124
    :cond_8
    const/16 v2, 0x12

    move/from16 v0, p1

    if-ne v0, v2, :cond_12

    .line 3126
    if-eqz p3, :cond_11

    move-object/from16 v17, p3

    .line 3128
    .local v17, "intent":Landroid/content/Intent;
    :goto_2
    const-string v2, "id"

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v3

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3130
    const-string v2, "com.sec.android.action.FILE_OPERATION_DONE"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 3132
    const-string v2, "success"

    const/4 v3, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3134
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 3136
    const/4 v2, 0x0

    const-string v3, "AbsBrowserFragment"

    const-string v6, "notify the file operation by drag & drop is done"

    invoke-static {v2, v3, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 3147
    .end local v17    # "intent":Landroid/content/Intent;
    :cond_9
    :goto_3
    if-eqz p3, :cond_10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v2, :cond_a

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v2

    const/16 v3, 0x201

    if-eq v2, v3, :cond_b

    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isCategoryFolder(I)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 3150
    :cond_b
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v10

    .line 3152
    .local v10, "bundle":Landroid/os/Bundle;
    if-eqz v10, :cond_10

    .line 3154
    const-string v2, "from"

    move/from16 v0, p1

    invoke-virtual {v10, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 3156
    const-string v2, "target_folder"

    invoke-virtual {v10, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 3158
    .restart local v5    # "path":Ljava/lang/String;
    const-string v2, "dest_fragment_id"

    const/4 v3, -0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 3160
    .restart local v4    # "dstFragmentId":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v2

    const/16 v3, 0x201

    if-ne v2, v3, :cond_c

    const/16 v2, 0xa

    if-eq v4, v2, :cond_c

    .line 3162
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->isNotMovingToPrivate(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 3164
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v5, v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z

    .line 3167
    :cond_c
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mIsSelector:Z

    .line 3169
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v9

    .line 3171
    .local v9, "args":Landroid/os/Bundle;
    if-eqz v9, :cond_e

    .line 3173
    const-string v2, "from_back"

    invoke-virtual {v9, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 3175
    const-string v2, "from_back"

    invoke-virtual {v9, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 3178
    :cond_d
    const-string v2, "from_back"

    const/4 v3, 0x0

    invoke-virtual {v9, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 3180
    :cond_e
    const-string v2, "FILE_OPERATION"

    const/4 v3, -0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v19

    .line 3182
    .restart local v19    # "operationType":I
    const/4 v2, 0x1

    move/from16 v0, v19

    if-eq v0, v2, :cond_f

    if-nez v19, :cond_10

    .line 3184
    :cond_f
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->isNotMovingToPrivate(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 3186
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4, v5, v10}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->switchFragments(IILjava/lang/String;Landroid/os/Bundle;)V

    .line 3221
    .end local v4    # "dstFragmentId":I
    .end local v5    # "path":Ljava/lang/String;
    .end local v9    # "args":Landroid/os/Bundle;
    .end local v10    # "bundle":Landroid/os/Bundle;
    .end local v19    # "operationType":I
    :cond_10
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    new-instance v3, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$19;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$19;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_1

    .line 3126
    :cond_11
    new-instance v17, Landroid/content/Intent;

    invoke-direct/range {v17 .. v17}, Landroid/content/Intent;-><init>()V

    goto/16 :goto_2

    .line 3138
    :cond_12
    const/4 v2, 0x2

    move/from16 v0, p1

    if-ne v0, v2, :cond_9

    .line 3140
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    if-eqz v2, :cond_9

    .line 3141
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    const/4 v6, 0x1

    invoke-virtual {v2, v3, v6}, Lcom/samsung/android/privatemode/PrivateModeManager;->unregisterClient(Landroid/os/IBinder;Z)Z

    .line 3142
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    goto/16 :goto_3

    .line 3193
    :cond_13
    if-eqz p3, :cond_15

    .line 3195
    const-string v2, "FILE_OPERATION"

    const/4 v3, -0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v19

    .line 3197
    .restart local v19    # "operationType":I
    const/4 v2, 0x1

    move/from16 v0, v19

    if-eq v0, v2, :cond_14

    if-eqz v19, :cond_14

    const/4 v2, 0x2

    move/from16 v0, v19

    if-ne v0, v2, :cond_16

    .line 3199
    :cond_14
    move-object/from16 v0, p0

    instance-of v2, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    if-nez v2, :cond_15

    .line 3201
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->finishSelectMode()V

    .line 3210
    .end local v19    # "operationType":I
    :cond_15
    :goto_5
    if-nez p2, :cond_10

    .line 3212
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    if-eqz v2, :cond_10

    .line 3214
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v6}, Lcom/samsung/android/privatemode/PrivateModeManager;->unregisterClient(Landroid/os/IBinder;Z)Z

    .line 3216
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPrivateModeBinder:Landroid/os/IBinder;

    goto :goto_4

    .line 3204
    .restart local v19    # "operationType":I
    :cond_16
    const/4 v2, 0x7

    move/from16 v0, v19

    if-ne v0, v2, :cond_15

    .line 3206
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->finishSelectMode()V

    goto :goto_5

    .line 3232
    .end local v19    # "operationType":I
    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onRefresh()V

    .line 3234
    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_3

    .line 3236
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->openDownloadedFile()V

    goto/16 :goto_1

    .line 3243
    :pswitch_5
    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_3

    .line 3245
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->openDownloadedFile()V

    goto/16 :goto_1

    .line 3253
    :pswitch_6
    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_3

    if-eqz p3, :cond_3

    .line 3255
    const-string v2, "settingChanged"

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mbSettingChanged:Z

    goto/16 :goto_1

    .line 3261
    :pswitch_7
    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_3

    .line 3262
    const/4 v2, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .line 3263
    .local v22, "selectedItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/32 v6, 0x1312d00

    cmp-long v2, v2, v6

    if-ltz v2, :cond_17

    .line 3264
    new-instance v2, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const/4 v6, 0x3

    invoke-direct {v2, v3, v6}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;-><init>(Landroid/content/Context;I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mLockFileThread:Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;

    .line 3271
    :goto_6
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mLockFileThread:Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mContextpath:Ljava/lang/String;

    aput-object v7, v3, v6

    const/4 v6, 0x1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mContextpath:Ljava/lang/String;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v26, "."

    move-object/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v26, "enc"

    move-object/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v6

    const/4 v6, 0x2

    const-string v7, "lock"

    aput-object v7, v3, v6

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3275
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->isSelectMode()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 3276
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->finishSelectMode()V

    goto/16 :goto_1

    .line 3267
    :cond_17
    new-instance v2, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const/4 v6, 0x4

    invoke-direct {v2, v3, v6}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;-><init>(Landroid/content/Context;I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mLockFileThread:Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;

    goto :goto_6

    .line 3272
    :catch_0
    move-exception v14

    .line 3273
    .local v14, "ex":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3275
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->isSelectMode()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 3276
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->finishSelectMode()V

    goto/16 :goto_1

    .line 3275
    .end local v14    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->isSelectMode()Z

    move-result v3

    if-eqz v3, :cond_18

    .line 3276
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->finishSelectMode()V

    :cond_18
    throw v2

    .line 3282
    .end local v22    # "selectedItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    :pswitch_8
    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_3

    .line 3283
    const/4 v2, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .line 3284
    .restart local v22    # "selectedItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/32 v6, 0x1312d00

    cmp-long v2, v2, v6

    if-ltz v2, :cond_19

    .line 3285
    new-instance v2, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const/4 v6, 0x7

    invoke-direct {v2, v3, v6}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;-><init>(Landroid/content/Context;I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mLockFileThread:Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;

    .line 3292
    :goto_7
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mLockFileThread:Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mContextpath:Ljava/lang/String;

    aput-object v7, v3, v6

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mContextpath:Ljava/lang/String;

    aput-object v7, v3, v6

    const/4 v6, 0x2

    const-string v7, "unlock"

    aput-object v7, v3, v6

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 3296
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->isSelectMode()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 3298
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->finishSelectMode()V

    goto/16 :goto_1

    .line 3288
    :cond_19
    new-instance v2, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const/16 v6, 0x8

    invoke-direct {v2, v3, v6}, Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;-><init>(Landroid/content/Context;I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mLockFileThread:Lcom/sec/android/app/myfiles/utils/EncryptionUtils$lockFileThread;

    goto :goto_7

    .line 3293
    :catch_1
    move-exception v14

    .line 3294
    .restart local v14    # "ex":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v14}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 3296
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->isSelectMode()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 3298
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->finishSelectMode()V

    goto/16 :goto_1

    .line 3296
    .end local v14    # "ex":Ljava/lang/Exception;
    :catchall_1
    move-exception v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->isSelectMode()Z

    move-result v3

    if-eqz v3, :cond_1a

    .line 3298
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->finishSelectMode()V

    :cond_1a
    throw v2

    .line 3307
    .end local v22    # "selectedItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    :pswitch_9
    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_3

    .line 3309
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectionType()I

    move-result v2

    if-nez v2, :cond_1c

    if-eqz p3, :cond_1c

    .line 3311
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "/storage/sdcard0/Android/data/com.dropbox.android/files/scratch"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v20

    .line 3313
    .local v20, "path":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v21, Landroid/content/Intent;

    invoke-direct/range {v21 .. v21}, Landroid/content/Intent;-><init>()V

    .line 3316
    .local v21, "result":Landroid/content/Intent;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/storage/sdcard0/Android/data/com.dropbox.android/files/scratch"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v2, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 3319
    .local v12, "dPath":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 3320
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "/storage/sdcard0/Android/data/com.baidu.netdisk/files/scratch"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v20

    .line 3321
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/storage/sdcard0/Android/data/com.baidu.netdisk/files/scratch"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v2, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 3322
    const/4 v2, 0x2

    const-string v3, "AbsBrowserFragment"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Baidu dPath:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v3, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 3325
    :cond_1b
    new-instance v15, Ljava/io/File;

    invoke-direct {v15, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3327
    .local v15, "f":Ljava/io/File;
    const-string v2, "FILE"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3329
    const-string v2, "FILE_URI"

    invoke-static {v15}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 3331
    invoke-static {v15}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 3333
    const-string v2, "CONTENT_TYPE"

    invoke-static {v12}, Lcom/sec/android/app/myfiles/MediaFile;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3335
    const/4 v2, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v2, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->setResultAndFinish(ILandroid/content/Intent;)V

    goto/16 :goto_1

    .line 3337
    .end local v12    # "dPath":Ljava/lang/String;
    .end local v15    # "f":Ljava/io/File;
    .end local v20    # "path":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v21    # "result":Landroid/content/Intent;
    :cond_1c
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectionType()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 3339
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemPosition()Ljava/util/ArrayList;

    move-result-object v24

    .line 3341
    .local v24, "selectedItemPos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v23

    .line 3347
    .local v23, "selectedItemCount":I
    move/from16 v0, v23

    new-array v13, v0, [Ljava/lang/String;

    .line 3350
    .local v13, "dpathList":[Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 3351
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_8
    move/from16 v0, v16

    move/from16 v1, v23

    if-ge v0, v1, :cond_1e

    .line 3352
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v0, v24

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v3, v2}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/database/Cursor;

    .line 3353
    .local v11, "cursor":Landroid/database/Cursor;
    const-string v2, "_data"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 3354
    .restart local v5    # "path":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/storage/sdcard0/Android/data/com.baidu.netdisk/files/scratch"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v13, v16

    .line 3351
    add-int/lit8 v16, v16, 0x1

    goto :goto_8

    .line 3358
    .end local v5    # "path":Ljava/lang/String;
    .end local v11    # "cursor":Landroid/database/Cursor;
    .end local v16    # "i":I
    :cond_1d
    const/16 v16, 0x0

    .restart local v16    # "i":I
    :goto_9
    move/from16 v0, v16

    move/from16 v1, v23

    if-ge v0, v1, :cond_1e

    .line 3359
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v0, v24

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v3, v2}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/database/Cursor;

    .line 3360
    .restart local v11    # "cursor":Landroid/database/Cursor;
    const-string v2, "_data"

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 3361
    .restart local v5    # "path":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/storage/sdcard0/Android/data/com.dropbox.android/files/scratch"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v13, v16

    .line 3358
    add-int/lit8 v16, v16, 0x1

    goto :goto_9

    .line 3365
    .end local v5    # "path":Ljava/lang/String;
    .end local v11    # "cursor":Landroid/database/Cursor;
    :cond_1e
    new-instance v21, Landroid/content/Intent;

    invoke-direct/range {v21 .. v21}, Landroid/content/Intent;-><init>()V

    .line 3369
    .restart local v21    # "result":Landroid/content/Intent;
    const-string v2, "FILE"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 3371
    const/4 v2, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v2, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->setResultAndFinish(ILandroid/content/Intent;)V

    goto/16 :goto_1

    .line 3380
    .end local v13    # "dpathList":[Ljava/lang/String;
    .end local v16    # "i":I
    .end local v21    # "result":Landroid/content/Intent;
    .end local v23    # "selectedItemCount":I
    .end local v24    # "selectedItemPos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :pswitch_a
    const/4 v2, -0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_3

    if-eqz p3, :cond_3

    .line 3382
    const/4 v5, 0x0

    .line 3384
    .restart local v5    # "path":Ljava/lang/String;
    const/4 v12, 0x0

    .line 3387
    .restart local v12    # "dPath":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v2

    if-eqz v2, :cond_21

    .line 3388
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "/storage/sdcard0/Android/data/com.baidu.netdisk/files/scratch"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "path":Ljava/lang/String;
    check-cast v5, Ljava/lang/String;

    .line 3389
    .restart local v5    # "path":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/storage/sdcard0/Android/data/com.baidu.netdisk/files/scratch"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 3395
    :goto_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v12, v2}, Lcom/sec/android/app/myfiles/MediaFile;->getShareMimeType(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    .line 3397
    .local v8, "MIMEType":Ljava/lang/String;
    invoke-static {v12}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/MediaFile;->isDrmFileType(I)Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 3398
    const-string v8, ""

    .line 3400
    :cond_1f
    new-instance v15, Ljava/io/File;

    invoke-direct {v15, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 3402
    .restart local v15    # "f":Ljava/io/File;
    invoke-static {v15}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v25

    .line 3404
    .local v25, "uri":Landroid/net/Uri;
    invoke-virtual {v15}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, ".txt"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_20

    const-string v2, "text/plain"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_20

    .line 3406
    const-string v8, "application/txt"

    .line 3409
    :cond_20
    const/4 v2, 0x2

    const-string v3, "AbsBrowserFragment"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "startShareAppList MIMEType = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v3, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 3411
    new-instance v17, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 3413
    .restart local v17    # "intent":Landroid/content/Intent;
    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 3415
    const-string v2, "from-myfiles"

    const/4 v3, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 3417
    const-string v2, "android.intent.extra.STREAM"

    move-object/from16 v0, v17

    move-object/from16 v1, v25

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 3419
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b001f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-static {v0, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 3391
    .end local v8    # "MIMEType":Ljava/lang/String;
    .end local v15    # "f":Ljava/io/File;
    .end local v17    # "intent":Landroid/content/Intent;
    .end local v25    # "uri":Landroid/net/Uri;
    :cond_21
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "/storage/sdcard0/Android/data/com.dropbox.android/files/scratch"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "path":Ljava/lang/String;
    check-cast v5, Ljava/lang/String;

    .line 3392
    .restart local v5    # "path":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/storage/sdcard0/Android/data/com.dropbox.android/files/scratch"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_a

    .line 3428
    .end local v5    # "path":Ljava/lang/String;
    .end local v12    # "dPath":Ljava/lang/String;
    :pswitch_b
    if-nez p2, :cond_22

    .line 3430
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v3, 0x7f0b00cb

    const/4 v6, 0x0

    invoke-static {v2, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 3433
    :cond_22
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->finishSelectMode()V

    goto/16 :goto_1

    .line 3052
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_b
        :pswitch_b
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 316
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->onAttach(Landroid/app/Activity;)V

    .line 318
    :try_start_0
    check-cast p1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$onCopyMoveSelectedListener;

    .end local p1    # "activity":Landroid/app/Activity;
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mCallback:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$onCopyMoveSelectedListener;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 322
    :goto_0
    return-void

    .line 319
    :catch_0
    move-exception v0

    .line 320
    .local v0, "e":Ljava/lang/ClassCastException;
    const/4 v1, 0x2

    const-string v2, "AbsBrowserFragment"

    const-string v3, "onAttach: classcastexception."

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 647
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 649
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->setActionBarIcon(I)V

    .line 667
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isNormalWindow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 678
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x1

    .line 344
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->onCreate(Landroid/os/Bundle;)V

    .line 346
    const-string v6, "VerificationLog"

    const-string v7, "AbsbrowserFragment onCreate"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6, p0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->setOnDropListener(Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;)V

    .line 351
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getNavigation()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .line 353
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v6, :cond_0

    .line 355
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigationObserver:Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;

    invoke-virtual {v6, v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->registerObserver(Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;)V

    .line 358
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 360
    .local v0, "argument":Landroid/os/Bundle;
    if-eqz v0, :cond_7

    .line 362
    const-string v6, "ISSELECTOR"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mIsSelector:Z

    .line 364
    const-string v6, "CONTENT_TYPE"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 366
    .local v5, "filterMimetype":Ljava/lang/String;
    const-string v6, "CONTENT_EXTENSION"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 368
    .local v4, "filterExtension":Ljava/lang/String;
    const-string v6, "EXCEPT_CONTENT_TYPE"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 370
    .local v3, "filterExceptMimetype":Ljava/lang/String;
    const-string v6, "EXCEPT_CONTENT_EXTENSION"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 372
    .local v2, "filterExceptExtension":Ljava/lang/String;
    const-string v6, "SELECTOR_CATEGORY_TYPE"

    const/4 v7, -0x1

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectorCategoryType:I

    .line 374
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 376
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6, v4}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->addFilterExtension(Ljava/lang/String;)V

    .line 378
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6, v8}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->setEnableFiltering(Z)V

    .line 381
    :cond_1
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 383
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6, v5}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->addFilterMimeType(Ljava/lang/String;)V

    .line 385
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6, v8}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->setEnableFiltering(Z)V

    .line 388
    :cond_2
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 390
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6, v2}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->addExceptFilterExtension(Ljava/lang/String;)V

    .line 392
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6, v8}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->setEnableFiltering(Z)V

    .line 395
    :cond_3
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 397
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6, v3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->addExceptFilterMimeType(Ljava/lang/String;)V

    .line 399
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6, v8}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->setEnableFiltering(Z)V

    .line 402
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v6, v6, Lcom/sec/android/app/myfiles/SelectorActivity;

    if-eqz v6, :cond_6

    .line 403
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v6, Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/SelectorActivity;->getmSelectionType()I

    move-result v6

    if-eq v6, v8, :cond_5

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v6, Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/SelectorActivity;->getmSelectionType()I

    move-result v6

    if-nez v6, :cond_6

    .line 405
    :cond_5
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 407
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6, v8}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->setEnableFiltering(Z)V

    .line 408
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6, v8}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->setDrmFiltering(Z)V

    .line 413
    :cond_6
    const-string v6, "JUST_SELECT_MODE"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mJustSelectMode:Z

    .line 415
    const-string v6, "reply_by_broadcast"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mReplyByBroadcast:Z

    .line 418
    .end local v2    # "filterExceptExtension":Ljava/lang/String;
    .end local v3    # "filterExceptMimetype":Ljava/lang/String;
    .end local v4    # "filterExtension":Ljava/lang/String;
    .end local v5    # "filterMimetype":Ljava/lang/String;
    :cond_7
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 435
    .local v1, "cr":Landroid/content/ContentResolver;
    new-instance v6, Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-direct {v6, v7, v1}, Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;-><init>(Landroid/content/Context;Landroid/content/ContentResolver;)V

    iput-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSideSyncControl:Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;

    .line 436
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    instance-of v6, v6, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v6, :cond_8

    .line 437
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/myfiles/MainActivity;

    iput-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    .line 440
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const-string v7, "input_method"

    invoke-virtual {v6, v7}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/inputmethod/InputMethodManager;

    iput-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    .line 442
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const-string v7, "quickconnect"

    invoke-virtual {v6, v7}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/quickconnect/QuickConnectManager;

    iput-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 444
    new-instance v6, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$1;

    invoke-direct {v6, p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V

    iput-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    .line 489
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    if-eqz v6, :cond_9

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    if-eqz v6, :cond_9

    .line 490
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    invoke-virtual {v6, v7}, Lcom/samsung/android/quickconnect/QuickConnectManager;->registerListener(Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;)V

    .line 493
    :cond_9
    return-void
.end method

.method protected abstract onCreateDetailFragmentListener()Landroid/app/DialogFragment;
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 20
    .param p1, "id"    # I

    .prologue
    .line 2713
    new-instance v6, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-direct {v6, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2715
    .local v6, "dialog":Landroid/app/AlertDialog$Builder;
    packed-switch p1, :pswitch_data_0

    .line 2917
    invoke-super/range {p0 .. p1}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v3

    .line 2920
    :goto_0
    return-object v3

    .line 2730
    :pswitch_0
    const v16, 0x7f0b0020

    new-instance v17, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$12;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$12;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V

    move/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v6, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2742
    const v16, 0x7f0b0017

    new-instance v17, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$13;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$13;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V

    move/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v6, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2920
    :goto_1
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    goto :goto_0

    .line 2759
    :pswitch_1
    move/from16 v7, p1

    .line 2761
    .local v7, "dialogId":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v14

    .line 2763
    .local v14, "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    const/16 v16, 0x4

    move/from16 v0, p1

    move/from16 v1, v16

    if-eq v0, v1, :cond_2

    .line 2765
    invoke-virtual {v14}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDoNotShowCheckDropBox()Z

    move-result v16

    if-eqz v16, :cond_2

    .line 2767
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    instance-of v0, v0, Lcom/sec/android/app/myfiles/MainActivity;

    move/from16 v16, v0

    if-eqz v16, :cond_0

    .line 2769
    const/16 v16, 0x5

    move/from16 v0, p1

    move/from16 v1, v16

    if-ne v0, v1, :cond_1

    .line 2770
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v16, v0

    check-cast v16, Lcom/sec/android/app/myfiles/MainActivity;

    const/16 v17, 0x1f

    const/16 v18, 0x0

    invoke-virtual/range {v16 .. v18}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    .line 2776
    :cond_0
    :goto_2
    const/4 v3, 0x0

    goto :goto_0

    .line 2773
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v16, v0

    check-cast v16, Lcom/sec/android/app/myfiles/MainActivity;

    const/16 v17, 0x8

    const/16 v18, 0x0

    invoke-virtual/range {v16 .. v18}, Lcom/sec/android/app/myfiles/MainActivity;->startBrowser(ILandroid/os/Bundle;)V

    goto :goto_2

    .line 2780
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v10

    .line 2782
    .local v10, "inflater":Landroid/view/LayoutInflater;
    const v16, 0x7f040032

    const/16 v17, 0x0

    move/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v10, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 2784
    .local v5, "customView":Landroid/view/View;
    const v16, 0x7f0f00c7

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    .line 2787
    .local v4, "checkDoNotShow":Landroid/widget/CheckBox;
    const v16, 0x7f0b0125

    move/from16 v0, v16

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 2789
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isChinaModel()Z

    move-result v16

    if-eqz v16, :cond_3

    .line 2790
    const v16, 0x7f0f00c5

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    .line 2792
    .local v13, "mobileDataWarningDialog":Landroid/widget/TextView;
    const v16, 0x7f0b00e0

    move/from16 v0, v16

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setText(I)V

    .line 2795
    .end local v13    # "mobileDataWarningDialog":Landroid/widget/TextView;
    :cond_3
    invoke-virtual {v6, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v16

    const/16 v17, 0x0

    invoke-virtual/range {v16 .. v17}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 2797
    new-instance v16, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$14;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$14;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V

    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 2812
    const v16, 0x7f0b017a

    new-instance v17, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$15;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v7, v14, v4}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$15;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;ILcom/sec/android/app/myfiles/utils/SharedDataStore;Landroid/widget/CheckBox;)V

    move/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v6, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2841
    const v16, 0x7f0b0017

    new-instance v17, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$16;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$16;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V

    move/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v6, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_1

    .line 2853
    .end local v4    # "checkDoNotShow":Landroid/widget/CheckBox;
    .end local v5    # "customView":Landroid/view/View;
    .end local v7    # "dialogId":I
    .end local v10    # "inflater":Landroid/view/LayoutInflater;
    .end local v14    # "sds":Lcom/sec/android/app/myfiles/utils/SharedDataStore;
    :pswitch_2
    const v16, 0x7f0b002a

    move/from16 v0, v16

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 2855
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v16

    const v17, 0x7f04002f

    const/16 v18, 0x0

    invoke-virtual/range {v16 .. v18}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v15

    .line 2857
    .local v15, "view":Landroid/view/View;
    const v16, 0x7f0f00c2

    invoke-virtual/range {v15 .. v16}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/GridView;

    .line 2859
    .local v8, "grid":Landroid/widget/GridView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPersona:Landroid/os/PersonaManager;

    move-object/from16 v16, v0

    invoke-static {}, Landroid/os/PersonaManager;->getKnoxInfo()Landroid/os/Bundle;

    move-result-object v16

    const-string v17, "KnoxIdNamePair"

    invoke-virtual/range {v16 .. v17}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    .line 2861
    .local v2, "KnoxIdNamePair":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/String;>;"
    if-nez v2, :cond_4

    .line 2862
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 2864
    :cond_4
    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .line 2865
    .local v9, "idSet":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mKnoxItems:Ljava/util/ArrayList;

    .line 2866
    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_5

    .line 2867
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/Integer;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Integer;->intValue()I

    move-result v11

    .line 2868
    .local v11, "knoxId":I
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 2869
    .local v12, "knoxName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mKnoxItems:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    new-instance v17, Lcom/sec/android/app/myfiles/element/PersonaItem;

    move-object/from16 v0, v17

    invoke-direct {v0, v11, v12}, Lcom/sec/android/app/myfiles/element/PersonaItem;-><init>(ILjava/lang/String;)V

    invoke-virtual/range {v16 .. v17}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2870
    const/16 v16, 0x2

    const-string v17, "AbsBrowserFragment"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "mKnoxItems add : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v16 .. v18}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 2873
    .end local v11    # "knoxId":I
    .end local v12    # "knoxName":Ljava/lang/String;
    :cond_5
    const/16 v16, 0x2

    const-string v17, "AbsBrowserFragment"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "mKnoxItems size : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mKnoxItems:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v16 .. v18}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 2874
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mKnoxItems:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v16

    if-nez v16, :cond_6

    .line 2875
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 2878
    :cond_6
    new-instance v16, Lcom/sec/android/app/myfiles/adapter/ContainerListAdapater;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mKnoxItems:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    invoke-direct/range {v16 .. v18}, Lcom/sec/android/app/myfiles/adapter/ContainerListAdapater;-><init>(Landroid/content/Context;Ljava/util/List;)V

    move-object/from16 v0, v16

    invoke-virtual {v8, v0}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2881
    invoke-virtual {v6, v15}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 2892
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    .line 2894
    .local v3, "alderDialog":Landroid/app/AlertDialog;
    new-instance v16, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$17;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$17;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V

    move-object/from16 v0, v16

    invoke-virtual {v8, v0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto/16 :goto_0

    .line 2715
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected abstract onCreateFileOperationListener()Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;
.end method

.method protected abstract onCreateMultipleDetailFragmentListener()Landroid/app/DialogFragment;
.end method

.method public onCreateSelectedItemChangeListener()Lcom/sec/android/app/myfiles/adapter/SelectedItemChangeListener;
    .locals 1

    .prologue
    .line 4962
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$26;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V

    return-object v0
.end method

.method protected onCreateTreeViewItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 4802
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$24;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v5, 0x28

    const/16 v4, 0x8

    .line 499
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 501
    .local v1, "view":Landroid/view/View;
    const-string v2, "VerificationLog"

    const-string v3, "AbsbrowserFragment onCreateview"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 503
    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v3, 0x201

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/4 v3, 0x3

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/4 v3, 0x4

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/4 v3, 0x5

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/4 v3, 0x7

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    if-eq v2, v4, :cond_0

    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v3, 0x1f

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v3, 0xa

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    if-eq v2, v5, :cond_0

    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v3, 0x9

    if-ne v2, v3, :cond_1

    .line 510
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectedItemsDragListener:Landroid/view/View$OnDragListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 512
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectedItemsDragListener:Landroid/view/View$OnDragListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 514
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mTwMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setTwMultiSelectedListener(Landroid/widget/AdapterView$OnTwMultiSelectedListener;)V

    .line 516
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mTwMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->setTwMultiSelectedListener(Landroid/widget/AdapterView$OnTwMultiSelectedListener;)V

    .line 518
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v2, p0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setOnDirectDragListener(Lcom/sec/android/app/myfiles/view/IDirectDragable$OnDirectDragListener;)V

    .line 520
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    invoke-virtual {v2, p0}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->setOnDirectDragListener(Lcom/sec/android/app/myfiles/view/IDirectDragable$OnDirectDragListener;)V

    .line 522
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$2;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V

    .line 562
    .local v0, "sl":Landroid/widget/AbsListView$OnScrollListener;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 563
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 565
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mEmptyView:Landroid/view/View;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectedItemsDragListener:Landroid/view/View$OnDragListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 571
    .end local v0    # "sl":Landroid/widget/AbsListView$OnScrollListener;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mEmptyView:Landroid/view/View;

    const v3, 0x7f0f00b0

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mEmptyTextView:Landroid/widget/TextView;

    .line 574
    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    if-ne v2, v5, :cond_2

    .line 576
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mEmptyTextView:Landroid/widget/TextView;

    const v3, 0x7f0b0149

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 579
    :cond_2
    iget v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v3, 0x12

    if-eq v2, v3, :cond_5

    .line 583
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mEmptyTextView:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 602
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    if-eqz v2, :cond_3

    .line 603
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mEmptyView:Landroid/view/View;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setEmptyView(Landroid/view/View;)V

    .line 606
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    if-eqz v2, :cond_4

    .line 607
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mEmptyView:Landroid/view/View;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->setEmptyView(Landroid/view/View;)V

    .line 610
    :cond_4
    return-object v1

    .line 599
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mEmptyTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 775
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->onDestroy()V

    .line 777
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->initFileObserver()V

    .line 780
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 782
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->initCursor()V

    .line 784
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 785
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 787
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v0, :cond_2

    .line 789
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigationObserver:Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->removeObserver(Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;)V

    .line 792
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    if-eqz v0, :cond_3

    .line 794
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mTreeView:Lcom/sec/android/app/myfiles/view/MyFilesTreeView;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/view/MyFilesTreeView;->destroy()V

    .line 797
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    if-eqz v0, :cond_4

    .line 798
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/quickconnect/QuickConnectManager;->unregisterListener(Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;)V

    .line 799
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mQuickConnectManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 800
    iput-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    .line 805
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mWaitProgressDlg:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mWaitProgressDlg:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 807
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mWaitProgressDlg:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->dismissWaitProgressDialog()V

    .line 809
    :cond_5
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->RemoveAllPaddingThumbnailMessage()V

    .line 821
    return-void
.end method

.method public onDirectDragStarted(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "argument"    # Landroid/os/Bundle;

    .prologue
    .line 3576
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startDragMode(Landroid/view/View;Landroid/os/Bundle;)V

    .line 3577
    return-void
.end method

.method public onDrop(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 18
    .param p1, "targetFolder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3528
    .local p2, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ge v3, v4, :cond_1

    .line 3529
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v4, 0x7f0b00cb

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 3570
    :goto_0
    return-void

    .line 3534
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const-string v4, "activity"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/app/ActivityManager;

    .line 3536
    .local v16, "activityManager":Landroid/app/ActivityManager;
    const/4 v3, 0x2

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v17

    .line 3538
    .local v17, "infos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/4 v3, 0x0

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v3, v3, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.sec.android.app.myfiles"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    const/4 v3, 0x1

    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v3, v3, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.sec.android.app.myfiles"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 3541
    const/4 v8, 0x1

    .line 3543
    .local v8, "operation":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDragSourceFragmentId()I

    move-result v3

    const/16 v4, 0xa

    if-ne v3, v4, :cond_3

    .line 3545
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v4

    const/4 v3, 0x1

    if-ne v8, v3, :cond_2

    const/4 v3, 0x1

    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual {v0, v4, v1, v2, v3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->handleOnDropFromFtp(ILjava/lang/String;Ljava/util/ArrayList;Z)V

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    .line 3547
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDragSourceFragmentId()I

    move-result v3

    const/16 v4, 0x1f

    if-eq v3, v4, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v3

    const/16 v4, 0x1f

    if-ne v3, v4, :cond_5

    .line 3549
    :cond_4
    const/4 v8, 0x0

    .line 3551
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDragSourceFragmentId()I

    move-result v4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v5

    const/16 v9, 0x12

    move-object/from16 v3, p0

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    invoke-virtual/range {v3 .. v9}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startCopyOrMove(IILjava/lang/String;Ljava/util/ArrayList;II)V

    goto/16 :goto_0

    .line 3554
    :cond_5
    if-eqz p2, :cond_6

    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/Utils;->getStorage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/myfiles/utils/Utils;->getStorage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->getStorage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 3556
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDragSourceFragmentId()I

    move-result v10

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v11

    const/4 v14, 0x1

    const/16 v15, 0x12

    move-object/from16 v9, p0

    move-object/from16 v12, p1

    move-object/from16 v13, p2

    invoke-virtual/range {v9 .. v15}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startCopyOrMove(IILjava/lang/String;Ljava/util/ArrayList;II)V

    goto/16 :goto_0

    .line 3560
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDragSourceFragmentId()I

    move-result v10

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v11

    const/4 v14, 0x0

    const/16 v15, 0x12

    move-object/from16 v9, p0

    move-object/from16 v12, p1

    move-object/from16 v13, p2

    invoke-virtual/range {v9 .. v15}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startCopyOrMove(IILjava/lang/String;Ljava/util/ArrayList;II)V

    goto/16 :goto_0

    .line 3565
    .end local v8    # "operation":I
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getDragSourceFragmentId()I

    move-result v10

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v11

    const/4 v14, 0x0

    const/16 v15, 0x12

    move-object/from16 v9, p0

    move-object/from16 v12, p1

    move-object/from16 v13, p2

    invoke-virtual/range {v9 .. v15}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startCopyOrMove(IILjava/lang/String;Ljava/util/ArrayList;II)V

    goto/16 :goto_0
.end method

.method protected onDropFinished(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 6159
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mIsDragMode:Z

    .line 6161
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v0, :cond_0

    .line 6163
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->finishDrag()V

    .line 6165
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 35
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 1703
    invoke-super/range {p0 .. p1}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v30

    if-nez v30, :cond_22

    .line 1705
    const/16 v30, 0x0

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mBackupSelectedItems:Ljava/util/ArrayList;

    .line 1706
    const/4 v4, 0x0

    .line 1708
    .local v4, "audioPlayer":Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v30

    sparse-switch v30, :sswitch_data_0

    .line 2246
    .end local v4    # "audioPlayer":Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;
    :cond_0
    :goto_0
    const/16 v30, 0x0

    :goto_1
    return v30

    .line 1712
    .restart local v4    # "audioPlayer":Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;
    :sswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v30, v0

    new-instance v31, Landroid/view/KeyEvent;

    const/16 v32, 0x1

    const/16 v33, 0x4

    invoke-direct/range {v31 .. v33}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual/range {v30 .. v31}, Lcom/sec/android/app/myfiles/AbsMainActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    .line 1714
    const/16 v30, 0x1

    goto :goto_1

    .line 1719
    :sswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    instance-of v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;

    move/from16 v30, v0

    if-eqz v30, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v30, v0

    check-cast v30, Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/app/myfiles/SelectorActivity;->getAudioPlayer()Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 1720
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->stop()V

    .line 1723
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectionType()I

    move-result v26

    .line 1725
    .local v26, "selectionType":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemPosition()Ljava/util/ArrayList;

    move-result-object v24

    .line 1727
    .local v24, "selectedItemPos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v23

    .line 1729
    .local v23, "selectedItemCount":I
    if-gtz v23, :cond_2

    const/16 v30, 0x2

    move/from16 v0, v26

    move/from16 v1, v30

    if-eq v0, v1, :cond_2

    const/16 v30, 0x3

    move/from16 v0, v26

    move/from16 v1, v30

    if-eq v0, v1, :cond_2

    .line 1731
    const/16 v30, 0x1

    goto :goto_1

    .line 1734
    :cond_2
    if-eqz v26, :cond_3

    const/16 v30, 0x1

    move/from16 v0, v26

    move/from16 v1, v30

    if-ne v0, v1, :cond_10

    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v30

    const/16 v31, 0x8

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v30

    const/16 v31, 0xa

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v30

    const/16 v31, 0x1f

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_10

    .line 1737
    :cond_4
    if-nez v26, :cond_8

    .line 1745
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v31, v0

    const/16 v30, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Ljava/lang/Integer;

    invoke-virtual/range {v30 .. v30}, Ljava/lang/Integer;->intValue()I

    move-result v30

    move-object/from16 v0, v31

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/database/Cursor;

    .line 1747
    .local v5, "cursor":Landroid/database/Cursor;
    const-string v30, "_data"

    move-object/from16 v0, v30

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v30

    move/from16 v0, v30

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 1750
    .local v17, "path":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v30

    if-eqz v30, :cond_5

    .line 1751
    new-instance v8, Ljava/io/File;

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "/storage/sdcard0/Android/data/com.baidu.netdisk/files/scratch"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1756
    .local v8, "f":Ljava/io/File;
    :goto_2
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v30

    if-nez v30, :cond_6

    .line 1758
    const/16 v30, 0xf

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startDownload(Ljava/lang/String;I)V

    .line 1874
    .end local v5    # "cursor":Landroid/database/Cursor;
    .end local v8    # "f":Ljava/io/File;
    .end local v17    # "path":Ljava/lang/String;
    :goto_3
    const/16 v30, 0x1

    goto/16 :goto_1

    .line 1753
    .restart local v5    # "cursor":Landroid/database/Cursor;
    .restart local v17    # "path":Ljava/lang/String;
    :cond_5
    new-instance v8, Ljava/io/File;

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "/storage/sdcard0/Android/data/com.dropbox.android/files/scratch"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .restart local v8    # "f":Ljava/io/File;
    goto :goto_2

    .line 1762
    :cond_6
    new-instance v20, Landroid/content/Intent;

    invoke-direct/range {v20 .. v20}, Landroid/content/Intent;-><init>()V

    .line 1764
    .local v20, "result":Landroid/content/Intent;
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectedItemsPathAsIntent(Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v20

    .line 1767
    if-eqz v20, :cond_7

    .line 1769
    const-string v30, "dest_fragment_id"

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v31

    move-object/from16 v0, v20

    move-object/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1771
    const/16 v30, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v30

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->setResultAndFinish(ILandroid/content/Intent;)V

    goto :goto_3

    .line 1777
    :cond_7
    const/16 v30, 0x0

    const/16 v31, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v30

    move-object/from16 v2, v31

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->setResultAndFinish(ILandroid/content/Intent;)V

    goto :goto_3

    .line 1784
    .end local v5    # "cursor":Landroid/database/Cursor;
    .end local v8    # "f":Ljava/io/File;
    .end local v17    # "path":Ljava/lang/String;
    .end local v20    # "result":Landroid/content/Intent;
    :cond_8
    const/16 v30, 0x64

    move/from16 v0, v23

    move/from16 v1, v30

    if-le v0, v1, :cond_9

    .line 1786
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    const v31, 0x7f0b0066

    const/16 v32, 0x1

    move/from16 v0, v32

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    const/16 v34, 0x64

    invoke-static/range {v34 .. v34}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v34

    aput-object v34, v32, v33

    invoke-virtual/range {v30 .. v32}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    .line 1788
    .local v16, "msg":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v30, v0

    const/16 v31, 0x1

    move-object/from16 v0, v30

    move-object/from16 v1, v16

    move/from16 v2, v31

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Landroid/widget/Toast;->show()V

    goto/16 :goto_3

    .line 1796
    .end local v16    # "msg":Ljava/lang/String;
    :cond_9
    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    .line 1800
    .local v18, "pathList":[Ljava/lang/String;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1802
    .local v6, "downloadPathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_4
    move/from16 v0, v23

    if-ge v9, v0, :cond_d

    .line 1804
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v31, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Ljava/lang/Integer;

    invoke-virtual/range {v30 .. v30}, Ljava/lang/Integer;->intValue()I

    move-result v30

    move-object/from16 v0, v31

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/database/Cursor;

    .line 1806
    .restart local v5    # "cursor":Landroid/database/Cursor;
    const-string v30, "_data"

    move-object/from16 v0, v30

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v30

    move/from16 v0, v30

    invoke-interface {v5, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 1808
    .restart local v17    # "path":Ljava/lang/String;
    if-eqz v17, :cond_b

    .line 1811
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v30

    if-eqz v30, :cond_c

    .line 1812
    new-instance v8, Ljava/io/File;

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "/storage/sdcard0/Android/data/com.baidu.netdisk/files/scratch"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1817
    .restart local v8    # "f":Ljava/io/File;
    :goto_5
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v30

    if-nez v30, :cond_a

    .line 1819
    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1822
    :cond_a
    aput-object v17, v18, v9

    .line 1802
    .end local v8    # "f":Ljava/io/File;
    :cond_b
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    .line 1814
    :cond_c
    new-instance v8, Ljava/io/File;

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "/storage/sdcard0/Android/data/com.dropbox.android/files/scratch"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .restart local v8    # "f":Ljava/io/File;
    goto :goto_5

    .line 1826
    .end local v5    # "cursor":Landroid/database/Cursor;
    .end local v8    # "f":Ljava/io/File;
    .end local v17    # "path":Ljava/lang/String;
    :cond_d
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v30

    if-eqz v30, :cond_e

    .line 1828
    const/16 v30, 0xf

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-virtual {v0, v6, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startDownload(Ljava/util/ArrayList;I)V

    goto/16 :goto_3

    .line 1832
    :cond_e
    new-instance v20, Landroid/content/Intent;

    invoke-direct/range {v20 .. v20}, Landroid/content/Intent;-><init>()V

    .line 1834
    .restart local v20    # "result":Landroid/content/Intent;
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectedItemsPathAsIntent(Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v20

    .line 1837
    if-eqz v20, :cond_f

    .line 1839
    const-string v30, "dest_fragment_id"

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v31

    move-object/from16 v0, v20

    move-object/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1841
    const/16 v30, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v30

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->setResultAndFinish(ILandroid/content/Intent;)V

    goto/16 :goto_3

    .line 1847
    :cond_f
    const/16 v30, 0x0

    const/16 v31, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v30

    move-object/from16 v2, v31

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->setResultAndFinish(ILandroid/content/Intent;)V

    goto/16 :goto_3

    .line 1855
    .end local v6    # "downloadPathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v9    # "i":I
    .end local v18    # "pathList":[Ljava/lang/String;
    .end local v20    # "result":Landroid/content/Intent;
    :cond_10
    new-instance v20, Landroid/content/Intent;

    invoke-direct/range {v20 .. v20}, Landroid/content/Intent;-><init>()V

    .line 1857
    .restart local v20    # "result":Landroid/content/Intent;
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectedItemsPathAsIntent(Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v20

    .line 1860
    if-eqz v20, :cond_11

    .line 1862
    const-string v30, "dest_fragment_id"

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v31

    move-object/from16 v0, v20

    move-object/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1864
    const/16 v30, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v30

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->setResultAndFinish(ILandroid/content/Intent;)V

    goto/16 :goto_3

    .line 1870
    :cond_11
    const/16 v30, 0x0

    const/16 v31, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v30

    move-object/from16 v2, v31

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->setResultAndFinish(ILandroid/content/Intent;)V

    goto/16 :goto_3

    .line 1879
    .end local v20    # "result":Landroid/content/Intent;
    .end local v23    # "selectedItemCount":I
    .end local v24    # "selectedItemPos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v26    # "selectionType":I
    :sswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    instance-of v0, v0, Lcom/sec/android/app/myfiles/SelectorActivity;

    move/from16 v30, v0

    if-eqz v30, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v30, v0

    check-cast v30, Lcom/sec/android/app/myfiles/SelectorActivity;

    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/app/myfiles/SelectorActivity;->getAudioPlayer()Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    move-result-object v4

    if-eqz v4, :cond_12

    .line 1880
    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->stop()V

    .line 1883
    :cond_12
    const/16 v30, 0x0

    const/16 v31, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v30

    move-object/from16 v2, v31

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->setResultAndFinish(ILandroid/content/Intent;)V

    .line 1885
    const/16 v30, 0x1

    goto/16 :goto_1

    .line 1890
    :sswitch_3
    const/16 v30, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->showDialog(I)V

    .line 1892
    const/16 v30, 0x1

    goto/16 :goto_1

    .line 1897
    :sswitch_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startShareAppList()V

    .line 1899
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->finishSelectMode()V

    .line 1901
    const/16 v30, 0x1

    goto/16 :goto_1

    .line 1906
    :sswitch_5
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startRename()V

    .line 1908
    const/16 v30, 0x1

    goto/16 :goto_1

    .line 1912
    :sswitch_6
    const/16 v30, 0x1

    move/from16 v0, v30

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mStartMoveToKnox:Z

    .line 1913
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v30, v0

    invoke-static/range {v30 .. v30}, Landroid/os/PersonaManager;->getKnoxInfoForApp(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Lcom/sec/android/app/myfiles/utils/Utils;->setKnoxinfoForAppBundle(Landroid/os/Bundle;)V

    .line 1914
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v30, v0

    const-string v31, "persona"

    invoke-virtual/range {v30 .. v31}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Landroid/os/PersonaManager;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPersona:Landroid/os/PersonaManager;

    .line 1915
    const-string v30, "2.0"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v31, v0

    invoke-static/range {v31 .. v31}, Lcom/sec/android/app/myfiles/utils/Utils;->getKNOXVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_18

    .line 1917
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v25

    .line 1919
    .local v25, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    new-instance v27, Ljava/util/ArrayList;

    invoke-direct/range {v27 .. v27}, Ljava/util/ArrayList;-><init>()V

    .line 1923
    .local v27, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v30, v0

    const-string v31, "rcp"

    invoke-virtual/range {v30 .. v31}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/os/RCPManager;

    .line 1925
    .local v15, "mRcpManager":Landroid/os/RCPManager;
    const-wide/16 v28, 0x0

    .line 1927
    .local v28, "threadId":J
    const/16 v19, 0x4

    .line 1930
    .local v19, "requestApp":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mMoveToKnoxFileList:Ljava/util/ArrayList;

    move-object/from16 v30, v0

    if-nez v30, :cond_14

    .line 1932
    new-instance v30, Ljava/util/ArrayList;

    invoke-direct/range {v30 .. v30}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mMoveToKnoxFileList:Ljava/util/ArrayList;

    .line 1939
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mMoveToKnoxFolderList:Ljava/util/ArrayList;

    move-object/from16 v30, v0

    if-nez v30, :cond_15

    .line 1941
    new-instance v30, Ljava/util/ArrayList;

    invoke-direct/range {v30 .. v30}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mMoveToKnoxFolderList:Ljava/util/ArrayList;

    .line 1949
    :goto_7
    const/16 v22, 0x0

    .line 1951
    .local v22, "selectedBrowserItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_8
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v30

    if-eqz v30, :cond_17

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    .local v21, "selectItem":Ljava/lang/Object;
    move-object/from16 v22, v21

    .line 1953
    check-cast v22, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .line 1955
    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1958
    move-object/from16 v0, v22

    iget v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mFormat:I

    move/from16 v30, v0

    const/16 v31, 0x3001

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_13

    move-object/from16 v0, v22

    iget v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mFormat:I

    move/from16 v30, v0

    const/16 v31, -0x1

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_16

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/myfiles/element/BrowserItem;->isFile()Ljava/lang/Boolean;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v30

    if-nez v30, :cond_16

    .line 1961
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mMoveToKnoxFolderList:Ljava/util/ArrayList;

    move-object/from16 v30, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 1936
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v21    # "selectItem":Ljava/lang/Object;
    .end local v22    # "selectedBrowserItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mMoveToKnoxFileList:Ljava/util/ArrayList;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->clear()V

    goto :goto_6

    .line 1945
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mMoveToKnoxFolderList:Ljava/util/ArrayList;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Ljava/util/ArrayList;->clear()V

    goto :goto_7

    .line 1966
    .restart local v10    # "i$":Ljava/util/Iterator;
    .restart local v21    # "selectItem":Ljava/lang/Object;
    .restart local v22    # "selectedBrowserItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mMoveToKnoxFileList:Ljava/util/ArrayList;

    move-object/from16 v30, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 1972
    .end local v21    # "selectItem":Ljava/lang/Object;
    :cond_17
    :try_start_0
    move/from16 v0, v19

    move-object/from16 v1, v27

    move-object/from16 v2, v27

    invoke-virtual {v15, v0, v1, v2}, Landroid/os/RCPManager;->moveFilesForApp(ILjava/util/List;Ljava/util/List;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v28

    .line 1997
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v15    # "mRcpManager":Landroid/os/RCPManager;
    .end local v19    # "requestApp":I
    .end local v22    # "selectedBrowserItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    .end local v25    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v27    # "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v28    # "threadId":J
    :goto_9
    const/16 v30, 0x1

    goto/16 :goto_1

    .line 1973
    .restart local v10    # "i$":Ljava/util/Iterator;
    .restart local v15    # "mRcpManager":Landroid/os/RCPManager;
    .restart local v19    # "requestApp":I
    .restart local v22    # "selectedBrowserItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    .restart local v25    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .restart local v27    # "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v28    # "threadId":J
    :catch_0
    move-exception v7

    .line 1974
    .local v7, "e":Landroid/os/RemoteException;
    invoke-virtual {v7}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_9

    .line 1990
    .end local v7    # "e":Landroid/os/RemoteException;
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v15    # "mRcpManager":Landroid/os/RCPManager;
    .end local v19    # "requestApp":I
    .end local v22    # "selectedBrowserItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    .end local v25    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v27    # "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v28    # "threadId":J
    :cond_18
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v30

    const v31, 0x7f0f0126

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_19

    .line 1991
    const/16 v30, 0x5

    const/16 v31, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startKnoxFileOperation(II)V

    goto :goto_9

    .line 1993
    :cond_19
    const/16 v30, 0x6

    const/16 v31, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startKnoxFileOperation(II)V

    goto :goto_9

    .line 2001
    :sswitch_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v30, v0

    const-string v31, "PRIV"

    const/16 v32, 0x0

    invoke-static/range {v30 .. v32}, Lcom/sec/android/app/myfiles/utils/Utils;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 2002
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    move-object/from16 v30, v0

    invoke-static/range {v30 .. v30}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretMode(Landroid/content/Context;)Z

    move-result v30

    if-eqz v30, :cond_1a

    .line 2004
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPrivateModeHandler:Landroid/os/Handler;

    move-object/from16 v30, v0

    const/16 v31, 0x2

    const-wide/16 v32, 0x12c

    invoke-virtual/range {v30 .. v33}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2054
    :goto_a
    const/16 v30, 0x1

    goto/16 :goto_1

    .line 2009
    :cond_1a
    :try_start_1
    new-instance v30, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$7;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$7;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPrivateModeClient:Lcom/samsung/android/privatemode/IPrivateModeClient;

    .line 2047
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPrivateModeClient:Lcom/samsung/android/privatemode/IPrivateModeClient;

    move-object/from16 v31, v0

    invoke-static/range {v30 .. v31}, Lcom/samsung/android/privatemode/PrivateModeManager;->getInstance(Landroid/content/Context;Lcom/samsung/android/privatemode/IPrivateModeClient;)Lcom/samsung/android/privatemode/PrivateModeManager;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_a

    .line 2050
    :catch_1
    move-exception v7

    .line 2051
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_a

    .line 2058
    .end local v7    # "e":Ljava/lang/Exception;
    :sswitch_8
    new-instance v13, Landroid/content/Intent;

    const-string v30, "com.sec.android.app.myfiles.PICK_SELECT_PATH"

    move-object/from16 v0, v30

    invoke-direct {v13, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2060
    .local v13, "intentForRemoveFromPrivate":Landroid/content/Intent;
    const-string v30, "FILE_OPERATION_PRIVATE"

    const/16 v31, 0xe

    move-object/from16 v0, v30

    move/from16 v1, v31

    invoke-virtual {v13, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2062
    const-string v30, "FILE_OPERATION"

    const/16 v31, 0x1

    move-object/from16 v0, v30

    move/from16 v1, v31

    invoke-virtual {v13, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2064
    const-string v30, "SELECTOR_SOURCE_TYPE"

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v31, v0

    move-object/from16 v0, v30

    move/from16 v1, v31

    invoke-virtual {v13, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2066
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v30, v0

    if-eqz v30, :cond_1b

    .line 2068
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v14

    .line 2070
    .local v14, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mBackupSelectedItems:Ljava/util/ArrayList;

    .line 2087
    .end local v14    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_1b
    const-string v30, "SELECTOR_FTP_MOVE"

    const/16 v31, 0x1

    move-object/from16 v0, v30

    move/from16 v1, v31

    invoke-virtual {v13, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2091
    const/16 v30, 0x1

    :try_start_2
    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-virtual {v0, v13, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    .line 2097
    :goto_b
    const/16 v30, 0x1

    goto/16 :goto_1

    .line 2092
    :catch_2
    move-exception v7

    .line 2094
    .local v7, "e":Landroid/content/ActivityNotFoundException;
    const/16 v30, 0x2

    const-string v31, "AbsBrowserFragment"

    const-string v32, "com.sec.android.app.myfiles.PICK_SELECT_PATH not found - remove from private"

    invoke-static/range {v30 .. v32}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_b

    .line 2101
    .end local v7    # "e":Landroid/content/ActivityNotFoundException;
    .end local v13    # "intentForRemoveFromPrivate":Landroid/content/Intent;
    :sswitch_9
    new-instance v12, Landroid/content/Intent;

    const-string v30, "com.sec.android.app.myfiles.PICK_SELECT_PATH"

    move-object/from16 v0, v30

    invoke-direct {v12, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2103
    .local v12, "intentForMove":Landroid/content/Intent;
    const-string v30, "FILE_OPERATION"

    const/16 v31, 0x1

    move-object/from16 v0, v30

    move/from16 v1, v31

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2105
    const-string v30, "SELECTOR_SOURCE_TYPE"

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v31, v0

    move-object/from16 v0, v30

    move/from16 v1, v31

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2107
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v30, v0

    if-eqz v30, :cond_1c

    .line 2109
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v14

    .line 2111
    .restart local v14    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mBackupSelectedItems:Ljava/util/ArrayList;

    .line 2113
    if-eqz v14, :cond_1c

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v30

    if-lez v30, :cond_1c

    .line 2114
    const/4 v9, 0x0

    .restart local v9    # "i":I
    :goto_c
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v30

    move/from16 v0, v30

    if-ge v9, v0, :cond_1c

    .line 2115
    invoke-virtual {v14, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/sec/android/app/myfiles/element/BrowserItem;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v30, v0

    invoke-static/range {v30 .. v30}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxFolder(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_1d

    .line 2117
    const-string v30, "FILE_OPERATION_PRIVATE"

    const/16 v31, 0xf

    move-object/from16 v0, v30

    move/from16 v1, v31

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2138
    .end local v9    # "i":I
    .end local v14    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_1c
    const-string v30, "SELECTOR_FTP_MOVE"

    const/16 v31, 0x1

    move-object/from16 v0, v30

    move/from16 v1, v31

    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2142
    const/16 v30, 0x1

    :try_start_3
    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-virtual {v0, v12, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_3
    .catch Landroid/content/ActivityNotFoundException; {:try_start_3 .. :try_end_3} :catch_3

    .line 2148
    :goto_d
    const/16 v30, 0x1

    goto/16 :goto_1

    .line 2114
    .restart local v9    # "i":I
    .restart local v14    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_1d
    add-int/lit8 v9, v9, 0x1

    goto :goto_c

    .line 2143
    .end local v9    # "i":I
    .end local v14    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :catch_3
    move-exception v7

    .line 2145
    .restart local v7    # "e":Landroid/content/ActivityNotFoundException;
    const/16 v30, 0x2

    const-string v31, "AbsBrowserFragment"

    const-string v32, "com.sec.android.app.myfiles.PICK_SELECT_PATH not found - move"

    invoke-static/range {v30 .. v32}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_d

    .line 2153
    .end local v7    # "e":Landroid/content/ActivityNotFoundException;
    .end local v12    # "intentForMove":Landroid/content/Intent;
    :sswitch_a
    new-instance v11, Landroid/content/Intent;

    const-string v30, "com.sec.android.app.myfiles.PICK_SELECT_PATH"

    move-object/from16 v0, v30

    invoke-direct {v11, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2155
    .local v11, "intent":Landroid/content/Intent;
    const-string v30, "FILE_OPERATION"

    const/16 v31, 0x0

    move-object/from16 v0, v30

    move/from16 v1, v31

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2157
    const-string v30, "SELECTOR_SOURCE_TYPE"

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    move/from16 v31, v0

    move-object/from16 v0, v30

    move/from16 v1, v31

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2159
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v30, v0

    if-eqz v30, :cond_1e

    .line 2161
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v14

    .line 2163
    .restart local v14    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mBackupSelectedItems:Ljava/util/ArrayList;

    .line 2165
    if-eqz v14, :cond_1e

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v30

    if-lez v30, :cond_1e

    .line 2166
    const/4 v9, 0x0

    .restart local v9    # "i":I
    :goto_e
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v30

    move/from16 v0, v30

    if-ge v9, v0, :cond_1e

    .line 2167
    invoke-virtual {v14, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/sec/android/app/myfiles/element/BrowserItem;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v30, v0

    invoke-static/range {v30 .. v30}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxFolder(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_1f

    .line 2169
    const-string v30, "FILE_OPERATION_PRIVATE"

    const/16 v31, 0xf

    move-object/from16 v0, v30

    move/from16 v1, v31

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2190
    .end local v9    # "i":I
    .end local v14    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_1e
    const-string v30, "SELECTOR_FTP_MOVE"

    const/16 v31, 0x0

    move-object/from16 v0, v30

    move/from16 v1, v31

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2194
    const/16 v30, 0x0

    :try_start_4
    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-virtual {v0, v11, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_4
    .catch Landroid/content/ActivityNotFoundException; {:try_start_4 .. :try_end_4} :catch_4

    .line 2200
    :goto_f
    const/16 v30, 0x1

    goto/16 :goto_1

    .line 2166
    .restart local v9    # "i":I
    .restart local v14    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_1f
    add-int/lit8 v9, v9, 0x1

    goto :goto_e

    .line 2195
    .end local v9    # "i":I
    .end local v14    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :catch_4
    move-exception v7

    .line 2197
    .restart local v7    # "e":Landroid/content/ActivityNotFoundException;
    const/16 v30, 0x2

    const-string v31, "AbsBrowserFragment"

    const-string v32, "com.sec.android.app.myfiles.PICK_SELECT_PATH not found - copy"

    invoke-static/range {v30 .. v32}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_f

    .line 2203
    .end local v7    # "e":Landroid/content/ActivityNotFoundException;
    .end local v11    # "intent":Landroid/content/Intent;
    :sswitch_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v30, v0

    if-eqz v30, :cond_20

    .line 2204
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectedItems:Ljava/util/ArrayList;

    .line 2205
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectedItems:Ljava/util/ArrayList;

    move-object/from16 v30, v0

    if-eqz v30, :cond_20

    .line 2206
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectedItems:Ljava/util/ArrayList;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    invoke-virtual/range {v30 .. v31}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/sec/android/app/myfiles/element/BrowserItem;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mContextpath:Ljava/lang/String;

    .line 2207
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mContextpath:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->lock(Ljava/lang/String;)V

    .line 2210
    :cond_20
    const/16 v30, 0x1

    goto/16 :goto_1

    .line 2214
    :sswitch_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v30, v0

    if-eqz v30, :cond_21

    .line 2215
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectedItems:Ljava/util/ArrayList;

    .line 2216
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectedItems:Ljava/util/ArrayList;

    move-object/from16 v30, v0

    if-eqz v30, :cond_21

    .line 2217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectedItems:Ljava/util/ArrayList;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    invoke-virtual/range {v30 .. v31}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/sec/android/app/myfiles/element/BrowserItem;

    move-object/from16 v0, v30

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mContextpath:Ljava/lang/String;

    .line 2218
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mContextpath:Ljava/lang/String;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->lock(Ljava/lang/String;)V

    .line 2221
    :cond_21
    const/16 v30, 0x1

    goto/16 :goto_1

    .line 2226
    :sswitch_d
    const/16 v30, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->showDetail(Ljava/lang/String;)V

    .line 2228
    const/16 v30, 0x1

    goto/16 :goto_1

    .line 2232
    :sswitch_e
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->addSingleFileShortcutToHome()V

    .line 2234
    const/16 v30, 0x1

    goto/16 :goto_1

    .line 2239
    .end local v4    # "audioPlayer":Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;
    :cond_22
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v30

    const v31, 0x7f0f0136

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_0

    .line 2241
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v30, v0

    if-eqz v30, :cond_0

    .line 2242
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->clearHistoricalPaths()V

    goto/16 :goto_0

    .line 1708
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0f0120 -> :sswitch_4
        0x7f0f0121 -> :sswitch_3
        0x7f0f0122 -> :sswitch_9
        0x7f0f0123 -> :sswitch_a
        0x7f0f0124 -> :sswitch_7
        0x7f0f0125 -> :sswitch_8
        0x7f0f0126 -> :sswitch_6
        0x7f0f0127 -> :sswitch_6
        0x7f0f0128 -> :sswitch_5
        0x7f0f012a -> :sswitch_e
        0x7f0f012e -> :sswitch_b
        0x7f0f012f -> :sswitch_c
        0x7f0f0130 -> :sswitch_d
        0x7f0f0147 -> :sswitch_2
        0x7f0f0148 -> :sswitch_1
    .end sparse-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 715
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->onPause()V

    .line 717
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v0, :cond_0

    .line 719
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->onPause()V

    .line 722
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    if-eqz v0, :cond_1

    .line 724
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    if-eqz v0, :cond_1

    .line 726
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->removeDelayedMessage()V

    .line 734
    :cond_1
    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 18
    .param p1, "id"    # I
    .param p2, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 2926
    invoke-super/range {p0 .. p2}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 2928
    packed-switch p1, :pswitch_data_0

    .line 3030
    .end local p2    # "dialog":Landroid/app/Dialog;
    :goto_0
    :pswitch_0
    return-void

    .line 2932
    .restart local p2    # "dialog":Landroid/app/Dialog;
    :pswitch_1
    const/4 v9, 0x0

    .line 2933
    .local v9, "isFileDeleted":Z
    const/4 v10, 0x0

    .line 2935
    .local v10, "isFolderDeleted":Z
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v12

    .line 2937
    .local v12, "selectedItemCount":I
    const-string v6, ""

    .line 2938
    .local v6, "dialogTitle":Ljava/lang/String;
    const-string v5, ""

    .line 2940
    .local v5, "dialogMessage":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 2942
    .local v3, "deleteType":Ljava/lang/Object;
    check-cast v3, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .end local v3    # "deleteType":Ljava/lang/Object;
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/element/BrowserItem;->isFile()Ljava/lang/Boolean;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 2944
    const/4 v9, 0x1

    goto :goto_1

    .line 2947
    :cond_0
    const/4 v10, 0x1

    goto :goto_1

    .line 2951
    :cond_1
    if-eqz v9, :cond_3

    if-nez v10, :cond_3

    .line 2953
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0c0010

    invoke-virtual {v13, v14, v12}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v6

    .line 2955
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0c0011

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v13, v14, v12, v15}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2970
    :goto_2
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v14, 0x28

    if-ne v13, v14, :cond_2

    .line 2971
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0c0010

    invoke-virtual {v13, v14, v12}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v6

    .line 2972
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0c0011

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v13, v14, v12, v15}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2974
    :cond_2
    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 2976
    check-cast p2, Landroid/app/AlertDialog;

    .end local p2    # "dialog":Landroid/app/Dialog;
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2957
    .restart local p2    # "dialog":Landroid/app/Dialog;
    :cond_3
    if-nez v9, :cond_4

    if-eqz v10, :cond_4

    .line 2959
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0c000f

    invoke-virtual {v13, v14, v12}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v6

    .line 2961
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0c0012

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v13, v14, v12, v15}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    .line 2965
    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0b00f2

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 2967
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0b00f1

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v13, v14, v15}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_2

    .line 2984
    .end local v5    # "dialogMessage":Ljava/lang/String;
    .end local v6    # "dialogTitle":Ljava/lang/String;
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v9    # "isFileDeleted":Z
    .end local v10    # "isFolderDeleted":Z
    .end local v12    # "selectedItemCount":I
    :pswitch_2
    move/from16 v4, p1

    .line 2986
    .local v4, "dialogId":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v13}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v8

    .line 2988
    .local v8, "inflater":Landroid/view/LayoutInflater;
    const v13, 0x7f040032

    const/4 v14, 0x0

    invoke-virtual {v8, v13, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 2990
    .local v2, "customView":Landroid/view/View;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isChinaModel()Z

    move-result v13

    if-eqz v13, :cond_5

    .line 2991
    const v13, 0x7f0f00c5

    invoke-virtual {v2, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 2992
    .local v11, "mobileDataWarningDialog":Landroid/widget/TextView;
    const v13, 0x7f0b00e0

    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setText(I)V

    .line 2995
    .end local v11    # "mobileDataWarningDialog":Landroid/widget/TextView;
    :cond_5
    const v13, 0x7f0f00c7

    invoke-virtual {v2, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 3009
    .local v1, "checkDoNotShow":Landroid/widget/CheckBox;
    new-instance v13, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$18;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v4, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$18;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;ILandroid/widget/CheckBox;)V

    invoke-virtual {v1, v13}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3025
    check-cast p2, Landroid/app/AlertDialog;

    .end local p2    # "dialog":Landroid/app/Dialog;
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 2928
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 14
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1396
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 1398
    sget-object v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    if-eqz v10, :cond_10

    .line 1401
    iget v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mRunFrom:I

    const/16 v11, 0x15

    if-ne v10, v11, :cond_1

    .line 1402
    sget-object v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v11, 0x7f0f0135

    invoke-interface {v10, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    .local v7, "menuItem":Landroid/view/MenuItem;
    if-eqz v7, :cond_0

    .line 1404
    const/4 v10, 0x0

    invoke-interface {v7, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1407
    :cond_0
    sget-object v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v11, 0x7f0f0136

    invoke-interface {v10, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 1409
    const/4 v10, 0x0

    invoke-interface {v7, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1412
    .end local v7    # "menuItem":Landroid/view/MenuItem;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v10

    iget v10, v10, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v10}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->setActionBarIcon(I)V

    .line 1413
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v10, :cond_4

    .line 1414
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v10}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1416
    const v10, 0x7f0f0129

    invoke-interface {p1, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v10

    if-eqz v10, :cond_2

    .line 1417
    const v10, 0x7f0f0129

    invoke-interface {p1, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v10

    const/4 v11, 0x0

    invoke-interface {v10, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1420
    :cond_2
    const v10, 0x7f0f012a

    invoke-interface {p1, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v10

    if-eqz v10, :cond_3

    .line 1421
    const v10, 0x7f0f012a

    invoke-interface {p1, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v10

    const/4 v11, 0x0

    invoke-interface {v10, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1424
    :cond_3
    const v10, 0x7f0f013d

    invoke-interface {p1, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v10

    if-eqz v10, :cond_4

    .line 1425
    const v10, 0x7f0f013d

    invoke-interface {p1, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v10

    const/4 v11, 0x0

    invoke-interface {v10, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1430
    :cond_4
    sget-object v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v11, 0x7f0f0147

    invoke-interface {v10, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v10

    if-eqz v10, :cond_5

    .line 1431
    sget-object v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v11, 0x7f0f0147

    invoke-interface {v10, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v10

    const/4 v11, 0x0

    invoke-interface {v10, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1433
    :cond_5
    sget-object v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v11, 0x7f0f0148

    invoke-interface {v10, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v10

    if-eqz v10, :cond_6

    .line 1434
    sget-object v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v11, 0x7f0f0148

    invoke-interface {v10, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v10

    const/4 v11, 0x0

    invoke-interface {v10, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1437
    :cond_6
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v10, :cond_18

    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    instance-of v10, v10, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    if-eqz v10, :cond_18

    .line 1439
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    check-cast v10, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/navigation/AllFileBrowserNavigation;->isStorageChooserScreenVisible()Z

    move-result v10

    if-eqz v10, :cond_12

    .line 1441
    sget-object v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v11, 0x7f0f013a

    invoke-interface {v10, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    .restart local v7    # "menuItem":Landroid/view/MenuItem;
    if-eqz v7, :cond_7

    .line 1443
    const/4 v10, 0x0

    invoke-interface {v7, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1446
    :cond_7
    sget-object v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v11, 0x7f0f0137

    invoke-interface {v10, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    if-eqz v7, :cond_8

    .line 1448
    const/4 v10, 0x0

    invoke-interface {v7, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1451
    :cond_8
    sget-object v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v11, 0x7f0f0138

    invoke-interface {v10, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    if-eqz v7, :cond_9

    .line 1453
    const/4 v10, 0x0

    invoke-interface {v7, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1456
    :cond_9
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->isSelectMode()Z

    move-result v10

    if-eqz v10, :cond_b

    .line 1458
    sget-object v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v11, 0x7f0f0135

    invoke-interface {v10, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    if-eqz v7, :cond_a

    .line 1460
    const/4 v10, 0x0

    invoke-interface {v7, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1463
    :cond_a
    sget-object v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v11, 0x7f0f0136

    invoke-interface {v10, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    if-eqz v7, :cond_b

    .line 1465
    const/4 v10, 0x0

    invoke-interface {v7, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1498
    :cond_b
    :goto_0
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v10

    if-eqz v10, :cond_c

    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v10

    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v10

    if-nez v10, :cond_16

    .line 1500
    :cond_c
    sget-object v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v11, 0x7f0f0137

    invoke-interface {v10, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    if-eqz v7, :cond_d

    .line 1502
    const/4 v10, 0x0

    invoke-interface {v7, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1505
    :cond_d
    sget-object v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v11, 0x7f0f0138

    invoke-interface {v10, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    if-eqz v7, :cond_e

    .line 1507
    const/4 v10, 0x0

    invoke-interface {v7, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1562
    .end local v7    # "menuItem":Landroid/view/MenuItem;
    :cond_e
    :goto_1
    sget-object v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v11, 0x7f0f013e

    invoke-interface {v10, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    .restart local v7    # "menuItem":Landroid/view/MenuItem;
    if-eqz v7, :cond_f

    .line 1564
    const/4 v10, 0x0

    invoke-interface {v7, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1567
    :cond_f
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v10

    if-nez v10, :cond_20

    .line 1569
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectModeFrom()I

    move-result v10

    if-nez v10, :cond_1d

    .line 1677
    .end local v7    # "menuItem":Landroid/view/MenuItem;
    :cond_10
    :goto_2
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v10, :cond_11

    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-static {v10}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v10

    if-nez v10, :cond_11

    const v10, 0x7f0f0129

    invoke-interface {p1, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v10

    if-eqz v10, :cond_11

    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v10, :cond_11

    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxFolder(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_11

    .line 1680
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    const/4 v11, 0x1

    invoke-static {v10, v11}, Lcom/sec/android/app/myfiles/utils/Utils;->getShortCutCount(Landroid/content/Context;Z)I

    move-result v10

    const/16 v11, 0x32

    if-ne v10, v11, :cond_2b

    .line 1682
    const v10, 0x7f0f0129

    invoke-interface {p1, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v10

    const/4 v11, 0x0

    invoke-interface {v10, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1684
    const v10, 0x7f0f012a

    invoke-interface {p1, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v10

    if-eqz v10, :cond_11

    .line 1686
    const v10, 0x7f0f012a

    invoke-interface {p1, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v10

    const/4 v11, 0x1

    invoke-interface {v10, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1697
    :cond_11
    :goto_3
    return-void

    .line 1471
    :cond_12
    sget-object v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v11, 0x7f0f0137

    invoke-interface {v10, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    .restart local v7    # "menuItem":Landroid/view/MenuItem;
    if-eqz v7, :cond_13

    .line 1473
    const/4 v10, 0x1

    invoke-interface {v7, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1476
    :cond_13
    sget-object v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v11, 0x7f0f0138

    invoke-interface {v10, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    if-eqz v7, :cond_14

    .line 1479
    const/4 v10, 0x1

    invoke-interface {v7, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1484
    :cond_14
    sget-object v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v11, 0x7f0f013a

    invoke-interface {v10, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    if-eqz v7, :cond_15

    .line 1486
    const/4 v10, 0x1

    invoke-interface {v7, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1489
    :cond_15
    sget-object v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v11, 0x7f0f0129

    invoke-interface {v10, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    if-eqz v7, :cond_b

    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxFolder(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 1491
    const/4 v10, 0x0

    invoke-interface {v7, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 1512
    :cond_16
    sget-object v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v11, 0x7f0f0137

    invoke-interface {v10, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    if-eqz v7, :cond_17

    .line 1514
    const/4 v10, 0x1

    invoke-interface {v7, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1517
    :cond_17
    sget-object v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v11, 0x7f0f0138

    invoke-interface {v10, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    if-eqz v7, :cond_e

    .line 1520
    const/4 v10, 0x1

    invoke-interface {v7, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_1

    .line 1529
    .end local v7    # "menuItem":Landroid/view/MenuItem;
    :cond_18
    const/4 v2, 0x0

    .line 1530
    .local v2, "categoryCusrsor":Landroid/database/Cursor;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v10

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v11

    invoke-virtual {v10, v11}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->getCategoryCursor(I)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_e

    .line 1532
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v10

    if-nez v10, :cond_1b

    .line 1534
    sget-object v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v11, 0x7f0f0137

    invoke-interface {v10, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    .restart local v7    # "menuItem":Landroid/view/MenuItem;
    if-eqz v7, :cond_19

    .line 1536
    const/4 v10, 0x0

    invoke-interface {v7, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1539
    :cond_19
    sget-object v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v11, 0x7f0f0138

    invoke-interface {v10, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    if-eqz v7, :cond_1a

    .line 1541
    const/4 v10, 0x0

    invoke-interface {v7, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1558
    :cond_1a
    :goto_4
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 1545
    .end local v7    # "menuItem":Landroid/view/MenuItem;
    :cond_1b
    sget-object v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v11, 0x7f0f0137

    invoke-interface {v10, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    .restart local v7    # "menuItem":Landroid/view/MenuItem;
    if-eqz v7, :cond_1c

    .line 1547
    const/4 v10, 0x1

    invoke-interface {v7, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1550
    :cond_1c
    sget-object v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v11, 0x7f0f0138

    invoke-interface {v10, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    if-eqz v7, :cond_1a

    .line 1553
    const/4 v10, 0x1

    invoke-interface {v7, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_4

    .line 1573
    .end local v2    # "categoryCusrsor":Landroid/database/Cursor;
    :cond_1d
    sget-object v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v11, 0x7f0f0148

    invoke-interface {v10, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    if-eqz v7, :cond_10

    .line 1575
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v10, :cond_1e

    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v10

    const-string v11, "/storage"

    if-ne v10, v11, :cond_1e

    .line 1577
    const/4 v10, 0x0

    invoke-interface {v7, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1582
    :cond_1e
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectionType()I

    move-result v10

    const/4 v11, 0x2

    if-eq v10, v11, :cond_1f

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectionType()I

    move-result v10

    const/4 v11, 0x3

    if-eq v10, v11, :cond_1f

    .line 1585
    const/4 v10, 0x0

    invoke-interface {v7, v10}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1588
    :cond_1f
    sget-object v10, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mOptionsMenu:Landroid/view/Menu;

    const v11, 0x7f0f013b

    invoke-interface {v10, v11}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    if-eqz v7, :cond_10

    .line 1590
    const/4 v10, 0x0

    invoke-interface {v7, v10}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_2

    .line 1597
    :cond_20
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectModeFrom()I

    move-result v10

    if-nez v10, :cond_10

    .line 1599
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v10

    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getCount()I

    move-result v11

    if-ge v10, v11, :cond_21

    .line 1601
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v10, :cond_21

    .line 1603
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    const/4 v11, 0x1

    const/4 v12, 0x1

    invoke-virtual {v10, v11, v12}, Lcom/sec/android/app/myfiles/MainActivity;->setSelectOptionsVisibility(ZZ)V

    .line 1608
    :cond_21
    const/4 v0, 0x0

    .line 1609
    .local v0, "blockSendVia":Z
    const/4 v1, 0x0

    .line 1610
    .local v1, "blockSendtoKNOX":Z
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectedItems:Ljava/util/ArrayList;

    .line 1612
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectedItems:Ljava/util/ArrayList;

    if-eqz v10, :cond_2a

    .line 1614
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectedItems:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_22
    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_25

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    .local v9, "selectedItem":Ljava/lang/Object;
    move-object v8, v9

    .line 1616
    check-cast v8, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .line 1618
    .local v8, "selected":Lcom/sec/android/app/myfiles/element/BrowserItem;
    iget v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v11, 0x8

    if-eq v10, v11, :cond_23

    iget v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v11, 0x1f

    if-ne v10, v11, :cond_27

    .line 1620
    :cond_23
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v11, v8, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    invoke-static {v10, v11}, Lcom/sec/android/app/myfiles/utils/Utils;->isDirectoryOfDropbox(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v10

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    .line 1622
    .local v6, "isDirectory":Ljava/lang/Boolean;
    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-nez v10, :cond_24

    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v10

    const/4 v11, 0x1

    if-le v10, v11, :cond_22

    .line 1624
    :cond_24
    const/4 v0, 0x1

    .line 1661
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "isDirectory":Ljava/lang/Boolean;
    .end local v8    # "selected":Lcom/sec/android/app/myfiles/element/BrowserItem;
    .end local v9    # "selectedItem":Ljava/lang/Object;
    :cond_25
    :goto_6
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_10

    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectedItems:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lez v10, :cond_10

    .line 1663
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectedItems:Ljava/util/ArrayList;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/myfiles/element/BrowserItem;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    invoke-static {v10}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v4

    .line 1665
    .local v4, "fileTypeInt":I
    iget v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v11, 0x8

    if-eq v10, v11, :cond_26

    iget v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v11, 0x1f

    if-ne v10, v11, :cond_10

    .line 1667
    :cond_26
    invoke-static {v4}, Lcom/sec/android/app/myfiles/MediaFile;->isAudioFileType(I)Z

    move-result v10

    if-nez v10, :cond_10

    invoke-static {v4}, Lcom/sec/android/app/myfiles/MediaFile;->isVideoFileType(I)Z

    move-result v10

    if-eqz v10, :cond_10

    goto/16 :goto_2

    .line 1629
    .end local v4    # "fileTypeInt":I
    .restart local v5    # "i$":Ljava/util/Iterator;
    .restart local v8    # "selected":Lcom/sec/android/app/myfiles/element/BrowserItem;
    .restart local v9    # "selectedItem":Ljava/lang/Object;
    :cond_27
    iget-object v10, v8, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    if-eqz v10, :cond_28

    iget-object v10, v8, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    sget-object v11, Lcom/sec/android/app/myfiles/utils/Utils;->mSecretBoxRoot:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_28

    .line 1631
    const/4 v1, 0x1

    goto :goto_5

    .line 1635
    :cond_28
    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/element/BrowserItem;->isFile()Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-eqz v10, :cond_29

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getFile()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->length()J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v10, v10, v12

    if-eqz v10, :cond_29

    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v11, v8, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    invoke-static {v10, v11}, Lcom/sec/android/app/myfiles/drm/DrmUtils;->isForwardable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_22

    .line 1638
    :cond_29
    const/4 v0, 0x1

    .line 1640
    iget-object v10, v8, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    if-eqz v10, :cond_25

    .line 1642
    new-instance v3, Ljava/io/File;

    iget-object v10, v8, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    invoke-direct {v3, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1644
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v10

    if-eqz v10, :cond_25

    .line 1646
    const/4 v1, 0x1

    goto :goto_6

    .line 1656
    .end local v3    # "file":Ljava/io/File;
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v8    # "selected":Lcom/sec/android/app/myfiles/element/BrowserItem;
    .end local v9    # "selectedItem":Ljava/lang/Object;
    :cond_2a
    const/4 v0, 0x1

    .line 1658
    const/4 v1, 0x1

    goto/16 :goto_6

    .line 1690
    .end local v0    # "blockSendVia":Z
    .end local v1    # "blockSendtoKNOX":Z
    .end local v7    # "menuItem":Landroid/view/MenuItem;
    :cond_2b
    const v10, 0x7f0f0129

    invoke-interface {p1, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v10

    const/4 v11, 0x1

    invoke-interface {v10, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1692
    const v10, 0x7f0f012a

    invoke-interface {p1, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v10

    if-eqz v10, :cond_11

    .line 1694
    const v10, 0x7f0f012a

    invoke-interface {p1, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v10

    const/4 v11, 0x0

    invoke-interface {v10, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_3
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 739
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsListableFragment;->onResume()V

    .line 741
    const-string v0, "VerificationLog"

    const-string v1, "AbsbrowserFragment onresume"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 742
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "IS_SHORTCUT_FOLDER"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mIsShortcutFolder:Z

    .line 744
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "SHORTCUT_ROOT_FOLDER"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mShortcutRootFolder:Ljava/lang/String;

    .line 746
    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mIsHomeBtnSelected:Z

    .line 748
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ISSELECTOR"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mIsSelector:Z

    .line 750
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getSplitLocation()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSplitPoint:I

    .line 752
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v0, :cond_0

    .line 754
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->onResume()V

    .line 757
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mbSettingChanged:Z

    if-eqz v0, :cond_1

    .line 759
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->refreshAdapter(I)V

    .line 762
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "CLEAR_HISTORICALPATH"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 764
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->clearHistoricalPaths()V

    .line 766
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "CLEAR_HISTORICALPATH"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 769
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->initFileObserver()V

    .line 770
    return-void
.end method

.method public removeDelayedMessage()V
    .locals 1

    .prologue
    .line 1288
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v0, :cond_0

    .line 1290
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    if-eqz v0, :cond_1

    .line 1292
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    if-eqz v0, :cond_0

    .line 1294
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AllFileBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->removeDelayedMessage()V

    .line 1320
    :cond_0
    :goto_0
    return-void

    .line 1297
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter;

    if-eqz v0, :cond_2

    .line 1299
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    if-eqz v0, :cond_0

    .line 1301
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AudioBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->removeDelayedMessage()V

    goto :goto_0

    .line 1304
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;

    if-eqz v0, :cond_3

    .line 1306
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    if-eqz v0, :cond_0

    .line 1308
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->removeDelayedMessage()V

    goto :goto_0

    .line 1311
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;

    if-eqz v0, :cond_0

    .line 1313
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    if-eqz v0, :cond_0

    .line 1315
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/VideoBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->removeDelayedMessage()V

    goto :goto_0
.end method

.method protected selectAllItem()V
    .locals 3

    .prologue
    .line 3507
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v0, :cond_0

    .line 3509
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/MainActivity;->setSelectOptionsVisibility(ZZ)V

    .line 3513
    :cond_0
    return-void
.end method

.method public selectItemControl(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 6505
    if-nez p1, :cond_0

    .line 6506
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->selectAllItem()V

    .line 6510
    :goto_0
    return-void

    .line 6508
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->unselectAllItem()V

    goto :goto_0
.end method

.method protected setDragImage(Landroid/widget/ImageView;Lcom/sec/android/app/myfiles/element/BrowserItem;Z)V
    .locals 2
    .param p1, "icon"    # Landroid/widget/ImageView;
    .param p2, "item"    # Lcom/sec/android/app/myfiles/element/BrowserItem;
    .param p3, "isFile"    # Z

    .prologue
    .line 1153
    if-eqz p3, :cond_1

    .line 1155
    iget-object v1, p2, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->getThumbnailDrawableWithoutMakeCache(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1156
    .local v0, "dragIcon":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    .line 1157
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1165
    .end local v0    # "dragIcon":Landroid/graphics/drawable/Drawable;
    :goto_0
    return-void

    .line 1159
    .restart local v0    # "dragIcon":Landroid/graphics/drawable/Drawable;
    :cond_0
    iget-object v1, p2, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getDragFileIcon(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 1163
    .end local v0    # "dragIcon":Landroid/graphics/drawable/Drawable;
    :cond_1
    const v1, 0x7f0200af

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public setForceRefreshPathIndicatorBar(Z)V
    .locals 1
    .param p1, "force"    # Z

    .prologue
    .line 6606
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    if-eqz v0, :cond_0

    .line 6608
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setForceRefreshPathIndicatorBar(Z)V

    .line 6611
    :cond_0
    return-void
.end method

.method public setFromFolderHoverForAirButtonMode(Ljava/lang/String;Z)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "bFolderHoverAirButton"    # Z

    .prologue
    .line 6416
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFolderHoverFilePath:Ljava/lang/String;

    .line 6417
    iput-boolean p2, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->bFolderFromAirButton:Z

    .line 6418
    return-void
.end method

.method protected setListOrGridPosition(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x0

    .line 4928
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mViewMode:I

    packed-switch v0, :pswitch_data_0

    .line 4947
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$25;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$25;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->post(Ljava/lang/Runnable;)Z

    .line 4957
    :goto_0
    return-void

    .line 4933
    :pswitch_0
    if-nez p1, :cond_0

    .line 4935
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    .line 4939
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v0, p1, v1, v1}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->smoothScrollToPositionFromTop(III)V

    goto :goto_0

    .line 4928
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public setSelectModeActionBar()V
    .locals 4

    .prologue
    const/4 v3, 0x7

    .line 1325
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectModeFrom()I

    move-result v0

    if-nez v0, :cond_2

    .line 1327
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v0, :cond_1

    .line 1329
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const/high16 v1, 0x7f040000

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectActionBarView:Landroid/view/View;

    .line 1331
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectActionBarView:Landroid/view/View;

    const v1, 0x7f0f0003

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectAllButton:Landroid/widget/TextView;

    .line 1335
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v0, :cond_0

    .line 1339
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$4;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1364
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$5;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/MainActivity;->setSelectAllClickListener(Landroid/view/View$OnClickListener;)V

    .line 1373
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$6;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$6;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/MainActivity;->setUnselectAllClickListener(Landroid/view/View$OnClickListener;)V

    .line 1382
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActionBarManager:Lcom/sec/android/app/myfiles/view/ActionBarManager;

    if-eqz v0, :cond_1

    .line 1384
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActionBarManager:Lcom/sec/android/app/myfiles/view/ActionBarManager;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectActionBarView:Landroid/view/View;

    invoke-virtual {v0, v3, v1}, Lcom/sec/android/app/myfiles/view/ActionBarManager;->setActionBar(ILandroid/view/View;)V

    .line 1392
    :cond_1
    :goto_0
    return-void

    .line 1388
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActionBarManager:Lcom/sec/android/app/myfiles/view/ActionBarManager;

    if-eqz v0, :cond_1

    .line 1390
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActionBarManager:Lcom/sec/android/app/myfiles/view/ActionBarManager;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/myfiles/view/ActionBarManager;->setActionBar(I)V

    goto :goto_0
.end method

.method public setmIsHomeBtnSelected(Z)V
    .locals 0
    .param p1, "isHomeBtnSelected"    # Z

    .prologue
    .line 292
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mIsHomeBtnSelected:Z

    .line 293
    return-void
.end method

.method public setmIsSelector(Z)V
    .locals 0
    .param p1, "isSelector"    # Z

    .prologue
    .line 338
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mIsSelector:Z

    .line 339
    return-void
.end method

.method public share(Ljava/lang/String;Z)V
    .locals 12
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "needCheck"    # Z

    .prologue
    .line 2602
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2603
    .local v3, "file":Ljava/io/File;
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v8, p1}, Lcom/sec/android/app/myfiles/utils/FileUtils;->pathToUri(Landroid/app/Activity;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 2604
    .local v7, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-static {p1, v8}, Lcom/sec/android/app/myfiles/MediaFile;->getShareMimeType(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 2607
    .local v0, "MIMEType":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 2707
    :cond_0
    :goto_0
    return-void

    .line 2610
    :cond_1
    if-eqz p2, :cond_2

    const-string v8, "image/jpeg"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 2612
    invoke-static {p1}, Lcom/quramsoft/qdio/QdioJNI;->checkAudioInJPEG(Ljava/lang/String;)Lcom/quramsoft/qdio/QdioJNI$QdioJPEGData;

    move-result-object v6

    .line 2614
    .local v6, "qdioData":Lcom/quramsoft/qdio/QdioJNI$QdioJPEGData;
    if-eqz v6, :cond_2

    .line 2621
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->showDialogForSoundShot(Ljava/lang/String;)V

    goto :goto_0

    .line 2627
    .end local v6    # "qdioData":Lcom/quramsoft/qdio/QdioJNI$QdioJPEGData;
    :cond_2
    if-eqz p2, :cond_3

    const-string v8, "image/golf"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 2634
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->showDialogForSoundShot(Ljava/lang/String;)V

    goto :goto_0

    .line 2639
    :cond_3
    const-string v8, "image/golf"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2643
    const/4 v8, 0x0

    invoke-static {p1, p1, v8}, Lcom/sec/android/app/myfiles/utils/FileUtils;->changeToImageFilePath(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v4

    .line 2645
    .local v4, "filePath":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 2650
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 2652
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v8, p1}, Lcom/sec/android/app/myfiles/utils/FileUtils;->pathToUri(Landroid/app/Activity;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 2655
    .end local v4    # "filePath":Ljava/lang/String;
    :cond_4
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    const-string v9, ".txt"

    invoke-virtual {v8, v9}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    const-string v8, "text/plain"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 2656
    const-string v0, "application/txt"

    .line 2659
    :cond_5
    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "image/"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 2660
    const-string v0, "image/*"

    .line 2663
    :cond_6
    const-string v8, "application/vnd.samsung.scc"

    invoke-virtual {v0, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 2665
    const-string v0, "application/scc"

    .line 2668
    :cond_7
    const/4 v8, 0x2

    const-string v9, "AbsBrowserFragment"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "startShareAppList MIMEType = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 2671
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v8

    if-eqz v8, :cond_d

    .line 2672
    new-instance v2, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/storage/sdcard0/Android/data/com.baidu.netdisk/files/scratch"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v2, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2677
    .local v2, "f":Ljava/io/File;
    :goto_1
    iget v8, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v9, 0x8

    if-eq v8, v9, :cond_8

    iget v8, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v9, 0x1f

    if-eq v8, v9, :cond_8

    iget v8, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v9, 0x12

    if-ne v8, v9, :cond_9

    const-string v8, "/storage"

    invoke-virtual {p1, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_9

    :cond_8
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_f

    .line 2680
    :cond_9
    new-instance v5, Landroid/content/Intent;

    const-string v8, "android.intent.action.SEND"

    invoke-direct {v5, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2681
    .local v5, "intent":Landroid/content/Intent;
    invoke-virtual {v5, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2682
    const-string v8, "from-myfiles"

    const/4 v9, 0x1

    invoke-virtual {v5, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2684
    iget v8, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v9, 0x8

    if-eq v8, v9, :cond_a

    iget v8, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v9, 0x1f

    if-eq v8, v9, :cond_a

    iget v8, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v9, 0x12

    if-ne v8, v9, :cond_c

    const-string v8, "/storage"

    invoke-virtual {p1, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_c

    .line 2685
    :cond_a
    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v7

    .line 2686
    iget v8, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v9, 0x8

    if-eq v8, v9, :cond_b

    iget v8, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v9, 0x1f

    if-eq v8, v9, :cond_b

    iget v8, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v9, 0x12

    if-ne v8, v9, :cond_c

    .line 2687
    :cond_b
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v8

    if-eqz v8, :cond_e

    .line 2688
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/storage/sdcard0/Android/data/com.baidu.netdisk/files/scratch"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 2696
    :cond_c
    :goto_2
    const-string v8, "android.intent.extra.STREAM"

    invoke-virtual {v5, v8, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2697
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b001f

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    .line 2698
    .local v1, "chooserIntent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    const-class v9, Lcom/sec/android/app/myfiles/MyFilesChooserActivity;

    invoke-virtual {v1, v8, v9}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 2699
    const-string v8, "path_list"

    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSharePathList:Ljava/util/ArrayList;

    invoke-virtual {v1, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2700
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startActivity(Landroid/content/Intent;)V

    .line 2702
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSharePathList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_0

    .line 2674
    .end local v1    # "chooserIntent":Landroid/content/Intent;
    .end local v2    # "f":Ljava/io/File;
    .end local v5    # "intent":Landroid/content/Intent;
    :cond_d
    new-instance v2, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/storage/sdcard0/Android/data/com.dropbox.android/files/scratch"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v2, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .restart local v2    # "f":Ljava/io/File;
    goto/16 :goto_1

    .line 2690
    .restart local v5    # "intent":Landroid/content/Intent;
    :cond_e
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/storage/sdcard0/Android/data/com.dropbox.android/files/scratch"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    .line 2705
    .end local v5    # "intent":Landroid/content/Intent;
    :cond_f
    const/16 v8, 0x11

    invoke-virtual {p0, p1, v8}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startDownload(Ljava/lang/String;I)V

    goto/16 :goto_0
.end method

.method protected showDetail(Ljava/lang/String;)V
    .locals 14
    .param p1, "nearbyPath"    # Ljava/lang/String;

    .prologue
    .line 4509
    const/4 v11, 0x0

    .line 4510
    .local v11, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    instance-of v12, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    if-nez v12, :cond_0

    instance-of v12, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;

    if-eqz v12, :cond_2

    .line 4511
    :cond_0
    new-instance v11, Ljava/util/ArrayList;

    .end local v11    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 4512
    .restart local v11    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget v12, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mContextMenuPosition:I

    invoke-virtual {p0, v12}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getBrowserItem(I)Lcom/sec/android/app/myfiles/element/BrowserItem;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4516
    :goto_0
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-gtz v12, :cond_3

    .line 4639
    :cond_1
    :goto_1
    return-void

    .line 4514
    :cond_2
    iget-object v12, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v12}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v11

    goto :goto_0

    .line 4520
    :cond_3
    const/4 v9, 0x0

    .line 4521
    .local v9, "selItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    instance-of v12, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    if-nez v12, :cond_4

    instance-of v12, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;

    if-eqz v12, :cond_c

    .line 4522
    :cond_4
    new-instance v9, Ljava/util/ArrayList;

    .end local v9    # "selItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 4523
    .restart local v9    # "selItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget v12, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mContextMenuPosition:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v9, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4528
    :goto_2
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_f

    .line 4530
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onCreateDetailFragmentListener()Landroid/app/DialogFragment;

    move-result-object v2

    .line 4532
    .local v2, "detail":Landroid/app/DialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 4534
    .local v0, "argument":Landroid/os/Bundle;
    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .line 4536
    .local v10, "selectedItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    iget v12, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v13, 0x9

    if-eq v12, v13, :cond_5

    instance-of v12, p0, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;

    if-eqz v12, :cond_d

    .line 4537
    :cond_5
    const-string v12, "detail_item_path"

    invoke-virtual {v0, v12, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4541
    :goto_3
    const-string v13, "detail_item_index"

    const/4 v12, 0x0

    invoke-virtual {v9, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    invoke-virtual {v0, v13, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 4544
    iget v12, v10, Lcom/sec/android/app/myfiles/element/BrowserItem;->mFormat:I

    const/16 v13, 0x3001

    if-eq v12, v13, :cond_6

    iget v12, v10, Lcom/sec/android/app/myfiles/element/BrowserItem;->mFormat:I

    const/4 v13, -0x1

    if-ne v12, v13, :cond_b

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/BrowserItem;->isFile()Ljava/lang/Boolean;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v12

    if-nez v12, :cond_b

    .line 4546
    :cond_6
    iget-object v12, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v12, :cond_b

    .line 4548
    const/4 v1, 0x0

    .line 4550
    .local v1, "cursor":Landroid/database/Cursor;
    iget v12, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v13, 0x12

    if-ne v12, v13, :cond_e

    iget v12, v10, Lcom/sec/android/app/myfiles/element/BrowserItem;->mSearchCategory:I

    const/16 v13, 0x8

    if-ne v12, v13, :cond_e

    .line 4552
    iget-object v12, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getPath()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/sec/android/app/myfiles/utils/Utils;->getDropboxFilesinFolder(Landroid/content/Context;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 4559
    :goto_4
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 4561
    .local v4, "formats":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz v1, :cond_b

    .line 4563
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v12

    if-eqz v12, :cond_a

    .line 4565
    :cond_7
    const/4 v3, 0x0

    .line 4567
    .local v3, "format":I
    iget v12, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v13, 0x201

    if-eq v12, v13, :cond_8

    iget v12, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v13, 0x8

    if-eq v12, v13, :cond_8

    iget v12, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v13, 0x1f

    if-eq v12, v13, :cond_8

    iget v12, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v13, 0x12

    if-ne v12, v13, :cond_9

    .line 4569
    :cond_8
    const-string v12, "format"

    invoke-interface {v1, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    invoke-interface {v1, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 4573
    :cond_9
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v4, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4574
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v12

    if-nez v12, :cond_7

    .line 4576
    .end local v3    # "format":I
    :cond_a
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 4577
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_b

    .line 4578
    const-string v12, "detail_item_format"

    invoke-virtual {v0, v12, v4}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 4584
    .end local v1    # "cursor":Landroid/database/Cursor;
    .end local v4    # "formats":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_b
    if-eqz v2, :cond_1

    .line 4586
    invoke-virtual {v2, v0}, Landroid/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 4588
    const/4 v12, 0x0

    invoke-virtual {v2, p0, v12}, Landroid/app/DialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 4590
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v12

    const-string v13, "detail"

    invoke-virtual {v2, v12, v13}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 4525
    .end local v0    # "argument":Landroid/os/Bundle;
    .end local v2    # "detail":Landroid/app/DialogFragment;
    .end local v10    # "selectedItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    :cond_c
    iget-object v12, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v12}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemPosition()Ljava/util/ArrayList;

    move-result-object v9

    goto/16 :goto_2

    .line 4539
    .restart local v0    # "argument":Landroid/os/Bundle;
    .restart local v2    # "detail":Landroid/app/DialogFragment;
    .restart local v10    # "selectedItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    :cond_d
    const-string v12, "detail_item_path"

    iget-object v13, v10, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    invoke-virtual {v0, v12, v13}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 4556
    .restart local v1    # "cursor":Landroid/database/Cursor;
    :cond_e
    iget-object v12, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getPath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInFolder(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_4

    .line 4595
    .end local v0    # "argument":Landroid/os/Bundle;
    .end local v1    # "cursor":Landroid/database/Cursor;
    .end local v2    # "detail":Landroid/app/DialogFragment;
    .end local v10    # "selectedItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    :cond_f
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onCreateMultipleDetailFragmentListener()Landroid/app/DialogFragment;

    move-result-object v2

    .line 4597
    .restart local v2    # "detail":Landroid/app/DialogFragment;
    if-eqz v2, :cond_1

    .line 4601
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 4603
    .restart local v0    # "argument":Landroid/os/Bundle;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 4605
    .restart local v4    # "formats":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 4607
    .local v8, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_5
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-ge v5, v12, :cond_13

    .line 4609
    iget-object v13, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    invoke-virtual {v13, v12}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    .line 4611
    .local v6, "item":Ljava/lang/Object;
    if-eqz v6, :cond_12

    instance-of v12, v6, Landroid/database/Cursor;

    if-eqz v12, :cond_12

    move-object v1, v6

    .line 4613
    check-cast v1, Landroid/database/Cursor;

    .line 4615
    .restart local v1    # "cursor":Landroid/database/Cursor;
    const/4 v3, 0x0

    .line 4617
    .restart local v3    # "format":I
    iget v12, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v13, 0x201

    if-eq v12, v13, :cond_10

    iget v12, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v13, 0x8

    if-eq v12, v13, :cond_10

    iget v12, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v13, 0x1f

    if-ne v12, v13, :cond_11

    .line 4619
    :cond_10
    const-string v12, "format"

    invoke-interface {v1, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    invoke-interface {v1, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 4621
    :cond_11
    const-string v12, "_data"

    invoke-interface {v1, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    invoke-interface {v1, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 4623
    .local v7, "path":Ljava/lang/String;
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4625
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v4, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4607
    .end local v1    # "cursor":Landroid/database/Cursor;
    .end local v3    # "format":I
    .end local v7    # "path":Ljava/lang/String;
    :cond_12
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 4629
    .end local v6    # "item":Ljava/lang/Object;
    :cond_13
    const-string v12, "detail_item_path"

    invoke-virtual {v0, v12, v8}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 4631
    const-string v12, "detail_item_format"

    invoke-virtual {v0, v12, v4}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 4633
    invoke-virtual {v2, v0}, Landroid/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 4635
    const/4 v12, 0x0

    invoke-virtual {v2, p0, v12}, Landroid/app/DialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 4637
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v12

    const-string v13, "detail"

    invoke-virtual {v2, v12, v13}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method protected showPathIndicator(Z)V
    .locals 3
    .param p1, "show"    # Z

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 3486
    if-eqz p1, :cond_0

    .line 3488
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPathIndicatorContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 3490
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setVisibility(I)V

    .line 3492
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mCategoryTitleContainer:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 3502
    :goto_0
    return-void

    .line 3496
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPathIndicatorContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 3498
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setVisibility(I)V

    .line 3500
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mCategoryTitleContainer:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public showWaitProgressDialog(Z)V
    .locals 4
    .param p1, "isShow"    # Z

    .prologue
    .line 6351
    if-eqz p1, :cond_0

    .line 6353
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mProgressHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 6360
    :goto_0
    return-void

    .line 6357
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mProgressHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method protected startCopyOrMove(IILjava/lang/String;Ljava/util/ArrayList;I)V
    .locals 7
    .param p1, "srcFragmentId"    # I
    .param p2, "dstFragmentId"    # I
    .param p3, "dstFolder"    # Ljava/lang/String;
    .param p5, "operation"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 4217
    .local p4, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v6, 0x2

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startCopyOrMove(IILjava/lang/String;Ljava/util/ArrayList;II)V

    .line 4218
    return-void
.end method

.method protected startCopyOrMove(IILjava/lang/String;Ljava/util/ArrayList;II)V
    .locals 9
    .param p1, "srcFragmentId"    # I
    .param p2, "dstFragmentId"    # I
    .param p3, "dstFolder"    # Ljava/lang/String;
    .param p5, "operation"    # I
    .param p6, "requestCode"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .local p4, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/16 v8, 0xa

    const/16 v4, 0x8

    const/4 v7, 0x2

    const v6, 0x7f0b00cb

    const/4 v5, 0x0

    .line 4224
    if-eqz p5, :cond_0

    const/4 v3, 0x1

    if-eq p5, v3, :cond_0

    .line 4286
    :goto_0
    return-void

    .line 4229
    :cond_0
    if-ne p2, v8, :cond_2

    if-eq p1, v4, :cond_1

    const/16 v3, 0x1f

    if-ne p1, v3, :cond_2

    .line 4231
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v3, v6, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 4234
    :cond_2
    if-eq p2, v4, :cond_3

    const/16 v3, 0x1f

    if-ne p2, v3, :cond_4

    :cond_3
    if-ne p1, v8, :cond_4

    .line 4236
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v3, v6, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 4241
    :cond_4
    invoke-direct {p0, p4, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->verifySelectedFiles(Ljava/util/ArrayList;I)Z

    move-result v3

    if-nez v3, :cond_6

    .line 4243
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->finishSelectMode()V

    .line 4245
    if-nez p5, :cond_5

    .line 4247
    const-string v3, "AbsBrowserFragment"

    const-string v4, "startCopyOrMove verifySelectedFiles:: Copy Failed(2)"

    invoke-static {v7, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 4248
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v4, 0x7f0b00c9

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 4251
    :cond_5
    const-string v3, "AbsBrowserFragment"

    const-string v4, "startCopyOrMove verifySelectedFiles:: Move Failed(2)"

    invoke-static {v7, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 4252
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v3, v6, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 4258
    :cond_6
    invoke-static {p5}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(I)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v2

    .line 4260
    .local v2, "fragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    invoke-virtual {v2, p0, p6}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 4266
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 4268
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v3, "dst_folder"

    invoke-virtual {v0, v3, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4270
    const-string v3, "target_datas"

    invoke-virtual {v0, v3, p4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 4272
    const-string v3, "src_fragment_id"

    invoke-virtual {v0, v3, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 4274
    const-string v3, "dest_fragment_id"

    invoke-virtual {v0, v3, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 4276
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 4277
    .local v1, "fm":Landroid/app/FragmentManager;
    if-eqz v1, :cond_7

    .line 4278
    const-string v3, "createFolder"

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 4280
    :cond_7
    const-string v3, "AbsBrowserFragment"

    const-string v4, "Fragment manager startCopyOrMove was null!!!!"

    invoke-static {v5, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method protected startDownload(Ljava/lang/String;I)V
    .locals 12
    .param p1, "selectedPath"    # Ljava/lang/String;
    .param p2, "requestCode"    # I

    .prologue
    const/4 v11, 0x0

    .line 4309
    const/16 v8, 0x8

    .line 4310
    .local v8, "srcFragmentId":I
    const/16 v4, 0x201

    .line 4312
    .local v4, "dstFragmentId":I
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v10

    if-nez v10, :cond_1

    .line 4351
    :cond_0
    :goto_0
    return-void

    .line 4315
    :cond_1
    move-object v7, p1

    .line 4316
    .local v7, "path":Ljava/lang/String;
    sget-object v10, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v10}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v7, v11, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 4317
    .local v1, "dir":Ljava/lang/String;
    const-string v10, "/storage/sdcard0/Android/data/com.dropbox.android/files/scratch"

    invoke-virtual {v10, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 4320
    .local v3, "dstFolder":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 4321
    const/16 v8, 0x1f

    .line 4322
    const-string v10, "/storage/sdcard0/Android/data/com.baidu.netdisk/files/scratch"

    invoke-virtual {v10, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 4325
    :cond_2
    sget-object v10, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v10}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v7, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->downloadPath:Ljava/lang/String;

    .line 4327
    new-instance v5, Ljava/io/File;

    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->downloadPath:Ljava/lang/String;

    invoke-direct {v5, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 4328
    .local v5, "f":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 4329
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->openDownloadedFile()V

    goto :goto_0

    .line 4333
    :cond_3
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 4335
    .local v2, "dstFol":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_4

    .line 4336
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 4338
    :cond_4
    invoke-static {v11}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(I)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v6

    .line 4339
    .local v6, "fragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    invoke-virtual {v6, p0, p2}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 4341
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 4342
    .local v9, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4344
    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 4345
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v10, "dst_folder"

    invoke-virtual {v0, v10, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4346
    const-string v10, "target_datas"

    invoke-virtual {v0, v10, v9}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 4347
    const-string v10, "src_fragment_id"

    invoke-virtual {v0, v10, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 4348
    const-string v10, "dest_fragment_id"

    invoke-virtual {v0, v10, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 4350
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v10

    const-string v11, "cacheDownload"

    invoke-virtual {v6, v10, v11}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected startDownload(Ljava/util/ArrayList;I)V
    .locals 13
    .param p2, "requestCode"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .local p1, "selectedPath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v12, 0x0

    .line 4356
    const/16 v9, 0x8

    .line 4357
    .local v9, "srcFragmentId":I
    const/16 v4, 0x201

    .line 4359
    .local v4, "dstFragmentId":I
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 4361
    .local v7, "listCount":I
    invoke-virtual {p1, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 4362
    .local v8, "path":Ljava/lang/String;
    sget-object v11, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v11}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v11

    invoke-virtual {v8, v12, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 4363
    .local v1, "dir":Ljava/lang/String;
    const-string v11, "/storage/sdcard0/Android/data/com.dropbox.android/files/scratch"

    invoke-virtual {v11, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 4366
    .local v3, "dstFolder":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 4367
    const/16 v9, 0x1f

    .line 4368
    const-string v11, "/storage/sdcard0/Android/data/com.baidu.netdisk/files/scratch"

    invoke-virtual {v11, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 4371
    :cond_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 4373
    .local v2, "dstFol":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_1

    .line 4374
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 4376
    :cond_1
    invoke-static {v12}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(I)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v5

    .line 4377
    .local v5, "fragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    invoke-virtual {v5, p0, p2}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 4379
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 4380
    .local v10, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v7, :cond_2

    .line 4382
    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4380
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 4386
    :cond_2
    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 4387
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v11, "dst_folder"

    invoke-virtual {v0, v11, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4388
    const-string v11, "target_datas"

    invoke-virtual {v0, v11, v10}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 4389
    const-string v11, "src_fragment_id"

    invoke-virtual {v0, v11, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 4390
    const-string v11, "dest_fragment_id"

    invoke-virtual {v0, v11, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 4392
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v11

    const-string v12, "cacheDownload"

    invoke-virtual {v5, v11, v12}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 4393
    return-void
.end method

.method public startDragMode(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 36
    .param p1, "view"    # Landroid/view/View;
    .param p2, "arg"    # Landroid/os/Bundle;

    .prologue
    .line 827
    const/16 v31, 0x1

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mIsDragMode:Z

    .line 829
    const/4 v11, 0x0

    .line 831
    .local v11, "fileUri":Landroid/net/Uri;
    const/4 v4, 0x0

    .line 833
    .local v4, "dragData":Landroid/content/ClipData;
    const/16 v19, 0x0

    .line 835
    .local v19, "intent":Landroid/content/Intent;
    const/16 v21, 0x0

    .line 837
    .local v21, "item":Lcom/sec/android/app/myfiles/element/BrowserItem;
    const/16 v20, 0x0

    .line 839
    .local v20, "isFile":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v29

    .line 841
    .local v29, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->size()I

    move-result v28

    .line 843
    .local v28, "selectedItemCount":I
    const/16 v31, 0x0

    const-string v32, "AbsBrowserFragment"

    const-string v33, "startDragMode()"

    invoke-static/range {v31 .. v33}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 845
    const/16 v31, 0x64

    move/from16 v0, v28

    move/from16 v1, v31

    if-le v0, v1, :cond_1

    .line 847
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v31

    const v32, 0x7f0b0066

    const/16 v33, 0x1

    move/from16 v0, v33

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v33, v0

    const/16 v34, 0x0

    const/16 v35, 0x64

    invoke-static/range {v35 .. v35}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v35

    aput-object v35, v33, v34

    invoke-virtual/range {v31 .. v33}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v27

    .line 849
    .local v27, "msg":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v31, v0

    const/16 v32, 0x1

    move-object/from16 v0, v31

    move-object/from16 v1, v27

    move/from16 v2, v32

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Landroid/widget/Toast;->show()V

    .line 850
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->finishSelectMode()V

    .line 1150
    .end local v27    # "msg":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 854
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v31

    packed-switch v31, :pswitch_data_0

    .line 935
    :pswitch_0
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_1
    move/from16 v0, v16

    move/from16 v1, v28

    if-ge v0, v1, :cond_7

    .line 937
    move-object/from16 v0, v29

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    .end local v21    # "item":Lcom/sec/android/app/myfiles/element/BrowserItem;
    check-cast v21, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .line 939
    .restart local v21    # "item":Lcom/sec/android/app/myfiles/element/BrowserItem;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v31, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v32

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v33, v0

    invoke-static/range {v31 .. v33}, Lcom/sec/android/app/myfiles/utils/FileUtils;->pathToUri(Landroid/app/Activity;ILjava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    .line 941
    if-nez v16, :cond_6

    .line 943
    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/myfiles/element/BrowserItem;->isFile()Ljava/lang/Boolean;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v20

    .line 945
    const-string v31, "selectedUri"

    move-object/from16 v0, v31

    invoke-static {v0, v11}, Landroid/content/ClipData;->newRawUri(Ljava/lang/CharSequence;Landroid/net/Uri;)Landroid/content/ClipData;

    move-result-object v4

    .line 935
    :goto_2
    add-int/lit8 v16, v16, 0x1

    goto :goto_1

    .line 858
    .end local v16    # "i":I
    :pswitch_1
    if-eqz p2, :cond_7

    const-string v31, "ftp"

    move-object/from16 v0, p2

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v31

    if-eqz v31, :cond_7

    .line 860
    const-string v31, "ftp"

    move-object/from16 v0, p2

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 866
    .local v14, "ftpParamsStr":Ljava/lang/String;
    :try_start_0
    new-instance v13, Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-direct {v13, v14}, Lcom/sec/android/app/myfiles/ftp/FTPParams;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 873
    .local v13, "ftpParams":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    :goto_3
    if-eqz v13, :cond_7

    .line 875
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-virtual {v0, v14}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setFtpParamsString(Ljava/lang/String;)V

    .line 877
    const/16 v16, 0x0

    .restart local v16    # "i":I
    :goto_4
    move/from16 v0, v16

    move/from16 v1, v28

    if-ge v0, v1, :cond_7

    .line 879
    move-object/from16 v0, v29

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    .end local v21    # "item":Lcom/sec/android/app/myfiles/element/BrowserItem;
    check-cast v21, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .line 881
    .restart local v21    # "item":Lcom/sec/android/app/myfiles/element/BrowserItem;
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-static {v0, v13}, Lcom/sec/android/app/myfiles/ftp/FTPUtils;->getFtpUri(Ljava/lang/String;Lcom/sec/android/app/myfiles/ftp/FTPParams;)Landroid/net/Uri;

    move-result-object v11

    .line 883
    if-nez v16, :cond_3

    .line 885
    move-object/from16 v0, v21

    iget v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mItemType:I

    move/from16 v31, v0

    if-nez v31, :cond_2

    const/16 v20, 0x1

    .line 887
    :goto_5
    const-string v31, "selectedFTPUri"

    move-object/from16 v0, v31

    invoke-static {v0, v11}, Landroid/content/ClipData;->newRawUri(Ljava/lang/CharSequence;Landroid/net/Uri;)Landroid/content/ClipData;

    move-result-object v4

    .line 877
    :goto_6
    add-int/lit8 v16, v16, 0x1

    goto :goto_4

    .line 868
    .end local v13    # "ftpParams":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .end local v16    # "i":I
    :catch_0
    move-exception v7

    .line 870
    .local v7, "e":Ljava/lang/IllegalArgumentException;
    const/4 v13, 0x0

    .restart local v13    # "ftpParams":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    goto :goto_3

    .line 885
    .end local v7    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v16    # "i":I
    :cond_2
    const/16 v20, 0x0

    goto :goto_5

    .line 891
    :cond_3
    new-instance v31, Landroid/content/ClipData$Item;

    move-object/from16 v0, v31

    invoke-direct {v0, v11}, Landroid/content/ClipData$Item;-><init>(Landroid/net/Uri;)V

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Landroid/content/ClipData;->addItem(Landroid/content/ClipData$Item;)V

    goto :goto_6

    .line 902
    .end local v13    # "ftpParams":Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .end local v14    # "ftpParamsStr":Ljava/lang/String;
    .end local v16    # "i":I
    :pswitch_2
    const/16 v20, 0x1

    .line 904
    if-eqz p2, :cond_4

    const-string v31, "dropbox"

    move-object/from16 v0, p2

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v31

    if-eqz v31, :cond_4

    .line 906
    const-string v31, "dropbox"

    move-object/from16 v0, p2

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v12

    .line 908
    .local v12, "format":I
    const/16 v31, 0x3001

    move/from16 v0, v31

    if-ne v12, v0, :cond_4

    .line 910
    const/16 v20, 0x0

    .line 914
    .end local v12    # "format":I
    :cond_4
    const/16 v16, 0x0

    .restart local v16    # "i":I
    :goto_7
    move/from16 v0, v16

    move/from16 v1, v28

    if-ge v0, v1, :cond_7

    .line 916
    move-object/from16 v0, v29

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    .end local v21    # "item":Lcom/sec/android/app/myfiles/element/BrowserItem;
    check-cast v21, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .line 918
    .restart local v21    # "item":Lcom/sec/android/app/myfiles/element/BrowserItem;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v31, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v32

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v33, v0

    invoke-static/range {v31 .. v33}, Lcom/sec/android/app/myfiles/utils/FileUtils;->pathToUri(Landroid/app/Activity;ILjava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    .line 920
    if-nez v16, :cond_5

    .line 922
    const-string v31, "selectedCloudUri"

    move-object/from16 v0, v31

    invoke-static {v0, v11}, Landroid/content/ClipData;->newRawUri(Ljava/lang/CharSequence;Landroid/net/Uri;)Landroid/content/ClipData;

    move-result-object v4

    .line 914
    :goto_8
    add-int/lit8 v16, v16, 0x1

    goto :goto_7

    .line 926
    :cond_5
    new-instance v31, Landroid/content/ClipData$Item;

    move-object/from16 v0, v31

    invoke-direct {v0, v11}, Landroid/content/ClipData$Item;-><init>(Landroid/net/Uri;)V

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Landroid/content/ClipData;->addItem(Landroid/content/ClipData$Item;)V

    goto :goto_8

    .line 949
    :cond_6
    new-instance v31, Landroid/content/ClipData$Item;

    move-object/from16 v0, v31

    invoke-direct {v0, v11}, Landroid/content/ClipData$Item;-><init>(Landroid/net/Uri;)V

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Landroid/content/ClipData;->addItem(Landroid/content/ClipData$Item;)V

    goto/16 :goto_2

    .line 958
    .end local v16    # "i":I
    :cond_7
    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    .line 960
    .local v26, "mShareList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSideSyncControl:Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v31, v0

    invoke-static/range {v31 .. v31}, Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;->isKMSRunning(Landroid/content/Context;)Z

    move-result v31

    if-eqz v31, :cond_9

    .line 962
    const/16 v16, 0x0

    .restart local v16    # "i":I
    :goto_9
    move/from16 v0, v16

    move/from16 v1, v28

    if-ge v0, v1, :cond_8

    .line 964
    move-object/from16 v0, v29

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    .end local v21    # "item":Lcom/sec/android/app/myfiles/element/BrowserItem;
    check-cast v21, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .line 966
    .restart local v21    # "item":Lcom/sec/android/app/myfiles/element/BrowserItem;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v31, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v32

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    move-object/from16 v33, v0

    invoke-static/range {v31 .. v33}, Lcom/sec/android/app/myfiles/utils/FileUtils;->pathToUri(Landroid/app/Activity;ILjava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    .line 968
    move-object/from16 v0, v26

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 962
    add-int/lit8 v16, v16, 0x1

    goto :goto_9

    .line 972
    :cond_8
    new-instance v19, Landroid/content/Intent;

    .end local v19    # "intent":Landroid/content/Intent;
    const-string v31, "sidesync.app.action.KMS_FILETRANSFER_DRAG_FILEINFO"

    move-object/from16 v0, v19

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 974
    .restart local v19    # "intent":Landroid/content/Intent;
    const-string v31, "from-myfiles"

    const/16 v32, 0x1

    move-object/from16 v0, v19

    move-object/from16 v1, v31

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 976
    const-string v31, "android.intent.extra.STREAM"

    move-object/from16 v0, v19

    move-object/from16 v1, v31

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 978
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v31

    const-string v32, "sidesync.app.action.permission.KMS_FILETRANSFER_DRAG_FILEINFO"

    move-object/from16 v0, v31

    move-object/from16 v1, v19

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 980
    const/16 v31, 0x2

    const-string v32, "AbsBrowserFragment"

    const-string v33, "startDragMode() send intent : KMS_FILETRANSFER_DRAG_FILEINFO"

    invoke-static/range {v31 .. v33}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 997
    .end local v16    # "i":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSideSyncControl:Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;

    move-object/from16 v31, v0

    invoke-static {}, Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;->isPSSRunning()Z

    move-result v31

    if-eqz v31, :cond_a

    .line 999
    new-instance v19, Landroid/content/Intent;

    .end local v19    # "intent":Landroid/content/Intent;
    const-string v31, "com.sec.android.sidesync.source.START_DRAG"

    move-object/from16 v0, v19

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1001
    .restart local v19    # "intent":Landroid/content/Intent;
    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setClipData(Landroid/content/ClipData;)V

    .line 1003
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1005
    const/16 v31, 0x2

    const-string v32, "AbsBrowserFragment"

    const-string v33, "startDragMode() send intent : START_DRAG"

    invoke-static/range {v31 .. v33}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 1008
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->startDrag()V

    .line 1010
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v31

    invoke-static/range {v31 .. v31}, Lcom/sec/android/app/myfiles/utils/Utils;->getScreenState(Landroid/content/Context;)I

    move-result v31

    const/16 v32, 0x2

    move/from16 v0, v31

    move/from16 v1, v32

    if-eq v0, v1, :cond_b

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v31

    invoke-static/range {v31 .. v31}, Lcom/sec/android/app/myfiles/utils/Utils;->getScreenState(Landroid/content/Context;)I

    move-result v31

    const/16 v32, 0x3

    move/from16 v0, v31

    move/from16 v1, v32

    if-eq v0, v1, :cond_b

    .line 1013
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->startDrag()V

    .line 1017
    :cond_b
    const/16 v30, 0x0

    .line 1018
    .local v30, "width":I
    const/4 v15, 0x0

    .line 1059
    .local v15, "height":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v31

    const v32, 0x7f040014

    const/16 v33, 0x0

    invoke-virtual/range {v31 .. v33}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 1060
    .local v6, "dragView":Landroid/view/View;
    const v31, 0x7f0f0031

    move/from16 v0, v31

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/ImageView;

    .line 1061
    .local v18, "icon":Landroid/widget/ImageView;
    const v31, 0x7f0f005f

    move/from16 v0, v31

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Landroid/widget/TextView;

    .line 1062
    .local v23, "itemsCount":Landroid/widget/TextView;
    const v31, 0x7f0f005d

    move/from16 v0, v31

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 1063
    .local v9, "fileName":Landroid/widget/TextView;
    const v31, 0x7f0f005c

    move/from16 v0, v31

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 1064
    .local v10, "fileSize":Landroid/widget/TextView;
    invoke-static/range {v28 .. v28}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1065
    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->size()I

    move-result v31

    if-lez v31, :cond_d

    .line 1066
    const/16 v31, 0x0

    move-object/from16 v0, v29

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    .end local v21    # "item":Lcom/sec/android/app/myfiles/element/BrowserItem;
    check-cast v21, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .line 1067
    .restart local v21    # "item":Lcom/sec/android/app/myfiles/element/BrowserItem;
    if-eqz p2, :cond_d

    const-string v31, "ITEM_INITIATING_DRAG_PATH"

    move-object/from16 v0, p2

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v31

    if-eqz v31, :cond_d

    .line 1068
    const-string v31, "ITEM_INITIATING_DRAG_PATH"

    move-object/from16 v0, p2

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 1069
    .local v22, "itemInitiagingDragPath":Ljava/lang/String;
    if-eqz v22, :cond_d

    .line 1070
    invoke-virtual/range {v29 .. v29}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .local v17, "i$":Ljava/util/Iterator;
    :cond_c
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v31

    if-eqz v31, :cond_d

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    .local v25, "mItem":Ljava/lang/Object;
    move-object/from16 v31, v25

    .line 1071
    check-cast v31, Lcom/sec/android/app/myfiles/element/BrowserItem;

    invoke-virtual/range {v31 .. v31}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getPath()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_c

    move-object/from16 v21, v25

    .line 1072
    check-cast v21, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .line 1079
    .end local v17    # "i$":Ljava/util/Iterator;
    .end local v22    # "itemInitiagingDragPath":Ljava/lang/String;
    .end local v25    # "mItem":Ljava/lang/Object;
    :cond_d
    if-eqz v21, :cond_e

    .line 1080
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v21

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->setDragImage(Landroid/widget/ImageView;Lcom/sec/android/app/myfiles/element/BrowserItem;Z)V

    .line 1081
    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getPath()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getDragFileText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1084
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v31

    const-string v32, "fonts/Roboto-Medium.ttf"

    invoke-static/range {v31 .. v32}, Lcom/sec/android/app/myfiles/utils/MyFilesTypeFaceLoader;->getTypeface(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1085
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    move-object/from16 v31, v0

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/myfiles/element/BrowserItem;->getPath()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getItemSize(Ljava/lang/String;)J

    move-result-wide v32

    move-wide/from16 v0, v32

    move-object/from16 v2, v21

    iput-wide v0, v2, Lcom/sec/android/app/myfiles/element/BrowserItem;->mSize:J

    .line 1086
    move-object/from16 v0, v21

    iget-wide v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mSize:J

    move-wide/from16 v32, v0

    const-wide/16 v34, 0x0

    cmp-long v31, v32, v34

    if-lez v31, :cond_e

    .line 1087
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v31

    move-object/from16 v0, v21

    iget-wide v0, v0, Lcom/sec/android/app/myfiles/element/BrowserItem;->mSize:J

    move-wide/from16 v32, v0

    invoke-static/range {v31 .. v33}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1090
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v31

    const-string v32, "fonts/Roboto-Medium.ttf"

    invoke-static/range {v31 .. v32}, Lcom/sec/android/app/myfiles/utils/MyFilesTypeFaceLoader;->getTypeface(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1093
    :cond_e
    const/16 v31, 0x1

    move/from16 v0, v28

    move/from16 v1, v31

    if-ne v0, v1, :cond_f

    .line 1094
    const v31, 0x7f0f005e

    move/from16 v0, v31

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v24

    check-cast v24, Landroid/widget/RelativeLayout;

    .line 1095
    .local v24, "itemsCountLayout":Landroid/widget/RelativeLayout;
    const/16 v31, 0x8

    move-object/from16 v0, v24

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1098
    .end local v24    # "itemsCountLayout":Landroid/widget/RelativeLayout;
    :cond_f
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->isAdded()Z

    move-result v31

    if-eqz v31, :cond_11

    .line 1099
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v31

    const v32, 0x7f0901a0

    invoke-virtual/range {v31 .. v32}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v31

    move/from16 v0, v31

    float-to-int v0, v0

    move/from16 v30, v0

    .line 1100
    const/16 v31, 0x63

    move/from16 v0, v28

    move/from16 v1, v31

    if-le v0, v1, :cond_13

    .line 1101
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v31

    const v32, 0x7f0901b2

    invoke-virtual/range {v31 .. v32}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v31

    move/from16 v0, v31

    float-to-int v0, v0

    move/from16 v31, v0

    add-int v30, v30, v31

    .line 1105
    :cond_10
    :goto_a
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v31

    const v32, 0x7f0901a1

    invoke-virtual/range {v31 .. v32}, Landroid/content/res/Resources;->getDimension(I)F
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v31

    move/from16 v0, v31

    float-to-int v15, v0

    .line 1111
    :cond_11
    :goto_b
    new-instance v31, Landroid/app/ActionBar$LayoutParams;

    const/16 v32, -0x1

    const/16 v33, -0x2

    invoke-direct/range {v31 .. v33}, Landroid/app/ActionBar$LayoutParams;-><init>(II)V

    move-object/from16 v0, v31

    invoke-virtual {v6, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1114
    const/high16 v31, 0x40000000    # 2.0f

    invoke-static/range {v30 .. v31}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v31

    const/high16 v32, 0x40000000    # 2.0f

    move/from16 v0, v32

    invoke-static {v15, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v32

    move/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v6, v0, v1}, Landroid/view/View;->measure(II)V

    .line 1119
    const/16 v31, 0x0

    const/16 v32, 0x0

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v33

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v34

    move/from16 v0, v31

    move/from16 v1, v32

    move/from16 v2, v33

    move/from16 v3, v34

    invoke-virtual {v6, v0, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 1123
    if-eqz v4, :cond_0

    .line 1125
    new-instance v5, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$3;

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v6, v6}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$3;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;Landroid/view/View;Landroid/view/View;)V

    .line 1136
    .local v5, "dragShadow":Landroid/view/View$DragShadowBuilder;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-object/from16 v31, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v32

    invoke-virtual/range {v31 .. v32}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setDragSourceFragmentId(I)V

    .line 1138
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v31, v0

    if-eqz v31, :cond_12

    .line 1140
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setDragSourcePath(Ljava/lang/String;)V

    .line 1144
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFileOperationDoneReceiver:Landroid/content/BroadcastReceiver;

    move-object/from16 v32, v0

    new-instance v33, Landroid/content/IntentFilter;

    const-string v34, "com.sec.android.action.FILE_OPERATION_DONE"

    invoke-direct/range {v33 .. v34}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v31 .. v33}, Lcom/sec/android/app/myfiles/AbsMainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1148
    const/16 v31, 0x0

    const/16 v32, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    move/from16 v2, v32

    invoke-virtual {v0, v4, v5, v1, v2}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    goto/16 :goto_0

    .line 1102
    .end local v5    # "dragShadow":Landroid/view/View$DragShadowBuilder;
    :cond_13
    const/16 v31, 0x9

    move/from16 v0, v28

    move/from16 v1, v31

    if-le v0, v1, :cond_10

    .line 1103
    :try_start_2
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v31

    const v32, 0x7f0901b1

    invoke-virtual/range {v31 .. v32}, Landroid/content/res/Resources;->getDimension(I)F
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_1

    move-result v31

    move/from16 v0, v31

    float-to-int v0, v0

    move/from16 v31, v0

    add-int v30, v30, v31

    goto/16 :goto_a

    .line 1107
    :catch_1
    move-exception v8

    .line 1108
    .local v8, "ex":Ljava/lang/IllegalStateException;
    invoke-virtual {v8}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto/16 :goto_b

    .line 854
    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public startFileOperation(I)V
    .locals 6
    .param p1, "selectedMenu"    # I

    .prologue
    const/4 v5, 0x2

    .line 6423
    invoke-virtual {p0, v5}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->setSelectModeFrom(I)V

    .line 6425
    packed-switch p1, :pswitch_data_0

    .line 6468
    :goto_0
    return-void

    .line 6428
    :pswitch_0
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.sec.android.app.myfiles.PICK_SELECT_PATH"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 6432
    .local v1, "intent":Landroid/content/Intent;
    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p0, v1, v3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 6433
    :catch_0
    move-exception v0

    .line 6435
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v3, "AbsBrowserFragment"

    const-string v4, "com.sec.android.app.myfiles.PICK_SELECT_PATH not found - airbutton copy"

    invoke-static {v5, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 6441
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v1    # "intent":Landroid/content/Intent;
    :pswitch_1
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.android.app.myfiles.PICK_SELECT_PATH"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 6445
    .local v2, "intentForMove":Landroid/content/Intent;
    const/4 v3, 0x1

    :try_start_1
    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 6446
    :catch_1
    move-exception v0

    .line 6448
    .restart local v0    # "e":Landroid/content/ActivityNotFoundException;
    const-string v3, "AbsBrowserFragment"

    const-string v4, "com.sec.android.app.myfiles.PICK_SELECT_PATH not found - airbutton move"

    invoke-static {v5, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 6454
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v2    # "intentForMove":Landroid/content/Intent;
    :pswitch_2
    invoke-virtual {p0, v5}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->showDialog(I)V

    goto :goto_0

    .line 6458
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startRename()V

    goto :goto_0

    .line 6462
    :pswitch_4
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startShareAppList()V

    goto :goto_0

    .line 6425
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public startKnoxFileOperation(II)V
    .locals 8
    .param p1, "operationType"    # I
    .param p2, "personaID"    # I

    .prologue
    .line 6473
    invoke-static {p1}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(I)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v1

    .line 6475
    .local v1, "fragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    const/16 v6, 0x15

    invoke-virtual {v1, p0, v6}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 6477
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 6479
    .local v0, "arguments":Landroid/os/Bundle;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v4

    .line 6481
    .local v4, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 6483
    .local v5, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 6485
    .local v3, "item":Ljava/lang/Object;
    check-cast v3, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .end local v3    # "item":Ljava/lang/Object;
    iget-object v6, v3, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 6488
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v6, :cond_1

    .line 6490
    const-string v6, "src_folder"

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 6494
    :cond_1
    const-string v6, "target_datas"

    invoke-virtual {v0, v6, v5}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 6496
    const-string v6, "src_fragment_id"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 6498
    const/4 v6, -0x1

    if-eq p2, v6, :cond_2

    .line 6499
    const-string v6, "persona_item_id"

    invoke-virtual {v0, v6, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 6501
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    const-string v7, "KnoxOperation"

    invoke-virtual {v1, v6, v7}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 6502
    return-void
.end method

.method protected startRemoteShareDownload(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6
    .param p1, "fileId"    # Ljava/lang/String;
    .param p2, "mediaId"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "requestCode"    # I

    .prologue
    .line 4290
    const/4 v4, 0x0

    invoke-static {v4}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->newInstance(I)Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;

    move-result-object v2

    .line 4291
    .local v2, "fragment":Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;
    invoke-virtual {v2, p0, p4}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 4293
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 4294
    .local v3, "targetDatas":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4295
    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4297
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/sec/android/app/myfiles/utils/Constant;->RSHARE_DOWNLOAD_DIR_URI:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 4298
    .local v1, "fileUri":Landroid/net/Uri;
    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->downloadPath:Ljava/lang/String;

    .line 4300
    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 4301
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v4, "dst_folder"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4302
    const-string v4, "target_datas"

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 4303
    const-string v4, "src_fragment_id"

    const/16 v5, 0x25

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 4304
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "cacheDownload"

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationUiFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 4305
    return-void
.end method

.method protected startRename()V
    .locals 7

    .prologue
    .line 4397
    new-instance v2, Lcom/sec/android/app/myfiles/fragment/RenameFragment;

    invoke-direct {v2}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;-><init>()V

    .line 4399
    .local v2, "fragment":Lcom/sec/android/app/myfiles/fragment/RenameFragment;
    const/4 v5, 0x2

    invoke-virtual {v2, p0, v5}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 4401
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 4403
    .local v0, "argument":Landroid/os/Bundle;
    const-string v5, "title"

    const v6, 0x7f0b0023

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 4405
    const-string v5, "src_fragment_id"

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentId()I

    move-result v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 4407
    const/4 v4, 0x0

    .line 4413
    .local v4, "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    instance-of v5, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    if-eqz v5, :cond_1

    .line 4414
    new-instance v4, Ljava/util/ArrayList;

    .end local v4    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 4415
    .restart local v4    # "selectedItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget v5, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mContextMenuPosition:I

    invoke-virtual {p0, v5}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getBrowserItem(I)Lcom/sec/android/app/myfiles/element/BrowserItem;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4421
    :goto_0
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-gtz v5, :cond_2

    .line 4436
    :cond_0
    :goto_1
    return-void

    .line 4417
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v4

    goto :goto_0

    .line 4425
    :cond_2
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .line 4427
    .local v3, "selectedItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    const-string v5, "current_item"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectedItemStringForRename(Lcom/sec/android/app/myfiles/element/BrowserItem;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4429
    invoke-virtual {v2, v0}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->setArguments(Landroid/os/Bundle;)V

    .line 4432
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const-string v6, "rename"

    invoke-virtual {v2, v5, v6}, Lcom/sec/android/app/myfiles/fragment/RenameFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 4433
    :catch_0
    move-exception v1

    .line 4434
    .local v1, "ex":Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1
.end method

.method protected startShareAppList()V
    .locals 15

    .prologue
    .line 2391
    const/4 v5, 0x0

    .line 2392
    .local v5, "mSelectedList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    instance-of v10, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    if-eqz v10, :cond_2

    .line 2393
    new-instance v5, Ljava/util/ArrayList;

    .end local v5    # "mSelectedList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2394
    .restart local v5    # "mSelectedList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mContextMenuPosition:I

    invoke-virtual {p0, v10}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getBrowserItem(I)Lcom/sec/android/app/myfiles/element/BrowserItem;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2406
    :cond_0
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_3

    .line 2408
    const/4 v10, 0x0

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/myfiles/element/BrowserItem;

    if-eqz v10, :cond_1

    .line 2409
    const/4 v10, 0x0

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/myfiles/element/BrowserItem;

    iget-object v10, v10, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    const/4 v11, 0x1

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->share(Ljava/lang/String;Z)V

    .line 2465
    :cond_1
    :goto_0
    return-void

    .line 2396
    :cond_2
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItem()Ljava/util/ArrayList;

    move-result-object v5

    .line 2398
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v10

    const/16 v11, 0x64

    if-le v10, v11, :cond_0

    .line 2399
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    const v11, 0x7f0b0066

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    const/16 v14, 0x64

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {p0, v11, v12}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    invoke-static {v10, v11, v12}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v10

    invoke-virtual {v10}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 2412
    :cond_3
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-eqz v10, :cond_1

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v10

    const/4 v11, 0x1

    if-le v10, v11, :cond_1

    .line 2413
    const/4 v0, 0x0

    .line 2414
    .local v0, "MIMEType":Ljava/lang/String;
    const/4 v7, 0x0

    .line 2415
    .local v7, "overallMIMEtype":Ljava/lang/String;
    const/4 v9, 0x1

    .line 2417
    .local v9, "tryCheckMIMEType":Z
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2419
    .local v6, "mShareList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-ge v3, v10, :cond_7

    .line 2420
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/myfiles/element/BrowserItem;

    .line 2421
    .local v8, "selectedItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    new-instance v2, Ljava/io/File;

    iget-object v10, v8, Lcom/sec/android/app/myfiles/element/BrowserItem;->mPath:Ljava/lang/String;

    invoke-direct {v2, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2422
    .local v2, "file":Ljava/io/File;
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/myfiles/utils/FileUtils;->pathToUri(Landroid/app/Activity;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2423
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSharePathList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2424
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/myfiles/MediaFile;->getShareMimeType(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 2427
    if-nez v0, :cond_4

    .line 2429
    const/4 v10, 0x2

    const-string v11, "AbsBrowserFragment"

    const-string v12, "startShareAppList MIMEType = null "

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2434
    :cond_4
    if-eqz v9, :cond_6

    .line 2435
    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    const-string v11, ".txt"

    invoke-virtual {v10, v11}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    const-string v10, "text/plain"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 2437
    const-string v0, "application/txt"

    .line 2439
    :cond_5
    invoke-static {v0, v7}, Lcom/sec/android/app/myfiles/utils/Utils;->checkShareMIMEType(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2442
    const-string v10, "application/*"

    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 2443
    const/4 v9, 0x0

    .line 2419
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 2447
    .end local v2    # "file":Ljava/io/File;
    .end local v8    # "selectedItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    :cond_7
    const/4 v10, 0x2

    const-string v11, "AbsBrowserFragment"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "startShareAppList MIMEType = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 2449
    new-instance v4, Landroid/content/Intent;

    const-string v10, "android.intent.action.SEND_MULTIPLE"

    invoke-direct {v4, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2450
    .local v4, "intent":Landroid/content/Intent;
    invoke-virtual {v4, v7}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2451
    const-string v10, "from-myfiles"

    const/4 v11, 0x1

    invoke-virtual {v4, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2452
    const-string v10, "android.intent.extra.STREAM"

    invoke-virtual {v4, v10, v6}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 2454
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0b001f

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v4, v10}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    .line 2455
    .local v1, "chooserIntent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    const-class v11, Lcom/sec/android/app/myfiles/MyFilesChooserActivity;

    invoke-virtual {v1, v10, v11}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 2456
    const-string v10, "path_list"

    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSharePathList:Ljava/util/ArrayList;

    invoke-virtual {v1, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2457
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->startActivity(Landroid/content/Intent;)V

    .line 2458
    iget-object v10, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSharePathList:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_0
.end method

.method protected switchFragments(IILjava/lang/String;Landroid/os/Bundle;)V
    .locals 10
    .param p1, "srcFragmentId"    # I
    .param p2, "dstFragmentId"    # I
    .param p3, "dstFolder"    # Ljava/lang/String;
    .param p4, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 3963
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v6, p3}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setCurrentDirectory(Ljava/lang/String;)V

    .line 3965
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v6, :cond_1

    .line 3966
    const/16 v6, 0x8

    if-ne p1, v6, :cond_0

    if-ne p2, p1, :cond_0

    .line 3968
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mDropbox:Ljava/lang/String;

    const-string v8, ""

    invoke-virtual {p3, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z

    .line 3969
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->finishSelectMode()V

    .line 3971
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v2

    .line 3972
    .local v2, "dropboxpath":Ljava/lang/String;
    const-string v6, "/"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 3973
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mDropboxRoot:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 3978
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->AsyncOpen()V

    .line 3979
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v1

    .line 3980
    .local v1, "c":Landroid/database/Cursor;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 3981
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "ISSELECTOR"

    const/4 v8, 0x1

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 3982
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mIsSelector:Z

    .line 3986
    .end local v1    # "c":Landroid/database/Cursor;
    .end local v2    # "dropboxpath":Ljava/lang/String;
    :cond_0
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 3988
    const/16 v6, 0x1f

    if-ne p1, v6, :cond_1

    if-ne p2, p1, :cond_1

    .line 3990
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    const-string v7, "Baidu/"

    const-string v8, ""

    invoke-virtual {p3, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z

    .line 3991
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->finishSelectMode()V

    .line 3993
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v2

    .line 3994
    .restart local v2    # "dropboxpath":Ljava/lang/String;
    const-string v6, "/"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 3995
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    const-string v7, "/Baidu"

    invoke-virtual {v6, v7}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 4000
    :goto_1
    const/4 v6, 0x1

    sput-boolean v6, Lcom/sec/android/app/myfiles/utils/Utils;->needUpdate:Z

    .line 4001
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->AsyncOpen()V

    .line 4002
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v1

    .line 4003
    .restart local v1    # "c":Landroid/database/Cursor;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 4004
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "ISSELECTOR"

    const/4 v8, 0x1

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 4005
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mIsSelector:Z

    .line 4010
    .end local v1    # "c":Landroid/database/Cursor;
    .end local v2    # "dropboxpath":Ljava/lang/String;
    :cond_1
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->isCategoryFolder(I)Z

    move-result v3

    .line 4011
    .local v3, "isCategoryFolder":Z
    const/4 v4, 0x0

    .line 4014
    .local v4, "isCheck":Z
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v6

    if-eqz v6, :cond_11

    .line 4015
    if-nez v3, :cond_7

    invoke-static {p3}, Lcom/sec/android/app/myfiles/utils/Utils;->isBaiduFolder(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/16 v6, 0x1f

    if-ne p1, v6, :cond_7

    :cond_2
    const/16 v6, 0x1f

    if-ne p1, v6, :cond_3

    const/16 v6, 0x1f

    if-ne p2, v6, :cond_7

    :cond_3
    const/16 v6, 0xa

    if-ne p1, v6, :cond_4

    const/16 v6, 0x201

    if-eq p2, v6, :cond_7

    :cond_4
    const/16 v6, 0xa

    if-ne p1, v6, :cond_5

    const/16 v6, 0x24

    if-eq p2, v6, :cond_7

    :cond_5
    const/16 v6, 0x201

    if-ne p1, v6, :cond_6

    const/16 v6, 0xa

    if-eq p2, v6, :cond_7

    :cond_6
    const/16 v6, 0xa

    if-ne p1, v6, :cond_10

    const/16 v6, 0xa

    if-ne p2, v6, :cond_10

    :cond_7
    const/4 v4, 0x1

    .line 4022
    :goto_2
    if-eqz v4, :cond_8

    const/16 v6, 0x1f

    if-eq p1, v6, :cond_8

    const/16 v6, 0x1f

    if-ne p2, v6, :cond_8

    .line 4023
    const/4 v6, 0x0

    const-string v7, "AbsBrowserFragment"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "1. baidu isCheck:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 4024
    const/4 v6, 0x1

    sput-boolean v6, Lcom/sec/android/app/myfiles/utils/Utils;->needUpdate:Z

    .line 4036
    :cond_8
    :goto_3
    const/4 v6, 0x0

    const-string v7, "AbsBrowserFragment"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "1. isCheck:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 4038
    if-eqz v4, :cond_d

    .line 4040
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 4041
    .local v0, "args":Landroid/os/Bundle;
    const/4 v5, 0x0

    .line 4042
    .local v5, "return_path":Ljava/lang/String;
    if-eqz v3, :cond_19

    .line 4043
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/Utils;->getCategoryTag(I)Ljava/lang/String;

    move-result-object v5

    .line 4060
    :cond_9
    :goto_4
    const-string v6, "src_fragment_id"

    invoke-virtual {v0, v6, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 4061
    const-string v6, "dest_fragment_id"

    invoke-virtual {v0, v6, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 4062
    const-string v6, "FOLDERPATH"

    invoke-virtual {v0, v6, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4063
    const-string v6, "RETURNPATH"

    invoke-virtual {v0, v6, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4064
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mIsSelector:Z

    .line 4065
    const-string v6, "ISSELECTOR"

    const/4 v7, 0x1

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 4067
    if-eqz p4, :cond_b

    .line 4069
    const-string v6, "dst-ftp-param"

    invoke-virtual {p4, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 4071
    const-string v6, "dst-ftp-param"

    const-string v7, "dst-ftp-param"

    invoke-virtual {p4, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4075
    :cond_a
    const-string v6, "src-ftp-param"

    invoke-virtual {p4, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 4077
    const-string v6, "src-ftp-param"

    const-string v7, "src-ftp-param"

    invoke-virtual {p4, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 4083
    :cond_b
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v6, :cond_c

    instance-of v6, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    if-eqz v6, :cond_c

    .line 4084
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goUp()Z

    .line 4086
    :cond_c
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mCallback:Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$onCopyMoveSelectedListener;

    invoke-interface {v6, v0, p3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$onCopyMoveSelectedListener;->onCopyMoveSelected(Landroid/os/Bundle;Ljava/lang/String;)V

    .line 4088
    .end local v0    # "args":Landroid/os/Bundle;
    .end local v5    # "return_path":Ljava/lang/String;
    :cond_d
    return-void

    .line 3975
    .end local v3    # "isCategoryFolder":Z
    .end local v4    # "isCheck":Z
    .restart local v2    # "dropboxpath":Ljava/lang/String;
    :cond_e
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mDropbox:Ljava/lang/String;

    const-string v7, ""

    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 3976
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mDropboxRoot:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3997
    :cond_f
    const-string v6, "Baidu/"

    const-string v7, ""

    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 3998
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "/Baidu"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 4015
    .end local v2    # "dropboxpath":Ljava/lang/String;
    .restart local v3    # "isCategoryFolder":Z
    .restart local v4    # "isCheck":Z
    :cond_10
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 4028
    :cond_11
    if-nez v3, :cond_17

    invoke-static {p3}, Lcom/sec/android/app/myfiles/utils/Utils;->isDropboxFolder(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_12

    const/16 v6, 0x8

    if-ne p1, v6, :cond_17

    :cond_12
    const/16 v6, 0x8

    if-ne p1, v6, :cond_13

    const/16 v6, 0x8

    if-ne p2, v6, :cond_17

    :cond_13
    const/16 v6, 0xa

    if-ne p1, v6, :cond_14

    const/16 v6, 0x201

    if-eq p2, v6, :cond_17

    :cond_14
    const/16 v6, 0xa

    if-ne p1, v6, :cond_15

    const/16 v6, 0x24

    if-eq p2, v6, :cond_17

    :cond_15
    const/16 v6, 0x201

    if-ne p1, v6, :cond_16

    const/16 v6, 0xa

    if-eq p2, v6, :cond_17

    :cond_16
    const/16 v6, 0xa

    if-ne p1, v6, :cond_18

    const/16 v6, 0xa

    if-ne p2, v6, :cond_18

    :cond_17
    const/4 v4, 0x1

    :goto_5
    goto/16 :goto_3

    :cond_18
    const/4 v4, 0x0

    goto :goto_5

    .line 4045
    .restart local v0    # "args":Landroid/os/Bundle;
    .restart local v5    # "return_path":Ljava/lang/String;
    :cond_19
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v6, :cond_1a

    .line 4046
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v5

    .line 4047
    :cond_1a
    const/16 v6, 0x8

    if-ne p1, v6, :cond_1c

    .line 4048
    if-eqz v5, :cond_1b

    .line 4049
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mDropbox:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_4

    .line 4051
    :cond_1b
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mDropbox:Ljava/lang/String;

    goto/16 :goto_4

    .line 4053
    :cond_1c
    const/16 v6, 0x1f

    if-ne p1, v6, :cond_9

    .line 4054
    if-eqz v5, :cond_1d

    .line 4055
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Baidu/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_4

    .line 4057
    :cond_1d
    const-string v5, "Baidu/"

    goto/16 :goto_4
.end method

.method protected unselectAllItem()V
    .locals 3

    .prologue
    .line 3518
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v0, :cond_0

    .line 3520
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mActivity:Lcom/sec/android/app/myfiles/MainActivity;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/MainActivity;->setSelectOptionsVisibility(ZZ)V

    .line 3523
    :cond_0
    return-void
.end method

.method protected updateAdapter(Landroid/database/Cursor;)V
    .locals 8
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v7, 0x2

    const/4 v6, -0x1

    const/16 v5, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 3582
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->RemoveAllPaddingThumbnailMessage()V

    .line 3585
    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectorCategoryType:I

    if-eq v1, v6, :cond_1

    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectorCategoryType:I

    const/4 v2, 0x5

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/SelectorActivity;

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectorCategoryType:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    .line 3689
    :cond_0
    :goto_0
    return-void

    .line 3589
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v1, :cond_0

    .line 3592
    if-nez p1, :cond_3

    .line 3593
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 3594
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectionType()I

    move-result v1

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/SelectorActivity;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectAllButton:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    .line 3596
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectAllButton:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 3597
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllContainer:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_2

    .line 3598
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 3601
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->notifyDataSetChanged()V

    goto :goto_0

    .line 3605
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectionType()I

    move-result v1

    if-ne v1, v4, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/SelectorActivity;

    if-eqz v1, :cond_5

    const-string v1, "format"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v6, :cond_5

    .line 3608
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectAllButton:Landroid/widget/TextView;

    if-eqz v1, :cond_5

    .line 3609
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectAllButton:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 3610
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllContainer:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_4

    .line 3611
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 3613
    :cond_4
    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_5

    .line 3614
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 3615
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 3616
    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    if-eq v1, v5, :cond_c

    .line 3617
    const-string v1, "format"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_d

    .line 3618
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectAllButton:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 3619
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllContainer:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_5

    .line 3620
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 3639
    .end local v0    # "i":I
    :cond_5
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->notifyDataSetChanged()V

    .line 3642
    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_7

    .line 3643
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mEmptyView:Landroid/view/View;

    if-eqz v1, :cond_7

    .line 3651
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->unselectAllItem()V

    .line 3652
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectModeFrom()I

    move-result v1

    if-nez v1, :cond_6

    .line 3653
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->finishSelectMode()V

    .line 3654
    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->dismissDialog(I)V

    .line 3657
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    instance-of v1, v1, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v1, :cond_7

    .line 3658
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/MainActivity;->updateEmptyView()V

    .line 3664
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->getSelectModeFrom()I

    move-result v1

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->validateSelectedItems()Z

    move-result v1

    if-nez v1, :cond_8

    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v2, 0x9

    if-eq v1, v2, :cond_8

    .line 3667
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->finishSelectMode()V

    .line 3669
    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->dismissDialog(I)V

    .line 3671
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v1, :cond_9

    .line 3672
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 3674
    :cond_9
    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    if-eq v1, v5, :cond_a

    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mFragmentId:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_0

    .line 3675
    :cond_a
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    if-eqz v1, :cond_b

    .line 3676
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->awakeScrollBar()V

    .line 3677
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setSelection(I)V

    .line 3680
    :cond_b
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    if-eqz v1, :cond_0

    .line 3681
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    new-instance v2, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$20;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment$20;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 3625
    .restart local v0    # "i":I
    :cond_c
    const-string v1, "format"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-ne v1, v4, :cond_d

    .line 3626
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mSelectAllButton:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 3627
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllContainer:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_5

    .line 3628
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/SelectorActivity;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/SelectorActivity;->mSelectAllContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_2

    .line 3633
    :cond_d
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 3615
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1
.end method
