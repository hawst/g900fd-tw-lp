.class public Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;
.super Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;
.source "KnoxFileOperationTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;
    }
.end annotation


# static fields
.field private static final FAIL_ERROR_CODE_CANCELED:I = 0x2

.field private static final FAIL_ERROR_CODE_CONTAINER_STATE_PROBLEM:I = 0x1

.field private static final FAIL_ERROR_CODE_FILE_MOVE_FAIL:I = 0x8

.field private static final FAIL_ERROR_CODE_NOT_ALLOWED_FILENAME:I = 0x5

.field private static final FAIL_ERROR_CODE_NOT_ALLOWED_SRCPATH:I = 0x4

.field private static final FAIL_ERROR_CODE_NOT_ERROR:I = 0x0

.field private static final FAIL_ERROR_CODE_SRCDIR_REMOVE_FAIL:I = 0x3

.field private static final FAIL_ERROR_CODE_SRC_NOT_EXIST:I = 0x6

.field private static final FAIL_ERROR_CODE_STORAGE_FULL:I = 0x7

.field private static final KNOX_EXTRA_KEY_ERROR_CODE:Ljava/lang/String; = "ERRORCODE"

.field private static final KNOX_EXTRA_KEY_PACKAGE_NAME:Ljava/lang/String; = "PACKAGENAME"

.field private static final KNOX_EXTRA_KEY_PROGRESS:Ljava/lang/String; = "PROGRESS"

.field private static final KNOX_EXTRA_KEY_SUCCESS_CNT:Ljava/lang/String; = "SUCCESSCNT"

.field private static final KNOX_EXTRA_KEY_TARGET_PATH:Ljava/lang/String; = "PATH"

.field private static final MODULE:Ljava/lang/String; = "KnoxFileOperationTask"


# instance fields
.field private mCurPercentage:I

.field private mCurrentProgress:I

.field private mErrorCode:I

.field private mFailedCount:I

.field private mFileRelayComplete:Z

.field private mFileRelayDone:Z

.field private mFileRelayError:Z

.field private mFirstProgress:Z

.field private mKnoxReceiver:Landroid/content/BroadcastReceiver;

.field private mPM:Landroid/os/PersonaManager;

.field private mProgressView:Landroid/view/View;

.field private mRCPInterfaceCallBack:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;

.field private mRcpm:Landroid/os/RCPManager;

.field private mResult:I

.field private mThreadId:J

.field private mWaitKnowReplyFlag:Z

.field private mWaitKnoxReply:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILandroid/view/View;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "operation"    # I
    .param p3, "contentType"    # I
    .param p4, "progressView"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 102
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;-><init>(Landroid/content/Context;IILandroid/view/View;)V

    .line 69
    iput v2, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mErrorCode:I

    .line 73
    iput-boolean v2, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mWaitKnowReplyFlag:Z

    .line 85
    iput-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mRCPInterfaceCallBack:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;

    .line 87
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mThreadId:J

    .line 89
    iput v2, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mCurPercentage:I

    .line 91
    iput-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mPM:Landroid/os/PersonaManager;

    .line 93
    iput-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mRcpm:Landroid/os/RCPManager;

    .line 97
    iput v2, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mCurrentProgress:I

    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mFirstProgress:Z

    .line 468
    new-instance v0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$2;-><init>(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mKnoxReceiver:Landroid/content/BroadcastReceiver;

    .line 103
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->setKnoxReceiver()V

    .line 104
    new-instance v0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;

    invoke-direct {v0, p0, v3}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;-><init>(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$1;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mRCPInterfaceCallBack:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$RCPInterfaceCallBack;

    .line 105
    iput-object p4, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressView:Landroid/view/View;

    .line 106
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mErrorCode:I

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;[Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->publishProgress([Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$102(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mErrorCode:I

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mFirstProgress:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mFirstProgress:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mCurrentProgress:I

    return v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mCurrentProgress:I

    return p1
.end method

.method static synthetic access$1300(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;[Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->publishProgress([Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;[Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->publishProgress([Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$302(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mFileRelayDone:Z

    return p1
.end method

.method static synthetic access$402(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mFileRelayError:Z

    return p1
.end method

.method static synthetic access$508(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mFailedCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mFailedCount:I

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mFileRelayComplete:Z

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;[Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->publishProgress([Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mWaitKnoxReply:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mWaitKnowReplyFlag:Z

    return p1
.end method

.method private setKnoxReceiver()V
    .locals 3

    .prologue
    .line 394
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 396
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.knox.container.FileRelayDone"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 398
    const-string v1, "com.sec.knox.container.FileRelayFail"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 400
    const-string v1, "com.sec.knox.container.FileRelayComplete"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 403
    const-string v1, "com.sec.knox.container.FileRelayProgress"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 405
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mKnoxReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 406
    return-void
.end method

.method private waitReplyFromKnoxAgent()V
    .locals 4

    .prologue
    .line 412
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->isCancelled:Z

    if-eqz v1, :cond_0

    .line 461
    :goto_0
    return-void

    .line 417
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mWaitKnoxReply:Ljava/lang/Runnable;

    if-nez v1, :cond_1

    .line 419
    new-instance v1, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$1;-><init>(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mWaitKnoxReply:Ljava/lang/Runnable;

    .line 436
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mWaitKnoxReply:Ljava/lang/Runnable;

    monitor-enter v2

    .line 440
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->isCancelled:Z

    if-eqz v1, :cond_2

    .line 442
    monitor-exit v2

    goto :goto_0

    .line 460
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 445
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mWaitKnoxReply:Ljava/lang/Runnable;

    invoke-virtual {v1, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 447
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mWaitKnowReplyFlag:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 451
    :goto_1
    :try_start_2
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mWaitKnowReplyFlag:Z

    if-eqz v1, :cond_3

    .line 453
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mWaitKnoxReply:Ljava/lang/Runnable;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 456
    :catch_0
    move-exception v0

    .line 458
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 460
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public cancelMove(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 341
    return-void
.end method

.method protected cancelOperation()V
    .locals 2

    .prologue
    .line 361
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->cancelOperation()V

    .line 372
    iget v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mOperationType:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mOperationType:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    :cond_0
    const-string v0, "2.0"

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getKNOXVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 375
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mFileRelayError:Z

    .line 389
    :goto_0
    return-void

    .line 380
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/utils/Utils;->KNOXFileRelayCancel(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 384
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mWaitKnoxReply:Ljava/lang/Runnable;

    monitor-enter v1

    .line 386
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mWaitKnoxReply:Ljava/lang/Runnable;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 387
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected varargs doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;
    .locals 12
    .param p1, "params"    # [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .prologue
    .line 120
    const/4 v7, 0x0

    aget-object v3, p1, v7

    .line 122
    .local v3, "param":Lcom/sec/android/app/myfiles/element/FileOperationParam;
    iget-object v7, v3, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mSrcFolder:Ljava/lang/String;

    iput-object v7, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mSrcFolder:Ljava/lang/String;

    .line 124
    iget-object v7, v3, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    iput-object v7, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    .line 126
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    if-nez v7, :cond_0

    .line 127
    const/4 v7, 0x0

    .line 227
    :goto_0
    return-object v7

    .line 129
    :cond_0
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    iput v8, v7, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mTotalTarget:I

    .line 131
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget v8, v3, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mStartIndex:I

    iput v8, v7, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTarget:I

    .line 134
    new-instance v2, Landroid/content/Intent;

    const-string v7, "com.sec.knox.container.FileRelayRequest"

    invoke-direct {v2, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 136
    .local v2, "intent":Landroid/content/Intent;
    iget v7, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mOperationType:I

    packed-switch v7, :pswitch_data_0

    .line 218
    :goto_1
    iget-boolean v7, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mFileRelayComplete:Z

    if-nez v7, :cond_1

    iget-boolean v7, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->isCancelled:Z

    if-eqz v7, :cond_7

    .line 227
    :cond_1
    const/4 v7, 0x0

    goto :goto_0

    .line 143
    :pswitch_0
    const/4 v4, 0x0

    .line 144
    .local v4, "result":I
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget v7, v7, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTarget:I

    mul-int/lit8 v7, v7, 0x64

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    div-int/2addr v7, v8

    iput v7, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mCurPercentage:I

    .line 145
    iget v7, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mOperationType:I

    const/4 v8, 0x5

    if-eq v7, v8, :cond_2

    iget v7, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mOperationType:I

    const/4 v8, 0x6

    if-ne v7, v8, :cond_5

    :cond_2
    const-string v7, "2.0"

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/sec/android/app/myfiles/utils/Utils;->getKNOXVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 149
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mContext:Landroid/content/Context;

    const-string v8, "rcp"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/os/RCPManager;

    iput-object v7, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mRcpm:Landroid/os/RCPManager;

    .line 151
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mRcpm:Landroid/os/RCPManager;

    if-nez v7, :cond_3

    .line 152
    const/4 v7, 0x0

    const-string v8, "KnoxFileOperationTask"

    const-string v9, " mRcpm is null ! "

    invoke-static {v7, v8, v9}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 153
    const/4 v7, 0x0

    goto :goto_0

    .line 156
    :cond_3
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 157
    .local v5, "userId":I
    const/4 v7, 0x0

    invoke-static {v7}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 159
    .local v6, "userId0":I
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    .line 161
    .local v0, "destFilePaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_0
    iget v7, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mOperationType:I

    const/4 v8, 0x5

    if-ne v7, v8, :cond_4

    .line 162
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget v8, v3, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mContainerId:I

    iput v8, v7, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mTargetId:I

    .line 166
    :goto_2
    const/4 v7, 0x1

    new-array v7, v7, [Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    aput-object v9, v7, v8

    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->publishProgress([Ljava/lang/Object;)V

    .line 167
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressBar:Landroid/widget/ProgressBar;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget v8, v8, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mCurrentTargetPercentage:I

    invoke-virtual {v7, v8}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 170
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mRcpm:Landroid/os/RCPManager;

    const/4 v8, 0x4

    iget-object v9, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v7, v8, v9, v0}, Landroid/os/RCPManager;->moveFilesForApp(ILjava/util/List;Ljava/util/List;)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mThreadId:J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 171
    :catch_0
    move-exception v1

    .line 173
    .local v1, "e":Landroid/os/RemoteException;
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mFileRelayError:Z

    .line 174
    const/16 v7, 0x8

    iput v7, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mErrorCode:I

    .line 175
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 177
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 164
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_4
    :try_start_1
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iput v6, v7, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mTargetId:I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 185
    .end local v0    # "destFilePaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v5    # "userId":I
    .end local v6    # "userId0":I
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    iget-object v9, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v9

    iget v10, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mOperationType:I

    iget v11, v3, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mContainerId:I

    invoke-static {v7, v8, v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/Utils;->KNOXFileRelayRequest(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;II)I

    move-result v4

    .line 189
    if-eqz v4, :cond_6

    .line 190
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mFileRelayError:Z

    .line 191
    const/16 v7, 0x8

    iput v7, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mErrorCode:I

    .line 192
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 195
    :cond_6
    const/4 v7, 0x1

    new-array v7, v7, [Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    aput-object v9, v7, v8

    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->publishProgress([Ljava/lang/Object;)V

    .line 196
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressBar:Landroid/widget/ProgressBar;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget v8, v8, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mCurrentTargetPercentage:I

    invoke-virtual {v7, v8}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto/16 :goto_1

    .line 224
    .end local v4    # "result":I
    :cond_7
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->waitReplyFromKnoxAgent()V

    goto/16 :goto_1

    .line 136
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 43
    check-cast p1, [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V
    .locals 3
    .param p1, "result"    # Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .prologue
    .line 347
    const v0, 0x7f0b0017

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->showToast(I)V

    .line 349
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mKnoxReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 351
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mKnoxReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 354
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;->onCancelled(Ljava/util/ArrayList;I)V

    .line 356
    return-void
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 43
    check-cast p1, Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->onCancelled(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V

    return-void
.end method

.method protected onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V
    .locals 9
    .param p1, "result"    # Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 261
    const-string v3, "KnoxFileOperationTask"

    const-string v4, " onPostExecute() start "

    invoke-static {v6, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 262
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    if-eqz v3, :cond_4

    .line 265
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget v3, v3, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTarget:I

    if-lez v3, :cond_2

    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mFileRelayComplete:Z

    if-eqz v3, :cond_2

    .line 274
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b014a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 275
    .local v2, "knoxName":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mPM:Landroid/os/PersonaManager;

    invoke-static {}, Landroid/os/PersonaManager;->getKnoxInfo()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "KnoxIdNamePair"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    .line 276
    .local v1, "KnoxIdNamePair":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/String;>;"
    if-eqz v1, :cond_0

    .line 277
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget v3, v3, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mTargetId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "knoxName":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 280
    .restart local v2    # "knoxName":Ljava/lang/String;
    :cond_0
    const/4 v0, 0x0

    .line 281
    .local v0, "KNOXTitle":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget v3, v3, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTarget:I

    if-ne v3, v7, :cond_6

    .line 282
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mContext:Landroid/content/Context;

    const v4, 0x7f0b014b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v2, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 286
    :cond_1
    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->showToast(Ljava/lang/String;)V

    .line 289
    .end local v0    # "KNOXTitle":Ljava/lang/String;
    .end local v1    # "KnoxIdNamePair":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/String;>;"
    .end local v2    # "knoxName":Ljava/lang/String;
    :cond_2
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mFileRelayError:Z

    if-eqz v3, :cond_3

    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mErrorCode:I

    if-eqz v3, :cond_3

    .line 291
    const-string v3, "KnoxFileOperationTask"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " onPostExecute() - mErrorCode "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mErrorCode:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 293
    iget v3, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mErrorCode:I

    sparse-switch v3, :sswitch_data_0

    .line 316
    :cond_3
    :goto_1
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->isCancelled:Z

    if-eqz v3, :cond_7

    .line 318
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    invoke-interface {v3, v8, v6}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;->onCancelled(Ljava/util/ArrayList;I)V

    .line 326
    :cond_4
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mTargetFolder:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/android/app/myfiles/utils/Utils;->sendScan(Landroid/content/Context;Ljava/lang/String;)V

    .line 327
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mSrcFolder:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/sec/android/app/myfiles/utils/Utils;->sendScan(Landroid/content/Context;Ljava/lang/String;)V

    .line 329
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mKnoxReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v3, :cond_5

    .line 331
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mKnoxReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v4}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 333
    :cond_5
    return-void

    .line 283
    .restart local v0    # "KNOXTitle":Ljava/lang/String;
    .restart local v1    # "KnoxIdNamePair":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/String;>;"
    .restart local v2    # "knoxName":Ljava/lang/String;
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget v3, v3, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTarget:I

    if-le v3, v7, :cond_1

    .line 284
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mContext:Landroid/content/Context;

    const v4, 0x7f0b014c

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget v5, v5, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTarget:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    aput-object v2, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 296
    .end local v0    # "KNOXTitle":Ljava/lang/String;
    .end local v1    # "KnoxIdNamePair":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/String;>;"
    .end local v2    # "knoxName":Ljava/lang/String;
    :sswitch_0
    const v3, 0x7f0b0067

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->showToast(I)V

    goto :goto_1

    .line 300
    :sswitch_1
    const v3, 0x7f0b0017

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->showToast(I)V

    goto :goto_1

    .line 303
    :sswitch_2
    const v3, 0x7f0b00cb

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->showToast(I)V

    goto :goto_1

    .line 322
    :cond_7
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget v4, v4, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mTotalTarget:I

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget v5, v5, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mTotalTarget:I

    invoke-interface {v3, v4, v5, v8}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;->onCompleted(IILandroid/os/Bundle;)V

    goto :goto_2

    .line 293
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x7 -> :sswitch_0
        0x8 -> :sswitch_2
    .end sparse-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 43
    check-cast p1, Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Lcom/sec/android/app/myfiles/element/FileOperationProgress;)V
    .locals 5
    .param p1, "values"    # [Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 231
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;->onProgressUpdate([Ljava/lang/Object;)V

    .line 233
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mCountText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 235
    aget-object v0, p1, v3

    iget v0, v0, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mTotalTarget:I

    if-ne v0, v4, :cond_1

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mCountText:Landroid/widget/TextView;

    const-string v1, "1 / 1"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 244
    :goto_0
    aget-object v0, p1, v3

    iget v0, v0, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mTotalTarget:I

    if-ne v0, v4, :cond_2

    .line 246
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mPercentText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v2, p1, v3

    iget v2, v2, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mCurrentTargetPercentage:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " %"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 255
    :cond_0
    :goto_1
    return-void

    .line 241
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mCountText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v2, p1, v3

    iget v2, v2, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTarget:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-object v2, p1, v3

    iget v2, v2, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mTotalTarget:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 250
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mPercentText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v2, p1, v3

    iget v2, v2, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTarget:I

    mul-int/lit8 v2, v2, 0x64

    aget-object v3, p1, v3

    iget v3, v3, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mTotalTarget:I

    div-int/2addr v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " %"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 43
    check-cast p1, [Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->onProgressUpdate([Lcom/sec/android/app/myfiles/element/FileOperationProgress;)V

    return-void
.end method

.method protected setupProgressView()V
    .locals 2

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 113
    return-void
.end method
