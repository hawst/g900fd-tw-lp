.class public Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$HistoryAsyncOpenOperationFragment;
.super Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;
.source "HistoryNavigation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/navigation/HistoryNavigation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HistoryAsyncOpenOperationFragment"
.end annotation


# instance fields
.field private mRunnable:Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$AsyncOpenRunnable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 260
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/openoperation/AsyncOpenOperationUiFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public AsyncOpenExcute()V
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$HistoryAsyncOpenOperationFragment;->mRunnable:Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$AsyncOpenRunnable;

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$HistoryAsyncOpenOperationFragment;->mRunnable:Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$AsyncOpenRunnable;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$AsyncOpenRunnable;->run()V

    .line 283
    :cond_0
    return-void
.end method

.method public initialize(Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$AsyncOpenRunnable;Lcom/sec/android/app/myfiles/fragment/AbsFragment;)V
    .locals 1
    .param p1, "runnable"    # Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$AsyncOpenRunnable;
    .param p2, "parentFragment"    # Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .prologue
    .line 264
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$HistoryAsyncOpenOperationFragment;->mRunnable:Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$AsyncOpenRunnable;

    .line 266
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$HistoryAsyncOpenOperationFragment;->mCancelButtonActive:Z

    .line 268
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$HistoryAsyncOpenOperationFragment;->mWaitOnCancel:Z

    .line 270
    iput-object p2, p0, Lcom/sec/android/app/myfiles/navigation/HistoryNavigation$HistoryAsyncOpenOperationFragment;->mParentFrgment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    .line 272
    return-void
.end method
