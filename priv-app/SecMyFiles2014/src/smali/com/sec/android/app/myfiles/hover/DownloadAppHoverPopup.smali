.class public Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;
.super Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
.source "DownloadAppHoverPopup.java"


# static fields
.field private static final MODULE:Ljava/lang/String; = "DownloadAppHoverPopup"


# instance fields
.field private layout:Landroid/widget/RelativeLayout;

.field private mAppName:Landroid/widget/TextView;

.field private mAppSize:Landroid/widget/TextView;

.field private mCacheSize:Landroid/widget/TextView;

.field private mDatalSize:Landroid/widget/TextView;

.field private mTotalSize:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 48
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;-><init>(Landroid/content/Context;)V

    .line 39
    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->layout:Landroid/widget/RelativeLayout;

    .line 41
    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mAppName:Landroid/widget/TextView;

    .line 42
    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mTotalSize:Landroid/widget/TextView;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mAppSize:Landroid/widget/TextView;

    .line 44
    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mDatalSize:Landroid/widget/TextView;

    .line 45
    iput-object v0, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mCacheSize:Landroid/widget/TextView;

    .line 49
    return-void
.end method


# virtual methods
.method protected changeList()V
    .locals 0

    .prologue
    .line 195
    return-void
.end method

.method protected changeListThreadStart()V
    .locals 0

    .prologue
    .line 201
    return-void
.end method

.method protected dissmissPopup()V
    .locals 4

    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->isShow()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 165
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 166
    const/4 v1, 0x0

    const-string v2, "DownloadAppHoverPopup"

    const-string v3, "downloadapp_hover_popup.dismiss(); "

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 174
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    .line 177
    :cond_0
    return-void

    .line 168
    :catch_0
    move-exception v0

    .line 170
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method protected initViews()Z
    .locals 9

    .prologue
    .line 54
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 56
    .local v4, "res":Landroid/content/res/Resources;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 57
    .local v0, "appName":Ljava/lang/StringBuilder;
    const v6, 0x7f0b0033

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    const-string v6, " : "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mAppEntry:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->getLabel()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 62
    .local v5, "totalStr":Ljava/lang/StringBuilder;
    const v6, 0x7f0b004a

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mAppEntry:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->getTotalSize()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 67
    .local v1, "appStr":Ljava/lang/StringBuilder;
    const v6, 0x7f0b0098

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    const-string v6, " : "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mAppEntry:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->getCodeSize()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 72
    .local v3, "dataStr":Ljava/lang/StringBuilder;
    const v6, 0x7f0b0099

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    const-string v6, " : "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mAppEntry:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->getDataSize()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 77
    .local v2, "cacheStr":Ljava/lang/StringBuilder;
    const v6, 0x7f0b009a

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    const-string v6, " : "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mAppEntry:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->getCacheSize()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    new-instance v6, Landroid/app/Dialog;

    iget-object v7, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-direct {v6, v7}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    .line 83
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v7, 0x7f040025

    const/4 v8, 0x0

    invoke-static {v6, v7, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout;

    iput-object v6, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->layout:Landroid/widget/RelativeLayout;

    .line 85
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->layout:Landroid/widget/RelativeLayout;

    const v7, 0x7f0f00aa

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mAppName:Landroid/widget/TextView;

    .line 86
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mAppName:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->layout:Landroid/widget/RelativeLayout;

    const v7, 0x7f0f00ab

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mTotalSize:Landroid/widget/TextView;

    .line 89
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mTotalSize:Landroid/widget/TextView;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->layout:Landroid/widget/RelativeLayout;

    const v7, 0x7f0f00ac

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mAppSize:Landroid/widget/TextView;

    .line 92
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mAppSize:Landroid/widget/TextView;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->layout:Landroid/widget/RelativeLayout;

    const v7, 0x7f0f00ad

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mDatalSize:Landroid/widget/TextView;

    .line 95
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mDatalSize:Landroid/widget/TextView;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->layout:Landroid/widget/RelativeLayout;

    const v7, 0x7f0f00ae

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mCacheSize:Landroid/widget/TextView;

    .line 98
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mCacheSize:Landroid/widget/TextView;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->setDialogProperties()V

    .line 101
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->setDialogPosition()V

    .line 103
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    iget-object v7, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v7}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 105
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->layout:Landroid/widget/RelativeLayout;

    new-instance v7, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup$1;

    invoke-direct {v7, p0}, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup$1;-><init>(Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;)V

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 151
    const/4 v6, 0x1

    return v6
.end method

.method protected setDialogPosition()V
    .locals 4

    .prologue
    .line 181
    const/4 v1, 0x0

    const-string v2, "DownloadAppHoverPopup"

    const-string v3, "setPosition"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    .line 183
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 184
    .local v0, "params":Landroid/view/WindowManager$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mHoverRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 185
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mHoverRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->getStatusBarHeight(Landroid/content/Context;)I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 186
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 187
    iget-object v1, p0, Lcom/sec/android/app/myfiles/hover/DownloadAppHoverPopup;->mHoverDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x33

    invoke-virtual {v1, v2}, Landroid/view/Window;->setGravity(I)V

    .line 189
    return-void
.end method

.method protected showContent()V
    .locals 0

    .prologue
    .line 158
    return-void
.end method
