.class Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;
.super Ljava/lang/Object;
.source "BaiduDownLoadOperationTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DownloadableFile"
.end annotation


# instance fields
.field public mDestinationPath:Ljava/lang/String;

.field public mDownloadPercent:J

.field public mServerPath:Ljava/lang/String;

.field public mSize:J

.field public mStarted:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;JLjava/lang/String;)V
    .locals 4
    .param p1, "serverPath"    # Ljava/lang/String;
    .param p2, "currentSize"    # J
    .param p4, "dstPath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 153
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;->mServerPath:Ljava/lang/String;

    .line 155
    iput-wide v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;->mSize:J

    .line 157
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;->mDestinationPath:Ljava/lang/String;

    .line 159
    iput-wide v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;->mDownloadPercent:J

    .line 161
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;->mStarted:Z

    .line 165
    iput-object p1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;->mServerPath:Ljava/lang/String;

    .line 166
    iput-wide p2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;->mSize:J

    .line 167
    iput-object p4, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;->mDestinationPath:Ljava/lang/String;

    .line 168
    iput-wide v2, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;->mDownloadPercent:J

    .line 169
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduDownLoadOperationTask$DownloadableFile;->mStarted:Z

    .line 170
    return-void
.end method
