.class Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment$4;
.super Landroid/content/BroadcastReceiver;
.source "AbsInputDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->setAbortReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;)V
    .locals 0

    .prologue
    .line 359
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 364
    const-string v4, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 366
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;

    .line 368
    .local v3, "enclosingFragment":Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment$4;->this$0:Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 370
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_3

    if-eqz v3, :cond_3

    .line 372
    const-string v4, "current_item"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 374
    .local v2, "currentItem":Ljava/lang/String;
    const-string v4, "current_folder"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 376
    .local v1, "currentFolder":Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxFolder(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxFolder(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->getSecretModeState()Z

    move-result v4

    if-nez v4, :cond_1

    .line 378
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->dismissAllowingStateLoss()V

    .line 381
    :cond_1
    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isExternalFolder(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->isExternalFolder(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_2
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->isAdded()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 384
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->dismissAllowingStateLoss()V

    .line 392
    .end local v0    # "args":Landroid/os/Bundle;
    .end local v1    # "currentFolder":Ljava/lang/String;
    .end local v2    # "currentItem":Ljava/lang/String;
    .end local v3    # "enclosingFragment":Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;
    :cond_3
    return-void
.end method
