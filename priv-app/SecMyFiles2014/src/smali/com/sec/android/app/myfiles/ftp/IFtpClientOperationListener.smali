.class public interface abstract Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;
.super Ljava/lang/Object;
.source "IFtpClientOperationListener.java"


# virtual methods
.method public abstract notifyCreateFolder(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;)V
.end method

.method public abstract notifyDeleteFile(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;)V
.end method

.method public abstract notifyDeleteFiles(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/util/Collection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/myfiles/ftp/FTPParams;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract notifyDeleteFolder(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;)V
.end method

.method public abstract notifyDeleteFolders(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/util/Collection;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/app/myfiles/ftp/FTPParams;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract notifyRenameItem(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract notifyUploadFile(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;Ljava/lang/String;)V
.end method
