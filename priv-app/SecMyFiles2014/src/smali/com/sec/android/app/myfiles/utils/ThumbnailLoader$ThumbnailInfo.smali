.class public Lcom/sec/android/app/myfiles/utils/ThumbnailLoader$ThumbnailInfo;
.super Ljava/lang/Object;
.source "ThumbnailLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/utils/ThumbnailLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ThumbnailInfo"
.end annotation


# instance fields
.field public mCategoryType:I

.field public mDocExtnOverlay:Landroid/widget/ImageView;

.field public mFileType:I

.field public mIconImageView:Landroid/widget/ImageView;

.field public mOverlayIconImageView:Landroid/widget/ImageView;

.field public mPath:Ljava/lang/String;

.field public mScrollState:I

.field public mThumbnailDrawable:Landroid/graphics/drawable/Drawable;

.field public mThumbnailImageView:Landroid/widget/ImageView;

.field public mThumbnailInfoContext:Landroid/content/Context;

.field public mTitleTextView:Landroid/widget/TextView;

.field public mViewMode:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 229
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
