.class Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5$1$1$1;
.super Ljava/lang/Object;
.source "CategoryBrowserFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5$1$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$3:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5$1$1;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5$1$1;)V
    .locals 0

    .prologue
    .line 759
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5$1$1$1;->this$3:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5$1$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 763
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5$1$1$1;->this$3:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5$1$1;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5$1$1;->this$2:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5$1;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5$1;->this$1:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->onRefresh()V

    .line 765
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5$1$1$1;->this$3:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5$1$1;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5$1$1;->this$2:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5$1;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5$1;->this$1:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->isMultiWindowMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 767
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5$1$1$1;->this$3:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5$1$1;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5$1$1;->this$2:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5$1;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5$1;->this$1:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "Intent.ACTION_MEDIA_SCAN_LAUNCH"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "file://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5$1$1$1;->this$3:Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5$1$1;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/fragment/CategoryBrowserFragment$5$1$1;->val$path:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 769
    :cond_0
    return-void
.end method
