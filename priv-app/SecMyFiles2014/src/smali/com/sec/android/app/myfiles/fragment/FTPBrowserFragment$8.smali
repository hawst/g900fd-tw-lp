.class Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$8;
.super Ljava/lang/Object;
.source "FTPBrowserFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->onCreateItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)V
    .locals 0

    .prologue
    .line 1699
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 7
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .local p1, "parentView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1704
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v3, v3, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v3, :cond_0

    .line 1705
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v3, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v3, v3, Lcom/sec/android/app/myfiles/MainActivity;->mIsSearchMode:Z

    if-eqz v3, :cond_0

    move v3, v4

    .line 1759
    :goto_0
    return v3

    .line 1710
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSelector:Z
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->access$500(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 1712
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v3

    invoke-interface {v3, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/Cursor;

    .line 1713
    .local v1, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 1714
    .local v2, "path":Ljava/lang/String;
    if-eqz v1, :cond_6

    .line 1716
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1717
    .local v0, "arg":Landroid/os/Bundle;
    const-string v3, "_data"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1718
    const-string v3, "ITEM_INITIATING_DRAG_PATH"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1719
    const-string v3, "ftp"

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParams:Lcom/sec/android/app/myfiles/ftp/FTPParams;
    invoke-static {v6}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->access$800(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)Lcom/sec/android/app/myfiles/ftp/FTPParams;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v3, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1721
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->isSelectMode()Z

    move-result v3

    if-nez v3, :cond_3

    .line 1723
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    invoke-virtual {v3, v5, p3, v4}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->startSelectMode(III)V

    .line 1725
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1727
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mCurrentListableView:Lcom/sec/android/app/myfiles/view/IDirectDragable;

    invoke-interface {v3, v5}, Lcom/sec/android/app/myfiles/view/IDirectDragable;->setDirectDragMode(Z)V

    :goto_1
    move v3, v5

    .line 1736
    goto :goto_0

    .line 1733
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mCurrentListableView:Lcom/sec/android/app/myfiles/view/IDirectDragable;

    invoke-interface {v3, v4}, Lcom/sec/android/app/myfiles/view/IDirectDragable;->setDirectDragMode(Z)V

    goto :goto_1

    .line 1740
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemPosition()Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1742
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 1744
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->isShowTreeView()Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v3

    if-nez v3, :cond_5

    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSideSyncControl:Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;->isKMSRunning(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSideSyncControl:Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;

    invoke-static {}, Lcom/sec/android/app/myfiles/sidesync/SideSyncControl;->isPSSRunning()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1747
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    invoke-virtual {v3, p2, v0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->startDragMode(Landroid/view/View;Landroid/os/Bundle;)V

    .end local v0    # "arg":Landroid/os/Bundle;
    .end local v1    # "cursor":Landroid/database/Cursor;
    .end local v2    # "path":Ljava/lang/String;
    :cond_6
    :goto_2
    move v3, v4

    .line 1759
    goto/16 :goto_0

    .line 1752
    .restart local v0    # "arg":Landroid/os/Bundle;
    .restart local v1    # "cursor":Landroid/database/Cursor;
    .restart local v2    # "path":Ljava/lang/String;
    :cond_7
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    invoke-virtual {v3, p2, v0}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->startDragMode(Landroid/view/View;Landroid/os/Bundle;)V

    goto :goto_2
.end method
