.class public Lcom/sec/android/app/myfiles/fragment/HistoryFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;
.source "HistoryFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/fragment/HistoryFragment$8;
    }
.end annotation


# static fields
.field private static final CONFIRM_OMA_REQUEST:I = 0x2712

.field private static final CONFIRM_OMA_TAG:Ljava/lang/String; = "confirm_oma"

.field private static final DELETE_THEN_REFRESH_REQUEST:I = 0x2711

.field private static final MODULE:Ljava/lang/String;

.field private static final MOVED_TO_PRIVATE:I = 0x2


# instance fields
.field private deletedCount:J

.field private mBrowserAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

.field private mConfirmOmaDialog:Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;

.field private mContext:Landroid/content/Context;

.field private mCursor:Landroid/database/Cursor;

.field private mCusorFilesInCurrentFolder:Landroid/database/Cursor;

.field private mDownloadContentObserver:Landroid/database/ContentObserver;

.field private mDownloadManager:Landroid/app/DownloadManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 971
    const-class v0, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->MODULE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;-><init>()V

    .line 63
    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mConfirmOmaDialog:Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;

    .line 64
    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mCursor:Landroid/database/Cursor;

    .line 65
    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mCusorFilesInCurrentFolder:Landroid/database/Cursor;

    .line 67
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->deletedCount:J

    .line 945
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$7;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$7;-><init>(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mDownloadContentObserver:Landroid/database/ContentObserver;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->deleteAllDownloads()I

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 61
    invoke-static {p0, p1}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->startActivityForIntent(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/HistoryFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->openIfFileExists(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/HistoryFragment;
    .param p1, "x1"    # J
    .param p3, "x2"    # Ljava/lang/String;
    .param p4, "x3"    # Ljava/lang/String;
    .param p5, "x4"    # Ljava/lang/String;

    .prologue
    .line 61
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->showDeleteOfRetryDownloadDialog(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;)Landroid/app/DownloadManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mDownloadManager:Landroid/app/DownloadManager;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->refreshContent()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;J)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/HistoryFragment;
    .param p1, "x1"    # J

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->deleteDownload(J)I

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    .prologue
    .line 61
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->deletedCount:J

    return-wide v0
.end method

.method static synthetic access$810(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;)J
    .locals 4
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/HistoryFragment;

    .prologue
    .line 61
    iget-wide v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->deletedCount:J

    const-wide/16 v2, 0x1

    sub-long v2, v0, v2

    iput-wide v2, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->deletedCount:J

    return-wide v0
.end method

.method private deleteAllDownloads()I
    .locals 12

    .prologue
    .line 758
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 760
    .local v2, "downloadsToRemove":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Long;>;"
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mDownloadManager:Landroid/app/DownloadManager;

    new-instance v7, Landroid/app/DownloadManager$Query;

    invoke-direct {v7}, Landroid/app/DownloadManager$Query;-><init>()V

    invoke-virtual {v6, v7}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v0

    .line 762
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_2

    .line 766
    :try_start_0
    const-string v6, "_id"

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    .line 768
    .local v4, "idCol":I
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 772
    :cond_0
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 774
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    if-nez v6, :cond_0

    .line 780
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 786
    .end local v4    # "idCol":I
    :cond_2
    const/4 v5, 0x0

    .line 788
    .local v5, "removed":I
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 790
    .local v1, "downloadId":Ljava/lang/Long;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mDownloadManager:Landroid/app/DownloadManager;

    const/4 v7, 0x1

    new-array v7, v7, [J

    const/4 v8, 0x0

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    aput-wide v10, v7, v8

    invoke-virtual {v6, v7}, Landroid/app/DownloadManager;->remove([J)I

    move-result v6

    add-int/2addr v5, v6

    .line 792
    goto :goto_0

    .line 780
    .end local v1    # "downloadId":Ljava/lang/Long;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v5    # "removed":I
    :catchall_0
    move-exception v6

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v6

    .line 793
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v5    # "removed":I
    :cond_3
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->deletedCount:J

    .line 794
    iget-wide v6, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->deletedCount:J

    int-to-long v8, v5

    add-long/2addr v6, v8

    iput-wide v6, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->deletedCount:J

    .line 796
    return v5
.end method

.method private deleteDownload(J)I
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 752
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mDownloadManager:Landroid/app/DownloadManager;

    const/4 v1, 0x1

    new-array v1, v1, [J

    const/4 v2, 0x0

    aput-wide p1, v1, v2

    invoke-virtual {v0, v1}, Landroid/app/DownloadManager;->remove([J)I

    move-result v0

    return v0
.end method

.method private openIfFileExists(Ljava/lang/String;)Z
    .locals 6
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 802
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 804
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 806
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 808
    const/16 v3, 0x28

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->getCurrentSortBy()I

    move-result v5

    invoke-static {v3, v4, v1, v2, v5}, Lcom/sec/android/app/myfiles/utils/FileUtils;->openFile(ILjava/io/File;Landroid/app/Activity;ZI)V

    .line 810
    const/4 v1, 0x1

    .line 816
    .end local v0    # "file":Ljava/io/File;
    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method private refreshContent()V
    .locals 2

    .prologue
    .line 403
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mCusorFilesInCurrentFolder:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mCusorFilesInCurrentFolder:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 405
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mCusorFilesInCurrentFolder:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 406
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mCusorFilesInCurrentFolder:Landroid/database/Cursor;

    .line 409
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mCusorFilesInCurrentFolder:Landroid/database/Cursor;

    .line 411
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mCusorFilesInCurrentFolder:Landroid/database/Cursor;

    if-eqz v1, :cond_5

    .line 415
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 417
    .local v0, "omaItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/HistoryItem;>;"
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mCusorFilesInCurrentFolder:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 421
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mCusorFilesInCurrentFolder:Landroid/database/Cursor;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/element/HistoryItem;->isOmaPendingFromCursor(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 423
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mCusorFilesInCurrentFolder:Landroid/database/Cursor;

    invoke-static {v1}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getFromCacheCursor(Landroid/database/Cursor;)Lcom/sec/android/app/myfiles/element/HistoryItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 427
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mCusorFilesInCurrentFolder:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_1

    .line 431
    :cond_3
    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->showOmaDialogs(Ljava/util/ArrayList;)V

    .line 436
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mCusorFilesInCurrentFolder:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mCusorFilesInCurrentFolder:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_5

    .line 438
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mCusorFilesInCurrentFolder:Landroid/database/Cursor;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 440
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->invalidateOptionsMenu()V

    .line 447
    .end local v0    # "omaItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/HistoryItem;>;"
    :cond_5
    return-void
.end method

.method private requestDownload(J)Z
    .locals 7
    .param p1, "id"    # J

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 738
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mDownloadManager:Landroid/app/DownloadManager;

    const/4 v4, 0x1

    new-array v4, v4, [J

    const/4 v5, 0x0

    aput-wide p1, v4, v5

    invoke-virtual {v3, v4}, Landroid/app/DownloadManager;->restartDownload([J)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 746
    :goto_0
    return v1

    .line 742
    :catch_0
    move-exception v0

    .line 744
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v3, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->MODULE:Ljava/lang/String;

    const-string v4, "Cannot restart incomplete download!"

    invoke-static {v1, v3, v4, v0}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v1, v2

    .line 746
    goto :goto_0
.end method

.method private resetFragmentState()V
    .locals 1

    .prologue
    .line 486
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 488
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->finishSelectMode()V

    .line 492
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->AsyncOpen()V

    .line 494
    return-void
.end method

.method private showDeleteOfRetryDownloadDialog(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "externalId"    # J
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "localPath"    # Ljava/lang/String;
    .param p5, "source"    # Ljava/lang/String;

    .prologue
    .line 684
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 686
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0b0166

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 688
    const v2, 0x7f0b0167

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 690
    const v2, 0x7f0b0168

    new-instance v3, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$4;

    invoke-direct {v3, p0, p1, p2}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$4;-><init>(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;J)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 710
    const v2, 0x7f0b0020

    new-instance v3, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$5;

    invoke-direct {v3, p0, p1, p2}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$5;-><init>(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;J)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 728
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 730
    .local v1, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 732
    return-void
.end method

.method private showOmaDialogs(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/HistoryItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 451
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/HistoryItem;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 453
    const/16 v0, 0x2712

    invoke-static {p1, p0, v0}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->getDialog(Ljava/util/ArrayList;Landroid/app/Fragment;I)Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mConfirmOmaDialog:Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;

    .line 455
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mConfirmOmaDialog:Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;

    if-eqz v0, :cond_0

    .line 457
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mConfirmOmaDialog:Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "confirm_oma"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->showAllowingStateLoss(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 461
    :cond_0
    return-void
.end method

.method private static startActivityForIntent(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 822
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    .line 826
    :try_start_0
    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 836
    :cond_0
    :goto_0
    return-void

    .line 828
    :catch_0
    move-exception v0

    .line 830
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const/4 v1, 0x1

    sget-object v2, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->MODULE:Ljava/lang/String;

    const-string v3, "Activity not found"

    invoke-static {v1, v2, v3, v0}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public AsyncOpenCompleted()V
    .locals 0

    .prologue
    .line 397
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->refreshContent()V

    .line 399
    return-void
.end method

.method protected HistoryDetailFragment()Landroid/app/DialogFragment;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 556
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mBrowserAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v4, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->getSelectedHistoryItems()Ljava/util/List;

    move-result-object v3

    .line 559
    .local v3, "selectedItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/element/HistoryItem;>;"
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 560
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/HistoryItem;

    .line 562
    .local v1, "historyItem":Lcom/sec/android/app/myfiles/element/HistoryItem;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getData()Ljava/lang/String;

    move-result-object v2

    .line 564
    .local v2, "path":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 566
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 568
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 570
    new-instance v4, Lcom/sec/android/app/myfiles/fragment/DetailFragment;

    invoke-direct {v4}, Lcom/sec/android/app/myfiles/fragment/DetailFragment;-><init>()V

    .line 577
    .end local v0    # "file":Ljava/io/File;
    .end local v1    # "historyItem":Lcom/sec/android/app/myfiles/element/HistoryItem;
    .end local v2    # "path":Ljava/lang/String;
    :goto_0
    return-object v4

    .line 573
    .restart local v0    # "file":Ljava/io/File;
    .restart local v1    # "historyItem":Lcom/sec/android/app/myfiles/element/HistoryItem;
    .restart local v2    # "path":Ljava/lang/String;
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mContext:Landroid/content/Context;

    const v5, 0x7f0b00ef

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 577
    .end local v0    # "file":Ljava/io/File;
    .end local v1    # "historyItem":Lcom/sec/android/app/myfiles/element/HistoryItem;
    .end local v2    # "path":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public deleteSelectedItem()V
    .locals 8

    .prologue
    .line 466
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mBrowserAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    check-cast v3, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/HistoryBrowserAdapter;->getSelectedHistoryItems()Ljava/util/List;

    move-result-object v2

    .line 469
    .local v2, "itemsToDelete":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/element/HistoryItem;>;"
    const-wide/16 v4, 0x0

    .line 470
    .local v4, "removed":J
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/element/HistoryItem;

    .line 472
    .local v1, "item":Lcom/sec/android/app/myfiles/element/HistoryItem;
    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/element/HistoryItem;->getExternalId()J

    move-result-wide v6

    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->deleteDownload(J)I

    move-result v3

    int-to-long v6, v3

    add-long/2addr v4, v6

    .line 474
    goto :goto_0

    .line 476
    .end local v1    # "item":Lcom/sec/android/app/myfiles/element/HistoryItem;
    :cond_0
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->deletedCount:J

    .line 477
    iget-wide v6, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->deletedCount:J

    add-long/2addr v6, v4

    iput-wide v6, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->deletedCount:J

    .line 480
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->resetFragmentState()V

    .line 482
    return-void
.end method

.method public getOptionsMenuResId(Z)I
    .locals 1
    .param p1, "isSelectMode"    # Z

    .prologue
    .line 316
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 318
    const v0, 0x7f0e0018

    .line 322
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0e0009

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 499
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 501
    sparse-switch p1, :sswitch_data_0

    .line 533
    :goto_0
    return-void

    .line 505
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->finishSelectMode()V

    .line 507
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->AsyncOpen()V

    goto :goto_0

    .line 515
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->AsyncOpen()V

    goto :goto_0

    .line 523
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->AsyncOpen()V

    goto :goto_0

    .line 501
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_2
        0x2711 -> :sswitch_0
        0x2712 -> :sswitch_1
    .end sparse-switch
.end method

.method public onBackPressed()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 877
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 879
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->finishSelectMode()V

    .line 898
    :cond_0
    :goto_0
    return v2

    .line 885
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v0, :cond_0

    .line 887
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v0, "android.intent.action.VIEW_DOWNLOADS"

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 890
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->finish()V

    goto :goto_0

    .line 893
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 72
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onCreate(Landroid/os/Bundle;)V

    .line 74
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->deletedCount:J

    .line 76
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mContext:Landroid/content/Context;

    .line 78
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v1, p0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->setSupportAsync(Lcom/sec/android/app/myfiles/fragment/AbsFragment;)V

    .line 80
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v1, :cond_0

    .line 82
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mBrowserAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    .line 86
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mContext:Landroid/content/Context;

    const-string v2, "download"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/DownloadManager;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mDownloadManager:Landroid/app/DownloadManager;

    .line 88
    new-instance v1, Landroid/app/DownloadManager$Query;

    invoke-direct {v1}, Landroid/app/DownloadManager$Query;-><init>()V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/DownloadManager$Query;->setOnlyIncludeVisibleInDownloadsUi(Z)Landroid/app/DownloadManager$Query;

    move-result-object v0

    .line 90
    .local v0, "query":Landroid/app/DownloadManager$Query;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mDownloadManager:Landroid/app/DownloadManager;

    invoke-virtual {v1, v0}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mCursor:Landroid/database/Cursor;

    .line 92
    return-void
.end method

.method protected onCreateDetailFragmentListener()Landroid/app/DialogFragment;
    .locals 1

    .prologue
    .line 545
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->HistoryDetailFragment()Landroid/app/DialogFragment;

    move-result-object v0

    return-object v0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 275
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 277
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    packed-switch p1, :pswitch_data_0

    .line 308
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v1

    :goto_0
    return-object v1

    .line 281
    :pswitch_0
    const v1, 0x7f0b0020

    new-instance v2, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 294
    const v1, 0x7f0b0017

    new-instance v2, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$2;-><init>(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 303
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    .line 277
    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreateFileOperationListener()Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;
    .locals 1

    .prologue
    .line 539
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 597
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$3;-><init>(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;)V

    return-object v0
.end method

.method protected onCreateItemLongClickListener()Landroid/widget/AdapterView$OnItemLongClickListener;
    .locals 1

    .prologue
    .line 841
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment$6;-><init>(Lcom/sec/android/app/myfiles/fragment/HistoryFragment;)V

    return-object v0
.end method

.method protected onCreateMultipleDetailFragmentListener()Landroid/app/DialogFragment;
    .locals 1

    .prologue
    .line 583
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/HistoryMultipleDetailFragment;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/fragment/HistoryMultipleDetailFragment;-><init>()V

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 172
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onDestroy()V

    .line 174
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->deletedCount:J

    .line 175
    return-void
.end method

.method protected onDropFinished(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 590
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->AsyncOpen()V

    .line 592
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x1

    .line 198
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 237
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    :goto_0
    return v2

    .line 202
    :sswitch_0
    const/4 v3, -0x1

    const/4 v4, 0x0

    invoke-virtual {p0, v2, v3, v4}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->startSelectMode(III)V

    goto :goto_0

    .line 213
    :sswitch_1
    const/16 v3, 0xd

    invoke-virtual {p0, v3}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->showDialog(I)V

    goto :goto_0

    .line 221
    :sswitch_2
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-string v4, "com.android.providers.downloads.ui"

    const-string v5, "com.android.providers.downloads.ui.saveas.SecDownloadSaveasPreferences"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 225
    .local v1, "intent":Landroid/content/Intent;
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 226
    :catch_0
    move-exception v0

    .line 228
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const/4 v3, 0x2

    sget-object v4, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->MODULE:Ljava/lang/String;

    const-string v5, "com.android.providers.downloads.ui.saveas.SecDownloadSaveasPreferences not found - download settings"

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 198
    :sswitch_data_0
    .sparse-switch
        0x7f0f0137 -> :sswitch_0
        0x7f0f0145 -> :sswitch_1
        0x7f0f0146 -> :sswitch_2
    .end sparse-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mConfirmOmaDialog:Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mConfirmOmaDialog:Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/ConfirmOmaDialog;->dismiss()V

    .line 158
    :cond_0
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onPause()V

    .line 160
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->deletedCount:J

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mDownloadContentObserver:Landroid/database/ContentObserver;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 167
    :cond_1
    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 8
    .param p1, "id"    # I
    .param p2, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 247
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 249
    packed-switch p1, :pswitch_data_0

    .line 269
    .end local p2    # "dialog":Landroid/app/Dialog;
    :goto_0
    return-void

    .line 253
    .restart local p2    # "dialog":Landroid/app/Dialog;
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCount()I

    move-result v0

    .line 255
    .local v0, "historyItemsCount":I
    const-string v2, ""

    .line 257
    .local v2, "title":Ljava/lang/String;
    const-string v1, ""

    .line 259
    .local v1, "message":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0169

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 261
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0011

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 263
    invoke-virtual {p2, v2}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 265
    check-cast p2, Landroid/app/AlertDialog;

    .end local p2    # "dialog":Landroid/app/Dialog;
    invoke-virtual {p2, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 249
    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_0
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 331
    invoke-super {p0, p1}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 333
    const/4 v0, 0x0

    .line 335
    .local v0, "menuItem":Landroid/view/MenuItem;
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mOptionsMenu:Landroid/view/Menu;

    if-eqz v1, :cond_6

    .line 340
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mOptionsMenu:Landroid/view/Menu;

    const v2, 0x7f0f0128

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 342
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 347
    :cond_0
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mOptionsMenu:Landroid/view/Menu;

    const v2, 0x7f0f012b

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 349
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 353
    :cond_1
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isSupportDownloadSaveAsInChinaModel()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 354
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mOptionsMenu:Landroid/view/Menu;

    const v2, 0x7f0f0146

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 355
    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 361
    :cond_2
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mOptionsMenu:Landroid/view/Menu;

    const v2, 0x7f0f0148

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 363
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 367
    :cond_3
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mOptionsMenu:Landroid/view/Menu;

    const v2, 0x7f0f013a

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 369
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 373
    :cond_4
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mOptionsMenu:Landroid/view/Menu;

    const v2, 0x7f0f0129

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 374
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 377
    :cond_5
    sget-object v1, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mOptionsMenu:Landroid/view/Menu;

    const v2, 0x7f0f0145

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 379
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v1, :cond_6

    .line 381
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCount()I

    move-result v1

    if-nez v1, :cond_7

    .line 383
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 392
    :cond_6
    :goto_0
    return-void

    .line 386
    :cond_7
    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onRefresh()V
    .locals 2

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mFragmentId:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;->refreshAdapter(I)V

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mEmptyView:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mGridView:Lcom/sec/android/app/myfiles/view/MyFilesGridView;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mEmptyView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/MyFilesGridView;->setEmptyView(Landroid/view/View;)V

    .line 190
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mListView:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mEmptyView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->setEmptyView(Landroid/view/View;)V

    .line 193
    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 6

    .prologue
    const v5, 0x7f0b017d

    const/4 v4, 0x0

    .line 109
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onResume()V

    .line 111
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mStartMoveToKnox:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->isSelectMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 113
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mStartMoveToKnox:Z

    .line 114
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->finishSelectMode()V

    .line 117
    :cond_0
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->deletedCount:J

    .line 119
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mTitleBarTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f0b014d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mCategoryTitleContainer:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 129
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_1

    .line 131
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mCursor:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mDownloadContentObserver:Landroid/database/ContentObserver;

    invoke-interface {v1, v2}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 149
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mTreeViewAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    const-string v2, "40"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;->setCategoryItemListSelection(Ljava/lang/String;)I

    .line 150
    return-void

    .line 135
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mContext:Landroid/content/Context;

    const v2, 0x7f0b017d

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 136
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView()V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 139
    :catch_0
    move-exception v0

    .line 141
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 143
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 145
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mContext:Landroid/content/Context;

    invoke-static {v1, v5, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 146
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView()V

    goto :goto_0
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 97
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->onStart()V

    .line 99
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->isSelectMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->AsyncOpen()V

    .line 104
    :cond_0
    return-void
.end method

.method protected selectAllItem()V
    .locals 1

    .prologue
    .line 907
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->selectAllItem()V

    .line 909
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 911
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mBrowserAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->selectAllItem()V

    .line 915
    :cond_0
    return-void
.end method

.method protected unselectAllItem()V
    .locals 1

    .prologue
    .line 920
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->unselectAllItem()V

    .line 922
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->isSelectMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 924
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/HistoryFragment;->mBrowserAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->unselectAllItem()V

    .line 928
    :cond_0
    return-void
.end method
