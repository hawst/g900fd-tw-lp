.class Lcom/sec/android/app/myfiles/MainActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "MainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/MainActivity;->setExternalStorageReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/MainActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/MainActivity;)V
    .locals 0

    .prologue
    .line 1466
    iput-object p1, p0, Lcom/sec/android/app/myfiles/MainActivity$1;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x2

    .line 1471
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1473
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 1475
    .local v1, "uri":Landroid/net/Uri;
    const-string v2, "MainActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Broadcast receive : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " , uri : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1477
    const-string v2, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "android.intent.action.MEDIA_SCAN"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1481
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$1;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isMountedStateCheck(Landroid/content/Context;)V

    .line 1482
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$1;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v2, v2, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    if-eqz v2, :cond_1

    .line 1484
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$1;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v2, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->StorageListRefresh()V

    .line 1499
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$1;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/MainActivity;->access$000(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 1500
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$1;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/MainActivity;->access$000(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1502
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$1;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$1;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v2, v2, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    if-eqz v2, :cond_2

    .line 1503
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$1;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v2, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v2, v7}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->setForceRefreshPathIndicatorBar(Z)V

    .line 1541
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$1;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$1;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v2, v2, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    if-eqz v2, :cond_3

    .line 1542
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$1;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v2, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/fragment/AbsBrowserFragment;->removeDelayedMessage()V

    .line 1565
    :cond_3
    :goto_0
    return-void

    .line 1546
    :cond_4
    const-string v2, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1548
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$1;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # setter for: Lcom/sec/android/app/myfiles/MainActivity;->mFinishNearByDevice:Z
    invoke-static {v2, v6}, Lcom/sec/android/app/myfiles/MainActivity;->access$102(Lcom/sec/android/app/myfiles/MainActivity;Z)Z

    .line 1549
    invoke-static {v7}, Lcom/sec/android/app/myfiles/utils/Utils;->setMediaScannerScannig(Z)V

    goto :goto_0

    .line 1551
    :cond_5
    const-string v2, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1553
    invoke-static {v6}, Lcom/sec/android/app/myfiles/utils/Utils;->setMediaScannerScannig(Z)V

    .line 1561
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$1;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/MainActivity;->access$000(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 1562
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$1;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/MainActivity;->access$000(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1563
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$1;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # invokes: Lcom/sec/android/app/myfiles/MainActivity;->refreshFragmentsLocation()V
    invoke-static {v2}, Lcom/sec/android/app/myfiles/MainActivity;->access$200(Lcom/sec/android/app/myfiles/MainActivity;)V

    goto :goto_0
.end method
