.class public Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;
.super Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
.source "FTPBrowserAdapter.java"


# static fields
.field private static final DOT:C = '.'

.field private static final MODULE:Ljava/lang/String; = "FTPBrowserAdapter"

.field private static final SELECTABLE_DIRS:Z = true


# instance fields
.field private mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

.field private mSelectedItemBackground:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "c"    # Landroid/database/Cursor;
    .param p4, "flags"    # I
    .param p5, "navigation"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .prologue
    .line 64
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 486
    const/4 v0, 0x2

    const/16 v1, 0x55

    const/16 v2, 0x6a

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->mSelectedItemBackground:I

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->mContext:Landroid/content/Context;

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-static {v0, v1}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    new-instance v0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 71
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;

    .prologue
    .line 57
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->mSelectedItemBackground:I

    return v0
.end method

.method private getDefaultFileIcon(ZLjava/lang/String;)I
    .locals 1
    .param p1, "isDir"    # Z
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 473
    if-eqz p1, :cond_0

    .line 474
    const v0, 0x7f0200af

    .line 476
    :goto_0
    return v0

    :cond_0
    invoke-static {p2}, Lcom/sec/android/app/myfiles/MediaFile;->getSmallIcon(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private static getParentDir(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 276
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getParentFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 278
    .local v0, "parent":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 284
    .end local v0    # "parent":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "parent":Ljava/lang/String;
    :cond_0
    sget-object v0, Ljava/io/File;->separator:Ljava/lang/String;

    goto :goto_0
.end method

.method protected static getText(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "fileName"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 292
    if-eqz p1, :cond_0

    if-eqz p0, :cond_0

    .line 293
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowFileExtensionStatus()Z

    move-result v1

    if-nez v1, :cond_0

    .line 294
    const/16 v1, 0x2e

    invoke-virtual {p0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 295
    .local v0, "lastDot":I
    if-lez v0, :cond_0

    .line 296
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 300
    .end local v0    # "lastDot":I
    .end local p0    # "fileName":Ljava/lang/String;
    :cond_0
    return-object p0
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 12
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 77
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 79
    const-string v9, "title"

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 80
    .local v3, "fileName":Ljava/lang/String;
    const-string v9, "date"

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 81
    .local v0, "date":J
    const-string v9, "file_size"

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 82
    .local v6, "size":I
    const-string v9, "is_directory"

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 83
    .local v5, "isDir":I
    const-string v9, "_data"

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 84
    .local v4, "filePath":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;

    .line 86
    .local v8, "vh":Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;
    const/4 v9, 0x1

    if-eq v5, v9, :cond_0

    const/4 v9, 0x2

    if-ne v5, v9, :cond_1

    .line 89
    :cond_0
    iget v9, p0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->mSelectModeFrom:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_1

    .line 91
    iget v9, p0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->mSelectionType:I

    if-nez v9, :cond_4

    .line 93
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectRadioButton:Landroid/widget/RadioButton;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 102
    :cond_1
    :goto_0
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    if-eqz v9, :cond_2

    .line 103
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 105
    :cond_2
    iget v9, p0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->mViewMode:I

    packed-switch v9, :pswitch_data_0

    .line 135
    :cond_3
    :goto_1
    invoke-virtual {p0, v5, v4}, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->getOnDragListener(ILjava/lang/String;)Landroid/view/View$OnDragListener;

    move-result-object v9

    invoke-virtual {p1, v9}, Landroid/view/View;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 218
    return-void

    .line 95
    :cond_4
    iget v9, p0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->mSelectionType:I

    const/4 v10, 0x1

    if-ne v9, v10, :cond_1

    .line 97
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0

    .line 108
    :pswitch_0
    const-string v2, ""

    .line 109
    .local v2, "dateStr":Ljava/lang/String;
    const-string v7, ""

    .line 110
    .local v7, "sizeStr":Ljava/lang/String;
    const-wide/16 v10, 0x0

    cmp-long v9, v0, v10

    if-lez v9, :cond_5

    .line 111
    iget-object v9, p0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-static {v9, v0, v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    .line 113
    :cond_5
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    invoke-virtual {v9, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    if-lez v6, :cond_6

    .line 115
    iget-object v9, p0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->mContext:Landroid/content/Context;

    int-to-long v10, v6

    invoke-static {v9, v10, v11}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v7

    .line 117
    :cond_6
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    .end local v2    # "dateStr":Ljava/lang/String;
    .end local v7    # "sizeStr":Ljava/lang/String;
    :pswitch_1
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    iget-object v10, p0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3, v10}, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->getText(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    if-eqz v9, :cond_3

    .line 121
    iget-object v10, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const/4 v9, 0x1

    if-eq v5, v9, :cond_7

    const/4 v9, 0x2

    if-ne v5, v9, :cond_8

    :cond_7
    const/4 v9, 0x1

    :goto_2
    invoke-direct {p0, v9, v3}, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->getDefaultFileIcon(ZLjava/lang/String;)I

    move-result v9

    invoke-virtual {v10, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_8
    const/4 v9, 0x0

    goto :goto_2

    .line 125
    :pswitch_2
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    iget-object v10, p0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3, v10}, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->getText(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    if-eqz v9, :cond_a

    .line 127
    iget-object v10, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const/4 v9, 0x1

    if-eq v5, v9, :cond_9

    const/4 v9, 0x2

    if-ne v5, v9, :cond_b

    :cond_9
    const/4 v9, 0x1

    :goto_3
    invoke-direct {p0, v9, v3}, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->getDefaultFileIcon(ZLjava/lang/String;)I

    move-result v9

    invoke-virtual {v10, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 129
    :cond_a
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v9, :cond_3

    .line 130
    iget-object v9, v8, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 127
    :cond_b
    const/4 v9, 0x0

    goto :goto_3

    .line 105
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected getContentType()I
    .locals 1

    .prologue
    .line 310
    const/4 v0, 0x0

    return v0
.end method

.method protected getOnDragListener(ILjava/lang/String;)Landroid/view/View$OnDragListener;
    .locals 1
    .param p1, "isDir"    # I
    .param p2, "filePath"    # Ljava/lang/String;

    .prologue
    .line 315
    new-instance v0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter$1;-><init>(Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;ILjava/lang/String;)V

    return-object v0
.end method

.method public getSelectedItem()Ljava/util/ArrayList;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 223
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 225
    .local v14, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->getSelectedItemPosition()Ljava/util/ArrayList;

    move-result-object v15

    .line 227
    .local v15, "selItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz v15, :cond_2

    .line 229
    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 231
    .local v9, "i":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/database/Cursor;

    .line 233
    .local v11, "item":Landroid/database/Cursor;
    if-eqz v11, :cond_0

    .line 235
    const-string v3, "_id"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 237
    .local v4, "id":J
    const-string v3, "_data"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 239
    .local v13, "path":Ljava/lang/String;
    const-string v3, "title"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 241
    .local v12, "name":Ljava/lang/String;
    const-string v3, "is_directory"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 243
    .local v7, "type":I
    const-string v3, "file_size"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 245
    .local v16, "size":J
    invoke-static {v13}, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->getParentDir(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-wide/from16 v0, v16

    invoke-static {v12, v7, v3, v0, v1}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getItem(Ljava/lang/String;ILjava/lang/String;J)Lcom/sec/android/app/myfiles/ftp/FTPItem;

    move-result-object v8

    .line 247
    .local v8, "ftpItem":Lcom/sec/android/app/myfiles/ftp/FTPItem;
    if-eqz v8, :cond_0

    .line 249
    new-instance v2, Lcom/sec/android/app/myfiles/element/BrowserItem;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->getContentType()I

    move-result v3

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v2 .. v7}, Lcom/sec/android/app/myfiles/element/BrowserItem;-><init>(IJLjava/lang/String;I)V

    .line 251
    .local v2, "mBrowserItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    const/4 v3, 0x1

    if-ne v7, v3, :cond_1

    .line 253
    const/16 v3, 0x3001

    iput v3, v2, Lcom/sec/android/app/myfiles/element/BrowserItem;->mFormat:I

    .line 260
    :goto_1
    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 256
    :cond_1
    iput v7, v2, Lcom/sec/android/app/myfiles/element/BrowserItem;->mFormat:I

    goto :goto_1

    .line 270
    .end local v2    # "mBrowserItem":Lcom/sec/android/app/myfiles/element/BrowserItem;
    .end local v4    # "id":J
    .end local v7    # "type":I
    .end local v8    # "ftpItem":Lcom/sec/android/app/myfiles/ftp/FTPItem;
    .end local v9    # "i":I
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v11    # "item":Landroid/database/Cursor;
    .end local v12    # "name":Ljava/lang/String;
    .end local v13    # "path":Ljava/lang/String;
    .end local v16    # "size":J
    :cond_2
    return-object v14
.end method

.method public getSelectedItemForAddshortcut()Ljava/util/ArrayList;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 491
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 493
    .local v8, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->getSelectedItemPosition()Ljava/util/ArrayList;

    move-result-object v9

    .line 495
    .local v9, "selItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz v9, :cond_1

    .line 497
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 499
    .local v1, "i":I
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/database/Cursor;

    .line 501
    .local v3, "item":Landroid/database/Cursor;
    if-eqz v3, :cond_0

    .line 503
    const-string v13, "_id"

    invoke-interface {v3, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    invoke-interface {v3, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 505
    .local v4, "id":J
    const-string v13, "_data"

    invoke-interface {v3, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    invoke-interface {v3, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 507
    .local v7, "path":Ljava/lang/String;
    const-string v13, "title"

    invoke-interface {v3, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    invoke-interface {v3, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 509
    .local v6, "name":Ljava/lang/String;
    const-string v13, "is_directory"

    invoke-interface {v3, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    invoke-interface {v3, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 511
    .local v12, "type":I
    const-string v13, "file_size"

    invoke-interface {v3, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    invoke-interface {v3, v13}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 513
    .local v10, "size":J
    invoke-static {v7}, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->getParentDir(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v6, v12, v13, v10, v11}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getItem(Ljava/lang/String;ILjava/lang/String;J)Lcom/sec/android/app/myfiles/ftp/FTPItem;

    move-result-object v0

    .line 515
    .local v0, "ftpItem":Lcom/sec/android/app/myfiles/ftp/FTPItem;
    if-eqz v0, :cond_0

    .line 517
    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 527
    .end local v0    # "ftpItem":Lcom/sec/android/app/myfiles/ftp/FTPItem;
    .end local v1    # "i":I
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "item":Landroid/database/Cursor;
    .end local v4    # "id":J
    .end local v6    # "name":Ljava/lang/String;
    .end local v7    # "path":Ljava/lang/String;
    .end local v10    # "size":J
    .end local v12    # "type":I
    :cond_1
    return-object v8
.end method

.method public selectAllItem()V
    .locals 8

    .prologue
    .line 453
    iget-boolean v4, p0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->mIsSelectMode:Z

    if-eqz v4, :cond_1

    .line 454
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 455
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 456
    const-string v4, "_id"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 457
    .local v2, "idIndex":I
    const-string v4, "_data"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 458
    .local v1, "dataIndex":I
    const-string v4, "is_directory"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 463
    .local v3, "isDirIndex":I
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->mSelectedItemMap:Ljava/util/HashMap;

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 464
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_0

    .line 465
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->notifyToSelectedItemChangeListener()V

    .line 466
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->notifyDataSetChanged()V

    .line 469
    .end local v0    # "cursor":Landroid/database/Cursor;
    .end local v1    # "dataIndex":I
    .end local v2    # "idIndex":I
    .end local v3    # "isDirIndex":I
    :cond_1
    return-void
.end method
