.class Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation$1;
.super Ljava/lang/Object;
.source "DownloadFolderOperation.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->getFetchingDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation$1;->this$0:Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation$1;->this$0:Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mAbort:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->access$002(Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;Z)Z

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation$1;->this$0:Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;

    # getter for: Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mTransferring:Z
    invoke-static {v0}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->access$100(Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation$1;->this$0:Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;

    # getter for: Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;->access$200(Lcom/sec/android/app/myfiles/ftp/operation/DownloadFolderOperation;)Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->abortTransfer()V

    .line 126
    :cond_0
    return-void
.end method
