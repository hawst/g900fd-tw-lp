.class Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;
.super Ljava/lang/Object;
.source "FileBrowserFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)V
    .locals 0

    .prologue
    .line 1368
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 12
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1373
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v8, 0x0

    const-string v9, "FileBrowserFragment"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onItemClick() : parent = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1375
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v8

    invoke-interface {v8, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 1376
    .local v0, "cursor":Landroid/database/Cursor;
    const-string v8, "_id"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1377
    .local v2, "cursorId":J
    const-string v8, "_data"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1379
    .local v6, "path":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v8, v8, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->isSelectMode()Z

    move-result v8

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget v8, v8, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mRunFrom:I

    const/16 v9, 0x15

    if-ne v8, v9, :cond_c

    .line 1382
    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1384
    .local v1, "file":Ljava/io/File;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_4

    .line 1386
    :cond_1
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v8, v8, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v9, 0x7f0b005a

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    .line 1388
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)Landroid/os/Handler;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1390
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)Landroid/os/Handler;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/os/Handler;->removeMessages(I)V

    .line 1393
    :cond_2
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)Landroid/os/Handler;

    move-result-object v8

    const/4 v9, 0x0

    const-wide/16 v10, 0x1f4

    invoke-virtual {v8, v9, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1516
    .end local v1    # "file":Ljava/io/File;
    :cond_3
    :goto_0
    return-void

    .line 1400
    .restart local v1    # "file":Ljava/io/File;
    :cond_4
    const-string v8, "format"

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    const/16 v9, 0x3001

    if-ne v8, v9, :cond_7

    .line 1403
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->isSelectMode()Z

    move-result v8

    if-eqz v8, :cond_5

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v8, v8, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    if-eqz v8, :cond_5

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v8, v8, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getSelectedItemCount()I

    move-result v8

    if-lez v8, :cond_5

    .line 1405
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v8, v8, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->unselectAllItem()V

    .line 1408
    :cond_5
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    # invokes: Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->moveToFolder(Ljava/lang/String;Ljava/lang/Boolean;)V
    invoke-static {v8, v6, v9}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1410
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isCheckMountedStorage()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1412
    const/4 v5, 0x0

    .line 1414
    .local v5, "menuItem":Landroid/view/MenuItem;
    sget-object v8, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mOptionsMenu:Landroid/view/Menu;

    const v9, 0x7f0f0148

    invoke-interface {v8, v9}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 1416
    invoke-static {v6}, Lcom/sec/android/app/myfiles/utils/Utils;->isRootFolder(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_6

    const/4 v8, 0x1

    :goto_1
    invoke-interface {v5, v8}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_6
    const/4 v8, 0x0

    goto :goto_1

    .line 1422
    .end local v5    # "menuItem":Landroid/view/MenuItem;
    :cond_7
    const/4 v7, 0x0

    .line 1423
    .local v7, "selectionButton":Landroid/view/View;
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget v8, v8, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mRunFrom:I

    const/16 v9, 0x15

    if-ne v8, v9, :cond_9

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-boolean v8, v8, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mJustSelectMode:Z

    if-eqz v8, :cond_9

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getSelectionType()I

    move-result v8

    if-nez v8, :cond_9

    .line 1426
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getSelectionType()I

    move-result v8

    if-nez v8, :cond_8

    .line 1428
    const v8, 0x7f0f0038

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 1432
    :cond_8
    if-eqz v7, :cond_3

    .line 1434
    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    .line 1435
    invoke-virtual {v7}, Landroid/view/View;->performClick()Z

    .line 1436
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    goto/16 :goto_0

    .line 1439
    :cond_9
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget v8, v8, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mRunFrom:I

    const/16 v9, 0x15

    if-ne v8, v9, :cond_a

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-boolean v8, v8, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mJustSelectMode:Z

    if-eqz v8, :cond_a

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getSelectionType()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_a

    .line 1441
    const v8, 0x7f0f0037

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 1443
    if-eqz v7, :cond_3

    .line 1445
    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    .line 1446
    invoke-virtual {v7}, Landroid/view/View;->performClick()Z

    .line 1447
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    goto/16 :goto_0

    .line 1452
    :cond_a
    invoke-static {v6}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v4

    .line 1454
    .local v4, "fileTypeInt":I
    invoke-static {v4}, Lcom/sec/android/app/myfiles/MediaFile;->isArchiveFileType(I)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 1456
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    const/4 v9, 0x1

    # setter for: Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mExtractExecuteNormalMode:Z
    invoke-static {v8, v9}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->access$402(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;Z)Z

    .line 1457
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    # setter for: Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mSelectedPathForExtract:Ljava/lang/String;
    invoke-static {v8, v6}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->access$502(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 1458
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    # invokes: Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->createFolderForExtract(Ljava/lang/String;)Z
    invoke-static {v8, v6}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->access$600(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 1462
    :cond_b
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isClickValid()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1464
    const/16 v8, 0x201

    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v11}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getCurrentSortBy()I

    move-result v11

    invoke-static {v8, v1, v9, v10, v11}, Lcom/sec/android/app/myfiles/utils/FileUtils;->openFile(ILjava/io/File;Landroid/app/Activity;ZI)V

    .line 1466
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->initFileObserver()V

    .line 1468
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    new-instance v9, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5$1;

    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, p0, v10}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5$1;-><init>(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;Ljava/lang/String;)V

    iput-object v9, v8, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mFileObserver:Landroid/os/FileObserver;

    .line 1505
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v8, v8, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mFileObserver:Landroid/os/FileObserver;

    invoke-virtual {v8}, Landroid/os/FileObserver;->startWatching()V

    goto/16 :goto_0

    .line 1513
    .end local v1    # "file":Ljava/io/File;
    .end local v4    # "fileTypeInt":I
    .end local v7    # "selectionButton":Landroid/view/View;
    :cond_c
    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v8, v8, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v8, v2, v3, v6}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->multipleSelect(JLjava/lang/String;)V

    goto/16 :goto_0
.end method
