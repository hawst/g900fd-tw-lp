.class Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter$1;
.super Ljava/lang/Object;
.source "FTPBrowserAdapter.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->getOnDragListener(ILjava/lang/String;)Landroid/view/View$OnDragListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;

.field final synthetic val$filePath:Ljava/lang/String;

.field final synthetic val$isDir:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 315
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;

    iput p2, p0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter$1;->val$isDir:I

    iput-object p3, p0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter$1;->val$filePath:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 16
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    .line 320
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getAction()I

    move-result v1

    .line 322
    .local v1, "action":I
    const/4 v12, 0x0

    const-string v13, "FTPBrowserAdapter"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "onDrag() receive : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 324
    packed-switch v1, :pswitch_data_0

    .line 446
    :cond_0
    :goto_0
    const/4 v12, 0x1

    :goto_1
    return v12

    .line 327
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;

    iget-boolean v12, v12, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->mIsDragMode:Z

    if-nez v12, :cond_0

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter$1;->val$isDir:I

    const/4 v13, 0x1

    if-eq v12, v13, :cond_1

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter$1;->val$isDir:I

    const/4 v13, 0x2

    if-ne v12, v13, :cond_0

    .line 329
    :cond_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->mSelectedItemBackground:I
    invoke-static {v12}, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;)I

    move-result v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 334
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;

    iget-boolean v12, v12, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->mIsDragMode:Z

    if-nez v12, :cond_0

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter$1;->val$isDir:I

    const/4 v13, 0x1

    if-eq v12, v13, :cond_2

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter$1;->val$isDir:I

    const/4 v13, 0x2

    if-ne v12, v13, :cond_0

    .line 336
    :cond_2
    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 341
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;

    iget-boolean v12, v12, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->mIsDragMode:Z

    if-nez v12, :cond_d

    .line 343
    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/view/View;->setBackgroundColor(I)V

    .line 345
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->mDropListener:Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;

    if-eqz v12, :cond_c

    .line 347
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getClipData()Landroid/content/ClipData;

    move-result-object v2

    .line 349
    .local v2, "clipData":Landroid/content/ClipData;
    if-eqz v2, :cond_b

    invoke-virtual {v2}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v12

    if-eqz v12, :cond_b

    .line 351
    invoke-virtual {v2}, Landroid/content/ClipData;->getDescription()Landroid/content/ClipDescription;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/ClipDescription;->getLabel()Ljava/lang/CharSequence;

    move-result-object v8

    .line 353
    .local v8, "label":Ljava/lang/CharSequence;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_a

    const-string v12, "selectedFTPUri"

    invoke-virtual {v12, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_3

    const-string v12, "selectedUri"

    invoke-virtual {v12, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 356
    :cond_3
    invoke-virtual {v2}, Landroid/content/ClipData;->getItemCount()I

    move-result v6

    .line 358
    .local v6, "itemCount":I
    const/4 v10, 0x0

    .line 360
    .local v10, "selectedFilePath":Ljava/lang/String;
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 362
    .local v9, "selectedFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    if-ge v4, v6, :cond_7

    .line 364
    invoke-virtual {v2, v4}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v7

    .line 366
    .local v7, "itemUri":Landroid/content/ClipData$Item;
    if-eqz v7, :cond_4

    .line 368
    invoke-virtual {v7}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v11

    .line 370
    .local v11, "uri":Landroid/net/Uri;
    if-eqz v11, :cond_6

    .line 372
    const-string v12, "selectedFTPUri"

    invoke-virtual {v12, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 374
    invoke-static {v11}, Lcom/sec/android/app/myfiles/ftp/FTPUtils;->getItemPathFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v10

    .line 388
    :goto_3
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_4

    .line 390
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 362
    .end local v11    # "uri":Landroid/net/Uri;
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 378
    .restart local v11    # "uri":Landroid/net/Uri;
    :cond_5
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-static {v12, v11}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getFilePathFromUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v10

    goto :goto_3

    .line 384
    :cond_6
    invoke-virtual {v7}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v12

    invoke-interface {v12}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    goto :goto_3

    .line 395
    .end local v7    # "itemUri":Landroid/content/ClipData$Item;
    .end local v11    # "uri":Landroid/net/Uri;
    :cond_7
    const/4 v3, 0x0

    .line 397
    .local v3, "dstFolder":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter$1;->val$isDir:I

    const/4 v13, 0x1

    if-eq v12, v13, :cond_8

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter$1;->val$isDir:I

    const/4 v13, 0x2

    if-ne v12, v13, :cond_9

    .line 399
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter$1;->val$filePath:Ljava/lang/String;

    .line 406
    :goto_4
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->mDropListener:Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;

    invoke-interface {v12, v3, v9}, Lcom/sec/android/app/myfiles/adapter/AbsBaseAdapter$OnDropListener;->onDrop(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto/16 :goto_0

    .line 403
    :cond_9
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter$1;->val$filePath:Ljava/lang/String;

    invoke-static {v12}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getParentFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_4

    .line 411
    .end local v3    # "dstFolder":Ljava/lang/String;
    .end local v4    # "i":I
    .end local v6    # "itemCount":I
    .end local v9    # "selectedFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v10    # "selectedFilePath":Ljava/lang/String;
    :cond_a
    const/4 v12, 0x0

    goto/16 :goto_1

    .line 417
    .end local v8    # "label":Ljava/lang/CharSequence;
    :cond_b
    const/4 v12, 0x0

    goto/16 :goto_1

    .line 423
    .end local v2    # "clipData":Landroid/content/ClipData;
    :cond_c
    const/4 v12, 0x0

    goto/16 :goto_1

    .line 428
    :cond_d
    new-instance v5, Landroid/content/Intent;

    const-string v12, "com.sec.android.action.FILE_OPERATION_DONE"

    invoke-direct {v5, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 430
    .local v5, "intent":Landroid/content/Intent;
    const-string v12, "success"

    const/4 v13, 0x0

    invoke-virtual {v5, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 432
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v12, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 439
    .end local v5    # "intent":Landroid/content/Intent;
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;

    iget-boolean v12, v12, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->mIsDragMode:Z

    if-eqz v12, :cond_0

    .line 441
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;

    invoke-virtual {v12}, Lcom/sec/android/app/myfiles/adapter/FTPBrowserAdapter;->finishDrag()V

    goto/16 :goto_0

    .line 324
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
