.class public Lcom/sec/android/app/myfiles/utils/RequestManager;
.super Ljava/lang/Object;
.source "RequestManager.java"


# static fields
.field private static mRequestQueue:Lcom/android/volley/RequestQueue;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    return-void
.end method

.method public static getRequestQueue()Lcom/android/volley/RequestQueue;
    .locals 2

    .prologue
    .line 42
    sget-object v0, Lcom/sec/android/app/myfiles/utils/RequestManager;->mRequestQueue:Lcom/android/volley/RequestQueue;

    if-eqz v0, :cond_0

    .line 43
    sget-object v0, Lcom/sec/android/app/myfiles/utils/RequestManager;->mRequestQueue:Lcom/android/volley/RequestQueue;

    return-object v0

    .line 45
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static init(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-static {p0}, Lcom/android/volley/toolbox/Volley;->newRequestQueue(Landroid/content/Context;)Lcom/android/volley/RequestQueue;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/utils/RequestManager;->mRequestQueue:Lcom/android/volley/RequestQueue;

    .line 34
    return-void
.end method
