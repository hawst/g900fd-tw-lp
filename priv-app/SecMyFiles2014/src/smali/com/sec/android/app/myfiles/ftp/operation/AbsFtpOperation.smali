.class public abstract Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;
.super Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;
.source "AbsFtpOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;,
        Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FoldersFilesTuple;
    }
.end annotation


# instance fields
.field protected final mClientOperationListener:Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;

.field protected mDownloadTempDir:Ljava/lang/String;

.field protected volatile mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

.field protected final mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

.field protected mProcessingHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "operation"    # I
    .param p3, "contentType"    # I
    .param p4, "progressView"    # Landroid/view/View;
    .param p5, "ftpHandlerParams"    # Ljava/lang/String;

    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/myfiles/fileoperation/AbsFileOperationTask;-><init>(Landroid/content/Context;IILandroid/view/View;)V

    .line 939
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/android/app/myfiles/utils/Constant;->DEFAULT_TEMP_DOWNLOAD_DIR:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/.myFilesTmp"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mDownloadTempDir:Ljava/lang/String;

    .line 941
    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    iput-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 947
    new-instance v1, Landroid/os/Handler;

    new-instance v2, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$4;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;)V

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mProcessingHandler:Landroid/os/Handler;

    .line 60
    iput p3, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mSourceFragmentId:I

    .line 62
    if-eqz p5, :cond_0

    invoke-virtual {p5}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 64
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "IFTPConnectionHandler cannot be null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 67
    :cond_1
    new-instance v1, Lcom/sec/android/app/myfiles/ftp/FTPCache;

    invoke-direct {v1, p1}, Lcom/sec/android/app/myfiles/ftp/FTPCache;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mClientOperationListener:Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;

    .line 69
    invoke-static {}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->getInstance()Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;

    move-result-object v0

    .line 71
    .local v0, "factory":Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;
    if-nez v0, :cond_2

    .line 73
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "FTPConnectionHandlerFactory cannot be null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 76
    :cond_2
    new-instance v1, Lcom/sec/android/app/myfiles/ftp/FTPParams;

    invoke-direct {v1, p5}, Lcom/sec/android/app/myfiles/ftp/FTPParams;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/ftp/FTPConnectionHandlerFactory;->getFTPConnectionHandler(Lcom/sec/android/app/myfiles/ftp/FTPParams;)Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    .line 78
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    if-nez v1, :cond_3

    .line 80
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "FTPConnectionHandler cannot be null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 83
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mClientOperationListener:Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;

    invoke-interface {v1, v2}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->setFtpClientOperationListener(Lcom/sec/android/app/myfiles/ftp/IFtpClientOperationListener;)V

    .line 84
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mPercentText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mCountText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mProcessing:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mProcessing:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mProcessing:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mProcessing:Landroid/widget/TextView;

    return-object v0
.end method

.method protected static varargs checkFileOperationParam(Z[Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/ftp/FTPErr;
    .locals 7
    .param p0, "checkIfLocalExists"    # Z
    .param p1, "args"    # [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .prologue
    const/4 v6, 0x0

    .line 644
    if-eqz p1, :cond_6

    array-length v5, p1

    if-lez v5, :cond_6

    .line 646
    aget-object v4, p1, v6

    .line 648
    .local v4, "param":Lcom/sec/android/app/myfiles/element/FileOperationParam;
    iget-object v5, v4, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    if-eqz v5, :cond_0

    iget-object v5, v4, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, v4, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    if-eqz v5, :cond_0

    iget-object v5, v4, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 651
    :cond_0
    sget-object v5, Lcom/sec/android/app/myfiles/ftp/FTPErr;->WRONG_ARGUMENT:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 697
    .end local v4    # "param":Lcom/sec/android/app/myfiles/element/FileOperationParam;
    :goto_0
    return-object v5

    .line 655
    .restart local v4    # "param":Lcom/sec/android/app/myfiles/element/FileOperationParam;
    :cond_1
    if-eqz p0, :cond_3

    .line 657
    iget-object v5, v4, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 659
    .local v0, "f":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 661
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_2

    .line 663
    sget-object v5, Lcom/sec/android/app/myfiles/ftp/FTPErr;->WRONG_ARGUMENT:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    goto :goto_0

    .line 669
    .end local v0    # "f":Ljava/lang/String;
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_3
    iget-object v5, v4, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mSrcFolder:Ljava/lang/String;

    if-nez v5, :cond_4

    .line 671
    iget-object v5, v4, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getItem(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPItem;

    move-result-object v3

    .line 673
    .local v3, "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    if-eqz v3, :cond_5

    .line 675
    invoke-interface {v3}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getDir()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mSrcFolder:Ljava/lang/String;

    .line 683
    :goto_1
    iget-object v5, v4, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mSrcFolder:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 685
    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    iput-object v5, v4, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mSrcFolder:Ljava/lang/String;

    .line 691
    .end local v3    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    :cond_4
    sget-object v5, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    goto :goto_0

    .line 679
    .restart local v3    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    :cond_5
    iget-object v5, v4, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v5}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getParentFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mSrcFolder:Ljava/lang/String;

    goto :goto_1

    .line 697
    .end local v3    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .end local v4    # "param":Lcom/sec/android/app/myfiles/element/FileOperationParam;
    :cond_6
    sget-object v5, Lcom/sec/android/app/myfiles/ftp/FTPErr;->WRONG_ARGUMENT:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    goto :goto_0
.end method

.method protected static varargs checkTargetSourceSameDir([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/ftp/FTPErr;
    .locals 3
    .param p0, "args"    # [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .prologue
    .line 705
    if-eqz p0, :cond_1

    array-length v2, p0

    if-lez v2, :cond_1

    .line 707
    const/4 v2, 0x0

    aget-object v0, p0, v2

    .line 709
    .local v0, "param":Lcom/sec/android/app/myfiles/element/FileOperationParam;
    if-eqz v0, :cond_1

    .line 711
    iget-object v1, v0, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mSrcFolder:Ljava/lang/String;

    .line 713
    .local v1, "src":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 715
    iget-object v2, v0, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 717
    sget-object v2, Lcom/sec/android/app/myfiles/ftp/FTPErr;->TARGET_SAME_AS_SOURCE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 731
    .end local v0    # "param":Lcom/sec/android/app/myfiles/element/FileOperationParam;
    .end local v1    # "src":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 721
    .restart local v0    # "param":Lcom/sec/android/app/myfiles/element/FileOperationParam;
    .restart local v1    # "src":Ljava/lang/String;
    :cond_0
    sget-object v2, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    goto :goto_0

    .line 731
    .end local v0    # "param":Lcom/sec/android/app/myfiles/element/FileOperationParam;
    .end local v1    # "src":Ljava/lang/String;
    :cond_1
    sget-object v2, Lcom/sec/android/app/myfiles/ftp/FTPErr;->WRONG_ARGUMENT:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    goto :goto_0
.end method

.method protected static deleteItemsOnLocal([Ljava/lang/String;[Ljava/lang/String;)I
    .locals 9
    .param p0, "folders"    # [Ljava/lang/String;
    .param p1, "files"    # [Ljava/lang/String;

    .prologue
    .line 478
    const/4 v7, 0x0

    .line 480
    .local v7, "succeeded":I
    if-eqz p1, :cond_1

    .line 482
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v6, v0, v4

    .line 484
    .local v6, "path":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 486
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 488
    add-int/lit8 v7, v7, 0x1

    .line 482
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 496
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "file":Ljava/io/File;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .end local v6    # "path":Ljava/lang/String;
    :cond_1
    if-eqz p0, :cond_3

    array-length v8, p0

    if-lez v8, :cond_3

    .line 498
    array-length v8, p0

    add-int/lit8 v3, v8, -0x1

    .local v3, "i":I
    :goto_1
    if-ltz v3, :cond_3

    .line 500
    new-instance v2, Ljava/io/File;

    aget-object v8, p0, v3

    invoke-direct {v2, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 502
    .local v2, "folder":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 504
    add-int/lit8 v7, v7, 0x1

    .line 498
    :cond_2
    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    .line 512
    .end local v2    # "folder":Ljava/io/File;
    .end local v3    # "i":I
    :cond_3
    return v7
.end method

.method protected static discoverLocalStructure([Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FoldersFilesTuple;
    .locals 13
    .param p0, "srcFiles"    # [Ljava/lang/String;

    .prologue
    .line 777
    if-eqz p0, :cond_6

    array-length v10, p0

    if-lez v10, :cond_6

    .line 779
    new-instance v6, Ljava/util/Stack;

    invoke-direct {v6}, Ljava/util/Stack;-><init>()V

    .line 781
    .local v6, "folder":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/String;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 783
    .local v5, "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v3, Ljava/util/ArrayDeque;

    invoke-direct {v3}, Ljava/util/ArrayDeque;-><init>()V

    .line 787
    .local v3, "discoverFolders":Ljava/util/ArrayDeque;, "Ljava/util/ArrayDeque<Ljava/lang/String;>;"
    move-object v0, p0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v8, v0

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_0
    if-ge v7, v8, :cond_2

    aget-object v9, v0, v7

    .line 789
    .local v9, "path":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 791
    .local v4, "file":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 795
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 797
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 787
    :cond_0
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 799
    :cond_1
    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 801
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 807
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v4    # "file":Ljava/io/File;
    .end local v9    # "path":Ljava/lang/String;
    :cond_2
    invoke-virtual {v3}, Ljava/util/ArrayDeque;->size()I

    move-result v10

    if-lez v10, :cond_5

    .line 809
    invoke-virtual {v3}, Ljava/util/ArrayDeque;->pop()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 811
    .restart local v9    # "path":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 813
    .restart local v4    # "file":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 815
    .local v2, "childs":[Ljava/io/File;
    if-eqz v2, :cond_2

    array-length v10, v2

    if-lez v10, :cond_2

    .line 817
    move-object v0, v2

    .local v0, "arr$":[Ljava/io/File;
    array-length v8, v0

    const/4 v7, 0x0

    :goto_2
    if-ge v7, v8, :cond_2

    aget-object v1, v0, v7

    .line 819
    .local v1, "child":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 821
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    .line 825
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 817
    :cond_3
    :goto_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 827
    :cond_4
    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 829
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 839
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "child":Ljava/io/File;
    .end local v2    # "childs":[Ljava/io/File;
    .end local v4    # "file":Ljava/io/File;
    .end local v9    # "path":Ljava/lang/String;
    :cond_5
    new-instance v10, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FoldersFilesTuple;

    invoke-static {v6}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->getArrayFromPaths(Ljava/util/Collection;)[Ljava/lang/String;

    move-result-object v11

    invoke-static {v5}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->getArrayFromPaths(Ljava/util/Collection;)[Ljava/lang/String;

    move-result-object v12

    invoke-direct {v10, v11, v12}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FoldersFilesTuple;-><init>([Ljava/lang/String;[Ljava/lang/String;)V

    .line 843
    .end local v3    # "discoverFolders":Ljava/util/ArrayDeque;, "Ljava/util/ArrayDeque<Ljava/lang/String;>;"
    .end local v5    # "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v6    # "folder":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/String;>;"
    .end local v7    # "i$":I
    .end local v8    # "len$":I
    :goto_4
    return-object v10

    :cond_6
    const/4 v10, 0x0

    goto :goto_4
.end method

.method protected static getArrayFromPaths(Ljava/util/Collection;)[Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 877
    .local p0, "paths":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    if-eqz p0, :cond_0

    .line 879
    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {p0, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 883
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    goto :goto_0
.end method

.method protected static getImmediateTarget(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "ftpSourceName"    # Ljava/lang/String;
    .param p1, "localTargetPath"    # Ljava/lang/String;

    .prologue
    .line 536
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 538
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 540
    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 542
    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 546
    :cond_0
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 548
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static getParentDir(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 851
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/FileUtils;->getParentFolderPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 853
    .local v0, "parent":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .end local v0    # "parent":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "parent":Ljava/lang/String;
    :cond_0
    sget-object v0, Ljava/io/File;->separator:Ljava/lang/String;

    goto :goto_0
.end method

.method protected static getPathWithoutPrefix(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "prefix"    # Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;

    .prologue
    .line 859
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 861
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 863
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    .line 865
    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 869
    :cond_0
    invoke-static {p1}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-static {v2}, Ljava/util/regex/Matcher;->quoteReplacement(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 871
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected static getPrefix(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 891
    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 893
    .local v2, "split":[Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 895
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_1

    .line 897
    if-lez v0, :cond_0

    .line 899
    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 903
    :cond_0
    aget-object v3, v2, v0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 895
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 907
    :cond_1
    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 909
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method protected static getTarget(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "file"    # Ljava/lang/String;
    .param p1, "targetDir"    # Ljava/lang/String;
    .param p2, "srcDir"    # Ljava/lang/String;

    .prologue
    .line 394
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 396
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 398
    invoke-static {p2}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Ljava/util/regex/Matcher;->quoteReplacement(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 399
    .local v1, "result":Ljava/lang/String;
    const-string v2, "//"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 400
    const-string v2, "//"

    const-string v3, "/"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 422
    .end local v1    # "result":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v1

    .line 406
    :cond_1
    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 408
    .local v0, "filename":Ljava/lang/String;
    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 410
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 414
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 422
    .end local v0    # "filename":Ljava/lang/String;
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected static getTargetPath(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "ftpSourceFullPath"    # Ljava/lang/String;
    .param p1, "localTargetPath"    # Ljava/lang/String;
    .param p2, "ftpRootParent"    # Ljava/lang/String;

    .prologue
    .line 627
    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 630
    .local v0, "localPath":Ljava/lang/String;
    :goto_0
    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 632
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 636
    :goto_1
    return-object v1

    .end local v0    # "localPath":Ljava/lang/String;
    :cond_0
    move-object v0, p1

    .line 627
    goto :goto_0

    .line 636
    .restart local v0    # "localPath":Ljava/lang/String;
    :cond_1
    invoke-static {p2}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/util/regex/Matcher;->quoteReplacement(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method protected static makeLocalDirectory(Ljava/lang/String;)Z
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 101
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 103
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 105
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 107
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    .line 117
    .end local v0    # "dir":Ljava/io/File;
    :goto_0
    return v1

    .line 111
    .restart local v0    # "dir":Ljava/io/File;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0

    .line 117
    .end local v0    # "dir":Ljava/io/File;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected checkConnection(Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;)Lcom/sec/android/app/myfiles/ftp/FTPErr;
    .locals 3
    .param p1, "ftpHandler"    # Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    .prologue
    .line 751
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    sget-object v2, Lcom/sec/android/app/myfiles/ftp/FTPErr;->WRONG_ARGUMENT:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    if-eq v1, v2, :cond_1

    .line 753
    invoke-interface {p1}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncPrepareConnection()Z

    move-result v0

    .line 755
    .local v0, "prepared":Z
    if-eqz v0, :cond_0

    .line 757
    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 767
    .end local v0    # "prepared":Z
    :goto_0
    return-object v1

    .line 761
    .restart local v0    # "prepared":Z
    :cond_0
    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPErr;->LOGIN_FAILED:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    goto :goto_0

    .line 767
    .end local v0    # "prepared":Z
    :cond_1
    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPErr;->WRONG_ARGUMENT:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    goto :goto_0
.end method

.method protected createDirectoriesOnFtp(Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 7
    .param p1, "ftpHandler"    # Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    .param p2, "folders"    # [Ljava/lang/String;
    .param p3, "srcDir"    # Ljava/lang/String;
    .param p4, "targetDir"    # Ljava/lang/String;

    .prologue
    .line 315
    iget-boolean v6, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->isCancelled:Z

    if-nez v6, :cond_1

    if-eqz p2, :cond_1

    .line 317
    const/4 v4, 0x0

    .line 319
    .local v4, "succeeded":I
    move-object v0, p2

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v1, v0, v2

    .line 321
    .local v1, "folder":Ljava/lang/String;
    invoke-static {v1, p4, p3}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->getTargetPath(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 323
    .local v5, "targetFolder":Ljava/lang/String;
    invoke-virtual {p0, v5}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->setLabel(Ljava/lang/String;)V

    .line 325
    invoke-interface {p1, v5}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncCreateFolder(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 327
    add-int/lit8 v4, v4, 0x1

    .line 319
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 337
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "folder":Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "succeeded":I
    .end local v5    # "targetFolder":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    :cond_2
    return v4
.end method

.method protected deleteFilesOnFtp(Ljava/util/Stack;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p2, "srcPath"    # Ljava/lang/String;
    .param p3, "dstPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    .prologue
    .line 518
    .local p1, "files":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 520
    .local v0, "succeeded":I
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->isCancelled:Z

    if-nez v1, :cond_1

    invoke-virtual {p1}, Ljava/util/Stack;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 522
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1, p2, p3}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->getTargetPath(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncDeleteFile(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 524
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 530
    :cond_1
    return v0
.end method

.method protected deleteFoldersOnFtp(Ljava/util/Stack;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p2, "srcPath"    # Ljava/lang/String;
    .param p3, "dstPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    .prologue
    .line 460
    .local p1, "folders":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 462
    .local v0, "succeeded":I
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->isCancelled:Z

    if-nez v1, :cond_1

    invoke-virtual {p1}, Ljava/util/Stack;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 464
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1, p2, p3}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->getTargetPath(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncDeleteFolder(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 466
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 472
    :cond_1
    return v0
.end method

.method protected deleteItemsOnFtp(Ljava/util/Stack;Ljava/util/Stack;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPErr;
    .locals 5
    .param p3, "srcPath"    # Ljava/lang/String;
    .param p4, "dstPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/sec/android/app/myfiles/ftp/FTPErr;"
        }
    .end annotation

    .prologue
    .line 430
    .local p1, "folders":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/String;>;"
    .local p2, "files":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/String;>;"
    iget-boolean v4, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->isCancelled:Z

    if-nez v4, :cond_2

    .line 432
    invoke-virtual {p1}, Ljava/util/Stack;->size()I

    move-result v3

    .line 434
    .local v3, "foldersToDelete":I
    invoke-virtual {p2}, Ljava/util/Stack;->size()I

    move-result v2

    .line 436
    .local v2, "filesToDelete":I
    invoke-virtual {p0, p2, p3, p4}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->deleteFilesOnFtp(Ljava/util/Stack;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 438
    .local v0, "deletedFiles":I
    invoke-virtual {p0, p1, p3, p4}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->deleteFoldersOnFtp(Ljava/util/Stack;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 440
    .local v1, "deletedFolders":I
    if-ne v1, v3, :cond_0

    if-eq v0, v2, :cond_1

    .line 442
    :cond_0
    sget-object v4, Lcom/sec/android/app/myfiles/ftp/FTPErr;->PARTIALLY_DONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 452
    .end local v0    # "deletedFiles":I
    .end local v1    # "deletedFolders":I
    .end local v2    # "filesToDelete":I
    .end local v3    # "foldersToDelete":I
    :goto_0
    return-object v4

    .line 446
    .restart local v0    # "deletedFiles":I
    .restart local v1    # "deletedFolders":I
    .restart local v2    # "filesToDelete":I
    .restart local v3    # "foldersToDelete":I
    :cond_1
    sget-object v4, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    goto :goto_0

    .line 452
    .end local v0    # "deletedFiles":I
    .end local v1    # "deletedFolders":I
    .end local v2    # "filesToDelete":I
    .end local v3    # "foldersToDelete":I
    :cond_2
    sget-object v4, Lcom/sec/android/app/myfiles/ftp/FTPErr;->PARTIALLY_DONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    goto :goto_0
.end method

.method protected discoverFtp(Ljava/util/List;)Landroid/util/Pair;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/util/Pair",
            "<[",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 128
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v8, Ljava/util/ArrayDeque;

    invoke-direct {v8}, Ljava/util/ArrayDeque;-><init>()V

    .line 130
    .local v8, "jobFolder":Ljava/util/ArrayDeque;, "Ljava/util/ArrayDeque<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    new-instance v4, Ljava/util/ArrayDeque;

    invoke-direct {v4}, Ljava/util/ArrayDeque;-><init>()V

    .line 132
    .local v4, "folders":Ljava/util/ArrayDeque;, "Ljava/util/ArrayDeque<Ljava/lang/String;>;"
    new-instance v3, Ljava/util/ArrayDeque;

    invoke-direct {v3}, Ljava/util/ArrayDeque;-><init>()V

    .line 134
    .local v3, "files":Ljava/util/ArrayDeque;, "Ljava/util/ArrayDeque<Ljava/lang/String;>;"
    iget-object v10, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mContext:Landroid/content/Context;

    const v11, 0x7f0b00b0

    invoke-virtual {v10, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 136
    .local v2, "discovering":Ljava/lang/String;
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 138
    .local v7, "itemStr":Ljava/lang/String;
    iget-boolean v10, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->isCancelled:Z

    if-eqz v10, :cond_4

    .line 164
    .end local v7    # "itemStr":Ljava/lang/String;
    :cond_1
    iget-boolean v10, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->isCancelled:Z

    if-nez v10, :cond_8

    invoke-virtual {v8}, Ljava/util/ArrayDeque;->size()I

    move-result v10

    if-lez v10, :cond_8

    .line 166
    invoke-virtual {v8}, Ljava/util/ArrayDeque;->pop()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .line 168
    .local v6, "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface {v6}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getTitle()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->setLabel(Ljava/lang/String;)V

    .line 170
    iget-object v10, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v10, v6}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncBrowseForItemContent(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)Ljava/util/List;

    move-result-object v1

    .line 172
    .local v1, "children":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    if-eqz v1, :cond_1

    .line 174
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .line 176
    .local v0, "child":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    if-eqz v0, :cond_2

    .line 178
    invoke-interface {v0}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isDirectory()Z

    move-result v10

    if-nez v10, :cond_3

    invoke-interface {v0}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isSymbolicLink()Z

    move-result v10

    if-eqz v10, :cond_7

    .line 180
    :cond_3
    invoke-virtual {v8, v0}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 182
    invoke-interface {v0}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    goto :goto_1

    .line 144
    .end local v0    # "child":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .end local v1    # "children":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    .end local v6    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .restart local v7    # "itemStr":Ljava/lang/String;
    :cond_4
    invoke-static {v7}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getItem(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPItem;

    move-result-object v9

    .line 146
    .local v9, "rootItem":Lcom/sec/android/app/myfiles/ftp/FTPItem;
    if-eqz v9, :cond_0

    .line 148
    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->isDirectory()Z

    move-result v10

    if-nez v10, :cond_5

    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->isSymbolicLink()Z

    move-result v10

    if-eqz v10, :cond_6

    .line 150
    :cond_5
    invoke-virtual {v8, v9}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 152
    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 154
    :cond_6
    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->isFile()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 156
    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 184
    .end local v7    # "itemStr":Ljava/lang/String;
    .end local v9    # "rootItem":Lcom/sec/android/app/myfiles/ftp/FTPItem;
    .restart local v0    # "child":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .restart local v1    # "children":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    .restart local v6    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    :cond_7
    invoke-interface {v0}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isFile()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 186
    invoke-interface {v0}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    goto :goto_1

    .line 198
    .end local v0    # "child":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .end local v1    # "children":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    .end local v6    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    :cond_8
    new-instance v10, Landroid/util/Pair;

    invoke-virtual {v3}, Ljava/util/ArrayDeque;->size()I

    move-result v11

    new-array v11, v11, [Ljava/lang/String;

    invoke-virtual {v3, v11}, Ljava/util/ArrayDeque;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v4}, Ljava/util/ArrayDeque;->size()I

    move-result v12

    new-array v12, v12, [Ljava/lang/String;

    invoke-virtual {v4, v12}, Ljava/util/ArrayDeque;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v12

    invoke-direct {v10, v11, v12}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v10
.end method

.method protected discoverFtpAndMakeLocalDirectories(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;
    .locals 1
    .param p2, "targetFolder"    # Ljava/lang/String;
    .param p3, "srcFolder"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;"
        }
    .end annotation

    .prologue
    .line 206
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->discoverFtpAndMakeLocalDirectories(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Z)Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;

    move-result-object v0

    return-object v0
.end method

.method protected discoverFtpAndMakeLocalDirectories(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Z)Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;
    .locals 15
    .param p2, "targetFolder"    # Ljava/lang/String;
    .param p3, "srcFolder"    # Ljava/lang/String;
    .param p4, "mDirectDownload"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;"
        }
    .end annotation

    .prologue
    .line 212
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v9, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;

    invoke-direct {v9}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;-><init>()V

    .line 214
    .local v9, "res":Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 216
    .local v11, "sb":Ljava/lang/StringBuilder;
    iget-object v13, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mContext:Landroid/content/Context;

    const v14, 0x7f0b00b0

    invoke-virtual {v13, v14}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 218
    .local v4, "discovering":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 220
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 222
    .local v8, "itemStr":Ljava/lang/String;
    iget-boolean v13, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->isCancelled:Z

    if-eqz v13, :cond_2

    .line 308
    .end local v8    # "itemStr":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object v9

    .line 228
    .restart local v8    # "itemStr":Ljava/lang/String;
    :cond_2
    invoke-static {v8}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getItem(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPItem;

    move-result-object v10

    .line 230
    .local v10, "rootItem":Lcom/sec/android/app/myfiles/ftp/FTPItem;
    if-eqz v10, :cond_a

    .line 232
    iget-object v13, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v13, v14}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncFileExists(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_9

    .line 234
    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->isDirectory()Z

    move-result v13

    if-nez v13, :cond_3

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->isSymbolicLink()Z

    move-result v13

    if-eqz v13, :cond_7

    .line 236
    :cond_3
    iget-object v13, v9, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mJobFolder:Ljava/util/ArrayDeque;

    invoke-virtual {v13, v10}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    .line 238
    iget-object v13, v9, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mFolders:Ljava/util/Stack;

    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-static {v14, v0, v1}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->getTargetPath(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 264
    :cond_4
    :goto_1
    iget-boolean v13, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->isCancelled:Z

    if-nez v13, :cond_0

    iget-object v13, v9, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mJobFolder:Ljava/util/ArrayDeque;

    invoke-virtual {v13}, Ljava/util/ArrayDeque;->size()I

    move-result v13

    if-lez v13, :cond_0

    .line 266
    iget-object v13, v9, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mJobFolder:Ljava/util/ArrayDeque;

    invoke-virtual {v13}, Ljava/util/ArrayDeque;->pop()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .line 268
    .local v7, "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    invoke-interface {v7}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-static {v13, v0, v1}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->getTargetPath(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 270
    .local v12, "target":Ljava/lang/String;
    invoke-static {v12}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->makeLocalDirectory(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 272
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-interface {v7}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getTitle()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {p0, v13}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->setLabel(Ljava/lang/String;)V

    .line 274
    iget-object v13, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v13, v7}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncBrowseForItemContent(Lcom/sec/android/app/myfiles/ftp/IFTPItem;)Ljava/util/List;

    move-result-object v3

    .line 276
    .local v3, "children":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    if-eqz v3, :cond_4

    .line 278
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .line 280
    .local v2, "child":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    const/4 v13, 0x0

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->setLength(I)V

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v13, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v2}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getTitle()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    invoke-interface {v2}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isDirectory()Z

    move-result v13

    if-nez v13, :cond_6

    invoke-interface {v2}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isSymbolicLink()Z

    move-result v13

    if-eqz v13, :cond_b

    .line 284
    :cond_6
    iget-object v13, v9, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mJobFolder:Ljava/util/ArrayDeque;

    invoke-virtual {v13, v2}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 286
    iget-object v13, v9, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mFolders:Ljava/util/Stack;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 240
    .end local v2    # "child":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .end local v3    # "children":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v7    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .end local v12    # "target":Ljava/lang/String;
    :cond_7
    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->isFile()Z

    move-result v13

    if-eqz v13, :cond_4

    .line 242
    iget-object v13, v9, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mJobFile:Ljava/util/ArrayDeque;

    invoke-virtual {v13, v10}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 244
    iget-object v14, v9, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mFiles:Ljava/util/Stack;

    if-eqz p4, :cond_8

    move-object/from16 v13, p2

    :goto_3
    invoke-virtual {v14, v13}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    :cond_8
    invoke-virtual {v10}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-static {v13, v0, v1}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->getTargetPath(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    goto :goto_3

    .line 250
    :cond_9
    sget-object v13, Lcom/sec/android/app/myfiles/ftp/FTPErr;->PARTIALLY_DONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    iput-object v13, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    goto/16 :goto_0

    .line 258
    :cond_a
    sget-object v13, Lcom/sec/android/app/myfiles/ftp/FTPErr;->PARTIALLY_DONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    iput-object v13, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    goto/16 :goto_0

    .line 288
    .restart local v2    # "child":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .restart local v3    # "children":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    .restart local v6    # "i$":Ljava/util/Iterator;
    .restart local v7    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .restart local v12    # "target":Ljava/lang/String;
    :cond_b
    invoke-interface {v2}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->isFile()Z

    move-result v13

    if-eqz v13, :cond_5

    .line 290
    iget-object v13, v9, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mJobFile:Ljava/util/ArrayDeque;

    invoke-virtual {v13, v2}, Ljava/util/ArrayDeque;->push(Ljava/lang/Object;)V

    .line 292
    iget-object v13, v9, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FTPDiscoveryResult;->mFiles:Ljava/util/Stack;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2
.end method

.method protected downloadItems(Ljava/util/ArrayDeque;Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p2, "targetFolder"    # Ljava/lang/String;
    .param p3, "srcParentDir"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayDeque",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    .prologue
    .line 554
    .local p1, "jobFile":Ljava/util/ArrayDeque;, "Ljava/util/ArrayDeque<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->downloadItems(Ljava/util/ArrayDeque;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    return v0
.end method

.method protected downloadItems(Ljava/util/ArrayDeque;Ljava/lang/String;Ljava/lang/String;Z)I
    .locals 8
    .param p2, "targetFolder"    # Ljava/lang/String;
    .param p3, "srcParentDir"    # Ljava/lang/String;
    .param p4, "directDownload"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayDeque",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/IFTPItem;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z)I"
        }
    .end annotation

    .prologue
    .line 560
    .local p1, "jobFile":Ljava/util/ArrayDeque;, "Ljava/util/ArrayDeque<Lcom/sec/android/app/myfiles/ftp/IFTPItem;>;"
    const/4 v3, 0x0

    .line 562
    .local v3, "succeeded":I
    invoke-virtual {p1}, Ljava/util/ArrayDeque;->size()I

    move-result v4

    .line 564
    .local v4, "total":I
    const/4 v1, 0x0

    .line 566
    .local v1, "i":I
    :cond_0
    :goto_0
    iget-boolean v5, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->isCancelled:Z

    if-nez v5, :cond_2

    invoke-virtual {p1}, Ljava/util/ArrayDeque;->size()I

    move-result v5

    if-lez v5, :cond_2

    .line 568
    add-int/lit8 v1, v1, 0x1

    move v0, v1

    .line 570
    .local v0, "current":I
    invoke-virtual {p1}, Ljava/util/ArrayDeque;->pop()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .line 572
    .local v2, "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    invoke-interface {v2}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->setLabel(Ljava/lang/String;)V

    .line 574
    iget-object v6, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    if-eqz p4, :cond_1

    move-object v5, p2

    :goto_1
    new-instance v7, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$2;

    invoke-direct {v7, p0, v0, v4}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$2;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;II)V

    invoke-interface {v6, v2, v5, v7}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncDownloadFileToLocal(Lcom/sec/android/app/myfiles/ftp/IFTPItem;Ljava/lang/String;Lcom/sec/android/app/myfiles/openoperation/IProgressUpdatable;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 587
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 574
    :cond_1
    invoke-interface {v2}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, p2, p3}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->getTargetPath(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 593
    .end local v0    # "current":I
    .end local v2    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    :cond_2
    return v3
.end method

.method protected sendScansToParents([Ljava/lang/String;)V
    .locals 6
    .param p1, "paths"    # [Ljava/lang/String;

    .prologue
    .line 737
    if-eqz p1, :cond_0

    .line 739
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 741
    .local v3, "path":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->getParentDir(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/myfiles/utils/Utils;->sendScan(Landroid/content/Context;Ljava/lang/String;)V

    .line 739
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 747
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "path":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method protected setLabel(Ljava/lang/String;)V
    .locals 0
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 90
    return-void
.end method

.method protected setupProgressView()V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 97
    return-void
.end method

.method protected updateProgressBar(III)V
    .locals 2
    .param p1, "progress"    # I
    .param p2, "current"    # I
    .param p3, "total"    # I

    .prologue
    .line 599
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 601
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 605
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mPercentText:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mCountText:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 607
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$3;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$3;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;III)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 623
    :cond_1
    return-void
.end method

.method protected uploadFiles(Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 10
    .param p1, "ftpHandler"    # Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    .param p2, "files"    # [Ljava/lang/String;
    .param p3, "targetDirectory"    # Ljava/lang/String;
    .param p4, "srcDirectory"    # Ljava/lang/String;

    .prologue
    .line 345
    if-eqz p2, :cond_3

    if-eqz p3, :cond_3

    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_3

    .line 347
    const/4 v3, 0x0

    .local v3, "i":I
    const/4 v6, 0x0

    .line 349
    .local v6, "succeeded":I
    array-length v7, p2

    .line 351
    .local v7, "total":I
    move-object v0, p2

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v2, v0, v4

    .line 353
    .local v2, "file":Ljava/lang/String;
    add-int/lit8 v3, v3, 0x1

    move v1, v3

    .line 355
    .local v1, "current":I
    iget-boolean v8, p0, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->isCancelled:Z

    if-eqz v8, :cond_1

    .line 386
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "current":I
    .end local v2    # "file":Ljava/lang/String;
    .end local v3    # "i":I
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .end local v6    # "succeeded":I
    .end local v7    # "total":I
    :cond_0
    :goto_1
    return v6

    .line 361
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v1    # "current":I
    .restart local v2    # "file":Ljava/lang/String;
    .restart local v3    # "i":I
    .restart local v4    # "i$":I
    .restart local v5    # "len$":I
    .restart local v6    # "succeeded":I
    .restart local v7    # "total":I
    :cond_1
    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->setLabel(Ljava/lang/String;)V

    .line 363
    invoke-static {v2, p3, p4}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->getTarget(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$1;

    invoke-direct {v9, p0, v1, v7}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$1;-><init>(Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;II)V

    invoke-interface {p1, v2, v8, v9}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncUploadFileFromLocal(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/app/myfiles/openoperation/IProgressUpdatable;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 376
    add-int/lit8 v6, v6, 0x1

    .line 351
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 386
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "current":I
    .end local v2    # "file":Ljava/lang/String;
    .end local v3    # "i":I
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .end local v6    # "succeeded":I
    .end local v7    # "total":I
    :cond_3
    const/4 v6, 0x0

    goto :goto_1
.end method
