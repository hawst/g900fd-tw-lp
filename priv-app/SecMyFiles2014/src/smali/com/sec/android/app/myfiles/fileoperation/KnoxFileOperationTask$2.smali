.class Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$2;
.super Landroid/content/BroadcastReceiver;
.source "KnoxFileOperationTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;)V
    .locals 0

    .prologue
    .line 468
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v5, 0x64

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 473
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 475
    .local v0, "action":Ljava/lang/String;
    const-string v6, "PACKAGENAME"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 477
    .local v2, "packageName":Ljava/lang/String;
    const-string v6, "SUCCESSCNT"

    invoke-virtual {p2, v6, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 479
    .local v4, "successCnt":I
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    const-string v7, "ERRORCODE"

    invoke-virtual {p2, v7, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    # setter for: Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mErrorCode:I
    invoke-static {v6, v7}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->access$102(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;I)I

    .line 481
    const-string v6, "PROGRESS"

    invoke-virtual {p2, v6, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 484
    .local v1, "curPercentage":I
    const-string v6, "KnoxFileOperationTask"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " onReceive() - "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " packageName "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v9, v6, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 486
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 488
    const-string v6, "com.sec.knox.container.FileRelayDone"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 490
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget v6, v5, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTarget:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v5, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTarget:I

    .line 492
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    new-array v6, v10, [Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    aput-object v7, v6, v9

    # invokes: Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->publishProgress([Ljava/lang/Object;)V
    invoke-static {v5, v6}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->access$200(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;[Ljava/lang/Object;)V

    .line 494
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget v5, v5, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mDoneTarget:I

    mul-int/lit8 v5, v5, 0x64

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    div-int v3, v5, v6

    .line 496
    .local v3, "progressRate":I
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v5, v3}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 498
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    # setter for: Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mFileRelayDone:Z
    invoke-static {v5, v10}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->access$302(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;Z)Z

    .line 529
    .end local v1    # "curPercentage":I
    .end local v3    # "progressRate":I
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mWaitKnoxReply:Ljava/lang/Runnable;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->access$800(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;)Ljava/lang/Runnable;

    move-result-object v6

    monitor-enter v6

    .line 531
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    const/4 v7, 0x0

    # setter for: Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mWaitKnowReplyFlag:Z
    invoke-static {v5, v7}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->access$902(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;Z)Z

    .line 533
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mWaitKnoxReply:Ljava/lang/Runnable;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->access$800(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;)Ljava/lang/Runnable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->notify()V

    .line 535
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 541
    :goto_1
    return-void

    .line 501
    .restart local v1    # "curPercentage":I
    :cond_1
    const-string v6, "com.sec.knox.container.FileRelayFail"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 503
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    # setter for: Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mFileRelayError:Z
    invoke-static {v5, v10}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->access$402(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;Z)Z

    .line 505
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    # operator++ for: Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mFailedCount:I
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->access$508(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;)I

    .line 507
    const-string v5, "KnoxFileOperationTask"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " file operation failed - mErrorCode : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mErrorCode:I
    invoke-static {v7}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->access$100(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v9, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 509
    :cond_2
    const-string v6, "com.sec.knox.container.FileRelayComplete"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 511
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    # setter for: Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mFileRelayComplete:Z
    invoke-static {v5, v10}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->access$602(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;Z)Z

    goto :goto_0

    .line 513
    :cond_3
    const-string v6, "com.sec.knox.container.FileRelayProgress"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 515
    const-string v6, "KnoxFileOperationTask"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ACTION_KNOX_FILE_RELAY_PROGRESS curPercentage : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v9, v6, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 517
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    if-lt v1, v5, :cond_4

    move v1, v5

    .end local v1    # "curPercentage":I
    :cond_4
    iput v1, v6, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mCurrentTargetPercentage:I

    .line 519
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget v5, v5, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mTotalTarget:I

    if-ne v5, v10, :cond_0

    .line 521
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    new-array v6, v10, [Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    aput-object v7, v6, v9

    # invokes: Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->publishProgress([Ljava/lang/Object;)V
    invoke-static {v5, v6}, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->access$700(Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;[Ljava/lang/Object;)V

    .line 523
    iget-object v5, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressBar:Landroid/widget/ProgressBar;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/fileoperation/KnoxFileOperationTask;->mProgressParam:Lcom/sec/android/app/myfiles/element/FileOperationProgress;

    iget v6, v6, Lcom/sec/android/app/myfiles/element/FileOperationProgress;->mCurrentTargetPercentage:I

    invoke-virtual {v5, v6}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto/16 :goto_0

    .line 535
    :catchall_0
    move-exception v5

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5

    .line 539
    .restart local v1    # "curPercentage":I
    :cond_5
    const-string v5, "KnoxFileOperationTask"

    const-string v6, " it\'s not incomming intent to MyFiles"

    invoke-static {v9, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method
