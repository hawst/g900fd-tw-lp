.class Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;
.super Ljava/lang/Object;
.source "RecentlyUsedAdapter.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

.field final synthetic val$filePosition:I

.field final synthetic val$fileType:I

.field final synthetic val$hoverFilePath:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;ILjava/lang/String;I)V
    .locals 0

    .prologue
    .line 212
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    iput p2, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->val$fileType:I

    iput-object p3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->val$hoverFilePath:Ljava/lang/String;

    iput p4, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->val$filePosition:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x1

    const/16 v10, 0x201

    const/4 v9, 0x2

    const/4 v5, 0x0

    .line 217
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->isSelectMode()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3, p2}, Lcom/sec/android/app/myfiles/utils/Utils;->isHoverSettingDisabled(Landroid/content/Context;Landroid/view/MotionEvent;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mVideoHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->access$100(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mAudioHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->access$200(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mDocumentHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->access$300(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 220
    const/4 v0, 0x0

    .line 222
    .local v0, "bSupportHover":Z
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 223
    .local v2, "hoverRect":Landroid/graphics/Rect;
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    check-cast v3, Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 225
    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    iget v7, v2, Landroid/graphics/Rect;->left:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v8, v3

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    iget v3, v3, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mViewMode:I

    if-ne v3, v9, :cond_1

    move v3, v4

    :goto_0
    invoke-virtual {v6, v7, v8, v3}, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->setHoverLeft(IIZ)I

    move-result v3

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 226
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    iget v4, v2, Landroid/graphics/Rect;->top:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {v3, v4, v6}, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->setHoverTop(II)I

    move-result v3

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 228
    iget v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->val$fileType:I

    invoke-static {v3}, Lcom/sec/android/app/myfiles/MediaFile;->isVideoFileType(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 230
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/myfiles/utils/Utils;->isOnCall(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 231
    const/4 v0, 0x0

    .line 265
    :goto_1
    if-eqz v0, :cond_0

    .line 267
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 354
    .end local v0    # "bSupportHover":Z
    .end local v2    # "hoverRect":Landroid/graphics/Rect;
    :cond_0
    :goto_2
    :pswitch_0
    return v5

    .restart local v0    # "bSupportHover":Z
    .restart local v2    # "hoverRect":Landroid/graphics/Rect;
    :cond_1
    move v3, v5

    .line 225
    goto :goto_0

    .line 233
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mVideoHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->access$100(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 234
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mVideoHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->access$100(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->val$hoverFilePath:Ljava/lang/String;

    iget v6, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->val$filePosition:I

    invoke-virtual {v3, v10, v2, v4, v6}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->setParam(ILandroid/graphics/Rect;Ljava/lang/String;I)V

    .line 235
    const/4 v0, 0x1

    goto :goto_1

    .line 238
    :cond_3
    iget v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->val$fileType:I

    invoke-static {v3}, Lcom/sec/android/app/myfiles/MediaFile;->isAudioFileType(I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 240
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mAudioHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->access$200(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 241
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mAudioHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->access$200(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->val$hoverFilePath:Ljava/lang/String;

    iget v6, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->val$filePosition:I

    invoke-virtual {v3, v10, v2, v4, v6}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->setParam(ILandroid/graphics/Rect;Ljava/lang/String;I)V

    .line 242
    const/4 v0, 0x1

    goto :goto_1

    .line 244
    :cond_4
    iget v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->val$fileType:I

    invoke-static {v3}, Lcom/sec/android/app/myfiles/MediaFile;->isImageFileType(I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 246
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 247
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->val$hoverFilePath:Ljava/lang/String;

    iget v6, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->val$filePosition:I

    invoke-virtual {v3, v10, v2, v4, v6}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->setParam(ILandroid/graphics/Rect;Ljava/lang/String;I)V

    .line 248
    const/4 v0, 0x1

    goto :goto_1

    .line 250
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    iget v3, v3, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mViewMode:I

    if-ne v3, v9, :cond_8

    iget v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->val$fileType:I

    invoke-static {v3}, Lcom/sec/android/app/myfiles/MediaFile;->isDocumentFileType(I)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 251
    iget v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->val$fileType:I

    const/16 v4, 0x4c

    if-eq v3, v4, :cond_6

    iget v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->val$fileType:I

    const/16 v4, 0x4b

    if-ne v3, v4, :cond_7

    .line 252
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 254
    :cond_7
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mDocumentHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->access$300(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 255
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mDocumentHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->access$300(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    move-result-object v3

    const/4 v4, 0x5

    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->val$hoverFilePath:Ljava/lang/String;

    iget v7, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->val$filePosition:I

    invoke-virtual {v3, v4, v2, v6, v7}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->setParam(ILandroid/graphics/Rect;Ljava/lang/String;I)V

    .line 256
    const/4 v0, 0x1

    goto/16 :goto_1

    .line 261
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 272
    :pswitch_1
    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v3

    if-ne v3, v9, :cond_9

    .line 274
    const/16 v3, 0xa

    const/4 v4, -0x1

    invoke-static {v3, v4}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 286
    :cond_9
    :goto_3
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    const/16 v4, 0x1f4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->sendDelayedMessage(I)V

    .line 289
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    # setter for: Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mLastHoverEnterEventTime:J
    invoke-static {v3, v6, v7}, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->access$402(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;J)J

    .line 290
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    invoke-static {p2}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v4

    # setter for: Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mLastMotionEvent:Landroid/view/MotionEvent;
    invoke-static {v3, v4}, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->access$502(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    .line 291
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    # setter for: Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mLastHoverEnteredView:Landroid/view/View;
    invoke-static {v3, p1}, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->access$602(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;Landroid/view/View;)Landroid/view/View;

    goto/16 :goto_2

    .line 278
    :catch_0
    move-exception v1

    .line 280
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_3

    .line 297
    .end local v1    # "e":Landroid/os/RemoteException;
    :pswitch_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->removeDelayedMessage()V

    .line 300
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v3

    if-ne v3, v9, :cond_a

    .line 302
    const/4 v3, 0x1

    const/4 v4, -0x1

    invoke-static {v3, v4}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 314
    :cond_a
    :goto_4
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mLastHoverEnterEventTime:J
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->access$400(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;)J

    move-result-wide v6

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v8

    cmp-long v3, v6, v8

    if-nez v3, :cond_b

    .line 315
    const-string v3, "RecentlyUsedAdapter"

    const-string v4, "resending hover enter event"

    invoke-static {v5, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 316
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mLastHoverEnteredView:Landroid/view/View;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->access$600(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;)Landroid/view/View;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mLastMotionEvent:Landroid/view/MotionEvent;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->access$500(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;)Landroid/view/MotionEvent;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    .line 320
    :try_start_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mLastMotionEvent:Landroid/view/MotionEvent;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->access$500(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;)Landroid/view/MotionEvent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/MotionEvent;->recycle()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2

    .line 328
    :goto_5
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    const-wide/16 v6, -0x1

    # setter for: Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mLastHoverEnterEventTime:J
    invoke-static {v3, v6, v7}, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->access$402(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;J)J

    goto/16 :goto_2

    .line 305
    :catch_1
    move-exception v1

    .line 307
    .restart local v1    # "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_4

    .line 322
    .end local v1    # "e":Landroid/os/RemoteException;
    :catch_2
    move-exception v1

    .line 325
    .local v1, "e":Ljava/lang/RuntimeException;
    const-string v3, "RecentlyUsedAdapter"

    invoke-virtual {v1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 329
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :cond_b
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mLastHoverEnterEventTime:J
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->access$400(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;)J

    move-result-wide v6

    const-wide/16 v8, -0x1

    cmp-long v3, v6, v8

    if-eqz v3, :cond_0

    .line 333
    :try_start_3
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->mLastMotionEvent:Landroid/view/MotionEvent;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;->access$500(Lcom/sec/android/app/myfiles/adapter/RecentlyUsedAdapter;)Landroid/view/MotionEvent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/MotionEvent;->recycle()V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_2

    .line 335
    :catch_3
    move-exception v1

    .line 338
    .restart local v1    # "e":Ljava/lang/RuntimeException;
    const-string v3, "RecentlyUsedAdapter"

    invoke-virtual {v1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 267
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
