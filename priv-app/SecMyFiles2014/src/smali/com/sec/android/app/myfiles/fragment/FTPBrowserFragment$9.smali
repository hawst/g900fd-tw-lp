.class Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$9;
.super Ljava/lang/Object;
.source "FTPBrowserFragment.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)V
    .locals 0

    .prologue
    .line 2064
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPathSelected(ILjava/lang/String;)V
    .locals 8
    .param p1, "targetFolderID"    # I
    .param p2, "targetFolderPath"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2067
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->isSelectMode()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSelector:Z
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->access$500(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2068
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->finishSelectMode()V

    .line 2070
    :cond_0
    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 2071
    .local v1, "items":[Ljava/lang/String;
    array-length v3, v1

    if-le v3, v7, :cond_4

    .line 2072
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2073
    .local v2, "path":Ljava/lang/StringBuilder;
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_1

    .line 2074
    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2075
    aget-object v3, v1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2073
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2077
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrefix:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    # setter for: Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPreviousPath:Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->access$002(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 2078
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    # setter for: Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSwitchOnBack:Z
    invoke-static {v3, v6}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->access$602(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;Z)Z

    .line 2079
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z

    .line 2080
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrefix:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    .line 2087
    .end local v0    # "i":I
    .end local v2    # "path":Ljava/lang/StringBuilder;
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getSelectionType()I

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->getSelectionType()I

    move-result v3

    if-ne v3, v7, :cond_3

    .line 2088
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->unselectAllItem()V

    .line 2090
    :cond_3
    return-void

    .line 2082
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrefix:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    # setter for: Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPreviousPath:Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->access$002(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 2083
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    # setter for: Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mSwitchOnBack:Z
    invoke-static {v3, v6}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->access$602(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;Z)Z

    .line 2084
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->goToRoot(Ljava/lang/Boolean;)Z

    .line 2085
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPathIndicator:Lcom/sec/android/app/myfiles/view/PathIndicator;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment$9;->this$0:Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->mPrefix:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/FTPBrowserFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/myfiles/view/PathIndicator;->setPath(Ljava/lang/String;)V

    goto :goto_1
.end method
