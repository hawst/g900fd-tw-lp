.class Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$6;
.super Ljava/lang/Object;
.source "DownloadedAppFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->onPrepareDialog(ILandroid/app/Dialog;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

.field final synthetic val$checkDoNotShow:Landroid/widget/CheckBox;

.field final synthetic val$dialogId:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;ILandroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 633
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    iput p2, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$6;->val$dialogId:I

    iput-object p3, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$6;->val$checkDoNotShow:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 638
    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$6;->val$dialogId:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$6;->val$dialogId:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 640
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$6;->val$checkDoNotShow:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 641
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->bDoNotCheckDropBox:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$202(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;Z)Z

    .line 646
    :cond_1
    :goto_0
    return-void

    .line 643
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$6;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->bDoNotCheckDropBox:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$202(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;Z)Z

    goto :goto_0
.end method
