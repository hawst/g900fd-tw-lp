.class Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$7;
.super Ljava/lang/Object;
.source "AddFTPFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;)V
    .locals 0

    .prologue
    .line 468
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1
    .param p1, "arg0"    # Landroid/text/Editable;

    .prologue
    .line 471
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->validate()V

    .line 472
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 476
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .param p1, "str"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 482
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    # setter for: Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->mStrLen:I
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;->access$702(Lcom/sec/android/app/myfiles/fragment/AddFTPFragment;I)I

    .line 483
    return-void
.end method
