.class Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$1;
.super Landroid/database/ContentObserver;
.source "RemoteShareNavigation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;-><init>(Landroid/content/Context;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 135
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 4
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 138
    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    .line 139
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    # getter for: Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mMessageFolder:I
    invoke-static {v1}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->access$000(Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;)I

    move-result v1

    const/16 v2, 0x26

    if-ne v1, v2, :cond_4

    sget-object v0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->INBOX_CONTENT_URI:Landroid/net/Uri;

    .line 140
    .local v0, "currentUri":Landroid/net/Uri;
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mCurrentFolderPath:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 141
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mCurrentFolderPath:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 144
    :cond_0
    const-string v1, "Remote Share ContentObserver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Uri : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " has been changed; Current: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    # getter for: Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->mIsSyncing:Z
    invoke-static {v1}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->access$100(Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0, p2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "content"

    invoke-static {p2, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 147
    :cond_2
    const-string v1, "Remote Share ContentObserver.onChange()"

    const-string v2, "Refreshing navigation"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    iget-object v1, p0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->refreshNavigation()V

    .line 150
    :cond_3
    return-void

    .line 139
    .end local v0    # "currentUri":Landroid/net/Uri;
    :cond_4
    sget-object v0, Lcom/sec/android/app/myfiles/navigation/RemoteShareNavigation;->OUTBOX_CONTENT_URI:Landroid/net/Uri;

    goto :goto_0
.end method
