.class public Lcom/sec/android/app/myfiles/adapter/UserShortcutAdapter;
.super Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
.source "UserShortcutAdapter.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "c"    # Landroid/database/Cursor;
    .param p4, "flags"    # I
    .param p5, "navigation"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .prologue
    .line 35
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 36
    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/16 v5, 0x8

    .line 45
    invoke-super {p0, p1, p2, p3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 46
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;

    .line 47
    .local v2, "vh":Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;
    if-eqz v2, :cond_1

    .line 48
    iget-object v3, v2, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectRadioButton:Landroid/widget/RadioButton;

    invoke-virtual {v3, v5}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 49
    iget-object v3, v2, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSelectCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v3, v5}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 51
    const-string v3, "title"

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 52
    .local v0, "shortcutTitle":Ljava/lang/String;
    const-string v3, "type"

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 53
    .local v1, "shortcutType":I
    iget-object v3, v2, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    const/16 v3, 0x1a

    if-ne v1, v3, :cond_3

    .line 56
    iget v3, p0, Lcom/sec/android/app/myfiles/adapter/UserShortcutAdapter;->mViewMode:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 57
    iget-object v3, v2, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const v4, 0x7f0200b1

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 69
    :cond_0
    :goto_0
    iget v3, p0, Lcom/sec/android/app/myfiles/adapter/UserShortcutAdapter;->mViewMode:I

    packed-switch v3, :pswitch_data_0

    .line 78
    .end local v0    # "shortcutTitle":Ljava/lang/String;
    .end local v1    # "shortcutType":I
    :cond_1
    :goto_1
    return-void

    .line 59
    .restart local v0    # "shortcutTitle":Ljava/lang/String;
    .restart local v1    # "shortcutType":I
    :cond_2
    iget-object v3, v2, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const v4, 0x7f0200af

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 61
    :cond_3
    const/16 v3, 0x1c

    if-ne v1, v3, :cond_4

    .line 62
    iget-object v3, v2, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const v4, 0x7f020051

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 63
    :cond_4
    if-ne v1, v5, :cond_5

    .line 64
    iget-object v3, v2, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const v4, 0x7f020050

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 65
    :cond_5
    const/16 v3, 0x1f

    if-ne v1, v3, :cond_0

    .line 66
    iget-object v3, v2, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const v4, 0x7f02004f

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 71
    :pswitch_0
    iget-object v3, v2, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 74
    :pswitch_1
    iget-object v3, v2, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDetailContainer:Landroid/view/ViewGroup;

    if-eqz v3, :cond_1

    .line 75
    iget-object v3, v2, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDetailContainer:Landroid/view/ViewGroup;

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1

    .line 69
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected getContentType()I
    .locals 1

    .prologue
    .line 40
    const/16 v0, 0x23

    return v0
.end method
