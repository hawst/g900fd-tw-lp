.class public final Lcom/sec/android/app/myfiles/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final action_bar_browser_select_mode:I = 0x7f040000

.field public static final action_bar_category_home_title:I = 0x7f040001

.field public static final action_bar_category_home_title_up:I = 0x7f040002

.field public static final action_bar_category_select_mode:I = 0x7f040003

.field public static final action_bar_common:I = 0x7f040004

.field public static final action_bar_device_select:I = 0x7f040005

.field public static final action_bar_search:I = 0x7f040006

.field public static final action_bar_search_easymode:I = 0x7f040007

.field public static final action_bar_selector:I = 0x7f040008

.field public static final action_bar_selector_multiple:I = 0x7f040009

.field public static final action_mode_search:I = 0x7f04000a

.field public static final action_mode_search_easyux:I = 0x7f04000b

.field public static final add_ftp:I = 0x7f04000c

.field public static final add_ftps:I = 0x7f04000d

.field public static final add_sftp:I = 0x7f04000e

.field public static final browser_grid_item:I = 0x7f04000f

.field public static final browser_grid_item_remote_share:I = 0x7f040010

.field public static final browser_layout:I = 0x7f040011

.field public static final browser_layout_slow:I = 0x7f040012

.field public static final browser_list_drag_item:I = 0x7f040013

.field public static final browser_list_drag_with_thumbs_item:I = 0x7f040014

.field public static final browser_list_item:I = 0x7f040015

.field public static final browser_list_item_remote_share:I = 0x7f040016

.field public static final category_home_grid_item:I = 0x7f040017

.field public static final category_home_grid_item_easymode:I = 0x7f040018

.field public static final category_home_grid_item_multiwindow:I = 0x7f040019

.field public static final category_home_layout:I = 0x7f04001a

.field public static final category_home_layout_easyux:I = 0x7f04001b

.field public static final category_home_layout_multiwindow:I = 0x7f04001c

.field public static final category_home_shortcut:I = 0x7f04001d

.field public static final category_home_shortcut_grid_item:I = 0x7f04001e

.field public static final category_home_shortcut_multiwindow:I = 0x7f04001f

.field public static final change_lock_password:I = 0x7f040020

.field public static final confirm_oma_layout:I = 0x7f040021

.field public static final create_folder_layout:I = 0x7f040022

.field public static final custom_dialog_lock:I = 0x7f040023

.field public static final detail_view:I = 0x7f040024

.field public static final downloadapp_hover_popup:I = 0x7f040025

.field public static final downloaded_apps_list_item:I = 0x7f040026

.field public static final empty_view:I = 0x7f040027

.field public static final folder_hover_body:I = 0x7f040028

.field public static final folder_hover_no_item:I = 0x7f040029

.field public static final folder_hover_popup:I = 0x7f04002a

.field public static final ftp_detail_view:I = 0x7f04002b

.field public static final hover_operations_include_layout:I = 0x7f04002c

.field public static final image_hover_popup:I = 0x7f04002d

.field public static final knox_container_item:I = 0x7f04002e

.field public static final knox_gridview:I = 0x7f04002f

.field public static final main:I = 0x7f040030

.field public static final main_list_item:I = 0x7f040031

.field public static final mobile_data_warning_dialog:I = 0x7f040032

.field public static final mobile_data_warning_dialog_nearbydevice:I = 0x7f040033

.field public static final music_hover_no_albumart:I = 0x7f040034

.field public static final music_hover_popup:I = 0x7f040035

.field public static final need_wifi_dialog:I = 0x7f040036

.field public static final no_network_dialog:I = 0x7f040037

.field public static final open_file_layout:I = 0x7f040038

.field public static final operation_progress_dialog:I = 0x7f040039

.field public static final overwrite_checkdialog:I = 0x7f04003a

.field public static final path_indicator_button:I = 0x7f04003b

.field public static final search_advanced_options_layout:I = 0x7f04003c

.field public static final search_browser_list_item:I = 0x7f04003d

.field public static final search_file_type_list_row:I = 0x7f04003e

.field public static final search_layout:I = 0x7f04003f

.field public static final search_layout_easyux:I = 0x7f040040

.field public static final search_location_list_row:I = 0x7f040041

.field public static final search_set_date_layout:I = 0x7f040042

.field public static final search_spinner:I = 0x7f040043

.field public static final select_all_popup_item:I = 0x7f040044

.field public static final select_all_popup_menu:I = 0x7f040045

.field public static final selector:I = 0x7f040046

.field public static final shortcut_list_item:I = 0x7f040047

.field public static final shortcut_popup_item:I = 0x7f040048

.field public static final sort_by_layout:I = 0x7f040049

.field public static final storage_usage:I = 0x7f04004a

.field public static final storage_usage_layout:I = 0x7f04004b

.field public static final storage_usage_layout_multiwindow:I = 0x7f04004c

.field public static final storage_usage_multiwindow:I = 0x7f04004d

.field public static final tree_list_item:I = 0x7f04004e

.field public static final video_hover_popup:I = 0x7f04004f

.field public static final wifi_only_checkbox_layout:I = 0x7f040050


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1880
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
