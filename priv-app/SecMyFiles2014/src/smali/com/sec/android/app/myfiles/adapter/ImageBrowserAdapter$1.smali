.class Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter$1;
.super Ljava/lang/Object;
.source "ImageBrowserAdapter.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;

.field final synthetic val$filePosition:I

.field final synthetic val$fileType:I

.field final synthetic val$hoverFilePath:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;ILjava/lang/String;I)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;

    iput p2, p0, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter$1;->val$fileType:I

    iput-object p3, p0, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter$1;->val$hoverFilePath:Ljava/lang/String;

    iput p4, p0, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter$1;->val$filePosition:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v9, 0x2

    .line 133
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;->isSelectMode()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3, p2}, Lcom/sec/android/app/myfiles/utils/Utils;->isHoverSettingDisabled(Landroid/content/Context;Landroid/view/MotionEvent;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    if-eqz v3, :cond_0

    .line 136
    const/4 v0, 0x0

    .line 138
    .local v0, "bSupportHover":Z
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 139
    .local v2, "hoverRect":Landroid/graphics/Rect;
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    check-cast v3, Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 141
    iget-object v6, p0, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;

    iget v7, v2, Landroid/graphics/Rect;->left:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v8, v3

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;

    iget v3, v3, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;->mViewMode:I

    if-ne v3, v9, :cond_1

    move v3, v4

    :goto_0
    invoke-virtual {v6, v7, v8, v3}, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;->setHoverLeft(IIZ)I

    move-result v3

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 142
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;

    iget v5, v2, Landroid/graphics/Rect;->top:I

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {v3, v5, v6}, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;->setHoverTop(II)I

    move-result v3

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 144
    iget v3, p0, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter$1;->val$fileType:I

    invoke-static {v3}, Lcom/sec/android/app/myfiles/MediaFile;->isImageFileType(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 146
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter$1;->val$hoverFilePath:Ljava/lang/String;

    iget v6, p0, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter$1;->val$filePosition:I

    invoke-virtual {v3, v9, v2, v5, v6}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->setParam(ILandroid/graphics/Rect;Ljava/lang/String;I)V

    .line 147
    const/4 v0, 0x1

    .line 155
    :goto_1
    if-eqz v0, :cond_0

    .line 157
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 207
    .end local v0    # "bSupportHover":Z
    .end local v2    # "hoverRect":Landroid/graphics/Rect;
    :cond_0
    :goto_2
    :pswitch_0
    return v4

    .restart local v0    # "bSupportHover":Z
    .restart local v2    # "hoverRect":Landroid/graphics/Rect;
    :cond_1
    move v3, v5

    .line 141
    goto :goto_0

    .line 151
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 160
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;

    # setter for: Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;->previousListener:Landroid/view/View$OnHoverListener;
    invoke-static {v3, p0}, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;->access$002(Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;Landroid/view/View$OnHoverListener;)Landroid/view/View$OnHoverListener;

    .line 163
    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v3

    if-ne v3, v9, :cond_3

    .line 165
    const/16 v3, 0xa

    const/4 v5, -0x1

    invoke-static {v3, v5}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    :cond_3
    :goto_3
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    const/16 v5, 0x1f4

    invoke-virtual {v3, v5}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->sendDelayedMessage(I)V

    goto :goto_2

    .line 168
    :catch_0
    move-exception v1

    .line 170
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_3

    .line 179
    .end local v1    # "e":Landroid/os/RemoteException;
    :pswitch_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;->previousListener:Landroid/view/View$OnHoverListener;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;->access$000(Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;)Landroid/view/View$OnHoverListener;

    move-result-object v3

    if-ne v3, p0, :cond_0

    .line 182
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v3

    if-ne v3, v9, :cond_4

    .line 184
    const/4 v3, 0x1

    const/4 v5, -0x1

    invoke-static {v3, v5}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 192
    :cond_4
    :goto_4
    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter$1;->this$0:Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/adapter/ImageBrowserAdapter;->mHoverMgr:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/hover/AbsHoverManager;->removeDelayedMessage()V

    goto :goto_2

    .line 187
    :catch_1
    move-exception v1

    .line 189
    .restart local v1    # "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_4

    .line 157
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
