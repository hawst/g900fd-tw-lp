.class public Lcom/sec/android/app/myfiles/adapter/SearchAdapter;
.super Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;
.source "SearchAdapter.java"


# static fields
.field private static final HOVER_DETECT_MS_FOLDER:I = 0x12c

.field private static final SPACE_CHAR:C = ' '


# instance fields
.field private appEntryOfSearchApp:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

.field private mAdvancedSearchItemHeight:I

.field private mAudioHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

.field private mContentSubtitlePos:I

.field private mContext:Landroid/content/Context;

.field private mDocumentHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

.field private mDownloadedAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mFolderHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

.field private mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

.field private mIsNeedReplaceDropboxWithBaiduCloud:Z

.field private mQueryWordHighlightColorSpan:Landroid/text/style/ForegroundColorSpan;

.field private mSavedSearchKeyWord:Ljava/lang/String;

.field private mScreenState:I

.field private mSearchResultHighlightColor:I

.field private mSelectedItemBackground:I

.field private mVideoHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "c"    # Landroid/database/Cursor;
    .param p4, "flags"    # I
    .param p5, "navigation"    # Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    .prologue
    const/4 v3, 0x0

    .line 113
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;ILcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    .line 75
    iput-object v3, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->appEntryOfSearchApp:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .line 77
    const/4 v0, 0x2

    const/16 v1, 0x55

    const/16 v2, 0x6a

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mSelectedItemBackground:I

    .line 82
    iput-object v3, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 84
    iput-object v3, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mVideoHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 86
    iput-object v3, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mAudioHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 88
    iput-object v3, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mFolderHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 90
    iput-object v3, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mDocumentHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 96
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContentSubtitlePos:I

    .line 115
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-static {v0, v1}, Landroid/util/GeneralUtil;->hasSystemFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    new-instance v0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 122
    new-instance v0, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/hover/VideoHoverPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mVideoHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 124
    new-instance v0, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/hover/MusicHoverPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mAudioHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 126
    new-instance v0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mFolderHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 128
    new-instance v0, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/myfiles/hover/DocumentHoverPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mDocumentHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090182

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mAdvancedSearchItemHeight:I

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080033

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mSearchResultHighlightColor:I

    .line 136
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    iget v1, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mSearchResultHighlightColor:I

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mQueryWordHighlightColorSpan:Landroid/text/style/ForegroundColorSpan;

    .line 138
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isNeedReplaceDropboxWithBaiduCloud()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mIsNeedReplaceDropboxWithBaiduCloud:Z

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->getScreenState(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mScreenState:I

    .line 141
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    .prologue
    .line 69
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mSelectedItemBackground:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mImageHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mVideoHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mAudioHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mFolderHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;)Lcom/sec/android/app/myfiles/hover/AbsHoverManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/adapter/SearchAdapter;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mDocumentHover:Lcom/sec/android/app/myfiles/hover/AbsHoverManager;

    return-object v0
.end method

.method private createListCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 36
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 1425
    new-instance v29, Landroid/database/MatrixCursor;

    sget-object v31, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mFilesApiProjectionWithDownloadedAppsProjection:[Ljava/lang/String;

    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 1427
    .local v29, "titleSearchCursor":Landroid/database/MatrixCursor;
    new-instance v22, Landroid/database/MatrixCursor;

    sget-object v31, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mFilesContentSearchApiProjection:[Ljava/lang/String;

    move-object/from16 v0, v22

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 1431
    .local v22, "contentSearchCursor":Landroid/database/MatrixCursor;
    if-eqz p1, :cond_2

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v31

    if-nez v31, :cond_2

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v31

    if-eqz v31, :cond_2

    .line 1433
    :cond_0
    const-string v31, "show_contentsearch_head"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v31

    const/16 v32, -0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_1

    const-string v31, "show_titlesearch_head"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v31

    const/16 v32, -0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-eq v0, v1, :cond_4

    .line 1489
    :cond_1
    :goto_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v31

    if-nez v31, :cond_0

    .line 1493
    :cond_2
    const/16 v18, 0x0

    .line 1494
    .local v18, "arrayCursor":[Landroid/database/Cursor;
    invoke-virtual/range {v29 .. v29}, Landroid/database/MatrixCursor;->getCount()I

    move-result v31

    if-lez v31, :cond_e

    .line 1496
    new-instance v30, Landroid/database/MatrixCursor;

    sget-object v31, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mFilesTitleSearchProjection:[Ljava/lang/String;

    invoke-direct/range {v30 .. v31}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 1498
    .local v30, "titleSearchHeading":Landroid/database/MatrixCursor;
    const/16 v31, 0x8

    move/from16 v0, v31

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v31, 0x0

    const-string v32, ""

    invoke-virtual/range {v32 .. v32}, Ljava/lang/String;->hashCode()I

    move-result v32

    invoke-static/range {v32 .. v32}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    aput-object v32, v19, v31

    const/16 v31, 0x1

    const-string v32, ""

    aput-object v32, v19, v31

    const/16 v31, 0x2

    const-string v32, ""

    aput-object v32, v19, v31

    const/16 v31, 0x3

    const-string v32, "0"

    aput-object v32, v19, v31

    const/16 v31, 0x4

    const-string v32, ""

    aput-object v32, v19, v31

    const/16 v31, 0x5

    const-string v32, ""

    aput-object v32, v19, v31

    const/16 v31, 0x6

    const-string v32, "true"

    aput-object v32, v19, v31

    const/16 v31, 0x7

    const-string v32, ""

    aput-object v32, v19, v31

    .line 1500
    .local v19, "column":[Ljava/lang/Object;
    move-object/from16 v0, v30

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 1502
    const/16 v31, 0x2

    move/from16 v0, v31

    new-array v0, v0, [Landroid/database/Cursor;

    move-object/from16 v25, v0

    .line 1503
    .local v25, "fileNameSearchArrayCursor":[Landroid/database/Cursor;
    const/16 v31, 0x0

    aput-object v30, v25, v31

    .line 1504
    const/16 v31, 0x1

    new-instance v32, Lcom/sec/android/app/myfiles/SortCursor;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mCurrentSortBy:I

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mCurrentInOrder:I

    move/from16 v34, v0

    const/16 v35, 0x1

    move-object/from16 v0, v32

    move-object/from16 v1, v29

    move/from16 v2, v33

    move/from16 v3, v34

    move/from16 v4, v35

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/myfiles/SortCursor;-><init>(Landroid/database/Cursor;IIZ)V

    aput-object v32, v25, v31

    .line 1505
    new-instance p1, Landroid/database/MergeCursor;

    .end local p1    # "cursor":Landroid/database/Cursor;
    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .line 1506
    .restart local p1    # "cursor":Landroid/database/Cursor;
    const/16 v31, 0x2

    move/from16 v0, v31

    new-array v0, v0, [Landroid/database/Cursor;

    move-object/from16 v18, v0

    .line 1507
    const/16 v31, 0x0

    aput-object p1, v18, v31

    .line 1511
    .end local v19    # "column":[Ljava/lang/Object;
    .end local v25    # "fileNameSearchArrayCursor":[Landroid/database/Cursor;
    .end local v30    # "titleSearchHeading":Landroid/database/MatrixCursor;
    :goto_1
    invoke-virtual/range {v22 .. v22}, Landroid/database/MatrixCursor;->getCount()I

    move-result v31

    if-lez v31, :cond_10

    .line 1513
    new-instance v23, Landroid/database/MatrixCursor;

    sget-object v31, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mFilesContentSearchProjection:[Ljava/lang/String;

    move-object/from16 v0, v23

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 1515
    .local v23, "contentSearchHeading":Landroid/database/MatrixCursor;
    const/16 v31, 0x8

    move/from16 v0, v31

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v31, 0x0

    const-string v32, ""

    invoke-virtual/range {v32 .. v32}, Ljava/lang/String;->hashCode()I

    move-result v32

    invoke-static/range {v32 .. v32}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v32

    aput-object v32, v19, v31

    const/16 v31, 0x1

    const-string v32, ""

    aput-object v32, v19, v31

    const/16 v31, 0x2

    const-string v32, ""

    aput-object v32, v19, v31

    const/16 v31, 0x3

    const-string v32, "0"

    aput-object v32, v19, v31

    const/16 v31, 0x4

    const-string v32, ""

    aput-object v32, v19, v31

    const/16 v31, 0x5

    const-string v32, ""

    aput-object v32, v19, v31

    const/16 v31, 0x6

    const-string v32, "true"

    aput-object v32, v19, v31

    const/16 v31, 0x7

    const-string v32, ""

    aput-object v32, v19, v31

    .line 1517
    .restart local v19    # "column":[Ljava/lang/Object;
    move-object/from16 v0, v23

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 1520
    const/16 v31, 0x2

    move/from16 v0, v31

    new-array v0, v0, [Landroid/database/Cursor;

    move-object/from16 v21, v0

    .line 1521
    .local v21, "contentSearchArrayCursor":[Landroid/database/Cursor;
    const/16 v31, 0x0

    aput-object v23, v21, v31

    .line 1522
    const/16 v31, 0x1

    new-instance v32, Lcom/sec/android/app/myfiles/SortCursor;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mCurrentSortBy:I

    move/from16 v33, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mCurrentInOrder:I

    move/from16 v34, v0

    const/16 v35, 0x1

    move-object/from16 v0, v32

    move-object/from16 v1, v22

    move/from16 v2, v33

    move/from16 v3, v34

    move/from16 v4, v35

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/myfiles/SortCursor;-><init>(Landroid/database/Cursor;IIZ)V

    aput-object v32, v21, v31

    .line 1523
    new-instance v28, Landroid/database/MergeCursor;

    move-object/from16 v0, v28

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .line 1524
    .local v28, "newContentCursor":Landroid/database/Cursor;
    if-eqz v18, :cond_f

    .line 1525
    const/16 v31, 0x1

    aput-object v28, v18, v31

    .line 1526
    new-instance p1, Landroid/database/MergeCursor;

    .end local p1    # "cursor":Landroid/database/Cursor;
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .line 1537
    .end local v19    # "column":[Ljava/lang/Object;
    .end local v21    # "contentSearchArrayCursor":[Landroid/database/Cursor;
    .end local v23    # "contentSearchHeading":Landroid/database/MatrixCursor;
    .end local v28    # "newContentCursor":Landroid/database/Cursor;
    .restart local p1    # "cursor":Landroid/database/Cursor;
    :goto_2
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/sec/android/app/myfiles/SortCursor;

    move/from16 v31, v0

    if-eqz v31, :cond_3

    .line 1538
    new-instance v24, Landroid/database/MergeCursor;

    const/16 v31, 0x1

    move/from16 v0, v31

    new-array v0, v0, [Landroid/database/Cursor;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    aput-object p1, v31, v32

    move-object/from16 v0, v24

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .end local p1    # "cursor":Landroid/database/Cursor;
    .local v24, "cursor":Landroid/database/Cursor;
    move-object/from16 p1, v24

    .line 1540
    .end local v24    # "cursor":Landroid/database/Cursor;
    .restart local p1    # "cursor":Landroid/database/Cursor;
    :cond_3
    return-object p1

    .line 1437
    .end local v18    # "arrayCursor":[Landroid/database/Cursor;
    :cond_4
    const-wide/16 v14, -0x1

    .line 1438
    .local v14, "ID":J
    const-string v6, ""

    .line 1439
    .local v6, "DATA":Ljava/lang/String;
    const-string v7, ""

    .line 1440
    .local v7, "TITLE":Ljava/lang/String;
    const-wide/16 v12, -0x1

    .line 1441
    .local v12, "FORMAT":J
    const-wide/16 v16, -0x1

    .line 1442
    .local v16, "SIZE":J
    const-wide/16 v8, -0x1

    .line 1443
    .local v8, "DATE_MODIFIED":J
    const-wide/16 v10, -0x1

    .line 1444
    .local v10, "DOWNLOADED_APP_INDEX_IF_EXIST":J
    const-string v20, ""

    .line 1445
    .local v20, "contentSearch":Ljava/lang/String;
    const/16 v27, 0x0

    .line 1447
    .local v27, "isConentSearchResult":Z
    const/16 v26, -0x1

    .line 1449
    .local v26, "index":I
    const-string v31, "_id"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    const/16 v31, -0x1

    move/from16 v0, v26

    move/from16 v1, v31

    if-eq v0, v1, :cond_5

    .line 1450
    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 1452
    :cond_5
    const-string v31, "_data"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    const/16 v31, -0x1

    move/from16 v0, v26

    move/from16 v1, v31

    if-eq v0, v1, :cond_6

    .line 1453
    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1455
    :cond_6
    const-string v31, "title"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    const/16 v31, -0x1

    move/from16 v0, v26

    move/from16 v1, v31

    if-eq v0, v1, :cond_7

    .line 1456
    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1458
    :cond_7
    const-string v31, "format"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    const/16 v31, -0x1

    move/from16 v0, v26

    move/from16 v1, v31

    if-eq v0, v1, :cond_8

    .line 1459
    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 1461
    :cond_8
    const-string v31, "_size"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    const/16 v31, -0x1

    move/from16 v0, v26

    move/from16 v1, v31

    if-eq v0, v1, :cond_9

    .line 1462
    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 1464
    :cond_9
    const-string v31, "date_modified"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    const/16 v31, -0x1

    move/from16 v0, v26

    move/from16 v1, v31

    if-eq v0, v1, :cond_a

    .line 1466
    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 1468
    :cond_a
    const-string v31, "index"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    const/16 v31, -0x1

    move/from16 v0, v26

    move/from16 v1, v31

    if-eq v0, v1, :cond_b

    .line 1469
    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 1471
    :cond_b
    const-string v31, "content_search"

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    const/16 v31, -0x1

    move/from16 v0, v26

    move/from16 v1, v31

    if-eq v0, v1, :cond_c

    .line 1473
    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 1474
    const/16 v27, 0x1

    .line 1476
    :cond_c
    if-eqz v27, :cond_d

    .line 1477
    const/16 v31, 0x7

    move/from16 v0, v31

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v33

    aput-object v33, v31, v32

    const/16 v32, 0x1

    aput-object v6, v31, v32

    const/16 v32, 0x2

    aput-object v7, v31, v32

    const/16 v32, 0x3

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v33

    aput-object v33, v31, v32

    const/16 v32, 0x4

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v33

    aput-object v33, v31, v32

    const/16 v32, 0x5

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v33

    aput-object v33, v31, v32

    const/16 v32, 0x6

    aput-object v20, v31, v32

    move-object/from16 v0, v22

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1482
    :cond_d
    const/16 v31, 0x7

    move/from16 v0, v31

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v33

    aput-object v33, v31, v32

    const/16 v32, 0x1

    aput-object v6, v31, v32

    const/16 v32, 0x2

    aput-object v7, v31, v32

    const/16 v32, 0x3

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v33

    aput-object v33, v31, v32

    const/16 v32, 0x4

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v33

    aput-object v33, v31, v32

    const/16 v32, 0x5

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v33

    aput-object v33, v31, v32

    const/16 v32, 0x6

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v33

    aput-object v33, v31, v32

    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1509
    .end local v6    # "DATA":Ljava/lang/String;
    .end local v7    # "TITLE":Ljava/lang/String;
    .end local v8    # "DATE_MODIFIED":J
    .end local v10    # "DOWNLOADED_APP_INDEX_IF_EXIST":J
    .end local v12    # "FORMAT":J
    .end local v14    # "ID":J
    .end local v16    # "SIZE":J
    .end local v20    # "contentSearch":Ljava/lang/String;
    .end local v26    # "index":I
    .end local v27    # "isConentSearchResult":Z
    .restart local v18    # "arrayCursor":[Landroid/database/Cursor;
    :cond_e
    invoke-virtual/range {v29 .. v29}, Landroid/database/MatrixCursor;->close()V

    goto/16 :goto_1

    .line 1528
    .restart local v19    # "column":[Ljava/lang/Object;
    .restart local v21    # "contentSearchArrayCursor":[Landroid/database/Cursor;
    .restart local v23    # "contentSearchHeading":Landroid/database/MatrixCursor;
    .restart local v28    # "newContentCursor":Landroid/database/Cursor;
    :cond_f
    move-object/from16 p1, v28

    goto/16 :goto_2

    .line 1531
    .end local v19    # "column":[Ljava/lang/Object;
    .end local v21    # "contentSearchArrayCursor":[Landroid/database/Cursor;
    .end local v23    # "contentSearchHeading":Landroid/database/MatrixCursor;
    .end local v28    # "newContentCursor":Landroid/database/Cursor;
    :cond_10
    invoke-virtual/range {v22 .. v22}, Landroid/database/MatrixCursor;->close()V

    goto/16 :goto_2
.end method

.method private createThumbnailCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 30
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 1319
    new-instance v24, Landroid/database/MatrixCursor;

    sget-object v25, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mFilesApiProjectionWithDownloadedAppsProjection:[Ljava/lang/String;

    invoke-direct/range {v24 .. v25}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 1321
    .local v24, "titleSearchCursor":Landroid/database/MatrixCursor;
    new-instance v20, Landroid/database/MatrixCursor;

    sget-object v25, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->mFilesContentSearchApiProjection:[Ljava/lang/String;

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 1325
    .local v20, "contentSearchCursor":Landroid/database/MatrixCursor;
    if-eqz p1, :cond_2

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v25

    if-nez v25, :cond_2

    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v25

    if-eqz v25, :cond_2

    .line 1327
    :cond_0
    const-string v25, "show_contentsearch_head"

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    const/16 v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_1

    const-string v25, "show_titlesearch_head"

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    const/16 v26, -0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-eq v0, v1, :cond_4

    .line 1384
    :cond_1
    :goto_0
    invoke-interface/range {p1 .. p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v25

    if-nez v25, :cond_0

    .line 1388
    :cond_2
    const/16 v18, 0x0

    .line 1389
    .local v18, "arrayCursor":[Landroid/database/Cursor;
    invoke-virtual/range {v24 .. v24}, Landroid/database/MatrixCursor;->getCount()I

    move-result v25

    if-lez v25, :cond_e

    .line 1390
    new-instance p1, Lcom/sec/android/app/myfiles/SortCursor;

    .end local p1    # "cursor":Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mCurrentSortBy:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mCurrentInOrder:I

    move/from16 v26, v0

    const/16 v27, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    move/from16 v4, v27

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/myfiles/SortCursor;-><init>(Landroid/database/Cursor;IIZ)V

    .line 1391
    .restart local p1    # "cursor":Landroid/database/Cursor;
    const/16 v25, 0x2

    move/from16 v0, v25

    new-array v0, v0, [Landroid/database/Cursor;

    move-object/from16 v18, v0

    .line 1392
    const/16 v25, 0x0

    aput-object p1, v18, v25

    .line 1396
    :goto_1
    invoke-virtual/range {v20 .. v20}, Landroid/database/MatrixCursor;->getCount()I

    move-result v25

    if-lez v25, :cond_10

    .line 1397
    if-eqz v18, :cond_f

    .line 1398
    const/16 v25, 0x1

    new-instance v26, Lcom/sec/android/app/myfiles/SortCursor;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mCurrentSortBy:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mCurrentInOrder:I

    move/from16 v28, v0

    const/16 v29, 0x1

    move-object/from16 v0, v26

    move-object/from16 v1, v20

    move/from16 v2, v27

    move/from16 v3, v28

    move/from16 v4, v29

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/myfiles/SortCursor;-><init>(Landroid/database/Cursor;IIZ)V

    aput-object v26, v18, v25

    .line 1399
    new-instance p1, Landroid/database/MergeCursor;

    .end local p1    # "cursor":Landroid/database/Cursor;
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .line 1411
    .restart local p1    # "cursor":Landroid/database/Cursor;
    :goto_2
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/sec/android/app/myfiles/SortCursor;

    move/from16 v25, v0

    if-eqz v25, :cond_3

    .line 1412
    new-instance v21, Landroid/database/MergeCursor;

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Landroid/database/Cursor;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    aput-object p1, v25, v26

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .end local p1    # "cursor":Landroid/database/Cursor;
    .local v21, "cursor":Landroid/database/Cursor;
    move-object/from16 p1, v21

    .line 1414
    .end local v21    # "cursor":Landroid/database/Cursor;
    .restart local p1    # "cursor":Landroid/database/Cursor;
    :cond_3
    return-object p1

    .line 1332
    .end local v18    # "arrayCursor":[Landroid/database/Cursor;
    :cond_4
    const-wide/16 v14, -0x1

    .line 1333
    .local v14, "ID":J
    const-string v6, ""

    .line 1334
    .local v6, "DATA":Ljava/lang/String;
    const-string v7, ""

    .line 1335
    .local v7, "TITLE":Ljava/lang/String;
    const-wide/16 v12, -0x1

    .line 1336
    .local v12, "FORMAT":J
    const-wide/16 v16, -0x1

    .line 1337
    .local v16, "SIZE":J
    const-wide/16 v8, -0x1

    .line 1338
    .local v8, "DATE_MODIFIED":J
    const-wide/16 v10, -0x1

    .line 1339
    .local v10, "DOWNLOADED_APP_INDEX_IF_EXIST":J
    const-string v19, ""

    .line 1340
    .local v19, "contentSearch":Ljava/lang/String;
    const/16 v23, 0x0

    .line 1342
    .local v23, "isConentSearchResult":Z
    const/16 v22, -0x1

    .line 1344
    .local v22, "index":I
    const-string v25, "_id"

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    const/16 v25, -0x1

    move/from16 v0, v22

    move/from16 v1, v25

    if-eq v0, v1, :cond_5

    .line 1345
    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 1347
    :cond_5
    const-string v25, "_data"

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    const/16 v25, -0x1

    move/from16 v0, v22

    move/from16 v1, v25

    if-eq v0, v1, :cond_6

    .line 1348
    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1350
    :cond_6
    const-string v25, "title"

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    const/16 v25, -0x1

    move/from16 v0, v22

    move/from16 v1, v25

    if-eq v0, v1, :cond_7

    .line 1351
    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1353
    :cond_7
    const-string v25, "format"

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    const/16 v25, -0x1

    move/from16 v0, v22

    move/from16 v1, v25

    if-eq v0, v1, :cond_8

    .line 1354
    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 1356
    :cond_8
    const-string v25, "_size"

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    const/16 v25, -0x1

    move/from16 v0, v22

    move/from16 v1, v25

    if-eq v0, v1, :cond_9

    .line 1357
    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 1359
    :cond_9
    const-string v25, "date_modified"

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    const/16 v25, -0x1

    move/from16 v0, v22

    move/from16 v1, v25

    if-eq v0, v1, :cond_a

    .line 1361
    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 1363
    :cond_a
    const-string v25, "index"

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    const/16 v25, -0x1

    move/from16 v0, v22

    move/from16 v1, v25

    if-eq v0, v1, :cond_b

    .line 1364
    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 1366
    :cond_b
    const-string v25, "content_search"

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    const/16 v25, -0x1

    move/from16 v0, v22

    move/from16 v1, v25

    if-eq v0, v1, :cond_c

    .line 1368
    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 1369
    const/16 v23, 0x1

    .line 1371
    :cond_c
    if-eqz v23, :cond_d

    .line 1372
    const/16 v25, 0x7

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x1

    aput-object v6, v25, v26

    const/16 v26, 0x2

    aput-object v7, v25, v26

    const/16 v26, 0x3

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x4

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x5

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x6

    aput-object v19, v25, v26

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1377
    :cond_d
    const/16 v25, 0x7

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x1

    aput-object v6, v25, v26

    const/16 v26, 0x2

    aput-object v7, v25, v26

    const/16 v26, 0x3

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x4

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x5

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x6

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-virtual/range {v24 .. v25}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1394
    .end local v6    # "DATA":Ljava/lang/String;
    .end local v7    # "TITLE":Ljava/lang/String;
    .end local v8    # "DATE_MODIFIED":J
    .end local v10    # "DOWNLOADED_APP_INDEX_IF_EXIST":J
    .end local v12    # "FORMAT":J
    .end local v14    # "ID":J
    .end local v16    # "SIZE":J
    .end local v19    # "contentSearch":Ljava/lang/String;
    .end local v22    # "index":I
    .end local v23    # "isConentSearchResult":Z
    .restart local v18    # "arrayCursor":[Landroid/database/Cursor;
    :cond_e
    invoke-virtual/range {v24 .. v24}, Landroid/database/MatrixCursor;->close()V

    goto/16 :goto_1

    .line 1401
    :cond_f
    new-instance p1, Lcom/sec/android/app/myfiles/SortCursor;

    .end local p1    # "cursor":Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mCurrentSortBy:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mCurrentInOrder:I

    move/from16 v26, v0

    const/16 v27, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move/from16 v2, v25

    move/from16 v3, v26

    move/from16 v4, v27

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/app/myfiles/SortCursor;-><init>(Landroid/database/Cursor;IIZ)V

    .restart local p1    # "cursor":Landroid/database/Cursor;
    goto/16 :goto_2

    .line 1404
    :cond_10
    invoke-virtual/range {v20 .. v20}, Landroid/database/MatrixCursor;->close()V

    goto/16 :goto_2
.end method

.method private disableOtherViews(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;Landroid/view/View;)V
    .locals 4
    .param p1, "vh"    # Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x2

    const/16 v2, 0x8

    .line 147
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mViewMode:I

    if-eq v0, v3, :cond_0

    .line 148
    iget-object v0, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    iget-object v0, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    :cond_0
    iget-object v0, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 153
    iget-object v0, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 154
    iget-object v0, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 156
    iget v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mViewMode:I

    if-eq v0, v3, :cond_1

    iget-object v0, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDivider:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 157
    iget-object v0, p1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDivider:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 160
    :cond_1
    invoke-virtual {p2, v2}, Landroid/view/View;->setMinimumHeight(I)V

    .line 161
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/view/View;->setClickable(Z)V

    .line 162
    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 41
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 183
    invoke-super/range {p0 .. p3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 185
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;

    .line 187
    .local v35, "vh":Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v29

    .line 189
    .local v29, "position":I
    const-string v7, "format"

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    .line 193
    .local v20, "format":I
    const-string v7, "category"

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 194
    .local v14, "categoryInd":I
    const/4 v7, -0x1

    if-eq v14, v7, :cond_2

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 196
    .local v8, "category":I
    :goto_0
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const/16 v36, 0x0

    move-object/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 198
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDivider:Landroid/widget/ImageView;

    if-eqz v7, :cond_0

    .line 200
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDivider:Landroid/widget/ImageView;

    const/16 v36, 0x8

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 204
    :cond_0
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mViewMode:I

    const/16 v36, 0x2

    move/from16 v0, v36

    if-ne v7, v0, :cond_1

    .line 206
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v36, 0x7f090034

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v0, v7

    move/from16 v26, v0

    .line 208
    .local v26, "itemHeight":I
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v28

    check-cast v28, Landroid/widget/RelativeLayout$LayoutParams;

    .line 210
    .local v28, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v36, 0x7f09003e

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v7, v7

    move-object/from16 v0, v28

    iput v7, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 212
    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/view/View;->setMinimumHeight(I)V

    .line 231
    .end local v26    # "itemHeight":I
    .end local v28    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    const-string v7, "show_titlesearch_head"

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v34

    .line 233
    .local v34, "titleSearchHeadIdx":I
    const-string v7, "show_contentsearch_head"

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 235
    .local v16, "contentSearchHeadIdx":I
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mViewMode:I

    const/16 v36, 0x2

    move/from16 v0, v36

    if-ne v7, v0, :cond_3

    const/4 v7, -0x1

    move/from16 v0, v34

    if-eq v0, v7, :cond_3

    const/4 v7, -0x1

    move/from16 v0, v16

    if-eq v0, v7, :cond_3

    .line 985
    :goto_1
    return-void

    .line 194
    .end local v8    # "category":I
    .end local v16    # "contentSearchHeadIdx":I
    .end local v34    # "titleSearchHeadIdx":I
    :cond_2
    const/4 v8, -0x1

    goto/16 :goto_0

    .line 240
    .restart local v8    # "category":I
    .restart local v16    # "contentSearchHeadIdx":I
    .restart local v34    # "titleSearchHeadIdx":I
    :cond_3
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v28

    .line 242
    .local v28, "lp":Landroid/view/ViewGroup$LayoutParams;
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mViewMode:I

    const/16 v36, 0x2

    move/from16 v0, v36

    if-eq v7, v0, :cond_6

    const/4 v7, -0x1

    move/from16 v0, v34

    if-eq v0, v7, :cond_6

    .line 244
    move-object/from16 v0, p3

    move/from16 v1, v34

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v31

    .line 246
    .local v31, "ret":Ljava/lang/String;
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitleSearchHeader:Landroid/widget/TextView;

    if-eqz v7, :cond_4

    .line 248
    const-string v7, "true"

    move-object/from16 v0, v31

    invoke-virtual {v7, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v7

    if-nez v7, :cond_5

    .line 250
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitleSearchHeader:Landroid/widget/TextView;

    const/16 v36, 0x0

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 252
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitleSearchHeader:Landroid/widget/TextView;

    const v36, 0x7f0b00e4

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(I)V

    .line 254
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v36, 0x7f09015e

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v7, v7

    move-object/from16 v0, v28

    iput v7, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 257
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mHighlight:Landroid/widget/TextView;

    const/16 v36, 0x8

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 259
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mPersonalContentsIcon:Landroid/widget/ImageView;

    if-eqz v7, :cond_4

    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mPersonalContentsIcon:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 262
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mPersonalContentsIcon:Landroid/widget/ImageView;

    const/16 v36, 0x8

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 271
    :cond_4
    :goto_2
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->disableOtherViews(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;Landroid/view/View;)V

    goto/16 :goto_1

    .line 267
    :cond_5
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitleSearchHeader:Landroid/widget/TextView;

    const/16 v36, 0x8

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 277
    .end local v31    # "ret":Ljava/lang/String;
    :cond_6
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mViewMode:I

    const/16 v36, 0x2

    move/from16 v0, v36

    if-eq v7, v0, :cond_9

    const/4 v7, -0x1

    move/from16 v0, v16

    if-eq v0, v7, :cond_9

    .line 279
    move-object/from16 v0, p3

    move/from16 v1, v16

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v31

    .line 281
    .restart local v31    # "ret":Ljava/lang/String;
    const-string v7, "true"

    move-object/from16 v0, v31

    invoke-virtual {v7, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v7

    if-nez v7, :cond_8

    .line 283
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitleSearchHeader:Landroid/widget/TextView;

    if-eqz v7, :cond_7

    .line 285
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitleSearchHeader:Landroid/widget/TextView;

    const/16 v36, 0x0

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 287
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitleSearchHeader:Landroid/widget/TextView;

    const v36, 0x7f0b00e5

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(I)V

    .line 289
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v36, 0x7f09015e

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v7, v7

    move-object/from16 v0, v28

    iput v7, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 292
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mHighlight:Landroid/widget/TextView;

    const/16 v36, 0x8

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 294
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mPersonalContentsIcon:Landroid/widget/ImageView;

    if-eqz v7, :cond_7

    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mPersonalContentsIcon:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->isEnabled()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 297
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mPersonalContentsIcon:Landroid/widget/ImageView;

    const/16 v36, 0x8

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 306
    :cond_7
    :goto_3
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->disableOtherViews(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;Landroid/view/View;)V

    .line 308
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDivider:Landroid/widget/ImageView;

    const/16 v36, 0x0

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 310
    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContentSubtitlePos:I

    goto/16 :goto_1

    .line 303
    :cond_8
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDivider:Landroid/widget/ImageView;

    const/16 v36, 0x8

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3

    .line 315
    .end local v31    # "ret":Ljava/lang/String;
    :cond_9
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mViewMode:I

    const/16 v36, 0x2

    move/from16 v0, v36

    if-eq v7, v0, :cond_a

    const/4 v7, -0x1

    move/from16 v0, v34

    if-ne v0, v7, :cond_a

    const/4 v7, -0x1

    move/from16 v0, v16

    if-ne v0, v7, :cond_a

    .line 317
    const/4 v7, -0x2

    move-object/from16 v0, v28

    iput v7, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 319
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v36, 0x7f090182

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v0, v7

    move/from16 v26, v0

    .line 321
    .restart local v26    # "itemHeight":I
    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/view/View;->setMinimumHeight(I)V

    .line 324
    .end local v26    # "itemHeight":I
    :cond_a
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getVisibility()I

    move-result v7

    if-eqz v7, :cond_b

    .line 326
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const/16 v36, 0x0

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 329
    :cond_b
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getVisibility()I

    move-result v7

    if-eqz v7, :cond_c

    .line 331
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    const/16 v36, 0x0

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 335
    :cond_c
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mViewMode:I

    const/16 v36, 0x2

    move/from16 v0, v36

    if-eq v7, v0, :cond_d

    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDivider:Landroid/widget/ImageView;

    if-eqz v7, :cond_d

    .line 337
    const/4 v7, 0x1

    move/from16 v0, v29

    if-ne v0, v7, :cond_19

    .line 339
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDivider:Landroid/widget/ImageView;

    const/16 v36, 0x8

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 352
    :cond_d
    :goto_4
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mViewMode:I

    const/16 v36, 0x2

    move/from16 v0, v36

    if-eq v7, v0, :cond_f

    .line 354
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v7

    const/16 v36, 0x9

    move/from16 v0, v36

    if-gt v7, v0, :cond_e

    .line 356
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    move/from16 v0, v29

    if-ne v7, v0, :cond_e

    .line 370
    :cond_e
    if-nez v29, :cond_f

    const/4 v7, -0x1

    move/from16 v0, v16

    if-ne v0, v7, :cond_f

    .line 377
    :cond_f
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mViewMode:I

    const/16 v36, 0x2

    move/from16 v0, v36

    if-eq v7, v0, :cond_10

    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitleSearchHeader:Landroid/widget/TextView;

    if-eqz v7, :cond_10

    .line 379
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitleSearchHeader:Landroid/widget/TextView;

    const/16 v36, 0x8

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 383
    :cond_10
    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/view/View;->setClickable(Z)V

    .line 386
    const-string v7, "_data"

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 388
    .local v10, "filePath":Ljava/lang/String;
    const/16 v7, 0x2f

    invoke-virtual {v10, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v10, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v17

    .line 391
    .local v17, "fileName":Ljava/lang/String;
    const/16 v22, 0x0

    .line 396
    .local v22, "highlightedContent":Ljava/lang/String;
    const-string v7, "highlight_content"

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    .local v21, "highlightContentIdx":I
    const/4 v7, -0x1

    move/from16 v0, v21

    if-eq v0, v7, :cond_11

    .line 398
    move-object/from16 v0, p3

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 402
    :cond_11
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    const/16 v36, 0x0

    move-object/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 404
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v7, :cond_12

    .line 406
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    const/16 v36, 0x0

    move-object/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 409
    :cond_12
    invoke-static {v10}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxFolder(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1a

    .line 411
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mPersonalContentsIcon:Landroid/widget/ImageView;

    const/16 v36, 0x0

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 418
    :goto_5
    const/16 v7, 0x3001

    move/from16 v0, v20

    if-ne v0, v7, :cond_1b

    .line 421
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getVisibility()I

    move-result v7

    const/16 v36, 0x8

    move/from16 v0, v36

    if-ne v7, v0, :cond_13

    .line 423
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    const/16 v36, 0x0

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 427
    :cond_13
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_14

    .line 450
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 452
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const v36, 0x7f0200af

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 509
    :cond_14
    :goto_6
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mViewMode:I

    const/16 v36, 0x1

    move/from16 v0, v36

    if-ne v7, v0, :cond_24

    .line 514
    const/16 v7, 0x3001

    move/from16 v0, v20

    if-ne v0, v7, :cond_22

    .line 516
    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    .line 526
    .local v33, "sb":Ljava/lang/StringBuilder;
    const/16 v7, 0x8

    if-ne v8, v7, :cond_21

    .line 528
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v36, 0x7f0c0002

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    invoke-static {v0, v10}, Lcom/sec/android/app/myfiles/utils/Utils;->getDropboxChildFileCount(Landroid/content/Context;Ljava/lang/String;)I

    move-result v37

    const/16 v38, 0x1

    move/from16 v0, v38

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v38, v0

    const/16 v39, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    invoke-static {v0, v10}, Lcom/sec/android/app/myfiles/utils/Utils;->getDropboxChildFileCount(Landroid/content/Context;Ljava/lang/String;)I

    move-result v40

    invoke-static/range {v40 .. v40}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v40

    aput-object v40, v38, v39

    move/from16 v0, v36

    move/from16 v1, v37

    move-object/from16 v2, v38

    invoke-virtual {v7, v0, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v33

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 535
    :goto_7
    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 543
    .end local v33    # "sb":Ljava/lang/StringBuilder;
    .local v15, "content":Ljava/lang/String;
    :goto_8
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mDownloadedAppList:Ljava/util/ArrayList;

    if-eqz v7, :cond_15

    const v7, 0x7f0b0007

    move/from16 v0, v20

    if-ne v0, v7, :cond_15

    .line 545
    const-string v7, "index"

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    const/16 v36, -0x1

    move/from16 v0, v36

    if-eq v7, v0, :cond_15

    const-string v7, "index"

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    const/16 v36, -0x1

    move/from16 v0, v36

    if-eq v7, v0, :cond_15

    .line 547
    const-string v7, "index"

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v25

    .line 549
    .local v25, "indexOfSearchApp":I
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mDownloadedAppList:Ljava/util/ArrayList;

    move/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->appEntryOfSearchApp:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .line 551
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->appEntryOfSearchApp:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget-wide v0, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->size:J

    move-wide/from16 v36, v0

    move-wide/from16 v0, v36

    invoke-static {v7, v0, v1}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v15

    .line 556
    .end local v25    # "indexOfSearchApp":I
    :cond_15
    if-eqz v22, :cond_23

    const-string v7, ""

    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_23

    .line 557
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    invoke-virtual {v7, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 558
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mHighlight:Landroid/widget/TextView;

    const/16 v36, 0x0

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 559
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mHighlight:Landroid/widget/TextView;

    move-object/from16 v0, v35

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mHighlight:Landroid/widget/TextView;

    move-object/from16 v36, v0

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v38, 0x20

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mSavedSearchKeyWord:Ljava/lang/String;

    move-object/from16 v38, v0

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    const/16 v38, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    move-object/from16 v2, v22

    move-object/from16 v3, v37

    move/from16 v4, v38

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->getColoredSpannableString(Landroid/widget/TextView;Ljava/lang/CharSequence;Ljava/lang/String;Z)Landroid/text/SpannableString;

    move-result-object v36

    move-object/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 569
    :goto_9
    const-string v7, "date_modified"

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v36

    const-wide/16 v38, 0x3e8

    mul-long v18, v36, v38

    .line 570
    .local v18, "dateValue":J
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    const-wide/16 v36, 0x3e8

    mul-long v36, v36, v18

    move-wide/from16 v0, v36

    invoke-static {v7, v0, v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getDateFormatByFormatSetting(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v15

    .line 572
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    invoke-virtual {v7, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 702
    .end local v15    # "content":Ljava/lang/String;
    .end local v18    # "dateValue":J
    :cond_16
    :goto_a
    new-instance v7, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$1;

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v7, v0, v1, v10}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$1;-><init>(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;ILjava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/view/View;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 818
    invoke-static {v10}, Lcom/sec/android/app/myfiles/MediaFile;->getFileTypeInt(Ljava/lang/String;)I

    move-result v9

    .line 819
    .local v9, "fileType":I
    move-object v12, v10

    .line 820
    .local v12, "hoverFilePath":Ljava/lang/String;
    const/16 v7, 0x3001

    move/from16 v0, v20

    if-ne v0, v7, :cond_30

    const/4 v11, 0x1

    .line 822
    .local v11, "bFolderFormat":Z
    :goto_b
    new-instance v6, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;

    move-object/from16 v7, p0

    invoke-direct/range {v6 .. v12}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter$2;-><init>(Lcom/sec/android/app/myfiles/adapter/SearchAdapter;IILjava/lang/String;ZLjava/lang/String;)V

    .line 960
    .local v6, "hoverListener":Landroid/view/View$OnHoverListener;
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mViewMode:I

    if-eqz v7, :cond_17

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mViewMode:I

    const/16 v36, 0x1

    move/from16 v0, v36

    if-ne v7, v0, :cond_31

    .line 962
    :cond_17
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v7, v6}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 974
    :cond_18
    :goto_c
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v32

    .line 977
    .local v32, "s":Ljava/lang/String;
    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_32

    .line 979
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    move-object/from16 v0, v32

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 341
    .end local v6    # "hoverListener":Landroid/view/View$OnHoverListener;
    .end local v9    # "fileType":I
    .end local v10    # "filePath":Ljava/lang/String;
    .end local v11    # "bFolderFormat":Z
    .end local v12    # "hoverFilePath":Ljava/lang/String;
    .end local v17    # "fileName":Ljava/lang/String;
    .end local v21    # "highlightContentIdx":I
    .end local v22    # "highlightedContent":Ljava/lang/String;
    .end local v32    # "s":Ljava/lang/String;
    :cond_19
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContentSubtitlePos:I

    const/16 v36, -0x1

    move/from16 v0, v36

    if-eq v7, v0, :cond_d

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContentSubtitlePos:I

    move/from16 v0, v29

    if-ne v0, v7, :cond_d

    .line 343
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDivider:Landroid/widget/ImageView;

    const/16 v36, 0x0

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_4

    .line 414
    .restart local v10    # "filePath":Ljava/lang/String;
    .restart local v17    # "fileName":Ljava/lang/String;
    .restart local v21    # "highlightContentIdx":I
    .restart local v22    # "highlightedContent":Ljava/lang/String;
    :cond_1a
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mPersonalContentsIcon:Landroid/widget/ImageView;

    const/16 v36, 0x8

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_5

    .line 456
    :cond_1b
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mDownloadedAppList:Ljava/util/ArrayList;

    if-eqz v7, :cond_1c

    const v7, 0x7f0b0007

    move/from16 v0, v20

    if-ne v0, v7, :cond_1c

    .line 458
    const-string v7, "index"

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 460
    .local v24, "indexColumnIdx":I
    const/4 v7, -0x1

    move/from16 v0, v24

    if-eq v0, v7, :cond_14

    .line 462
    move-object/from16 v0, p3

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v23

    .line 464
    .local v23, "index":I
    const/4 v7, -0x1

    move/from16 v0, v23

    if-eq v0, v7, :cond_14

    .line 466
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mDownloadedAppList:Ljava/util/ArrayList;

    move/from16 v0, v23

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .line 467
    .local v13, "appEntry":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    iget-object v0, v13, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->icon:Landroid/graphics/drawable/Drawable;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 468
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    iget-object v0, v13, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->label:Ljava/lang/String;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 469
    const v7, 0x7f0f0032

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v13}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_6

    .line 474
    .end local v13    # "appEntry":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    .end local v23    # "index":I
    .end local v24    # "indexColumnIdx":I
    :cond_1c
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_1d

    .line 476
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getShowFileExtensionStatus()Z

    move-result v7

    if-eqz v7, :cond_1f

    .line 478
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 491
    :cond_1d
    :goto_d
    invoke-static {v10}, Lcom/sec/android/app/myfiles/MediaFile;->getSmallIcon(Ljava/lang/String;)I

    move-result v30

    .line 493
    .local v30, "resId":I
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    move/from16 v0, v30

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 495
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v7, :cond_1e

    .line 497
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    const/16 v36, 0x0

    move-object/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 500
    :cond_1e
    const/16 v7, 0x8

    if-ne v8, v7, :cond_20

    .line 501
    const/16 v7, 0x14

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-virtual {v0, v1, v10, v7}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->loadThumbnail(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;Ljava/lang/String;I)V

    goto/16 :goto_6

    .line 482
    .end local v30    # "resId":I
    :cond_1f
    const/16 v7, 0x2e

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v27

    .line 484
    .local v27, "lastDot":I
    if-lez v27, :cond_1d

    .line 486
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    const/16 v36, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v36

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_d

    .line 503
    .end local v27    # "lastDot":I
    .restart local v30    # "resId":I
    :cond_20
    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-virtual {v0, v1, v10}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->loadThumbnail(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;Ljava/lang/String;)V

    goto/16 :goto_6

    .line 532
    .end local v30    # "resId":I
    .restart local v33    # "sb":Ljava/lang/StringBuilder;
    :cond_21
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v36, 0x7f0c0002

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    invoke-static {v0, v10}, Lcom/sec/android/app/myfiles/utils/Utils;->getChildFileCount(Landroid/content/Context;Ljava/lang/String;)I

    move-result v37

    const/16 v38, 0x1

    move/from16 v0, v38

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v38, v0

    const/16 v39, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    invoke-static {v0, v10}, Lcom/sec/android/app/myfiles/utils/Utils;->getChildFileCount(Landroid/content/Context;Ljava/lang/String;)I

    move-result v40

    invoke-static/range {v40 .. v40}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v40

    aput-object v40, v38, v39

    move/from16 v0, v36

    move/from16 v1, v37

    move-object/from16 v2, v38

    invoke-virtual {v7, v0, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v33

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    .line 539
    .end local v33    # "sb":Ljava/lang/StringBuilder;
    :cond_22
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    const-string v36, "_size"

    move-object/from16 v0, p3

    move-object/from16 v1, v36

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v36

    move-object/from16 v0, p3

    move/from16 v1, v36

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v36

    move-wide/from16 v0, v36

    invoke-static {v7, v0, v1}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v15

    .restart local v15    # "content":Ljava/lang/String;
    goto/16 :goto_8

    .line 562
    :cond_23
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    invoke-virtual {v7, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 563
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mHighlight:Landroid/widget/TextView;

    const/16 v36, 0x8

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_9

    .line 574
    .end local v15    # "content":Ljava/lang/String;
    :cond_24
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mViewMode:I

    if-nez v7, :cond_2f

    .line 578
    const-string v7, "/storage"

    invoke-virtual {v10, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_25

    const v7, 0x7f0b0007

    move/from16 v0, v20

    if-ne v0, v7, :cond_28

    .line 580
    :cond_25
    move-object v15, v10

    .line 591
    .restart local v15    # "content":Ljava/lang/String;
    :goto_e
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDetailContainer:Landroid/view/ViewGroup;

    if-eqz v7, :cond_26

    .line 593
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDetailContainer:Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v7

    if-eqz v7, :cond_26

    .line 595
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDetailContainer:Landroid/view/ViewGroup;

    const/16 v36, 0x0

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 600
    :cond_26
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mScreenState:I

    const/16 v36, 0x1

    move/from16 v0, v36

    if-ne v7, v0, :cond_2a

    .line 602
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMaxWidth()I

    move-result v7

    const/16 v36, 0x578

    move/from16 v0, v36

    if-eq v7, v0, :cond_27

    .line 604
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    const/16 v36, 0x578

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 623
    :cond_27
    :goto_f
    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2d

    .line 625
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    invoke-virtual {v7, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 627
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mHighlight:Landroid/widget/TextView;

    const/16 v36, 0x0

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 629
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mHighlight:Landroid/widget/TextView;

    move-object/from16 v0, v35

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mHighlight:Landroid/widget/TextView;

    move-object/from16 v36, v0

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v38, 0x20

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mSavedSearchKeyWord:Ljava/lang/String;

    move-object/from16 v38, v0

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    const/16 v38, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    move-object/from16 v2, v22

    move-object/from16 v3, v37

    move/from16 v4, v38

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->getColoredSpannableString(Landroid/widget/TextView;Ljava/lang/CharSequence;Ljava/lang/String;Z)Landroid/text/SpannableString;

    move-result-object v36

    move-object/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 644
    :goto_10
    const/16 v7, 0x3001

    move/from16 v0, v20

    if-ne v0, v7, :cond_2e

    .line 646
    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    .line 656
    .restart local v33    # "sb":Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v36, 0x7f0c0002

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    invoke-static {v0, v10}, Lcom/sec/android/app/myfiles/utils/Utils;->getChildFileCount(Landroid/content/Context;Ljava/lang/String;)I

    move-result v37

    const/16 v38, 0x1

    move/from16 v0, v38

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v38, v0

    const/16 v39, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    invoke-static {v0, v10}, Lcom/sec/android/app/myfiles/utils/Utils;->getChildFileCount(Landroid/content/Context;Ljava/lang/String;)I

    move-result v40

    invoke-static/range {v40 .. v40}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v40

    aput-object v40, v38, v39

    move/from16 v0, v36

    move/from16 v1, v37

    move-object/from16 v2, v38

    invoke-virtual {v7, v0, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v33

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 658
    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 660
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    const-string v36, ""

    move-object/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 671
    .end local v33    # "sb":Ljava/lang/StringBuilder;
    :goto_11
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mDownloadedAppList:Ljava/util/ArrayList;

    if-eqz v7, :cond_16

    const v7, 0x7f0b0007

    move/from16 v0, v20

    if-ne v0, v7, :cond_16

    .line 673
    const-string v7, "index"

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 675
    .restart local v24    # "indexColumnIdx":I
    const/4 v7, -0x1

    move/from16 v0, v24

    if-eq v0, v7, :cond_16

    .line 677
    move-object/from16 v0, p3

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v25

    .line 679
    .restart local v25    # "indexOfSearchApp":I
    const/4 v7, -0x1

    move/from16 v0, v25

    if-eq v0, v7, :cond_16

    .line 681
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mDownloadedAppList:Ljava/util/ArrayList;

    move/from16 v0, v25

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->appEntryOfSearchApp:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .line 683
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->appEntryOfSearchApp:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget-wide v0, v0, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->size:J

    move-wide/from16 v36, v0

    move-wide/from16 v0, v36

    invoke-static {v7, v0, v1}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v15

    .line 685
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    invoke-virtual {v7, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_a

    .line 582
    .end local v15    # "content":Ljava/lang/String;
    .end local v24    # "indexColumnIdx":I
    .end local v25    # "indexOfSearchApp":I
    :cond_28
    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mIsNeedReplaceDropboxWithBaiduCloud:Z

    if-eqz v7, :cond_29

    .line 584
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v36

    const v37, 0x7f0b00fa

    invoke-virtual/range {v36 .. v37}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, v36

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .restart local v15    # "content":Ljava/lang/String;
    goto/16 :goto_e

    .line 588
    .end local v15    # "content":Ljava/lang/String;
    :cond_29
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v36

    const v37, 0x7f0b0008

    invoke-virtual/range {v36 .. v37}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v36

    move-object/from16 v0, v36

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .restart local v15    # "content":Ljava/lang/String;
    goto/16 :goto_e

    .line 607
    :cond_2a
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mScreenState:I

    if-eqz v7, :cond_2b

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mScreenState:I

    const/16 v36, 0x2

    move/from16 v0, v36

    if-ne v7, v0, :cond_2c

    .line 609
    :cond_2b
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMaxWidth()I

    move-result v7

    const/16 v36, 0x2bc

    move/from16 v0, v36

    if-eq v7, v0, :cond_27

    .line 611
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    const/16 v36, 0x2bc

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setMaxWidth(I)V

    goto/16 :goto_f

    .line 614
    :cond_2c
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mScreenState:I

    const/16 v36, 0x3

    move/from16 v0, v36

    if-ne v7, v0, :cond_27

    .line 616
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMaxWidth()I

    move-result v7

    const/16 v36, 0x1c2

    move/from16 v0, v36

    if-eq v7, v0, :cond_27

    .line 618
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    const/16 v36, 0x1c2

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setMaxWidth(I)V

    goto/16 :goto_f

    .line 635
    :cond_2d
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mSizeOrItems:Landroid/widget/TextView;

    invoke-virtual {v7, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 637
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mHighlight:Landroid/widget/TextView;

    const/16 v36, 0x8

    move/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_10

    .line 664
    :cond_2e
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    const-string v36, "_size"

    move-object/from16 v0, p3

    move-object/from16 v1, v36

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v36

    move-object/from16 v0, p3

    move/from16 v1, v36

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v36

    move-wide/from16 v0, v36

    invoke-static {v7, v0, v1}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v15

    .line 667
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mDate:Landroid/widget/TextView;

    invoke-virtual {v7, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_11

    .line 690
    .end local v15    # "content":Ljava/lang/String;
    :cond_2f
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mViewMode:I

    const/16 v36, 0x2

    move/from16 v0, v36

    if-ne v7, v0, :cond_16

    .line 692
    const/16 v7, 0x3001

    move/from16 v0, v20

    if-ne v0, v7, :cond_16

    .line 694
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v7, :cond_16

    .line 696
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    const/16 v36, 0x0

    move-object/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_a

    .line 820
    .restart local v9    # "fileType":I
    .restart local v12    # "hoverFilePath":Ljava/lang/String;
    :cond_30
    const/4 v11, 0x0

    goto/16 :goto_b

    .line 966
    .restart local v6    # "hoverListener":Landroid/view/View$OnHoverListener;
    .restart local v11    # "bFolderFormat":Z
    :cond_31
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    if-eqz v7, :cond_18

    .line 968
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v7, v6}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    goto/16 :goto_c

    .line 983
    .restart local v32    # "s":Ljava/lang/String;
    :cond_32
    move-object/from16 v0, v35

    iget-object v7, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    move-object/from16 v0, v35

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    move-object/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mSavedSearchKeyWord:Ljava/lang/String;

    move-object/from16 v37, v0

    const/16 v38, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    move-object/from16 v2, v32

    move-object/from16 v3, v37

    move/from16 v4, v38

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->getColoredSpannableString(Landroid/widget/TextView;Ljava/lang/CharSequence;Ljava/lang/String;Z)Landroid/text/SpannableString;

    move-result-object v36

    move-object/from16 v0, v36

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method

.method public configurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1546
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->getScreenState(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mScreenState:I

    .line 1547
    return-void
.end method

.method createNewCursor(Landroid/database/Cursor;I)Landroid/database/Cursor;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "newMode"    # I

    .prologue
    .line 1304
    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 1305
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->createThumbnailCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    .line 1307
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->createListCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0
.end method

.method protected getColoredSpannableString(Landroid/widget/TextView;Ljava/lang/CharSequence;Ljava/lang/String;Z)Landroid/text/SpannableString;
    .locals 11
    .param p1, "textView"    # Landroid/widget/TextView;
    .param p2, "text"    # Ljava/lang/CharSequence;
    .param p3, "queryText"    # Ljava/lang/String;
    .param p4, "isContentHighlighter"    # Z

    .prologue
    const/16 v10, 0x21

    const/4 v9, -0x1

    .line 990
    if-nez p2, :cond_0

    .line 991
    const-string p2, ""

    .line 994
    :cond_0
    if-eqz p3, :cond_5

    .line 997
    invoke-virtual {p1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v7

    invoke-virtual {p3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v8

    invoke-static {v7, p2, v8}, Landroid/text/TextUtils;->getPrefixCharForIndian(Landroid/text/TextPaint;Ljava/lang/CharSequence;[C)[C

    move-result-object v4

    .line 999
    .local v4, "spanChar":[C
    if-eqz v4, :cond_2

    .line 1001
    new-instance v5, Landroid/text/SpannableString;

    invoke-direct {v5, p2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1002
    .local v5, "spanString":Landroid/text/SpannableString;
    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    aget-char v8, v4, v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    .line 1003
    .local v6, "startpoint":I
    const/4 v0, 0x0

    .line 1005
    .local v0, "endpoint":I
    array-length v7, v4

    if-eqz v7, :cond_1

    if-eq v6, v9, :cond_1

    .line 1006
    array-length v7, v4

    add-int v0, v6, v7

    .line 1007
    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mQueryWordHighlightColorSpan:Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {v5, v7, v6, v0, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1038
    .end local v0    # "endpoint":I
    .end local v4    # "spanChar":[C
    .end local v5    # "spanString":Landroid/text/SpannableString;
    .end local v6    # "startpoint":I
    :cond_1
    :goto_0
    return-object v5

    .line 1014
    .restart local v4    # "spanChar":[C
    :cond_2
    const-string v7, "[  ]+"

    invoke-static {v7}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    .line 1016
    .local v2, "p":Ljava/util/regex/Pattern;
    invoke-virtual {v2, p3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 1017
    if-eqz p4, :cond_4

    .line 1020
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v8, 0x20

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 1025
    :cond_3
    :goto_1
    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 1027
    .local v1, "index":I
    if-eq v1, v9, :cond_5

    .line 1028
    new-instance v3, Landroid/text/SpannableString;

    invoke-direct {v3, p2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1030
    .local v3, "span":Landroid/text/SpannableString;
    iget-object v7, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mQueryWordHighlightColorSpan:Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v8, v1

    invoke-virtual {v3, v7, v1, v8, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    move-object v5, v3

    .line 1033
    goto :goto_0

    .line 1021
    .end local v1    # "index":I
    .end local v3    # "span":Landroid/text/SpannableString;
    :cond_4
    const-string v7, " "

    invoke-virtual {p3, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1022
    const/4 v7, 0x1

    invoke-virtual {p3, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p3

    goto :goto_1

    .line 1038
    .end local v2    # "p":Ljava/util/regex/Pattern;
    .end local v4    # "spanChar":[C
    :cond_5
    new-instance v5, Landroid/text/SpannableString;

    invoke-direct {v5, p2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected getContentType()I
    .locals 1

    .prologue
    .line 1238
    const/16 v0, 0x201

    return v0
.end method

.method public setDownlodedAppsList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1251
    .local p1, "downlodedAppsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;>;"
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mDownloadedAppList:Ljava/util/ArrayList;

    .line 1253
    return-void
.end method

.method public setSearchKeyword(Ljava/lang/String;)V
    .locals 0
    .param p1, "mSavedSearchKeyWord"    # Ljava/lang/String;

    .prologue
    .line 1244
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/SearchAdapter;->mSavedSearchKeyWord:Ljava/lang/String;

    .line 1245
    return-void
.end method
