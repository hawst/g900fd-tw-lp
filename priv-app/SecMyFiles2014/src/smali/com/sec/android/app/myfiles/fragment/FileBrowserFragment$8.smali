.class Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$8;
.super Ljava/lang/Object;
.source "FileBrowserFragment.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/view/PathIndicator$OnPathChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;)V
    .locals 0

    .prologue
    .line 1634
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPathSelected(ILjava/lang/String;)V
    .locals 5
    .param p1, "targetFolderID"    # I
    .param p2, "targetFolderPath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1639
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->initFileObserver()V

    .line 1641
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->isSelectMode()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getSelectionType()I

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getSelectionType()I

    move-result v3

    if-eq v3, v1, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getSelectModeFrom()I

    move-result v3

    if-eq v3, v1, :cond_0

    .line 1643
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->finishSelectMode()V

    .line 1648
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    # invokes: Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->moveToFolder(Ljava/lang/String;Ljava/lang/Boolean;)V
    invoke-static {v3, p2, v4}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1650
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getSelectionType()I

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->getSelectionType()I

    move-result v3

    if-ne v3, v1, :cond_2

    .line 1651
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/FileBrowserFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->unselectAllItem()V

    .line 1653
    :cond_2
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isCheckMountedStorage()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1655
    const/4 v0, 0x0

    .line 1656
    .local v0, "menuItem":Landroid/view/MenuItem;
    sget-object v3, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->mOptionsMenu:Landroid/view/Menu;

    const v4, 0x7f0f0148

    invoke-interface {v3, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1658
    invoke-static {p2}, Lcom/sec/android/app/myfiles/utils/Utils;->isRootFolder(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    :goto_0
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1661
    .end local v0    # "menuItem":Landroid/view/MenuItem;
    :cond_3
    return-void

    .restart local v0    # "menuItem":Landroid/view/MenuItem;
    :cond_4
    move v1, v2

    .line 1658
    goto :goto_0
.end method
