.class Lcom/sec/android/app/myfiles/SelectorActivity$4;
.super Ljava/lang/Object;
.source "SelectorActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/SelectorActivity;->onCreateWarningDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

.field final synthetic val$dialogId:I

.field final synthetic val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/SelectorActivity;ILcom/sec/android/app/myfiles/utils/SharedDataStore;)V
    .locals 0

    .prologue
    .line 1736
    iput-object p1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$4;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    iput p2, p0, Lcom/sec/android/app/myfiles/SelectorActivity$4;->val$dialogId:I

    iput-object p3, p0, Lcom/sec/android/app/myfiles/SelectorActivity$4;->val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 1741
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 1743
    iget v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$4;->val$dialogId:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$4;->val$dialogId:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 1746
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$4;->val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$4;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->bDoNotCheckDropBox:Z
    invoke-static {v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$1500(Lcom/sec/android/app/myfiles/SelectorActivity;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setDoNotShowCheckDropBox(Z)V

    .line 1747
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$4;->val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->commitShowDataWarning()Z

    .line 1757
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$4;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mForWarningPosition:I
    invoke-static {v0}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$600(Lcom/sec/android/app/myfiles/SelectorActivity;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 1758
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$4;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mTabHost:Landroid/widget/TabHost;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$1700(Lcom/sec/android/app/myfiles/SelectorActivity;)Landroid/widget/TabHost;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$4;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->mForWarningPosition:I
    invoke-static {v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$600(Lcom/sec/android/app/myfiles/SelectorActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->setCurrentTab(I)V

    .line 1761
    :cond_2
    return-void

    .line 1750
    :cond_3
    iget v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$4;->val$dialogId:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 1752
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$4;->val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/SelectorActivity$4;->this$0:Lcom/sec/android/app/myfiles/SelectorActivity;

    # getter for: Lcom/sec/android/app/myfiles/SelectorActivity;->bDoNotCheckFTP:Z
    invoke-static {v1}, Lcom/sec/android/app/myfiles/SelectorActivity;->access$1600(Lcom/sec/android/app/myfiles/SelectorActivity;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setDoNotShowCheckFTP(Z)V

    .line 1753
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SelectorActivity$4;->val$sds:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->commitShowDataWarning()Z

    goto :goto_0
.end method
