.class public Lcom/sec/android/app/myfiles/utils/ImageCacheManager;
.super Ljava/lang/Object;
.source "ImageCacheManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/utils/ImageCacheManager$CacheType;
    }
.end annotation


# static fields
.field private static mInstance:Lcom/sec/android/app/myfiles/utils/ImageCacheManager;


# instance fields
.field private mImageCache:Lcom/android/volley/toolbox/ImageLoader$ImageCache;

.field private mImageLoader:Lcom/android/volley/toolbox/ImageLoader;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method private createKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 118
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance()Lcom/sec/android/app/myfiles/utils/ImageCacheManager;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ImageCacheManager;->mInstance:Lcom/sec/android/app/myfiles/utils/ImageCacheManager;

    if-nez v0, :cond_0

    .line 50
    new-instance v0, Lcom/sec/android/app/myfiles/utils/ImageCacheManager;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/utils/ImageCacheManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/utils/ImageCacheManager;->mInstance:Lcom/sec/android/app/myfiles/utils/ImageCacheManager;

    .line 53
    :cond_0
    sget-object v0, Lcom/sec/android/app/myfiles/utils/ImageCacheManager;->mInstance:Lcom/sec/android/app/myfiles/utils/ImageCacheManager;

    return-object v0
.end method


# virtual methods
.method public getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 77
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/utils/ImageCacheManager;->mImageCache:Lcom/android/volley/toolbox/ImageLoader$ImageCache;

    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/utils/ImageCacheManager;->createKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/android/volley/toolbox/ImageLoader$ImageCache;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 78
    :catch_0
    move-exception v0

    .line 79
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Disk Cache Not initialized"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getImage(Ljava/lang/String;Lcom/android/volley/toolbox/ImageLoader$ImageListener;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/android/volley/toolbox/ImageLoader$ImageListener;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/ImageCacheManager;->mImageLoader:Lcom/android/volley/toolbox/ImageLoader;

    invoke-virtual {v0, p1, p2}, Lcom/android/volley/toolbox/ImageLoader;->get(Ljava/lang/String;Lcom/android/volley/toolbox/ImageLoader$ImageListener;)Lcom/android/volley/toolbox/ImageLoader$ImageContainer;

    .line 101
    return-void
.end method

.method public getImageLoader()Lcom/android/volley/toolbox/ImageLoader;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/ImageCacheManager;->mImageLoader:Lcom/android/volley/toolbox/ImageLoader;

    return-object v0
.end method

.method public init(Landroid/content/Context;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cacheSize"    # I

    .prologue
    .line 70
    new-instance v0, Lcom/sec/android/app/myfiles/utils/BitmapLruImageCache;

    invoke-direct {v0, p2}, Lcom/sec/android/app/myfiles/utils/BitmapLruImageCache;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/utils/ImageCacheManager;->mImageCache:Lcom/android/volley/toolbox/ImageLoader$ImageCache;

    .line 71
    new-instance v0, Lcom/android/volley/toolbox/ImageLoader;

    invoke-static {}, Lcom/sec/android/app/myfiles/utils/RequestManager;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/utils/ImageCacheManager;->mImageCache:Lcom/android/volley/toolbox/ImageLoader$ImageCache;

    invoke-direct {v0, v1, v2}, Lcom/android/volley/toolbox/ImageLoader;-><init>(Lcom/android/volley/RequestQueue;Lcom/android/volley/toolbox/ImageLoader$ImageCache;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/utils/ImageCacheManager;->mImageLoader:Lcom/android/volley/toolbox/ImageLoader;

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/myfiles/utils/ImageCacheManager;->mImageLoader:Lcom/android/volley/toolbox/ImageLoader;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/volley/toolbox/ImageLoader;->setBatchedResponseDelay(I)V

    .line 73
    return-void
.end method

.method public putBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 85
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/utils/ImageCacheManager;->mImageCache:Lcom/android/volley/toolbox/ImageLoader$ImageCache;

    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/utils/ImageCacheManager;->createKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p2}, Lcom/android/volley/toolbox/ImageLoader$ImageCache;->putBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    return-void

    .line 86
    :catch_0
    move-exception v0

    .line 87
    .local v0, "e":Ljava/lang/NullPointerException;
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Disk Cache Not initialized"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
