.class public Lcom/sec/android/app/myfiles/fragment/InputNameFragment;
.super Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;
.source "InputNameFragment.java"


# instance fields
.field private mExistZipFile:Ljava/io/File;

.field private mFile:Ljava/io/File;

.field private mInputName:Ljava/lang/String;

.field private mPath:Ljava/lang/String;

.field private mToast:Landroid/widget/Toast;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mToast:Landroid/widget/Toast;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/fragment/InputNameFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mInputName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/myfiles/fragment/InputNameFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/InputNameFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mInputName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/fragment/InputNameFragment;)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mExistZipFile:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/myfiles/fragment/InputNameFragment;Ljava/io/File;)Ljava/io/File;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/InputNameFragment;
    .param p1, "x1"    # Ljava/io/File;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mExistZipFile:Ljava/io/File;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/myfiles/fragment/InputNameFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/InputNameFragment;
    .param p1, "x1"    # I

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->showToast(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/myfiles/fragment/InputNameFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/myfiles/fragment/InputNameFragment;)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mFile:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/myfiles/fragment/InputNameFragment;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/InputNameFragment;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/myfiles/fragment/InputNameFragment;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/fragment/InputNameFragment;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mToast:Landroid/widget/Toast;

    return-object p1
.end method

.method private showToast(I)V
    .locals 2
    .param p1, "resId"    # I

    .prologue
    .line 214
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$4;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$4;-><init>(Lcom/sec/android/app/myfiles/fragment/InputNameFragment;I)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 230
    return-void
.end method


# virtual methods
.method public onResume()V
    .locals 3

    .prologue
    .line 127
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mPath:Ljava/lang/String;

    const-string v2, "Dropbox/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 128
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mPath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 129
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 130
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->dismiss()V

    .line 132
    .end local v0    # "file":Ljava/io/File;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    check-cast v1, Landroid/app/AlertDialog;

    if-eqz v1, :cond_1

    .line 133
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    check-cast v1, Landroid/app/AlertDialog;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mOkButton:Landroid/widget/Button;

    .line 134
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    check-cast v1, Landroid/app/AlertDialog;

    const/4 v2, -0x2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mCancelButton:Landroid/widget/Button;

    .line 137
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "title"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const v2, 0x7f0b0023

    if-ne v1, v2, :cond_4

    .line 138
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/storage/emulated/0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mInputEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0b003e

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mInputEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 139
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mOkButton:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 142
    :cond_4
    invoke-super {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->onResume()V

    .line 143
    return-void
.end method

.method protected setupDialog(Landroid/app/AlertDialog$Builder;)V
    .locals 3
    .param p1, "builder"    # Landroid/app/AlertDialog$Builder;

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "current_item"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mPath:Ljava/lang/String;

    .line 58
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mPath:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mFile:Ljava/io/File;

    .line 60
    const v0, 0x7f0b0023

    .line 62
    .local v0, "positiveButton":I
    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->cur_operation:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    .line 63
    const v0, 0x7f0b0027

    .line 66
    :cond_0
    iget v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->cur_operation:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_1

    .line 67
    const v0, 0x7f0b0028

    .line 70
    :cond_1
    new-instance v1, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/InputNameFragment;)V

    invoke-virtual {p1, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 115
    const v1, 0x7f0b0017

    new-instance v2, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$2;-><init>(Lcom/sec/android/app/myfiles/fragment/InputNameFragment;)V

    invoke-virtual {p1, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 122
    return-void
.end method

.method protected setupInputEditText(Landroid/widget/EditText;)V
    .locals 4
    .param p1, "editText"    # Landroid/widget/EditText;

    .prologue
    const/4 v3, 0x1

    .line 148
    if-nez p1, :cond_0

    .line 210
    :goto_0
    return-void

    .line 153
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "file_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 155
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "file_name"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 156
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "file_name"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 157
    const v0, 0x7f0b003e

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setText(I)V

    .line 165
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->mPath:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getEditTextFilter(Landroid/content/Context;I)[Landroid/text/InputFilter;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 167
    invoke-virtual {p1, v3}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 169
    invoke-virtual {p1, v3}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 171
    const-string v0, "inputType=PredictionOff;inputType=filename;disableEmoticonInput=true"

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 175
    const v0, 0x84001

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setInputType(I)V

    .line 178
    new-instance v0, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment$3;-><init>(Lcom/sec/android/app/myfiles/fragment/InputNameFragment;)V

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    goto :goto_0

    .line 158
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "file_name"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "extSdCard"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 159
    const v0, 0x7f0b003f

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setText(I)V

    goto :goto_1

    .line 160
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/InputNameFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "file_name"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Private"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 161
    const v0, 0x7f0b0151

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setText(I)V

    goto :goto_1
.end method
