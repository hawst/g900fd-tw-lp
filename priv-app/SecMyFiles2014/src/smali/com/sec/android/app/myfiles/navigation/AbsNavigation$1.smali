.class Lcom/sec/android/app/myfiles/navigation/AbsNavigation$1;
.super Ljava/lang/Object;
.source "AbsNavigation.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->notifyUpdate(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

.field final synthetic val$needToRefresh:Z


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/navigation/AbsNavigation;Z)V
    .locals 0

    .prologue
    .line 490
    iput-object p1, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    iput-boolean p2, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$1;->val$needToRefresh:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 495
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    iget-object v3, v2, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mObserverList:Ljava/util/ArrayList;

    monitor-enter v3

    .line 497
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->mObserverList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;

    .line 499
    .local v1, "observer":Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$1;->val$needToRefresh:Z

    if-eqz v2, :cond_0

    .line 501
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getInstance()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;->refresh(Lcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    goto :goto_0

    .line 508
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "observer":Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 505
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v1    # "observer":Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$1;->this$0:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getInstance()Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;->update(Lcom/sec/android/app/myfiles/navigation/AbsNavigation;)V

    goto :goto_0

    .line 508
    .end local v1    # "observer":Lcom/sec/android/app/myfiles/navigation/AbsNavigation$NavigationObserver;
    :cond_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 509
    return-void
.end method
