.class Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$33;
.super Ljava/lang/Object;
.source "CategoryHomeFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnTwMultiSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field dragSelectEnable:Z

.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V
    .locals 1

    .prologue
    .line 3542
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$33;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3544
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$33;->dragSelectEnable:Z

    return-void
.end method


# virtual methods
.method public OnTwMultiSelectStart(II)V
    .locals 4
    .param p1, "startX"    # I
    .param p2, "startY"    # I

    .prologue
    const/4 v3, 0x1

    .line 3559
    const/4 v0, 0x0

    const-string v1, "CategoryHomeFragment"

    const-string v2, "MultiSelect Start = "

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 3561
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$33;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getSelectionType()I

    move-result v0

    if-nez v0, :cond_1

    .line 3572
    :cond_0
    :goto_0
    return-void

    .line 3564
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$33;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$33;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->getSelectableItemsCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 3568
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$33;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$2000(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setEnableDragBlock(Z)V

    .line 3570
    iput-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$33;->dragSelectEnable:Z

    goto :goto_0
.end method

.method public OnTwMultiSelectStop(II)V
    .locals 7
    .param p1, "endX"    # I
    .param p2, "endY"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 3577
    const-string v3, "CategoryHomeFragment"

    const-string v4, "MultiSelect end = "

    invoke-static {v5, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 3579
    iget-boolean v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$33;->dragSelectEnable:Z

    if-eqz v3, :cond_4

    .line 3581
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$33;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->isSelectMode()Z

    move-result v3

    if-nez v3, :cond_0

    .line 3583
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$33;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    const/4 v4, -0x2

    invoke-virtual {v3, v6, v4, v5}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->startSelectMode(III)V

    .line 3584
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$33;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    move-result-object v3

    invoke-virtual {v3, v5, v5}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->startSelectMode(II)V

    .line 3585
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$33;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    move-result-object v3

    invoke-virtual {v3, v5, v5}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->startSelectMode(II)V

    .line 3586
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$33;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCategoryAdapter:Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$000(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;

    move-result-object v3

    invoke-virtual {v3, v5, v5}, Lcom/sec/android/app/myfiles/adapter/CategoryAdapter;->startSelectMode(II)V

    .line 3587
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$33;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mStorageListAdapter:Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$500(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;

    move-result-object v3

    invoke-virtual {v3, v5, v5}, Lcom/sec/android/app/myfiles/adapter/StorageListAdapter;->startSelectMode(II)V

    .line 3588
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$33;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mRemoteShareListAdapter:Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$600(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;

    move-result-object v3

    invoke-virtual {v3, v5, v5}, Lcom/sec/android/app/myfiles/adapter/RemoteShareListAdapter;->startSelectMode(II)V

    .line 3589
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$33;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setAllItemCount()V

    .line 3592
    :cond_0
    const/4 v2, 0x0

    .line 3595
    .local v2, "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$33;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mTwDragSelectedItemMap:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$1900(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 3597
    .local v0, "i":I
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$33;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mTwDragSelectedItemMap:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$1900(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Ljava/util/HashMap;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v6, :cond_1

    .line 3599
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$33;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    check-cast v2, Lcom/sec/android/app/myfiles/element/ShortcutItem;

    .line 3601
    .restart local v2    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v3

    const/16 v4, 0x1f

    if-eq v3, v4, :cond_1

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/element/ShortcutItem;->getType()I

    move-result v3

    const/16 v4, 0x8

    if-eq v3, v4, :cond_1

    .line 3606
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$33;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mCloudListAdapter:Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/sec/android/app/myfiles/adapter/CloudListAdapter;->selectItem(I)V

    goto :goto_0

    .line 3610
    .end local v0    # "i":I
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$33;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mTwDragSelectedItemMap:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$1900(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    .line 3611
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$33;->dragSelectEnable:Z

    .line 3614
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "item":Lcom/sec/android/app/myfiles/element/ShortcutItem;
    :cond_4
    return-void
.end method

.method public onTwMultiSelected(Landroid/widget/AdapterView;Landroid/view/View;IJZZZ)V
    .locals 4
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .param p6, "isShiftpress"    # Z
    .param p7, "isCtrlpress"    # Z
    .param p8, "isPentpress"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJZZZ)V"
        }
    .end annotation

    .prologue
    .line 3548
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v0, 0x0

    const-string v1, "CategoryHomeFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onTwMultiSelected call position = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  PenPress is = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->e(ILjava/lang/String;Ljava/lang/String;)I

    .line 3550
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$33;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mTwDragSelectedItemMap:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$1900(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3551
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$33;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mTwDragSelectedItemMap:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$1900(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Ljava/util/HashMap;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$33;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mTwDragSelectedItemMap:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$1900(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3554
    :goto_0
    return-void

    .line 3553
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$33;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mTwDragSelectedItemMap:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$1900(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
