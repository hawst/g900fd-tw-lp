.class Lcom/sec/android/app/myfiles/view/MyFilesListView$2;
.super Landroid/view/animation/Animation;
.source "MyFilesListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/view/MyFilesListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/view/MyFilesListView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/view/MyFilesListView;)V
    .locals 0

    .prologue
    .line 303
    iput-object p1, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView$2;->this$0:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 2
    .param p1, "interpolatedTime"    # F
    .param p2, "t"    # Landroid/view/animation/Transformation;

    .prologue
    .line 308
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView$2;->this$0:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView$2;->this$0:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    # getter for: Lcom/sec/android/app/myfiles/view/MyFilesListView;->mMaximumHeight:I
    invoke-static {v0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->access$300(Lcom/sec/android/app/myfiles/view/MyFilesListView;)I

    move-result v0

    :goto_0
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 311
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView$2;->this$0:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->requestLayout()V

    .line 312
    return-void

    .line 308
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesListView$2;->this$0:Lcom/sec/android/app/myfiles/view/MyFilesListView;

    # getter for: Lcom/sec/android/app/myfiles/view/MyFilesListView;->mMaximumHeight:I
    invoke-static {v0}, Lcom/sec/android/app/myfiles/view/MyFilesListView;->access$300(Lcom/sec/android/app/myfiles/view/MyFilesListView;)I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    goto :goto_0
.end method

.method public willChangeBounds()Z
    .locals 1

    .prologue
    .line 317
    const/4 v0, 0x1

    return v0
.end method
