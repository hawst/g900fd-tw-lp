.class Lcom/sec/android/app/myfiles/SearchActivity$3;
.super Ljava/lang/Object;
.source "SearchActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/SearchActivity;->initSearchMenu()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/SearchActivity;

.field final synthetic val$adapter:Lcom/sec/android/app/myfiles/SearchActivity$SpinnerArrayAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/SearchActivity;Lcom/sec/android/app/myfiles/SearchActivity$SpinnerArrayAdapter;)V
    .locals 0

    .prologue
    .line 271
    iput-object p1, p0, Lcom/sec/android/app/myfiles/SearchActivity$3;->this$0:Lcom/sec/android/app/myfiles/SearchActivity;

    iput-object p2, p0, Lcom/sec/android/app/myfiles/SearchActivity$3;->val$adapter:Lcom/sec/android/app/myfiles/SearchActivity$SpinnerArrayAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 276
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity$3;->val$adapter:Lcom/sec/android/app/myfiles/SearchActivity$SpinnerArrayAdapter;

    invoke-virtual {v0, p3}, Lcom/sec/android/app/myfiles/SearchActivity$SpinnerArrayAdapter;->setCurrentPosition(I)V

    .line 278
    packed-switch p3, :pswitch_data_0

    .line 309
    :goto_0
    return-void

    .line 282
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity$3;->this$0:Lcom/sec/android/app/myfiles/SearchActivity;

    # invokes: Lcom/sec/android/app/myfiles/SearchActivity;->handleMenuItemAll()V
    invoke-static {v0}, Lcom/sec/android/app/myfiles/SearchActivity;->access$300(Lcom/sec/android/app/myfiles/SearchActivity;)V

    goto :goto_0

    .line 286
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity$3;->this$0:Lcom/sec/android/app/myfiles/SearchActivity;

    # invokes: Lcom/sec/android/app/myfiles/SearchActivity;->handleMenuItemLocation()V
    invoke-static {v0}, Lcom/sec/android/app/myfiles/SearchActivity;->access$400(Lcom/sec/android/app/myfiles/SearchActivity;)V

    goto :goto_0

    .line 291
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity$3;->this$0:Lcom/sec/android/app/myfiles/SearchActivity;

    # invokes: Lcom/sec/android/app/myfiles/SearchActivity;->handleMenuItemExtension()V
    invoke-static {v0}, Lcom/sec/android/app/myfiles/SearchActivity;->access$500(Lcom/sec/android/app/myfiles/SearchActivity;)V

    goto :goto_0

    .line 296
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity$3;->this$0:Lcom/sec/android/app/myfiles/SearchActivity;

    # invokes: Lcom/sec/android/app/myfiles/SearchActivity;->handleMenuItemDate()V
    invoke-static {v0}, Lcom/sec/android/app/myfiles/SearchActivity;->access$600(Lcom/sec/android/app/myfiles/SearchActivity;)V

    goto :goto_0

    .line 301
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity$3;->this$0:Lcom/sec/android/app/myfiles/SearchActivity;

    # invokes: Lcom/sec/android/app/myfiles/SearchActivity;->handleMenuItemFileType()V
    invoke-static {v0}, Lcom/sec/android/app/myfiles/SearchActivity;->access$700(Lcom/sec/android/app/myfiles/SearchActivity;)V

    goto :goto_0

    .line 306
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/myfiles/SearchActivity$3;->this$0:Lcom/sec/android/app/myfiles/SearchActivity;

    # invokes: Lcom/sec/android/app/myfiles/SearchActivity;->handleMenuItemAdvanced()V
    invoke-static {v0}, Lcom/sec/android/app/myfiles/SearchActivity;->access$800(Lcom/sec/android/app/myfiles/SearchActivity;)V

    goto :goto_0

    .line 278
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_5
    .end packed-switch
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 313
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
