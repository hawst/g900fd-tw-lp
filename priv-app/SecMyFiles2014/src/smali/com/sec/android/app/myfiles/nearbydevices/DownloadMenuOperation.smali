.class public Lcom/sec/android/app/myfiles/nearbydevices/DownloadMenuOperation;
.super Ljava/lang/Object;
.source "DownloadMenuOperation.java"


# static fields
.field public static final MAX_DOWNLOAD_ITEMS:I = 0xc8

.field public static final MODULE:Ljava/lang/String; = "DownloadMenuOperation"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static downloadItems(Ljava/util/ArrayList;Ljava/lang/String;)Z
    .locals 1
    .param p1, "serverName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Item;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 31
    .local p0, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    invoke-static {}, Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;->getDownLoader()Lcom/samsung/android/allshare/extension/SECDownloader;

    move-result-object v0

    invoke-virtual {v0, p1, p0}, Lcom/samsung/android/allshare/extension/SECDownloader;->Download(Ljava/lang/String;Ljava/util/ArrayList;)Z

    move-result v0

    return v0
.end method
