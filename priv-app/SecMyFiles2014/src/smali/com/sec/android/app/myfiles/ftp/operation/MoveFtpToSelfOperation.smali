.class public Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;
.super Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;
.source "MoveFtpToSelfOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation$1;
    }
.end annotation


# instance fields
.field private mDoneCount:I

.field private mParam:Lcom/sec/android/app/myfiles/element/FileOperationParam;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "operation"    # I
    .param p3, "contentType"    # I
    .param p4, "progressView"    # Landroid/view/View;
    .param p5, "ftpHandlerParams"    # Ljava/lang/String;

    .prologue
    .line 38
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;-><init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;)V

    .line 219
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mDoneCount:I

    .line 40
    return-void
.end method


# virtual methods
.method protected cancelOperation()V
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v0}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->abortTransfer()V

    .line 213
    invoke-super {p0}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->cancelOperation()V

    .line 215
    return-void
.end method

.method protected varargs doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;
    .locals 11
    .param p1, "args"    # [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 65
    invoke-static {v9, p1}, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->checkFileOperationParam(Z[Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/ftp/FTPErr;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 67
    iget-object v7, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    sget-object v8, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    if-ne v7, v8, :cond_0

    .line 69
    invoke-static {p1}, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->checkTargetSourceSameDir([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/ftp/FTPErr;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 73
    :cond_0
    iget-object v7, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    sget-object v8, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    if-ne v7, v8, :cond_1

    .line 75
    iget-object v7, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-virtual {p0, v7}, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->checkConnection(Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;)Lcom/sec/android/app/myfiles/ftp/FTPErr;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 79
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    sget-object v8, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    if-ne v7, v8, :cond_8

    .line 81
    aget-object v4, p1, v9

    .line 83
    .local v4, "param":Lcom/sec/android/app/myfiles/element/FileOperationParam;
    iput-object v4, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mParam:Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .line 85
    const/4 v5, 0x0

    .line 87
    .local v5, "succeeded":I
    iget-object v7, v4, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 89
    .local v3, "itemStr":Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/ftp/FTPItem;->getItem(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPItem;

    move-result-object v1

    .line 91
    .local v1, "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    if-eqz v1, :cond_2

    .line 93
    invoke-interface {v1}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v2

    .line 95
    .local v2, "itemPath":Ljava/lang/String;
    iget-object v7, v4, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    iget-object v8, v4, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mSrcFolder:Ljava/lang/String;

    invoke-static {v2, v7, v8}, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->getTargetPath(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 97
    .local v6, "targetPath":Ljava/lang/String;
    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mRunRename:Z

    .line 99
    iput-boolean v9, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mOverwrite:Z

    .line 101
    iget-object v7, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mContext:Landroid/content/Context;

    invoke-static {v7, v6}, Lcom/sec/android/app/myfiles/utils/Utils;->getIfFtpFileExist(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 102
    iput v10, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mFormat:I

    .line 103
    iput-object v6, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mPath:Ljava/lang/String;

    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->showRenameDialog()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 106
    iget-object v7, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mPath:Ljava/lang/String;

    invoke-virtual {p0, v7, v10}, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->startRename(Ljava/lang/String;Z)V

    .line 110
    :cond_3
    iget-boolean v7, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mRunRename:Z

    if-eqz v7, :cond_4

    .line 112
    iget-object v7, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mRename:Ljava/lang/String;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mRename:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_5

    .line 114
    iget-object v6, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mRename:Ljava/lang/String;

    .line 122
    :cond_4
    :goto_1
    if-eqz v6, :cond_2

    iget-object v7, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v7, v2, v6}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncRenameFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 125
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 116
    :cond_5
    iget-boolean v7, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mOverwrite:Z

    if-nez v7, :cond_4

    .line 118
    const/4 v6, 0x0

    goto :goto_1

    .line 133
    .end local v1    # "item":Lcom/sec/android/app/myfiles/ftp/IFTPItem;
    .end local v2    # "itemPath":Ljava/lang/String;
    .end local v3    # "itemStr":Ljava/lang/String;
    .end local v6    # "targetPath":Ljava/lang/String;
    :cond_6
    iget-object v7, v4, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-eq v5, v7, :cond_7

    .line 135
    sget-object v7, Lcom/sec/android/app/myfiles/ftp/FTPErr;->PARTIALLY_DONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    iput-object v7, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 139
    :cond_7
    iput v5, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mDoneCount:I

    .line 143
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v4    # "param":Lcom/sec/android/app/myfiles/element/FileOperationParam;
    .end local v5    # "succeeded":I
    :cond_8
    const/4 v7, 0x0

    return-object v7
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 33
    check-cast p1, [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V
    .locals 7
    .param p1, "result"    # Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0xa

    .line 149
    const/4 v1, 0x0

    const-string v2, "MoveFtpToSelfOperation"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onPostExecute : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 150
    sget-object v1, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation$1;->$SwitchMap$com$sec$android$app$myfiles$ftp$FTPErr:[I

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPErr;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 190
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    if-eqz v1, :cond_0

    .line 192
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    iget v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mDoneCount:I

    invoke-interface {v1, v6, v2}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;->onCancelled(Ljava/util/ArrayList;I)V

    .line 196
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    sget-object v2, Lcom/sec/android/app/myfiles/ftp/FTPErr;->TARGET_SAME_AS_SOURCE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    if-eq v1, v2, :cond_1

    .line 198
    const v1, 0x7f0b00cb

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->showToast(I)V

    .line 206
    :cond_1
    :goto_0
    return-void

    .line 154
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    if-eqz v1, :cond_3

    .line 156
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 158
    .local v0, "arg":Landroid/os/Bundle;
    const-string v1, "FILE_OPERATION"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 160
    const-string v1, "src_fragment_id"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 162
    const-string v1, "dest_fragment_id"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 164
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mParam:Lcom/sec/android/app/myfiles/element/FileOperationParam;

    if-eqz v1, :cond_2

    .line 166
    const-string v1, "target_folder"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mParam:Lcom/sec/android/app/myfiles/element/FileOperationParam;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    iget v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mDoneCount:I

    iget v3, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mDoneCount:I

    invoke-interface {v1, v2, v3, v0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;->onCompleted(IILandroid/os/Bundle;)V

    .line 174
    .end local v0    # "arg":Landroid/os/Bundle;
    :cond_3
    const v1, 0x7f0c0008

    iget v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mDoneCount:I

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->showToastPrurals(II)V

    goto :goto_0

    .line 180
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    if-eqz v1, :cond_4

    .line 181
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    iget v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mDoneCount:I

    invoke-interface {v1, v6, v2}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;->onCancelled(Ljava/util/ArrayList;I)V

    .line 184
    :cond_4
    const v1, 0x7f0b0069

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->showToast(I)V

    goto :goto_0

    .line 150
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 33
    check-cast p1, Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V

    return-void
.end method

.method protected setupProgressView()V
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 47
    return-void
.end method

.method protected startRename(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "isFile"    # Z

    .prologue
    .line 52
    const/4 v0, 0x0

    .line 53
    .local v0, "i":I
    move-object v1, p1

    .line 55
    .local v1, "renamedFile":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0, p1, v0, p2}, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->addPostfix(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v1

    .line 56
    add-int/lit8 v0, v0, 0x1

    .line 57
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mContext:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getIfFtpFileExist(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 59
    iput-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/MoveFtpToSelfOperation;->mRename:Ljava/lang/String;

    .line 60
    return-void
.end method
