.class Lcom/sec/android/app/myfiles/ftp/FTPSupplier$2;
.super Ljava/lang/Object;
.source "FTPSupplier.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/ftp/FTPAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->deleteFiles([Ljava/lang/String;[Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mCurrentFile:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/ftp/FTPSupplier;)V
    .locals 1

    .prologue
    .line 277
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier$2;->this$0:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 307
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier$2;->mCurrentFile:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getWorkElement()Ljava/lang/String;
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier$2;->mCurrentFile:Ljava/lang/String;

    return-object v0
.end method

.method public onDone(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "result"    # Landroid/os/Bundle;

    .prologue
    .line 298
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier$2;->this$0:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mResponseListener:Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;

    invoke-interface {v0}, Lcom/sec/android/app/myfiles/ftp/IFTPResponseListener;->onFileDeleted()V

    .line 299
    return-void
.end method

.method public work(Landroid/os/Bundle;)Z
    .locals 7
    .param p1, "arg"    # Landroid/os/Bundle;

    .prologue
    .line 280
    if-eqz p1, :cond_1

    .line 281
    const-string v6, "recur_disc_files"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 282
    .local v2, "files":[Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 283
    move-object v0, v2

    .local v0, "arr$":[Ljava/lang/String;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v1, v0, v4

    .line 284
    .local v1, "file":Ljava/lang/String;
    iput-object v1, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier$2;->mCurrentFile:Ljava/lang/String;

    .line 285
    iget-object v6, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier$2;->this$0:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    # getter for: Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    invoke-static {v6}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->access$000(Lcom/sec/android/app/myfiles/ftp/FTPSupplier;)Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    move-result-object v6

    invoke-interface {v6, v1}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncDeleteFile(Ljava/lang/String;)Z

    .line 283
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 288
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "file":Ljava/lang/String;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :cond_0
    const-string v6, "recur_disc_folders"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 289
    .local v3, "folders":[Ljava/lang/String;
    if-eqz v3, :cond_1

    array-length v6, v3

    if-lez v6, :cond_1

    .line 290
    iget-object v6, p0, Lcom/sec/android/app/myfiles/ftp/FTPSupplier$2;->this$0:Lcom/sec/android/app/myfiles/ftp/FTPSupplier;

    # getter for: Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;
    invoke-static {v6}, Lcom/sec/android/app/myfiles/ftp/FTPSupplier;->access$000(Lcom/sec/android/app/myfiles/ftp/FTPSupplier;)Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    move-result-object v6

    invoke-interface {v6, v3}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->syncDeleteFolders([Ljava/lang/String;)I

    .line 293
    .end local v2    # "files":[Ljava/lang/String;
    .end local v3    # "folders":[Ljava/lang/String;
    :cond_1
    const/4 v6, 0x1

    return v6
.end method
