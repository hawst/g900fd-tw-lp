.class public final enum Lcom/sec/android/app/myfiles/ftp/FTPMode;
.super Ljava/lang/Enum;
.source "FTPMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/myfiles/ftp/FTPMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/myfiles/ftp/FTPMode;

.field public static final enum ACTIVE:Lcom/sec/android/app/myfiles/ftp/FTPMode;

.field public static final enum PASSIVE:Lcom/sec/android/app/myfiles/ftp/FTPMode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 20
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPMode;

    const-string v1, "ACTIVE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/FTPMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPMode;->ACTIVE:Lcom/sec/android/app/myfiles/ftp/FTPMode;

    .line 21
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPMode;

    const-string v1, "PASSIVE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/myfiles/ftp/FTPMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPMode;->PASSIVE:Lcom/sec/android/app/myfiles/ftp/FTPMode;

    .line 19
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/myfiles/ftp/FTPMode;

    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPMode;->ACTIVE:Lcom/sec/android/app/myfiles/ftp/FTPMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPMode;->PASSIVE:Lcom/sec/android/app/myfiles/ftp/FTPMode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPMode;->$VALUES:[Lcom/sec/android/app/myfiles/ftp/FTPMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 19
    const-class v0, Lcom/sec/android/app/myfiles/ftp/FTPMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/ftp/FTPMode;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/myfiles/ftp/FTPMode;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/app/myfiles/ftp/FTPMode;->$VALUES:[Lcom/sec/android/app/myfiles/ftp/FTPMode;

    invoke-virtual {v0}, [Lcom/sec/android/app/myfiles/ftp/FTPMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/myfiles/ftp/FTPMode;

    return-object v0
.end method
