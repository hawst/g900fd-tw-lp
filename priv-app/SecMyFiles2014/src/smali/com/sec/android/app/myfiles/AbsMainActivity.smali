.class public abstract Lcom/sec/android/app/myfiles/AbsMainActivity;
.super Landroid/app/Activity;
.source "AbsMainActivity.java"

# interfaces
.implements Landroid/content/ComponentCallbacks2;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;
    }
.end annotation


# static fields
.field private static final MODULE:Ljava/lang/String; = "AbsMainActivity"


# instance fields
.field private cloudItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/ShortcutItem;",
            ">;"
        }
    .end annotation
.end field

.field private isDeleteDialogWasShowing:Z

.field protected mActionBarManager:Lcom/sec/android/app/myfiles/view/ActionBarManager;

.field private mAdapterManager:Lcom/sec/android/app/myfiles/adapter/AdapterManager;

.field private mBundle:Landroid/os/Bundle;

.field protected mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

.field protected mDefaultActionBarView:Landroid/view/View;

.field protected mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

.field private mEasyModeChangeReceiver:Landroid/content/BroadcastReceiver;

.field private mEasyModeChangeReceiverFilter:Landroid/content/IntentFilter;

.field protected mIsAudioSelector:Z

.field private mIsPaused:Z

.field public mIsWorkingUpdateCategorySize:Z

.field protected mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

.field protected mRunFromSelector:Z

.field protected mSavedInstanceFragmentId:I

.field private mScannerFinishedReceiver:Landroid/content/BroadcastReceiver;

.field private mScannerFinishedReceiverFilter:Landroid/content/IntentFilter;

.field private mSearchMode:Z

.field private mTAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

.field protected mUsbLaunchMode:Z

.field private shortcutItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/ShortcutItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 77
    new-instance v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    .line 91
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mIsPaused:Z

    .line 93
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mBundle:Landroid/os/Bundle;

    .line 95
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mUsbLaunchMode:Z

    .line 447
    return-void
.end method

.method private setEasyModeChangeReceiver()V
    .locals 2

    .prologue
    .line 353
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mEasyModeChangeReceiverFilter:Landroid/content/IntentFilter;

    .line 355
    iget-object v0, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mEasyModeChangeReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "com.android.launcher.action.EASY_MODE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 357
    new-instance v0, Lcom/sec/android/app/myfiles/AbsMainActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/AbsMainActivity$1;-><init>(Lcom/sec/android/app/myfiles/AbsMainActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mEasyModeChangeReceiver:Landroid/content/BroadcastReceiver;

    .line 387
    iget-object v0, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mEasyModeChangeReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mEasyModeChangeReceiverFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 389
    return-void
.end method

.method private setScannerFinisedReceiver()V
    .locals 2

    .prologue
    .line 393
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mScannerFinishedReceiverFilter:Landroid/content/IntentFilter;

    .line 395
    iget-object v0, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mScannerFinishedReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 397
    iget-object v0, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mScannerFinishedReceiverFilter:Landroid/content/IntentFilter;

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 399
    new-instance v0, Lcom/sec/android/app/myfiles/AbsMainActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/AbsMainActivity$2;-><init>(Lcom/sec/android/app/myfiles/AbsMainActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mScannerFinishedReceiver:Landroid/content/BroadcastReceiver;

    .line 428
    return-void
.end method

.method private unregisterScannerFinisedReceiver()V
    .locals 2

    .prologue
    .line 431
    iget-object v1, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mScannerFinishedReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 433
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mScannerFinishedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 438
    :cond_0
    :goto_0
    return-void

    .line 434
    :catch_0
    move-exception v0

    .line 435
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getActionBarManager()Lcom/sec/android/app/myfiles/view/ActionBarManager;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mActionBarManager:Lcom/sec/android/app/myfiles/view/ActionBarManager;

    if-nez v0, :cond_0

    .line 223
    new-instance v0, Lcom/sec/android/app/myfiles/view/ActionBarManager;

    invoke-direct {v0, p0}, Lcom/sec/android/app/myfiles/view/ActionBarManager;-><init>(Lcom/sec/android/app/myfiles/AbsMainActivity;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mActionBarManager:Lcom/sec/android/app/myfiles/view/ActionBarManager;

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mActionBarManager:Lcom/sec/android/app/myfiles/view/ActionBarManager;

    return-object v0
.end method

.method public getActionBarView()Landroid/view/View;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mDefaultActionBarView:Landroid/view/View;

    return-object v0
.end method

.method public getAdapterManager()Lcom/sec/android/app/myfiles/adapter/AdapterManager;
    .locals 4

    .prologue
    .line 232
    const/4 v0, 0x2

    const-string v1, "AbsMainActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AbsMainActivity : getAdapterManager() - mAdapterManager : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mAdapterManager:Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mAdapterManager:Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    if-nez v0, :cond_0

    .line 236
    new-instance v0, Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mRunFromSelector:Z

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/myfiles/adapter/AdapterManager;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mAdapterManager:Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mAdapterManager:Lcom/sec/android/app/myfiles/adapter/AdapterManager;

    return-object v0
.end method

.method public getCloudArrayList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/ShortcutItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 262
    iget-object v0, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->cloudItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getCurrentFragmentID()I
    .locals 1

    .prologue
    .line 444
    const/4 v0, -0x1

    return v0
.end method

.method public getIsAudioSelector()Z
    .locals 1

    .prologue
    .line 107
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mIsAudioSelector:Z

    return v0
.end method

.method protected abstract getMainLayoutResId()I
.end method

.method public getShortcutArrayList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/ShortcutItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 251
    iget-object v0, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->shortcutItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getTAdapter()Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mTAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    return-object v0
.end method

.method public getmDevicesCollection()Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    return-object v0
.end method

.method public getmNearbyDevicesProvider()Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    return-object v0
.end method

.method public abstract initialActionBar()V
.end method

.method public isDeleteDialogWasShowing()Z
    .locals 1

    .prologue
    .line 268
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->isDeleteDialogWasShowing:Z

    return v0
.end method

.method public isPaused()Z
    .locals 1

    .prologue
    .line 191
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mIsPaused:Z

    return v0
.end method

.method public isSearchMode()Z
    .locals 1

    .prologue
    .line 303
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mSearchMode:Z

    return v0
.end method

.method protected abstract loadComponentFromView()V
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 113
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/Window;->requestFeature(I)Z

    .line 115
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 118
    const-string v2, "VerificationLog"

    const-string v3, "onCreate"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 124
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_0

    .line 126
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mBundle:Landroid/os/Bundle;

    .line 128
    iget-object v2, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mBundle:Landroid/os/Bundle;

    if-eqz v2, :cond_0

    const-string v2, "LAUNCH_FROM_USB_STORAGE"

    iget-object v3, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mBundle:Landroid/os/Bundle;

    const-string v4, "LAUNCH_MODE"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 130
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mUsbLaunchMode:Z

    .line 135
    :cond_0
    invoke-static {p0}, Landroid/os/PersonaManager;->getKnoxInfoForApp(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->setKnoxinfoForAppBundle(Landroid/os/Bundle;)V

    .line 137
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    .line 138
    .local v0, "easyModeState":Z
    invoke-static {v0}, Lcom/sec/android/app/myfiles/utils/Utils;->setEasyMode(Z)V

    .line 140
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setEasyModeChangeReceiver()V

    .line 142
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x400

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 143
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->setNotibarAutoShowing(Landroid/app/Activity;)V

    .line 145
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getMainLayoutResId()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setContentView(I)V

    .line 147
    new-instance v2, Lcom/sec/android/app/myfiles/view/ActionBarManager;

    invoke-direct {v2, p0}, Lcom/sec/android/app/myfiles/view/ActionBarManager;-><init>(Lcom/sec/android/app/myfiles/AbsMainActivity;)V

    iput-object v2, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mActionBarManager:Lcom/sec/android/app/myfiles/view/ActionBarManager;

    .line 148
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mRunFromSelector:Z

    .line 150
    invoke-static {p0}, Lcom/sec/android/app/myfiles/utils/Utils;->isMountedStateCheck(Landroid/content/Context;)V

    .line 152
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->loadComponentFromView()V

    .line 154
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->initialActionBar()V

    .line 156
    if-eqz p1, :cond_1

    .line 158
    const-string v2, "currentFragmentId"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mSavedInstanceFragmentId:I

    .line 161
    :cond_1
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mIsAudioSelector:Z

    .line 162
    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mIsWorkingUpdateCategorySize:Z

    .line 164
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mUsbLaunchMode:Z

    if-nez v2, :cond_2

    .line 165
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView()V

    .line 167
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setScannerFinisedReceiver()V

    .line 169
    iget-object v2, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mScannerFinishedReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mScannerFinishedReceiverFilter:Landroid/content/IntentFilter;

    if-eqz v2, :cond_3

    .line 170
    iget-object v2, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mScannerFinishedReceiver:Landroid/content/BroadcastReceiver;

    iget-object v3, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mScannerFinishedReceiverFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 173
    :cond_3
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 197
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 198
    const-string v1, "VerificationLog"

    const-string v2, "onDestory"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    iget-object v1, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mEasyModeChangeReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 202
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mEasyModeChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 208
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->unregisterScannerFinisedReceiver()V

    .line 209
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->clearThumbnailCache()V

    .line 210
    return-void

    .line 203
    :catch_0
    move-exception v0

    .line 204
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 186
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 187
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mIsPaused:Z

    .line 188
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 178
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 179
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mIsPaused:Z

    .line 181
    const-string v0, "VerificationLog"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 4
    .param p1, "level"    # I

    .prologue
    .line 328
    invoke-super {p0, p1}, Landroid/app/Activity;->onTrimMemory(I)V

    .line 329
    const/4 v0, 0x0

    const-string v1, "AbsMainActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AbsMainActivity : onTrimMemory : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 330
    sparse-switch p1, :sswitch_data_0

    .line 349
    :goto_0
    :sswitch_0
    return-void

    .line 341
    :sswitch_1
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/MediaLoader;->clearThumbnailCache()V

    goto :goto_0

    .line 330
    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0xa -> :sswitch_0
        0xf -> :sswitch_0
        0x14 -> :sswitch_0
        0x28 -> :sswitch_1
        0x3c -> :sswitch_1
        0x50 -> :sswitch_1
    .end sparse-switch
.end method

.method protected refreshCategoryTree()V
    .locals 0

    .prologue
    .line 442
    return-void
.end method

.method public setCloudArrayList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/ShortcutItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 256
    .local p1, "mArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/ShortcutItem;>;"
    iput-object p1, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->cloudItems:Ljava/util/ArrayList;

    .line 257
    return-void
.end method

.method public abstract setDefaultView()V
.end method

.method public setDefaultView(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "b"    # Landroid/os/Bundle;

    .prologue
    .line 323
    return-void
.end method

.method public setDeleteDialogWasShowing(Z)V
    .locals 0
    .param p1, "isDeleteDialogWasShowing"    # Z

    .prologue
    .line 274
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->isDeleteDialogWasShowing:Z

    .line 275
    return-void
.end method

.method public setSearchMode(Z)V
    .locals 0
    .param p1, "isSearchMode"    # Z

    .prologue
    .line 308
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mSearchMode:Z

    .line 309
    return-void
.end method

.method public setShortcutArrayList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/element/ShortcutItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 245
    .local p1, "mArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/element/ShortcutItem;>;"
    iput-object p1, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->shortcutItems:Ljava/util/ArrayList;

    .line 246
    return-void
.end method

.method public setTAdapter(Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;)V
    .locals 0
    .param p1, "mTAdapter"    # Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mTAdapter:Lcom/sec/android/app/myfiles/adapter/BrowserTreeViewAdapter;

    .line 104
    return-void
.end method

.method public setmDevicesCollection(Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;)V
    .locals 0
    .param p1, "mDevicesCollection"    # Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    .prologue
    .line 298
    iput-object p1, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    .line 299
    return-void
.end method

.method public setmNearbyDevicesProvider(Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;)V
    .locals 0
    .param p1, "mNearbyDevicesProvider"    # Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    .prologue
    .line 286
    iput-object p1, p0, Lcom/sec/android/app/myfiles/AbsMainActivity;->mNearbyDevicesProvider:Lcom/sec/android/app/myfiles/nearbydevices/NearbyDevicesProvider;

    .line 287
    return-void
.end method
