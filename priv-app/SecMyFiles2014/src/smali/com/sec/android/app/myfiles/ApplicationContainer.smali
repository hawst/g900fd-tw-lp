.class public Lcom/sec/android/app/myfiles/ApplicationContainer;
.super Landroid/app/Application;
.source "ApplicationContainer.java"


# instance fields
.field private mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

.field private mShortcutAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method


# virtual methods
.method public getDevicesCollection()Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ApplicationContainer;->mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    return-object v0
.end method

.method public getShortcutAdapter()Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ApplicationContainer;->mShortcutAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "arg0"    # Landroid/content/res/Configuration;

    .prologue
    .line 47
    invoke-super {p0, p1}, Landroid/app/Application;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 48
    return-void
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 36
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 38
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 0
    .param p1, "arg0"    # I

    .prologue
    .line 42
    invoke-super {p0, p1}, Landroid/app/Application;->onTrimMemory(I)V

    .line 43
    return-void
.end method

.method public setDevicesCollection(Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;)V
    .locals 0
    .param p1, "devicesCollection"    # Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ApplicationContainer;->mDevicesCollection:Lcom/sec/android/app/myfiles/nearbydevices/CategoryAdapterDevicesCollection;

    .line 57
    return-void
.end method

.method public setShortcutAdapter(Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;)V
    .locals 0
    .param p1, "shortcutAdapter"    # Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ApplicationContainer;->mShortcutAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutAdapter;

    .line 65
    return-void
.end method
