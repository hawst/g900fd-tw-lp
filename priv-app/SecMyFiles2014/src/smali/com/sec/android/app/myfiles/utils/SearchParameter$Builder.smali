.class public Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;
.super Ljava/lang/Object;
.source "SearchParameter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/utils/SearchParameter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field mDateFrom:J

.field mDateTo:J

.field mExtentionString:Ljava/lang/String;

.field mFileTypes:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/sec/android/app/myfiles/utils/FileType;",
            ">;"
        }
    .end annotation
.end field

.field mKeyword:Ljava/lang/String;

.field mSearchType:I

.field mStorageType:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/myfiles/utils/SearchParameter;)V
    .locals 2
    .param p1, "param"    # Lcom/sec/android/app/myfiles/utils/SearchParameter;

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    if-eqz p1, :cond_0

    .line 74
    # getter for: Lcom/sec/android/app/myfiles/utils/SearchParameter;->mKeyword:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->access$000(Lcom/sec/android/app/myfiles/utils/SearchParameter;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->mKeyword:Ljava/lang/String;

    .line 75
    # getter for: Lcom/sec/android/app/myfiles/utils/SearchParameter;->mSearchType:I
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->access$100(Lcom/sec/android/app/myfiles/utils/SearchParameter;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->mSearchType:I

    .line 76
    # getter for: Lcom/sec/android/app/myfiles/utils/SearchParameter;->mStorageType:Ljava/util/ArrayList;
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->access$200(Lcom/sec/android/app/myfiles/utils/SearchParameter;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->mStorageType:Ljava/util/ArrayList;

    .line 77
    # getter for: Lcom/sec/android/app/myfiles/utils/SearchParameter;->mFileTypes:Ljava/util/EnumSet;
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->access$300(Lcom/sec/android/app/myfiles/utils/SearchParameter;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->mFileTypes:Ljava/util/EnumSet;

    .line 78
    # getter for: Lcom/sec/android/app/myfiles/utils/SearchParameter;->mDateFrom:J
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->access$400(Lcom/sec/android/app/myfiles/utils/SearchParameter;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->mDateFrom:J

    .line 79
    # getter for: Lcom/sec/android/app/myfiles/utils/SearchParameter;->mDateTo:J
    invoke-static {p1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->access$500(Lcom/sec/android/app/myfiles/utils/SearchParameter;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->mDateTo:J

    .line 81
    :cond_0
    return-void
.end method


# virtual methods
.method public build()Lcom/sec/android/app/myfiles/utils/SearchParameter;
    .locals 2

    .prologue
    .line 84
    new-instance v0, Lcom/sec/android/app/myfiles/utils/SearchParameter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/myfiles/utils/SearchParameter;-><init>(Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;Lcom/sec/android/app/myfiles/utils/SearchParameter$1;)V

    return-object v0
.end method

.method public dateFrom(J)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;
    .locals 1
    .param p1, "dateFrom"    # J

    .prologue
    .line 115
    iput-wide p1, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->mDateFrom:J

    .line 116
    return-object p0
.end method

.method public dateTo(J)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;
    .locals 1
    .param p1, "dateTo"    # J

    .prologue
    .line 120
    iput-wide p1, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->mDateTo:J

    .line 121
    return-object p0
.end method

.method public extentionString(Ljava/lang/String;)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;
    .locals 0
    .param p1, "extentionString"    # Ljava/lang/String;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->mExtentionString:Ljava/lang/String;

    .line 126
    return-object p0
.end method

.method public fileTypes(Ljava/util/EnumSet;)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/sec/android/app/myfiles/utils/FileType;",
            ">;)",
            "Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;"
        }
    .end annotation

    .prologue
    .line 105
    .local p1, "fileTypes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/sec/android/app/myfiles/utils/FileType;>;"
    iput-object p1, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->mFileTypes:Ljava/util/EnumSet;

    .line 106
    return-object p0
.end method

.method public varargs fileTypes([Lcom/sec/android/app/myfiles/utils/FileType;)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;
    .locals 1
    .param p1, "fileTypes"    # [Lcom/sec/android/app/myfiles/utils/FileType;

    .prologue
    .line 98
    array-length v0, p1

    if-lez v0, :cond_0

    .line 99
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-static {v0, p1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;[Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->mFileTypes:Ljava/util/EnumSet;

    .line 101
    :cond_0
    return-object p0
.end method

.method public keyword(Ljava/lang/String;)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;
    .locals 0
    .param p1, "keyword"    # Ljava/lang/String;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->mKeyword:Ljava/lang/String;

    .line 89
    return-object p0
.end method

.method public searchType(I)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;
    .locals 0
    .param p1, "searchType"    # I

    .prologue
    .line 110
    iput p1, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->mSearchType:I

    .line 111
    return-object p0
.end method

.method public storageType(Ljava/util/ArrayList;)Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;"
        }
    .end annotation

    .prologue
    .line 93
    .local p1, "storageType":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/android/app/myfiles/utils/SearchParameter$Builder;->mStorageType:Ljava/util/ArrayList;

    .line 94
    return-object p0
.end method
