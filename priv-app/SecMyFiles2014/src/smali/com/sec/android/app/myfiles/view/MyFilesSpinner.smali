.class public Lcom/sec/android/app/myfiles/view/MyFilesSpinner;
.super Landroid/widget/Spinner;
.source "MyFilesSpinner.java"


# instance fields
.field private mListener:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private mOldPosition:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesSpinner;->mOldPosition:I

    .line 33
    return-void
.end method


# virtual methods
.method public setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/widget/AdapterView$OnItemSelectedListener;

    .prologue
    .line 59
    invoke-super {p0, p1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 61
    iput-object p1, p0, Lcom/sec/android/app/myfiles/view/MyFilesSpinner;->mListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 62
    return-void
.end method

.method public setSelection(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 39
    iget v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesSpinner;->mOldPosition:I

    if-ne v0, p1, :cond_1

    .line 41
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/MyFilesSpinner;->twDismissSpinnerPopup()V

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesSpinner;->mListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/myfiles/view/MyFilesSpinner;->mListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/MyFilesSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v2

    invoke-interface {v2, p1}, Landroid/widget/SpinnerAdapter;->getItemId(I)J

    move-result-wide v4

    move-object v2, p0

    move v3, p1

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemSelectedListener;->onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 54
    :cond_0
    :goto_0
    return-void

    .line 50
    :cond_1
    iput p1, p0, Lcom/sec/android/app/myfiles/view/MyFilesSpinner;->mOldPosition:I

    .line 52
    invoke-super {p0, p1}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_0
.end method
