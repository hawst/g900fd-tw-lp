.class Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$2;
.super Landroid/os/AsyncTask;
.source "BaiduNavigation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->NewOpenAsyncTask()Landroid/os/AsyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;)V
    .locals 0

    .prologue
    .line 587
    iput-object p1, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$2;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 587
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$2;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 590
    const/4 v0, 0x2

    const-string v1, "BaiduNavigation"

    const-string v2, "doInBackground-synclist()"

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 591
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$2;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->synclist(Z)V

    .line 592
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 587
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$2;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 4
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    const/4 v3, 0x2

    .line 597
    const-string v0, "BaiduNavigation"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPostExecute needUpdate: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/app/myfiles/utils/Utils;->needUpdate:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    .line 600
    sget-boolean v0, Lcom/sec/android/app/myfiles/utils/Utils;->needUpdate:Z

    if-eqz v0, :cond_0

    .line 606
    :goto_0
    return-void

    .line 602
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$2;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->access$500(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;)Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 603
    iget-object v0, p0, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation$2;->this$0:Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;

    # getter for: Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->mParentFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;->access$600(Lcom/sec/android/app/myfiles/cloud/baidu/BaiduNavigation;)Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->AsyncOpenCompleted()V

    .line 604
    :cond_1
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 605
    const-string v0, "BaiduNavigation"

    const-string v1, "onPostExecute end"

    invoke-static {v3, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->i(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
