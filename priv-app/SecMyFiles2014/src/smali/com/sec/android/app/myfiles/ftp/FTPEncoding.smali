.class public final enum Lcom/sec/android/app/myfiles/ftp/FTPEncoding;
.super Ljava/lang/Enum;
.source "FTPEncoding.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/myfiles/ftp/FTPEncoding;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

.field public static final enum AUTO:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

.field public static final enum BIG5:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

.field public static final enum BIG5_HKSCS:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

.field public static final enum CESU_8:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

.field public static final enum DOCU_1:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

.field public static final enum UTF_8:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;


# instance fields
.field private mName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 20
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    const-string v1, "AUTO"

    const-string v2, "Auto"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->AUTO:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    .line 21
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    const-string v1, "UTF_8"

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->UTF_8:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    .line 22
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    const-string v1, "BIG5"

    const-string v2, "Big5"

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->BIG5:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    .line 23
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    const-string v1, "BIG5_HKSCS"

    const-string v2, "Big5-HKSCS"

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->BIG5_HKSCS:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    .line 24
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    const-string v1, "DOCU_1"

    const-string v2, "DOCU-1"

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->DOCU_1:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    .line 25
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    const-string v1, "CESU_8"

    const/4 v2, 0x5

    const-string v3, "CESU-8"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->CESU_8:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    .line 19
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->AUTO:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->UTF_8:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->BIG5:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->BIG5_HKSCS:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->DOCU_1:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->CESU_8:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->$VALUES:[Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 28
    iput-object p3, p0, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->mName:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public static getValueOf(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPEncoding;
    .locals 5
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    .line 33
    invoke-static {}, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->values()[Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    move-result-object v0

    .local v0, "arr$":[Lcom/sec/android/app/myfiles/ftp/FTPEncoding;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 34
    .local v3, "val":Lcom/sec/android/app/myfiles/ftp/FTPEncoding;
    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 38
    .end local v3    # "val":Lcom/sec/android/app/myfiles/ftp/FTPEncoding;
    :goto_1
    return-object v3

    .line 33
    .restart local v3    # "val":Lcom/sec/android/app/myfiles/ftp/FTPEncoding;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 38
    .end local v3    # "val":Lcom/sec/android/app/myfiles/ftp/FTPEncoding;
    :cond_1
    sget-object v3, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->AUTO:Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/FTPEncoding;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 19
    const-class v0, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/myfiles/ftp/FTPEncoding;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->$VALUES:[Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    invoke-virtual {v0}, [Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/myfiles/ftp/FTPEncoding;

    return-object v0
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/FTPEncoding;->mName:Ljava/lang/String;

    return-object v0
.end method
