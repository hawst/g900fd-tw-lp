.class Lcom/sec/android/app/myfiles/MainActivity$7;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Landroid/view/ActionMode$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/MainActivity;->updateActionMode(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/MainActivity;

.field final synthetic val$id:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/MainActivity;I)V
    .locals 0

    .prologue
    .line 2162
    iput-object p1, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iput p2, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->val$id:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x1

    .line 2521
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iput-boolean v2, v0, Lcom/sec/android/app/myfiles/MainActivity;->mAddShortcutFromActionmode:Z

    .line 2523
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0f014c

    if-ne v0, v1, :cond_1

    .line 2525
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # setter for: Lcom/sec/android/app/myfiles/MainActivity;->mSelectionTypeChange:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/myfiles/MainActivity;->access$1102(Lcom/sec/android/app/myfiles/MainActivity;Z)Z

    .line 2526
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/myfiles/MainActivity;->handleMenuItemAdvanced(I)V

    .line 2543
    :cond_0
    :goto_0
    return v2

    .line 2529
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v0, :cond_0

    .line 2530
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 2531
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v0, v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    if-eqz v0, :cond_0

    .line 2532
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 2534
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->clickShortcutRename()V

    goto :goto_0

    .line 2537
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->clickShortcutEditFtp()V

    goto :goto_0

    .line 2532
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0f0128 -> :sswitch_0
        0x7f0f0132 -> :sswitch_1
    .end sparse-switch
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 12
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    const/16 v8, -0x14

    const v7, 0x7f0f0003

    const/4 v11, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 2166
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iput-object p1, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionMode:Landroid/view/ActionMode;

    .line 2168
    iget v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->val$id:I

    sparse-switch v4, :sswitch_data_0

    .line 2416
    :goto_0
    return v9

    .line 2170
    :sswitch_0
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iput-boolean v9, v4, Lcom/sec/android/app/myfiles/MainActivity;->mIsSearchMode:Z

    .line 2171
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    new-instance v5, Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/MainActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    sget v8, Lcom/sec/android/app/myfiles/MainActivity;->FRAGMENT_LAYOUT_ID:I

    invoke-direct {v5, v6, v7, v8}, Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;-><init>(Landroid/app/FragmentManager;Landroid/content/Context;I)V

    iput-object v5, v4, Lcom/sec/android/app/myfiles/MainActivity;->mSearchFragmentManager:Lcom/sec/android/app/myfiles/fragment/SearchFragmentManager;

    .line 2172
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/MainActivity;->access$300(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2173
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/MainActivity;->access$300(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f04000b

    invoke-virtual {v4, v5, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 2177
    .local v1, "customView":Landroid/view/View;
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/MainActivity;->access$300(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2178
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v4

    const v5, 0x7f0e0017

    invoke-virtual {v4, v5, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2179
    iget-object v5, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v4, 0x7f0f000e

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v5, Lcom/sec/android/app/myfiles/MainActivity;->mAdvancedSearchResult:Landroid/widget/TextView;

    .line 2181
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v4, 0x7f0f0014

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/SearchView;

    iput-object v4, v5, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    .line 2182
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    new-instance v5, Lcom/sec/android/app/myfiles/MainActivity$7$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/myfiles/MainActivity$7$1;-><init>(Lcom/sec/android/app/myfiles/MainActivity$7;)V

    const-wide/16 v6, 0x96

    invoke-virtual {v4, v5, v6, v7}, Landroid/widget/SearchView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2194
    iget-object v5, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v4, 0x7f0f0011

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionModeCustom:Landroid/widget/LinearLayout;

    .line 2196
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionModeCustom:Landroid/widget/LinearLayout;

    new-instance v5, Lcom/sec/android/app/myfiles/MainActivity$7$2;

    invoke-direct {v5, p0}, Lcom/sec/android/app/myfiles/MainActivity$7$2;-><init>(Lcom/sec/android/app/myfiles/MainActivity$7;)V

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2207
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    iget-object v4, v4, Landroid/widget/SearchView;->mQueryTextView:Landroid/widget/SearchView$SearchAutoComplete;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/MainActivity;->access$300(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v11}, Lcom/sec/android/app/myfiles/utils/Utils;->getEditTextFilter(Landroid/content/Context;I)[Landroid/text/InputFilter;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/SearchView$SearchAutoComplete;->setFilters([Landroid/text/InputFilter;)V

    .line 2208
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mQueryTextListener:Landroid/widget/SearchView$OnQueryTextListener;

    invoke-virtual {v4, v5}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    .line 2210
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v4}, Landroid/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const-string v5, "android:id/search_src_text"

    invoke-virtual {v4, v5, v10, v10}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 2211
    .local v3, "editTextId":I
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/MainActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v4, v3}, Landroid/widget/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 2212
    .local v2, "editBoxView":Landroid/view/View;
    if-eqz v2, :cond_1

    instance-of v4, v2, Landroid/widget/EditText;

    if-eqz v4, :cond_1

    .line 2214
    check-cast v2, Landroid/widget/EditText;

    .end local v2    # "editBoxView":Landroid/view/View;
    invoke-virtual {v2, v9}, Landroid/widget/EditText;->getInputExtras(Z)Landroid/os/Bundle;

    move-result-object v0

    .line 2215
    .local v0, "b":Landroid/os/Bundle;
    const-string v4, "maxLength"

    const/16 v5, 0x32

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2217
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    # setter for: Lcom/sec/android/app/myfiles/MainActivity;->mCalendar:Ljava/util/Calendar;
    invoke-static {v4, v5}, Lcom/sec/android/app/myfiles/MainActivity;->access$702(Lcom/sec/android/app/myfiles/MainActivity;Ljava/util/Calendar;)Ljava/util/Calendar;

    .line 2218
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mCalendar:Ljava/util/Calendar;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/MainActivity;->access$700(Lcom/sec/android/app/myfiles/MainActivity;)Ljava/util/Calendar;

    move-result-object v4

    const/4 v5, 0x6

    const/4 v6, -0x7

    invoke-virtual {v4, v5, v6}, Ljava/util/Calendar;->add(II)V

    .line 2220
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/MainActivity;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getSearchFromDate()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/MainActivity;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getSearchToDate()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_2

    .line 2222
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/MainActivity;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchToDate(J)V

    .line 2223
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/MainActivity;->mSharedDataStore:Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mCalendar:Ljava/util/Calendar;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/MainActivity;->access$700(Lcom/sec/android/app/myfiles/MainActivity;)Ljava/util/Calendar;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->setSearchFromDate(J)V

    .line 2226
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iput v9, v4, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentSearchType:I

    .line 2228
    invoke-virtual {p1, v1}, Landroid/view/ActionMode;->setCustomView(Landroid/view/View;)V

    .line 2229
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # invokes: Lcom/sec/android/app/myfiles/MainActivity;->initSearchMenu(Landroid/view/View;)V
    invoke-static {v4, v1}, Lcom/sec/android/app/myfiles/MainActivity;->access$800(Lcom/sec/android/app/myfiles/MainActivity;Landroid/view/View;)V

    goto/16 :goto_0

    .line 2175
    .end local v1    # "customView":Landroid/view/View;
    .end local v3    # "editTextId":I
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/MainActivity;->access$300(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f04000a

    invoke-virtual {v4, v5, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .restart local v1    # "customView":Landroid/view/View;
    goto/16 :goto_1

    .line 2236
    .end local v1    # "customView":Landroid/view/View;
    :sswitch_1
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/MainActivity;->access$300(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const/high16 v5, 0x7f040000

    invoke-virtual {v4, v5, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 2237
    .restart local v1    # "customView":Landroid/view/View;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/MainActivity;->access$300(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2238
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v4

    const v5, 0x7f0e0002

    invoke-virtual {v4, v5, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2242
    :goto_2
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iput-boolean v9, v4, Lcom/sec/android/app/myfiles/MainActivity;->blockSendtoKNOX:Z

    .line 2244
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v5, 0x7f0f0001

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllContainer:Landroid/view/View;

    .line 2246
    iget-object v5, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v5, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllButton:Landroid/widget/TextView;

    .line 2248
    iget-object v5, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v4, 0x7f0f0002

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    .line 2250
    iget-object v5, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v6, 0x7f0f0047

    invoke-virtual {v4, v6}, Lcom/sec/android/app/myfiles/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, v5, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllContainer:Landroid/widget/RelativeLayout;

    .line 2252
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllContainer:Landroid/widget/RelativeLayout;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2254
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllContainer:Landroid/view/View;

    new-instance v5, Lcom/sec/android/app/myfiles/MainActivity$7$3;

    invoke-direct {v5, p0}, Lcom/sec/android/app/myfiles/MainActivity$7$3;-><init>(Lcom/sec/android/app/myfiles/MainActivity$7;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2281
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v5, 0x7f0f0120

    invoke-interface {p2, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeShare:Landroid/view/MenuItem;

    .line 2282
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v5, 0x7f0f0121

    invoke-interface {p2, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    .line 2283
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v5, 0x7f0f0122

    invoke-interface {p2, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeMove:Landroid/view/MenuItem;

    .line 2284
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v5, 0x7f0f0123

    invoke-interface {p2, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeCopy:Landroid/view/MenuItem;

    .line 2285
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v5, 0x7f0f0124

    invoke-interface {p2, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionmoveMoveToPrivate:Landroid/view/MenuItem;

    .line 2286
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v5, 0x7f0f0125

    invoke-interface {p2, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionmoveRemoveFromPrivate:Landroid/view/MenuItem;

    .line 2287
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v5, 0x7f0f0128

    invoke-interface {p2, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeRename:Landroid/view/MenuItem;

    .line 2288
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v5, 0x7f0f012e

    invoke-interface {p2, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeLock:Landroid/view/MenuItem;

    .line 2289
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v5, 0x7f0f012f

    invoke-interface {p2, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeUnlock:Landroid/view/MenuItem;

    .line 2291
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/MainActivity;->access$300(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 2292
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v5, 0x7f0f012b

    invoke-interface {p2, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeZip:Landroid/view/MenuItem;

    .line 2293
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v5, 0x7f0f012c

    invoke-interface {p2, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeExtract:Landroid/view/MenuItem;

    .line 2294
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v5, 0x7f0f012d

    invoke-interface {p2, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeExtractHere:Landroid/view/MenuItem;

    .line 2296
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v5, 0x7f0f0130

    invoke-interface {p2, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDetail:Landroid/view/MenuItem;

    .line 2297
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v5, 0x7f0f0131

    invoke-interface {p2, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDownload:Landroid/view/MenuItem;

    .line 2298
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v5, 0x7f0f0132

    invoke-interface {p2, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeFtpEdit:Landroid/view/MenuItem;

    .line 2299
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v5, 0x7f0f0126

    invoke-interface {p2, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeMoveToKNOX:Landroid/view/MenuItem;

    .line 2300
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v5, 0x7f0f0127

    invoke-interface {p2, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeRemoveFromKNOX:Landroid/view/MenuItem;

    .line 2301
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v5, 0x7f0f0129

    invoke-interface {p2, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeAddShortcut:Landroid/view/MenuItem;

    .line 2302
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v5, 0x7f0f012a

    invoke-interface {p2, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeAddShortcutHome:Landroid/view/MenuItem;

    .line 2304
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    new-instance v5, Landroid/widget/ListPopupWindow;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/app/myfiles/MainActivity;->access$300(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllListPopupWindow:Landroid/widget/ListPopupWindow;
    invoke-static {v4, v5}, Lcom/sec/android/app/myfiles/MainActivity;->access$902(Lcom/sec/android/app/myfiles/MainActivity;Landroid/widget/ListPopupWindow;)Landroid/widget/ListPopupWindow;

    .line 2306
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllListPopupWindow:Landroid/widget/ListPopupWindow;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/MainActivity;->access$900(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/widget/ListPopupWindow;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllButton:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    .line 2308
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllListPopupWindow:Landroid/widget/ListPopupWindow;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/MainActivity;->access$900(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/widget/ListPopupWindow;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/widget/ListPopupWindow;->setVerticalOffset(I)V

    .line 2310
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllListPopupWindow:Landroid/widget/ListPopupWindow;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/MainActivity;->access$900(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/widget/ListPopupWindow;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/widget/ListPopupWindow;->setModal(Z)V

    .line 2312
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v4, v10}, Lcom/sec/android/app/myfiles/MainActivity;->setSpinnerWidth(Ljava/lang/String;)V

    .line 2314
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v4, v9, v9}, Lcom/sec/android/app/myfiles/MainActivity;->setSelectOptionsVisibility(ZZ)V

    .line 2316
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllListPopupWindow:Landroid/widget/ListPopupWindow;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/MainActivity;->access$900(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/widget/ListPopupWindow;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # invokes: Lcom/sec/android/app/myfiles/MainActivity;->createSelectOptionClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/MainActivity;->access$1000(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2318
    invoke-virtual {p1, v1}, Landroid/view/ActionMode;->setCustomView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 2240
    :cond_5
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v4

    const v5, 0x7f0e0001

    invoke-virtual {v4, v5, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    goto/16 :goto_2

    .line 2325
    .end local v1    # "customView":Landroid/view/View;
    :sswitch_2
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/MainActivity;->access$300(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const/high16 v5, 0x7f040000

    invoke-virtual {v4, v5, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 2326
    .restart local v1    # "customView":Landroid/view/View;
    iget v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->val$id:I

    const/4 v5, 0x4

    if-ne v4, v5, :cond_9

    .line 2327
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v4

    const v5, 0x7f0e0019

    invoke-virtual {v4, v5, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2336
    :goto_3
    iget-object v5, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v4, 0x7f0f0011

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionModeCustom:Landroid/widget/LinearLayout;

    .line 2338
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionModeCustom:Landroid/widget/LinearLayout;

    if-eqz v4, :cond_6

    .line 2339
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionModeCustom:Landroid/widget/LinearLayout;

    new-instance v5, Lcom/sec/android/app/myfiles/MainActivity$7$4;

    invoke-direct {v5, p0}, Lcom/sec/android/app/myfiles/MainActivity$7$4;-><init>(Lcom/sec/android/app/myfiles/MainActivity$7;)V

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2350
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v5, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllButton:Landroid/widget/TextView;

    .line 2352
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    new-instance v5, Landroid/widget/ListPopupWindow;

    iget-object v6, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/app/myfiles/MainActivity;->access$300(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllListPopupWindow:Landroid/widget/ListPopupWindow;
    invoke-static {v4, v5}, Lcom/sec/android/app/myfiles/MainActivity;->access$902(Lcom/sec/android/app/myfiles/MainActivity;Landroid/widget/ListPopupWindow;)Landroid/widget/ListPopupWindow;

    .line 2354
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllListPopupWindow:Landroid/widget/ListPopupWindow;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/MainActivity;->access$900(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/widget/ListPopupWindow;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v5, v5, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllButton:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    .line 2356
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllListPopupWindow:Landroid/widget/ListPopupWindow;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/MainActivity;->access$900(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/widget/ListPopupWindow;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/widget/ListPopupWindow;->setVerticalOffset(I)V

    .line 2358
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllListPopupWindow:Landroid/widget/ListPopupWindow;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/MainActivity;->access$900(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/widget/ListPopupWindow;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/widget/ListPopupWindow;->setModal(Z)V

    .line 2360
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v4, v10}, Lcom/sec/android/app/myfiles/MainActivity;->setSpinnerWidth(Ljava/lang/String;)V

    .line 2362
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v4, v9, v9}, Lcom/sec/android/app/myfiles/MainActivity;->setSelectOptionsVisibility(ZZ)V

    .line 2364
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllListPopupWindow:Landroid/widget/ListPopupWindow;
    invoke-static {v4}, Lcom/sec/android/app/myfiles/MainActivity;->access$900(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/widget/ListPopupWindow;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # invokes: Lcom/sec/android/app/myfiles/MainActivity;->createSelectOptionClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    invoke-static {v5}, Lcom/sec/android/app/myfiles/MainActivity;->access$1000(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2366
    iget-object v5, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v5, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllButton:Landroid/widget/TextView;

    .line 2367
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v5, 0x7f0f0001

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllContainer:Landroid/view/View;

    .line 2368
    iget-object v5, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v4, 0x7f0f0002

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, v5, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllCheckBox:Landroid/widget/ImageView;

    .line 2370
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v5, 0x7f0f0144

    invoke-interface {p2, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    .line 2371
    iget v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->val$id:I

    const/4 v5, 0x6

    if-ne v4, v5, :cond_7

    .line 2373
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeDelete:Landroid/view/MenuItem;

    invoke-interface {v4, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2376
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionbarSelectAllContainer:Landroid/view/View;

    new-instance v5, Lcom/sec/android/app/myfiles/MainActivity$7$5;

    invoke-direct {v5, p0}, Lcom/sec/android/app/myfiles/MainActivity$7$5;-><init>(Lcom/sec/android/app/myfiles/MainActivity$7;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2410
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v4, v4, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    if-eqz v4, :cond_8

    .line 2412
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    check-cast v4, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v4}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->setAllItemCount()V

    .line 2414
    :cond_8
    invoke-virtual {p1, v1}, Landroid/view/ActionMode;->setCustomView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 2329
    :cond_9
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v4

    const v5, 0x7f0e001a

    invoke-virtual {v4, v5, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 2330
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v5, 0x7f0f0128

    invoke-interface {p2, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeRename:Landroid/view/MenuItem;

    .line 2331
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const v5, 0x7f0f0132

    invoke-interface {p2, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeFtpEdit:Landroid/view/MenuItem;

    .line 2332
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeRename:Landroid/view/MenuItem;

    invoke-interface {v4, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2333
    iget-object v4, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v4, v4, Lcom/sec/android/app/myfiles/MainActivity;->mActionmodeFtpEdit:Landroid/view/MenuItem;

    invoke-interface {v4, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_3

    .line 2168
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_2
        0x5 -> :sswitch_1
        0x6 -> :sswitch_2
        0x11 -> :sswitch_0
    .end sparse-switch
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 5
    .param p1, "mode"    # Landroid/view/ActionMode;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 2547
    iget v1, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->val$id:I

    sparse-switch v1, :sswitch_data_0

    .line 2582
    :goto_0
    return-void

    .line 2549
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/myfiles/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 2550
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isInputMethodShown()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2551
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    .line 2554
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v1, v1, Lcom/sec/android/app/myfiles/MainActivity;->mSearchFromAdvance:Z

    if-eqz v1, :cond_2

    .line 2555
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    if-eqz v1, :cond_2

    .line 2556
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/fragment/DropboxSearchFragment;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/fragment/NearbyDevicesSearchFragment;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduSearchFragment;

    if-eqz v1, :cond_2

    .line 2558
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->onBackPressed()Z

    .line 2562
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iput-object v4, v1, Lcom/sec/android/app/myfiles/MainActivity;->mActionMode:Landroid/view/ActionMode;

    .line 2563
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iput-boolean v3, v1, Lcom/sec/android/app/myfiles/MainActivity;->mIsSearchMode:Z

    .line 2564
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iput-boolean v3, v1, Lcom/sec/android/app/myfiles/MainActivity;->mSearchFromAdvance:Z

    goto :goto_0

    .line 2569
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllContainer:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_3

    .line 2570
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/MainActivity;->mSelectAllContainer:Landroid/widget/RelativeLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2575
    :cond_3
    :sswitch_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->isSelectMode()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2576
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mFragment:Lcom/sec/android/app/myfiles/fragment/AbsFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/AbsFragment;->finishSelectMode()V

    .line 2578
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iput-object v4, v1, Lcom/sec/android/app/myfiles/MainActivity;->mActionMode:Landroid/view/ActionMode;

    .line 2579
    iget-object v1, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iput-boolean v3, v1, Lcom/sec/android/app/myfiles/MainActivity;->mAddShortcutFromActionmode:Z

    goto/16 :goto_0

    .line 2547
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_2
        0x5 -> :sswitch_1
        0x6 -> :sswitch_2
        0x11 -> :sswitch_0
    .end sparse-switch
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 7
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    const v6, 0x7f0f0129

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2421
    const/4 v1, 0x0

    .line 2423
    .local v1, "menuItem":Landroid/view/MenuItem;
    const/4 v0, 0x0

    .line 2424
    .local v0, "CurrentTag":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    if-eqz v2, :cond_0

    .line 2425
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mCurrentFragment:Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;

    iget v2, v2, Lcom/sec/android/app/myfiles/AbsMainActivity$FragmentState;->mId:I

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->getFragmentTagByID(I)Ljava/lang/String;

    move-result-object v0

    .line 2428
    :cond_0
    if-eqz v0, :cond_2

    const-string v2, "downloaded_apps"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "ftp"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "dropbox"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "nearby_devices"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2430
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iput-boolean v5, v2, Lcom/sec/android/app/myfiles/MainActivity;->blockSendtoKNOX:Z

    .line 2433
    :cond_2
    iget v2, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->val$id:I

    sparse-switch v2, :sswitch_data_0

    .line 2516
    :cond_3
    :goto_0
    return v5

    .line 2436
    :sswitch_0
    const v2, 0x7f0f014c

    invoke-interface {p2, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2437
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mSearchFromAdvance:Z

    if-eqz v2, :cond_4

    .line 2438
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 2440
    :cond_4
    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 2446
    :sswitch_1
    const v2, 0x7f0f0126

    invoke-interface {p2, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 2447
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->blockSendtoKNOX:Z

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/MainActivity;->access$300(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isKNOXFileRelayAvailable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/MainActivity;->access$300(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isKNOXMode(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 2448
    :cond_5
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2454
    :cond_6
    :goto_1
    const v2, 0x7f0f0127

    invoke-interface {p2, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 2455
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->blockSendtoKNOX:Z

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/MainActivity;->access$300(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isKNOXFileRelayAvailable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/MainActivity;->access$300(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isKNOXMode(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_11

    .line 2456
    :cond_7
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2461
    :cond_8
    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/MainActivity;->access$300(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isEasyMode(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_9

    invoke-interface {p2, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 2462
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/MainActivity;->access$300(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v5}, Lcom/sec/android/app/myfiles/utils/Utils;->getShortCutCount(Landroid/content/Context;Z)I

    move-result v2

    const/16 v3, 0x32

    if-ne v2, v3, :cond_9

    .line 2463
    invoke-interface {p2, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2467
    :cond_9
    const v2, 0x7f0f0124

    invoke-interface {p2, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_a

    .line 2469
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/MainActivity;->access$300(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/privatemode/PrivateModeManager;->isReady(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_13

    if-eqz v0, :cond_13

    const-string v2, "nearby_devices"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    .line 2470
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mEnableMoveToPrivate:Z

    if-eqz v2, :cond_12

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mDisableMoveToPrivate:Z

    if-nez v2, :cond_12

    .line 2471
    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2481
    :cond_a
    :goto_3
    const v2, 0x7f0f0125

    invoke-interface {p2, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_b

    .line 2482
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    # getter for: Lcom/sec/android/app/myfiles/MainActivity;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/MainActivity;->access$300(Lcom/sec/android/app/myfiles/MainActivity;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/privatemode/PrivateModeManager;->isReady(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_15

    if-eqz v0, :cond_15

    const-string v2, "nearby_devices"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    .line 2483
    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mEnableMoveToPrivate:Z

    if-nez v2, :cond_14

    iget-object v2, p0, Lcom/sec/android/app/myfiles/MainActivity$7;->this$0:Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v2, v2, Lcom/sec/android/app/myfiles/MainActivity;->mDisableMoveToPrivate:Z

    if-eqz v2, :cond_14

    .line 2484
    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2493
    :cond_b
    :goto_4
    if-eqz v0, :cond_3

    const-string v2, "history"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2494
    const v2, 0x7f0f0128

    invoke-interface {p2, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_c

    .line 2495
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2498
    :cond_c
    const v2, 0x7f0f012b

    invoke-interface {p2, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_d

    .line 2499
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2502
    :cond_d
    const v2, 0x7f0f0122

    invoke-interface {p2, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_e

    .line 2503
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2506
    :cond_e
    const v2, 0x7f0f0123

    invoke-interface {p2, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_f

    .line 2507
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2509
    :cond_f
    invoke-interface {p2, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2510
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    .line 2450
    :cond_10
    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_1

    .line 2458
    :cond_11
    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_2

    .line 2473
    :cond_12
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_3

    .line 2477
    :cond_13
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_3

    .line 2486
    :cond_14
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_4

    .line 2489
    :cond_15
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_4

    .line 2433
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x11 -> :sswitch_0
    .end sparse-switch
.end method
