.class Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$5;
.super Ljava/lang/Object;
.source "CreateFolderFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->clickOK()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

.field final synthetic val$currentPath:Ljava/lang/String;

.field final synthetic val$newFolderName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 306
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    iput-object p2, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$5;->val$currentPath:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$5;->val$newFolderName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 311
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 313
    .local v0, "activity":Landroid/app/Activity;
    const-string v3, "CreateFolderFragment"

    const-string v4, "Baidu Create Folder"

    invoke-static {v5, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 314
    if-eqz v0, :cond_0

    .line 316
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;

    move-result-object v1

    .line 318
    .local v1, "baiduAPIHelper":Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;
    const/4 v2, 0x0

    .line 320
    .local v2, "folder":Lcom/samsung/scloud/data/SCloudFolder;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$5;->val$currentPath:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 321
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$5;->val$newFolderName:Ljava/lang/String;

    const-string v4, "/"

    invoke-virtual {v1, v3, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->createFolder(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;

    move-result-object v2

    .line 325
    :goto_0
    if-eqz v2, :cond_2

    .line 327
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 328
    const-string v3, "CreateFolderFragment"

    const-string v4, "Baidu MESSAGE_CREATEFOLDER_SUCCESS"

    invoke-static {v5, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 351
    .end local v1    # "baiduAPIHelper":Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;
    .end local v2    # "folder":Lcom/samsung/scloud/data/SCloudFolder;
    :cond_0
    :goto_1
    return-void

    .line 323
    .restart local v1    # "baiduAPIHelper":Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;
    .restart local v2    # "folder":Lcom/samsung/scloud/data/SCloudFolder;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$5;->val$newFolderName:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$5;->val$currentPath:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Lcom/sec/android/app/myfiles/cloud/baidu/BaiduActivity$BaiduAPIHelper;->createFolder(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;

    move-result-object v2

    goto :goto_0

    .line 331
    :cond_2
    const-string v3, "CreateFolderFragment"

    const-string v4, "Baidu creation_failed"

    invoke-static {v5, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->w(ILjava/lang/String;Ljava/lang/String;)I

    .line 332
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 334
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$5;->this$0:Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$5$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$5$1;-><init>(Lcom/sec/android/app/myfiles/fragment/CreateFolderFragment$5;)V

    invoke-virtual {v3, v4}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_1
.end method
