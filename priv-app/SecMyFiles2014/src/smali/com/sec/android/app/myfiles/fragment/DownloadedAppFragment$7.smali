.class Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$7;
.super Ljava/lang/Object;
.source "DownloadedAppFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)V
    .locals 0

    .prologue
    .line 880
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 885
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->isSelectMode()Z

    move-result v3

    if-nez v3, :cond_2

    .line 887
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->getSelectModeFrom()I

    move-result v3

    if-nez v3, :cond_0

    .line 888
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->isSelectMode()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 889
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    if-eqz v3, :cond_0

    .line 890
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->finishSelectMode()V

    .line 894
    :cond_0
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;

    .line 896
    .local v1, "item":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;->getLaunchIntent(Landroid/content/pm/PackageManager;)Landroid/content/Intent;

    move-result-object v2

    .line 898
    .local v2, "launchIntent":Landroid/content/Intent;
    if-eqz v2, :cond_1

    .line 902
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    invoke-virtual {v3, v2}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 915
    .end local v1    # "item":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    .end local v2    # "launchIntent":Landroid/content/Intent;
    :cond_1
    :goto_0
    return-void

    .line 903
    .restart local v1    # "item":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    .restart local v2    # "launchIntent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 905
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const/4 v3, 0x2

    const-string v4, "DownloadedAppFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "downloadedapps"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 911
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v1    # "item":Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter$AppEntry;
    .end local v2    # "launchIntent":Landroid/content/Intent;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment$7;->this$0:Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;
    invoke-static {v3}, Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/DownloadedAppFragment;)Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;

    move-result-object v3

    invoke-virtual {v3, p3}, Lcom/sec/android/app/myfiles/adapter/DownloadedAppAdapter;->multipleSelect(I)V

    goto :goto_0
.end method
