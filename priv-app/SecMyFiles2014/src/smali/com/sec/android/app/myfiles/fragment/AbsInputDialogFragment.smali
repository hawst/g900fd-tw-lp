.class public abstract Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;
.super Landroid/app/DialogFragment;
.source "AbsInputDialogFragment.java"


# static fields
.field private static final MODULE:Ljava/lang/String; = "AbsInputDialogFragment"


# instance fields
.field protected cur_operation:I

.field protected mCancelButton:Landroid/widget/Button;

.field protected mCur_folder:Ljava/lang/String;

.field protected mExistZipFile:Ljava/io/File;

.field protected mInputEditText:Landroid/widget/EditText;

.field protected mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

.field private mNewPathLenth:I

.field protected mOkButton:Landroid/widget/Button;

.field private mPrePathLenth:I

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field protected mTargetFragmentTag:Ljava/lang/String;

.field protected mZipFileName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method private setAbortReceiver()V
    .locals 3

    .prologue
    .line 353
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 355
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 357
    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 359
    new-instance v1, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment$4;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;)V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 396
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 398
    return-void
.end method


# virtual methods
.method protected onClickSearchCallback(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "result"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 335
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 336
    .local v1, "activity":Landroid/app/Activity;
    if-eqz v1, :cond_0

    .line 340
    :try_start_0
    move-object v0, v1

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    move-object v2, v0

    .line 341
    .local v2, "mActivity":Lcom/sec/android/app/myfiles/MainActivity;
    invoke-virtual {v2, p1, p2, p3}, Lcom/sec/android/app/myfiles/MainActivity;->onActivityResult(IILandroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 349
    .end local v2    # "mActivity":Lcom/sec/android/app/myfiles/MainActivity;
    :cond_0
    :goto_0
    return-void

    .line 347
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 169
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->setTargetFragmentAgain()V

    .line 170
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 171
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 176
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 178
    const/4 v0, 0x2

    const-string v1, "AbsInputDialogFragment"

    const-string v2, "onCreate "

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 180
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    .line 182
    if-eqz p1, :cond_0

    const-string v0, "fragmenttag"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 183
    const-string v0, "fragmenttag"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mTargetFragmentTag:Ljava/lang/String;

    .line 186
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->setAbortReceiver()V

    .line 187
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 206
    const/4 v6, 0x2

    const-string v7, "AbsInputDialogFragment"

    const-string v8, "onCreateDialog "

    invoke-static {v6, v7, v8}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 208
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "title"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 209
    .local v5, "titleResId":I
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "kind_of_operation"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->cur_operation:I

    .line 210
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "current_folder"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mCur_folder:Ljava/lang/String;

    .line 211
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "zipfilename"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mZipFileName:Ljava/lang/String;

    .line 213
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v1, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 215
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 217
    .local v4, "inflater":Landroid/view/LayoutInflater;
    const v6, 0x7f040022

    const/4 v7, 0x0

    invoke-virtual {v4, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 219
    .local v2, "customView":Landroid/view/View;
    const v6, 0x7f0f00a3

    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/EditText;

    iput-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mInputEditText:Landroid/widget/EditText;

    .line 221
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mCur_folder:Ljava/lang/String;

    if-eqz v6, :cond_4

    .line 222
    new-instance v6, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mCur_folder:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mZipFileName:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".zip"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mExistZipFile:Ljava/io/File;

    .line 223
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mExistZipFile:Ljava/io/File;

    .line 224
    .local v3, "existZipFile":Ljava/io/File;
    iget v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->cur_operation:I

    const/4 v7, 0x5

    if-ne v6, v7, :cond_2

    .line 225
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mExistZipFile:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mPrePathLenth:I

    .line 227
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mExistZipFile:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 229
    new-instance v6, Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v7, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/io/File;

    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mCur_folder:Ljava/lang/String;

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/myfiles/utils/Utils;->getZipItem(Ljava/lang/String;[Ljava/io/File;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mExistZipFile:Ljava/io/File;

    .line 233
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mExistZipFile:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mNewPathLenth:I

    goto :goto_0

    .line 237
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mInputEditText:Landroid/widget/EditText;

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mExistZipFile:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 252
    .end local v3    # "existZipFile":Ljava/io/File;
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mInputEditText:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->selectAll()V

    .line 258
    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 260
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 262
    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->setupDialog(Landroid/app/AlertDialog$Builder;)V

    .line 264
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mInputEditText:Landroid/widget/EditText;

    invoke-virtual {p0, v6}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->setupInputEditText(Landroid/widget/EditText;)V

    .line 267
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mInputEditText:Landroid/widget/EditText;

    if-eqz v6, :cond_1

    .line 269
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mInputEditText:Landroid/widget/EditText;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->getInputExtras(Z)Landroid/os/Bundle;

    move-result-object v0

    .line 270
    .local v0, "b":Landroid/os/Bundle;
    const-string v6, "maxLength"

    const/16 v7, 0x32

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 274
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_1
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    return-object v6

    .line 240
    .restart local v3    # "existZipFile":Ljava/io/File;
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "ftp_folder_name"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 241
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mInputEditText:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "ftp_folder_name"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/myfiles/utils/Utils;->getFtpNewFolderItem(Landroid/content/Context;Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 244
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mInputEditText:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    new-instance v8, Ljava/io/File;

    iget-object v9, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mCur_folder:Ljava/lang/String;

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/myfiles/utils/Utils;->getNewFolderItem(Landroid/content/Context;[Ljava/io/File;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 249
    .end local v3    # "existZipFile":Ljava/io/File;
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mInputEditText:Landroid/widget/EditText;

    const v7, 0x7f0b001a

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setText(I)V

    goto :goto_1
.end method

.method public onPause()V
    .locals 4

    .prologue
    .line 81
    const/4 v1, 0x0

    const-string v2, "AbsInputDialogFragment"

    const-string v3, "onPause"

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 82
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->setTargetFragmentAgain()V

    .line 84
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    :goto_0
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 89
    return-void

    .line 85
    :catch_0
    move-exception v0

    .line 86
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 280
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 282
    const-string v0, "AbsInputDialogFragment"

    const-string v1, "onResume"

    invoke-static {v2, v0, v1}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mInputEditText:Landroid/widget/EditText;

    if-nez v0, :cond_0

    .line 331
    :goto_0
    return-void

    .line 287
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mInputEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 289
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 292
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mInputEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 293
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mInputEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 295
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isAccessoryKeyboardState()I

    move-result v0

    if-lez v0, :cond_2

    .line 296
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mInputEditText:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment$1;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 316
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mInputEditText:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment$3;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 329
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->setTargetFragmentAgain()V

    .line 330
    invoke-direct {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->setAbortReceiver()V

    goto :goto_0

    .line 306
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mInputEditText:Landroid/widget/EditText;

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment$2;-><init>(Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 192
    const/4 v0, 0x2

    const-string v1, "AbsInputDialogFragment"

    const-string v2, "onSaveInstanceState "

    invoke-static {v0, v1, v2}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 194
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->setTargetFragmentAgain()V

    .line 196
    const-string v0, "fragmenttag"

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mTargetFragmentTag:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 199
    return-void
.end method

.method public setTargetFragment(Landroid/app/Fragment;I)V
    .locals 1
    .param p1, "fragment"    # Landroid/app/Fragment;
    .param p2, "requestCode"    # I

    .prologue
    .line 158
    if-eqz p1, :cond_0

    .line 160
    invoke-virtual {p1}, Landroid/app/Fragment;->getTag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->setTargetFragmentTag(Ljava/lang/String;)V

    .line 162
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/DialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 163
    return-void
.end method

.method protected setTargetFragmentAgain()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    .line 98
    const/4 v5, 0x0

    const-string v6, "AbsInputDialogFragment"

    const-string v7, "setTargetFragmentAgain start"

    invoke-static {v5, v6, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 100
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    .line 102
    .local v2, "fm":Landroid/app/FragmentManager;
    if-nez v2, :cond_0

    .line 103
    const-string v5, "AbsInputDialogFragment"

    const-string v6, "setTargetFragmentAgain fm is null !!"

    invoke-static {v8, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->dismiss()V

    .line 152
    :goto_0
    return-void

    .line 108
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v4

    .line 110
    .local v4, "targetFragment":Landroid/app/Fragment;
    if-eqz v4, :cond_3

    .line 112
    invoke-virtual {v4}, Landroid/app/Fragment;->getTag()Ljava/lang/String;

    move-result-object v3

    .line 114
    .local v3, "tag":Ljava/lang/String;
    if-nez v3, :cond_1

    .line 116
    const-string v5, "AbsInputDialogFragment"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setTargetFragmentAgain tag == null ,mTargetFragmentTag =  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mTargetFragmentTag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v8, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 118
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mTargetFragmentTag:Ljava/lang/String;

    .line 122
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 124
    .local v0, "TargetFragment":Landroid/app/Fragment;
    if-eqz v0, :cond_2

    .line 128
    const-string v5, "AbsInputDialogFragment"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setTargetFragmentAgain setTargetFragment done! TargetFragment =  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v8, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 130
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->getTargetRequestCode()I

    move-result v5

    invoke-virtual {p0, v0, v5}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    goto :goto_0

    .line 134
    :cond_2
    const-string v5, "AbsInputDialogFragment"

    const-string v6, " targetFragment info is  null  dismiss"

    invoke-static {v8, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 138
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->dismissAllowingStateLoss()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 140
    :catch_0
    move-exception v1

    .line 142
    .local v1, "ex":Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 148
    .end local v0    # "TargetFragment":Landroid/app/Fragment;
    .end local v1    # "ex":Ljava/lang/IllegalStateException;
    .end local v3    # "tag":Ljava/lang/String;
    :cond_3
    const-string v5, "AbsInputDialogFragment"

    const-string v6, "setTargetFragmentAgain targetFragment is null"

    invoke-static {v8, v5, v6}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setTargetFragmentTag(Ljava/lang/String;)V
    .locals 0
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/AbsInputDialogFragment;->mTargetFragmentTag:Ljava/lang/String;

    .line 94
    return-void
.end method

.method protected abstract setupDialog(Landroid/app/AlertDialog$Builder;)V
.end method

.method protected abstract setupInputEditText(Landroid/widget/EditText;)V
.end method
