.class public Lcom/sec/android/app/myfiles/navigation/UserShortcutNaigation;
.super Lcom/sec/android/app/myfiles/navigation/AbsNavigation;
.source "UserShortcutNaigation.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;-><init>(Landroid/content/Context;I)V

    .line 29
    return-void
.end method


# virtual methods
.method public AsyncOpen()V
    .locals 0

    .prologue
    .line 111
    return-void
.end method

.method public getFilesInCurrentFolder()Landroid/database/Cursor;
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 81
    iget-object v5, p0, Lcom/sec/android/app/myfiles/navigation/UserShortcutNaigation;->mContext:Landroid/content/Context;

    if-nez v5, :cond_0

    .line 82
    const/4 v5, 0x0

    .line 98
    :goto_0
    return-object v5

    .line 84
    :cond_0
    const/4 v1, 0x1

    .line 85
    .local v1, "enableDropbox":Z
    const/4 v2, 0x1

    .line 86
    .local v2, "enableFtp":Z
    iget-object v5, p0, Lcom/sec/android/app/myfiles/navigation/UserShortcutNaigation;->mContext:Landroid/content/Context;

    check-cast v5, Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 87
    .local v4, "intent":Landroid/content/Intent;
    if-eqz v4, :cond_2

    .line 88
    const-string v5, "SELECTOR_SOURCE_TYPE"

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 89
    .local v3, "fragmentId":I
    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 90
    .local v0, "action":Ljava/lang/String;
    if-eq v3, v6, :cond_2

    const-string v5, "com.sec.android.app.myfiles.PICK_SELECT_PATH"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 91
    const/16 v5, 0x8

    if-eq v3, v5, :cond_1

    const/16 v5, 0x1f

    if-ne v3, v5, :cond_3

    .line 92
    :cond_1
    const/4 v2, 0x0

    .line 98
    .end local v0    # "action":Ljava/lang/String;
    .end local v3    # "fragmentId":I
    :cond_2
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/myfiles/navigation/UserShortcutNaigation;->mContext:Landroid/content/Context;

    invoke-static {v5, v1, v2}, Lcom/sec/android/app/myfiles/utils/Utils;->getUserShortcutCursor(Landroid/content/Context;ZZ)Landroid/database/Cursor;

    move-result-object v5

    goto :goto_0

    .line 93
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v3    # "fragmentId":I
    :cond_3
    const/16 v5, 0xa

    if-ne v3, v5, :cond_2

    .line 94
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public getFilesInFolder(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/navigation/UserShortcutNaigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getFilesInFolder(Ljava/lang/String;I)Landroid/database/Cursor;
    .locals 2
    .param p1, "folderPath"    # Ljava/lang/String;
    .param p2, "showFolderFileType"    # I

    .prologue
    .line 68
    iget v1, p0, Lcom/sec/android/app/myfiles/navigation/UserShortcutNaigation;->mShowFolderFileType:I

    .line 70
    .local v1, "prevShowFolderFileType":I
    iput p2, p0, Lcom/sec/android/app/myfiles/navigation/UserShortcutNaigation;->mShowFolderFileType:I

    .line 72
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/navigation/UserShortcutNaigation;->getFilesInFolder(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 74
    .local v0, "c":Landroid/database/Cursor;
    iput v1, p0, Lcom/sec/android/app/myfiles/navigation/UserShortcutNaigation;->mShowFolderFileType:I

    .line 76
    return-object v0
.end method

.method public goBack()Z
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x1

    return v0
.end method

.method public goTo(Ljava/lang/String;Ljava/lang/Boolean;)Z
    .locals 1
    .param p1, "folderPath"    # Ljava/lang/String;
    .param p2, "isBack"    # Ljava/lang/Boolean;

    .prologue
    .line 40
    const/4 v0, 0x0

    return v0
.end method

.method public goToRoot(Ljava/lang/Boolean;)Z
    .locals 1
    .param p1, "isBack"    # Ljava/lang/Boolean;

    .prologue
    .line 46
    const/4 v0, 0x1

    return v0
.end method

.method public goUp()Z
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x1

    return v0
.end method

.method public isCurrentFolderRoot()Z
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x1

    return v0
.end method

.method public refreshNavigation()V
    .locals 0

    .prologue
    .line 105
    return-void
.end method

.method public setCurrentPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 117
    return-void
.end method
