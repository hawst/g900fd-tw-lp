.class Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$3;
.super Ljava/lang/Object;
.source "CategoryHomeFragment.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)V
    .locals 0

    .prologue
    .line 404
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I
    .param p6, "oldLeft"    # I
    .param p7, "oldTop"    # I
    .param p8, "oldRight"    # I
    .param p9, "oldBottom"    # I

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x2

    const/4 v3, 0x1

    .line 429
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mScreenState:I
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)I

    move-result v1

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getScreenState(Landroid/content/Context;)I

    move-result v1

    if-eq v1, v4, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mScreenState:I
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)I

    move-result v1

    if-ne v1, v4, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getScreenState(Landroid/content/Context;)I

    move-result v1

    if-ne v1, v3, :cond_4

    .line 431
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iput-boolean v3, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->isResetNeeded:Z

    .line 433
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 435
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "shortcut_mode_enable_intent_key"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->isSelectMode()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 437
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v1, :cond_2

    .line 438
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v1, v1, Lcom/sec/android/app/myfiles/MainActivity;->mIsSearchMode:Z

    if-eqz v1, :cond_2

    .line 440
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/MainActivity;

    iput-boolean v3, v1, Lcom/sec/android/app/myfiles/MainActivity;->mIsSearchRetainForPen:Z

    .line 444
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView(Landroid/os/Bundle;)V

    .line 464
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_3
    :goto_0
    return-void

    .line 447
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mScreenState:I
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)I

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getScreenState(Landroid/content/Context;)I

    move-result v1

    if-eq v1, v2, :cond_6

    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mScreenState:I
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$300(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)I

    move-result v1

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/myfiles/utils/Utils;->getScreenState(Landroid/content/Context;)I

    move-result v1

    if-nez v1, :cond_3

    .line 449
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iput-boolean v3, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->isResetNeeded:Z

    .line 451
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 453
    .restart local v0    # "b":Landroid/os/Bundle;
    const-string v1, "shortcut_mode_enable_intent_key"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mShortcutListAdapter:Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;)Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/adapter/ShortcutListAdapter;->isSelectMode()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 455
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    instance-of v1, v1, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v1, :cond_7

    .line 456
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/MainActivity;

    iget-boolean v1, v1, Lcom/sec/android/app/myfiles/MainActivity;->mIsSearchMode:Z

    if-eqz v1, :cond_7

    .line 458
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    check-cast v1, Lcom/sec/android/app/myfiles/MainActivity;

    iput-boolean v3, v1, Lcom/sec/android/app/myfiles/MainActivity;->mIsSearchRetainForPen:Z

    .line 462
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment$3;->this$0:Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/fragment/CategoryHomeFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView(Landroid/os/Bundle;)V

    goto :goto_0
.end method
