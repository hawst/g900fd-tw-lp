.class public Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;
.super Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;
.source "CopyMoveLocalToFtpOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation$1;
    }
.end annotation


# instance fields
.field private volatile mDoneCount:I

.field private final mMove:Z

.field private mParam:Lcom/sec/android/app/myfiles/element/FileOperationParam;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "operation"    # I
    .param p3, "contentType"    # I
    .param p4, "progressView"    # Landroid/view/View;
    .param p5, "ftpHandlerParams"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 36
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;-><init>(Landroid/content/Context;IILandroid/view/View;Ljava/lang/String;)V

    .line 234
    iput v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mDoneCount:I

    .line 38
    if-ne p2, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mMove:Z

    .line 40
    return-void

    :cond_0
    move v0, v1

    .line 38
    goto :goto_0
.end method

.method private copyMoveFilesWithoutFolderStructure(Lcom/sec/android/app/myfiles/element/FileOperationParam;)V
    .locals 7
    .param p1, "param"    # Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .prologue
    const/4 v6, 0x0

    .line 95
    iget-object v4, p1, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    iget-object v5, p1, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 97
    .local v0, "files":[Ljava/lang/String;
    array-length v3, v0

    .line 99
    .local v3, "totalFiles":I
    iget-object v4, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    iget-object v5, p1, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    invoke-virtual {p0, v4, v0, v5, v6}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->uploadFiles(Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 101
    .local v1, "filesUploaded":I
    if-ne v1, v3, :cond_1

    sget-object v4, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    :goto_0
    iput-object v4, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 103
    iget-boolean v4, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mMove:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    sget-object v5, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    if-ne v4, v5, :cond_0

    .line 105
    invoke-static {v6, v0}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->deleteItemsOnLocal([Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 107
    .local v2, "itemsDeleted":I
    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->sendScansToParents([Ljava/lang/String;)V

    .line 109
    if-ne v2, v3, :cond_2

    sget-object v4, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    :goto_1
    iput-object v4, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 113
    .end local v2    # "itemsDeleted":I
    :cond_0
    iput v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mDoneCount:I

    .line 115
    return-void

    .line 101
    :cond_1
    sget-object v4, Lcom/sec/android/app/myfiles/ftp/FTPErr;->PARTIALLY_DONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    goto :goto_0

    .line 109
    .restart local v2    # "itemsDeleted":I
    :cond_2
    sget-object v4, Lcom/sec/android/app/myfiles/ftp/FTPErr;->PARTIALLY_DONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    goto :goto_1
.end method

.method private copyMovePreservingFolderStructure(Lcom/sec/android/app/myfiles/element/FileOperationParam;)V
    .locals 11
    .param p1, "param"    # Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .prologue
    .line 119
    iget-object v8, p1, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    iget-object v9, p1, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    new-array v9, v9, [Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Ljava/lang/String;

    invoke-static {v8}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->discoverLocalStructure([Ljava/lang/String;)Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FoldersFilesTuple;

    move-result-object v7

    .line 122
    .local v7, "tpl":Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FoldersFilesTuple;
    if-eqz v7, :cond_1

    .line 124
    iget-object v1, v7, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FoldersFilesTuple;->mFiles:[Ljava/lang/String;

    .line 126
    .local v1, "files":[Ljava/lang/String;
    iget-object v3, v7, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FoldersFilesTuple;->mFolders:[Ljava/lang/String;

    .line 128
    .local v3, "folders":[Ljava/lang/String;
    if-eqz v1, :cond_1

    if-eqz v3, :cond_1

    .line 130
    array-length v5, v1

    .line 132
    .local v5, "totalFiles":I
    array-length v6, v3

    .line 134
    .local v6, "totalFolders":I
    iget-object v8, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    iget-object v9, p1, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mSrcFolder:Ljava/lang/String;

    iget-object v10, p1, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    invoke-virtual {p0, v8, v3, v9, v10}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->createDirectoriesOnFtp(Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 136
    .local v0, "directoriesCreated":I
    iget-object v8, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    iget-object v9, p1, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    iget-object v10, p1, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mSrcFolder:Ljava/lang/String;

    invoke-virtual {p0, v8, v1, v9, v10}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->uploadFiles(Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 138
    .local v2, "filesUploaded":I
    if-ne v0, v6, :cond_2

    if-ne v2, v5, :cond_2

    sget-object v8, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    :goto_0
    iput-object v8, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 141
    iget-boolean v8, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mMove:Z

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    sget-object v9, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    if-ne v8, v9, :cond_0

    .line 143
    iget-object v8, v7, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation$FoldersFilesTuple;->mFiles:[Ljava/lang/String;

    invoke-static {v3, v8}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->deleteItemsOnLocal([Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    .line 145
    .local v4, "itemsDeleted":I
    add-int v8, v5, v6

    if-ne v4, v8, :cond_3

    sget-object v8, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    :goto_1
    iput-object v8, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 148
    iget-object v8, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mContext:Landroid/content/Context;

    iget-object v9, p1, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mSrcFolder:Ljava/lang/String;

    invoke-static {v8, v9}, Lcom/sec/android/app/myfiles/utils/Utils;->sendScan(Landroid/content/Context;Ljava/lang/String;)V

    .line 152
    .end local v4    # "itemsDeleted":I
    :cond_0
    iget-object v8, p1, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetDatas:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    iput v8, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mDoneCount:I

    .line 158
    .end local v0    # "directoriesCreated":I
    .end local v1    # "files":[Ljava/lang/String;
    .end local v2    # "filesUploaded":I
    .end local v3    # "folders":[Ljava/lang/String;
    .end local v5    # "totalFiles":I
    .end local v6    # "totalFolders":I
    :cond_1
    return-void

    .line 138
    .restart local v0    # "directoriesCreated":I
    .restart local v1    # "files":[Ljava/lang/String;
    .restart local v2    # "filesUploaded":I
    .restart local v3    # "folders":[Ljava/lang/String;
    .restart local v5    # "totalFiles":I
    .restart local v6    # "totalFolders":I
    :cond_2
    sget-object v8, Lcom/sec/android/app/myfiles/ftp/FTPErr;->PARTIALLY_DONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    goto :goto_0

    .line 145
    .restart local v4    # "itemsDeleted":I
    :cond_3
    sget-object v8, Lcom/sec/android/app/myfiles/ftp/FTPErr;->PARTIALLY_DONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    goto :goto_1
.end method


# virtual methods
.method protected cancelOperation()V
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-interface {v0}, Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;->abortTransfer()V

    .line 165
    invoke-super {p0}, Lcom/sec/android/app/myfiles/ftp/operation/AbsFtpOperation;->cancelOperation()V

    .line 167
    return-void
.end method

.method protected varargs doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;
    .locals 3
    .param p1, "args"    # [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .prologue
    .line 52
    const/4 v1, 0x1

    invoke-static {v1, p1}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->checkFileOperationParam(Z[Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/ftp/FTPErr;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 54
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    sget-object v2, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    if-ne v1, v2, :cond_0

    .line 56
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mFtpHandler:Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->checkConnection(Lcom/sec/android/app/myfiles/ftp/IFTPConnectionHandler;)Lcom/sec/android/app/myfiles/ftp/FTPErr;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    .line 60
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    sget-object v2, Lcom/sec/android/app/myfiles/ftp/FTPErr;->NONE:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    if-ne v1, v2, :cond_1

    .line 62
    const/4 v1, 0x0

    aget-object v0, p1, v1

    .line 64
    .local v0, "param":Lcom/sec/android/app/myfiles/element/FileOperationParam;
    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mParam:Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .line 66
    iget v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mSourceFragmentId:I

    sparse-switch v1, :sswitch_data_0

    .line 89
    .end local v0    # "param":Lcom/sec/android/app/myfiles/element/FileOperationParam;
    :cond_1
    :goto_0
    const/4 v1, 0x0

    return-object v1

    .line 71
    .restart local v0    # "param":Lcom/sec/android/app/myfiles/element/FileOperationParam;
    :sswitch_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->copyMovePreservingFolderStructure(Lcom/sec/android/app/myfiles/element/FileOperationParam;)V

    goto :goto_0

    .line 81
    :sswitch_1
    invoke-direct {p0, v0}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->copyMoveFilesWithoutFolderStructure(Lcom/sec/android/app/myfiles/element/FileOperationParam;)V

    goto :goto_0

    .line 66
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_1
        0x3 -> :sswitch_1
        0x4 -> :sswitch_1
        0x5 -> :sswitch_1
        0x12 -> :sswitch_0
        0x28 -> :sswitch_1
        0x201 -> :sswitch_0
    .end sparse-switch
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 31
    check-cast p1, [Lcom/sec/android/app/myfiles/element/FileOperationParam;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->doInBackground([Lcom/sec/android/app/myfiles/element/FileOperationParam;)Lcom/sec/android/app/myfiles/element/FileOperationResult;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V
    .locals 4
    .param p1, "result"    # Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .prologue
    .line 172
    sget-object v1, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation$1;->$SwitchMap$com$sec$android$app$myfiles$ftp$FTPErr:[I

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mFtpErr:Lcom/sec/android/app/myfiles/ftp/FTPErr;

    invoke-virtual {v2}, Lcom/sec/android/app/myfiles/ftp/FTPErr;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 210
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    if-eqz v1, :cond_0

    .line 212
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mDoneCount:I

    invoke-interface {v1, v2, v3}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;->onCancelled(Ljava/util/ArrayList;I)V

    .line 216
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mMove:Z

    if-eqz v1, :cond_5

    .line 218
    const v1, 0x7f0b00cb

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->showToast(I)V

    .line 230
    :cond_1
    :goto_0
    return-void

    .line 176
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    if-eqz v1, :cond_1

    .line 178
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 180
    .local v0, "arg":Landroid/os/Bundle;
    const-string v2, "FILE_OPERATION"

    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mMove:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 182
    const-string v1, "src_fragment_id"

    const/16 v2, 0x201

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 184
    const-string v1, "dest_fragment_id"

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 186
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mParam:Lcom/sec/android/app/myfiles/element/FileOperationParam;

    if-eqz v1, :cond_2

    .line 188
    const-string v1, "target_folder"

    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mParam:Lcom/sec/android/app/myfiles/element/FileOperationParam;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/element/FileOperationParam;->mTargetFolder:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    :cond_2
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mMove:Z

    if-eqz v1, :cond_4

    .line 194
    const v1, 0x7f0c0008

    iget v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mDoneCount:I

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->showToastPrurals(II)V

    .line 202
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mFileOperationListener:Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;

    iget v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mDoneCount:I

    iget v3, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mDoneCount:I

    invoke-interface {v1, v2, v3, v0}, Lcom/sec/android/app/myfiles/fileoperation/FileOperationListener;->onCompleted(IILandroid/os/Bundle;)V

    goto :goto_0

    .line 180
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 198
    :cond_4
    const v1, 0x7f0c0005

    iget v2, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mDoneCount:I

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->showToastPrurals(II)V

    goto :goto_2

    .line 222
    .end local v0    # "arg":Landroid/os/Bundle;
    :cond_5
    const v1, 0x7f0b00c9

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->showToast(I)V

    goto :goto_0

    .line 172
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 31
    check-cast p1, Lcom/sec/android/app/myfiles/element/FileOperationResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->onPostExecute(Lcom/sec/android/app/myfiles/element/FileOperationResult;)V

    return-void
.end method

.method protected setupProgressView()V
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/CopyMoveLocalToFtpOperation;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 47
    return-void
.end method
