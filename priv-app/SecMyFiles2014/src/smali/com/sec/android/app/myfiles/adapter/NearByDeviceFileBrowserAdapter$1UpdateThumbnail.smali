.class Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1UpdateThumbnail;
.super Ljava/lang/Object;
.source "NearByDeviceFileBrowserAdapter.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/nearbydevices/DrawableUpdatable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->handleSetIcon(Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;Ljava/lang/String;Landroid/widget/AdapterView;IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "UpdateThumbnail"
.end annotation


# instance fields
.field private final mImageView:Landroid/widget/ImageView;

.field private mThumbUri:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

.field final synthetic val$isVideo:Z

.field final synthetic val$viewHolder:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;Landroid/widget/ImageView;Ljava/lang/String;ZLcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;)V
    .locals 1
    .param p2, "imageView"    # Landroid/widget/ImageView;
    .param p3, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ImageView;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 470
    iput-object p1, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1UpdateThumbnail;->this$0:Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    iput-boolean p4, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1UpdateThumbnail;->val$isVideo:Z

    iput-object p5, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1UpdateThumbnail;->val$viewHolder:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 472
    iput-object p2, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1UpdateThumbnail;->mImageView:Landroid/widget/ImageView;

    .line 473
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1UpdateThumbnail;->mThumbUri:Ljava/lang/String;

    .line 475
    return-void
.end method


# virtual methods
.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 499
    iget-object v0, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1UpdateThumbnail;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public setDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 4
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 480
    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1UpdateThumbnail;->mThumbUri:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 481
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1UpdateThumbnail;->this$0:Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mImageOnTheFront:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->access$200(Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;)Ljava/util/HashMap;

    move-result-object v2

    monitor-enter v2

    .line 483
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1UpdateThumbnail;->this$0:Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mImageOnTheFront:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->access$200(Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;)Ljava/util/HashMap;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1UpdateThumbnail;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 485
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1UpdateThumbnail;->this$0:Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mImageOnTheFront:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->access$200(Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;)Ljava/util/HashMap;

    move-result-object v1

    iget-object v3, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1UpdateThumbnail;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 486
    .local v0, "uri":Ljava/lang/String;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1UpdateThumbnail;->mThumbUri:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 487
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1UpdateThumbnail;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 488
    iget-boolean v1, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1UpdateThumbnail;->val$isVideo:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1UpdateThumbnail;->val$viewHolder:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 489
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1UpdateThumbnail;->val$viewHolder:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;

    iget-object v1, v1, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter$ViewHolder;->mOverlay:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 492
    .end local v0    # "uri":Ljava/lang/String;
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 493
    iget-object v1, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1UpdateThumbnail;->this$0:Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;

    # getter for: Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->mThumbnailCache:Landroid/util/LruCache;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;->access$100(Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter;)Landroid/util/LruCache;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/adapter/NearByDeviceFileBrowserAdapter$1UpdateThumbnail;->mThumbUri:Ljava/lang/String;

    invoke-virtual {v1, v2, p1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 495
    :cond_1
    return-void

    .line 492
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
