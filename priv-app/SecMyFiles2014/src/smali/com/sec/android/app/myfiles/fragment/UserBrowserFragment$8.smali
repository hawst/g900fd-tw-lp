.class Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$8;
.super Landroid/content/BroadcastReceiver;
.source "UserBrowserFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->setExternalStorageReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;)V
    .locals 0

    .prologue
    .line 1285
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1290
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1291
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 1293
    .local v2, "uri":Landroid/net/Uri;
    const/4 v3, 0x2

    const-string v4, "UserBrowserFragment"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Broadcast receive : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " , uri : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 1295
    const-string v3, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1298
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    if-eqz v3, :cond_3

    .line 1301
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    instance-of v3, v3, Lcom/sec/android/app/myfiles/SelectorActivity;

    if-eqz v3, :cond_1

    .line 1304
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v1

    .line 1306
    .local v1, "path":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 1309
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->finish()V

    .line 1315
    .end local v1    # "path":Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->refreshNavigation()V

    .line 1317
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v3

    const-string v4, "/storage"

    if-eq v3, v4, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    iget-object v3, v3, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getCurrentFolder()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/myfiles/utils/Utils;->mInternalRoot:Ljava/lang/String;

    if-ne v3, v4, :cond_3

    .line 1319
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->isSelectMode()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1321
    iget-object v3, p0, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment$8;->this$0:Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;

    invoke-virtual {v3}, Lcom/sec/android/app/myfiles/fragment/UserBrowserFragment;->finishSelectMode()V

    .line 1329
    :cond_3
    return-void
.end method
