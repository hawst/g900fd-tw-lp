.class Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;
.super Landroid/os/AsyncTask;
.source "SearchFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fragment/SearchFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SearchAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Landroid/database/Cursor;",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private param:Lcom/sec/android/app/myfiles/utils/SearchParameter;

.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/myfiles/fragment/SearchFragment;Lcom/sec/android/app/myfiles/utils/SearchParameter;)V
    .locals 0
    .param p2, "param"    # Lcom/sec/android/app/myfiles/utils/SearchParameter;

    .prologue
    .line 1047
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1048
    iput-object p2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->param:Lcom/sec/android/app/myfiles/utils/SearchParameter;

    .line 1049
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/database/Cursor;
    .locals 13
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    const/4 v10, 0x2

    .line 1069
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 1070
    .local v5, "dataSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSearchNavigation:Lcom/sec/android/app/myfiles/navigation/SearchNavigation;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->param:Lcom/sec/android/app/myfiles/utils/SearchParameter;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->getSearchCursor(Lcom/sec/android/app/myfiles/utils/SearchParameter;)Landroid/database/Cursor;

    move-result-object v2

    .line 1072
    .local v2, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x0

    .line 1074
    .local v0, "arrayCursor":[Landroid/database/Cursor;
    if-eqz v2, :cond_3

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v7

    if-lez v7, :cond_3

    .line 1075
    new-array v0, v10, [Landroid/database/Cursor;

    .line 1076
    aput-object v2, v0, v11

    .line 1079
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    iget v7, v7, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mViewMode:I

    if-ne v7, v10, :cond_3

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v7

    if-nez v7, :cond_3

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1083
    :cond_0
    const-string v4, ""

    .line 1084
    .local v4, "data":Ljava/lang/String;
    const-string v7, "_data"

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .local v6, "index":I
    const/4 v7, -0x1

    if-eq v6, v7, :cond_1

    .line 1086
    invoke-interface {v2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1088
    :cond_1
    if-eqz v4, :cond_2

    const-string v7, ""

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 1089
    invoke-interface {v5, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1091
    :cond_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-nez v7, :cond_0

    .line 1094
    .end local v4    # "data":Ljava/lang/String;
    .end local v6    # "index":I
    :cond_3
    const/4 v1, 0x0

    .line 1095
    .local v1, "contentCursor":Landroid/database/Cursor;
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->param:Lcom/sec/android/app/myfiles/utils/SearchParameter;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getSearchType()I

    move-result v7

    if-eq v7, v10, :cond_4

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->param:Lcom/sec/android/app/myfiles/utils/SearchParameter;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getSearchType()I

    move-result v7

    const/4 v8, 0x6

    if-ne v7, v8, :cond_8

    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->param:Lcom/sec/android/app/myfiles/utils/SearchParameter;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getStorageType()Ljava/util/ArrayList;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    iget-object v8, v8, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v9, 0x7f0b003e

    invoke-virtual {v8, v9}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_8

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->param:Lcom/sec/android/app/myfiles/utils/SearchParameter;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getStorageType()Ljava/util/ArrayList;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    iget-object v8, v8, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v9, 0x7f0b003f

    invoke-virtual {v8, v9}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_8

    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->param:Lcom/sec/android/app/myfiles/utils/SearchParameter;

    invoke-virtual {v7}, Lcom/sec/android/app/myfiles/utils/SearchParameter;->getStorageType()Ljava/util/ArrayList;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    iget-object v8, v8, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v9, 0x7f0b0151

    invoke-virtual {v8, v9}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_8

    .line 1101
    const/4 v1, 0x0

    .line 1107
    :goto_0
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    iget v7, v7, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mViewMode:I

    if-ne v7, v10, :cond_5

    if-eqz v1, :cond_5

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v7

    if-lez v7, :cond_5

    .line 1109
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    # invokes: Lcom/sec/android/app/myfiles/fragment/SearchFragment;->createThumbnailCursor(Landroid/database/Cursor;Ljava/util/Set;)Landroid/database/Cursor;
    invoke-static {v7, v1, v5}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->access$500(Lcom/sec/android/app/myfiles/fragment/SearchFragment;Landroid/database/Cursor;Ljava/util/Set;)Landroid/database/Cursor;

    move-result-object v1

    .line 1111
    :cond_5
    if-eqz v1, :cond_a

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v7

    if-lez v7, :cond_a

    .line 1112
    if-eqz v0, :cond_9

    .line 1113
    aput-object v1, v0, v12

    .line 1114
    new-instance v2, Landroid/database/MergeCursor;

    .end local v2    # "cursor":Landroid/database/Cursor;
    invoke-direct {v2, v0}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .line 1125
    .restart local v2    # "cursor":Landroid/database/Cursor;
    :cond_6
    :goto_1
    instance-of v7, v2, Lcom/sec/android/app/myfiles/SortCursor;

    if-eqz v7, :cond_7

    .line 1126
    new-instance v3, Landroid/database/MergeCursor;

    new-array v7, v12, [Landroid/database/Cursor;

    aput-object v2, v7, v11

    invoke-direct {v3, v7}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    .end local v2    # "cursor":Landroid/database/Cursor;
    .local v3, "cursor":Landroid/database/Cursor;
    move-object v2, v3

    .line 1129
    .end local v3    # "cursor":Landroid/database/Cursor;
    .restart local v2    # "cursor":Landroid/database/Cursor;
    :cond_7
    return-object v2

    .line 1103
    :cond_8
    iget-object v7, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mSearchNavigation:Lcom/sec/android/app/myfiles/navigation/SearchNavigation;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->param:Lcom/sec/android/app/myfiles/utils/SearchParameter;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/myfiles/navigation/SearchNavigation;->getContentSearchCursor(Lcom/sec/android/app/myfiles/utils/SearchParameter;)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_0

    .line 1116
    :cond_9
    move-object v2, v1

    goto :goto_1

    .line 1118
    :cond_a
    if-eqz v1, :cond_6

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v7

    if-nez v7, :cond_6

    .line 1119
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 1044
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->doInBackground([Ljava/lang/Void;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "result"    # Landroid/database/Cursor;

    .prologue
    .line 1134
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/SearchFragment;->dialog:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/SearchFragment;)Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1135
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/SearchFragment;->dialog:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/SearchFragment;)Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->dismissWaitProgressDialog()V

    .line 1138
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mClearFocusAfterSearch:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->access$602(Lcom/sec/android/app/myfiles/fragment/SearchFragment;Z)Z

    .line 1139
    return-void
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1044
    check-cast p1, Landroid/database/Cursor;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->onCancelled(Landroid/database/Cursor;)V

    return-void
.end method

.method protected onPostExecute(Landroid/database/Cursor;)V
    .locals 4
    .param p1, "result"    # Landroid/database/Cursor;

    .prologue
    .line 1143
    if-eqz p1, :cond_0

    .line 1144
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    # invokes: Lcom/sec/android/app/myfiles/fragment/SearchFragment;->goSearch(Landroid/database/Cursor;)V
    invoke-static {v0, p1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->access$700(Lcom/sec/android/app/myfiles/fragment/SearchFragment;Landroid/database/Cursor;)V

    .line 1146
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/SearchFragment;->dialog:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/SearchFragment;)Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1147
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/SearchFragment;->dialog:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/SearchFragment;)Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->dismissWaitProgressDialog()V

    .line 1150
    :cond_1
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 1151
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/android/app/myfiles/MainActivity;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    iget-object v0, v0, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mAdapter:Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/adapter/AbsBrowserAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 1152
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/myfiles/MainActivity;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/MainActivity;->updateEmptySearchView()V

    .line 1154
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mClearFocusAfterSearch:Z
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->access$600(Lcom/sec/android/app/myfiles/fragment/SearchFragment;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1155
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask$1;-><init>(Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1175
    :goto_0
    return-void

    .line 1173
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mClearFocusAfterSearch:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->access$602(Lcom/sec/android/app/myfiles/fragment/SearchFragment;Z)Z

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1044
    check-cast p1, Landroid/database/Cursor;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->onPostExecute(Landroid/database/Cursor;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .prologue
    .line 1053
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1065
    :cond_0
    :goto_0
    return-void

    .line 1056
    :cond_1
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 1057
    const/4 v0, 0x1

    .line 1058
    .local v0, "showDialog":Z
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/SearchFragment;->isSearchRenameorDeleteRefresh:Z
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->access$100(Lcom/sec/android/app/myfiles/fragment/SearchFragment;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1059
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/myfiles/fragment/SearchFragment;->isSearchRenameorDeleteRefresh:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->access$102(Lcom/sec/android/app/myfiles/fragment/SearchFragment;Z)Z

    .line 1060
    const/4 v0, 0x0

    .line 1062
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/SearchFragment;->dialog:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/SearchFragment;)Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/SearchFragment;->dialog:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/SearchFragment;)Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->isShowing()Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    .line 1063
    iget-object v1, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    # getter for: Lcom/sec/android/app/myfiles/fragment/SearchFragment;->dialog:Lcom/sec/android/app/myfiles/utils/MyFilesDialog;
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->access$400(Lcom/sec/android/app/myfiles/fragment/SearchFragment;)Lcom/sec/android/app/myfiles/utils/MyFilesDialog;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fragment/SearchFragment$SearchAsyncTask;->this$0:Lcom/sec/android/app/myfiles/fragment/SearchFragment;

    iget-object v2, v2, Lcom/sec/android/app/myfiles/fragment/SearchFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v3, 0x7f0b006b

    invoke-virtual {v2, v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesDialog;->showWaitProgressDialog(Ljava/lang/String;Z)V

    goto :goto_0
.end method
