.class Lcom/sec/android/app/myfiles/SearchActivity$2;
.super Landroid/content/BroadcastReceiver;
.source "SearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/SearchActivity;->registerBroadcastReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/SearchActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/SearchActivity;)V
    .locals 0

    .prologue
    .line 222
    iput-object p1, p0, Lcom/sec/android/app/myfiles/SearchActivity$2;->this$0:Lcom/sec/android/app/myfiles/SearchActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x2

    .line 226
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 227
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 229
    .local v1, "uri":Landroid/net/Uri;
    const-string v2, "SearchActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Broadcast receive : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " , uri : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    .line 230
    const-string v2, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 232
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SearchActivity$2;->this$0:Lcom/sec/android/app/myfiles/SearchActivity;

    # getter for: Lcom/sec/android/app/myfiles/SearchActivity;->mrefreshHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/SearchActivity;->access$100(Lcom/sec/android/app/myfiles/SearchActivity;)Landroid/os/Handler;

    move-result-object v2

    const-wide/16 v4, 0x12c

    invoke-virtual {v2, v6, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 233
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SearchActivity$2;->this$0:Lcom/sec/android/app/myfiles/SearchActivity;

    invoke-static {v2}, Lcom/sec/android/app/myfiles/utils/Utils;->isMountedStateCheck(Landroid/content/Context;)V

    .line 234
    iget-object v2, p0, Lcom/sec/android/app/myfiles/SearchActivity$2;->this$0:Lcom/sec/android/app/myfiles/SearchActivity;

    # invokes: Lcom/sec/android/app/myfiles/SearchActivity;->refreshFragmentsLocation()V
    invoke-static {v2}, Lcom/sec/android/app/myfiles/SearchActivity;->access$000(Lcom/sec/android/app/myfiles/SearchActivity;)V

    .line 247
    :cond_0
    return-void
.end method
