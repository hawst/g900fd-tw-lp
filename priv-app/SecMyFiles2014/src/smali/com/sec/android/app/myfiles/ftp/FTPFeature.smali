.class public Lcom/sec/android/app/myfiles/ftp/FTPFeature;
.super Ljava/lang/Object;
.source "FTPFeature.java"


# static fields
.field public static final CONNECT_TIMEOUT_MS:I = 0x1770

.field public static final DRAG_N_DROP_MOVE_AS_DEFAULT:Z = true

.field public static final KEEP_ALIVE_TIMEOUT_S:I = 0x3c

.field public static final MDTM_CACHING_ENABLED:Z = false

.field public static final ON_UPDATE_CONTENT_TOAST:Z = false

.field public static final PUSH_BROWSE_DONT_CARE_TIME_DIFF:Z = false

.field public static final SHORTCUT_SESSION_NAME_FULL_PATH:Z = false

.field public static final SWITCH_FRAGMENT_ON_BACK_AFTER_COPY_MOVE:Z = true

.field public static final UPDATE_TIME_TICK:I = 0x2710

.field public static final USE_INTERMEDIATE_BUFFER:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
