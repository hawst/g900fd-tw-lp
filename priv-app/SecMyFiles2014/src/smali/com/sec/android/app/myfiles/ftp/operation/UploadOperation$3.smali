.class Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation$3;
.super Ljava/lang/Object;
.source "UploadOperation.java"

# interfaces
.implements Lcom/sec/android/app/myfiles/openoperation/IProgressUpdatable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->doInBackground([Landroid/os/Bundle;)Ljava/lang/Boolean;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;

.field final synthetic val$current:I

.field final synthetic val$total:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;II)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation$3;->this$0:Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;

    iput p2, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation$3;->val$current:I

    iput p3, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation$3;->val$total:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public updateProgress(I)V
    .locals 4
    .param p1, "progress"    # I

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation$3;->this$0:Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation$3;->val$current:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation$3;->val$total:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    # invokes: Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->publishProgress([Ljava/lang/Object;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;->access$300(Lcom/sec/android/app/myfiles/ftp/operation/UploadOperation;[Ljava/lang/Object;)V

    .line 121
    return-void
.end method
