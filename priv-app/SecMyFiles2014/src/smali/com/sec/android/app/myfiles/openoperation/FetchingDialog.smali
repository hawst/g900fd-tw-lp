.class public Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;
.super Landroid/app/DialogFragment;
.source "FetchingDialog.java"


# static fields
.field private static final HW_BACK_BUTTON_ENABLED:Z


# instance fields
.field private mAbort:Ljava/lang/Runnable;

.field private mCancelButtonActive:Z

.field private mCountText:Landroid/widget/TextView;

.field private mFileName:Landroid/widget/TextView;

.field private mPercentText:Landroid/widget/TextView;

.field private mProcessing:Landroid/widget/TextView;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mSubtitle:Ljava/lang/String;

.field private mText:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;

.field private final mWaitOnCancel:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 37
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 303
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mWaitOnCancel:Z

    .line 305
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mTitle:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mText:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mSubtitle:Ljava/lang/String;

    .line 308
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mCancelButtonActive:Z

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;Landroid/widget/Button;Landroid/app/Dialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;
    .param p1, "x1"    # Landroid/widget/Button;
    .param p2, "x2"    # Landroid/app/Dialog;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->setUiToCancelling(Landroid/widget/Button;Landroid/app/Dialog;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mAbort:Ljava/lang/Runnable;

    return-object v0
.end method

.method private setUiToCancelling(Landroid/widget/Button;Landroid/app/Dialog;)V
    .locals 1
    .param p1, "negativeButton"    # Landroid/widget/Button;
    .param p2, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 235
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 237
    const v0, 0x7f0b009b

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 239
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 243
    :cond_0
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 248
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 250
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->setRetainInstance(Z)V

    .line 252
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 72
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 74
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v4, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 76
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 78
    .local v2, "factory":Landroid/view/LayoutInflater;
    if-eqz v2, :cond_3

    .line 80
    const v4, 0x7f040039

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 82
    .local v3, "view":Landroid/view/View;
    if-eqz v3, :cond_2

    .line 84
    const v4, 0x7f0f00d4

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ProgressBar;

    iput-object v4, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mProgressBar:Landroid/widget/ProgressBar;

    .line 86
    const v4, 0x7f0f00d3

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mFileName:Landroid/widget/TextView;

    .line 88
    const v4, 0x7f0f00d0

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mProcessing:Landroid/widget/TextView;

    .line 90
    const v4, 0x7f0f00d6

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mPercentText:Landroid/widget/TextView;

    .line 92
    const v4, 0x7f0f00d5

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mCountText:Landroid/widget/TextView;

    .line 94
    const v4, 0x7f0f00d1

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 96
    .local v1, "descriptionLayout":Landroid/widget/LinearLayout;
    if-eqz v1, :cond_0

    .line 98
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 102
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mFileName:Landroid/widget/TextView;

    if-eqz v4, :cond_1

    .line 104
    iget-object v4, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mFileName:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mText:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mProcessing:Landroid/widget/TextView;

    if-eqz v4, :cond_2

    .line 110
    iget-object v4, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mSubtitle:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    .line 112
    iget-object v4, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mProcessing:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mSubtitle:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    .end local v1    # "descriptionLayout":Landroid/widget/LinearLayout;
    :cond_2
    :goto_0
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 126
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 128
    new-instance v4, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog$1;-><init>(Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;)V

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 165
    iget-boolean v4, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mCancelButtonActive:Z

    if-eqz v4, :cond_3

    .line 167
    const v4, 0x7f0b0017

    new-instance v5, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog$2;

    invoke-direct {v5, p0}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog$2;-><init>(Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 190
    .end local v3    # "view":Landroid/view/View;
    :cond_3
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    return-object v4

    .line 116
    .restart local v1    # "descriptionLayout":Landroid/widget/LinearLayout;
    .restart local v3    # "view":Landroid/view/View;
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mProcessing:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 257
    invoke-super {p0, p1, p2, p3}, Landroid/app/DialogFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 258
    .local v1, "view":Landroid/view/View;
    iget-boolean v2, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mCancelButtonActive:Z

    if-nez v2, :cond_0

    .line 259
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 260
    .local v0, "dialog":Landroid/app/Dialog;
    if-eqz v0, :cond_0

    .line 261
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 264
    .end local v0    # "dialog":Landroid/app/Dialog;
    :cond_0
    return-object v1
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 196
    invoke-super {p0}, Landroid/app/DialogFragment;->onStart()V

    .line 200
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    .line 202
    .local v0, "dialog":Landroid/app/AlertDialog;
    if-eqz v0, :cond_0

    .line 204
    const/4 v2, -0x2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    .line 206
    .local v1, "negativeButton":Landroid/widget/Button;
    if-eqz v1, :cond_0

    .line 208
    new-instance v2, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog$3;

    invoke-direct {v2, p0, v1, v0}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog$3;-><init>(Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;Landroid/widget/Button;Landroid/app/AlertDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 231
    .end local v1    # "negativeButton":Landroid/widget/Button;
    :cond_0
    return-void
.end method

.method public setAbortAction(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "abort"    # Ljava/lang/Runnable;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mAbort:Ljava/lang/Runnable;

    .line 61
    return-void
.end method

.method public setCancelButtonActive(Z)V
    .locals 0
    .param p1, "cancelButtonActive"    # Z

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mCancelButtonActive:Z

    .line 67
    return-void
.end method

.method public setFilename(Ljava/lang/String;)V
    .locals 1
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 268
    iget-object v0, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mFileName:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mFileName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 271
    :cond_0
    return-void
.end method

.method public setSubtitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "subtitle"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mSubtitle:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public setText(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mText:Ljava/lang/String;

    .line 55
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mTitle:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public updateProgress(I)V
    .locals 3
    .param p1, "progress"    # I

    .prologue
    const/16 v1, 0x64

    const/4 v0, 0x0

    .line 274
    iget-object v2, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v2, :cond_0

    .line 275
    iget-object v2, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 276
    iget-object v2, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v2, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 278
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mPercentText:Landroid/widget/TextView;

    if-eqz v2, :cond_2

    .line 279
    if-gez p1, :cond_3

    move p1, v0

    .line 280
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mPercentText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 282
    :cond_2
    return-void

    .line 279
    :cond_3
    if-le p1, v1, :cond_1

    move p1, v1

    goto :goto_0
.end method

.method public updateProgress(III)V
    .locals 3
    .param p1, "progress"    # I
    .param p2, "current"    # I
    .param p3, "total"    # I

    .prologue
    .line 285
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 286
    invoke-virtual {p0, p1}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->updateProgress(I)V

    .line 287
    iget-object v1, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mCountText:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 288
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 289
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 290
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 291
    const v1, 0x7f0b00af

    invoke-virtual {p0, v1}, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 295
    :goto_0
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 296
    iget-object v1, p0, Lcom/sec/android/app/myfiles/openoperation/FetchingDialog;->mCountText:Landroid/widget/TextView;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 299
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :cond_0
    return-void

    .line 293
    .restart local v0    # "sb":Ljava/lang/StringBuilder;
    :cond_1
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
