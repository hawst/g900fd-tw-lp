.class public Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$CancellationRunnable;
.super Ljava/lang/Object;
.source "DownLoadOperationTask.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CancellationRunnable"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;)V
    .locals 0

    .prologue
    .line 222
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$CancellationRunnable;->this$0:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 226
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$CancellationRunnable;->this$0:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mCancelableDownloadList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->access$300(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 227
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$CancellationRunnable;->this$0:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mCancelableDownloadList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->access$300(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 229
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$CancellationRunnable;->this$0:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mCancelableDownloadList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->access$300(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;

    invoke-virtual {v2}, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->cancelDownload()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 227
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 230
    :catch_0
    move-exception v0

    .line 231
    .local v0, "ex":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 235
    .end local v0    # "ex":Ljava/lang/Exception;
    .end local v1    # "i":I
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$CancellationRunnable;->this$0:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mCancelableUploadList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->access$400(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 236
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$CancellationRunnable;->this$0:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mCancelableUploadList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->access$400(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 238
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask$CancellationRunnable;->this$0:Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->mCancelableUploadList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;->access$400(Lcom/sec/android/app/myfiles/fileoperation/DownLoadOperationTask;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;

    invoke-virtual {v2}, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->cancelUpload()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 236
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 239
    :catch_1
    move-exception v0

    .line 240
    .restart local v0    # "ex":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 244
    .end local v0    # "ex":Ljava/lang/Exception;
    .end local v1    # "i":I
    :cond_1
    return-void
.end method
