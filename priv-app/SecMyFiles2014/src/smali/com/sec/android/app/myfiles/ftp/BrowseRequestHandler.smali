.class public Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;
.super Ljava/lang/Object;
.source "BrowseRequestHandler.java"


# static fields
.field private static final MODULE:Ljava/lang/String;

.field private static volatile mInstance:Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;


# instance fields
.field private final mBrowseMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Lcom/sec/android/app/myfiles/ftp/FTPParams;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 162
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;->mInstance:Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;

    .line 164
    const-class v0, Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;->MODULE:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 159
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;->mBrowseMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 53
    return-void
.end method

.method private static getBrowseIntent(Landroid/content/Context;Lcom/sec/android/app/myfiles/ftp/FTPParams;Lcom/sec/android/app/myfiles/ftp/IFTPItem;)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p2, "browseItem"    # Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .prologue
    .line 141
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 143
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/app/myfiles/ftp/BrowseService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 145
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;->FTP_PARAMS:Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/myfiles/ftp/FTPParams;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 147
    sget-object v1, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;->BROWSE_ITEM:Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;

    invoke-virtual {v1}, Lcom/sec/android/app/myfiles/ftp/BrowseService$Param;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 153
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getInstance()Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;
    .locals 2

    .prologue
    .line 35
    sget-object v0, Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;->mInstance:Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;

    if-nez v0, :cond_1

    .line 37
    const-class v1, Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;

    monitor-enter v1

    .line 39
    :try_start_0
    sget-object v0, Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;->mInstance:Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;

    invoke-direct {v0}, Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;-><init>()V

    sput-object v0, Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;->mInstance:Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;

    .line 45
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 49
    :cond_1
    sget-object v0, Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;->mInstance:Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;

    return-object v0

    .line 45
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public isBrowsePending(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;)Z
    .locals 2
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p2, "browsePath"    # Ljava/lang/String;

    .prologue
    .line 121
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;->mBrowseMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 123
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;->mBrowseMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    .line 125
    .local v0, "pending":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    monitor-enter v0

    .line 127
    :try_start_0
    invoke-virtual {v0, p2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    monitor-exit v0

    .line 133
    .end local v0    # "pending":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :goto_0
    return v1

    .line 129
    .restart local v0    # "pending":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 133
    .end local v0    # "pending":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public popBrowseRequest(Lcom/sec/android/app/myfiles/ftp/FTPParams;Ljava/lang/String;)V
    .locals 5
    .param p1, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p2, "browsePath"    # Ljava/lang/String;

    .prologue
    .line 99
    const/4 v1, 0x2

    sget-object v2, Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;->MODULE:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "popBrowseRequest browseItem: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    .line 101
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;->mBrowseMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 103
    iget-object v1, p0, Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;->mBrowseMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    .line 105
    .local v0, "pending":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    monitor-enter v0

    .line 107
    :try_start_0
    invoke-virtual {v0, p2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 109
    invoke-virtual {v0, p2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 113
    :cond_0
    monitor-exit v0

    .line 117
    .end local v0    # "pending":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_1
    return-void

    .line 113
    .restart local v0    # "pending":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public pushBrowseRequest(Landroid/content/Context;Lcom/sec/android/app/myfiles/ftp/FTPParams;Lcom/sec/android/app/myfiles/ftp/IFTPItem;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "params"    # Lcom/sec/android/app/myfiles/ftp/FTPParams;
    .param p3, "browseItem"    # Lcom/sec/android/app/myfiles/ftp/IFTPItem;

    .prologue
    .line 57
    const/4 v2, 0x2

    sget-object v3, Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;->MODULE:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "pushBrowseRequest browseItem: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->v(ILjava/lang/String;Ljava/lang/String;)I

    .line 59
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;->mBrowseMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, p2}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 65
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;->mBrowseMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, p2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashSet;

    .line 67
    .local v1, "pending":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    monitor-enter v1

    .line 69
    :try_start_0
    invoke-interface {p3}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 71
    invoke-interface {p3}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 72
    invoke-static {p1, p2, p3}, Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;->getBrowseIntent(Landroid/content/Context;Lcom/sec/android/app/myfiles/ftp/FTPParams;Lcom/sec/android/app/myfiles/ftp/IFTPItem;)Landroid/content/Intent;

    move-result-object v0

    .line 73
    .local v0, "i":Landroid/content/Intent;
    if-eqz v0, :cond_2

    .line 74
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 79
    .end local v0    # "i":Landroid/content/Intent;
    :cond_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 83
    .end local v1    # "pending":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_3
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 85
    .restart local v1    # "pending":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-interface {p3}, Lcom/sec/android/app/myfiles/ftp/IFTPItem;->getFullPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 87
    iget-object v2, p0, Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;->mBrowseMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, p2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 88
    invoke-static {p1, p2, p3}, Lcom/sec/android/app/myfiles/ftp/BrowseRequestHandler;->getBrowseIntent(Landroid/content/Context;Lcom/sec/android/app/myfiles/ftp/FTPParams;Lcom/sec/android/app/myfiles/ftp/IFTPItem;)Landroid/content/Intent;

    move-result-object v0

    .line 89
    .restart local v0    # "i":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 90
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method
