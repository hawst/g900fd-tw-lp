.class Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment$1;
.super Ljava/lang/Object;
.source "UserShortcutFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;->onCreateItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 17
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 70
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual/range {p1 .. p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v12

    move/from16 v0, p3

    invoke-interface {v12, v0}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/database/Cursor;

    .line 71
    .local v2, "cursor":Landroid/database/Cursor;
    const-string v12, "_data"

    invoke-interface {v2, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    invoke-interface {v2, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 72
    .local v8, "path":Ljava/lang/String;
    const-string v12, "type"

    invoke-interface {v2, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    invoke-interface {v2, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 74
    .local v11, "type":I
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 75
    .local v3, "file":Ljava/io/File;
    const/16 v12, 0x1a

    if-ne v11, v12, :cond_0

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_3

    :cond_0
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretBoxRootFolder(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_1

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v12}, Lcom/sec/android/app/myfiles/utils/Utils;->isSecretMode(Landroid/content/Context;)Z

    move-result v12

    if-eqz v12, :cond_3

    :cond_1
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    const-string v13, "/storage/extSdCard"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    sget-boolean v12, Lcom/sec/android/app/myfiles/utils/Utils;->mSdCardMounted:Z

    if-eqz v12, :cond_3

    :cond_2
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    const-string v13, "/storage/UsbDrive"

    invoke-virtual {v12, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_6

    sget-boolean v12, Lcom/sec/android/app/myfiles/utils/Utils;->mUsbMounted:Z

    if-nez v12, :cond_6

    .line 80
    :cond_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v12}, Lcom/sec/android/app/myfiles/AbsMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 81
    .local v9, "res":Landroid/content/res/Resources;
    const v12, 0x7f0c0013

    const/4 v13, 0x1

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    const/16 v16, 0x1

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v9, v12, v13, v14}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 82
    .local v7, "notiStr":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/4 v13, 0x0

    invoke-static {v12, v7, v13}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/Toast;->show()V

    .line 83
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const/16 v13, 0x1a

    invoke-static {v12, v8, v13}, Lcom/sec/android/app/myfiles/utils/Utils;->deleteShortcut(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v10

    .line 84
    .local v10, "rowsDeleted":I
    if-lez v10, :cond_5

    .line 85
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;

    iget-object v13, v13, Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;->mNavigation:Lcom/sec/android/app/myfiles/navigation/AbsNavigation;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/navigation/AbsNavigation;->getFilesInCurrentFolder()Landroid/database/Cursor;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;->updateAdapter(Landroid/database/Cursor;)V

    .line 114
    .end local v7    # "notiStr":Ljava/lang/String;
    .end local v9    # "res":Landroid/content/res/Resources;
    .end local v10    # "rowsDeleted":I
    :cond_4
    :goto_0
    return-void

    .line 88
    .restart local v7    # "notiStr":Ljava/lang/String;
    .restart local v9    # "res":Landroid/content/res/Resources;
    .restart local v10    # "rowsDeleted":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    const v13, 0x7f0b0063

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 93
    .end local v7    # "notiStr":Ljava/lang/String;
    .end local v9    # "res":Landroid/content/res/Resources;
    .end local v10    # "rowsDeleted":I
    :cond_6
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 94
    .local v1, "arg":Landroid/os/Bundle;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;

    invoke-virtual {v12}, Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    .line 95
    .local v6, "initialArgument":Landroid/os/Bundle;
    const-string v12, "CONTENT_TYPE"

    invoke-virtual {v6, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 96
    .local v5, "filterMimetype":Ljava/lang/String;
    const-string v12, "CONTENT_EXTENSION"

    invoke-virtual {v6, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 97
    .local v4, "filterExtension":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_8

    .line 98
    const-string v12, "CONTENT_EXTENSION"

    invoke-virtual {v1, v12, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    :cond_7
    :goto_1
    const-string v12, "id"

    const/16 v13, 0x1a

    invoke-virtual {v1, v12, v13}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 105
    const-string v12, "FOLDERPATH"

    invoke-virtual {v1, v12, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    const-string v12, "shortcut_type"

    invoke-virtual {v1, v12, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 107
    const-string v12, "run_from"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;

    iget v13, v13, Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;->mRunFrom:I

    invoke-virtual {v1, v12, v13}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 108
    const-string v12, "selection_type"

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;

    invoke-virtual {v13}, Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;->getSelectionType()I

    move-result v13

    invoke-virtual {v1, v12, v13}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 109
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    if-eqz v12, :cond_4

    .line 110
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment$1;->this$0:Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;

    iget-object v12, v12, Lcom/sec/android/app/myfiles/fragment/UserShortcutFragment;->mParentActivity:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v12, v1}, Lcom/sec/android/app/myfiles/AbsMainActivity;->setDefaultView(Landroid/os/Bundle;)V

    goto :goto_0

    .line 100
    :cond_8
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_7

    .line 101
    const-string v12, "CONTENT_TYPE"

    invoke-virtual {v1, v12, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
