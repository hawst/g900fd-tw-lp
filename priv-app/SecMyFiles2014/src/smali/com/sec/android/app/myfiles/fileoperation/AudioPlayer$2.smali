.class Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$2;
.super Landroid/os/Handler;
.source "AudioPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;)V
    .locals 0

    .prologue
    .line 272
    iput-object p1, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const-wide/16 v4, 0x14

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x5

    .line 276
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v3, :cond_0

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_2

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mCurrentVolume:F
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->access$300(Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    .line 296
    :cond_0
    :goto_0
    return-void

    .line 281
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mCurrentVolume:F
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->access$300(Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mCurrentVolume:F
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->access$300(Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->setVolume(FF)V

    .line 282
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->start()V

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mListPlayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->access$400(Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 286
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    const v1, 0x3c23d70a    # 0.01f

    # += operator for: Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mCurrentVolume:F
    invoke-static {v0, v1}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->access$316(Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;F)F

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mCurrentVolume:F
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->access$300(Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;)F

    move-result v0

    cmpg-float v0, v0, v2

    if-gez v0, :cond_3

    .line 288
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mListPlayerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->access$400(Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 293
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    iget-object v1, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mCurrentVolume:F
    invoke-static {v1}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->access$300(Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;)F

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    # getter for: Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mCurrentVolume:F
    invoke-static {v2}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->access$300(Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->setVolume(FF)V

    goto :goto_0

    .line 291
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer$2;->this$0:Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;

    # setter for: Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->mCurrentVolume:F
    invoke-static {v0, v2}, Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;->access$302(Lcom/sec/android/app/myfiles/fileoperation/AudioPlayer;F)F

    goto :goto_1
.end method
