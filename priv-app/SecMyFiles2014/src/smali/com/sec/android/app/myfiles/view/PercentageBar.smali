.class public Lcom/sec/android/app/myfiles/view/PercentageBar;
.super Landroid/view/View;
.source "PercentageBar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;,
        Lcom/sec/android/app/myfiles/view/PercentageBar$SineEase;
    }
.end annotation


# instance fields
.field private mAnimated:Z

.field private mBar:Landroid/graphics/Bitmap;

.field private mBarCanvas:Landroid/graphics/Canvas;

.field private mBarMask:Landroid/graphics/Bitmap;

.field private mBarMaskPaint:Landroid/graphics/Paint;

.field private mBeginTimePoint:J

.field private mBlock:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/drawable/NinePatchDrawable;",
            ">;"
        }
    .end annotation
.end field

.field private mBoundsRect:Landroid/graphics/Rect;

.field private final mEmptyPaint:Landroid/graphics/Paint;

.field private mEntries:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mMinTickWidth:I

.field private mSineEase:Lcom/sec/android/app/myfiles/view/PercentageBar$SineEase;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 45
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mEmptyPaint:Landroid/graphics/Paint;

    .line 47
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mMinTickWidth:I

    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mAnimated:Z

    .line 63
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBeginTimePoint:J

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v4, 0x0

    .line 73
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mEmptyPaint:Landroid/graphics/Paint;

    .line 47
    const/4 v1, 0x1

    iput v1, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mMinTickWidth:I

    .line 61
    iput-boolean v4, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mAnimated:Z

    .line 63
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBeginTimePoint:J

    .line 77
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 79
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0900f4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mMinTickWidth:I

    .line 81
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBlock:Ljava/util/ArrayList;

    .line 83
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBlock:Ljava/util/ArrayList;

    const v1, 0x7f0200eb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBlock:Ljava/util/ArrayList;

    const v1, 0x7f0200ee

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 87
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBlock:Ljava/util/ArrayList;

    const v1, 0x7f0200ec

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBlock:Ljava/util/ArrayList;

    const v1, 0x7f0200ea

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    iget-object v2, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBlock:Ljava/util/ArrayList;

    const v1, 0x7f0200ed

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 93
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mEmptyPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/PercentageBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x106000d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 97
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1}, Landroid/graphics/Canvas;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBarCanvas:Landroid/graphics/Canvas;

    .line 99
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBarMaskPaint:Landroid/graphics/Paint;

    .line 101
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBarMaskPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 103
    iget-object v1, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBarMaskPaint:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/PorterDuffXfermode;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v3}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 104
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 108
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mEmptyPaint:Landroid/graphics/Paint;

    .line 47
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mMinTickWidth:I

    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mAnimated:Z

    .line 63
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBeginTimePoint:J

    .line 109
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/myfiles/view/PercentageBar;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/view/PercentageBar;
    .param p1, "x1"    # J

    .prologue
    .line 41
    iput-wide p1, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBeginTimePoint:J

    return-wide p1
.end method

.method public static createUsageEntry(FLandroid/graphics/drawable/Drawable;)Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;
    .locals 1
    .param p0, "percentage"    # F
    .param p1, "blockResId"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 473
    new-instance v0, Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;-><init>(FLandroid/graphics/drawable/Drawable;)V

    return-object v0
.end method

.method private drawAnimated(Landroid/graphics/Canvas;)V
    .locals 29
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 236
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBarMask:Landroid/graphics/Bitmap;

    move-object/from16 v24, v0

    if-nez v24, :cond_0

    .line 319
    :goto_0
    return-void

    .line 241
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/view/PercentageBar;->getPaddingLeft()I

    move-result v17

    .line 243
    .local v17, "left":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/view/PercentageBar;->getWidth()I

    move-result v24

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/view/PercentageBar;->getPaddingRight()I

    move-result v25

    sub-int v18, v24, v25

    .line 245
    .local v18, "right":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/view/PercentageBar;->getPaddingTop()I

    move-result v22

    .line 247
    .local v22, "top":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/view/PercentageBar;->getHeight()I

    move-result v24

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/view/PercentageBar;->getPaddingBottom()I

    move-result v25

    sub-int v6, v24, v25

    .line 249
    .local v6, "bottom":I
    sub-int v23, v18, v17

    .line 251
    .local v23, "width":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBarCanvas:Landroid/graphics/Canvas;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBar:Landroid/graphics/Bitmap;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 253
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBeginTimePoint:J

    move-wide/from16 v24, v0

    const-wide/16 v26, -0x1

    cmp-long v24, v24, v26

    if-eqz v24, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mEntries:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    if-eqz v24, :cond_6

    .line 255
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    .line 257
    .local v7, "c":Ljava/util/Calendar;
    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v10

    .line 259
    .local v10, "currentTimePoint":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBeginTimePoint:J

    move-wide/from16 v24, v0

    sub-long v20, v10, v24

    .line 261
    .local v20, "timeElapsed":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mSineEase:Lcom/sec/android/app/myfiles/view/PercentageBar$SineEase;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/myfiles/view/PercentageBar$SineEase;->getTotalDuration()I

    move-result v24

    move/from16 v0, v24

    int-to-long v0, v0

    move-wide/from16 v24, v0

    cmp-long v24, v20, v24

    if-lez v24, :cond_1

    .line 263
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mSineEase:Lcom/sec/android/app/myfiles/view/PercentageBar$SineEase;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/app/myfiles/view/PercentageBar$SineEase;->getTotalDuration()I

    move-result v24

    move/from16 v0, v24

    int-to-long v0, v0

    move-wide/from16 v20, v0

    .line 265
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/myfiles/view/PercentageBar;->mAnimated:Z

    .line 268
    :cond_1
    const/16 v19, 0x0

    .line 270
    .local v19, "summaryWidth":I
    const/4 v13, -0x1

    .line 272
    .local v13, "entryIndex":I
    move/from16 v16, v17

    .line 274
    .local v16, "lastRight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBar:Landroid/graphics/Bitmap;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 276
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mEntries:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->size()I

    move-result v24

    move/from16 v0, v24

    if-ge v15, v0, :cond_6

    .line 278
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mEntries:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;

    .line 280
    .local v12, "e":Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;
    iget v0, v12, Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;->mPercentage:F

    move/from16 v24, v0

    const/16 v25, 0x0

    cmpl-float v24, v24, v25

    if-nez v24, :cond_3

    .line 276
    :cond_2
    :goto_2
    add-int/lit8 v15, v15, 0x1

    goto :goto_1

    .line 285
    :cond_3
    add-int/lit8 v13, v13, 0x1

    .line 287
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mMinTickWidth:I

    move/from16 v24, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v25, v0

    iget v0, v12, Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;->mPercentage:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    move/from16 v0, v25

    float-to-int v0, v0

    move/from16 v25, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->max(II)I

    move-result v9

    .line 289
    .local v9, "currentWidth":I
    add-int v19, v19, v9

    .line 291
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mSineEase:Lcom/sec/android/app/myfiles/view/PercentageBar$SineEase;

    move-object/from16 v24, v0

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v19

    invoke-virtual {v0, v13, v1, v2}, Lcom/sec/android/app/myfiles/view/PercentageBar$SineEase;->getStripCurrentWidth(III)I

    move-result v14

    .line 293
    .local v14, "entryWidth":I
    add-int v24, v17, v14

    move/from16 v0, v24

    move/from16 v1, v18

    if-le v0, v1, :cond_4

    move/from16 v8, v18

    .line 295
    .local v8, "curRight":I
    :goto_3
    sub-int v24, v8, v16

    const/16 v25, 0x5

    move/from16 v0, v24

    move/from16 v1, v25

    if-le v0, v1, :cond_2

    .line 297
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isRtl()Z

    move-result v24

    if-eqz v24, :cond_5

    .line 299
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBoundsRect:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/view/PercentageBar;->getWidth()I

    move-result v25

    sub-int v25, v25, v8

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/myfiles/view/PercentageBar;->getWidth()I

    move-result v26

    sub-int v26, v26, v16

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v22

    move/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 307
    :goto_4
    move/from16 v16, v8

    .line 309
    iget-object v0, v12, Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;->mBlockDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBoundsRect:Landroid/graphics/Rect;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 311
    iget-object v0, v12, Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;->mBlockDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBarCanvas:Landroid/graphics/Canvas;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_2

    .line 293
    .end local v8    # "curRight":I
    :cond_4
    add-int v8, v17, v14

    goto :goto_3

    .line 303
    .restart local v8    # "curRight":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBoundsRect:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move/from16 v1, v16

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2, v8, v6}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_4

    .line 316
    .end local v7    # "c":Ljava/util/Calendar;
    .end local v8    # "curRight":I
    .end local v9    # "currentWidth":I
    .end local v10    # "currentTimePoint":J
    .end local v12    # "e":Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;
    .end local v13    # "entryIndex":I
    .end local v14    # "entryWidth":I
    .end local v15    # "i":I
    .end local v16    # "lastRight":I
    .end local v19    # "summaryWidth":I
    .end local v20    # "timeElapsed":J
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBarCanvas:Landroid/graphics/Canvas;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBarMask:Landroid/graphics/Bitmap;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const/16 v27, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBarMaskPaint:Landroid/graphics/Paint;

    move-object/from16 v28, v0

    invoke-virtual/range {v24 .. v28}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 318
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBar:Landroid/graphics/Bitmap;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    move-object/from16 v4, v27

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public drawPercentage()V
    .locals 8

    .prologue
    .line 174
    iget-object v5, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mEntries:Ljava/util/ArrayList;

    if-nez v5, :cond_0

    .line 232
    :goto_0
    return-void

    .line 179
    :cond_0
    const/4 v2, 0x0

    .line 181
    .local v2, "entryCount":I
    iget-object v5, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mEntries:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;

    .line 183
    .local v1, "e":Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;
    iget v5, v1, Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;->mPercentage:F

    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-eqz v5, :cond_1

    .line 185
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 189
    .end local v1    # "e":Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;
    :cond_2
    new-instance v5, Lcom/sec/android/app/myfiles/view/PercentageBar$SineEase;

    const/16 v6, 0x1f4

    const/16 v7, 0xa0

    invoke-direct {v5, v2, v6, v7}, Lcom/sec/android/app/myfiles/view/PercentageBar$SineEase;-><init>(III)V

    iput-object v5, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mSineEase:Lcom/sec/android/app/myfiles/view/PercentageBar$SineEase;

    .line 191
    iget-object v5, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mSineEase:Lcom/sec/android/app/myfiles/view/PercentageBar$SineEase;

    invoke-virtual {v5}, Lcom/sec/android/app/myfiles/view/PercentageBar$SineEase;->getTotalDuration()I

    move-result v0

    .line 193
    .local v0, "animationDuration":I
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mAnimated:Z

    .line 195
    new-instance v4, Lcom/sec/android/app/myfiles/view/PercentageBar$1;

    invoke-direct {v4, p0, v0}, Lcom/sec/android/app/myfiles/view/PercentageBar$1;-><init>(Lcom/sec/android/app/myfiles/view/PercentageBar;I)V

    .line 231
    .local v4, "thread":Ljava/lang/Thread;
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v13, 0x0

    .line 379
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 381
    iget-boolean v10, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mAnimated:Z

    if-eqz v10, :cond_1

    .line 383
    invoke-direct {p0, p1}, Lcom/sec/android/app/myfiles/view/PercentageBar;->drawAnimated(Landroid/graphics/Canvas;)V

    .line 460
    :cond_0
    :goto_0
    return-void

    .line 388
    :cond_1
    iget-object v10, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBarMask:Landroid/graphics/Bitmap;

    if-eqz v10, :cond_0

    .line 390
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/PercentageBar;->getPaddingLeft()I

    move-result v5

    .line 392
    .local v5, "left":I
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/PercentageBar;->getWidth()I

    move-result v10

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/PercentageBar;->getPaddingRight()I

    move-result v11

    sub-int v7, v10, v11

    .line 394
    .local v7, "right":I
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/PercentageBar;->getPaddingTop()I

    move-result v8

    .line 396
    .local v8, "top":I
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/PercentageBar;->getHeight()I

    move-result v10

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/PercentageBar;->getPaddingBottom()I

    move-result v11

    sub-int v0, v10, v11

    .line 398
    .local v0, "bottom":I
    sub-int v9, v7, v5

    .line 400
    .local v9, "width":I
    move v4, v5

    .line 402
    .local v4, "lastX":I
    iget-object v10, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBarCanvas:Landroid/graphics/Canvas;

    iget-object v11, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBar:Landroid/graphics/Bitmap;

    invoke-virtual {v10, v11}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 404
    iget-object v10, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mEntries:Ljava/util/ArrayList;

    if-eqz v10, :cond_3

    .line 406
    iget-object v10, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mEntries:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;

    .line 410
    .local v1, "e":Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;
    iget v10, v1, Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;->mPercentage:F

    cmpl-float v10, v10, v13

    if-nez v10, :cond_2

    .line 412
    const/4 v2, 0x0

    .line 414
    .local v2, "entryWidth":I
    goto :goto_1

    .line 418
    .end local v2    # "entryWidth":I
    :cond_2
    iget v10, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mMinTickWidth:I

    int-to-float v11, v9

    iget v12, v1, Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;->mPercentage:F

    mul-float/2addr v11, v12

    float-to-int v11, v11

    invoke-static {v10, v11}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 421
    .restart local v2    # "entryWidth":I
    add-int v6, v4, v2

    .line 423
    .local v6, "nextX":I
    if-le v6, v7, :cond_5

    .line 425
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isRtl()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 426
    iget-object v10, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBoundsRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/PercentageBar;->getWidth()I

    move-result v11

    sub-int/2addr v11, v4

    invoke-virtual {v10, v5, v8, v11, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 431
    :goto_2
    iget-object v10, v1, Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;->mBlockDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v11, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBoundsRect:Landroid/graphics/Rect;

    invoke-virtual {v10, v11}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 433
    iget-object v10, v1, Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;->mBlockDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v11, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBarCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v10, v11}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 435
    move v4, v6

    .line 456
    .end local v1    # "e":Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;
    .end local v2    # "entryWidth":I
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v6    # "nextX":I
    :cond_3
    iget-object v10, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBarCanvas:Landroid/graphics/Canvas;

    iget-object v11, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBarMask:Landroid/graphics/Bitmap;

    iget-object v12, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBarMaskPaint:Landroid/graphics/Paint;

    invoke-virtual {v10, v11, v13, v13, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 458
    iget-object v10, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBar:Landroid/graphics/Bitmap;

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v13, v13, v11}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 428
    .restart local v1    # "e":Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;
    .restart local v2    # "entryWidth":I
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v6    # "nextX":I
    :cond_4
    iget-object v10, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBoundsRect:Landroid/graphics/Rect;

    invoke-virtual {v10, v4, v8, v7, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_2

    .line 441
    :cond_5
    invoke-static {}, Lcom/sec/android/app/myfiles/utils/Utils;->isRtl()Z

    move-result v10

    if-eqz v10, :cond_6

    .line 442
    iget-object v10, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBoundsRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/PercentageBar;->getWidth()I

    move-result v11

    sub-int/2addr v11, v6

    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/PercentageBar;->getWidth()I

    move-result v12

    sub-int/2addr v12, v4

    invoke-virtual {v10, v11, v8, v12, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 447
    :goto_3
    iget-object v10, v1, Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;->mBlockDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v11, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBoundsRect:Landroid/graphics/Rect;

    invoke-virtual {v10, v11}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 449
    iget-object v10, v1, Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;->mBlockDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v11, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBarCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v10, v11}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 451
    move v4, v6

    .line 453
    goto :goto_1

    .line 444
    :cond_6
    iget-object v10, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBoundsRect:Landroid/graphics/Rect;

    invoke-virtual {v10, v4, v8, v6, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_3
.end method

.method protected onLayout(ZIIII)V
    .locals 10
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 114
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 116
    iget-object v7, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBar:Landroid/graphics/Bitmap;

    if-eqz v7, :cond_0

    if-eqz p1, :cond_1

    .line 118
    :cond_0
    sub-int v2, p4, p2

    .line 120
    .local v2, "barWidth":I
    sub-int v0, p5, p3

    .line 122
    .local v0, "barHeight":I
    if-lez v2, :cond_1

    if-gtz v0, :cond_2

    .line 164
    .end local v0    # "barHeight":I
    .end local v2    # "barWidth":I
    :cond_1
    :goto_0
    return-void

    .line 128
    .restart local v0    # "barHeight":I
    .restart local v2    # "barWidth":I
    :cond_2
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/myfiles/view/PercentageBar;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0200ef

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 131
    .local v1, "barMask":Landroid/graphics/drawable/Drawable;
    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v0, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 133
    .local v5, "newBar":Landroid/graphics/Bitmap;
    iget-object v7, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBar:Landroid/graphics/Bitmap;

    if-eqz v7, :cond_3

    .line 135
    iget-object v7, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBar:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    .line 138
    :cond_3
    iput-object v5, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBar:Landroid/graphics/Bitmap;

    .line 141
    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v0, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 143
    .local v6, "newBarMask":Landroid/graphics/Bitmap;
    iget-object v7, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBarMask:Landroid/graphics/Bitmap;

    if-eqz v7, :cond_4

    .line 145
    iget-object v7, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBarMask:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    .line 148
    :cond_4
    iput-object v6, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBarMask:Landroid/graphics/Bitmap;

    .line 151
    new-instance v3, Landroid/graphics/Canvas;

    iget-object v7, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBarMask:Landroid/graphics/Bitmap;

    invoke-direct {v3, v7}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 153
    .local v3, "canvas":Landroid/graphics/Canvas;
    new-instance v7, Landroid/graphics/Rect;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {v7, v8, v9, v2, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v7, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBoundsRect:Landroid/graphics/Rect;

    .line 155
    iget-object v7, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mBoundsRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v7}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 157
    invoke-virtual {v1, v3}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 159
    .end local v1    # "barMask":Landroid/graphics/drawable/Drawable;
    .end local v3    # "canvas":Landroid/graphics/Canvas;
    .end local v5    # "newBar":Landroid/graphics/Bitmap;
    .end local v6    # "newBarMask":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v4

    .line 161
    .local v4, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v4}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0
.end method

.method public setUsageEntries(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 169
    .local p1, "entries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/myfiles/view/PercentageBar$UsageEntry;>;"
    iput-object p1, p0, Lcom/sec/android/app/myfiles/view/PercentageBar;->mEntries:Ljava/util/ArrayList;

    .line 170
    return-void
.end method
