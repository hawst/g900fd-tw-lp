.class Lcom/sec/android/app/myfiles/hover/ImageHoverPopup$1;
.super Ljava/lang/Object;
.source "ImageHoverPopup.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->initViews()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;)V
    .locals 0

    .prologue
    .line 345
    iput-object p1, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v10, 0x0

    .line 350
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    if-nez v6, :cond_1

    .line 352
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;

    iget-boolean v6, v6, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->bFromNearByDevice:Z

    if-eqz v6, :cond_2

    .line 354
    const/4 v3, 0x0

    .line 355
    .local v3, "intent":Landroid/content/Intent;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mFileUri:Ljava/lang/String;

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 356
    .local v5, "uri":Landroid/net/Uri;
    new-instance v6, Landroid/content/Intent;

    const-string v7, "android.intent.action.VIEW"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mFileMimeType:Ljava/lang/String;

    invoke-virtual {v6, v5, v7}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 360
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-virtual {v6, v3}, Lcom/sec/android/app/myfiles/AbsMainActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 382
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v5    # "uri":Landroid/net/Uri;
    :cond_0
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;

    invoke-virtual {v6}, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->dissmissPopup()V

    .line 385
    :cond_1
    return v10

    .line 362
    .restart local v3    # "intent":Landroid/content/Intent;
    .restart local v5    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v1

    .line 364
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    const-string v6, "ImageHoverPopup"

    const-string v7, "Cannot find activity for the mime type: "

    invoke-static {v10, v6, v7}, Lcom/sec/android/app/myfiles/utils/MyFilesLog;->d(ILjava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 369
    .end local v1    # "e":Landroid/content/ActivityNotFoundException;
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v5    # "uri":Landroid/net/Uri;
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;

    iget-boolean v6, v6, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->bFromDropBox:Z

    if-eqz v6, :cond_3

    .line 371
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mFilePath:Ljava/lang/String;

    invoke-static {v6}, Lcom/sec/android/app/myfiles/utils/Utils;->getFileThumbnailFolderName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 372
    .local v4, "serverFolderName":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "large.jpg"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 373
    .local v0, "ThumbnailCacheFilePath":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 375
    .local v2, "file":Ljava/io/File;
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;

    iget v6, v6, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mCategoryType:I

    iget-object v7, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;

    iget-object v8, v8, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentSortBy()I

    move-result v8

    invoke-static {v6, v2, v7, v10, v8}, Lcom/sec/android/app/myfiles/utils/FileUtils;->openFile(ILjava/io/File;Landroid/app/Activity;ZI)V

    goto :goto_0

    .line 377
    .end local v0    # "ThumbnailCacheFilePath":Ljava/lang/String;
    .end local v2    # "file":Ljava/io/File;
    .end local v4    # "serverFolderName":Ljava/lang/String;
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;

    iget-object v6, v6, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mFile:Ljava/io/File;

    if-eqz v6, :cond_0

    .line 379
    iget-object v6, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;

    iget v6, v6, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mCategoryType:I

    iget-object v7, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;

    iget-object v7, v7, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mFile:Ljava/io/File;

    iget-object v8, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;

    iget-object v8, v8, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    iget-object v9, p0, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup$1;->this$0:Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;

    iget-object v9, v9, Lcom/sec/android/app/myfiles/hover/ImageHoverPopup;->mContext:Lcom/sec/android/app/myfiles/AbsMainActivity;

    invoke-static {v9}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/myfiles/utils/SharedDataStore;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/myfiles/utils/SharedDataStore;->getCurrentSortBy()I

    move-result v9

    invoke-static {v6, v7, v8, v10, v9}, Lcom/sec/android/app/myfiles/utils/FileUtils;->openFile(ILjava/io/File;Landroid/app/Activity;ZI)V

    goto :goto_0
.end method
