.class Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;
.super Ljava/lang/Object;
.source "FolderHoverPopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/myfiles/hover/FolderHoverPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "FileInfoItem"
.end annotation


# instance fields
.field private mFileFormat:I

.field private mFileIconID:I

.field private mFileName:Ljava/lang/String;

.field private mFilePath:Ljava/lang/String;

.field private mIsAudio:Z

.field private mIsImage:Z

.field private mIsVideo:Z


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1, "fileIconId"    # I
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "fileFormat"    # I

    .prologue
    const/4 v0, 0x0

    .line 680
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 649
    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mIsImage:Z

    .line 650
    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mIsVideo:Z

    .line 651
    iput-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mIsAudio:Z

    .line 682
    iput p1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mFileIconID:I

    .line 684
    iput-object p2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mFilePath:Ljava/lang/String;

    .line 686
    iput-object p3, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mFileName:Ljava/lang/String;

    .line 688
    iput p4, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mFileFormat:I

    .line 690
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;II)V
    .locals 3
    .param p1, "fileIconId"    # I
    .param p2, "filePath"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "fileFormat"    # I
    .param p5, "fileType"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 653
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 649
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mIsImage:Z

    .line 650
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mIsVideo:Z

    .line 651
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mIsAudio:Z

    .line 655
    iput p1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mFileIconID:I

    .line 657
    iput-object p2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mFilePath:Ljava/lang/String;

    .line 659
    iput-object p3, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mFileName:Ljava/lang/String;

    .line 661
    iput p4, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mFileFormat:I

    .line 663
    invoke-static {p5}, Lcom/sec/android/app/myfiles/MediaFile;->isVideoFileType(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 664
    iput-boolean v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mIsVideo:Z

    .line 668
    :goto_0
    invoke-static {p5}, Lcom/sec/android/app/myfiles/MediaFile;->isImageFileType(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 669
    iput-boolean v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mIsImage:Z

    .line 673
    :goto_1
    invoke-static {p5}, Lcom/sec/android/app/myfiles/MediaFile;->isAudioFileType(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 674
    iput-boolean v2, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mIsAudio:Z

    .line 678
    :goto_2
    return-void

    .line 666
    :cond_0
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mIsVideo:Z

    goto :goto_0

    .line 671
    :cond_1
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mIsImage:Z

    goto :goto_1

    .line 676
    :cond_2
    iput-boolean v1, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mIsAudio:Z

    goto :goto_2
.end method

.method static synthetic access$700(Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;

    .prologue
    .line 643
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mFilePath:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public checkHasThumbnail()Z
    .locals 1

    .prologue
    .line 721
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mIsVideo:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mIsImage:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mIsAudio:Z

    if-eqz v0, :cond_1

    .line 722
    :cond_0
    const/4 v0, 0x1

    .line 724
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFileFormat()I
    .locals 1

    .prologue
    .line 715
    iget v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mFileFormat:I

    return v0
.end method

.method public getFileIconID()I
    .locals 2

    .prologue
    .line 694
    iget v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mFileFormat:I

    const/16 v1, 0x3001

    if-ne v0, v1, :cond_0

    .line 695
    const v0, 0x7f0200af

    .line 697
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mFileIconID:I

    goto :goto_0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 709
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 703
    iget-object v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mFilePath:Ljava/lang/String;

    return-object v0
.end method

.method public isAudioFile()Z
    .locals 1

    .prologue
    .line 733
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mIsAudio:Z

    return v0
.end method

.method public isImageFile()Z
    .locals 1

    .prologue
    .line 737
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mIsImage:Z

    return v0
.end method

.method public isVidoeFile()Z
    .locals 1

    .prologue
    .line 729
    iget-boolean v0, p0, Lcom/sec/android/app/myfiles/hover/FolderHoverPopup$FileInfoItem;->mIsVideo:Z

    return v0
.end method
