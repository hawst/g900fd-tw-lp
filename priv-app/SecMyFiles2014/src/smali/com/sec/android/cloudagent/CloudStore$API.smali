.class public final Lcom/sec/android/cloudagent/CloudStore$API;
.super Ljava/lang/Object;
.source "CloudStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/cloudagent/CloudStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "API"
.end annotation


# static fields
.field public static final KEY_CLOUD_AVAILABLE:Ljava/lang/String; = "cloud_available"

.field public static final KEY_CLOUD_VENDOR:Ljava/lang/String; = "cloud_vendor"

.field public static final KEY_CLOUD_VENDOR_AVAILABLE:Ljava/lang/String; = "cloud_vendor_available"

.field public static final KEY_DOWNLOAD:Ljava/lang/String; = "download"

.field public static final KEY_EXCEPTION:Ljava/lang/String; = "exception"

.field public static final KEY_GETDOWNLOAD_URL:Ljava/lang/String; = "get_download_URL"

.field public static final KEY_GETSTREAMING_URL:Ljava/lang/String; = "get_streaming_URL"

.field public static final KEY_GETTHUMBNAIL:Ljava/lang/String; = "get_thumbnail"

.field public static final KEY_MAKE_AVAILABLE_OFFLINE:Ljava/lang/String; = "make_available_offline"

.field public static final KEY_PREFETCH:Ljava/lang/String; = "prefetch"

.field public static final KEY_PREFETCH_WITH_BLOCKING:Ljava/lang/String; = "prefetch_with_blocking"

.field public static final KEY_REVERT_AVAILABLE_OFFLINE:Ljava/lang/String; = "revert_available_offline"

.field public static final KEY_SHARE_URL:Ljava/lang/String; = "get_share_URL"

.field public static final KEY_SYNC:Ljava/lang/String; = "sync"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static download(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/cloudagent/exception/CloudException;
        }
    .end annotation

    .prologue
    .line 323
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    const-string v4, "download"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 325
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 326
    const-string v2, "exception"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;

    .line 327
    .local v1, "e":Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;
    if-eqz v1, :cond_0

    .line 328
    iget-object v2, v1, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;->o:Ljava/lang/Object;

    check-cast v2, Lcom/sec/android/cloudagent/exception/CloudException;

    throw v2

    .line 330
    .end local v1    # "e":Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;
    :cond_0
    return-void
.end method

.method public static getCloudVendorName(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 420
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    const-string v4, "cloud_vendor"

    invoke-virtual {v2, v3, v4, v1, v1}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 421
    .local v0, "result":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 422
    const-string v1, "cloud_vendor"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 424
    :cond_0
    return-object v1
.end method

.method public static getDownloadURL(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/cloudagent/exception/CloudException;
        }
    .end annotation

    .prologue
    .line 300
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v5

    const-string v6, "get_download_URL"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 301
    .local v0, "bundle":Landroid/os/Bundle;
    const/4 v2, 0x0

    .line 303
    .local v2, "url":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 304
    const-string v4, "DOWNLOAD_URL"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 305
    if-eqz v2, :cond_0

    move-object v3, v2

    .line 314
    .end local v2    # "url":Ljava/lang/String;
    .local v3, "url":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 308
    .end local v3    # "url":Ljava/lang/String;
    .restart local v2    # "url":Ljava/lang/String;
    :cond_0
    const-string v4, "exception"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;

    .line 309
    .local v1, "e":Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;
    if-eqz v1, :cond_1

    .line 310
    iget-object v4, v1, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;->o:Ljava/lang/Object;

    check-cast v4, Lcom/sec/android/cloudagent/exception/CloudException;

    throw v4

    .end local v1    # "e":Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;
    :cond_1
    move-object v3, v2

    .line 314
    .end local v2    # "url":Ljava/lang/String;
    .restart local v3    # "url":Ljava/lang/String;
    goto :goto_0
.end method

.method public static getSharedURL(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/cloudagent/exception/CloudException;
        }
    .end annotation

    .prologue
    .line 249
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v5

    const-string v6, "get_share_URL"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 250
    .local v0, "bundle":Landroid/os/Bundle;
    const/4 v2, 0x0

    .line 252
    .local v2, "url":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 253
    const-string v4, "SHARE_URL"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 254
    if-eqz v2, :cond_0

    move-object v3, v2

    .line 263
    .end local v2    # "url":Ljava/lang/String;
    .local v3, "url":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 257
    .end local v3    # "url":Ljava/lang/String;
    .restart local v2    # "url":Ljava/lang/String;
    :cond_0
    const-string v4, "exception"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;

    .line 258
    .local v1, "e":Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;
    if-eqz v1, :cond_1

    .line 259
    iget-object v4, v1, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;->o:Ljava/lang/Object;

    check-cast v4, Lcom/sec/android/cloudagent/exception/CloudException;

    throw v4

    .end local v1    # "e":Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;
    :cond_1
    move-object v3, v2

    .line 263
    .end local v2    # "url":Ljava/lang/String;
    .restart local v3    # "url":Ljava/lang/String;
    goto :goto_0
.end method

.method public static getStreamingURL(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/cloudagent/exception/CloudException;
        }
    .end annotation

    .prologue
    .line 274
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v5

    const-string v6, "get_streaming_URL"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 275
    .local v0, "bundle":Landroid/os/Bundle;
    const/4 v2, 0x0

    .line 277
    .local v2, "url":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 278
    const-string v4, "STREAMING_URL"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 279
    if-eqz v2, :cond_0

    move-object v3, v2

    .line 288
    .end local v2    # "url":Ljava/lang/String;
    .local v3, "url":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 282
    .end local v3    # "url":Ljava/lang/String;
    .restart local v2    # "url":Ljava/lang/String;
    :cond_0
    const-string v4, "exception"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;

    .line 283
    .local v1, "e":Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;
    if-eqz v1, :cond_1

    .line 284
    iget-object v4, v1, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;->o:Ljava/lang/Object;

    check-cast v4, Lcom/sec/android/cloudagent/exception/CloudException;

    throw v4

    .end local v1    # "e":Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;
    :cond_1
    move-object v3, v2

    .line 288
    .end local v2    # "url":Ljava/lang/String;
    .restart local v3    # "url":Ljava/lang/String;
    goto :goto_0
.end method

.method public static getThumbnail(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 229
    const/4 v0, 0x0

    .line 230
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Images;->getContentUri()Landroid/net/Uri;

    move-result-object v4

    const-string v5, "get_thumbnail"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    .line 232
    .local v1, "bundle":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 233
    const-string v3, "ThumbnailBitmap"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 235
    .local v2, "thumbPath":Ljava/lang/String;
    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 238
    .end local v2    # "thumbPath":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public static isCloudAvailable(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 371
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    const-string v3, "cloud_available"

    invoke-virtual {v1, v2, v3, v4, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 372
    .local v0, "result":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 373
    const-string v1, "cloudAvailable"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 375
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isCloudVendorAvailable(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 407
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    const-string v3, "cloud_vendor_available"

    invoke-virtual {v1, v2, v3, v4, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 408
    .local v0, "result":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 409
    const-string v1, "cloud_vendor_available"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 411
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static makeAvailableOffline(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 350
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "make_available_offline"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v1, v2, v3}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 352
    return-void
.end method

.method public static prefetch(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 339
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "prefetch"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v1, v2, v3}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 341
    return-void
.end method

.method public static prefetchWithBlocking(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/cloudagent/exception/CloudException;
        }
    .end annotation

    .prologue
    .line 385
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v5

    const-string v6, "prefetch_with_blocking"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 386
    .local v0, "bundle":Landroid/os/Bundle;
    const/4 v2, 0x0

    .line 387
    .local v2, "path":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 388
    const-string v4, "CACHED_PATH"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 390
    if-eqz v2, :cond_0

    move-object v3, v2

    .line 399
    .end local v2    # "path":Ljava/lang/String;
    .local v3, "path":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 393
    .end local v3    # "path":Ljava/lang/String;
    .restart local v2    # "path":Ljava/lang/String;
    :cond_0
    const-string v4, "exception"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;

    .line 394
    .local v1, "e":Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;
    if-eqz v1, :cond_1

    .line 395
    iget-object v4, v1, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;->o:Ljava/lang/Object;

    check-cast v4, Lcom/sec/android/cloudagent/exception/CloudException;

    throw v4

    .end local v1    # "e":Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;
    :cond_1
    move-object v3, v2

    .line 399
    .end local v2    # "path":Ljava/lang/String;
    .restart local v3    # "path":Ljava/lang/String;
    goto :goto_0
.end method

.method public static revertAvailableOffline(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 361
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "revert_available_offline"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v1, v2, v3}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 363
    return-void
.end method

.method public static sync(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 218
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    const-string v2, "sync"

    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 219
    return-void
.end method
