.class public Lcom/sec/orca/easysignup/api/AuthAPI;
.super Ljava/lang/Object;
.source "AuthAPI.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field mContext:Landroid/content/Context;

.field resolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const-class v0, Lcom/sec/orca/easysignup/api/AuthAPI;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/orca/easysignup/api/AuthAPI;->TAG:Ljava/lang/String;

    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/orca/easysignup/api/AuthAPI;->resolver:Landroid/content/ContentResolver;

    .line 19
    iput-object p1, p0, Lcom/sec/orca/easysignup/api/AuthAPI;->mContext:Landroid/content/Context;

    .line 20
    iget-object v0, p0, Lcom/sec/orca/easysignup/api/AuthAPI;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/orca/easysignup/api/AuthAPI;->resolver:Landroid/content/ContentResolver;

    .line 21
    return-void
.end method


# virtual methods
.method public getAccessToken()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 103
    const/4 v6, 0x0

    .line 105
    .local v6, "accessToken":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/orca/easysignup/api/AuthAPI;->resolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/orca/easysignup/db/DBHelper$AuthTable;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "access_token"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 107
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_0

    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    const-string v0, "access_token"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 111
    :cond_0
    if-eqz v7, :cond_1

    .line 112
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 116
    :cond_1
    if-eqz v6, :cond_3

    .line 117
    iget-object v0, p0, Lcom/sec/orca/easysignup/api/AuthAPI;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getAccessToken : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    :goto_0
    return-object v6

    .line 110
    :catchall_0
    move-exception v0

    .line 111
    if-eqz v7, :cond_2

    .line 112
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 114
    :cond_2
    throw v0

    .line 119
    :cond_3
    iget-object v0, p0, Lcom/sec/orca/easysignup/api/AuthAPI;->TAG:Ljava/lang/String;

    const-string v1, "getAccessToken is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getDuid()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 80
    const/4 v7, 0x0

    .line 82
    .local v7, "duid":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/orca/easysignup/api/AuthAPI;->resolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/orca/easysignup/db/DBHelper$AuthTable;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "duid"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 84
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    const-string v0, "duid"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 88
    :cond_0
    if-eqz v6, :cond_1

    .line 89
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 93
    :cond_1
    if-eqz v7, :cond_3

    .line 94
    iget-object v0, p0, Lcom/sec/orca/easysignup/api/AuthAPI;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getDuid : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    :goto_0
    return-object v7

    .line 87
    :catchall_0
    move-exception v0

    .line 88
    if-eqz v6, :cond_2

    .line 89
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 91
    :cond_2
    throw v0

    .line 96
    :cond_3
    iget-object v0, p0, Lcom/sec/orca/easysignup/api/AuthAPI;->TAG:Ljava/lang/String;

    const-string v1, "getDuid is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getMsisdn()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 126
    const/4 v7, 0x0

    .line 128
    .local v7, "msisdn":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/orca/easysignup/api/AuthAPI;->resolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/orca/easysignup/db/DBHelper$AuthTable;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "msisdn"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 130
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    const-string v0, "msisdn"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 134
    :cond_0
    if-eqz v6, :cond_1

    .line 135
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 139
    :cond_1
    if-eqz v7, :cond_3

    .line 140
    iget-object v0, p0, Lcom/sec/orca/easysignup/api/AuthAPI;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "msisdn : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    :goto_0
    return-object v7

    .line 133
    :catchall_0
    move-exception v0

    .line 134
    if-eqz v6, :cond_2

    .line 135
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 137
    :cond_2
    throw v0

    .line 142
    :cond_3
    iget-object v0, p0, Lcom/sec/orca/easysignup/api/AuthAPI;->TAG:Ljava/lang/String;

    const-string v1, "msisdn is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public declared-synchronized getServiceStatus(I)I
    .locals 10
    .param p1, "serviceId"    # I

    .prologue
    const/4 v9, 0x1

    .line 171
    monitor-enter p0

    const/4 v7, 0x0

    .line 173
    .local v7, "status":I
    :try_start_0
    iget-object v0, p0, Lcom/sec/orca/easysignup/api/AuthAPI;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/orca/easysignup/db/DBHelper$ServiceTable;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "status"

    aput-object v4, v2, v3

    const-string v3, "sid = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v6

    .line 175
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176
    const-string v0, "status"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v7

    .line 179
    :cond_0
    if-eqz v6, :cond_1

    .line 180
    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 184
    :cond_1
    if-ne v7, v9, :cond_3

    .line 185
    iget-object v0, p0, Lcom/sec/orca/easysignup/api/AuthAPI;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getServiceStatus : serviceId ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is ON"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 190
    :goto_0
    monitor-exit p0

    return v7

    .line 178
    :catchall_0
    move-exception v0

    .line 179
    if-eqz v6, :cond_2

    .line 180
    :try_start_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 182
    :cond_2
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 171
    .end local v6    # "cursor":Landroid/database/Cursor;
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 187
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :cond_3
    :try_start_4
    iget-object v0, p0, Lcom/sec/orca/easysignup/api/AuthAPI;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getServiceStatus : serviceId ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") is OFF"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0
.end method

.method public getUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 52
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 53
    .local v7, "builder":Ljava/lang/StringBuilder;
    const/4 v11, 0x0

    .line 55
    .local v11, "url":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/orca/easysignup/api/AuthAPI;->resolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/orca/easysignup/db/DBHelper$GldTable;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "type = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 57
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    const-string v0, "address"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 59
    .local v6, "address":Ljava/lang/String;
    const-string v0, "scheme"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 60
    .local v10, "scheme":Ljava/lang/String;
    const-string v0, "port"

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 62
    .local v9, "port":I
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v11

    .line 65
    .end local v6    # "address":Ljava/lang/String;
    .end local v9    # "port":I
    .end local v10    # "scheme":Ljava/lang/String;
    :cond_0
    if-eqz v8, :cond_1

    .line 66
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 70
    :cond_1
    if-eqz p1, :cond_3

    if-eqz v11, :cond_3

    .line 71
    iget-object v0, p0, Lcom/sec/orca/easysignup/api/AuthAPI;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "type : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / url : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    :goto_0
    return-object v11

    .line 64
    :catchall_0
    move-exception v0

    .line 65
    if-eqz v8, :cond_2

    .line 66
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 68
    :cond_2
    throw v0

    .line 73
    :cond_3
    iget-object v0, p0, Lcom/sec/orca/easysignup/api/AuthAPI;->TAG:Ljava/lang/String;

    const-string v1, "type is null or url is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isAuth()Z
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 24
    const/4 v6, 0x0

    .line 26
    .local v6, "bIsAuth":Z
    new-instance v7, Landroid/net/Uri$Builder;

    invoke-direct {v7}, Landroid/net/Uri$Builder;-><init>()V

    .line 27
    .local v7, "builder":Landroid/net/Uri$Builder;
    const-string v0, "content"

    invoke-virtual {v7, v0}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "com.sec.orca.easysignup.public"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "is_auth"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 30
    iget-object v0, p0, Lcom/sec/orca/easysignup/api/AuthAPI;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v7}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 32
    .local v8, "cursor":Landroid/database/Cursor;
    if-eqz v8, :cond_0

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_0

    .line 33
    const/4 v6, 0x1

    .line 36
    :cond_0
    if-eqz v8, :cond_1

    .line 37
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 41
    :cond_1
    if-eqz v6, :cond_3

    .line 42
    iget-object v0, p0, Lcom/sec/orca/easysignup/api/AuthAPI;->TAG:Ljava/lang/String;

    const-string v1, "isAuth is true"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    :goto_0
    return v6

    .line 35
    :catchall_0
    move-exception v0

    .line 36
    if-eqz v8, :cond_2

    .line 37
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 39
    :cond_2
    throw v0

    .line 44
    :cond_3
    iget-object v0, p0, Lcom/sec/orca/easysignup/api/AuthAPI;->TAG:Ljava/lang/String;

    const-string v1, "isAuth is false"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public login(Ljava/lang/String;)V
    .locals 3
    .param p1, "accessToken"    # Ljava/lang/String;

    .prologue
    .line 149
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.orca.easysignup.ACTION_LOGIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 150
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.orca.easysignup"

    const-string v2, "com.sec.orca.easysignup.receiver.ReqReceiver"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 151
    const-string v1, "accessToken"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 152
    iget-object v1, p0, Lcom/sec/orca/easysignup/api/AuthAPI;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 153
    return-void
.end method

.method public serviceOff(I)V
    .locals 3
    .param p1, "serviceId"    # I

    .prologue
    .line 163
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.orca.easysignup.ACTION_SERVICE_OFF"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 164
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.orca.easysignup"

    const-string v2, "com.sec.orca.easysignup.receiver.ReqReceiver"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 165
    const-string v1, "service_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 166
    iget-object v1, p0, Lcom/sec/orca/easysignup/api/AuthAPI;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 167
    return-void
.end method

.method public serviceOn(I)V
    .locals 3
    .param p1, "serviceId"    # I

    .prologue
    .line 156
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.orca.easysignup.ACTION_SERVICE_ON"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 157
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.orca.easysignup"

    const-string v2, "com.sec.orca.easysignup.receiver.ReqReceiver"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 158
    const-string v1, "service_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 159
    iget-object v1, p0, Lcom/sec/orca/easysignup/api/AuthAPI;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 160
    return-void
.end method
