.class public Lcom/samsung/app/share/via/external/ConvertThread;
.super Ljava/lang/Thread;
.source "ConvertThread.java"


# instance fields
.field private mProgressHandler:Landroid/os/Handler;

.field private nativeAccess:Lcom/samsung/app/share/via/external/NativeAccess;

.field private obj:Lcom/samsung/app/share/via/external/ShareviaObj;


# direct methods
.method public constructor <init>(Lcom/samsung/app/share/via/external/NativeAccess;Lcom/samsung/app/share/via/external/ShareviaObj;Landroid/os/Handler;)V
    .locals 0
    .param p1, "nativeAccess"    # Lcom/samsung/app/share/via/external/NativeAccess;
    .param p2, "obj"    # Lcom/samsung/app/share/via/external/ShareviaObj;
    .param p3, "ProgressHandler"    # Landroid/os/Handler;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/samsung/app/share/via/external/ConvertThread;->nativeAccess:Lcom/samsung/app/share/via/external/NativeAccess;

    .line 31
    iput-object p2, p0, Lcom/samsung/app/share/via/external/ConvertThread;->obj:Lcom/samsung/app/share/via/external/ShareviaObj;

    .line 32
    iput-object p3, p0, Lcom/samsung/app/share/via/external/ConvertThread;->mProgressHandler:Landroid/os/Handler;

    .line 33
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 38
    const/4 v0, 0x0

    .line 39
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/samsung/app/share/via/external/ConvertThread;->nativeAccess:Lcom/samsung/app/share/via/external/NativeAccess;

    iget-object v2, p0, Lcom/samsung/app/share/via/external/ConvertThread;->obj:Lcom/samsung/app/share/via/external/ShareviaObj;

    invoke-virtual {v1, v2}, Lcom/samsung/app/share/via/external/NativeAccess;->startProcessing(Lcom/samsung/app/share/via/external/ShareviaObj;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 41
    iget-object v1, p0, Lcom/samsung/app/share/via/external/ConvertThread;->mProgressHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/app/share/via/external/ConvertThread;->obj:Lcom/samsung/app/share/via/external/ShareviaObj;

    iget-object v2, v2, Lcom/samsung/app/share/via/external/ShareviaObj;->outputFileName:Ljava/lang/String;

    invoke-static {v1, v3, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 46
    :goto_0
    iget-object v1, p0, Lcom/samsung/app/share/via/external/ConvertThread;->mProgressHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 47
    return-void

    .line 44
    :cond_0
    iget-object v1, p0, Lcom/samsung/app/share/via/external/ConvertThread;->mProgressHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-static {v1, v3, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    goto :goto_0
.end method
