.class public Lcom/samsung/app/share/via/external/ShareviaObj;
.super Ljava/lang/Object;
.source "ShareviaObj.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/app/share/via/external/ShareviaObj$transcodeMode;,
        Lcom/samsung/app/share/via/external/ShareviaObj$codecType;,
        Lcom/samsung/app/share/via/external/ShareviaObj$videoResType;
    }
.end annotation


# instance fields
.field OutFileResolution:I

.field assetmngr:Landroid/content/res/AssetManager;

.field audioCodecType:I

.field audioLength:I

.field audioOffset:I

.field endTime:I

.field iconFileName:Ljava/lang/String;

.field inputFileName:Ljava/lang/String;

.field maxOutFileDuration:I

.field maxOutFileSize:I

.field outputFileName:Ljava/lang/String;

.field startTime:I

.field transcodeMode:I

.field videoCodecType:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    return-void
.end method


# virtual methods
.method public getShareViaAudioLength()I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->audioLength:I

    return v0
.end method

.method public getShareViaAudioOffset()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->audioOffset:I

    return v0
.end method

.method public getShareViaTranscodeMode()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->transcodeMode:I

    return v0
.end method

.method public setShareViaAssetmngr(Landroid/content/res/AssetManager;)V
    .locals 0
    .param p1, "assetmngr"    # Landroid/content/res/AssetManager;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->assetmngr:Landroid/content/res/AssetManager;

    .line 66
    return-void
.end method

.method public setShareViaAudioCodec(I)V
    .locals 0
    .param p1, "audiocodec"    # I

    .prologue
    .line 102
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->audioCodecType:I

    .line 103
    return-void
.end method

.method public setShareViaAudioLength(I)V
    .locals 0
    .param p1, "audioLength"    # I

    .prologue
    .line 78
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->audioLength:I

    .line 79
    return-void
.end method

.method public setShareViaAudioOffset(I)V
    .locals 0
    .param p1, "audioOffset"    # I

    .prologue
    .line 72
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->audioOffset:I

    .line 73
    return-void
.end method

.method public setShareViaEndTime(I)V
    .locals 0
    .param p1, "endtime"    # I

    .prologue
    .line 90
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->endTime:I

    .line 91
    return-void
.end method

.method public setShareViaIconFileName(Ljava/lang/String;)V
    .locals 0
    .param p1, "outfilename"    # Ljava/lang/String;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->iconFileName:Ljava/lang/String;

    .line 112
    return-void
.end method

.method public setShareViaInputFilename(Ljava/lang/String;)V
    .locals 0
    .param p1, "inputfilename"    # Ljava/lang/String;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->inputFileName:Ljava/lang/String;

    .line 106
    return-void
.end method

.method public setShareViaOutputFileResolution(I)V
    .locals 0
    .param p1, "OutfileResolution"    # I

    .prologue
    .line 114
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->OutFileResolution:I

    .line 115
    return-void
.end method

.method public setShareViaOutputFilename(Ljava/lang/String;)V
    .locals 0
    .param p1, "outfilename"    # Ljava/lang/String;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->outputFileName:Ljava/lang/String;

    .line 109
    return-void
.end method

.method public setShareViaStartTime(I)V
    .locals 0
    .param p1, "starttime"    # I

    .prologue
    .line 87
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->startTime:I

    .line 88
    return-void
.end method

.method public setShareViaTranscodeMode(I)V
    .locals 0
    .param p1, "transcodeMode"    # I

    .prologue
    .line 84
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->transcodeMode:I

    .line 85
    return-void
.end method

.method public setShareViaVideoCodec(I)V
    .locals 0
    .param p1, "vtVideoCodec"    # I

    .prologue
    .line 99
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->videoCodecType:I

    .line 100
    return-void
.end method

.method public setShareViamaxDuration(I)V
    .locals 0
    .param p1, "maxduration"    # I

    .prologue
    .line 93
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->maxOutFileDuration:I

    .line 94
    return-void
.end method

.method public setShareViamaxSize(I)V
    .locals 0
    .param p1, "maxsize"    # I

    .prologue
    .line 96
    iput p1, p0, Lcom/samsung/app/share/via/external/ShareviaObj;->maxOutFileSize:I

    .line 97
    return-void
.end method
