.class public Lcom/samsung/scloud/DropboxAPI$CancelableUpload;
.super Ljava/lang/Object;
.source "DropboxAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/scloud/DropboxAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CancelableUpload"
.end annotation


# instance fields
.field private destinationFolderIdOrPath:Ljava/lang/String;

.field private doRetryUpload:Z

.field private overwrite:Z

.field private parentRev:Ljava/lang/String;

.field private progressListener:Lcom/samsung/scloud/response/ProgressListener;

.field private sourceFile:Ljava/io/File;

.field final synthetic this$0:Lcom/samsung/scloud/DropboxAPI;

.field private uploadRequest:Lcom/dropbox/client2/DropboxAPI$UploadRequest;


# direct methods
.method public constructor <init>(Lcom/samsung/scloud/DropboxAPI;Ljava/io/File;Ljava/lang/String;ZLjava/lang/String;Lcom/samsung/scloud/response/ProgressListener;)V
    .locals 1
    .param p2, "sourceFile"    # Ljava/io/File;
    .param p3, "destinationFolderIdOrPath"    # Ljava/lang/String;
    .param p4, "overwrite"    # Z
    .param p5, "parentRev"    # Ljava/lang/String;
    .param p6, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;

    .prologue
    .line 2626
    iput-object p1, p0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->this$0:Lcom/samsung/scloud/DropboxAPI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2617
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->doRetryUpload:Z

    .line 2627
    iput-object p2, p0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->sourceFile:Ljava/io/File;

    .line 2628
    iput-object p3, p0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->destinationFolderIdOrPath:Ljava/lang/String;

    .line 2629
    iput-boolean p4, p0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->overwrite:Z

    .line 2630
    iput-object p5, p0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->parentRev:Ljava/lang/String;

    .line 2631
    iput-object p6, p0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->progressListener:Lcom/samsung/scloud/response/ProgressListener;

    .line 2632
    return-void
.end method

.method static synthetic access$700(Lcom/samsung/scloud/DropboxAPI$CancelableUpload;)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/scloud/DropboxAPI$CancelableUpload;

    .prologue
    .line 2614
    iget-object v0, p0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->sourceFile:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/scloud/DropboxAPI$CancelableUpload;)Lcom/samsung/scloud/response/ProgressListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/scloud/DropboxAPI$CancelableUpload;

    .prologue
    .line 2614
    iget-object v0, p0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->progressListener:Lcom/samsung/scloud/response/ProgressListener;

    return-object v0
.end method


# virtual methods
.method public cancelUpload()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2727
    iput-boolean v0, p0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->doRetryUpload:Z

    .line 2728
    iget-object v1, p0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->uploadRequest:Lcom/dropbox/client2/DropboxAPI$UploadRequest;

    if-eqz v1, :cond_0

    .line 2729
    iget-object v0, p0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->uploadRequest:Lcom/dropbox/client2/DropboxAPI$UploadRequest;

    invoke-interface {v0}, Lcom/dropbox/client2/DropboxAPI$UploadRequest;->abort()V

    .line 2730
    const/4 v0, 0x1

    .line 2732
    :cond_0
    return v0
.end method

.method public putFileCancelable()Lcom/samsung/scloud/data/SCloudFile;
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2636
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->sourceFile:Ljava/io/File;

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->sourceFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->destinationFolderIdOrPath:Ljava/lang/String;

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->destinationFolderIdOrPath:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x1

    if-ge v3, v4, :cond_1

    .line 2640
    :cond_0
    :try_start_0
    new-instance v3, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v3}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v3
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudInvalidParameterException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2641
    :catch_0
    move-exception v2

    .line 2643
    .local v2, "e":Lcom/samsung/scloud/exception/SCloudInvalidParameterException;
    invoke-virtual {v2}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;->printStackTrace()V

    .line 2647
    .end local v2    # "e":Lcom/samsung/scloud/exception/SCloudInvalidParameterException;
    :cond_1
    new-instance v8, Lcom/samsung/scloud/DropboxAPI$CancelableUpload$1;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Lcom/samsung/scloud/DropboxAPI$CancelableUpload$1;-><init>(Lcom/samsung/scloud/DropboxAPI$CancelableUpload;)V

    .line 2667
    .local v8, "listener":Lcom/dropbox/client2/ProgressListener;
    const/4 v5, 0x0

    .line 2668
    .local v5, "fis":Ljava/io/FileInputStream;
    const/16 v19, 0x0

    .line 2669
    .local v19, "uploadedFile":Lcom/dropbox/client2/DropboxAPI$Entry;
    const/16 v18, 0x0

    .local v18, "i":I
    move-object/from16 v17, v5

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .local v17, "fis":Ljava/io/FileInputStream;
    :goto_0
    const/4 v3, 0x5

    move/from16 v0, v18

    if-ge v0, v3, :cond_a

    .line 2670
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->doRetryUpload:Z

    if-nez v3, :cond_2

    .line 2671
    new-instance v3, Lcom/samsung/scloud/exception/SCloudCancelException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "file upload cancelled"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->sourceFile:Ljava/io/File;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/samsung/scloud/exception/SCloudCancelException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 2674
    :cond_2
    :try_start_1
    new-instance v5, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->sourceFile:Ljava/io/File;

    invoke-direct {v5, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 2676
    .end local v17    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :try_start_2
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->overwrite:Z

    if-eqz v3, :cond_5

    .line 2677
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->this$0:Lcom/samsung/scloud/DropboxAPI;

    # getter for: Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;
    invoke-static {v3}, Lcom/samsung/scloud/DropboxAPI;->access$600(Lcom/samsung/scloud/DropboxAPI;)Lcom/dropbox/client2/SamsungDropboxAPI;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->destinationFolderIdOrPath:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->sourceFile:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->sourceFile:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-virtual/range {v3 .. v8}, Lcom/dropbox/client2/SamsungDropboxAPI;->putFileOverwriteRequest(Ljava/lang/String;Ljava/io/InputStream;JLcom/dropbox/client2/ProgressListener;)Lcom/dropbox/client2/DropboxAPI$UploadRequest;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->uploadRequest:Lcom/dropbox/client2/DropboxAPI$UploadRequest;

    .line 2681
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->doRetryUpload:Z

    if-nez v3, :cond_3

    .line 2682
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->uploadRequest:Lcom/dropbox/client2/DropboxAPI$UploadRequest;

    invoke-interface {v3}, Lcom/dropbox/client2/DropboxAPI$UploadRequest;->abort()V

    .line 2683
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->uploadRequest:Lcom/dropbox/client2/DropboxAPI$UploadRequest;

    invoke-interface {v3}, Lcom/dropbox/client2/DropboxAPI$UploadRequest;->upload()Lcom/dropbox/client2/DropboxAPI$Entry;

    move-result-object v19

    .line 2696
    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->progressListener:Lcom/samsung/scloud/response/ProgressListener;

    if-eqz v3, :cond_4

    .line 2697
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->progressListener:Lcom/samsung/scloud/response/ProgressListener;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->sourceFile:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x2

    const-wide/16 v10, 0x64

    invoke-interface {v3, v4, v6, v10, v11}, Lcom/samsung/scloud/response/ProgressListener;->onUpdate(Ljava/lang/String;IJ)V

    .line 2701
    :cond_4
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    .line 2720
    :goto_2
    if-eqz v19, :cond_9

    .line 2721
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->this$0:Lcom/samsung/scloud/DropboxAPI;

    move-object/from16 v0, v19

    # invokes: Lcom/samsung/scloud/DropboxAPI;->parseEntry(Lcom/dropbox/client2/DropboxAPI$Entry;)Lcom/samsung/scloud/data/SCloudNode;
    invoke-static {v3, v0}, Lcom/samsung/scloud/DropboxAPI;->access$900(Lcom/samsung/scloud/DropboxAPI;Lcom/dropbox/client2/DropboxAPI$Entry;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v3

    check-cast v3, Lcom/samsung/scloud/data/SCloudFile;

    .line 2723
    :goto_3
    return-object v3

    .line 2686
    :cond_5
    :try_start_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->this$0:Lcom/samsung/scloud/DropboxAPI;

    # getter for: Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;
    invoke-static {v3}, Lcom/samsung/scloud/DropboxAPI;->access$600(Lcom/samsung/scloud/DropboxAPI;)Lcom/dropbox/client2/SamsungDropboxAPI;

    move-result-object v9

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->destinationFolderIdOrPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->sourceFile:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->sourceFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v12

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->parentRev:Ljava/lang/String;

    move-object v11, v5

    move-object v15, v8

    invoke-virtual/range {v9 .. v15}, Lcom/dropbox/client2/SamsungDropboxAPI;->putFileRequest(Ljava/lang/String;Ljava/io/InputStream;JLjava/lang/String;Lcom/dropbox/client2/ProgressListener;)Lcom/dropbox/client2/DropboxAPI$UploadRequest;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->uploadRequest:Lcom/dropbox/client2/DropboxAPI$UploadRequest;

    .line 2690
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->doRetryUpload:Z

    if-nez v3, :cond_6

    .line 2691
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->uploadRequest:Lcom/dropbox/client2/DropboxAPI$UploadRequest;

    invoke-interface {v3}, Lcom/dropbox/client2/DropboxAPI$UploadRequest;->abort()V

    .line 2692
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->uploadRequest:Lcom/dropbox/client2/DropboxAPI$UploadRequest;

    invoke-interface {v3}, Lcom/dropbox/client2/DropboxAPI$UploadRequest;->upload()Lcom/dropbox/client2/DropboxAPI$Entry;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    move-result-object v19

    goto :goto_1

    .line 2704
    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "fis":Ljava/io/FileInputStream;
    :catch_1
    move-exception v2

    move-object/from16 v5, v17

    .line 2705
    .end local v17    # "fis":Ljava/io/FileInputStream;
    .local v2, "e":Ljava/lang/Exception;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :goto_4
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->uploadRequest:Lcom/dropbox/client2/DropboxAPI$UploadRequest;

    .line 2707
    if-eqz v5, :cond_7

    .line 2708
    :try_start_4
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 2713
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->this$0:Lcom/samsung/scloud/DropboxAPI;

    # invokes: Lcom/samsung/scloud/DropboxAPI;->isRetryNeeded(Ljava/lang/Exception;)Z
    invoke-static {v3, v2}, Lcom/samsung/scloud/DropboxAPI;->access$300(Lcom/samsung/scloud/DropboxAPI;Ljava/lang/Exception;)Z

    move-result v3

    if-eqz v3, :cond_8

    const/4 v3, 0x4

    move/from16 v0, v18

    if-ge v0, v3, :cond_8

    .line 2714
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->this$0:Lcom/samsung/scloud/DropboxAPI;

    move/from16 v0, v18

    # invokes: Lcom/samsung/scloud/DropboxAPI;->doBackOff(I)V
    invoke-static {v3, v0}, Lcom/samsung/scloud/DropboxAPI;->access$400(Lcom/samsung/scloud/DropboxAPI;I)V

    .line 2669
    :goto_5
    add-int/lit8 v18, v18, 0x1

    move-object/from16 v17, v5

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_0

    .line 2709
    .end local v17    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    :catch_2
    move-exception v16

    .line 2710
    .local v16, "e1":Ljava/io/IOException;
    new-instance v3, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v4, "file close failed"

    invoke-direct {v3, v4}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 2717
    .end local v16    # "e1":Ljava/io/IOException;
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->this$0:Lcom/samsung/scloud/DropboxAPI;

    # invokes: Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V
    invoke-static {v3, v2}, Lcom/samsung/scloud/DropboxAPI;->access$500(Lcom/samsung/scloud/DropboxAPI;Ljava/lang/Exception;)V

    goto :goto_5

    .line 2723
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_9
    const/4 v3, 0x0

    goto/16 :goto_3

    .line 2704
    :catch_3
    move-exception v2

    goto :goto_4

    .end local v5    # "fis":Ljava/io/FileInputStream;
    .restart local v17    # "fis":Ljava/io/FileInputStream;
    :cond_a
    move-object/from16 v5, v17

    .end local v17    # "fis":Ljava/io/FileInputStream;
    .restart local v5    # "fis":Ljava/io/FileInputStream;
    goto/16 :goto_2
.end method
