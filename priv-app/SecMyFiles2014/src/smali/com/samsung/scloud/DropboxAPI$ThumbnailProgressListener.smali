.class Lcom/samsung/scloud/DropboxAPI$ThumbnailProgressListener;
.super Lcom/dropbox/client2/ProgressListener;
.source "DropboxAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/scloud/DropboxAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ThumbnailProgressListener"
.end annotation


# instance fields
.field private filename:Ljava/lang/String;

.field private progressListener:Lcom/samsung/scloud/response/ProgressListener;


# direct methods
.method public constructor <init>(Lcom/samsung/scloud/response/ProgressListener;Ljava/lang/String;)V
    .locals 0
    .param p1, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .param p2, "filename"    # Ljava/lang/String;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/dropbox/client2/ProgressListener;-><init>()V

    .line 106
    iput-object p1, p0, Lcom/samsung/scloud/DropboxAPI$ThumbnailProgressListener;->progressListener:Lcom/samsung/scloud/response/ProgressListener;

    .line 107
    iput-object p2, p0, Lcom/samsung/scloud/DropboxAPI$ThumbnailProgressListener;->filename:Ljava/lang/String;

    .line 108
    return-void
.end method


# virtual methods
.method public onProgress(JJ)V
    .locals 9
    .param p1, "bytes"    # J
    .param p3, "total"    # J

    .prologue
    .line 112
    iget-object v2, p0, Lcom/samsung/scloud/DropboxAPI$ThumbnailProgressListener;->progressListener:Lcom/samsung/scloud/response/ProgressListener;

    if-eqz v2, :cond_0

    .line 113
    cmp-long v2, p1, p3

    if-gez v2, :cond_1

    .line 114
    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    long-to-double v4, p1

    mul-double/2addr v2, v4

    long-to-double v4, p3

    div-double/2addr v2, v4

    double-to-long v0, v2

    .line 115
    .local v0, "prog":J
    iget-object v2, p0, Lcom/samsung/scloud/DropboxAPI$ThumbnailProgressListener;->progressListener:Lcom/samsung/scloud/response/ProgressListener;

    iget-object v3, p0, Lcom/samsung/scloud/DropboxAPI$ThumbnailProgressListener;->filename:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4, v0, v1}, Lcom/samsung/scloud/response/ProgressListener;->onUpdate(Ljava/lang/String;IJ)V

    .line 129
    .end local v0    # "prog":J
    :cond_0
    :goto_0
    return-void

    .line 121
    :cond_1
    iget-object v2, p0, Lcom/samsung/scloud/DropboxAPI$ThumbnailProgressListener;->progressListener:Lcom/samsung/scloud/response/ProgressListener;

    iget-object v3, p0, Lcom/samsung/scloud/DropboxAPI$ThumbnailProgressListener;->filename:Ljava/lang/String;

    const/4 v4, 0x2

    const-wide/16 v6, 0x64

    invoke-interface {v2, v3, v4, v6, v7}, Lcom/samsung/scloud/response/ProgressListener;->onUpdate(Ljava/lang/String;IJ)V

    goto :goto_0
.end method
