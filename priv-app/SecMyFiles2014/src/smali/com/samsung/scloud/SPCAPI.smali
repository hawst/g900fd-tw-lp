.class public final Lcom/samsung/scloud/SPCAPI;
.super Ljava/lang/Object;
.source "SPCAPI.java"

# interfaces
.implements Lcom/samsung/scloud/SCloudAPI;


# instance fields
.field private BASE_URI:Ljava/lang/String;

.field private IP_ADDRESS:Ljava/lang/String;

.field private PORT:Ljava/lang/String;

.field private authToken:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "apiKey"    # Ljava/lang/String;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/scloud/SPCAPI;->authToken:Ljava/lang/String;

    .line 55
    const-string v0, "http://"

    iput-object v0, p0, Lcom/samsung/scloud/SPCAPI;->BASE_URI:Ljava/lang/String;

    .line 63
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "ipAddress"    # Ljava/lang/String;
    .param p2, "port"    # Ljava/lang/String;

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/scloud/SPCAPI;->authToken:Ljava/lang/String;

    .line 55
    const-string v0, "http://"

    iput-object v0, p0, Lcom/samsung/scloud/SPCAPI;->BASE_URI:Ljava/lang/String;

    .line 66
    iput-object p1, p0, Lcom/samsung/scloud/SPCAPI;->IP_ADDRESS:Ljava/lang/String;

    .line 67
    iput-object p2, p0, Lcom/samsung/scloud/SPCAPI;->PORT:Ljava/lang/String;

    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/scloud/SPCAPI;->BASE_URI:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/scloud/SPCAPI;->IP_ADDRESS:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/scloud/SPCAPI;->PORT:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/spc/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/scloud/SPCAPI;->BASE_URI:Ljava/lang/String;

    .line 69
    return-void
.end method

.method private executeRestAPI(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 12
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "isPost"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    .line 729
    const/4 v7, 0x0

    .line 730
    .local v7, "source":Ljava/io/InputStream;
    const-string v9, "SCLOUD_SDK"

    const-string v10, "executeRestAPI -- start"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 731
    const/4 v2, 0x0

    .line 732
    .local v2, "httpPost":Lorg/apache/http/client/methods/HttpPost;
    const/4 v1, 0x0

    .line 733
    .local v1, "httpGet":Lorg/apache/http/client/methods/HttpGet;
    if-eqz p2, :cond_0

    .line 734
    new-instance v2, Lorg/apache/http/client/methods/HttpPost;

    .end local v2    # "httpPost":Lorg/apache/http/client/methods/HttpPost;
    invoke-direct {v2, p1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 739
    .restart local v2    # "httpPost":Lorg/apache/http/client/methods/HttpPost;
    :goto_0
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v0}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 740
    .local v0, "httpClient":Lorg/apache/http/impl/client/DefaultHttpClient;
    invoke-virtual {v0}, Lorg/apache/http/impl/client/DefaultHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v9

    const-string v10, "SDK"

    invoke-static {v9, v10}, Lorg/apache/http/params/HttpProtocolParams;->setUserAgent(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    .line 741
    if-eqz p2, :cond_1

    .line 742
    invoke-virtual {v0, v2}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v3

    .line 746
    .local v3, "httpResponse":Lorg/apache/http/HttpResponse;
    :goto_1
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v9

    invoke-interface {v9}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v8

    .line 747
    .local v8, "statusCode":I
    const/16 v9, 0xc8

    if-eq v8, v9, :cond_2

    .line 748
    const-string v9, "SCLOUD_SDK"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Error "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " for URL "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 749
    new-instance v9, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Error "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " for URL "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 736
    .end local v0    # "httpClient":Lorg/apache/http/impl/client/DefaultHttpClient;
    .end local v3    # "httpResponse":Lorg/apache/http/HttpResponse;
    .end local v8    # "statusCode":I
    :cond_0
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    .end local v1    # "httpGet":Lorg/apache/http/client/methods/HttpGet;
    invoke-direct {v1, p1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .restart local v1    # "httpGet":Lorg/apache/http/client/methods/HttpGet;
    goto :goto_0

    .line 744
    .restart local v0    # "httpClient":Lorg/apache/http/impl/client/DefaultHttpClient;
    :cond_1
    invoke-virtual {v0, v1}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v3

    .restart local v3    # "httpResponse":Lorg/apache/http/HttpResponse;
    goto :goto_1

    .line 751
    .restart local v8    # "statusCode":I
    :cond_2
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v9

    invoke-interface {v9}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v7

    .line 753
    new-instance v5, Ljava/io/BufferedReader;

    new-instance v9, Ljava/io/InputStreamReader;

    invoke-direct {v9, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v5, v9}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 754
    .local v5, "reader":Ljava/io/BufferedReader;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 755
    .local v6, "sb":Ljava/lang/StringBuilder;
    const/4 v4, 0x0

    .line 756
    .local v4, "line":Ljava/lang/String;
    :goto_2
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 757
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 759
    :cond_3
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V

    .line 760
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v9

    invoke-interface {v9}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 761
    const-string v9, "SCLOUD_SDK"

    const-string v10, "executeRestAPI -- end"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 762
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    return-object v9
.end method


# virtual methods
.method public addComment(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/Comment;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 514
    const/4 v0, 0x0

    return-object v0
.end method

.method public copy(Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/scloud/data/SCloudNode;
    .locals 1
    .param p1, "fromPath"    # Ljava/lang/String;
    .param p2, "toPath"    # Ljava/lang/String;
    .param p3, "overwrite"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 287
    const/4 v0, 0x0

    return-object v0
.end method

.method public createRepository(Lcom/samsung/scloud/data/Repository;)Lcom/samsung/scloud/data/Repository;
    .locals 1
    .param p1, "repo"    # Lcom/samsung/scloud/data/Repository;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 117
    const/4 v0, 0x0

    return-object v0
.end method

.method public createRepository(Ljava/lang/String;J)Lcom/samsung/scloud/data/Repository;
    .locals 1
    .param p1, "repositoryName"    # Ljava/lang/String;
    .param p2, "spaceAmount"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 111
    const/4 v0, 0x0

    return-object v0
.end method

.method public createShareURL(Ljava/lang/String;Ljava/lang/String;J)Lcom/samsung/scloud/data/ShareInfo;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .param p3, "endTimestamp"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 589
    const/4 v0, 0x0

    return-object v0
.end method

.method public delete(Ljava/lang/String;)Z
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 347
    const/4 v0, 0x0

    return v0
.end method

.method public deleteDeviceProfile()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 99
    return-void
.end method

.method public deleteRepository(Ljava/lang/String;)V
    .locals 0
    .param p1, "repositoryName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 149
    return-void
.end method

.method public editComment(Ljava/lang/String;JLjava/lang/String;)Lcom/samsung/scloud/data/Comment;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "commentID"    # J
    .param p4, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 508
    const/4 v0, 0x0

    return-object v0
.end method

.method public finishAuth()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 630
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getAuthToken()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 641
    iget-object v0, p0, Lcom/samsung/scloud/SPCAPI;->authToken:Ljava/lang/String;

    return-object v0
.end method

.method public getCollaboration(Ljava/lang/String;)Lcom/samsung/scloud/data/Collaboration;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 538
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCollaborations()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Collaboration;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 550
    const/4 v0, 0x0

    return-object v0
.end method

.method public getComments(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Comment;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 502
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDeltaList(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;
    .locals 1
    .param p1, "cursor"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/scloud/data/SCloudNodeList",
            "<",
            "Lcom/samsung/scloud/data/SCloudNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 703
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getDeviceProfile()Lcom/samsung/scloud/data/Device;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 74
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDeviceProfile(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 80
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDownloadURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 698
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/scloud/response/ProgressListener;)V
    .locals 0
    .param p1, "localFile"    # Ljava/io/File;
    .param p2, "serverFilePath"    # Ljava/lang/String;
    .param p3, "version"    # Ljava/lang/String;
    .param p4, "overwrite"    # Z
    .param p5, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 475
    return-void
.end method

.method public getFile(Ljava/io/File;Ljava/lang/String;ZLcom/samsung/scloud/response/ProgressListener;)V
    .locals 0
    .param p1, "localFile"    # Ljava/io/File;
    .param p2, "serverFilePath"    # Ljava/lang/String;
    .param p3, "overwrite"    # Z
    .param p4, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 456
    return-void
.end method

.method public getFileInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 281
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFilesAsZip(Ljava/io/File;Ljava/util/List;ZLcom/samsung/scloud/response/ProgressListener;)V
    .locals 0
    .param p1, "localFile"    # Ljava/io/File;
    .param p2, "path"    # Ljava/util/List;
    .param p3, "overwrite"    # Z
    .param p4, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 490
    return-void
.end method

.method public getFolderInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;
    .locals 14
    .param p1, "folderPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 159
    const-string v11, "SCLOUD_SDK"

    const-string v12, "getFolderInfo file start"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    if-nez p1, :cond_0

    .line 162
    new-instance v11, Lcom/samsung/scloud/exception/SCloudException;

    const-string v12, "Worng Input Parameters"

    invoke-direct {v11, v12}, Lcom/samsung/scloud/exception/SCloudException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 165
    :cond_0
    const/4 v2, 0x0

    .line 166
    .local v2, "folderURL":Ljava/lang/String;
    const/4 v8, 0x0

    .line 168
    .local v8, "sCloudFolder":Lcom/samsung/scloud/data/SCloudFolder;
    :try_start_0
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/samsung/scloud/SPCAPI;->BASE_URI:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "folder/GetFolderMetaInfo?UUID="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "&RepositoryName="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "&FolderId="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 171
    const-string v11, "SCLOUD_SDK"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getFolderInfo -- Request URL  "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    const/4 v11, 0x0

    invoke-direct {p0, v2, v11}, Lcom/samsung/scloud/SPCAPI;->executeRestAPI(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    .line 174
    .local v7, "response":Ljava/lang/String;
    const-string v11, "SCLOUD_SDK"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "RESPONSE----------> "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    new-instance v10, Lorg/json/JSONTokener;

    invoke-direct {v10, v7}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    .line 177
    .local v10, "tokener":Lorg/json/JSONTokener;
    invoke-virtual {v10}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/json/JSONObject;

    .line 178
    .local v6, "object":Lorg/json/JSONObject;
    const-string v11, "FolderMetaInfo"

    invoke-virtual {v6, v11}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    .line 179
    new-instance v9, Lcom/samsung/scloud/data/SCloudFolder;

    invoke-direct {v9}, Lcom/samsung/scloud/data/SCloudFolder;-><init>()V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_3

    .line 180
    .end local v8    # "sCloudFolder":Lcom/samsung/scloud/data/SCloudFolder;
    .local v9, "sCloudFolder":Lcom/samsung/scloud/data/SCloudFolder;
    :try_start_1
    const-string v11, "RepositoryName"

    invoke-virtual {v6, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Lcom/samsung/scloud/data/SCloudFolder;->setRepositoryName(Ljava/lang/String;)V

    .line 181
    const-string v11, "Description"

    invoke-virtual {v6, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Lcom/samsung/scloud/data/SCloudFolder;->setDescription(Ljava/lang/String;)V

    .line 182
    const-string v11, "FileCount"

    invoke-virtual {v6, v11}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v12

    invoke-virtual {v9, v12, v13}, Lcom/samsung/scloud/data/SCloudFolder;->setFileCount(J)V

    .line 183
    const-string v11, "HasCollaborators"

    invoke-virtual {v6, v11}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v11

    invoke-virtual {v9, v11}, Lcom/samsung/scloud/data/SCloudFolder;->setHasCollaborators(Z)V

    .line 184
    const-string v11, "ShareURL"

    invoke-virtual {v6, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Lcom/samsung/scloud/data/SCloudFolder;->setShareURL(Ljava/lang/String;)V

    .line 185
    const-string v11, "IsShared"

    invoke-virtual {v6, v11}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v11

    invoke-virtual {v9, v11}, Lcom/samsung/scloud/data/SCloudFolder;->setShared(Z)V

    .line 186
    const-string v11, "Name"

    invoke-virtual {v6, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Lcom/samsung/scloud/data/SCloudFolder;->setName(Ljava/lang/String;)V

    .line 187
    new-instance v1, Ljava/util/Date;

    const-string v11, "CreatedTimeStamp"

    invoke-virtual {v6, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v1, v11}, Ljava/util/Date;-><init>(Ljava/lang/String;)V

    .line 188
    .local v1, "date":Ljava/util/Date;
    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v12

    invoke-virtual {v9, v12, v13}, Lcom/samsung/scloud/data/SCloudFolder;->setCreatedTimestamp(J)V

    .line 189
    const-string v11, "AbsolutePath"

    invoke-virtual {v6, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Lcom/samsung/scloud/data/SCloudFolder;->setAbsolutePath(Ljava/lang/String;)V

    .line 190
    new-instance v1, Ljava/util/Date;

    .end local v1    # "date":Ljava/util/Date;
    const-string v11, "UpdatedAccessTime"

    invoke-virtual {v6, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v1, v11}, Ljava/util/Date;-><init>(Ljava/lang/String;)V

    .line 191
    .restart local v1    # "date":Ljava/util/Date;
    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v12

    invoke-virtual {v9, v12, v13}, Lcom/samsung/scloud/data/SCloudFolder;->setUpdatedTimestamp(J)V

    .line 192
    const-string v11, "IsDeleted"

    invoke-virtual {v6, v11}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v11

    invoke-virtual {v9, v11}, Lcom/samsung/scloud/data/SCloudFolder;->setDeleted(Z)V

    .line 193
    const-string v11, "Id"

    invoke-virtual {v6, v11}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Lcom/samsung/scloud/data/SCloudFolder;->setId(Ljava/lang/String;)V

    .line 194
    const-string v11, "Size"

    invoke-virtual {v6, v11}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v12

    invoke-virtual {v9, v12, v13}, Lcom/samsung/scloud/data/SCloudFolder;->setSize(J)V

    .line 195
    const-string v11, "FileName"

    invoke-virtual {v6, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Lcom/samsung/scloud/data/SCloudFolder;->setName(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_6
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_4

    .line 209
    const-string v11, "SCLOUD_SDK"

    const-string v12, "getFolderInfo file end"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    return-object v9

    .line 196
    .end local v1    # "date":Ljava/util/Date;
    .end local v6    # "object":Lorg/json/JSONObject;
    .end local v7    # "response":Ljava/lang/String;
    .end local v9    # "sCloudFolder":Lcom/samsung/scloud/data/SCloudFolder;
    .end local v10    # "tokener":Lorg/json/JSONTokener;
    .restart local v8    # "sCloudFolder":Lcom/samsung/scloud/data/SCloudFolder;
    :catch_0
    move-exception v5

    .line 197
    .local v5, "malformedURLException":Ljava/net/MalformedURLException;
    :goto_0
    const-string v11, "SCLOUD_SDK"

    const-string v12, "getFolderInfo - MalformedURLException"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    new-instance v11, Lcom/samsung/scloud/exception/SCloudException;

    invoke-direct {v11}, Lcom/samsung/scloud/exception/SCloudException;-><init>()V

    throw v11

    .line 199
    .end local v5    # "malformedURLException":Ljava/net/MalformedURLException;
    :catch_1
    move-exception v0

    .line 200
    .local v0, "clientProtocolException":Lorg/apache/http/client/ClientProtocolException;
    :goto_1
    const-string v11, "SCLOUD_SDK"

    const-string v12, "getFolderInfo - ClientProtocolException"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    new-instance v11, Lcom/samsung/scloud/exception/SCloudException;

    invoke-direct {v11}, Lcom/samsung/scloud/exception/SCloudException;-><init>()V

    throw v11

    .line 202
    .end local v0    # "clientProtocolException":Lorg/apache/http/client/ClientProtocolException;
    :catch_2
    move-exception v3

    .line 203
    .local v3, "ioException":Ljava/io/IOException;
    :goto_2
    const-string v11, "SCLOUD_SDK"

    const-string v12, "getFolderInfo - IOException"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    new-instance v11, Lcom/samsung/scloud/exception/SCloudIOException;

    invoke-direct {v11}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>()V

    throw v11

    .line 205
    .end local v3    # "ioException":Ljava/io/IOException;
    :catch_3
    move-exception v4

    .line 206
    .local v4, "jsonException":Lorg/json/JSONException;
    :goto_3
    const-string v11, "SCLOUD_SDK"

    const-string v12, "getFolderInfo - JSONException"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    new-instance v11, Lcom/samsung/scloud/exception/SCloudIOException;

    invoke-direct {v11}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>()V

    throw v11

    .line 205
    .end local v4    # "jsonException":Lorg/json/JSONException;
    .end local v8    # "sCloudFolder":Lcom/samsung/scloud/data/SCloudFolder;
    .restart local v6    # "object":Lorg/json/JSONObject;
    .restart local v7    # "response":Ljava/lang/String;
    .restart local v9    # "sCloudFolder":Lcom/samsung/scloud/data/SCloudFolder;
    .restart local v10    # "tokener":Lorg/json/JSONTokener;
    :catch_4
    move-exception v4

    move-object v8, v9

    .end local v9    # "sCloudFolder":Lcom/samsung/scloud/data/SCloudFolder;
    .restart local v8    # "sCloudFolder":Lcom/samsung/scloud/data/SCloudFolder;
    goto :goto_3

    .line 202
    .end local v8    # "sCloudFolder":Lcom/samsung/scloud/data/SCloudFolder;
    .restart local v9    # "sCloudFolder":Lcom/samsung/scloud/data/SCloudFolder;
    :catch_5
    move-exception v3

    move-object v8, v9

    .end local v9    # "sCloudFolder":Lcom/samsung/scloud/data/SCloudFolder;
    .restart local v8    # "sCloudFolder":Lcom/samsung/scloud/data/SCloudFolder;
    goto :goto_2

    .line 199
    .end local v8    # "sCloudFolder":Lcom/samsung/scloud/data/SCloudFolder;
    .restart local v9    # "sCloudFolder":Lcom/samsung/scloud/data/SCloudFolder;
    :catch_6
    move-exception v0

    move-object v8, v9

    .end local v9    # "sCloudFolder":Lcom/samsung/scloud/data/SCloudFolder;
    .restart local v8    # "sCloudFolder":Lcom/samsung/scloud/data/SCloudFolder;
    goto :goto_1

    .line 196
    .end local v8    # "sCloudFolder":Lcom/samsung/scloud/data/SCloudFolder;
    .restart local v9    # "sCloudFolder":Lcom/samsung/scloud/data/SCloudFolder;
    :catch_7
    move-exception v5

    move-object v8, v9

    .end local v9    # "sCloudFolder":Lcom/samsung/scloud/data/SCloudFolder;
    .restart local v8    # "sCloudFolder":Lcom/samsung/scloud/data/SCloudFolder;
    goto :goto_0
.end method

.method public getList(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;
    .locals 22
    .param p1, "folderPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 215
    const-string v19, "SCLOUD_SDK"

    const-string v20, "getList start"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    if-nez p1, :cond_0

    .line 218
    new-instance v19, Lcom/samsung/scloud/exception/SCloudException;

    const-string v20, "Worng Input Parameters"

    invoke-direct/range {v19 .. v20}, Lcom/samsung/scloud/exception/SCloudException;-><init>(Ljava/lang/String;)V

    throw v19

    .line 221
    :cond_0
    const/4 v4, 0x0

    .line 222
    .local v4, "fileURL":Ljava/lang/String;
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/scloud/SPCAPI;->getFolderInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;

    move-result-object v16

    .line 224
    .local v16, "sCloudFolder":Lcom/samsung/scloud/data/SCloudFolder;
    :try_start_0
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/scloud/SPCAPI;->BASE_URI:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "folder/GetFolder?UUID="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "&RepositoryName="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "&FolderId="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 227
    const-string v19, "SCLOUD_SDK"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "getList -- Request URL  "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v4, v1}, Lcom/samsung/scloud/SPCAPI;->executeRestAPI(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v14

    .line 231
    .local v14, "response":Ljava/lang/String;
    const-string v19, "SCLOUD_SDK"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "RESPONSE----------> "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    new-instance v13, Lorg/json/JSONObject;

    invoke-direct {v13, v14}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 236
    .local v13, "myjson":Lorg/json/JSONObject;
    const-string v19, "GetFolder"

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v15

    .line 238
    .local v15, "responseArray":Lorg/json/JSONArray;
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v15, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 239
    .local v5, "files":Lorg/json/JSONObject;
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v15, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    .line 241
    .local v7, "folders":Lorg/json/JSONObject;
    const-string v19, "FileInfo"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 242
    .local v3, "fileArray":Lorg/json/JSONArray;
    const-string v19, "FolderInfo"

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 244
    .local v6, "folderArray":Lorg/json/JSONArray;
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 245
    .local v17, "scloudFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/SCloudFile;>;"
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v19

    move/from16 v0, v19

    if-ge v8, v0, :cond_1

    .line 246
    const-string v19, "SCLOUD_SDK"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, " Fetching FILE information for ID ----------> "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v3, v8}, Lorg/json/JSONArray;->getInt(I)I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    invoke-virtual {v3, v8}, Lorg/json/JSONArray;->getInt(I)I

    move-result v19

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    .line 248
    .local v9, "id":Ljava/lang/Long;
    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/samsung/scloud/SPCAPI;->getFileInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFile;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 245
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 250
    .end local v9    # "id":Ljava/lang/Long;
    :cond_1
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 251
    .local v18, "scloudFolders":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/SCloudFolder;>;"
    const/4 v8, 0x0

    :goto_1
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v19

    move/from16 v0, v19

    if-ge v8, v0, :cond_2

    .line 252
    const-string v19, "SCLOUD_SDK"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Fetching FOLDER information for ID ----------> "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v6, v8}, Lorg/json/JSONArray;->getInt(I)I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    invoke-virtual {v6, v8}, Lorg/json/JSONArray;->getInt(I)I

    move-result v19

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    .line 255
    .restart local v9    # "id":Ljava/lang/Long;
    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/samsung/scloud/SPCAPI;->getFolderInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 251
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 258
    .end local v9    # "id":Ljava/lang/Long;
    :cond_2
    invoke-virtual/range {v16 .. v17}, Lcom/samsung/scloud/data/SCloudFolder;->setFiles(Ljava/util/ArrayList;)V

    .line 259
    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/scloud/data/SCloudFolder;->setFolders(Ljava/util/ArrayList;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_3

    .line 275
    const-string v19, "SCLOUD_SDK"

    const-string v20, "getFileInfo file end"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    return-object v16

    .line 261
    .end local v3    # "fileArray":Lorg/json/JSONArray;
    .end local v5    # "files":Lorg/json/JSONObject;
    .end local v6    # "folderArray":Lorg/json/JSONArray;
    .end local v7    # "folders":Lorg/json/JSONObject;
    .end local v8    # "i":I
    .end local v13    # "myjson":Lorg/json/JSONObject;
    .end local v14    # "response":Ljava/lang/String;
    .end local v15    # "responseArray":Lorg/json/JSONArray;
    .end local v17    # "scloudFiles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/SCloudFile;>;"
    .end local v18    # "scloudFolders":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/SCloudFolder;>;"
    :catch_0
    move-exception v12

    .line 262
    .local v12, "malformedURLException":Ljava/net/MalformedURLException;
    const-string v19, "SCLOUD_SDK"

    const-string v20, "getFileInfo - MalformedURLException"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    new-instance v19, Lcom/samsung/scloud/exception/SCloudException;

    invoke-direct/range {v19 .. v19}, Lcom/samsung/scloud/exception/SCloudException;-><init>()V

    throw v19

    .line 264
    .end local v12    # "malformedURLException":Ljava/net/MalformedURLException;
    :catch_1
    move-exception v2

    .line 265
    .local v2, "clientProtocolException":Lorg/apache/http/client/ClientProtocolException;
    const-string v19, "SCLOUD_SDK"

    const-string v20, "getFileInfo - ClientProtocolException"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    new-instance v19, Lcom/samsung/scloud/exception/SCloudException;

    invoke-direct/range {v19 .. v19}, Lcom/samsung/scloud/exception/SCloudException;-><init>()V

    throw v19

    .line 267
    .end local v2    # "clientProtocolException":Lorg/apache/http/client/ClientProtocolException;
    :catch_2
    move-exception v10

    .line 268
    .local v10, "ioException":Ljava/io/IOException;
    const-string v19, "SCLOUD_SDK"

    const-string v20, "getFileInfo - IOException"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    new-instance v19, Lcom/samsung/scloud/exception/SCloudIOException;

    invoke-direct/range {v19 .. v19}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>()V

    throw v19

    .line 270
    .end local v10    # "ioException":Ljava/io/IOException;
    :catch_3
    move-exception v11

    .line 271
    .local v11, "jsonException":Lorg/json/JSONException;
    invoke-virtual {v11}, Lorg/json/JSONException;->printStackTrace()V

    .line 272
    const-string v19, "SCLOUD_SDK"

    const-string v20, "getFileInfo - JSONException"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    new-instance v19, Lcom/samsung/scloud/exception/SCloudIOException;

    invoke-direct/range {v19 .. v19}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>()V

    throw v19
.end method

.method public getMetadata(Ljava/lang/String;)Lcom/samsung/scloud/data/Metadata;
    .locals 1
    .param p1, "fileIdOrPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 672
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMusicCoverArt(Ljava/lang/String;Ljava/io/File;)V
    .locals 1
    .param p1, "serverFileIdOrPath"    # Ljava/lang/String;
    .param p2, "localFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 709
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getMyCloudProvider()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 570
    const/4 v0, 0x0

    return-object v0
.end method

.method public getRecycleBin(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 658
    const/4 v0, 0x0

    return-object v0
.end method

.method public getRepository(Ljava/lang/String;)Lcom/samsung/scloud/data/Repository;
    .locals 1
    .param p1, "repositoryName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 129
    const/4 v0, 0x0

    return-object v0
.end method

.method public getRepositoryInfo()Lcom/samsung/scloud/data/RepositoryInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 123
    const/4 v0, 0x0

    return-object v0
.end method

.method public getShareURLInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/ShareInfo;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 595
    const/4 v0, 0x0

    return-object v0
.end method

.method public getStreamingInfoForMusic(Ljava/lang/String;)Lcom/samsung/scloud/data/StreamingInfo;
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 693
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getStreamingInfoForVideo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/StreamingInfo;
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "screenResolution"    # Ljava/lang/String;
    .param p3, "connectionType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 688
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getThumbnail(Ljava/io/File;Ljava/lang/String;IILcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 1
    .param p1, "localFile"    # Ljava/io/File;
    .param p2, "serverFilePath"    # Ljava/lang/String;
    .param p3, "thumbnail_type"    # I
    .param p4, "thumbnail_format"    # I
    .param p5, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 496
    const/4 v0, 0x0

    return-object v0
.end method

.method public getUpdate(JJ)Ljava/util/ArrayList;
    .locals 1
    .param p1, "beginTimeStamp"    # J
    .param p3, "endTimeStamp"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Update;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 564
    const/4 v0, 0x0

    return-object v0
.end method

.method public getUserInfo()Lcom/samsung/scloud/data/User;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 104
    const/4 v0, 0x0

    return-object v0
.end method

.method public getVersions(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Version;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 576
    const/4 v0, 0x0

    return-object v0
.end method

.method public getWholeFileList(ILjava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;
    .locals 1
    .param p1, "maxResults"    # I
    .param p2, "nextToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/scloud/data/SCloudNodeList",
            "<",
            "Lcom/samsung/scloud/data/SCloudFile;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 677
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getWholeFolderList(ILjava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;
    .locals 1
    .param p1, "maxResults"    # I
    .param p2, "nextToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/scloud/data/SCloudNodeList",
            "<",
            "Lcom/samsung/scloud/data/SCloudFolder;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 683
    const/4 v0, 0x0

    return-object v0
.end method

.method public inviteCollaborators(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Collaborator;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 558
    .local p2, "collaborators":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/Collaborator;>;"
    return-void
.end method

.method public isAuthSucceed()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 636
    const/4 v0, 0x0

    return v0
.end method

.method public login(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/User;
    .locals 1
    .param p1, "ID"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 607
    const/4 v0, 0x0

    return-object v0
.end method

.method public logout()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 614
    return-void
.end method

.method public makeCurrentVersion(Ljava/lang/String;J)Ljava/util/ArrayList;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "versionId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Version;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 582
    const/4 v0, 0x0

    return-object v0
.end method

.method public makeFolder(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;
    .locals 1
    .param p1, "folderName"    # Ljava/lang/String;
    .param p2, "parentPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 154
    const/4 v0, 0x0

    return-object v0
.end method

.method public move(Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/scloud/data/SCloudNode;
    .locals 1
    .param p1, "fromPath"    # Ljava/lang/String;
    .param p2, "toPath"    # Ljava/lang/String;
    .param p3, "overwrite"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 293
    const/4 v0, 0x0

    return-object v0
.end method

.method public putFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 1
    .param p1, "sourceFile"    # Ljava/io/File;
    .param p2, "filename"    # Ljava/lang/String;
    .param p3, "destinationFolderPath"    # Ljava/lang/String;
    .param p4, "overwrite"    # Z
    .param p5, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 431
    const/4 v0, 0x0

    return-object v0
.end method

.method public putFile(Ljava/io/File;Ljava/lang/String;ZLjava/lang/String;Lcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 1
    .param p1, "sourceFile"    # Ljava/io/File;
    .param p2, "destinationFolderIdOrPath"    # Ljava/lang/String;
    .param p3, "overwrite"    # Z
    .param p4, "parentRev"    # Ljava/lang/String;
    .param p5, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 481
    const/4 v0, 0x0

    return-object v0
.end method

.method public putFile(Ljava/io/FileInputStream;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 1
    .param p1, "sourceFileInputStream"    # Ljava/io/FileInputStream;
    .param p2, "filename"    # Ljava/lang/String;
    .param p3, "destinationFolderPath"    # Ljava/lang/String;
    .param p4, "overwrite"    # Z
    .param p5, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 439
    const/4 v0, 0x0

    return-object v0
.end method

.method public removeCollaboration(J)V
    .locals 0
    .param p1, "collaborationId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 545
    return-void
.end method

.method public removeComment(Ljava/lang/String;J)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "commentID"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 521
    return-void
.end method

.method public removeShareURL(Ljava/lang/String;)Z
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 601
    const/4 v0, 0x0

    return v0
.end method

.method public rename(Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/scloud/data/SCloudNode;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "newName"    # Ljava/lang/String;
    .param p3, "overwrite"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 341
    const/4 v0, 0x0

    return-object v0
.end method

.method public restoreFromRecycleBin(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "recycledBinPath"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 666
    return-void
.end method

.method public search(Ljava/lang/String;Ljava/lang/String;IZ)Lcom/samsung/scloud/data/SearchResult;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "fileLimit"    # I
    .param p4, "includeDeleted"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 620
    const/4 v0, 0x0

    return-object v0
.end method

.method public setActiveRepository(Ljava/lang/String;)V
    .locals 0
    .param p1, "repositoryName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 653
    return-void
.end method

.method public setAuthToken(Ljava/lang/String;)V
    .locals 0
    .param p1, "token"    # Ljava/lang/String;

    .prologue
    .line 646
    iput-object p1, p0, Lcom/samsung/scloud/SPCAPI;->authToken:Ljava/lang/String;

    .line 647
    return-void
.end method

.method public setDescription(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 527
    return-void
.end method

.method public setDeviceProfile(Lcom/samsung/scloud/data/Device;)V
    .locals 0
    .param p1, "deviceProfile"    # Lcom/samsung/scloud/data/Device;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 87
    return-void
.end method

.method public setDeviceProfile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 93
    return-void
.end method

.method public startAuth(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 626
    return-void
.end method

.method public unsetDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 533
    return-void
.end method

.method public updateRepository(Lcom/samsung/scloud/data/Repository;)Lcom/samsung/scloud/data/Repository;
    .locals 1
    .param p1, "repo"    # Lcom/samsung/scloud/data/Repository;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 142
    const/4 v0, 0x0

    return-object v0
.end method

.method public updateRepository(Ljava/lang/String;J)Lcom/samsung/scloud/data/Repository;
    .locals 1
    .param p1, "repositoryName"    # Ljava/lang/String;
    .param p2, "spaceAmount"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 136
    const/4 v0, 0x0

    return-object v0
.end method
