.class Lcom/samsung/scloud/DropboxAPI$1;
.super Lcom/dropbox/client2/ProgressListener;
.source "DropboxAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/scloud/DropboxAPI;->getFile(Ljava/io/File;Ljava/lang/String;ZLcom/samsung/scloud/response/ProgressListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/scloud/DropboxAPI;

.field final synthetic val$fileName:Ljava/lang/String;

.field final synthetic val$localFile:Ljava/io/File;

.field final synthetic val$progressListener:Lcom/samsung/scloud/response/ProgressListener;


# direct methods
.method constructor <init>(Lcom/samsung/scloud/DropboxAPI;Ljava/lang/String;Lcom/samsung/scloud/response/ProgressListener;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 481
    iput-object p1, p0, Lcom/samsung/scloud/DropboxAPI$1;->this$0:Lcom/samsung/scloud/DropboxAPI;

    iput-object p2, p0, Lcom/samsung/scloud/DropboxAPI$1;->val$fileName:Ljava/lang/String;

    iput-object p3, p0, Lcom/samsung/scloud/DropboxAPI$1;->val$progressListener:Lcom/samsung/scloud/response/ProgressListener;

    iput-object p4, p0, Lcom/samsung/scloud/DropboxAPI$1;->val$localFile:Ljava/io/File;

    invoke-direct {p0}, Lcom/dropbox/client2/ProgressListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgress(JJ)V
    .locals 7
    .param p1, "arg0"    # J
    .param p3, "arg1"    # J

    .prologue
    const-wide/16 v4, 0x64

    .line 489
    iget-object v0, p0, Lcom/samsung/scloud/DropboxAPI$1;->this$0:Lcom/samsung/scloud/DropboxAPI;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getFile("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/scloud/DropboxAPI$1;->val$fileName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "---->"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    mul-long v2, p1, v4

    div-long/2addr v2, p3

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/samsung/scloud/DropboxAPI;->dd(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/samsung/scloud/DropboxAPI;->access$000(Lcom/samsung/scloud/DropboxAPI;Ljava/lang/String;)V

    .line 492
    iget-object v0, p0, Lcom/samsung/scloud/DropboxAPI$1;->val$progressListener:Lcom/samsung/scloud/response/ProgressListener;

    if-eqz v0, :cond_0

    .line 493
    iget-object v0, p0, Lcom/samsung/scloud/DropboxAPI$1;->val$progressListener:Lcom/samsung/scloud/response/ProgressListener;

    iget-object v1, p0, Lcom/samsung/scloud/DropboxAPI$1;->val$localFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    mul-long/2addr v4, p1

    div-long/2addr v4, p3

    invoke-interface {v0, v1, v2, v4, v5}, Lcom/samsung/scloud/response/ProgressListener;->onUpdate(Ljava/lang/String;IJ)V

    .line 498
    :cond_0
    return-void
.end method
