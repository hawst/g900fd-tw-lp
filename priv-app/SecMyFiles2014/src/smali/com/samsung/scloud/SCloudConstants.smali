.class public interface abstract Lcom/samsung/scloud/SCloudConstants;
.super Ljava/lang/Object;
.source "SCloudConstants.java"


# static fields
.field public static final COLLABORATION_STATUS_ACCEPTED:Ljava/lang/String; = "accepted"

.field public static final COLLABORATION_STATUS_PENDING:Ljava/lang/String; = "pending"

.field public static final COPY_EXCEPTION:Ljava/lang/String; = "copy - file copy failed"

.field public static final DELETE_EXCEPTION:Ljava/lang/String; = "delete - deleting file failed"

.field public static final DOWNLOAD_COMPLETED:Ljava/lang/String; = "Download completed"

.field public static final DOWNLOAD_PROGRESS:Ljava/lang/String; = "Download Progess"

.field public static final GETCOLLAB_EXCEPTION:Ljava/lang/String; = "getCollaboration - getting Collaboration failed.."

.field public static final GETFILEINFO_FAILED:Ljava/lang/String; = "getFileInfo failed"

.field public static final GETFILEINFO_SEARCHEXCEPTION:Ljava/lang/String; = "getFileInfo - Failed to search files"

.field public static final GETFILEREVISION_EXCEPTION_1:Ljava/lang/String; = "getFile - File already exists,if you still wish to download please set overwrite to true"

.field public static final GETFILE_EXCEPTION_1:Ljava/lang/String; = "getFile - File already exists,if you still wish to download please set overwrite to true"

.field public static final GETFILE_EXCEPTION_2:Ljava/lang/String; = "getFile - Getting download url failed"

.field public static final GETFILE_EXCEPTION_3:Ljava/lang/String; = "getFile - Input file not found "

.field public static final GETFILE_EXCEPTION_4:Ljava/lang/String; = "getFile - file download failed  "

.field public static final GETFOLDERINFO_EXCEPTION:Ljava/lang/String; = "getFolderInfo failed"

.field public static final GETHUMB_EXCEPTION_1:Ljava/lang/String; = "getThumbnail - creating thumbnail download request failed"

.field public static final GETHUMB_EXCEPTION_2:Ljava/lang/String; = "getThumbnail - thumbnail download failed"

.field public static final GETHUMB_EXCEPTION_3:Ljava/lang/String; = "getThumbnail - this file has no thumbnail or no file"

.field public static final GETHUMB_EXCEPTION_4:Ljava/lang/String; = "getThumbnail - Input file not found "

.field public static final GETHUMB_EXCEPTION_5:Ljava/lang/String; = "getThumbnail - closing fileOutputStream failed"

.field public static final GETLIST_EXCEPTION_1:Ljava/lang/String; = "getList - getting children list failed"

.field public static final GETLIST_EXCEPTION_2:Ljava/lang/String; = "getList - getting fileInfo failed for child:"

.field public static final GETLIST_EXCEPTION_3:Ljava/lang/String; = "getList - getting parent folderinfo failed"

.field public static final GETTRASHFOLDERLIST_EXCEPTION:Ljava/lang/String; = "getTrashedFolderList - getting trash list failed"

.field public static final GETTRASHLIST_EXCEPTION:Ljava/lang/String; = "getTrashedFileList - getting trash list failed"

.field public static final GETUPDATE_EXCEPTION:Ljava/lang/String; = "getUpdate - getChangesRequest failed"

.field public static final GETUSERINFO_EXCEPTION:Ljava/lang/String; = "getUserInfo - getUserInfo failed"

.field public static final GETVERSIONS_EXCEPTION:Ljava/lang/String; = "getVersions - getting Revisions failed "

.field public static final INVITECOLLAB_EXCEPTION:Ljava/lang/String; = "inviteCollaborator - failed for"

.field public static final MAKEFOLDER_EXCEPTION_1:Ljava/lang/String; = "makeFolder - inserting into root folder failed"

.field public static final MAKEFOLDER_EXCEPTION_2:Ljava/lang/String; = "makeFolder - deleting root parent failed"

.field public static final MAKEFOLDER_EXCEPTION_3:Ljava/lang/String; = "makeFolder - adding target folder as parent failed"

.field public static final MIMETYPE_DOC:Ljava/lang/String; = "application/vnd.google-apps.document"

.field public static final MIMETYPE_DRAWING:Ljava/lang/String; = "application/vnd.google-apps.drawing"

.field public static final MIMETYPE_PDF:Ljava/lang/String; = "application/pdf"

.field public static final MIMETYPE_PPT:Ljava/lang/String; = "application/vnd.google-apps.presentation"

.field public static final MIMETYPE_SPREADSHEET:Ljava/lang/String; = "application/vnd.google-apps.spreadsheet"

.field public static final MOVE_EXCEPTION_1:Ljava/lang/String; = "move - getting file metadata failed"

.field public static final MOVE_EXCEPTION_2:Ljava/lang/String; = "move - copying file to target folder failed"

.field public static final NONE:Ljava/lang/String; = ""

.field public static final PUTFILE_EXCEPTION:Ljava/lang/String; = "Google Drive not support the overwriting the file with the filename, you should overwrite the file with file Id"

.field public static final PUTFILE_EXCEPTION_1:Ljava/lang/String; = "putFile - source file does not exist for "

.field public static final PUTFILE_EXCEPTION_2:Ljava/lang/String; = "putFile - getting file metadata failed"

.field public static final PUTFILE_EXCEPTION_3:Ljava/lang/String; = "putFile - close file failed"

.field public static final PUTFILE_EXCEPTION_4:Ljava/lang/String; = "putFile - closing fileInputStream failed"

.field public static final PUTNEWFILE_EXCEPTION_1:Ljava/lang/String; = "putNewFile - source file does not exist for "

.field public static final PUTNEWFILE_EXCEPTION_2:Ljava/lang/String; = "putNewFile - closing fileInoutStream failed"

.field public static final PUTNEWFILE_EXCEPTION_3:Ljava/lang/String; = "putNewFile - upload file failed"

.field public static final REMOVECOLLAB_EXCEPTION:Ljava/lang/String; = "removeCollaboration - remove Collaboration failed.."

.field public static final RENAME_EXCEPTION_1:Ljava/lang/String; = "rename - getting file metadata failed"

.field public static final RENAME_EXCEPTION_2:Ljava/lang/String; = "rename - copying file to target folder failed"

.field public static final RESTOREFROMTRASH_EXCEPTION:Ljava/lang/String; = "restoreFromRecycleBin - restore failed"

.field public static final ROLE_EDITOR:Ljava/lang/String; = "Editor"

.field public static final ROLE_OWNER:Ljava/lang/String; = "Owner"

.field public static final ROLE_VIEWER:Ljava/lang/String; = "Viewer"

.field public static final SCLOUD_PROVIDER_BOX:Ljava/lang/String; = "Box"

.field public static final SCLOUD_PROVIDER_GOOGLE_DRIVE:Ljava/lang/String; = "GOOGLE_DRIVE"

.field public static final SCLOUD_PROVIDER_SPC:Ljava/lang/String; = "SPC"

.field public static final SEARCH_EXCEPTION:Ljava/lang/String; = "search - Failed to search files"

.field public static final SETDESC_EXCEPTION_1:Ljava/lang/String; = "setDescription - getting file metadata failed"

.field public static final SETDESC_EXCEPTION_2:Ljava/lang/String; = "setDescription - setting file description failed"

.field public static final SHARE_HAS_EXPIRE:I = 0x1

.field public static final SHARE_HAS_NO_SHARE:I = 0x4

.field public static final SHARE_HAS_PASSWORD:I = 0x2

.field public static final STATUS_SERVICE_UNAVAILABLE:I = 0x1

.field public static final STATUS_UNKNOWN_HTTP_RESPONSE_CODE:I = 0x0

.field public static final TAG:Ljava/lang/String; = "SCLOUD_SDK"

.field public static final THUMBNAIL_1024x768:I = 0x0

.field public static final THUMBNAIL_320x240:I = 0x4

.field public static final THUMBNAIL_480x320:I = 0x3

.field public static final THUMBNAIL_640x480:I = 0x2

.field public static final THUMBNAIL_960x640:I = 0x1

.field public static final THUMBNAIL_JPEG:I = 0x0

.field public static final THUMBNAIL_PNG:I = 0x1

.field public static final TYPEFOLDER:Ljava/lang/String; = "application/vnd.google-apps.folder"

.field public static final TYPE_FILE:Ljava/lang/String; = "file"

.field public static final TYPE_FOLDER:Ljava/lang/String; = "folder"

.field public static final UNSETDESC_EXCEPTION_1:Ljava/lang/String; = "unsetDescription - getting file metadata failed"

.field public static final UNSETDESC_EXCEPTION_2:Ljava/lang/String; = "unsetDescription - unsetting file description failed"
