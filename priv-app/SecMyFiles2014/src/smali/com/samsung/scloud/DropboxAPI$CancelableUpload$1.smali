.class Lcom/samsung/scloud/DropboxAPI$CancelableUpload$1;
.super Lcom/dropbox/client2/ProgressListener;
.source "DropboxAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->putFileCancelable()Lcom/samsung/scloud/data/SCloudFile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/scloud/DropboxAPI$CancelableUpload;


# direct methods
.method constructor <init>(Lcom/samsung/scloud/DropboxAPI$CancelableUpload;)V
    .locals 0

    .prologue
    .line 2647
    iput-object p1, p0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload$1;->this$1:Lcom/samsung/scloud/DropboxAPI$CancelableUpload;

    invoke-direct {p0}, Lcom/dropbox/client2/ProgressListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgress(JJ)V
    .locals 7
    .param p1, "bytes"    # J
    .param p3, "total"    # J

    .prologue
    const-wide/16 v4, 0x64

    .line 2652
    iget-object v0, p0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload$1;->this$1:Lcom/samsung/scloud/DropboxAPI$CancelableUpload;

    iget-object v0, v0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->this$0:Lcom/samsung/scloud/DropboxAPI;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "putFile("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload$1;->this$1:Lcom/samsung/scloud/DropboxAPI$CancelableUpload;

    # getter for: Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->sourceFile:Ljava/io/File;
    invoke-static {v2}, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->access$700(Lcom/samsung/scloud/DropboxAPI$CancelableUpload;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "---->"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    mul-long v2, p1, v4

    div-long/2addr v2, p3

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/samsung/scloud/DropboxAPI;->dd(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/samsung/scloud/DropboxAPI;->access$000(Lcom/samsung/scloud/DropboxAPI;Ljava/lang/String;)V

    .line 2654
    iget-object v0, p0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload$1;->this$1:Lcom/samsung/scloud/DropboxAPI$CancelableUpload;

    # getter for: Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->progressListener:Lcom/samsung/scloud/response/ProgressListener;
    invoke-static {v0}, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->access$800(Lcom/samsung/scloud/DropboxAPI$CancelableUpload;)Lcom/samsung/scloud/response/ProgressListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2656
    cmp-long v0, p1, p3

    if-gez v0, :cond_0

    .line 2658
    iget-object v0, p0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload$1;->this$1:Lcom/samsung/scloud/DropboxAPI$CancelableUpload;

    # getter for: Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->progressListener:Lcom/samsung/scloud/response/ProgressListener;
    invoke-static {v0}, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->access$800(Lcom/samsung/scloud/DropboxAPI$CancelableUpload;)Lcom/samsung/scloud/response/ProgressListener;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/scloud/DropboxAPI$CancelableUpload$1;->this$1:Lcom/samsung/scloud/DropboxAPI$CancelableUpload;

    # getter for: Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->sourceFile:Ljava/io/File;
    invoke-static {v1}, Lcom/samsung/scloud/DropboxAPI$CancelableUpload;->access$700(Lcom/samsung/scloud/DropboxAPI$CancelableUpload;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    mul-long/2addr v4, p1

    div-long/2addr v4, p3

    invoke-interface {v0, v1, v2, v4, v5}, Lcom/samsung/scloud/response/ProgressListener;->onUpdate(Ljava/lang/String;IJ)V

    .line 2663
    :cond_0
    return-void
.end method
