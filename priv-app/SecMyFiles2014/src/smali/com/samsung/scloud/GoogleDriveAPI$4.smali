.class Lcom/samsung/scloud/GoogleDriveAPI$4;
.super Ljava/lang/Object;
.source "GoogleDriveAPI.java"

# interfaces
.implements Lcom/google/api/client/googleapis/media/MediaHttpDownloaderProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/scloud/GoogleDriveAPI;->getFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/scloud/response/ProgressListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/scloud/GoogleDriveAPI;

.field final synthetic val$localFile:Ljava/io/File;

.field final synthetic val$progressListener:Lcom/samsung/scloud/response/ProgressListener;


# direct methods
.method constructor <init>(Lcom/samsung/scloud/GoogleDriveAPI;Lcom/samsung/scloud/response/ProgressListener;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 825
    iput-object p1, p0, Lcom/samsung/scloud/GoogleDriveAPI$4;->this$0:Lcom/samsung/scloud/GoogleDriveAPI;

    iput-object p2, p0, Lcom/samsung/scloud/GoogleDriveAPI$4;->val$progressListener:Lcom/samsung/scloud/response/ProgressListener;

    iput-object p3, p0, Lcom/samsung/scloud/GoogleDriveAPI$4;->val$localFile:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public progressChanged(Lcom/google/api/client/googleapis/media/MediaHttpDownloader;)V
    .locals 8
    .param p1, "arg0"    # Lcom/google/api/client/googleapis/media/MediaHttpDownloader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x64

    .line 829
    invoke-virtual {p1}, Lcom/google/api/client/googleapis/media/MediaHttpDownloader;->getProgress()D

    move-result-wide v2

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    mul-double/2addr v2, v4

    double-to-long v0, v2

    .line 830
    .local v0, "progress":J
    iget-object v2, p0, Lcom/samsung/scloud/GoogleDriveAPI$4;->val$progressListener:Lcom/samsung/scloud/response/ProgressListener;

    if-eqz v2, :cond_0

    .line 831
    cmp-long v2, v0, v6

    if-gez v2, :cond_1

    .line 832
    iget-object v2, p0, Lcom/samsung/scloud/GoogleDriveAPI$4;->val$progressListener:Lcom/samsung/scloud/response/ProgressListener;

    iget-object v3, p0, Lcom/samsung/scloud/GoogleDriveAPI$4;->val$localFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4, v0, v1}, Lcom/samsung/scloud/response/ProgressListener;->onUpdate(Ljava/lang/String;IJ)V

    .line 837
    :cond_0
    :goto_0
    return-void

    .line 834
    :cond_1
    iget-object v2, p0, Lcom/samsung/scloud/GoogleDriveAPI$4;->val$progressListener:Lcom/samsung/scloud/response/ProgressListener;

    iget-object v3, p0, Lcom/samsung/scloud/GoogleDriveAPI$4;->val$localFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v2, v3, v4, v6, v7}, Lcom/samsung/scloud/response/ProgressListener;->onUpdate(Ljava/lang/String;IJ)V

    goto :goto_0
.end method
