.class public final Lcom/samsung/scloud/utils/SCloudUtils;
.super Ljava/lang/Object;
.source "SCloudUtils.java"


# static fields
.field private static final BYTES_IN_GIGABYTE:J = 0x40000000L

.field private static final BYTES_IN_KILOBYTE:J = 0x400L

.field private static final BYTES_IN_MEGABYTE:J = 0x100000L


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    return-void
.end method

.method public static parseFloat(Ljava/lang/String;)F
    .locals 1
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 121
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/samsung/scloud/utils/SCloudUtils;->parseFloat(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public static parseFloat(Ljava/lang/String;F)F
    .locals 1
    .param p0, "string"    # Ljava/lang/String;
    .param p1, "defaultValue"    # F

    .prologue
    .line 106
    :try_start_0
    invoke-static {p0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    .line 109
    .end local p1    # "defaultValue":F
    :goto_0
    return p1

    .line 108
    .restart local p1    # "defaultValue":F
    :catch_0
    move-exception v0

    .line 109
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public static parseInt(Ljava/lang/String;)I
    .locals 1
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/samsung/scloud/utils/SCloudUtils;->parseInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static parseInt(Ljava/lang/String;I)I
    .locals 1
    .param p0, "string"    # Ljava/lang/String;
    .param p1, "defaultValue"    # I

    .prologue
    .line 46
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    .line 49
    .end local p1    # "defaultValue":I
    :goto_0
    return p1

    .line 48
    .restart local p1    # "defaultValue":I
    :catch_0
    move-exception v0

    .line 49
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public static parseLong(Ljava/lang/String;)J
    .locals 2
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 91
    const-wide/16 v0, 0x0

    invoke-static {p0, v0, v1}, Lcom/samsung/scloud/utils/SCloudUtils;->parseLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public static parseLong(Ljava/lang/String;J)J
    .locals 1
    .param p0, "string"    # Ljava/lang/String;
    .param p1, "defaultValue"    # J

    .prologue
    .line 76
    :try_start_0
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p1

    .line 79
    .end local p1    # "defaultValue":J
    :goto_0
    return-wide p1

    .line 78
    .restart local p1    # "defaultValue":J
    :catch_0
    move-exception v0

    .line 79
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public static parseSizeString(Ljava/lang/String;)J
    .locals 6
    .param p0, "string"    # Ljava/lang/String;

    .prologue
    .line 132
    const-wide/16 v0, 0x1

    .line 133
    .local v0, "factor":J
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 134
    .local v2, "string2":Ljava/lang/String;
    const-string v3, "kb"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 135
    const-wide/16 v0, 0x400

    .line 146
    :goto_0
    const-string v3, "kb"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "mb"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "gb"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "bytes"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/scloud/utils/SCloudUtils;->parseFloat(Ljava/lang/String;)F

    move-result v3

    long-to-float v4, v0

    mul-float/2addr v3, v4

    float-to-long v4, v3

    :goto_1
    return-wide v4

    .line 137
    :cond_0
    const-string v3, "mb"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 138
    const-wide/32 v0, 0x100000

    goto :goto_0

    .line 140
    :cond_1
    const-string v3, "gb"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 141
    const-wide/32 v0, 0x40000000

    goto :goto_0

    .line 144
    :cond_2
    invoke-static {p0}, Lcom/samsung/scloud/utils/SCloudUtils;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    goto :goto_1
.end method
