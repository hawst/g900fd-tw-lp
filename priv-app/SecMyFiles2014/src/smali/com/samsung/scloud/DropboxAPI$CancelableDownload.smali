.class public Lcom/samsung/scloud/DropboxAPI$CancelableDownload;
.super Ljava/lang/Object;
.source "DropboxAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/scloud/DropboxAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CancelableDownload"
.end annotation


# instance fields
.field private dis:Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;

.field private doRetryDownload:Z

.field private localFile:Ljava/io/File;

.field private overwrite:Z

.field private progressListener:Lcom/samsung/scloud/response/ProgressListener;

.field private serverFileIdOrPath:Ljava/lang/String;

.field final synthetic this$0:Lcom/samsung/scloud/DropboxAPI;


# direct methods
.method public constructor <init>(Lcom/samsung/scloud/DropboxAPI;Ljava/io/File;Ljava/lang/String;ZLcom/samsung/scloud/response/ProgressListener;)V
    .locals 1
    .param p2, "localFile"    # Ljava/io/File;
    .param p3, "serverFileIdOrPath"    # Ljava/lang/String;
    .param p4, "overwrite"    # Z
    .param p5, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;

    .prologue
    .line 2514
    iput-object p1, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->this$0:Lcom/samsung/scloud/DropboxAPI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2507
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->doRetryDownload:Z

    .line 2515
    iput-object p2, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->localFile:Ljava/io/File;

    .line 2516
    iput-object p3, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->serverFileIdOrPath:Ljava/lang/String;

    .line 2517
    iput-boolean p4, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->overwrite:Z

    .line 2518
    iput-object p5, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->progressListener:Lcom/samsung/scloud/response/ProgressListener;

    .line 2519
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/scloud/DropboxAPI$CancelableDownload;)Lcom/samsung/scloud/response/ProgressListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/scloud/DropboxAPI$CancelableDownload;

    .prologue
    .line 2504
    iget-object v0, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->progressListener:Lcom/samsung/scloud/response/ProgressListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/scloud/DropboxAPI$CancelableDownload;)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/scloud/DropboxAPI$CancelableDownload;

    .prologue
    .line 2504
    iget-object v0, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->localFile:Ljava/io/File;

    return-object v0
.end method

.method private getFilecancelableDownload(Ljava/lang/String;Ljava/lang/String;Ljava/io/OutputStream;Lcom/dropbox/client2/ProgressListener;)Lcom/dropbox/client2/DropboxAPI$DropboxFileInfo;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "rev"    # Ljava/lang/String;
    .param p3, "os"    # Ljava/io/OutputStream;
    .param p4, "listener"    # Lcom/dropbox/client2/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/exception/DropboxException;
        }
    .end annotation

    .prologue
    .line 2591
    iget-object v0, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->this$0:Lcom/samsung/scloud/DropboxAPI;

    # getter for: Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;
    invoke-static {v0}, Lcom/samsung/scloud/DropboxAPI;->access$600(Lcom/samsung/scloud/DropboxAPI;)Lcom/dropbox/client2/SamsungDropboxAPI;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/dropbox/client2/SamsungDropboxAPI;->getFileStream(Ljava/lang/String;Ljava/lang/String;)Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->dis:Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;

    .line 2592
    iget-boolean v0, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->doRetryDownload:Z

    if-nez v0, :cond_0

    .line 2593
    invoke-virtual {p0}, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->cancelDownload()Z

    .line 2595
    :cond_0
    iget-object v0, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->dis:Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;

    invoke-virtual {v0, p3, p4}, Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;->copyStreamToOutput(Ljava/io/OutputStream;Lcom/dropbox/client2/ProgressListener;)V

    .line 2597
    iget-object v0, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->dis:Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;

    invoke-virtual {v0}, Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;->getFileInfo()Lcom/dropbox/client2/DropboxAPI$DropboxFileInfo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public cancelDownload()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2601
    iput-boolean v1, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->doRetryDownload:Z

    .line 2602
    iget-object v2, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->dis:Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;

    if-eqz v2, :cond_0

    .line 2604
    :try_start_0
    iget-object v2, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->dis:Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;

    invoke-virtual {v2}, Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2605
    const/4 v1, 0x1

    .line 2610
    :cond_0
    :goto_0
    return v1

    .line 2606
    :catch_0
    move-exception v0

    .line 2608
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public getFileCancelable()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;,
            Lcom/samsung/scloud/exception/SCloudCancelException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 2523
    iget-object v6, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->localFile:Ljava/io/File;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->serverFileIdOrPath:Ljava/lang/String;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->serverFileIdOrPath:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v7, 0x1

    if-ge v6, v7, :cond_1

    .line 2525
    :cond_0
    new-instance v6, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v6}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v6

    .line 2527
    :cond_1
    iget-object v6, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->localFile:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_2

    iget-boolean v6, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->overwrite:Z

    if-nez v6, :cond_2

    .line 2528
    new-instance v6, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v6}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v6

    .line 2530
    :cond_2
    const/4 v3, 0x0

    .line 2531
    .local v3, "fileOut":Ljava/io/FileOutputStream;
    iget-object v2, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->serverFileIdOrPath:Ljava/lang/String;

    .line 2532
    .local v2, "fileName":Ljava/lang/String;
    const/4 v5, 0x0

    .local v5, "i":I
    move-object v4, v3

    .end local v3    # "fileOut":Ljava/io/FileOutputStream;
    .local v4, "fileOut":Ljava/io/FileOutputStream;
    :goto_0
    const/4 v6, 0x5

    if-ge v5, v6, :cond_8

    .line 2533
    iget-boolean v6, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->doRetryDownload:Z

    if-nez v6, :cond_3

    .line 2534
    new-instance v6, Lcom/samsung/scloud/exception/SCloudCancelException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "file download cancelled"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->serverFileIdOrPath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/samsung/scloud/exception/SCloudCancelException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 2537
    :cond_3
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    iget-object v6, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->localFile:Ljava/io/File;

    invoke-direct {v3, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2538
    .end local v4    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v3    # "fileOut":Ljava/io/FileOutputStream;
    :try_start_1
    iget-object v6, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->serverFileIdOrPath:Ljava/lang/String;

    const/4 v7, 0x0

    new-instance v8, Lcom/samsung/scloud/DropboxAPI$CancelableDownload$1;

    invoke-direct {v8, p0, v2}, Lcom/samsung/scloud/DropboxAPI$CancelableDownload$1;-><init>(Lcom/samsung/scloud/DropboxAPI$CancelableDownload;Ljava/lang/String;)V

    invoke-direct {p0, v6, v7, v3, v8}, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->getFilecancelableDownload(Ljava/lang/String;Ljava/lang/String;Ljava/io/OutputStream;Lcom/dropbox/client2/ProgressListener;)Lcom/dropbox/client2/DropboxAPI$DropboxFileInfo;

    .line 2562
    iget-object v6, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->progressListener:Lcom/samsung/scloud/response/ProgressListener;

    if-eqz v6, :cond_4

    .line 2563
    iget-object v6, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->progressListener:Lcom/samsung/scloud/response/ProgressListener;

    iget-object v7, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->localFile:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x2

    const-wide/16 v10, 0x64

    invoke-interface {v6, v7, v8, v10, v11}, Lcom/samsung/scloud/response/ProgressListener;->onUpdate(Ljava/lang/String;IJ)V

    .line 2566
    :cond_4
    if-eqz v3, :cond_5

    .line 2567
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 2585
    :cond_5
    :goto_1
    return-void

    .line 2569
    .end local v3    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v4    # "fileOut":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v0

    move-object v3, v4

    .line 2570
    .end local v4    # "fileOut":Ljava/io/FileOutputStream;
    .local v0, "e":Ljava/lang/Exception;
    .restart local v3    # "fileOut":Ljava/io/FileOutputStream;
    :goto_2
    iput-object v9, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->dis:Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;

    .line 2572
    if-eqz v3, :cond_6

    .line 2573
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 2578
    :cond_6
    iget-object v6, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->this$0:Lcom/samsung/scloud/DropboxAPI;

    # invokes: Lcom/samsung/scloud/DropboxAPI;->isRetryNeeded(Ljava/lang/Exception;)Z
    invoke-static {v6, v0}, Lcom/samsung/scloud/DropboxAPI;->access$300(Lcom/samsung/scloud/DropboxAPI;Ljava/lang/Exception;)Z

    move-result v6

    if-eqz v6, :cond_7

    const/4 v6, 0x4

    if-ge v5, v6, :cond_7

    .line 2579
    iget-object v6, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->this$0:Lcom/samsung/scloud/DropboxAPI;

    # invokes: Lcom/samsung/scloud/DropboxAPI;->doBackOff(I)V
    invoke-static {v6, v5}, Lcom/samsung/scloud/DropboxAPI;->access$400(Lcom/samsung/scloud/DropboxAPI;I)V

    .line 2532
    :goto_3
    add-int/lit8 v5, v5, 0x1

    move-object v4, v3

    .end local v3    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v4    # "fileOut":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 2574
    .end local v4    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v3    # "fileOut":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v1

    .line 2575
    .local v1, "e1":Ljava/io/IOException;
    new-instance v6, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v7, "file close failed"

    invoke-direct {v6, v7}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 2582
    .end local v1    # "e1":Ljava/io/IOException;
    :cond_7
    iget-object v6, p0, Lcom/samsung/scloud/DropboxAPI$CancelableDownload;->this$0:Lcom/samsung/scloud/DropboxAPI;

    # invokes: Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V
    invoke-static {v6, v0}, Lcom/samsung/scloud/DropboxAPI;->access$500(Lcom/samsung/scloud/DropboxAPI;Ljava/lang/Exception;)V

    goto :goto_3

    .end local v0    # "e":Ljava/lang/Exception;
    .end local v3    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v4    # "fileOut":Ljava/io/FileOutputStream;
    :cond_8
    move-object v3, v4

    .line 2585
    .end local v4    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v3    # "fileOut":Ljava/io/FileOutputStream;
    goto :goto_1

    .line 2569
    :catch_2
    move-exception v0

    goto :goto_2
.end method
