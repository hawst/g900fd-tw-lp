.class public Lcom/samsung/scloud/data/GDataChanges;
.super Ljava/lang/Object;
.source "GDataChanges.java"


# instance fields
.field protected changes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/scloud/data/SCloudNode;",
            ">;"
        }
    .end annotation
.end field

.field protected etag:Ljava/lang/String;

.field protected largestChangeid:Ljava/lang/String;

.field protected nextLink:Ljava/lang/String;

.field protected nextPageToken:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getChanges()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/scloud/data/SCloudNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 114
    iget-object v0, p0, Lcom/samsung/scloud/data/GDataChanges;->changes:Ljava/util/List;

    return-object v0
.end method

.method public getEtag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/scloud/data/GDataChanges;->etag:Ljava/lang/String;

    return-object v0
.end method

.method public getLargestChangeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/samsung/scloud/data/GDataChanges;->largestChangeid:Ljava/lang/String;

    return-object v0
.end method

.method public getNextLink()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/scloud/data/GDataChanges;->nextLink:Ljava/lang/String;

    return-object v0
.end method

.method public getNextPageToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/scloud/data/GDataChanges;->nextPageToken:Ljava/lang/String;

    return-object v0
.end method

.method public setChanges(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/scloud/data/SCloudNode;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 118
    .local p1, "changes":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/scloud/data/SCloudNode;>;"
    iput-object p1, p0, Lcom/samsung/scloud/data/GDataChanges;->changes:Ljava/util/List;

    .line 119
    return-void
.end method

.method public setEtag(Ljava/lang/String;)V
    .locals 0
    .param p1, "etag"    # Ljava/lang/String;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/samsung/scloud/data/GDataChanges;->etag:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public setLargestChangeId(Ljava/lang/String;)V
    .locals 0
    .param p1, "largestChangeid"    # Ljava/lang/String;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/samsung/scloud/data/GDataChanges;->largestChangeid:Ljava/lang/String;

    .line 103
    return-void
.end method

.method public setNextLink(Ljava/lang/String;)V
    .locals 0
    .param p1, "nextLink"    # Ljava/lang/String;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/samsung/scloud/data/GDataChanges;->nextLink:Ljava/lang/String;

    .line 89
    return-void
.end method

.method public setNextPageToken(Ljava/lang/String;)V
    .locals 0
    .param p1, "nextPageToken"    # Ljava/lang/String;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/samsung/scloud/data/GDataChanges;->nextPageToken:Ljava/lang/String;

    .line 73
    return-void
.end method
