.class public final Lcom/samsung/scloud/data/SearchResult;
.super Ljava/lang/Object;
.source "SearchResult.java"


# instance fields
.field private containerDescription:Ljava/lang/String;

.field private containerId:J

.field private containerName:Ljava/lang/String;

.field private containerPath:Ljava/lang/String;

.field protected etag:Ljava/lang/String;

.field private matchDescription:Ljava/lang/String;

.field private matchName:Ljava/lang/String;

.field private matchSearchText:Ljava/lang/String;

.field protected nextLink:Ljava/lang/String;

.field protected nextPageToken:Ljava/lang/String;

.field protected searchResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/scloud/data/SCloudNode;",
            ">;"
        }
    .end annotation
.end field

.field private type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getContainerDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/scloud/data/SearchResult;->containerDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getContainerId()J
    .locals 2

    .prologue
    .line 128
    iget-wide v0, p0, Lcom/samsung/scloud/data/SearchResult;->containerId:J

    return-wide v0
.end method

.method public getContainerName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/scloud/data/SearchResult;->containerName:Ljava/lang/String;

    return-object v0
.end method

.method public getContainerPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/scloud/data/SearchResult;->containerPath:Ljava/lang/String;

    return-object v0
.end method

.method public getEtag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/scloud/data/SearchResult;->etag:Ljava/lang/String;

    return-object v0
.end method

.method public getMatchDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/samsung/scloud/data/SearchResult;->matchDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getMatchName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/samsung/scloud/data/SearchResult;->matchName:Ljava/lang/String;

    return-object v0
.end method

.method public getMatchSearchText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/samsung/scloud/data/SearchResult;->matchSearchText:Ljava/lang/String;

    return-object v0
.end method

.method public getNextLink()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/scloud/data/SearchResult;->nextLink:Ljava/lang/String;

    return-object v0
.end method

.method public getNextPageToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/scloud/data/SearchResult;->nextPageToken:Ljava/lang/String;

    return-object v0
.end method

.method public getSearchResults()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/scloud/data/SCloudNode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/scloud/data/SearchResult;->searchResults:Ljava/util/List;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/samsung/scloud/data/SearchResult;->type:Ljava/lang/String;

    return-object v0
.end method

.method public setContainerDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "containerDescription"    # Ljava/lang/String;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/samsung/scloud/data/SearchResult;->containerDescription:Ljava/lang/String;

    .line 141
    return-void
.end method

.method public setContainerId(J)V
    .locals 1
    .param p1, "containerId"    # J

    .prologue
    .line 132
    iput-wide p1, p0, Lcom/samsung/scloud/data/SearchResult;->containerId:J

    .line 133
    return-void
.end method

.method public setContainerName(Ljava/lang/String;)V
    .locals 0
    .param p1, "containerName"    # Ljava/lang/String;

    .prologue
    .line 124
    iput-object p1, p0, Lcom/samsung/scloud/data/SearchResult;->containerName:Ljava/lang/String;

    .line 125
    return-void
.end method

.method public setContainerPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "containerPath"    # Ljava/lang/String;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/samsung/scloud/data/SearchResult;->containerPath:Ljava/lang/String;

    .line 149
    return-void
.end method

.method public setEtag(Ljava/lang/String;)V
    .locals 0
    .param p1, "etag"    # Ljava/lang/String;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/samsung/scloud/data/SearchResult;->etag:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public setMatchDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "matchDescription"    # Ljava/lang/String;

    .prologue
    .line 172
    iput-object p1, p0, Lcom/samsung/scloud/data/SearchResult;->matchDescription:Ljava/lang/String;

    .line 173
    return-void
.end method

.method public setMatchName(Ljava/lang/String;)V
    .locals 0
    .param p1, "matchName"    # Ljava/lang/String;

    .prologue
    .line 164
    iput-object p1, p0, Lcom/samsung/scloud/data/SearchResult;->matchName:Ljava/lang/String;

    .line 165
    return-void
.end method

.method public setMatchSearchText(Ljava/lang/String;)V
    .locals 0
    .param p1, "matchSearchText"    # Ljava/lang/String;

    .prologue
    .line 180
    iput-object p1, p0, Lcom/samsung/scloud/data/SearchResult;->matchSearchText:Ljava/lang/String;

    .line 181
    return-void
.end method

.method public setNextLink(Ljava/lang/String;)V
    .locals 0
    .param p1, "nextLink"    # Ljava/lang/String;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/samsung/scloud/data/SearchResult;->nextLink:Ljava/lang/String;

    .line 69
    return-void
.end method

.method public setNextPageToken(Ljava/lang/String;)V
    .locals 0
    .param p1, "nextPageToken"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/samsung/scloud/data/SearchResult;->nextPageToken:Ljava/lang/String;

    .line 61
    return-void
.end method

.method public setSearchResults(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/scloud/data/SCloudNode;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 76
    .local p1, "searchResults":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/scloud/data/SCloudNode;>;"
    iput-object p1, p0, Lcom/samsung/scloud/data/SearchResult;->searchResults:Ljava/util/List;

    .line 77
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 156
    iput-object p1, p0, Lcom/samsung/scloud/data/SearchResult;->type:Ljava/lang/String;

    .line 157
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 185
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 186
    .local v0, "stringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "Container ID ---> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 187
    invoke-virtual {p0}, Lcom/samsung/scloud/data/SearchResult;->getContainerId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 188
    const-string v1, "  Container Name ---> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    invoke-virtual {p0}, Lcom/samsung/scloud/data/SearchResult;->getContainerName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    const-string v1, "  containerDescription ---> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    invoke-virtual {p0}, Lcom/samsung/scloud/data/SearchResult;->getContainerDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    const-string v1, "  containerPath ---> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    invoke-virtual {p0}, Lcom/samsung/scloud/data/SearchResult;->getContainerPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    const-string v1, "  type ---> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    invoke-virtual {p0}, Lcom/samsung/scloud/data/SearchResult;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    const-string v1, "  matchName ---> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    invoke-virtual {p0}, Lcom/samsung/scloud/data/SearchResult;->getMatchName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    const-string v1, "  matchDescription ---> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    invoke-virtual {p0}, Lcom/samsung/scloud/data/SearchResult;->getMatchDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    const-string v1, "  matchSearchText ---> "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    invoke-virtual {p0}, Lcom/samsung/scloud/data/SearchResult;->getMatchSearchText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
