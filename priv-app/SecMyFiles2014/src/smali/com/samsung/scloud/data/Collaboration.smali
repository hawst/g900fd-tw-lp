.class public final Lcom/samsung/scloud/data/Collaboration;
.super Ljava/lang/Object;
.source "Collaboration.java"


# instance fields
.field private collaborators:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Collaborator;",
            ">;"
        }
    .end annotation
.end field

.field private etag:Ljava/lang/String;

.field private path:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/scloud/data/Collaboration;->collaborators:Ljava/util/ArrayList;

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/scloud/data/Collaboration;->path:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 0
    .param p2, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Collaborator;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 31
    .local p1, "collaborators":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/Collaborator;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/samsung/scloud/data/Collaboration;->collaborators:Ljava/util/ArrayList;

    .line 33
    iput-object p2, p0, Lcom/samsung/scloud/data/Collaboration;->path:Ljava/lang/String;

    .line 34
    return-void
.end method


# virtual methods
.method public getCollaborators()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Collaborator;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/scloud/data/Collaboration;->collaborators:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getEtag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/scloud/data/Collaboration;->etag:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/samsung/scloud/data/Collaboration;->path:Ljava/lang/String;

    return-object v0
.end method

.method public setCollaborators(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Collaborator;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p1, "collaborators":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/Collaborator;>;"
    iput-object p1, p0, Lcom/samsung/scloud/data/Collaboration;->collaborators:Ljava/util/ArrayList;

    .line 56
    return-void
.end method

.method public setEtag(Ljava/lang/String;)V
    .locals 0
    .param p1, "etag"    # Ljava/lang/String;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/samsung/scloud/data/Collaboration;->etag:Ljava/lang/String;

    .line 42
    return-void
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/samsung/scloud/data/Collaboration;->path:Ljava/lang/String;

    .line 68
    return-void
.end method
