.class public final Lcom/samsung/scloud/data/SCloudFolder;
.super Lcom/samsung/scloud/data/SCloudNode;
.source "SCloudFolder.java"


# instance fields
.field private cover:Lcom/samsung/scloud/data/SCloudFile;

.field private fileCount:J

.field private files:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/SCloudFile;",
            ">;"
        }
    .end annotation
.end field

.field private folders:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/SCloudFolder;",
            ">;"
        }
    .end annotation
.end field

.field private hasCollaborators:Z

.field private revision:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 13
    invoke-direct {p0}, Lcom/samsung/scloud/data/SCloudNode;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/scloud/data/SCloudFolder;->hasCollaborators:Z

    .line 28
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/scloud/data/SCloudFolder;->fileCount:J

    .line 29
    iput-object v2, p0, Lcom/samsung/scloud/data/SCloudFolder;->files:Ljava/util/ArrayList;

    .line 30
    iput-object v2, p0, Lcom/samsung/scloud/data/SCloudFolder;->folders:Ljava/util/ArrayList;

    .line 37
    iput-object v2, p0, Lcom/samsung/scloud/data/SCloudFolder;->cover:Lcom/samsung/scloud/data/SCloudFile;

    return-void
.end method


# virtual methods
.method public getCover()Lcom/samsung/scloud/data/SCloudFile;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudFolder;->cover:Lcom/samsung/scloud/data/SCloudFile;

    return-object v0
.end method

.method public getFileCount()J
    .locals 2

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/samsung/scloud/data/SCloudFolder;->fileCount:J

    return-wide v0
.end method

.method public getFiles()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/SCloudFile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudFolder;->files:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getFolders()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/SCloudFolder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudFolder;->folders:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getNodeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    sget-object v0, Lcom/samsung/scloud/data/SCloudNode;->TYPE_FOLDER:Ljava/lang/String;

    return-object v0
.end method

.method public getRevision()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudFolder;->revision:Ljava/lang/String;

    return-object v0
.end method

.method public hasCollaborators()Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/samsung/scloud/data/SCloudFolder;->hasCollaborators:Z

    return v0
.end method

.method public setCover(Lcom/samsung/scloud/data/SCloudFile;)V
    .locals 0
    .param p1, "coverFile"    # Lcom/samsung/scloud/data/SCloudFile;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudFolder;->cover:Lcom/samsung/scloud/data/SCloudFile;

    .line 116
    return-void
.end method

.method public setFileCount(J)V
    .locals 1
    .param p1, "fileCount"    # J

    .prologue
    .line 77
    iput-wide p1, p0, Lcom/samsung/scloud/data/SCloudFolder;->fileCount:J

    .line 78
    return-void
.end method

.method public setFiles(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/SCloudFile;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 90
    .local p1, "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/SCloudFile;>;"
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudFolder;->files:Ljava/util/ArrayList;

    .line 91
    return-void
.end method

.method public setFolders(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/SCloudFolder;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 103
    .local p1, "folders":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/SCloudFolder;>;"
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudFolder;->folders:Ljava/util/ArrayList;

    .line 104
    return-void
.end method

.method public setHasCollaborators(Z)V
    .locals 0
    .param p1, "hasCollaborators"    # Z

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/samsung/scloud/data/SCloudFolder;->hasCollaborators:Z

    .line 63
    return-void
.end method

.method public setRevision(Ljava/lang/String;)V
    .locals 0
    .param p1, "rev"    # Ljava/lang/String;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudFolder;->revision:Ljava/lang/String;

    .line 124
    return-void
.end method
