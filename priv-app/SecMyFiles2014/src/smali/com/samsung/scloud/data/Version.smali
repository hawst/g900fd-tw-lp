.class public final Lcom/samsung/scloud/data/Version;
.super Ljava/lang/Object;
.source "Version.java"


# instance fields
.field private email:Ljava/lang/String;

.field private revisionId:Ljava/lang/String;

.field private uId:J

.field private updateTimestamp:J

.field private userName:Ljava/lang/String;

.field private versionId:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getRevisionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/scloud/data/Version;->revisionId:Ljava/lang/String;

    return-object v0
.end method

.method public getUpdateTimestamp()J
    .locals 2

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/samsung/scloud/data/Version;->updateTimestamp:J

    return-wide v0
.end method

.method public getUserEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/scloud/data/Version;->email:Ljava/lang/String;

    return-object v0
.end method

.method public getUserId()J
    .locals 2

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/samsung/scloud/data/Version;->uId:J

    return-wide v0
.end method

.method public getUserName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/scloud/data/Version;->userName:Ljava/lang/String;

    return-object v0
.end method

.method public getVersionId()J
    .locals 2

    .prologue
    .line 53
    iget-wide v0, p0, Lcom/samsung/scloud/data/Version;->versionId:J

    return-wide v0
.end method

.method public setRevisoinId(Ljava/lang/String;)V
    .locals 0
    .param p1, "versionId"    # Ljava/lang/String;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/samsung/scloud/data/Version;->revisionId:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public setUpdateTimestamp(J)V
    .locals 1
    .param p1, "updateTimestamp"    # J

    .prologue
    .line 50
    iput-wide p1, p0, Lcom/samsung/scloud/data/Version;->updateTimestamp:J

    .line 51
    return-void
.end method

.method public setUserEmail(Ljava/lang/String;)V
    .locals 0
    .param p1, "email"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/samsung/scloud/data/Version;->email:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public setUserId(J)V
    .locals 1
    .param p1, "uId"    # J

    .prologue
    .line 62
    iput-wide p1, p0, Lcom/samsung/scloud/data/Version;->uId:J

    .line 63
    return-void
.end method

.method public setUserName(Ljava/lang/String;)V
    .locals 0
    .param p1, "userName"    # Ljava/lang/String;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/samsung/scloud/data/Version;->userName:Ljava/lang/String;

    .line 69
    return-void
.end method

.method public setVersionId(J)V
    .locals 1
    .param p1, "versionId"    # J

    .prologue
    .line 56
    iput-wide p1, p0, Lcom/samsung/scloud/data/Version;->versionId:J

    .line 57
    return-void
.end method
