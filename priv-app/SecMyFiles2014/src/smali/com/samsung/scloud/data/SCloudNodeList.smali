.class public Lcom/samsung/scloud/data/SCloudNodeList;
.super Ljava/lang/Object;
.source "SCloudNodeList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field cursor:Ljava/lang/String;

.field hasMore:Z

.field lastestChangeId:J

.field nextPageToken:Ljava/lang/String;

.field nextURL:Ljava/lang/String;

.field nodes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field

.field reset:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .local p0, "this":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<TT;>;"
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object v2, p0, Lcom/samsung/scloud/data/SCloudNodeList;->nodes:Ljava/util/List;

    .line 15
    iput-object v2, p0, Lcom/samsung/scloud/data/SCloudNodeList;->nextURL:Ljava/lang/String;

    .line 16
    iput-object v2, p0, Lcom/samsung/scloud/data/SCloudNodeList;->nextPageToken:Ljava/lang/String;

    .line 23
    iput-object v2, p0, Lcom/samsung/scloud/data/SCloudNodeList;->cursor:Ljava/lang/String;

    .line 35
    iput-boolean v0, p0, Lcom/samsung/scloud/data/SCloudNodeList;->hasMore:Z

    .line 50
    iput-boolean v0, p0, Lcom/samsung/scloud/data/SCloudNodeList;->reset:Z

    .line 73
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/scloud/data/SCloudNodeList;->lastestChangeId:J

    .line 76
    iput-object v2, p0, Lcom/samsung/scloud/data/SCloudNodeList;->nextURL:Ljava/lang/String;

    .line 77
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/scloud/data/SCloudNodeList;->lastestChangeId:J

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/scloud/data/SCloudNodeList;->nodes:Ljava/util/List;

    .line 79
    return-void
.end method


# virtual methods
.method public getCursor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    .local p0, "this":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<TT;>;"
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudNodeList;->cursor:Ljava/lang/String;

    return-object v0
.end method

.method public getHasMore()Z
    .locals 1

    .prologue
    .line 38
    .local p0, "this":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<TT;>;"
    iget-boolean v0, p0, Lcom/samsung/scloud/data/SCloudNodeList;->hasMore:Z

    return v0
.end method

.method public getLastestChangeId()J
    .locals 2

    .prologue
    .line 113
    .local p0, "this":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<TT;>;"
    iget-wide v0, p0, Lcom/samsung/scloud/data/SCloudNodeList;->lastestChangeId:J

    return-wide v0
.end method

.method public getNextPageToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    .local p0, "this":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<TT;>;"
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudNodeList;->nextPageToken:Ljava/lang/String;

    return-object v0
.end method

.method public getNextURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    .local p0, "this":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<TT;>;"
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudNodeList;->nextURL:Ljava/lang/String;

    return-object v0
.end method

.method public getNodes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 85
    .local p0, "this":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<TT;>;"
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudNodeList;->nodes:Ljava/util/List;

    return-object v0
.end method

.method public getReset()Z
    .locals 1

    .prologue
    .line 53
    .local p0, "this":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<TT;>;"
    iget-boolean v0, p0, Lcom/samsung/scloud/data/SCloudNodeList;->reset:Z

    return v0
.end method

.method public setCursor(Ljava/lang/String;)V
    .locals 0
    .param p1, "cursor"    # Ljava/lang/String;

    .prologue
    .line 28
    .local p0, "this":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<TT;>;"
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudNodeList;->cursor:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public setHasMore(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 42
    .local p0, "this":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<TT;>;"
    iput-boolean p1, p0, Lcom/samsung/scloud/data/SCloudNodeList;->hasMore:Z

    .line 43
    return-void
.end method

.method public setLastestChangeId(J)V
    .locals 1
    .param p1, "lastestChangeId"    # J

    .prologue
    .line 120
    .local p0, "this":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<TT;>;"
    iput-wide p1, p0, Lcom/samsung/scloud/data/SCloudNodeList;->lastestChangeId:J

    .line 121
    return-void
.end method

.method public setNextPageToken(Ljava/lang/String;)V
    .locals 0
    .param p1, "nextPageToken"    # Ljava/lang/String;

    .prologue
    .line 70
    .local p0, "this":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<TT;>;"
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudNodeList;->nextPageToken:Ljava/lang/String;

    .line 71
    return-void
.end method

.method public setNextURL(Ljava/lang/String;)V
    .locals 0
    .param p1, "nextURL"    # Ljava/lang/String;

    .prologue
    .line 106
    .local p0, "this":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<TT;>;"
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudNodeList;->nextURL:Ljava/lang/String;

    .line 107
    return-void
.end method

.method public setNodes(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 92
    .local p0, "this":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<TT;>;"
    .local p1, "nodes":Ljava/util/List;, "Ljava/util/List<TT;>;"
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudNodeList;->nodes:Ljava/util/List;

    .line 93
    return-void
.end method

.method public setReset(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 57
    .local p0, "this":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<TT;>;"
    iput-boolean p1, p0, Lcom/samsung/scloud/data/SCloudNodeList;->reset:Z

    .line 58
    return-void
.end method
