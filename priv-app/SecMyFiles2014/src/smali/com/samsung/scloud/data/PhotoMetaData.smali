.class public Lcom/samsung/scloud/data/PhotoMetaData;
.super Ljava/lang/Object;
.source "PhotoMetaData.java"


# instance fields
.field protected albums:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/scloud/data/AlbumData;",
            ">;"
        }
    .end annotation
.end field

.field private dateTaken:J

.field private height:I

.field private latitude:Ljava/lang/String;

.field private longitude:Ljava/lang/String;

.field private orientation:I

.field private width:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/scloud/data/PhotoMetaData;->dateTaken:J

    .line 13
    iput v3, p0, Lcom/samsung/scloud/data/PhotoMetaData;->width:I

    .line 17
    iput v3, p0, Lcom/samsung/scloud/data/PhotoMetaData;->height:I

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/scloud/data/PhotoMetaData;->orientation:I

    .line 26
    iput-object v2, p0, Lcom/samsung/scloud/data/PhotoMetaData;->latitude:Ljava/lang/String;

    .line 30
    iput-object v2, p0, Lcom/samsung/scloud/data/PhotoMetaData;->longitude:Ljava/lang/String;

    .line 36
    iput-object v2, p0, Lcom/samsung/scloud/data/PhotoMetaData;->albums:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getAlbums()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/scloud/data/AlbumData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lcom/samsung/scloud/data/PhotoMetaData;->albums:Ljava/util/List;

    return-object v0
.end method

.method public getDateTaken()J
    .locals 2

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/samsung/scloud/data/PhotoMetaData;->dateTaken:J

    return-wide v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/samsung/scloud/data/PhotoMetaData;->height:I

    return v0
.end method

.method public getLatitude()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/scloud/data/PhotoMetaData;->latitude:Ljava/lang/String;

    return-object v0
.end method

.method public getLongitude()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/scloud/data/PhotoMetaData;->longitude:Ljava/lang/String;

    return-object v0
.end method

.method public getOrientation()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/samsung/scloud/data/PhotoMetaData;->orientation:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/scloud/data/PhotoMetaData;->width:I

    return v0
.end method

.method public setAlbums(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/scloud/data/AlbumData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 94
    .local p1, "albums":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/scloud/data/AlbumData;>;"
    iput-object p1, p0, Lcom/samsung/scloud/data/PhotoMetaData;->albums:Ljava/util/List;

    .line 95
    return-void
.end method

.method public setDateTaken(J)V
    .locals 1
    .param p1, "dateTaken"    # J

    .prologue
    .line 41
    iput-wide p1, p0, Lcom/samsung/scloud/data/PhotoMetaData;->dateTaken:J

    .line 42
    return-void
.end method

.method public setHeight(I)V
    .locals 0
    .param p1, "height"    # I

    .prologue
    .line 53
    iput p1, p0, Lcom/samsung/scloud/data/PhotoMetaData;->height:I

    .line 54
    return-void
.end method

.method public setLatitude(Ljava/lang/String;)V
    .locals 0
    .param p1, "latitude"    # Ljava/lang/String;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/samsung/scloud/data/PhotoMetaData;->latitude:Ljava/lang/String;

    .line 66
    return-void
.end method

.method public setLongitude(Ljava/lang/String;)V
    .locals 0
    .param p1, "longitude"    # Ljava/lang/String;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/samsung/scloud/data/PhotoMetaData;->longitude:Ljava/lang/String;

    .line 72
    return-void
.end method

.method public setOrientation(I)V
    .locals 0
    .param p1, "orientation"    # I

    .prologue
    .line 59
    iput p1, p0, Lcom/samsung/scloud/data/PhotoMetaData;->orientation:I

    .line 60
    return-void
.end method

.method public setWidth(I)V
    .locals 0
    .param p1, "width"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/samsung/scloud/data/PhotoMetaData;->width:I

    .line 48
    return-void
.end method
