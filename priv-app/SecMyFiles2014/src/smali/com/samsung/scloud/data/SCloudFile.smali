.class public final Lcom/samsung/scloud/data/SCloudFile;
.super Lcom/samsung/scloud/data/SCloudNode;
.source "SCloudFile.java"


# instance fields
.field protected audioMetaData:Lcom/samsung/scloud/data/AudioMetaData;

.field protected extendedAttributes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<**>;"
        }
    .end annotation
.end field

.field protected fileSignature:Lcom/samsung/scloud/data/FileSignature;

.field protected iconName:Ljava/lang/String;

.field protected md5Checksum:Ljava/lang/String;

.field protected mediaType:Ljava/lang/String;

.field protected mimeType:Ljava/lang/String;

.field protected parentRev:Ljava/lang/String;

.field protected photoMetaData:Lcom/samsung/scloud/data/PhotoMetaData;

.field protected rootName:Ljava/lang/String;

.field protected videoMetaData:Lcom/samsung/scloud/data/VideoMetaData;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 14
    invoke-direct {p0}, Lcom/samsung/scloud/data/SCloudNode;-><init>()V

    .line 22
    iput-object v0, p0, Lcom/samsung/scloud/data/SCloudFile;->extendedAttributes:Ljava/util/Map;

    .line 30
    iput-object v0, p0, Lcom/samsung/scloud/data/SCloudFile;->md5Checksum:Ljava/lang/String;

    .line 38
    iput-object v0, p0, Lcom/samsung/scloud/data/SCloudFile;->fileSignature:Lcom/samsung/scloud/data/FileSignature;

    .line 46
    iput-object v0, p0, Lcom/samsung/scloud/data/SCloudFile;->mimeType:Ljava/lang/String;

    .line 54
    iput-object v0, p0, Lcom/samsung/scloud/data/SCloudFile;->mediaType:Ljava/lang/String;

    .line 63
    iput-object v0, p0, Lcom/samsung/scloud/data/SCloudFile;->photoMetaData:Lcom/samsung/scloud/data/PhotoMetaData;

    .line 70
    iput-object v0, p0, Lcom/samsung/scloud/data/SCloudFile;->videoMetaData:Lcom/samsung/scloud/data/VideoMetaData;

    .line 77
    iput-object v0, p0, Lcom/samsung/scloud/data/SCloudFile;->audioMetaData:Lcom/samsung/scloud/data/AudioMetaData;

    .line 85
    iput-object v0, p0, Lcom/samsung/scloud/data/SCloudFile;->iconName:Ljava/lang/String;

    .line 91
    iput-object v0, p0, Lcom/samsung/scloud/data/SCloudFile;->rootName:Ljava/lang/String;

    .line 98
    iput-object v0, p0, Lcom/samsung/scloud/data/SCloudFile;->parentRev:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAudioMetaData()Lcom/samsung/scloud/data/AudioMetaData;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudFile;->audioMetaData:Lcom/samsung/scloud/data/AudioMetaData;

    return-object v0
.end method

.method public getExtendedAttributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<**>;"
        }
    .end annotation

    .prologue
    .line 226
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudFile;->extendedAttributes:Ljava/util/Map;

    return-object v0
.end method

.method public getFileSignature()Lcom/samsung/scloud/data/FileSignature;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudFile;->fileSignature:Lcom/samsung/scloud/data/FileSignature;

    return-object v0
.end method

.method public getIconName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudFile;->iconName:Ljava/lang/String;

    return-object v0
.end method

.method public getMd5Checksum()Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudFile;->md5Checksum:Ljava/lang/String;

    return-object v0
.end method

.method public getMediaType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudFile;->mediaType:Ljava/lang/String;

    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudFile;->mimeType:Ljava/lang/String;

    return-object v0
.end method

.method public getParentRev()Ljava/lang/String;
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudFile;->parentRev:Ljava/lang/String;

    return-object v0
.end method

.method public getPhotoMetaData()Lcom/samsung/scloud/data/PhotoMetaData;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudFile;->photoMetaData:Lcom/samsung/scloud/data/PhotoMetaData;

    return-object v0
.end method

.method public getRootName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudFile;->rootName:Ljava/lang/String;

    return-object v0
.end method

.method public getVideoMetaData()Lcom/samsung/scloud/data/VideoMetaData;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudFile;->videoMetaData:Lcom/samsung/scloud/data/VideoMetaData;

    return-object v0
.end method

.method public setAudioMetaData(Lcom/samsung/scloud/data/AudioMetaData;)V
    .locals 0
    .param p1, "audioMetaData"    # Lcom/samsung/scloud/data/AudioMetaData;

    .prologue
    .line 215
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudFile;->audioMetaData:Lcom/samsung/scloud/data/AudioMetaData;

    .line 216
    return-void
.end method

.method public setExtendedAttributes(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<**>;)V"
        }
    .end annotation

    .prologue
    .line 230
    .local p1, "extendedAttributes":Ljava/util/Map;, "Ljava/util/Map<**>;"
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudFile;->extendedAttributes:Ljava/util/Map;

    .line 231
    return-void
.end method

.method public setFileSignature(Lcom/samsung/scloud/data/FileSignature;)V
    .locals 0
    .param p1, "fileSignature"    # Lcom/samsung/scloud/data/FileSignature;

    .prologue
    .line 246
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudFile;->fileSignature:Lcom/samsung/scloud/data/FileSignature;

    .line 247
    return-void
.end method

.method public setIconName(Ljava/lang/String;)V
    .locals 0
    .param p1, "iconName"    # Ljava/lang/String;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudFile;->iconName:Ljava/lang/String;

    .line 115
    return-void
.end method

.method public setMd5Checksum(Ljava/lang/String;)V
    .locals 0
    .param p1, "md5Checksum"    # Ljava/lang/String;

    .prologue
    .line 156
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudFile;->md5Checksum:Ljava/lang/String;

    .line 157
    return-void
.end method

.method public setMediaType(Ljava/lang/String;)V
    .locals 0
    .param p1, "mediaType"    # Ljava/lang/String;

    .prologue
    .line 172
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudFile;->mediaType:Ljava/lang/String;

    .line 173
    return-void
.end method

.method public setMimeType(Ljava/lang/String;)V
    .locals 0
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 262
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudFile;->mimeType:Ljava/lang/String;

    .line 263
    return-void
.end method

.method public setParentRev(Ljava/lang/String;)V
    .locals 0
    .param p1, "parentRev"    # Ljava/lang/String;

    .prologue
    .line 277
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudFile;->parentRev:Ljava/lang/String;

    .line 278
    return-void
.end method

.method public setPhotoMetaData(Lcom/samsung/scloud/data/PhotoMetaData;)V
    .locals 0
    .param p1, "photoMetaData"    # Lcom/samsung/scloud/data/PhotoMetaData;

    .prologue
    .line 187
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudFile;->photoMetaData:Lcom/samsung/scloud/data/PhotoMetaData;

    .line 188
    return-void
.end method

.method public setRootName(Ljava/lang/String;)V
    .locals 0
    .param p1, "rootName"    # Ljava/lang/String;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudFile;->rootName:Ljava/lang/String;

    .line 137
    return-void
.end method

.method public setVideoMetaData(Lcom/samsung/scloud/data/VideoMetaData;)V
    .locals 0
    .param p1, "videoMetaData"    # Lcom/samsung/scloud/data/VideoMetaData;

    .prologue
    .line 201
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudFile;->videoMetaData:Lcom/samsung/scloud/data/VideoMetaData;

    .line 202
    return-void
.end method
