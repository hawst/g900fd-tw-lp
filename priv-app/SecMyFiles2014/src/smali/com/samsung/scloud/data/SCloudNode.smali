.class public abstract Lcom/samsung/scloud/data/SCloudNode;
.super Ljava/lang/Object;
.source "SCloudNode.java"


# static fields
.field public static ROLE_OWNER:Ljava/lang/String;

.field public static ROLE_READER:Ljava/lang/String;

.field public static ROLE_WRITER:Ljava/lang/String;

.field public static TYPE_FILE:Ljava/lang/String;

.field public static TYPE_FOLDER:Ljava/lang/String;

.field public static TYPE_GOOGLEAPP:Ljava/lang/String;

.field public static TYPE_UNKNOWN:Ljava/lang/String;


# instance fields
.field protected absolutePath:Ljava/lang/String;

.field protected accessControlUrl:Ljava/lang/String;

.field protected changeId:Ljava/lang/String;

.field protected createdTimestamp:J

.field protected description:Ljava/lang/String;

.field protected downloadUrl:Ljava/lang/String;

.field protected eTag:Ljava/lang/String;

.field protected id:Ljava/lang/String;

.field protected isDeleted:Z

.field protected isShared:Z

.field protected isStarred:Z

.field protected isTrashed:Z

.field protected lastViewedTimestamp:J

.field protected name:Ljava/lang/String;

.field private nodeType:Ljava/lang/String;

.field protected ownerNames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected parentFolderId:Ljava/lang/String;

.field protected parentFolderIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected quotaBytesUsed:J

.field protected repositoryId:Ljava/lang/String;

.field protected repositoryName:Ljava/lang/String;

.field protected role:Ljava/lang/String;

.field protected shareURL:Ljava/lang/String;

.field protected size:J

.field protected thumbExists:Z

.field protected thumbnailUrl:Ljava/lang/String;

.field protected updatedTimestamp:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-string v0, "folder"

    sput-object v0, Lcom/samsung/scloud/data/SCloudNode;->TYPE_FOLDER:Ljava/lang/String;

    .line 17
    const-string v0, "file"

    sput-object v0, Lcom/samsung/scloud/data/SCloudNode;->TYPE_FILE:Ljava/lang/String;

    .line 18
    const-string v0, "googleapp"

    sput-object v0, Lcom/samsung/scloud/data/SCloudNode;->TYPE_GOOGLEAPP:Ljava/lang/String;

    .line 19
    const-string v0, "unknown"

    sput-object v0, Lcom/samsung/scloud/data/SCloudNode;->TYPE_UNKNOWN:Ljava/lang/String;

    .line 21
    const-string v0, "owner"

    sput-object v0, Lcom/samsung/scloud/data/SCloudNode;->ROLE_OWNER:Ljava/lang/String;

    .line 22
    const-string v0, "reader"

    sput-object v0, Lcom/samsung/scloud/data/SCloudNode;->ROLE_READER:Ljava/lang/String;

    .line 23
    const-string v0, "writer"

    sput-object v0, Lcom/samsung/scloud/data/SCloudNode;->ROLE_WRITER:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object v1, p0, Lcom/samsung/scloud/data/SCloudNode;->id:Ljava/lang/String;

    .line 39
    iput-object v1, p0, Lcom/samsung/scloud/data/SCloudNode;->name:Ljava/lang/String;

    .line 47
    iput-wide v4, p0, Lcom/samsung/scloud/data/SCloudNode;->createdTimestamp:J

    .line 55
    iput-wide v4, p0, Lcom/samsung/scloud/data/SCloudNode;->updatedTimestamp:J

    .line 63
    iput-wide v4, p0, Lcom/samsung/scloud/data/SCloudNode;->lastViewedTimestamp:J

    .line 71
    iput-object v1, p0, Lcom/samsung/scloud/data/SCloudNode;->description:Ljava/lang/String;

    .line 79
    iput-boolean v2, p0, Lcom/samsung/scloud/data/SCloudNode;->isDeleted:Z

    .line 87
    iput-boolean v2, p0, Lcom/samsung/scloud/data/SCloudNode;->isTrashed:Z

    .line 95
    iput-boolean v2, p0, Lcom/samsung/scloud/data/SCloudNode;->isStarred:Z

    .line 103
    iput-boolean v2, p0, Lcom/samsung/scloud/data/SCloudNode;->isShared:Z

    .line 111
    iput-wide v4, p0, Lcom/samsung/scloud/data/SCloudNode;->size:J

    .line 119
    iput-object v1, p0, Lcom/samsung/scloud/data/SCloudNode;->shareURL:Ljava/lang/String;

    .line 127
    iput-object v1, p0, Lcom/samsung/scloud/data/SCloudNode;->eTag:Ljava/lang/String;

    .line 135
    iput-wide v4, p0, Lcom/samsung/scloud/data/SCloudNode;->quotaBytesUsed:J

    .line 143
    iput-object v1, p0, Lcom/samsung/scloud/data/SCloudNode;->ownerNames:Ljava/util/List;

    .line 151
    iput-object v1, p0, Lcom/samsung/scloud/data/SCloudNode;->downloadUrl:Ljava/lang/String;

    .line 159
    iput-object v1, p0, Lcom/samsung/scloud/data/SCloudNode;->accessControlUrl:Ljava/lang/String;

    .line 167
    iput-object v1, p0, Lcom/samsung/scloud/data/SCloudNode;->parentFolderId:Ljava/lang/String;

    .line 175
    iput-object v1, p0, Lcom/samsung/scloud/data/SCloudNode;->parentFolderIds:Ljava/util/List;

    .line 183
    sget-object v0, Lcom/samsung/scloud/data/SCloudNode;->TYPE_UNKNOWN:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/scloud/data/SCloudNode;->nodeType:Ljava/lang/String;

    .line 191
    iput-object v1, p0, Lcom/samsung/scloud/data/SCloudNode;->absolutePath:Ljava/lang/String;

    .line 199
    iput-object v1, p0, Lcom/samsung/scloud/data/SCloudNode;->repositoryName:Ljava/lang/String;

    .line 207
    iput-object v1, p0, Lcom/samsung/scloud/data/SCloudNode;->repositoryId:Ljava/lang/String;

    .line 215
    iput-object v1, p0, Lcom/samsung/scloud/data/SCloudNode;->role:Ljava/lang/String;

    .line 224
    iput-object v1, p0, Lcom/samsung/scloud/data/SCloudNode;->thumbnailUrl:Ljava/lang/String;

    .line 233
    iput-object v1, p0, Lcom/samsung/scloud/data/SCloudNode;->changeId:Ljava/lang/String;

    .line 241
    iput-boolean v2, p0, Lcom/samsung/scloud/data/SCloudNode;->thumbExists:Z

    return-void
.end method


# virtual methods
.method public getAbsolutePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 756
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudNode;->absolutePath:Ljava/lang/String;

    return-object v0
.end method

.method public getAccessControlUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 430
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudNode;->accessControlUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getChangeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudNode;->changeId:Ljava/lang/String;

    return-object v0
.end method

.method public getCreatedTimestamp()J
    .locals 2

    .prologue
    .line 616
    iget-wide v0, p0, Lcom/samsung/scloud/data/SCloudNode;->createdTimestamp:J

    return-wide v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 656
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudNode;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getDownloadUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 514
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudNode;->downloadUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 574
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudNode;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getLastViewedTimestamp()J
    .locals 2

    .prologue
    .line 350
    iget-wide v0, p0, Lcom/samsung/scloud/data/SCloudNode;->lastViewedTimestamp:J

    return-wide v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 594
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudNode;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudNode;->name:Ljava/lang/String;

    .line 596
    :goto_0
    return-object v0

    .line 595
    :cond_0
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudNode;->absolutePath:Ljava/lang/String;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/samsung/scloud/data/SCloudNode;->absolutePath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 596
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getNodeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 776
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudNode;->nodeType:Ljava/lang/String;

    return-object v0
.end method

.method public getOwnerNames()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 410
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudNode;->ownerNames:Ljava/util/List;

    return-object v0
.end method

.method public getParentFolderId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudNode;->parentFolderId:Ljava/lang/String;

    return-object v0
.end method

.method public getParentFolderIds()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 450
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudNode;->parentFolderIds:Ljava/util/List;

    return-object v0
.end method

.method public getQuotaBytesUsed()J
    .locals 2

    .prologue
    .line 330
    iget-wide v0, p0, Lcom/samsung/scloud/data/SCloudNode;->quotaBytesUsed:J

    return-wide v0
.end method

.method public getRepositoryId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 534
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudNode;->repositoryId:Ljava/lang/String;

    return-object v0
.end method

.method public getRepositoryName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 554
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudNode;->repositoryName:Ljava/lang/String;

    return-object v0
.end method

.method public getRole()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudNode;->role:Ljava/lang/String;

    return-object v0
.end method

.method public getShareURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 736
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudNode;->shareURL:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 716
    iget-wide v0, p0, Lcom/samsung/scloud/data/SCloudNode;->size:J

    return-wide v0
.end method

.method public getThumbnailUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudNode;->thumbnailUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getUpdatedTimestamp()J
    .locals 2

    .prologue
    .line 636
    iget-wide v0, p0, Lcom/samsung/scloud/data/SCloudNode;->updatedTimestamp:J

    return-wide v0
.end method

.method public geteTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/samsung/scloud/data/SCloudNode;->eTag:Ljava/lang/String;

    return-object v0
.end method

.method public isDeleted()Z
    .locals 1

    .prologue
    .line 676
    iget-boolean v0, p0, Lcom/samsung/scloud/data/SCloudNode;->isDeleted:Z

    return v0
.end method

.method public isShared()Z
    .locals 1

    .prologue
    .line 696
    iget-boolean v0, p0, Lcom/samsung/scloud/data/SCloudNode;->isShared:Z

    return v0
.end method

.method public isStarred()Z
    .locals 1

    .prologue
    .line 390
    iget-boolean v0, p0, Lcom/samsung/scloud/data/SCloudNode;->isStarred:Z

    return v0
.end method

.method public isThumbExists()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 252
    iget-boolean v1, p0, Lcom/samsung/scloud/data/SCloudNode;->thumbExists:Z

    if-eqz v1, :cond_1

    .line 254
    :cond_0
    :goto_0
    return v0

    .line 253
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/scloud/data/SCloudNode;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 254
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTrashed()Z
    .locals 1

    .prologue
    .line 370
    iget-boolean v0, p0, Lcom/samsung/scloud/data/SCloudNode;->isTrashed:Z

    return v0
.end method

.method public setAbsolutePath(Ljava/lang/String;)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 766
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudNode;->absolutePath:Ljava/lang/String;

    .line 767
    return-void
.end method

.method public setAccessControlUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "accessControlUrl"    # Ljava/lang/String;

    .prologue
    .line 440
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudNode;->accessControlUrl:Ljava/lang/String;

    .line 441
    return-void
.end method

.method public setChangeId(Ljava/lang/String;)V
    .locals 0
    .param p1, "changeId"    # Ljava/lang/String;

    .prologue
    .line 278
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudNode;->changeId:Ljava/lang/String;

    .line 279
    return-void
.end method

.method public setCreatedTimestamp(J)V
    .locals 1
    .param p1, "createdTimestamp"    # J

    .prologue
    .line 626
    iput-wide p1, p0, Lcom/samsung/scloud/data/SCloudNode;->createdTimestamp:J

    .line 627
    return-void
.end method

.method public setDeleted(Z)V
    .locals 0
    .param p1, "isDeleted"    # Z

    .prologue
    .line 686
    iput-boolean p1, p0, Lcom/samsung/scloud/data/SCloudNode;->isDeleted:Z

    .line 687
    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 666
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudNode;->description:Ljava/lang/String;

    .line 667
    return-void
.end method

.method public setDownloadUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "downloadUrl"    # Ljava/lang/String;

    .prologue
    .line 524
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudNode;->downloadUrl:Ljava/lang/String;

    .line 525
    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 584
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudNode;->id:Ljava/lang/String;

    .line 585
    return-void
.end method

.method public setLastViewedTimestamp(J)V
    .locals 1
    .param p1, "lastViewedTimestamp"    # J

    .prologue
    .line 360
    iput-wide p1, p0, Lcom/samsung/scloud/data/SCloudNode;->lastViewedTimestamp:J

    .line 361
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 606
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudNode;->name:Ljava/lang/String;

    .line 607
    return-void
.end method

.method public setNodeType(Ljava/lang/String;)V
    .locals 0
    .param p1, "nodeType"    # Ljava/lang/String;

    .prologue
    .line 786
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudNode;->nodeType:Ljava/lang/String;

    .line 787
    return-void
.end method

.method public setOwnerNames(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 420
    .local p1, "ownerNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudNode;->ownerNames:Ljava/util/List;

    .line 421
    return-void
.end method

.method public setParentFolderId(Ljava/lang/String;)V
    .locals 0
    .param p1, "parentFolderId"    # Ljava/lang/String;

    .prologue
    .line 298
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudNode;->parentFolderId:Ljava/lang/String;

    .line 299
    return-void
.end method

.method public setParentFolderIds(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 460
    .local p1, "parentFolderIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudNode;->parentFolderIds:Ljava/util/List;

    .line 461
    return-void
.end method

.method public setQuotaBytesUsed(J)V
    .locals 1
    .param p1, "quotaBytesUsed"    # J

    .prologue
    .line 340
    iput-wide p1, p0, Lcom/samsung/scloud/data/SCloudNode;->quotaBytesUsed:J

    .line 341
    return-void
.end method

.method public setRepositoryId(Ljava/lang/String;)V
    .locals 0
    .param p1, "repositoryId"    # Ljava/lang/String;

    .prologue
    .line 544
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudNode;->repositoryId:Ljava/lang/String;

    .line 545
    return-void
.end method

.method public setRepositoryName(Ljava/lang/String;)V
    .locals 0
    .param p1, "repositoryName"    # Ljava/lang/String;

    .prologue
    .line 564
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudNode;->repositoryName:Ljava/lang/String;

    .line 565
    return-void
.end method

.method public setRole(Ljava/lang/String;)V
    .locals 0
    .param p1, "role"    # Ljava/lang/String;

    .prologue
    .line 482
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudNode;->role:Ljava/lang/String;

    .line 483
    return-void
.end method

.method public setShareURL(Ljava/lang/String;)V
    .locals 0
    .param p1, "shareURL"    # Ljava/lang/String;

    .prologue
    .line 746
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudNode;->shareURL:Ljava/lang/String;

    .line 747
    return-void
.end method

.method public setShared(Z)V
    .locals 0
    .param p1, "isShared"    # Z

    .prologue
    .line 706
    iput-boolean p1, p0, Lcom/samsung/scloud/data/SCloudNode;->isShared:Z

    .line 707
    return-void
.end method

.method public setSize(J)V
    .locals 1
    .param p1, "size"    # J

    .prologue
    .line 726
    iput-wide p1, p0, Lcom/samsung/scloud/data/SCloudNode;->size:J

    .line 727
    return-void
.end method

.method public setStarred(Z)V
    .locals 0
    .param p1, "isStarred"    # Z

    .prologue
    .line 400
    iput-boolean p1, p0, Lcom/samsung/scloud/data/SCloudNode;->isStarred:Z

    .line 401
    return-void
.end method

.method public setThumbExists(Z)V
    .locals 0
    .param p1, "thumbExists"    # Z

    .prologue
    .line 264
    iput-boolean p1, p0, Lcom/samsung/scloud/data/SCloudNode;->thumbExists:Z

    .line 265
    return-void
.end method

.method public setThumbnailUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "thumbnailUrl"    # Ljava/lang/String;

    .prologue
    .line 504
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudNode;->thumbnailUrl:Ljava/lang/String;

    .line 505
    return-void
.end method

.method public setTrashed(Z)V
    .locals 0
    .param p1, "isTrashed"    # Z

    .prologue
    .line 380
    iput-boolean p1, p0, Lcom/samsung/scloud/data/SCloudNode;->isTrashed:Z

    .line 381
    return-void
.end method

.method public setUpdatedTimestamp(J)V
    .locals 1
    .param p1, "updatedTimestamp"    # J

    .prologue
    .line 646
    iput-wide p1, p0, Lcom/samsung/scloud/data/SCloudNode;->updatedTimestamp:J

    .line 647
    return-void
.end method

.method public seteTag(Ljava/lang/String;)V
    .locals 0
    .param p1, "etag"    # Ljava/lang/String;

    .prologue
    .line 320
    iput-object p1, p0, Lcom/samsung/scloud/data/SCloudNode;->eTag:Ljava/lang/String;

    .line 321
    return-void
.end method
