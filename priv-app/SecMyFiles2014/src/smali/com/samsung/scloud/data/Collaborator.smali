.class public final Lcom/samsung/scloud/data/Collaborator;
.super Ljava/lang/Object;
.source "Collaborator.java"


# instance fields
.field private additionalRoles:Ljava/util/List;

.field private authkey:Ljava/lang/String;

.field private collaborationId:Ljava/lang/String;

.field private collaborationPath:Ljava/lang/String;

.field private etag:Ljava/lang/String;

.field private photoLink:Ljava/lang/String;

.field private role:Ljava/lang/String;

.field private selfLink:Ljava/lang/String;

.field private status:Ljava/lang/String;

.field private type:Ljava/lang/String;

.field private userEmail:Ljava/lang/String;

.field private userId:J

.field private userName:Ljava/lang/String;

.field private value:Ljava/lang/String;

.field private withLink:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "userEmail"    # Ljava/lang/String;
    .param p2, "role"    # Ljava/lang/String;

    .prologue
    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    iput-object p1, p0, Lcom/samsung/scloud/data/Collaborator;->userEmail:Ljava/lang/String;

    .line 135
    iput-object p2, p0, Lcom/samsung/scloud/data/Collaborator;->role:Ljava/lang/String;

    .line 136
    return-void
.end method


# virtual methods
.method public getAdditionalRoles()Ljava/util/List;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/scloud/data/Collaborator;->additionalRoles:Ljava/util/List;

    return-object v0
.end method

.method public getAuthkey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/samsung/scloud/data/Collaborator;->authkey:Ljava/lang/String;

    return-object v0
.end method

.method public getCollaborationId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/samsung/scloud/data/Collaborator;->collaborationId:Ljava/lang/String;

    return-object v0
.end method

.method public getCollaborationPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/samsung/scloud/data/Collaborator;->collaborationPath:Ljava/lang/String;

    return-object v0
.end method

.method public getEtag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/scloud/data/Collaborator;->etag:Ljava/lang/String;

    return-object v0
.end method

.method public getPhotoLink()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/scloud/data/Collaborator;->photoLink:Ljava/lang/String;

    return-object v0
.end method

.method public getRole()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/samsung/scloud/data/Collaborator;->role:Ljava/lang/String;

    return-object v0
.end method

.method public getSelfLink()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/samsung/scloud/data/Collaborator;->selfLink:Ljava/lang/String;

    return-object v0
.end method

.method public getStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/samsung/scloud/data/Collaborator;->status:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/scloud/data/Collaborator;->type:Ljava/lang/String;

    return-object v0
.end method

.method public getUserEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/samsung/scloud/data/Collaborator;->userEmail:Ljava/lang/String;

    return-object v0
.end method

.method public getUserId()J
    .locals 2

    .prologue
    .line 142
    iget-wide v0, p0, Lcom/samsung/scloud/data/Collaborator;->userId:J

    return-wide v0
.end method

.method public getUserName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/samsung/scloud/data/Collaborator;->userName:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/scloud/data/Collaborator;->value:Ljava/lang/String;

    return-object v0
.end method

.method public isWithLink()Z
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/samsung/scloud/data/Collaborator;->withLink:Z

    return v0
.end method

.method public setAdditionalRoles(Ljava/util/List;)V
    .locals 0
    .param p1, "additionalRoles"    # Ljava/util/List;

    .prologue
    .line 124
    iput-object p1, p0, Lcom/samsung/scloud/data/Collaborator;->additionalRoles:Ljava/util/List;

    .line 125
    return-void
.end method

.method public setAuthkey(Ljava/lang/String;)V
    .locals 0
    .param p1, "authkey"    # Ljava/lang/String;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/samsung/scloud/data/Collaborator;->authkey:Ljava/lang/String;

    .line 117
    return-void
.end method

.method public setCollaborationId(Ljava/lang/String;)V
    .locals 0
    .param p1, "collaborationId"    # Ljava/lang/String;

    .prologue
    .line 186
    iput-object p1, p0, Lcom/samsung/scloud/data/Collaborator;->collaborationId:Ljava/lang/String;

    .line 187
    return-void
.end method

.method public setCollaborationPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "collaborationPath"    # Ljava/lang/String;

    .prologue
    .line 199
    iput-object p1, p0, Lcom/samsung/scloud/data/Collaborator;->collaborationPath:Ljava/lang/String;

    .line 200
    return-void
.end method

.method public setEtag(Ljava/lang/String;)V
    .locals 0
    .param p1, "etag"    # Ljava/lang/String;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/samsung/scloud/data/Collaborator;->etag:Ljava/lang/String;

    .line 70
    return-void
.end method

.method public setPhotoLink(Ljava/lang/String;)V
    .locals 0
    .param p1, "photoLink"    # Ljava/lang/String;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/samsung/scloud/data/Collaborator;->photoLink:Ljava/lang/String;

    .line 94
    return-void
.end method

.method public setRole(Ljava/lang/String;)V
    .locals 0
    .param p1, "role"    # Ljava/lang/String;

    .prologue
    .line 211
    iput-object p1, p0, Lcom/samsung/scloud/data/Collaborator;->role:Ljava/lang/String;

    .line 212
    return-void
.end method

.method public setSelfLink(Ljava/lang/String;)V
    .locals 0
    .param p1, "selfLink"    # Ljava/lang/String;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/samsung/scloud/data/Collaborator;->selfLink:Ljava/lang/String;

    .line 102
    return-void
.end method

.method public setStatus(Ljava/lang/String;)V
    .locals 0
    .param p1, "status"    # Ljava/lang/String;

    .prologue
    .line 225
    iput-object p1, p0, Lcom/samsung/scloud/data/Collaborator;->status:Ljava/lang/String;

    .line 226
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/samsung/scloud/data/Collaborator;->type:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public setUserEmail(Ljava/lang/String;)V
    .locals 0
    .param p1, "userEmail"    # Ljava/lang/String;

    .prologue
    .line 173
    iput-object p1, p0, Lcom/samsung/scloud/data/Collaborator;->userEmail:Ljava/lang/String;

    .line 174
    return-void
.end method

.method public setUserId(J)V
    .locals 1
    .param p1, "userId"    # J

    .prologue
    .line 148
    iput-wide p1, p0, Lcom/samsung/scloud/data/Collaborator;->userId:J

    .line 149
    return-void
.end method

.method public setUserName(Ljava/lang/String;)V
    .locals 0
    .param p1, "userName"    # Ljava/lang/String;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/samsung/scloud/data/Collaborator;->userName:Ljava/lang/String;

    .line 162
    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/samsung/scloud/data/Collaborator;->value:Ljava/lang/String;

    .line 86
    return-void
.end method

.method public setWithLink(Z)V
    .locals 0
    .param p1, "withLink"    # Z

    .prologue
    .line 108
    iput-boolean p1, p0, Lcom/samsung/scloud/data/Collaborator;->withLink:Z

    .line 109
    return-void
.end method
