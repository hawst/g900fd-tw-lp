.class public Lcom/samsung/scloud/data/Update$UpdateEntry;
.super Ljava/lang/Object;
.source "Update.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/scloud/data/Update;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UpdateEntry"
.end annotation


# instance fields
.field private extraLargeThumbnail:Ljava/net/URL;

.field private fileId:J

.field private fileName:Ljava/lang/String;

.field private largeThumbnail:Ljava/net/URL;

.field private sharedName:Ljava/lang/String;

.field private size:J

.field private smallThumbnail:Ljava/net/URL;

.field private type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getExtraLargeThumbnail()Ljava/net/URL;
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcom/samsung/scloud/data/Update$UpdateEntry;->extraLargeThumbnail:Ljava/net/URL;

    return-object v0
.end method

.method public getFileId()J
    .locals 2

    .prologue
    .line 231
    iget-wide v0, p0, Lcom/samsung/scloud/data/Update$UpdateEntry;->fileId:J

    return-wide v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/samsung/scloud/data/Update$UpdateEntry;->fileName:Ljava/lang/String;

    return-object v0
.end method

.method public getLargeThumbnail()Ljava/net/URL;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/samsung/scloud/data/Update$UpdateEntry;->largeThumbnail:Ljava/net/URL;

    return-object v0
.end method

.method public getSharedName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/samsung/scloud/data/Update$UpdateEntry;->sharedName:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 287
    iget-wide v0, p0, Lcom/samsung/scloud/data/Update$UpdateEntry;->size:J

    return-wide v0
.end method

.method public getSmallThumbnail()Ljava/net/URL;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/samsung/scloud/data/Update$UpdateEntry;->smallThumbnail:Ljava/net/URL;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lcom/samsung/scloud/data/Update$UpdateEntry;->type:Ljava/lang/String;

    return-object v0
.end method

.method public setExtraLargeThumbnail(Ljava/net/URL;)V
    .locals 0
    .param p1, "extraLargeThumbnail"    # Ljava/net/URL;

    .prologue
    .line 275
    iput-object p1, p0, Lcom/samsung/scloud/data/Update$UpdateEntry;->extraLargeThumbnail:Ljava/net/URL;

    .line 276
    return-void
.end method

.method public setFileId(J)V
    .locals 1
    .param p1, "fileId"    # J

    .prologue
    .line 235
    iput-wide p1, p0, Lcom/samsung/scloud/data/Update$UpdateEntry;->fileId:J

    .line 236
    return-void
.end method

.method public setFileName(Ljava/lang/String;)V
    .locals 0
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 243
    iput-object p1, p0, Lcom/samsung/scloud/data/Update$UpdateEntry;->fileName:Ljava/lang/String;

    .line 244
    return-void
.end method

.method public setLargeThumbnail(Ljava/net/URL;)V
    .locals 0
    .param p1, "largeThumbnail"    # Ljava/net/URL;

    .prologue
    .line 259
    iput-object p1, p0, Lcom/samsung/scloud/data/Update$UpdateEntry;->largeThumbnail:Ljava/net/URL;

    .line 260
    return-void
.end method

.method public setSharedName(Ljava/lang/String;)V
    .locals 0
    .param p1, "sharedName"    # Ljava/lang/String;

    .prologue
    .line 251
    iput-object p1, p0, Lcom/samsung/scloud/data/Update$UpdateEntry;->sharedName:Ljava/lang/String;

    .line 252
    return-void
.end method

.method public setSize(J)V
    .locals 1
    .param p1, "size"    # J

    .prologue
    .line 291
    iput-wide p1, p0, Lcom/samsung/scloud/data/Update$UpdateEntry;->size:J

    .line 292
    return-void
.end method

.method public setSmallThumbnail(Ljava/net/URL;)V
    .locals 0
    .param p1, "smallThumbnail"    # Ljava/net/URL;

    .prologue
    .line 267
    iput-object p1, p0, Lcom/samsung/scloud/data/Update$UpdateEntry;->smallThumbnail:Ljava/net/URL;

    .line 268
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 283
    iput-object p1, p0, Lcom/samsung/scloud/data/Update$UpdateEntry;->type:Ljava/lang/String;

    .line 284
    return-void
.end method
