.class public final Lcom/samsung/scloud/data/User;
.super Ljava/lang/Object;
.source "User.java"


# static fields
.field public static final ACCOUNT_STATUS_ACTIVE:I = 0x0

.field public static final ACCOUNT_STATUS_BLOCK:I = 0x3

.field public static final ACCOUNT_STATUS_INACTIVE:I = 0x4

.field public static final ACCOUNT_STATUS_PAUSED:I = 0x5

.field public static final ACCOUNT_STATUS_PENDING_TERM:I = 0x2

.field public static final ACCOUNT_STATUS_SUSPENDED:I = 0x1

.field public static final ACCOUNT_STATUS_TERMINATED:I = 0x6


# instance fields
.field protected OOSGuardHigh:J

.field protected OOSGuardLow:J

.field protected accounStatus:I

.field protected displayName:Ljava/lang/String;

.field protected documentMaxUploadSize:J

.field protected drawingMaxUploadSize:J

.field protected etag:Ljava/lang/String;

.field protected fileMaxUploadSize:J

.field protected largestChangeId:Ljava/lang/String;

.field protected mEmail:Ljava/lang/String;

.field protected mLogin:Ljava/lang/String;

.field protected mMaxUploadSize:J

.field protected mSpaceAmount:J

.field protected mSpaceUsed:J

.field protected mUserId:J

.field protected name:Ljava/lang/String;

.field protected pdfMaxUploadSize:J

.field protected permissionId:Ljava/lang/String;

.field protected presentationMaxUploadSize:J

.field protected quotaBytesShared:J

.field protected quotaBytesTotal:J

.field protected quotaBytesUsed:J

.field protected quotaBytesUsedInTrash:J

.field protected repositoyCount:I

.field protected rootFolderId:Ljava/lang/String;

.field protected spaceShared:J

.field protected spreadsheetMaxUploadSize:J


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/scloud/data/User;->displayName:Ljava/lang/String;

    .line 57
    iput v4, p0, Lcom/samsung/scloud/data/User;->accounStatus:I

    .line 60
    iput v4, p0, Lcom/samsung/scloud/data/User;->repositoyCount:I

    .line 63
    iput-wide v2, p0, Lcom/samsung/scloud/data/User;->spaceShared:J

    .line 66
    iput-wide v2, p0, Lcom/samsung/scloud/data/User;->OOSGuardLow:J

    .line 69
    iput-wide v2, p0, Lcom/samsung/scloud/data/User;->OOSGuardHigh:J

    .line 77
    iput-object v1, p0, Lcom/samsung/scloud/data/User;->etag:Ljava/lang/String;

    .line 84
    iput-object v1, p0, Lcom/samsung/scloud/data/User;->name:Ljava/lang/String;

    .line 91
    iput-object v1, p0, Lcom/samsung/scloud/data/User;->largestChangeId:Ljava/lang/String;

    .line 97
    iput-wide v2, p0, Lcom/samsung/scloud/data/User;->quotaBytesTotal:J

    .line 103
    iput-wide v2, p0, Lcom/samsung/scloud/data/User;->quotaBytesUsed:J

    .line 109
    iput-wide v2, p0, Lcom/samsung/scloud/data/User;->quotaBytesUsedInTrash:J

    .line 115
    iput-wide v2, p0, Lcom/samsung/scloud/data/User;->quotaBytesShared:J

    .line 121
    iput-wide v2, p0, Lcom/samsung/scloud/data/User;->documentMaxUploadSize:J

    .line 128
    iput-wide v2, p0, Lcom/samsung/scloud/data/User;->spreadsheetMaxUploadSize:J

    .line 135
    iput-wide v2, p0, Lcom/samsung/scloud/data/User;->presentationMaxUploadSize:J

    .line 142
    iput-wide v2, p0, Lcom/samsung/scloud/data/User;->drawingMaxUploadSize:J

    .line 149
    iput-wide v2, p0, Lcom/samsung/scloud/data/User;->pdfMaxUploadSize:J

    .line 156
    iput-wide v2, p0, Lcom/samsung/scloud/data/User;->fileMaxUploadSize:J

    .line 163
    iput-object v1, p0, Lcom/samsung/scloud/data/User;->rootFolderId:Ljava/lang/String;

    .line 169
    iput-object v1, p0, Lcom/samsung/scloud/data/User;->permissionId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAccounStatus()I
    .locals 1

    .prologue
    .line 351
    iget v0, p0, Lcom/samsung/scloud/data/User;->accounStatus:I

    return v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/samsung/scloud/data/User;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public getDocumentMaxUploadSize()J
    .locals 2

    .prologue
    .line 267
    iget-wide v0, p0, Lcom/samsung/scloud/data/User;->documentMaxUploadSize:J

    return-wide v0
.end method

.method public getDrawingMaxUploadSize()J
    .locals 2

    .prologue
    .line 297
    iget-wide v0, p0, Lcom/samsung/scloud/data/User;->drawingMaxUploadSize:J

    return-wide v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 463
    iget-object v0, p0, Lcom/samsung/scloud/data/User;->mEmail:Ljava/lang/String;

    return-object v0
.end method

.method public getEtag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/samsung/scloud/data/User;->etag:Ljava/lang/String;

    return-object v0
.end method

.method public getFileMaxUploadSize()J
    .locals 2

    .prologue
    .line 317
    iget-wide v0, p0, Lcom/samsung/scloud/data/User;->fileMaxUploadSize:J

    return-wide v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 423
    iget-wide v0, p0, Lcom/samsung/scloud/data/User;->mUserId:J

    return-wide v0
.end method

.method public getLargestChangeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/samsung/scloud/data/User;->largestChangeId:Ljava/lang/String;

    return-object v0
.end method

.method public getLoginId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 444
    iget-object v0, p0, Lcom/samsung/scloud/data/User;->mLogin:Ljava/lang/String;

    return-object v0
.end method

.method public getMaxUploadSize()J
    .locals 2

    .prologue
    .line 520
    iget-wide v0, p0, Lcom/samsung/scloud/data/User;->mMaxUploadSize:J

    return-wide v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/samsung/scloud/data/User;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getOOSGuardHigh()J
    .locals 2

    .prologue
    .line 407
    iget-wide v0, p0, Lcom/samsung/scloud/data/User;->OOSGuardHigh:J

    return-wide v0
.end method

.method public getOOSGuardLow()J
    .locals 2

    .prologue
    .line 393
    iget-wide v0, p0, Lcom/samsung/scloud/data/User;->OOSGuardLow:J

    return-wide v0
.end method

.method public getPdfMaxUploadSize()J
    .locals 2

    .prologue
    .line 307
    iget-wide v0, p0, Lcom/samsung/scloud/data/User;->pdfMaxUploadSize:J

    return-wide v0
.end method

.method public getPermissionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/samsung/scloud/data/User;->permissionId:Ljava/lang/String;

    return-object v0
.end method

.method public getPresentationMaxUploadSize()J
    .locals 2

    .prologue
    .line 287
    iget-wide v0, p0, Lcom/samsung/scloud/data/User;->presentationMaxUploadSize:J

    return-wide v0
.end method

.method public getQuotaBytesShared()J
    .locals 2

    .prologue
    .line 258
    iget-wide v0, p0, Lcom/samsung/scloud/data/User;->quotaBytesShared:J

    return-wide v0
.end method

.method public getQuotaBytesTotal()J
    .locals 2

    .prologue
    .line 230
    iget-wide v0, p0, Lcom/samsung/scloud/data/User;->quotaBytesTotal:J

    return-wide v0
.end method

.method public getQuotaBytesUsed()J
    .locals 2

    .prologue
    .line 240
    iget-wide v0, p0, Lcom/samsung/scloud/data/User;->quotaBytesUsed:J

    return-wide v0
.end method

.method public getQuotaBytesUsedInTrash()J
    .locals 2

    .prologue
    .line 250
    iget-wide v0, p0, Lcom/samsung/scloud/data/User;->quotaBytesUsedInTrash:J

    return-wide v0
.end method

.method public getRepositoyCount()I
    .locals 1

    .prologue
    .line 365
    iget v0, p0, Lcom/samsung/scloud/data/User;->repositoyCount:I

    return v0
.end method

.method public getRootFolderId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/samsung/scloud/data/User;->rootFolderId:Ljava/lang/String;

    return-object v0
.end method

.method public getSpaceAmount()J
    .locals 2

    .prologue
    .line 482
    iget-wide v0, p0, Lcom/samsung/scloud/data/User;->mSpaceAmount:J

    return-wide v0
.end method

.method public getSpaceShared()J
    .locals 2

    .prologue
    .line 379
    iget-wide v0, p0, Lcom/samsung/scloud/data/User;->spaceShared:J

    return-wide v0
.end method

.method public getSpaceUsed()J
    .locals 2

    .prologue
    .line 501
    iget-wide v0, p0, Lcom/samsung/scloud/data/User;->mSpaceUsed:J

    return-wide v0
.end method

.method public getSpreadsheetMaxUploadSize()J
    .locals 2

    .prologue
    .line 277
    iget-wide v0, p0, Lcom/samsung/scloud/data/User;->spreadsheetMaxUploadSize:J

    return-wide v0
.end method

.method public setAccounStatus(I)V
    .locals 0
    .param p1, "accounStatus"    # I

    .prologue
    .line 358
    iput p1, p0, Lcom/samsung/scloud/data/User;->accounStatus:I

    .line 359
    return-void
.end method

.method public setDisplayName(Ljava/lang/String;)V
    .locals 0
    .param p1, "displayName"    # Ljava/lang/String;

    .prologue
    .line 344
    iput-object p1, p0, Lcom/samsung/scloud/data/User;->displayName:Ljava/lang/String;

    .line 345
    return-void
.end method

.method public setDocumentMaxUploadSize(J)V
    .locals 1
    .param p1, "documentMaxUploadSize"    # J

    .prologue
    .line 271
    iput-wide p1, p0, Lcom/samsung/scloud/data/User;->documentMaxUploadSize:J

    .line 272
    return-void
.end method

.method public setDrawingMaxUploadSize(J)V
    .locals 1
    .param p1, "drawingMaxUploadSize"    # J

    .prologue
    .line 301
    iput-wide p1, p0, Lcom/samsung/scloud/data/User;->drawingMaxUploadSize:J

    .line 302
    return-void
.end method

.method public setEmail(Ljava/lang/String;)V
    .locals 0
    .param p1, "email"    # Ljava/lang/String;

    .prologue
    .line 454
    iput-object p1, p0, Lcom/samsung/scloud/data/User;->mEmail:Ljava/lang/String;

    .line 455
    return-void
.end method

.method public setEtag(Ljava/lang/String;)V
    .locals 0
    .param p1, "etag"    # Ljava/lang/String;

    .prologue
    .line 203
    iput-object p1, p0, Lcom/samsung/scloud/data/User;->etag:Ljava/lang/String;

    .line 204
    return-void
.end method

.method public setFileMaxUploadSize(J)V
    .locals 1
    .param p1, "fileMaxUploadSize"    # J

    .prologue
    .line 321
    iput-wide p1, p0, Lcom/samsung/scloud/data/User;->fileMaxUploadSize:J

    .line 322
    return-void
.end method

.method public setId(J)V
    .locals 1
    .param p1, "userId"    # J

    .prologue
    .line 330
    iput-wide p1, p0, Lcom/samsung/scloud/data/User;->mUserId:J

    .line 331
    return-void
.end method

.method public setLargestChangeId(Ljava/lang/String;)V
    .locals 0
    .param p1, "changeId"    # Ljava/lang/String;

    .prologue
    .line 223
    iput-object p1, p0, Lcom/samsung/scloud/data/User;->largestChangeId:Ljava/lang/String;

    .line 224
    return-void
.end method

.method public setLoginId(Ljava/lang/String;)V
    .locals 0
    .param p1, "login"    # Ljava/lang/String;

    .prologue
    .line 435
    iput-object p1, p0, Lcom/samsung/scloud/data/User;->mLogin:Ljava/lang/String;

    .line 436
    return-void
.end method

.method public setMaxUploadSize(J)V
    .locals 1
    .param p1, "maxUploadSize"    # J

    .prologue
    .line 511
    iput-wide p1, p0, Lcom/samsung/scloud/data/User;->mMaxUploadSize:J

    .line 512
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 213
    iput-object p1, p0, Lcom/samsung/scloud/data/User;->name:Ljava/lang/String;

    .line 214
    return-void
.end method

.method public setOOSGuardHigh(J)V
    .locals 1
    .param p1, "oOSGuardHigh"    # J

    .prologue
    .line 414
    iput-wide p1, p0, Lcom/samsung/scloud/data/User;->OOSGuardHigh:J

    .line 415
    return-void
.end method

.method public setOOSGuardLow(J)V
    .locals 1
    .param p1, "oOSGuardLow"    # J

    .prologue
    .line 400
    iput-wide p1, p0, Lcom/samsung/scloud/data/User;->OOSGuardLow:J

    .line 401
    return-void
.end method

.method public setPdfMaxUploadSize(J)V
    .locals 1
    .param p1, "pdfMaxUploadSize"    # J

    .prologue
    .line 311
    iput-wide p1, p0, Lcom/samsung/scloud/data/User;->pdfMaxUploadSize:J

    .line 312
    return-void
.end method

.method public setPermissionId(Ljava/lang/String;)V
    .locals 0
    .param p1, "permissionId"    # Ljava/lang/String;

    .prologue
    .line 181
    iput-object p1, p0, Lcom/samsung/scloud/data/User;->permissionId:Ljava/lang/String;

    .line 182
    return-void
.end method

.method public setPresentationMaxUploadSize(J)V
    .locals 1
    .param p1, "presentationMaxUploadSize"    # J

    .prologue
    .line 291
    iput-wide p1, p0, Lcom/samsung/scloud/data/User;->presentationMaxUploadSize:J

    .line 292
    return-void
.end method

.method public setQuotaBytesShared(J)V
    .locals 1
    .param p1, "quotaBytesShared"    # J

    .prologue
    .line 261
    iput-wide p1, p0, Lcom/samsung/scloud/data/User;->quotaBytesShared:J

    .line 262
    return-void
.end method

.method public setQuotaBytesTotal(J)V
    .locals 1
    .param p1, "quotaBytesTotal"    # J

    .prologue
    .line 234
    iput-wide p1, p0, Lcom/samsung/scloud/data/User;->quotaBytesTotal:J

    .line 235
    return-void
.end method

.method public setQuotaBytesUsed(J)V
    .locals 1
    .param p1, "quotaBytesUsed"    # J

    .prologue
    .line 244
    iput-wide p1, p0, Lcom/samsung/scloud/data/User;->quotaBytesUsed:J

    .line 245
    return-void
.end method

.method public setQuotaBytesUsedInTrash(J)V
    .locals 1
    .param p1, "quotaBytesUsedInTrash"    # J

    .prologue
    .line 254
    iput-wide p1, p0, Lcom/samsung/scloud/data/User;->quotaBytesUsedInTrash:J

    .line 255
    return-void
.end method

.method public setRepositoyCount(I)V
    .locals 0
    .param p1, "repositoyCount"    # I

    .prologue
    .line 372
    iput p1, p0, Lcom/samsung/scloud/data/User;->repositoyCount:I

    .line 373
    return-void
.end method

.method public setRootFolderId(Ljava/lang/String;)V
    .locals 0
    .param p1, "rootFolderId"    # Ljava/lang/String;

    .prologue
    .line 192
    iput-object p1, p0, Lcom/samsung/scloud/data/User;->rootFolderId:Ljava/lang/String;

    .line 193
    return-void
.end method

.method public setSpaceAmount(J)V
    .locals 1
    .param p1, "spaceAmount"    # J

    .prologue
    .line 473
    iput-wide p1, p0, Lcom/samsung/scloud/data/User;->mSpaceAmount:J

    .line 474
    return-void
.end method

.method public setSpaceShared(J)V
    .locals 1
    .param p1, "spaceShared"    # J

    .prologue
    .line 386
    iput-wide p1, p0, Lcom/samsung/scloud/data/User;->spaceShared:J

    .line 387
    return-void
.end method

.method public setSpaceUsed(J)V
    .locals 1
    .param p1, "spaceUsed"    # J

    .prologue
    .line 492
    iput-wide p1, p0, Lcom/samsung/scloud/data/User;->mSpaceUsed:J

    .line 493
    return-void
.end method

.method public setSpreadsheetMaxUploadSize(J)V
    .locals 1
    .param p1, "spreadsheetMaxUploadSize"    # J

    .prologue
    .line 281
    iput-wide p1, p0, Lcom/samsung/scloud/data/User;->spreadsheetMaxUploadSize:J

    .line 282
    return-void
.end method
