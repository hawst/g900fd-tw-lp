.class public Lcom/samsung/scloud/data/Metadata;
.super Ljava/lang/Object;
.source "Metadata.java"


# instance fields
.field private albumArt:Ljava/lang/String;

.field private albumArtist:Ljava/lang/String;

.field private albumTitle:Ljava/lang/String;

.field private artistName:Ljava/lang/String;

.field private bitrate:J

.field private category:Ljava/lang/String;

.field private compilation:Ljava/lang/String;

.field private composer:Ljava/lang/String;

.field private date:Ljava/lang/String;

.field private duration:J

.field private genre:Ljava/lang/String;

.field private height:I

.field private latitude:Ljava/lang/String;

.field private longitude:Ljava/lang/String;

.field private orientation:I

.field private resolution:Ljava/lang/String;

.field private trackNumber:J

.field private trackTitle:Ljava/lang/String;

.field private width:I


# direct methods
.method public constructor <init>(Lcom/samsung/scloud/data/Metadata;)V
    .locals 4
    .param p1, "metadata"    # Lcom/samsung/scloud/data/Metadata;

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->artistName:Ljava/lang/String;

    .line 8
    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->albumTitle:Ljava/lang/String;

    .line 9
    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->albumArtist:Ljava/lang/String;

    .line 10
    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->albumArt:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->composer:Ljava/lang/String;

    .line 12
    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->trackTitle:Ljava/lang/String;

    .line 13
    iput-wide v2, p0, Lcom/samsung/scloud/data/Metadata;->trackNumber:J

    .line 14
    iput-wide v2, p0, Lcom/samsung/scloud/data/Metadata;->duration:J

    .line 15
    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->compilation:Ljava/lang/String;

    .line 16
    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->date:Ljava/lang/String;

    .line 17
    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->genre:Ljava/lang/String;

    .line 19
    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->category:Ljava/lang/String;

    .line 21
    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->resolution:Ljava/lang/String;

    .line 22
    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->latitude:Ljava/lang/String;

    .line 23
    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->longitude:Ljava/lang/String;

    .line 25
    iput v1, p0, Lcom/samsung/scloud/data/Metadata;->width:I

    .line 26
    iput v1, p0, Lcom/samsung/scloud/data/Metadata;->height:I

    .line 27
    iput v1, p0, Lcom/samsung/scloud/data/Metadata;->orientation:I

    .line 28
    iput-wide v2, p0, Lcom/samsung/scloud/data/Metadata;->bitrate:J

    .line 65
    iget-object v0, p1, Lcom/samsung/scloud/data/Metadata;->artistName:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->artistName:Ljava/lang/String;

    .line 66
    iget-object v0, p1, Lcom/samsung/scloud/data/Metadata;->albumTitle:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->albumTitle:Ljava/lang/String;

    .line 67
    iget-object v0, p1, Lcom/samsung/scloud/data/Metadata;->albumArtist:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->albumArtist:Ljava/lang/String;

    .line 68
    iget-object v0, p1, Lcom/samsung/scloud/data/Metadata;->albumArt:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->albumArt:Ljava/lang/String;

    .line 69
    iget-object v0, p1, Lcom/samsung/scloud/data/Metadata;->composer:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->composer:Ljava/lang/String;

    .line 70
    iget-object v0, p1, Lcom/samsung/scloud/data/Metadata;->trackTitle:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->trackTitle:Ljava/lang/String;

    .line 71
    iget-wide v0, p1, Lcom/samsung/scloud/data/Metadata;->trackNumber:J

    iput-wide v0, p0, Lcom/samsung/scloud/data/Metadata;->trackNumber:J

    .line 72
    iget-wide v0, p1, Lcom/samsung/scloud/data/Metadata;->duration:J

    iput-wide v0, p0, Lcom/samsung/scloud/data/Metadata;->duration:J

    .line 73
    iget-object v0, p1, Lcom/samsung/scloud/data/Metadata;->compilation:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->compilation:Ljava/lang/String;

    .line 74
    iget-object v0, p1, Lcom/samsung/scloud/data/Metadata;->date:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->date:Ljava/lang/String;

    .line 75
    iget-object v0, p1, Lcom/samsung/scloud/data/Metadata;->genre:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->genre:Ljava/lang/String;

    .line 76
    iget-object v0, p1, Lcom/samsung/scloud/data/Metadata;->category:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->category:Ljava/lang/String;

    .line 77
    iget-object v0, p1, Lcom/samsung/scloud/data/Metadata;->resolution:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->resolution:Ljava/lang/String;

    .line 78
    iget-object v0, p1, Lcom/samsung/scloud/data/Metadata;->latitude:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->latitude:Ljava/lang/String;

    .line 79
    iget-object v0, p1, Lcom/samsung/scloud/data/Metadata;->longitude:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->longitude:Ljava/lang/String;

    .line 81
    iget v0, p1, Lcom/samsung/scloud/data/Metadata;->width:I

    iput v0, p0, Lcom/samsung/scloud/data/Metadata;->width:I

    .line 82
    iget v0, p1, Lcom/samsung/scloud/data/Metadata;->height:I

    iput v0, p0, Lcom/samsung/scloud/data/Metadata;->height:I

    .line 83
    iget v0, p1, Lcom/samsung/scloud/data/Metadata;->orientation:I

    iput v0, p0, Lcom/samsung/scloud/data/Metadata;->orientation:I

    .line 84
    iget-wide v0, p1, Lcom/samsung/scloud/data/Metadata;->bitrate:J

    iput-wide v0, p0, Lcom/samsung/scloud/data/Metadata;->bitrate:J

    .line 86
    return-void
.end method

.method public constructor <init>(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "metadataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-wide/16 v2, -0x1

    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->artistName:Ljava/lang/String;

    .line 8
    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->albumTitle:Ljava/lang/String;

    .line 9
    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->albumArtist:Ljava/lang/String;

    .line 10
    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->albumArt:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->composer:Ljava/lang/String;

    .line 12
    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->trackTitle:Ljava/lang/String;

    .line 13
    iput-wide v2, p0, Lcom/samsung/scloud/data/Metadata;->trackNumber:J

    .line 14
    iput-wide v2, p0, Lcom/samsung/scloud/data/Metadata;->duration:J

    .line 15
    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->compilation:Ljava/lang/String;

    .line 16
    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->date:Ljava/lang/String;

    .line 17
    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->genre:Ljava/lang/String;

    .line 19
    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->category:Ljava/lang/String;

    .line 21
    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->resolution:Ljava/lang/String;

    .line 22
    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->latitude:Ljava/lang/String;

    .line 23
    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->longitude:Ljava/lang/String;

    .line 25
    iput v1, p0, Lcom/samsung/scloud/data/Metadata;->width:I

    .line 26
    iput v1, p0, Lcom/samsung/scloud/data/Metadata;->height:I

    .line 27
    iput v1, p0, Lcom/samsung/scloud/data/Metadata;->orientation:I

    .line 28
    iput-wide v2, p0, Lcom/samsung/scloud/data/Metadata;->bitrate:J

    .line 33
    const-string v0, "artistName"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->artistName:Ljava/lang/String;

    .line 34
    const-string v0, "albumTitle"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->albumTitle:Ljava/lang/String;

    .line 35
    const-string v0, "albumArtist"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->albumArtist:Ljava/lang/String;

    .line 36
    const-string v0, "albumArt"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->albumArt:Ljava/lang/String;

    .line 37
    const-string v0, "composer"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->composer:Ljava/lang/String;

    .line 38
    const-string v0, "trackTitle"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->trackTitle:Ljava/lang/String;

    .line 39
    const-string v0, "trackNumber"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 40
    const-string v0, "trackNumber"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/scloud/data/Metadata;->trackNumber:J

    .line 41
    :cond_0
    const-string v0, "duration"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 42
    const-string v0, "duration"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/scloud/data/Metadata;->duration:J

    .line 43
    :cond_1
    const-string v0, "compilation"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->compilation:Ljava/lang/String;

    .line 44
    const-string v0, "date"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->date:Ljava/lang/String;

    .line 45
    const-string v0, "genre"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->genre:Ljava/lang/String;

    .line 46
    const-string v0, "category"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->category:Ljava/lang/String;

    .line 47
    const-string v0, "resolution"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->resolution:Ljava/lang/String;

    .line 48
    const-string v0, "latitude"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->latitude:Ljava/lang/String;

    .line 49
    const-string v0, "longitude"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/scloud/data/Metadata;->longitude:Ljava/lang/String;

    .line 51
    const-string v0, "width"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 52
    const-string v0, "width"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/scloud/data/Metadata;->width:I

    .line 53
    :cond_2
    const-string v0, "height"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 54
    const-string v0, "height"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/scloud/data/Metadata;->height:I

    .line 55
    :cond_3
    const-string v0, "orientation"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 56
    const-string v0, "orientation"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/scloud/data/Metadata;->orientation:I

    .line 57
    :cond_4
    const-string v0, "bitrate"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 58
    const-string v0, "bitrate"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/scloud/data/Metadata;->bitrate:J

    .line 61
    :cond_5
    return-void
.end method


# virtual methods
.method public getAlbumArt()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/scloud/data/Metadata;->albumArt:Ljava/lang/String;

    return-object v0
.end method

.method public getAlbumArtist()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/samsung/scloud/data/Metadata;->albumArtist:Ljava/lang/String;

    return-object v0
.end method

.method public getAlbumTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/samsung/scloud/data/Metadata;->albumTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getArtistName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/scloud/data/Metadata;->artistName:Ljava/lang/String;

    return-object v0
.end method

.method public getBitrate()J
    .locals 2

    .prologue
    .line 161
    iget-wide v0, p0, Lcom/samsung/scloud/data/Metadata;->bitrate:J

    return-wide v0
.end method

.method public getCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/samsung/scloud/data/Metadata;->category:Ljava/lang/String;

    return-object v0
.end method

.method public getCompilation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/samsung/scloud/data/Metadata;->compilation:Ljava/lang/String;

    return-object v0
.end method

.method public getComposer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/samsung/scloud/data/Metadata;->composer:Ljava/lang/String;

    return-object v0
.end method

.method public getDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/samsung/scloud/data/Metadata;->date:Ljava/lang/String;

    return-object v0
.end method

.method public getDuration()J
    .locals 2

    .prologue
    .line 117
    iget-wide v0, p0, Lcom/samsung/scloud/data/Metadata;->duration:J

    return-wide v0
.end method

.method public getGenre()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/samsung/scloud/data/Metadata;->genre:Ljava/lang/String;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 153
    iget v0, p0, Lcom/samsung/scloud/data/Metadata;->height:I

    return v0
.end method

.method public getLatitude()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/samsung/scloud/data/Metadata;->latitude:Ljava/lang/String;

    return-object v0
.end method

.method public getLongitude()Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/samsung/scloud/data/Metadata;->longitude:Ljava/lang/String;

    return-object v0
.end method

.method public getOrientation()I
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lcom/samsung/scloud/data/Metadata;->orientation:I

    return v0
.end method

.method public getResolution()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/samsung/scloud/data/Metadata;->resolution:Ljava/lang/String;

    return-object v0
.end method

.method public getTrackNumber()J
    .locals 2

    .prologue
    .line 113
    iget-wide v0, p0, Lcom/samsung/scloud/data/Metadata;->trackNumber:J

    return-wide v0
.end method

.method public getTrackTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/samsung/scloud/data/Metadata;->trackTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lcom/samsung/scloud/data/Metadata;->width:I

    return v0
.end method
