.class public Lcom/samsung/scloud/data/Comment;
.super Ljava/lang/Object;
.source "Comment.java"


# instance fields
.field private avatarUrl:Ljava/lang/String;

.field private comment:Ljava/lang/String;

.field private commentId:J

.field private createTimestamp:J

.field private userId:J

.field private userName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAvatarUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/samsung/scloud/data/Comment;->avatarUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getComment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/scloud/data/Comment;->comment:Ljava/lang/String;

    return-object v0
.end method

.method public getCommentId()J
    .locals 2

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/samsung/scloud/data/Comment;->commentId:J

    return-wide v0
.end method

.method public getCreateTimestamp()J
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/samsung/scloud/data/Comment;->createTimestamp:J

    return-wide v0
.end method

.method public getUserId()J
    .locals 2

    .prologue
    .line 62
    iget-wide v0, p0, Lcom/samsung/scloud/data/Comment;->userId:J

    return-wide v0
.end method

.method public getUserName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/samsung/scloud/data/Comment;->userName:Ljava/lang/String;

    return-object v0
.end method

.method public setAvatarUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "avatarUrl"    # Ljava/lang/String;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/samsung/scloud/data/Comment;->avatarUrl:Ljava/lang/String;

    .line 83
    return-void
.end method

.method public setComment(Ljava/lang/String;)V
    .locals 0
    .param p1, "comment"    # Ljava/lang/String;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/samsung/scloud/data/Comment;->comment:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public setCommentId(J)V
    .locals 1
    .param p1, "commentId"    # J

    .prologue
    .line 42
    iput-wide p1, p0, Lcom/samsung/scloud/data/Comment;->commentId:J

    .line 43
    return-void
.end method

.method public setCreateTimestamp(J)V
    .locals 1
    .param p1, "createTimestamp"    # J

    .prologue
    .line 58
    iput-wide p1, p0, Lcom/samsung/scloud/data/Comment;->createTimestamp:J

    .line 59
    return-void
.end method

.method public setUserId(J)V
    .locals 1
    .param p1, "userId"    # J

    .prologue
    .line 66
    iput-wide p1, p0, Lcom/samsung/scloud/data/Comment;->userId:J

    .line 67
    return-void
.end method

.method public setUserName(Ljava/lang/String;)V
    .locals 0
    .param p1, "userName"    # Ljava/lang/String;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/samsung/scloud/data/Comment;->userName:Ljava/lang/String;

    .line 75
    return-void
.end method
