.class public Lcom/samsung/scloud/data/AlbumData;
.super Ljava/lang/Object;
.source "AlbumData.java"


# instance fields
.field protected id:Ljava/lang/String;

.field protected name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    iput-object v0, p0, Lcom/samsung/scloud/data/AlbumData;->name:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lcom/samsung/scloud/data/AlbumData;->id:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/samsung/scloud/data/AlbumData;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/samsung/scloud/data/AlbumData;->name:Ljava/lang/String;

    return-object v0
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/samsung/scloud/data/AlbumData;->id:Ljava/lang/String;

    .line 24
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 17
    iput-object p1, p0, Lcom/samsung/scloud/data/AlbumData;->name:Ljava/lang/String;

    .line 18
    return-void
.end method
