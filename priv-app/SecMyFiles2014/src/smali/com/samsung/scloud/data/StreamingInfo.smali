.class public final Lcom/samsung/scloud/data/StreamingInfo;
.super Ljava/lang/Object;
.source "StreamingInfo.java"


# instance fields
.field protected expireTime:Ljava/util/Date;

.field protected streamUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object v0, p0, Lcom/samsung/scloud/data/StreamingInfo;->streamUrl:Ljava/lang/String;

    .line 16
    iput-object v0, p0, Lcom/samsung/scloud/data/StreamingInfo;->expireTime:Ljava/util/Date;

    return-void
.end method


# virtual methods
.method public getExpiry()Ljava/util/Date;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/scloud/data/StreamingInfo;->expireTime:Ljava/util/Date;

    return-object v0
.end method

.method public getStreamUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/samsung/scloud/data/StreamingInfo;->streamUrl:Ljava/lang/String;

    return-object v0
.end method

.method public setExpiry(Ljava/util/Date;)V
    .locals 0
    .param p1, "expiryTime"    # Ljava/util/Date;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/samsung/scloud/data/StreamingInfo;->expireTime:Ljava/util/Date;

    .line 32
    return-void
.end method

.method public setStreamUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/samsung/scloud/data/StreamingInfo;->streamUrl:Ljava/lang/String;

    .line 24
    return-void
.end method
