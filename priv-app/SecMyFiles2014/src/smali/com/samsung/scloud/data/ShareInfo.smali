.class public Lcom/samsung/scloud/data/ShareInfo;
.super Ljava/lang/Object;
.source "ShareInfo.java"


# instance fields
.field private expireTimestamp:J

.field private isShared:Z

.field private password:Ljava/lang/String;

.field private shareType:I

.field private shareURL:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getExpireTimestamp()J
    .locals 2

    .prologue
    .line 35
    iget-wide v0, p0, Lcom/samsung/scloud/data/ShareInfo;->expireTimestamp:J

    return-wide v0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/scloud/data/ShareInfo;->password:Ljava/lang/String;

    return-object v0
.end method

.method public getShareType()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/samsung/scloud/data/ShareInfo;->shareType:I

    return v0
.end method

.method public getShareURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/scloud/data/ShareInfo;->shareURL:Ljava/lang/String;

    return-object v0
.end method

.method public isShared()Z
    .locals 1

    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/samsung/scloud/data/ShareInfo;->isShared:Z

    return v0
.end method

.method public setExpireTimestamp(J)V
    .locals 1
    .param p1, "expireTimestamp"    # J

    .prologue
    .line 38
    iput-wide p1, p0, Lcom/samsung/scloud/data/ShareInfo;->expireTimestamp:J

    .line 39
    return-void
.end method

.method public setPassword(Ljava/lang/String;)V
    .locals 0
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/samsung/scloud/data/ShareInfo;->password:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public setShareType(I)V
    .locals 0
    .param p1, "shareType"    # I

    .prologue
    .line 32
    iput p1, p0, Lcom/samsung/scloud/data/ShareInfo;->shareType:I

    .line 33
    return-void
.end method

.method public setShareURL(Ljava/lang/String;)V
    .locals 0
    .param p1, "shareURL"    # Ljava/lang/String;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/samsung/scloud/data/ShareInfo;->shareURL:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public setShared(Z)V
    .locals 0
    .param p1, "isShared"    # Z

    .prologue
    .line 20
    iput-boolean p1, p0, Lcom/samsung/scloud/data/ShareInfo;->isShared:Z

    .line 21
    return-void
.end method
