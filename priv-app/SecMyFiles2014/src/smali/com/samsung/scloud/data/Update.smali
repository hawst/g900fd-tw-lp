.class public final Lcom/samsung/scloud/data/Update;
.super Ljava/lang/Object;
.source "Update.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/scloud/data/Update$UpdateEntry;
    }
.end annotation


# instance fields
.field private files:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Update$UpdateEntry;",
            ">;"
        }
    .end annotation
.end field

.field private folderId:J

.field private folderName:Ljava/lang/String;

.field private folderPath:Ljava/lang/String;

.field private folders:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Update$UpdateEntry;",
            ">;"
        }
    .end annotation
.end field

.field private isCollabAccess:Z

.field private isShared:Z

.field private ownerId:J

.field private sharedName:Ljava/lang/String;

.field private updateId:J

.field private updated:J

.field private updatedType:Ljava/lang/String;

.field private userEmail:Ljava/lang/String;

.field private userId:J

.field private userName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 204
    return-void
.end method


# virtual methods
.method public getFiles()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Update$UpdateEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 170
    iget-object v0, p0, Lcom/samsung/scloud/data/Update;->files:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getFolderId()J
    .locals 2

    .prologue
    .line 122
    iget-wide v0, p0, Lcom/samsung/scloud/data/Update;->folderId:J

    return-wide v0
.end method

.method public getFolderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/samsung/scloud/data/Update;->folderName:Ljava/lang/String;

    return-object v0
.end method

.method public getFolderPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/samsung/scloud/data/Update;->folderPath:Ljava/lang/String;

    return-object v0
.end method

.method public getFolders()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Update$UpdateEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 178
    iget-object v0, p0, Lcom/samsung/scloud/data/Update;->folders:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getOwnerId()J
    .locals 2

    .prologue
    .line 146
    iget-wide v0, p0, Lcom/samsung/scloud/data/Update;->ownerId:J

    return-wide v0
.end method

.method public getSharedName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/samsung/scloud/data/Update;->sharedName:Ljava/lang/String;

    return-object v0
.end method

.method public getUpdateId()J
    .locals 2

    .prologue
    .line 82
    iget-wide v0, p0, Lcom/samsung/scloud/data/Update;->updateId:J

    return-wide v0
.end method

.method public getUpdated()J
    .locals 2

    .prologue
    .line 106
    iget-wide v0, p0, Lcom/samsung/scloud/data/Update;->updated:J

    return-wide v0
.end method

.method public getUpdatedType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/samsung/scloud/data/Update;->updatedType:Ljava/lang/String;

    return-object v0
.end method

.method public getUserEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/samsung/scloud/data/Update;->userEmail:Ljava/lang/String;

    return-object v0
.end method

.method public getUserId()J
    .locals 2

    .prologue
    .line 90
    iget-wide v0, p0, Lcom/samsung/scloud/data/Update;->userId:J

    return-wide v0
.end method

.method public getUserName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/samsung/scloud/data/Update;->userName:Ljava/lang/String;

    return-object v0
.end method

.method public isCollabAccess()Z
    .locals 1

    .prologue
    .line 162
    iget-boolean v0, p0, Lcom/samsung/scloud/data/Update;->isCollabAccess:Z

    return v0
.end method

.method public isShared()Z
    .locals 1

    .prologue
    .line 130
    iget-boolean v0, p0, Lcom/samsung/scloud/data/Update;->isShared:Z

    return v0
.end method

.method public setCollabAccess(Z)V
    .locals 0
    .param p1, "isCollabAccess"    # Z

    .prologue
    .line 166
    iput-boolean p1, p0, Lcom/samsung/scloud/data/Update;->isCollabAccess:Z

    .line 167
    return-void
.end method

.method public setFiles(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Update$UpdateEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 174
    .local p1, "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/Update$UpdateEntry;>;"
    iput-object p1, p0, Lcom/samsung/scloud/data/Update;->files:Ljava/util/ArrayList;

    .line 175
    return-void
.end method

.method public setFolderId(J)V
    .locals 1
    .param p1, "folderId"    # J

    .prologue
    .line 126
    iput-wide p1, p0, Lcom/samsung/scloud/data/Update;->folderId:J

    .line 127
    return-void
.end method

.method public setFolderName(Ljava/lang/String;)V
    .locals 0
    .param p1, "folderName"    # Ljava/lang/String;

    .prologue
    .line 198
    iput-object p1, p0, Lcom/samsung/scloud/data/Update;->folderName:Ljava/lang/String;

    .line 199
    return-void
.end method

.method public setFolderPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 158
    iput-object p1, p0, Lcom/samsung/scloud/data/Update;->folderPath:Ljava/lang/String;

    .line 159
    return-void
.end method

.method public setFolders(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Update$UpdateEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 182
    .local p1, "folders":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/Update$UpdateEntry;>;"
    iput-object p1, p0, Lcom/samsung/scloud/data/Update;->folders:Ljava/util/ArrayList;

    .line 183
    return-void
.end method

.method public setOwnerId(J)V
    .locals 1
    .param p1, "ownerId"    # J

    .prologue
    .line 150
    iput-wide p1, p0, Lcom/samsung/scloud/data/Update;->ownerId:J

    .line 151
    return-void
.end method

.method public setShared(Z)V
    .locals 0
    .param p1, "isShared"    # Z

    .prologue
    .line 134
    iput-boolean p1, p0, Lcom/samsung/scloud/data/Update;->isShared:Z

    .line 135
    return-void
.end method

.method public setSharedName(Ljava/lang/String;)V
    .locals 0
    .param p1, "sharedName"    # Ljava/lang/String;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/samsung/scloud/data/Update;->sharedName:Ljava/lang/String;

    .line 143
    return-void
.end method

.method public setUpdateId(J)V
    .locals 1
    .param p1, "updateId"    # J

    .prologue
    .line 86
    iput-wide p1, p0, Lcom/samsung/scloud/data/Update;->updateId:J

    .line 87
    return-void
.end method

.method public setUpdated(J)V
    .locals 1
    .param p1, "updated"    # J

    .prologue
    .line 110
    iput-wide p1, p0, Lcom/samsung/scloud/data/Update;->updated:J

    .line 111
    return-void
.end method

.method public setUpdatedType(Ljava/lang/String;)V
    .locals 0
    .param p1, "updatedType"    # Ljava/lang/String;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/samsung/scloud/data/Update;->updatedType:Ljava/lang/String;

    .line 119
    return-void
.end method

.method public setUserEmail(Ljava/lang/String;)V
    .locals 0
    .param p1, "userEmail"    # Ljava/lang/String;

    .prologue
    .line 190
    iput-object p1, p0, Lcom/samsung/scloud/data/Update;->userEmail:Ljava/lang/String;

    .line 191
    return-void
.end method

.method public setUserId(J)V
    .locals 1
    .param p1, "userId"    # J

    .prologue
    .line 94
    iput-wide p1, p0, Lcom/samsung/scloud/data/Update;->userId:J

    .line 95
    return-void
.end method

.method public setUserName(Ljava/lang/String;)V
    .locals 0
    .param p1, "userName"    # Ljava/lang/String;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/samsung/scloud/data/Update;->userName:Ljava/lang/String;

    .line 103
    return-void
.end method
