.class public Lcom/samsung/scloud/data/VideoMetaData;
.super Ljava/lang/Object;
.source "VideoMetaData.java"


# instance fields
.field protected albums:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/scloud/data/AlbumData;",
            ">;"
        }
    .end annotation
.end field

.field private bitrate:J

.field private duration:J

.field private height:I

.field private width:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v0, -0x1

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput v0, p0, Lcom/samsung/scloud/data/VideoMetaData;->width:I

    .line 13
    iput v0, p0, Lcom/samsung/scloud/data/VideoMetaData;->height:I

    .line 17
    iput-wide v2, p0, Lcom/samsung/scloud/data/VideoMetaData;->duration:J

    .line 22
    iput-wide v2, p0, Lcom/samsung/scloud/data/VideoMetaData;->bitrate:J

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/scloud/data/VideoMetaData;->albums:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getAlbums()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/scloud/data/AlbumData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/scloud/data/VideoMetaData;->albums:Ljava/util/List;

    return-object v0
.end method

.method public getBitrate()J
    .locals 2

    .prologue
    .line 50
    iget-wide v0, p0, Lcom/samsung/scloud/data/VideoMetaData;->bitrate:J

    return-wide v0
.end method

.method public getDuration()J
    .locals 2

    .prologue
    .line 44
    iget-wide v0, p0, Lcom/samsung/scloud/data/VideoMetaData;->duration:J

    return-wide v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/samsung/scloud/data/VideoMetaData;->height:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lcom/samsung/scloud/data/VideoMetaData;->width:I

    return v0
.end method

.method public setAlbums(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/scloud/data/AlbumData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 75
    .local p1, "albums":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/scloud/data/AlbumData;>;"
    iput-object p1, p0, Lcom/samsung/scloud/data/VideoMetaData;->albums:Ljava/util/List;

    .line 76
    return-void
.end method

.method public setBitrate(J)V
    .locals 1
    .param p1, "bitrate"    # J

    .prologue
    .line 53
    iput-wide p1, p0, Lcom/samsung/scloud/data/VideoMetaData;->bitrate:J

    .line 54
    return-void
.end method

.method public setDuration(J)V
    .locals 1
    .param p1, "duration"    # J

    .prologue
    .line 47
    iput-wide p1, p0, Lcom/samsung/scloud/data/VideoMetaData;->duration:J

    .line 48
    return-void
.end method

.method public setHeight(I)V
    .locals 0
    .param p1, "height"    # I

    .prologue
    .line 41
    iput p1, p0, Lcom/samsung/scloud/data/VideoMetaData;->height:I

    .line 42
    return-void
.end method

.method public setWidth(I)V
    .locals 0
    .param p1, "width"    # I

    .prologue
    .line 35
    iput p1, p0, Lcom/samsung/scloud/data/VideoMetaData;->width:I

    .line 36
    return-void
.end method
