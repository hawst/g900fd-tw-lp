.class public Lcom/samsung/scloud/data/AudioMetaData;
.super Ljava/lang/Object;
.source "AudioMetaData.java"


# instance fields
.field private albumTitle:Ljava/lang/String;

.field private artistName:Ljava/lang/String;

.field private duration:J

.field private genre:Ljava/lang/String;

.field private trackNumber:J

.field private trackTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v0, 0x0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    iput-object v0, p0, Lcom/samsung/scloud/data/AudioMetaData;->artistName:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lcom/samsung/scloud/data/AudioMetaData;->albumTitle:Ljava/lang/String;

    .line 15
    iput-object v0, p0, Lcom/samsung/scloud/data/AudioMetaData;->trackTitle:Ljava/lang/String;

    .line 19
    iput-wide v2, p0, Lcom/samsung/scloud/data/AudioMetaData;->trackNumber:J

    .line 23
    iput-wide v2, p0, Lcom/samsung/scloud/data/AudioMetaData;->duration:J

    .line 27
    iput-object v0, p0, Lcom/samsung/scloud/data/AudioMetaData;->genre:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAlbumTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/scloud/data/AudioMetaData;->albumTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getArtistName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/samsung/scloud/data/AudioMetaData;->artistName:Ljava/lang/String;

    return-object v0
.end method

.method public getDuration()J
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/samsung/scloud/data/AudioMetaData;->duration:J

    return-wide v0
.end method

.method public getGenre()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/scloud/data/AudioMetaData;->genre:Ljava/lang/String;

    return-object v0
.end method

.method public getTrackNumber()J
    .locals 2

    .prologue
    .line 48
    iget-wide v0, p0, Lcom/samsung/scloud/data/AudioMetaData;->trackNumber:J

    return-wide v0
.end method

.method public getTrackTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/scloud/data/AudioMetaData;->trackTitle:Ljava/lang/String;

    return-object v0
.end method

.method public setAlbumTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "albumTitle"    # Ljava/lang/String;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/samsung/scloud/data/AudioMetaData;->albumTitle:Ljava/lang/String;

    .line 40
    return-void
.end method

.method public setArtistName(Ljava/lang/String;)V
    .locals 0
    .param p1, "artistName"    # Ljava/lang/String;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/samsung/scloud/data/AudioMetaData;->artistName:Ljava/lang/String;

    .line 34
    return-void
.end method

.method public setDuration(J)V
    .locals 1
    .param p1, "duration"    # J

    .prologue
    .line 57
    iput-wide p1, p0, Lcom/samsung/scloud/data/AudioMetaData;->duration:J

    .line 58
    return-void
.end method

.method public setGenre(Ljava/lang/String;)V
    .locals 0
    .param p1, "genre"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/samsung/scloud/data/AudioMetaData;->genre:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public setTrackNumber(J)V
    .locals 1
    .param p1, "trackNumber"    # J

    .prologue
    .line 51
    iput-wide p1, p0, Lcom/samsung/scloud/data/AudioMetaData;->trackNumber:J

    .line 52
    return-void
.end method

.method public setTrackTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "trackTitle"    # Ljava/lang/String;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/samsung/scloud/data/AudioMetaData;->trackTitle:Ljava/lang/String;

    .line 46
    return-void
.end method
