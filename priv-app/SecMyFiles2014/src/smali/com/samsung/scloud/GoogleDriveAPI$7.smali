.class Lcom/samsung/scloud/GoogleDriveAPI$7;
.super Ljava/lang/Object;
.source "GoogleDriveAPI.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/scloud/GoogleDriveAPI;->handleTokenExpire()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/accounts/AccountManagerCallback",
        "<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/scloud/GoogleDriveAPI;


# direct methods
.method constructor <init>(Lcom/samsung/scloud/GoogleDriveAPI;)V
    .locals 0

    .prologue
    .line 1878
    iput-object p1, p0, Lcom/samsung/scloud/GoogleDriveAPI$7;->this$0:Lcom/samsung/scloud/GoogleDriveAPI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1881
    .local p1, "future":Landroid/accounts/AccountManagerFuture;, "Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 1882
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "intent"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1883
    iget-object v2, p0, Lcom/samsung/scloud/GoogleDriveAPI$7;->this$0:Lcom/samsung/scloud/GoogleDriveAPI;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/samsung/scloud/GoogleDriveAPI;->mIsRetryAuth:Z

    .line 1884
    const-string v2, "SCLOUD_SDK"

    const-string v3, "Account info changed ..."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1890
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/samsung/scloud/GoogleDriveAPI$7;->this$0:Lcom/samsung/scloud/GoogleDriveAPI;

    # getter for: Lcom/samsung/scloud/GoogleDriveAPI;->mLatch:Ljava/util/concurrent/CountDownLatch;
    invoke-static {v2}, Lcom/samsung/scloud/GoogleDriveAPI;->access$000(Lcom/samsung/scloud/GoogleDriveAPI;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 1895
    .end local v0    # "bundle":Landroid/os/Bundle;
    :goto_1
    return-void

    .line 1885
    .restart local v0    # "bundle":Landroid/os/Bundle;
    :cond_1
    const-string v2, "authtoken"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1886
    iget-object v2, p0, Lcom/samsung/scloud/GoogleDriveAPI$7;->this$0:Lcom/samsung/scloud/GoogleDriveAPI;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/samsung/scloud/GoogleDriveAPI;->mIsRetryAuth:Z

    .line 1887
    iget-object v2, p0, Lcom/samsung/scloud/GoogleDriveAPI$7;->this$0:Lcom/samsung/scloud/GoogleDriveAPI;

    const-string v3, "authtoken"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/scloud/GoogleDriveAPI;->setAuthToken(Ljava/lang/String;)V

    .line 1888
    const-string v2, "SCLOUD_SDK"

    const-string v3, "after retry get access token ..."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1891
    .end local v0    # "bundle":Landroid/os/Bundle;
    :catch_0
    move-exception v1

    .line 1892
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "SCLOUD_SDK"

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1893
    iget-object v2, p0, Lcom/samsung/scloud/GoogleDriveAPI$7;->this$0:Lcom/samsung/scloud/GoogleDriveAPI;

    # getter for: Lcom/samsung/scloud/GoogleDriveAPI;->mLatch:Ljava/util/concurrent/CountDownLatch;
    invoke-static {v2}, Lcom/samsung/scloud/GoogleDriveAPI;->access$000(Lcom/samsung/scloud/GoogleDriveAPI;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_1
.end method
