.class Lcom/samsung/scloud/GoogleDriveAPI$5;
.super Ljava/lang/Object;
.source "GoogleDriveAPI.java"

# interfaces
.implements Lcom/google/api/client/googleapis/media/MediaHttpDownloaderProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/scloud/GoogleDriveAPI;->getThumbnail(Ljava/io/File;Ljava/lang/String;IILcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/scloud/GoogleDriveAPI;

.field final synthetic val$localFile:Ljava/io/File;

.field final synthetic val$progressListener:Lcom/samsung/scloud/response/ProgressListener;


# direct methods
.method constructor <init>(Lcom/samsung/scloud/GoogleDriveAPI;Lcom/samsung/scloud/response/ProgressListener;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 915
    iput-object p1, p0, Lcom/samsung/scloud/GoogleDriveAPI$5;->this$0:Lcom/samsung/scloud/GoogleDriveAPI;

    iput-object p2, p0, Lcom/samsung/scloud/GoogleDriveAPI$5;->val$progressListener:Lcom/samsung/scloud/response/ProgressListener;

    iput-object p3, p0, Lcom/samsung/scloud/GoogleDriveAPI$5;->val$localFile:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public progressChanged(Lcom/google/api/client/googleapis/media/MediaHttpDownloader;)V
    .locals 6
    .param p1, "arg0"    # Lcom/google/api/client/googleapis/media/MediaHttpDownloader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 919
    const-string v0, "SCLOUD_SDK"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Progress download:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/api/client/googleapis/media/MediaHttpDownloader;->getProgress()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "bytes downloaded:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/api/client/googleapis/media/MediaHttpDownloader;->getNumBytesDownloaded()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 920
    iget-object v0, p0, Lcom/samsung/scloud/GoogleDriveAPI$5;->val$progressListener:Lcom/samsung/scloud/response/ProgressListener;

    if-eqz v0, :cond_0

    .line 921
    iget-object v0, p0, Lcom/samsung/scloud/GoogleDriveAPI$5;->val$progressListener:Lcom/samsung/scloud/response/ProgressListener;

    iget-object v1, p0, Lcom/samsung/scloud/GoogleDriveAPI$5;->val$localFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {p1}, Lcom/google/api/client/googleapis/media/MediaHttpDownloader;->getProgress()D

    move-result-wide v4

    double-to-long v4, v4

    invoke-interface {v0, v1, v2, v4, v5}, Lcom/samsung/scloud/response/ProgressListener;->onUpdate(Ljava/lang/String;IJ)V

    .line 924
    :cond_0
    return-void
.end method
