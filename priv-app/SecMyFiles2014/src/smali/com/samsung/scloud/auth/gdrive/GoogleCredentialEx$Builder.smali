.class public Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx$Builder;
.super Lcom/google/api/client/googleapis/auth/oauth2/GoogleCredential$Builder;
.source "GoogleCredentialEx.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/google/api/client/googleapis/auth/oauth2/GoogleCredential$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/api/client/auth/oauth2/Credential;
    .locals 1

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx$Builder;->build()Lcom/google/api/client/googleapis/auth/oauth2/GoogleCredential;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/google/api/client/googleapis/auth/oauth2/GoogleCredential;
    .locals 13

    .prologue
    const/4 v8, 0x0

    .line 78
    new-instance v0, Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx;

    invoke-virtual {p0}, Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx$Builder;->getMethod()Lcom/google/api/client/auth/oauth2/Credential$AccessMethod;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx$Builder;->getTransport()Lcom/google/api/client/http/HttpTransport;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx$Builder;->getJsonFactory()Lcom/google/api/client/json/JsonFactory;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx$Builder;->getTokenServerUrl()Lcom/google/api/client/http/GenericUrl;

    move-result-object v4

    if-nez v4, :cond_0

    move-object v4, v8

    :goto_0
    invoke-virtual {p0}, Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx$Builder;->getClientAuthentication()Lcom/google/api/client/http/HttpExecuteInterceptor;

    move-result-object v5

    invoke-virtual {p0}, Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx$Builder;->getRequestInitializer()Lcom/google/api/client/http/HttpRequestInitializer;

    move-result-object v6

    invoke-virtual {p0}, Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx$Builder;->getRefreshListeners()Ljava/util/List;

    move-result-object v7

    invoke-virtual {p0}, Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx$Builder;->getClock()Lcom/google/api/client/util/Clock;

    move-result-object v12

    move-object v9, v8

    move-object v10, v8

    move-object v11, v8

    invoke-direct/range {v0 .. v12}, Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx;-><init>(Lcom/google/api/client/auth/oauth2/Credential$AccessMethod;Lcom/google/api/client/http/HttpTransport;Lcom/google/api/client/json/JsonFactory;Ljava/lang/String;Lcom/google/api/client/http/HttpExecuteInterceptor;Lcom/google/api/client/http/HttpRequestInitializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/security/PrivateKey;Ljava/lang/String;Lcom/google/api/client/util/Clock;)V

    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx$Builder;->getTokenServerUrl()Lcom/google/api/client/http/GenericUrl;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/api/client/http/GenericUrl;->build()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method
