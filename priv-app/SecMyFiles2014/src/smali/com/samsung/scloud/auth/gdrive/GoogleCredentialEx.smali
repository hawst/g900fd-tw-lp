.class public Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx;
.super Lcom/google/api/client/googleapis/auth/oauth2/GoogleCredential;
.source "GoogleCredentialEx.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx$Builder;
    }
.end annotation


# instance fields
.field backOffPolicy:Lcom/google/api/client/http/ExponentialBackOffPolicy;


# direct methods
.method protected constructor <init>(Lcom/google/api/client/auth/oauth2/Credential$AccessMethod;Lcom/google/api/client/http/HttpTransport;Lcom/google/api/client/json/JsonFactory;Ljava/lang/String;Lcom/google/api/client/http/HttpExecuteInterceptor;Lcom/google/api/client/http/HttpRequestInitializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/security/PrivateKey;Ljava/lang/String;Lcom/google/api/client/util/Clock;)V
    .locals 4
    .param p1, "method"    # Lcom/google/api/client/auth/oauth2/Credential$AccessMethod;
    .param p2, "transport"    # Lcom/google/api/client/http/HttpTransport;
    .param p3, "jsonFactory"    # Lcom/google/api/client/json/JsonFactory;
    .param p4, "tokenServerEncodedUrl"    # Ljava/lang/String;
    .param p5, "clientAuthentication"    # Lcom/google/api/client/http/HttpExecuteInterceptor;
    .param p6, "requestInitializer"    # Lcom/google/api/client/http/HttpRequestInitializer;
    .param p8, "serviceAccountId"    # Ljava/lang/String;
    .param p9, "serviceAccountScopes"    # Ljava/lang/String;
    .param p10, "serviceAccountPrivateKey"    # Ljava/security/PrivateKey;
    .param p11, "serviceAccountUser"    # Ljava/lang/String;
    .param p12, "clock"    # Lcom/google/api/client/util/Clock;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/client/auth/oauth2/Credential$AccessMethod;",
            "Lcom/google/api/client/http/HttpTransport;",
            "Lcom/google/api/client/json/JsonFactory;",
            "Ljava/lang/String;",
            "Lcom/google/api/client/http/HttpExecuteInterceptor;",
            "Lcom/google/api/client/http/HttpRequestInitializer;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/client/auth/oauth2/CredentialRefreshListener;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/security/PrivateKey;",
            "Ljava/lang/String;",
            "Lcom/google/api/client/util/Clock;",
            ")V"
        }
    .end annotation

    .prologue
    .line 44
    .local p7, "refreshListeners":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/client/auth/oauth2/CredentialRefreshListener;>;"
    invoke-direct/range {p0 .. p12}, Lcom/google/api/client/googleapis/auth/oauth2/GoogleCredential;-><init>(Lcom/google/api/client/auth/oauth2/Credential$AccessMethod;Lcom/google/api/client/http/HttpTransport;Lcom/google/api/client/json/JsonFactory;Ljava/lang/String;Lcom/google/api/client/http/HttpExecuteInterceptor;Lcom/google/api/client/http/HttpRequestInitializer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/security/PrivateKey;Ljava/lang/String;Lcom/google/api/client/util/Clock;)V

    .line 28
    invoke-static {}, Lcom/google/api/client/http/ExponentialBackOffPolicy;->builder()Lcom/google/api/client/http/ExponentialBackOffPolicy$Builder;

    move-result-object v0

    const/16 v1, 0x7d0

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/ExponentialBackOffPolicy$Builder;->setInitialIntervalMillis(I)Lcom/google/api/client/http/ExponentialBackOffPolicy$Builder;

    move-result-object v0

    const v1, 0x2bf20

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/ExponentialBackOffPolicy$Builder;->setMaxElapsedTimeMillis(I)Lcom/google/api/client/http/ExponentialBackOffPolicy$Builder;

    move-result-object v0

    const/16 v1, 0x1b58

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/ExponentialBackOffPolicy$Builder;->setMaxIntervalMillis(I)Lcom/google/api/client/http/ExponentialBackOffPolicy$Builder;

    move-result-object v0

    const-wide/high16 v2, 0x3ff8000000000000L    # 1.5

    invoke-virtual {v0, v2, v3}, Lcom/google/api/client/http/ExponentialBackOffPolicy$Builder;->setMultiplier(D)Lcom/google/api/client/http/ExponentialBackOffPolicy$Builder;

    move-result-object v0

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    invoke-virtual {v0, v2, v3}, Lcom/google/api/client/http/ExponentialBackOffPolicy$Builder;->setRandomizationFactor(D)Lcom/google/api/client/http/ExponentialBackOffPolicy$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/client/http/ExponentialBackOffPolicy$Builder;->build()Lcom/google/api/client/http/ExponentialBackOffPolicy;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx;->backOffPolicy:Lcom/google/api/client/http/ExponentialBackOffPolicy;

    .line 48
    return-void
.end method


# virtual methods
.method public handleResponse(Lcom/google/api/client/http/HttpRequest;Lcom/google/api/client/http/HttpResponse;Z)Z
    .locals 4
    .param p1, "request"    # Lcom/google/api/client/http/HttpRequest;
    .param p2, "response"    # Lcom/google/api/client/http/HttpResponse;
    .param p3, "supportsRetry"    # Z

    .prologue
    .line 62
    if-nez p2, :cond_0

    .line 63
    const-string v1, "GoogleDrive"

    const-string v2, "[GoogleCredentialEx]handleResponse(UnsuccessfulResponseHandler) response=null"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    const/4 v0, 0x0

    .line 71
    :goto_0
    return v0

    .line 67
    :cond_0
    const-string v1, "GoogleDrive"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[GoogleCredentialEx]handleResponse(UnsuccessfulResponseHandler) response code="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/api/client/http/HttpResponse;->getStatusCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " headers="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/api/client/http/HttpResponse;->getHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/api/client/http/HttpHeaders;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    invoke-super {p0, p1, p2, p3}, Lcom/google/api/client/googleapis/auth/oauth2/GoogleCredential;->handleResponse(Lcom/google/api/client/http/HttpRequest;Lcom/google/api/client/http/HttpResponse;Z)Z

    move-result v0

    .line 71
    .local v0, "ret":Z
    goto :goto_0
.end method

.method public initialize(Lcom/google/api/client/http/HttpRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/google/api/client/http/HttpRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v1, 0xea60

    .line 52
    invoke-super {p0, p1}, Lcom/google/api/client/googleapis/auth/oauth2/GoogleCredential;->initialize(Lcom/google/api/client/http/HttpRequest;)V

    .line 53
    iget-object v0, p0, Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx;->backOffPolicy:Lcom/google/api/client/http/ExponentialBackOffPolicy;

    invoke-virtual {p1, v0}, Lcom/google/api/client/http/HttpRequest;->setBackOffPolicy(Lcom/google/api/client/http/BackOffPolicy;)Lcom/google/api/client/http/HttpRequest;

    .line 54
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lcom/google/api/client/http/HttpRequest;->setNumberOfRetries(I)Lcom/google/api/client/http/HttpRequest;

    .line 55
    invoke-virtual {p1, v1}, Lcom/google/api/client/http/HttpRequest;->setConnectTimeout(I)Lcom/google/api/client/http/HttpRequest;

    .line 56
    invoke-virtual {p1, v1}, Lcom/google/api/client/http/HttpRequest;->setReadTimeout(I)Lcom/google/api/client/http/HttpRequest;

    .line 57
    const-string v0, "GoogleDrive"

    const-string v1, "[GoogleCredentialEx]Request initialized"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    return-void
.end method
