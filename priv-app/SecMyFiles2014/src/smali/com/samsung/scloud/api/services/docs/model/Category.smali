.class public Lcom/samsung/scloud/api/services/docs/model/Category;
.super Ljava/lang/Object;
.source "Category.java"


# instance fields
.field public label:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "@label"
    .end annotation
.end field

.field public scheme:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "@scheme"
    .end annotation
.end field

.field public term:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "@term"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static find(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "rel"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/scloud/api/services/docs/model/Link;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 20
    .local p0, "links":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/scloud/api/services/docs/model/Link;>;"
    if-eqz p0, :cond_1

    .line 21
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/scloud/api/services/docs/model/Link;

    .line 22
    .local v1, "link":Lcom/samsung/scloud/api/services/docs/model/Link;
    iget-object v2, v1, Lcom/samsung/scloud/api/services/docs/model/Link;->rel:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 23
    iget-object v2, v1, Lcom/samsung/scloud/api/services/docs/model/Link;->href:Ljava/lang/String;

    .line 27
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "link":Lcom/samsung/scloud/api/services/docs/model/Link;
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static newKind(Ljava/lang/String;)Lcom/samsung/scloud/api/services/docs/model/Category;
    .locals 3
    .param p0, "kind"    # Ljava/lang/String;

    .prologue
    .line 30
    new-instance v0, Lcom/samsung/scloud/api/services/docs/model/Category;

    invoke-direct {v0}, Lcom/samsung/scloud/api/services/docs/model/Category;-><init>()V

    .line 31
    .local v0, "category":Lcom/samsung/scloud/api/services/docs/model/Category;
    const-string v1, "http://schemas.google.com/g/2005#kind"

    iput-object v1, v0, Lcom/samsung/scloud/api/services/docs/model/Category;->scheme:Ljava/lang/String;

    .line 32
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://schemas.google.com/docs/2007#"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/scloud/api/services/docs/model/Category;->term:Ljava/lang/String;

    .line 33
    return-object v0
.end method
