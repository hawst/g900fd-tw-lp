.class public Lcom/samsung/scloud/api/services/docs/model/Feed;
.super Ljava/lang/Object;
.source "Feed.java"


# instance fields
.field public author:Lcom/samsung/scloud/api/services/docs/model/Author;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "author"
    .end annotation
.end field

.field public etag:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "gd:etag"
    .end annotation
.end field

.field public largestChangestamp:Lcom/samsung/scloud/api/services/docs/model/LargestChangestamp;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "docs:largestChangestamp"
    .end annotation
.end field

.field public links:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "link"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/scloud/api/services/docs/model/Link;",
            ">;"
        }
    .end annotation
.end field

.field public quotaBytesTotal:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "gd:quotaBytesTotal"
    .end annotation
.end field

.field public quotaBytesUsed:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "gd:quotaBytesUsed"
    .end annotation
.end field

.field public quotaBytesUsedInTrash:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "docs:quotaBytesUsedInTrash"
    .end annotation
.end field

.field public startIndex:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "openSearch:startIndex"
    .end annotation
.end field

.field public title:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public totalResults:I
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "openSearch:totalResults"
    .end annotation
.end field

.field public updated:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getNextLink()Ljava/lang/String;
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/samsung/scloud/api/services/docs/model/Feed;->links:Ljava/util/List;

    const-string v1, "next"

    invoke-static {v0, v1}, Lcom/samsung/scloud/api/services/docs/model/Link;->find(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPostLink()Ljava/lang/String;
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/scloud/api/services/docs/model/Feed;->links:Ljava/util/List;

    const-string v1, "http://schemas.google.com/docs/2007#alt-post"

    invoke-static {v0, v1}, Lcom/samsung/scloud/api/services/docs/model/Link;->find(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getResumableUploadLink()Ljava/lang/String;
    .locals 4

    .prologue
    .line 64
    iget-object v1, p0, Lcom/samsung/scloud/api/services/docs/model/Feed;->links:Ljava/util/List;

    const-string v2, "http://schemas.google.com/g/2005#resumable-create-media"

    invoke-static {v1, v2}, Lcom/samsung/scloud/api/services/docs/model/Link;->find(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 65
    .local v0, "link":Ljava/lang/String;
    const-string v1, "GoogleDrive"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Resumable upload link:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    return-object v0
.end method
