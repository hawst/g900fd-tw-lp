.class public Lcom/samsung/scloud/api/services/docs/DocsClient;
.super Lcom/samsung/scloud/api/services/docs/GDataXmlClient;
.source "DocsClient.java"


# static fields
.field static final DICTIONARY:Lcom/google/api/client/xml/XmlNamespaceDictionary;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 36
    new-instance v0, Lcom/google/api/client/xml/XmlNamespaceDictionary;

    invoke-direct {v0}, Lcom/google/api/client/xml/XmlNamespaceDictionary;-><init>()V

    const-string v1, ""

    const-string v2, "http://www.w3.org/2005/Atom"

    invoke-virtual {v0, v1, v2}, Lcom/google/api/client/xml/XmlNamespaceDictionary;->set(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/client/xml/XmlNamespaceDictionary;

    move-result-object v0

    const-string v1, "app"

    const-string v2, "http://www.w3.org/2007/app"

    invoke-virtual {v0, v1, v2}, Lcom/google/api/client/xml/XmlNamespaceDictionary;->set(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/client/xml/XmlNamespaceDictionary;

    move-result-object v0

    const-string v1, "batch"

    const-string v2, "http://schemas.google.com/gdata/batch"

    invoke-virtual {v0, v1, v2}, Lcom/google/api/client/xml/XmlNamespaceDictionary;->set(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/client/xml/XmlNamespaceDictionary;

    move-result-object v0

    const-string v1, "docs"

    const-string v2, "http://schemas.google.com/docs/2007"

    invoke-virtual {v0, v1, v2}, Lcom/google/api/client/xml/XmlNamespaceDictionary;->set(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/client/xml/XmlNamespaceDictionary;

    move-result-object v0

    const-string v1, "gAcl"

    const-string v2, "http://schemas.google.com/acl/2007"

    invoke-virtual {v0, v1, v2}, Lcom/google/api/client/xml/XmlNamespaceDictionary;->set(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/client/xml/XmlNamespaceDictionary;

    move-result-object v0

    const-string v1, "gd"

    const-string v2, "http://schemas.google.com/g/2005"

    invoke-virtual {v0, v1, v2}, Lcom/google/api/client/xml/XmlNamespaceDictionary;->set(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/client/xml/XmlNamespaceDictionary;

    move-result-object v0

    const-string v1, "openSearch"

    const-string v2, "http://a9.com/-/spec/opensearch/1.1/"

    invoke-virtual {v0, v1, v2}, Lcom/google/api/client/xml/XmlNamespaceDictionary;->set(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/client/xml/XmlNamespaceDictionary;

    move-result-object v0

    const-string v1, "xml"

    const-string v2, "http://www.w3.org/XML/1998/namespace"

    invoke-virtual {v0, v1, v2}, Lcom/google/api/client/xml/XmlNamespaceDictionary;->set(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/client/xml/XmlNamespaceDictionary;

    move-result-object v0

    sput-object v0, Lcom/samsung/scloud/api/services/docs/DocsClient;->DICTIONARY:Lcom/google/api/client/xml/XmlNamespaceDictionary;

    return-void
.end method

.method public constructor <init>(Lcom/google/api/client/http/HttpRequestFactory;)V
    .locals 2
    .param p1, "requestFactory"    # Lcom/google/api/client/http/HttpRequestFactory;

    .prologue
    .line 48
    const-string v0, "3"

    sget-object v1, Lcom/samsung/scloud/api/services/docs/DocsClient;->DICTIONARY:Lcom/google/api/client/xml/XmlNamespaceDictionary;

    invoke-direct {p0, v0, p1, v1}, Lcom/samsung/scloud/api/services/docs/GDataXmlClient;-><init>(Ljava/lang/String;Lcom/google/api/client/http/HttpRequestFactory;Lcom/google/api/client/xml/XmlNamespaceDictionary;)V

    .line 49
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/scloud/api/services/docs/DocsClient;->setPartialResponse(Z)V

    .line 50
    return-void
.end method


# virtual methods
.method public executeCopy(Lcom/samsung/scloud/api/services/docs/DocsUrl;Lcom/samsung/scloud/api/services/docs/model/DocumentListEntry;ZLjava/lang/String;)Lcom/samsung/scloud/api/services/docs/model/DocumentListEntry;
    .locals 1
    .param p1, "url"    # Lcom/samsung/scloud/api/services/docs/DocsUrl;
    .param p2, "entry"    # Lcom/samsung/scloud/api/services/docs/model/DocumentListEntry;
    .param p3, "overwrite"    # Z
    .param p4, "eTag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/samsung/scloud/api/services/docs/DocsClient;->executePost(Lcom/samsung/scloud/api/services/docs/DocsUrl;Ljava/lang/Object;ZLjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/scloud/api/services/docs/model/DocumentListEntry;

    return-object v0
.end method

.method public executeDeleteRequest(Lcom/samsung/scloud/api/services/docs/DocsUrl;Ljava/lang/String;)Z
    .locals 1
    .param p1, "deleteUrl"    # Lcom/samsung/scloud/api/services/docs/DocsUrl;
    .param p2, "etag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 81
    invoke-super {p0, p1, p2}, Lcom/samsung/scloud/api/services/docs/GDataXmlClient;->executeDelete(Lcom/google/api/client/googleapis/GoogleUrl;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method executeGet(Lcom/samsung/scloud/api/services/docs/DocsUrl;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .param p1, "url"    # Lcom/samsung/scloud/api/services/docs/DocsUrl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/samsung/scloud/api/services/docs/DocsUrl;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    .local p2, "parseAsType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-super {p0, p1, p2}, Lcom/samsung/scloud/api/services/docs/GDataXmlClient;->executeGet(Lcom/google/api/client/googleapis/GoogleUrl;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public executeGetDocumentListEntry(Lcom/samsung/scloud/api/services/docs/DocsUrl;)Lcom/samsung/scloud/api/services/docs/model/DocumentListEntry;
    .locals 1
    .param p1, "docsUrl"    # Lcom/samsung/scloud/api/services/docs/DocsUrl;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    const-class v0, Lcom/samsung/scloud/api/services/docs/model/DocumentListEntry;

    invoke-virtual {p0, p1, v0}, Lcom/samsung/scloud/api/services/docs/DocsClient;->executeGet(Lcom/samsung/scloud/api/services/docs/DocsUrl;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/scloud/api/services/docs/model/DocumentListEntry;

    return-object v0
.end method

.method public executeGetDocumentListFeed(Lcom/samsung/scloud/api/services/docs/DocsUrl;)Lcom/samsung/scloud/api/services/docs/model/DocumentListFeed;
    .locals 1
    .param p1, "docsUrl"    # Lcom/samsung/scloud/api/services/docs/DocsUrl;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    const-class v0, Lcom/samsung/scloud/api/services/docs/model/DocumentListFeed;

    invoke-virtual {p0, p1, v0}, Lcom/samsung/scloud/api/services/docs/DocsClient;->executeGet(Lcom/samsung/scloud/api/services/docs/DocsUrl;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/scloud/api/services/docs/model/DocumentListFeed;

    return-object v0
.end method

.method public executeInsert(Lcom/samsung/scloud/api/services/docs/DocsUrl;Lcom/samsung/scloud/api/services/docs/model/Entry;)Lcom/samsung/scloud/api/services/docs/model/Entry;
    .locals 1
    .param p1, "url"    # Lcom/samsung/scloud/api/services/docs/DocsUrl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/samsung/scloud/api/services/docs/model/Entry;",
            ">(",
            "Lcom/samsung/scloud/api/services/docs/DocsUrl;",
            "TT;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 76
    .local p2, "entry":Lcom/samsung/scloud/api/services/docs/model/Entry;, "TT;"
    invoke-virtual {p0, p1, p2}, Lcom/samsung/scloud/api/services/docs/DocsClient;->executePost(Lcom/samsung/scloud/api/services/docs/DocsUrl;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/scloud/api/services/docs/model/Entry;

    return-object v0
.end method

.method public executeMove(Lcom/samsung/scloud/api/services/docs/DocsUrl;Lcom/samsung/scloud/api/services/docs/DocsUrl;Lcom/samsung/scloud/api/services/docs/model/DocumentListEntry;Ljava/lang/String;)Lcom/samsung/scloud/api/services/docs/model/DocumentListEntry;
    .locals 1
    .param p1, "insertUrl"    # Lcom/samsung/scloud/api/services/docs/DocsUrl;
    .param p2, "deleteUrl"    # Lcom/samsung/scloud/api/services/docs/DocsUrl;
    .param p3, "insertEntry"    # Lcom/samsung/scloud/api/services/docs/model/DocumentListEntry;
    .param p4, "eTag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/samsung/scloud/api/services/docs/DocsClient;->executePostDelete(Lcom/samsung/scloud/api/services/docs/DocsUrl;Lcom/samsung/scloud/api/services/docs/DocsUrl;Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/scloud/api/services/docs/model/DocumentListEntry;

    return-object v0
.end method

.method executePost(Lcom/samsung/scloud/api/services/docs/DocsUrl;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "url"    # Lcom/samsung/scloud/api/services/docs/DocsUrl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/samsung/scloud/api/services/docs/DocsUrl;",
            "TT;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    .local p2, "content":Ljava/lang/Object;, "TT;"
    instance-of v0, p2, Lcom/samsung/scloud/api/services/docs/model/Feed;

    invoke-super {p0, p1, v0, p2}, Lcom/samsung/scloud/api/services/docs/GDataXmlClient;->executePost(Lcom/google/api/client/googleapis/GoogleUrl;ZLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method executePost(Lcom/samsung/scloud/api/services/docs/DocsUrl;Ljava/lang/Object;ZLjava/lang/String;)Ljava/lang/Object;
    .locals 6
    .param p1, "url"    # Lcom/samsung/scloud/api/services/docs/DocsUrl;
    .param p3, "overwrite"    # Z
    .param p4, "eTag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/samsung/scloud/api/services/docs/DocsUrl;",
            "TT;Z",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    .local p2, "content":Ljava/lang/Object;, "TT;"
    instance-of v2, p2, Lcom/samsung/scloud/api/services/docs/model/Feed;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-super/range {v0 .. v5}, Lcom/samsung/scloud/api/services/docs/GDataXmlClient;->executePost(Lcom/google/api/client/googleapis/GoogleUrl;ZLjava/lang/Object;ZLjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method executePostDelete(Lcom/samsung/scloud/api/services/docs/DocsUrl;Lcom/samsung/scloud/api/services/docs/DocsUrl;Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    .locals 6
    .param p1, "insertUrl"    # Lcom/samsung/scloud/api/services/docs/DocsUrl;
    .param p2, "deleteUrl"    # Lcom/samsung/scloud/api/services/docs/DocsUrl;
    .param p4, "eTag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/samsung/scloud/api/services/docs/DocsUrl;",
            "Lcom/samsung/scloud/api/services/docs/DocsUrl;",
            "TT;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    .local p3, "insertEntry":Ljava/lang/Object;, "TT;"
    instance-of v3, p3, Lcom/samsung/scloud/api/services/docs/model/Feed;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-super/range {v0 .. v5}, Lcom/samsung/scloud/api/services/docs/GDataXmlClient;->executePostDelete(Lcom/samsung/scloud/api/services/docs/DocsUrl;Lcom/samsung/scloud/api/services/docs/DocsUrl;ZLjava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method executePut(Lcom/samsung/scloud/api/services/docs/DocsUrl;Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1, "url"    # Lcom/samsung/scloud/api/services/docs/DocsUrl;
    .param p3, "eTag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/samsung/scloud/api/services/docs/DocsUrl;",
            "TT;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 98
    .local p2, "updatedEntry":Ljava/lang/Object;, "TT;"
    instance-of v0, p2, Lcom/samsung/scloud/api/services/docs/model/Feed;

    invoke-super {p0, p1, p2, v0, p3}, Lcom/samsung/scloud/api/services/docs/GDataXmlClient;->executePut(Lcom/samsung/scloud/api/services/docs/DocsUrl;Ljava/lang/Object;ZLjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public executePutRequest(Lcom/samsung/scloud/api/services/docs/DocsUrl;Lcom/samsung/scloud/api/services/docs/model/DocumentListEntry;Ljava/lang/String;)Lcom/samsung/scloud/api/services/docs/model/DocumentListEntry;
    .locals 1
    .param p1, "url"    # Lcom/samsung/scloud/api/services/docs/DocsUrl;
    .param p2, "original"    # Lcom/samsung/scloud/api/services/docs/model/DocumentListEntry;
    .param p3, "eTag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/scloud/api/services/docs/DocsClient;->executePut(Lcom/samsung/scloud/api/services/docs/DocsUrl;Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/scloud/api/services/docs/model/DocumentListEntry;

    return-object v0
.end method
