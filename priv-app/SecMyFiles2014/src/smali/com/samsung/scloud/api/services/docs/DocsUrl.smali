.class public Lcom/samsung/scloud/api/services/docs/DocsUrl;
.super Lcom/google/api/client/googleapis/GoogleUrl;
.source "DocsUrl.java"


# static fields
.field private static final PRETTY_PRINT:Z = true

.field public static final ROOT_URL:Ljava/lang/String; = "https://docs.google.com/feeds"

.field public static final URL_FULL:Ljava/lang/String; = "https://docs.google.com/feeds/default/private/full"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/google/api/client/googleapis/GoogleUrl;-><init>(Ljava/lang/String;)V

    .line 33
    return-void
.end method

.method public static forChangesInfo(JJ)Lcom/samsung/scloud/api/services/docs/DocsUrl;
    .locals 6
    .param p0, "startIndex"    # J
    .param p2, "maxResults"    # J

    .prologue
    .line 116
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "https://docs.google.com/feeds/default/private/changes?start-index="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&max-results="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 117
    .local v0, "ChangesInfoUrl":Ljava/lang/String;
    new-instance v1, Lcom/samsung/scloud/api/services/docs/DocsUrl;

    invoke-direct {v1, v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;-><init>(Ljava/lang/String;)V

    .line 118
    .local v1, "Url":Lcom/samsung/scloud/api/services/docs/DocsUrl;
    const-string v2, "GoogleDrive"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SEarch URL:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    return-object v1
.end method

.method private static forDefault()Lcom/samsung/scloud/api/services/docs/DocsUrl;
    .locals 3

    .prologue
    .line 40
    invoke-static {}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->forRoot()Lcom/samsung/scloud/api/services/docs/DocsUrl;

    move-result-object v0

    .line 41
    .local v0, "result":Lcom/samsung/scloud/api/services/docs/DocsUrl;
    invoke-virtual {v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->getPathParts()Ljava/util/List;

    move-result-object v1

    const-string v2, "default"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42
    return-object v0
.end method

.method public static forDefaultPrivateFull()Lcom/samsung/scloud/api/services/docs/DocsUrl;
    .locals 3

    .prologue
    .line 46
    invoke-static {}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->forDefault()Lcom/samsung/scloud/api/services/docs/DocsUrl;

    move-result-object v0

    .line 47
    .local v0, "result":Lcom/samsung/scloud/api/services/docs/DocsUrl;
    invoke-virtual {v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->getPathParts()Ljava/util/List;

    move-result-object v1

    const-string v2, "private"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    invoke-virtual {v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->getPathParts()Ljava/util/List;

    move-result-object v1

    const-string v2, "full"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    return-object v0
.end method

.method public static forFileSearch(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/api/services/docs/DocsUrl;
    .locals 5
    .param p0, "fileName"    # Ljava/lang/String;
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 97
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "https://docs.google.com/feeds/default/private/full/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/contents?title="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&title-exact=true"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 98
    .local v0, "sUrl":Ljava/lang/String;
    new-instance v1, Lcom/samsung/scloud/api/services/docs/DocsUrl;

    invoke-direct {v1, v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;-><init>(Ljava/lang/String;)V

    .line 99
    .local v1, "searchUrl":Lcom/samsung/scloud/api/services/docs/DocsUrl;
    const-string v2, "GoogleDrive"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SEarch URL:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    return-object v1
.end method

.method public static forFolderDelete(Ljava/lang/String;)Lcom/samsung/scloud/api/services/docs/DocsUrl;
    .locals 2
    .param p0, "pathId"    # Ljava/lang/String;

    .prologue
    .line 104
    invoke-static {}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->forDefaultPrivateFull()Lcom/samsung/scloud/api/services/docs/DocsUrl;

    move-result-object v0

    .line 105
    .local v0, "deleteUrl":Lcom/samsung/scloud/api/services/docs/DocsUrl;
    invoke-virtual {v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->getPathParts()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    return-object v0
.end method

.method public static forParticularFile(Ljava/lang/String;)Lcom/samsung/scloud/api/services/docs/DocsUrl;
    .locals 4
    .param p0, "resourceId"    # Ljava/lang/String;

    .prologue
    .line 86
    invoke-static {}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->forDefault()Lcom/samsung/scloud/api/services/docs/DocsUrl;

    move-result-object v0

    .line 87
    .local v0, "result":Lcom/samsung/scloud/api/services/docs/DocsUrl;
    invoke-virtual {v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->getPathParts()Ljava/util/List;

    move-result-object v1

    const-string v2, "private"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    invoke-virtual {v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->getPathParts()Ljava/util/List;

    move-result-object v1

    const-string v2, "full"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    invoke-virtual {v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->getPathParts()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    const-string v1, "showroot"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->set(Ljava/lang/String;Ljava/lang/Object;)V

    .line 91
    const-string v1, "GoogleDrive"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Google URL:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->getRawPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "????!?!?\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    return-object v0
.end method

.method public static forParticularFolder(Ljava/lang/String;)Lcom/samsung/scloud/api/services/docs/DocsUrl;
    .locals 4
    .param p0, "resourceId"    # Ljava/lang/String;

    .prologue
    .line 64
    invoke-static {}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->forDefault()Lcom/samsung/scloud/api/services/docs/DocsUrl;

    move-result-object v0

    .line 65
    .local v0, "result":Lcom/samsung/scloud/api/services/docs/DocsUrl;
    invoke-virtual {v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->getPathParts()Ljava/util/List;

    move-result-object v1

    const-string v2, "private"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 66
    invoke-virtual {v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->getPathParts()Ljava/util/List;

    move-result-object v1

    const-string v2, "full"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    invoke-virtual {v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->getPathParts()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    invoke-virtual {v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->getPathParts()Ljava/util/List;

    move-result-object v1

    const-string v2, "contents"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    const-string v1, "showroot"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->set(Ljava/lang/String;Ljava/lang/Object;)V

    .line 70
    const-string v1, "GoogleDrive"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Google URL:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->getRawPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "????!?!?\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    return-object v0
.end method

.method public static forParticularFolderAndFile(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/api/services/docs/DocsUrl;
    .locals 4
    .param p0, "folderId"    # Ljava/lang/String;
    .param p1, "fileId"    # Ljava/lang/String;

    .prologue
    .line 75
    invoke-static {}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->forDefault()Lcom/samsung/scloud/api/services/docs/DocsUrl;

    move-result-object v0

    .line 76
    .local v0, "result":Lcom/samsung/scloud/api/services/docs/DocsUrl;
    invoke-virtual {v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->getPathParts()Ljava/util/List;

    move-result-object v1

    const-string v2, "private"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    invoke-virtual {v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->getPathParts()Ljava/util/List;

    move-result-object v1

    const-string v2, "full"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    invoke-virtual {v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->getPathParts()Ljava/util/List;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "folder:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    invoke-virtual {v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->getPathParts()Ljava/util/List;

    move-result-object v1

    const-string v2, "contents"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 80
    invoke-virtual {v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->getPathParts()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    const-string v1, "GoogleDrive"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Google URL:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    return-object v0
.end method

.method private static forRoot()Lcom/samsung/scloud/api/services/docs/DocsUrl;
    .locals 2

    .prologue
    .line 36
    new-instance v0, Lcom/samsung/scloud/api/services/docs/DocsUrl;

    const-string v1, "https://docs.google.com/feeds"

    invoke-direct {v0, v1}, Lcom/samsung/scloud/api/services/docs/DocsUrl;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static forRootFolder()Lcom/samsung/scloud/api/services/docs/DocsUrl;
    .locals 4

    .prologue
    .line 53
    invoke-static {}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->forDefault()Lcom/samsung/scloud/api/services/docs/DocsUrl;

    move-result-object v0

    .line 54
    .local v0, "result":Lcom/samsung/scloud/api/services/docs/DocsUrl;
    invoke-virtual {v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->getPathParts()Ljava/util/List;

    move-result-object v1

    const-string v2, "private"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    invoke-virtual {v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->getPathParts()Ljava/util/List;

    move-result-object v1

    const-string v2, "full"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    invoke-virtual {v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->getPathParts()Ljava/util/List;

    move-result-object v1

    const-string v2, "folder:root"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    invoke-virtual {v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->getPathParts()Ljava/util/List;

    move-result-object v1

    const-string v2, "contents"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    const-string v1, "showroot"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->set(Ljava/lang/String;Ljava/lang/Object;)V

    .line 59
    const-string v1, "GoogleDrive"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Google URL:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->getRawPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "????!?!?\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    return-object v0
.end method

.method public static forUserInfo()Lcom/samsung/scloud/api/services/docs/DocsUrl;
    .locals 3

    .prologue
    .line 109
    invoke-static {}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->forRoot()Lcom/samsung/scloud/api/services/docs/DocsUrl;

    move-result-object v0

    .line 110
    .local v0, "userInfoUrl":Lcom/samsung/scloud/api/services/docs/DocsUrl;
    invoke-virtual {v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->getPathParts()Ljava/util/List;

    move-result-object v1

    const-string v2, "metadata"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    invoke-virtual {v0}, Lcom/samsung/scloud/api/services/docs/DocsUrl;->getPathParts()Ljava/util/List;

    move-result-object v1

    const-string v2, "default"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    return-object v0
.end method
