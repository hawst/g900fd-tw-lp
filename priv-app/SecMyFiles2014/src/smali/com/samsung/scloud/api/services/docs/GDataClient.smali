.class public abstract Lcom/samsung/scloud/api/services/docs/GDataClient;
.super Ljava/lang/Object;
.source "GDataClient.java"


# instance fields
.field private applicationName:Ljava/lang/String;

.field private final gdataVersion:Ljava/lang/String;

.field final override:Lcom/google/api/client/googleapis/MethodOverride;

.field private prettyPrint:Z

.field private requestFactory:Lcom/google/api/client/http/HttpRequestFactory;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Lcom/google/api/client/http/HttpRequestFactory;)V
    .locals 1
    .param p1, "gdataVersion"    # Ljava/lang/String;
    .param p2, "requestFactory"    # Lcom/google/api/client/http/HttpRequestFactory;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Lcom/google/api/client/googleapis/MethodOverride;

    invoke-direct {v0}, Lcom/google/api/client/googleapis/MethodOverride;-><init>()V

    iput-object v0, p0, Lcom/samsung/scloud/api/services/docs/GDataClient;->override:Lcom/google/api/client/googleapis/MethodOverride;

    .line 42
    iput-object p1, p0, Lcom/samsung/scloud/api/services/docs/GDataClient;->gdataVersion:Ljava/lang/String;

    .line 43
    iput-object p2, p0, Lcom/samsung/scloud/api/services/docs/GDataClient;->requestFactory:Lcom/google/api/client/http/HttpRequestFactory;

    .line 44
    return-void
.end method

.method private setIfMatch(Lcom/google/api/client/http/HttpRequest;Ljava/lang/String;)V
    .locals 1
    .param p1, "request"    # Lcom/google/api/client/http/HttpRequest;
    .param p2, "etag"    # Ljava/lang/String;

    .prologue
    .line 151
    if-eqz p2, :cond_0

    const-string v0, "W/"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 152
    invoke-virtual {p1}, Lcom/google/api/client/http/HttpRequest;->getHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/api/client/http/HttpHeaders;->setIfMatch(Ljava/lang/String;)V

    .line 154
    :cond_0
    return-void
.end method


# virtual methods
.method protected final execute(Lcom/google/api/client/http/HttpRequest;)Lcom/google/api/client/http/HttpResponse;
    .locals 1
    .param p1, "request"    # Lcom/google/api/client/http/HttpRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/samsung/scloud/api/services/docs/GDataClient;->prepare(Lcom/google/api/client/http/HttpRequest;)V

    .line 83
    invoke-virtual {p1}, Lcom/google/api/client/http/HttpRequest;->execute()Lcom/google/api/client/http/HttpResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final executeDelete(Lcom/google/api/client/googleapis/GoogleUrl;Ljava/lang/String;)Z
    .locals 2
    .param p1, "url"    # Lcom/google/api/client/googleapis/GoogleUrl;
    .param p2, "etag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 93
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Lcom/samsung/scloud/api/services/docs/GDataClient;->prepareUrl(Lcom/google/api/client/googleapis/GoogleUrl;Ljava/lang/Class;)V

    .line 94
    invoke-virtual {p0}, Lcom/samsung/scloud/api/services/docs/GDataClient;->getRequestFactory()Lcom/google/api/client/http/HttpRequestFactory;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/api/client/http/HttpRequestFactory;->buildDeleteRequest(Lcom/google/api/client/http/GenericUrl;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    .line 95
    .local v0, "request":Lcom/google/api/client/http/HttpRequest;
    invoke-direct {p0, v0, p2}, Lcom/samsung/scloud/api/services/docs/GDataClient;->setIfMatch(Lcom/google/api/client/http/HttpRequest;Ljava/lang/String;)V

    .line 96
    invoke-virtual {p0, v0}, Lcom/samsung/scloud/api/services/docs/GDataClient;->execute(Lcom/google/api/client/http/HttpRequest;)Lcom/google/api/client/http/HttpResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/api/client/http/HttpResponse;->isSuccessStatusCode()Z

    move-result v1

    return v1
.end method

.method protected final executeGet(Lcom/google/api/client/googleapis/GoogleUrl;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .param p1, "url"    # Lcom/google/api/client/googleapis/GoogleUrl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/api/client/googleapis/GoogleUrl;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 87
    .local p2, "parseAsType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-virtual {p0, p1, p2}, Lcom/samsung/scloud/api/services/docs/GDataClient;->prepareUrl(Lcom/google/api/client/googleapis/GoogleUrl;Ljava/lang/Class;)V

    .line 88
    invoke-virtual {p0}, Lcom/samsung/scloud/api/services/docs/GDataClient;->getRequestFactory()Lcom/google/api/client/http/HttpRequestFactory;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/api/client/http/HttpRequestFactory;->buildGetRequest(Lcom/google/api/client/http/GenericUrl;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    .line 89
    .local v0, "request":Lcom/google/api/client/http/HttpRequest;
    invoke-virtual {p0, v0}, Lcom/samsung/scloud/api/services/docs/GDataClient;->execute(Lcom/google/api/client/http/HttpRequest;)Lcom/google/api/client/http/HttpResponse;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/api/client/http/HttpResponse;->parseAs(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method protected final executePatchRelativeToOriginal(Lcom/google/api/client/googleapis/GoogleUrl;Lcom/google/api/client/http/AbstractHttpContent;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p1, "url"    # Lcom/google/api/client/googleapis/GoogleUrl;
    .param p2, "patchContent"    # Lcom/google/api/client/http/AbstractHttpContent;
    .param p4, "etag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/api/client/googleapis/GoogleUrl;",
            "Lcom/google/api/client/http/AbstractHttpContent;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 134
    .local p3, "parseAsType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-virtual {p0, p1, p3}, Lcom/samsung/scloud/api/services/docs/GDataClient;->prepareUrl(Lcom/google/api/client/googleapis/GoogleUrl;Ljava/lang/Class;)V

    .line 135
    invoke-virtual {p0}, Lcom/samsung/scloud/api/services/docs/GDataClient;->getRequestFactory()Lcom/google/api/client/http/HttpRequestFactory;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/google/api/client/http/HttpRequestFactory;->buildPatchRequest(Lcom/google/api/client/http/GenericUrl;Lcom/google/api/client/http/HttpContent;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    .line 136
    .local v0, "request":Lcom/google/api/client/http/HttpRequest;
    invoke-direct {p0, v0, p4}, Lcom/samsung/scloud/api/services/docs/GDataClient;->setIfMatch(Lcom/google/api/client/http/HttpRequest;Ljava/lang/String;)V

    .line 137
    invoke-virtual {p0, v0}, Lcom/samsung/scloud/api/services/docs/GDataClient;->execute(Lcom/google/api/client/http/HttpRequest;)Lcom/google/api/client/http/HttpResponse;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/google/api/client/http/HttpResponse;->parseAs(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method protected final executePost(Lcom/google/api/client/googleapis/GoogleUrl;Lcom/google/api/client/http/AbstractHttpContent;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .param p1, "url"    # Lcom/google/api/client/googleapis/GoogleUrl;
    .param p2, "content"    # Lcom/google/api/client/http/AbstractHttpContent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/api/client/googleapis/GoogleUrl;",
            "Lcom/google/api/client/http/AbstractHttpContent;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    .local p3, "parseAsType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-virtual {p0, p1, p3}, Lcom/samsung/scloud/api/services/docs/GDataClient;->prepareUrl(Lcom/google/api/client/googleapis/GoogleUrl;Ljava/lang/Class;)V

    .line 102
    invoke-virtual {p0}, Lcom/samsung/scloud/api/services/docs/GDataClient;->getRequestFactory()Lcom/google/api/client/http/HttpRequestFactory;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/google/api/client/http/HttpRequestFactory;->buildPostRequest(Lcom/google/api/client/http/GenericUrl;Lcom/google/api/client/http/HttpContent;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    .line 103
    .local v0, "request":Lcom/google/api/client/http/HttpRequest;
    invoke-virtual {p0, v0}, Lcom/samsung/scloud/api/services/docs/GDataClient;->execute(Lcom/google/api/client/http/HttpRequest;)Lcom/google/api/client/http/HttpResponse;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/google/api/client/http/HttpResponse;->parseAs(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    return-object v1
.end method

.method protected final executePost(Lcom/google/api/client/googleapis/GoogleUrl;Lcom/google/api/client/http/AbstractHttpContent;Ljava/lang/Class;ZLjava/lang/String;)Ljava/lang/Object;
    .locals 3
    .param p1, "url"    # Lcom/google/api/client/googleapis/GoogleUrl;
    .param p2, "content"    # Lcom/google/api/client/http/AbstractHttpContent;
    .param p4, "overwrite"    # Z
    .param p5, "eTag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/api/client/googleapis/GoogleUrl;",
            "Lcom/google/api/client/http/AbstractHttpContent;",
            "Ljava/lang/Class",
            "<TT;>;Z",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 108
    .local p3, "parseAsType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-virtual {p0, p1, p3}, Lcom/samsung/scloud/api/services/docs/GDataClient;->prepareUrl(Lcom/google/api/client/googleapis/GoogleUrl;Ljava/lang/Class;)V

    .line 109
    invoke-virtual {p0}, Lcom/samsung/scloud/api/services/docs/GDataClient;->getRequestFactory()Lcom/google/api/client/http/HttpRequestFactory;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Lcom/google/api/client/http/HttpRequestFactory;->buildPostRequest(Lcom/google/api/client/http/GenericUrl;Lcom/google/api/client/http/HttpContent;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    .line 114
    .local v0, "request":Lcom/google/api/client/http/HttpRequest;
    invoke-virtual {p0, v0}, Lcom/samsung/scloud/api/services/docs/GDataClient;->execute(Lcom/google/api/client/http/HttpRequest;)Lcom/google/api/client/http/HttpResponse;

    move-result-object v1

    .line 115
    .local v1, "response":Lcom/google/api/client/http/HttpResponse;
    invoke-virtual {v1, p3}, Lcom/google/api/client/http/HttpResponse;->parseAs(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    return-object v2
.end method

.method protected final executePostDelete(Lcom/samsung/scloud/api/services/docs/DocsUrl;Lcom/samsung/scloud/api/services/docs/DocsUrl;Lcom/google/api/client/http/AbstractHttpContent;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 4
    .param p1, "insertUrl"    # Lcom/samsung/scloud/api/services/docs/DocsUrl;
    .param p2, "deleteUrl"    # Lcom/samsung/scloud/api/services/docs/DocsUrl;
    .param p3, "insertContent"    # Lcom/google/api/client/http/AbstractHttpContent;
    .param p4, "eTag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/samsung/scloud/api/services/docs/DocsUrl;",
            "Lcom/samsung/scloud/api/services/docs/DocsUrl;",
            "Lcom/google/api/client/http/AbstractHttpContent;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 119
    .local p5, "parseAsType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-virtual {p0}, Lcom/samsung/scloud/api/services/docs/GDataClient;->getRequestFactory()Lcom/google/api/client/http/HttpRequestFactory;

    move-result-object v3

    invoke-virtual {v3, p1, p3}, Lcom/google/api/client/http/HttpRequestFactory;->buildPostRequest(Lcom/google/api/client/http/GenericUrl;Lcom/google/api/client/http/HttpContent;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v1

    .line 120
    .local v1, "insertRequest":Lcom/google/api/client/http/HttpRequest;
    invoke-virtual {p0, v1}, Lcom/samsung/scloud/api/services/docs/GDataClient;->execute(Lcom/google/api/client/http/HttpRequest;)Lcom/google/api/client/http/HttpResponse;

    move-result-object v2

    .line 121
    .local v2, "insertResponse":Lcom/google/api/client/http/HttpResponse;
    const/4 v0, 0x0

    .line 122
    .local v0, "deleteStatus":Z
    invoke-virtual {v2}, Lcom/google/api/client/http/HttpResponse;->isSuccessStatusCode()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 123
    const-string v3, "*"

    invoke-virtual {p0, p2, v3}, Lcom/samsung/scloud/api/services/docs/GDataClient;->executeDelete(Lcom/google/api/client/googleapis/GoogleUrl;Ljava/lang/String;)Z

    move-result v0

    .line 125
    :cond_0
    if-eqz v0, :cond_1

    .line 126
    invoke-virtual {v2, p5}, Lcom/google/api/client/http/HttpResponse;->parseAs(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    .line 128
    :goto_0
    return-object v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method protected final executePut(Lcom/google/api/client/googleapis/GoogleUrl;Lcom/google/api/client/http/AbstractHttpContent;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;
    .locals 3
    .param p1, "url"    # Lcom/google/api/client/googleapis/GoogleUrl;
    .param p2, "patchContent"    # Lcom/google/api/client/http/AbstractHttpContent;
    .param p4, "etag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/api/client/googleapis/GoogleUrl;",
            "Lcom/google/api/client/http/AbstractHttpContent;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 143
    .local p3, "parseAsType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-virtual {p0, p1, p3}, Lcom/samsung/scloud/api/services/docs/GDataClient;->prepareUrl(Lcom/google/api/client/googleapis/GoogleUrl;Ljava/lang/Class;)V

    .line 144
    invoke-virtual {p0}, Lcom/samsung/scloud/api/services/docs/GDataClient;->getRequestFactory()Lcom/google/api/client/http/HttpRequestFactory;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Lcom/google/api/client/http/HttpRequestFactory;->buildPutRequest(Lcom/google/api/client/http/GenericUrl;Lcom/google/api/client/http/HttpContent;)Lcom/google/api/client/http/HttpRequest;

    move-result-object v0

    .line 145
    .local v0, "request":Lcom/google/api/client/http/HttpRequest;
    invoke-direct {p0, v0, p4}, Lcom/samsung/scloud/api/services/docs/GDataClient;->setIfMatch(Lcom/google/api/client/http/HttpRequest;Ljava/lang/String;)V

    .line 146
    invoke-virtual {p0, v0}, Lcom/samsung/scloud/api/services/docs/GDataClient;->execute(Lcom/google/api/client/http/HttpRequest;)Lcom/google/api/client/http/HttpResponse;

    move-result-object v1

    .line 147
    .local v1, "response":Lcom/google/api/client/http/HttpResponse;
    invoke-virtual {v1, p3}, Lcom/google/api/client/http/HttpResponse;->parseAs(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    return-object v2
.end method

.method public final getApplicationName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/scloud/api/services/docs/GDataClient;->applicationName:Ljava/lang/String;

    return-object v0
.end method

.method public final getPrettyPrint()Z
    .locals 1

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/samsung/scloud/api/services/docs/GDataClient;->prettyPrint:Z

    return v0
.end method

.method protected final getRequestFactory()Lcom/google/api/client/http/HttpRequestFactory;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/samsung/scloud/api/services/docs/GDataClient;->requestFactory:Lcom/google/api/client/http/HttpRequestFactory;

    return-object v0
.end method

.method protected final getTransport()Lcom/google/api/client/http/HttpTransport;
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/samsung/scloud/api/services/docs/GDataClient;->getRequestFactory()Lcom/google/api/client/http/HttpRequestFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/api/client/http/HttpRequestFactory;->getTransport()Lcom/google/api/client/http/HttpTransport;

    move-result-object v0

    return-object v0
.end method

.method protected prepare(Lcom/google/api/client/http/HttpRequest;)V
    .locals 3
    .param p1, "request"    # Lcom/google/api/client/http/HttpRequest;

    .prologue
    .line 76
    invoke-virtual {p1}, Lcom/google/api/client/http/HttpRequest;->getHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/scloud/api/services/docs/GDataClient;->applicationName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/api/client/http/HttpHeaders;->setUserAgent(Ljava/lang/String;)V

    .line 77
    invoke-virtual {p1}, Lcom/google/api/client/http/HttpRequest;->getHeaders()Lcom/google/api/client/http/HttpHeaders;

    move-result-object v0

    const-string v1, "GData-Version"

    iget-object v2, p0, Lcom/samsung/scloud/api/services/docs/GDataClient;->gdataVersion:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/api/client/http/HttpHeaders;->put(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    iget-object v0, p0, Lcom/samsung/scloud/api/services/docs/GDataClient;->override:Lcom/google/api/client/googleapis/MethodOverride;

    invoke-virtual {v0, p1}, Lcom/google/api/client/googleapis/MethodOverride;->intercept(Lcom/google/api/client/http/HttpRequest;)V

    .line 79
    return-void
.end method

.method protected prepareUrl(Lcom/google/api/client/googleapis/GoogleUrl;Ljava/lang/Class;)V
    .locals 1
    .param p1, "url"    # Lcom/google/api/client/googleapis/GoogleUrl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/client/googleapis/GoogleUrl;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 72
    .local p2, "parseAsType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    iget-boolean v0, p0, Lcom/samsung/scloud/api/services/docs/GDataClient;->prettyPrint:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/api/client/googleapis/GoogleUrl;->setPrettyPrint(Ljava/lang/Boolean;)V

    .line 73
    return-void
.end method

.method public final setApplicationName(Ljava/lang/String;)V
    .locals 0
    .param p1, "applicationName"    # Ljava/lang/String;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/samsung/scloud/api/services/docs/GDataClient;->applicationName:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public final setPrettyPrint(Z)V
    .locals 0
    .param p1, "prettyPrint"    # Z

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/samsung/scloud/api/services/docs/GDataClient;->prettyPrint:Z

    .line 65
    return-void
.end method
