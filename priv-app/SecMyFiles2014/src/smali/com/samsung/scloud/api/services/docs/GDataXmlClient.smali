.class public abstract Lcom/samsung/scloud/api/services/docs/GDataXmlClient;
.super Lcom/samsung/scloud/api/services/docs/GDataClient;
.source "GDataXmlClient.java"


# instance fields
.field private final namespaceDictionary:Lcom/google/api/client/xml/XmlNamespaceDictionary;

.field private partialResponse:Z


# direct methods
.method protected constructor <init>(Ljava/lang/String;Lcom/google/api/client/http/HttpRequestFactory;Lcom/google/api/client/xml/XmlNamespaceDictionary;)V
    .locals 1
    .param p1, "gdataVersion"    # Ljava/lang/String;
    .param p2, "requestFactory"    # Lcom/google/api/client/http/HttpRequestFactory;
    .param p3, "namespaceDictionary"    # Lcom/google/api/client/xml/XmlNamespaceDictionary;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/samsung/scloud/api/services/docs/GDataClient;-><init>(Ljava/lang/String;Lcom/google/api/client/http/HttpRequestFactory;)V

    .line 36
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/scloud/api/services/docs/GDataXmlClient;->partialResponse:Z

    .line 41
    iput-object p3, p0, Lcom/samsung/scloud/api/services/docs/GDataXmlClient;->namespaceDictionary:Lcom/google/api/client/xml/XmlNamespaceDictionary;

    .line 42
    return-void
.end method


# virtual methods
.method protected final executePatchRelativeToOriginal(Lcom/google/api/client/googleapis/GoogleUrl;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    .locals 3
    .param p1, "url"    # Lcom/google/api/client/googleapis/GoogleUrl;
    .param p4, "etag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/api/client/googleapis/GoogleUrl;",
            "TT;TT;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    .local p2, "original":Ljava/lang/Object;, "TT;"
    .local p3, "updated":Ljava/lang/Object;, "TT;"
    new-instance v0, Lcom/google/api/client/googleapis/xml/atom/AtomPatchRelativeToOriginalContent;

    iget-object v2, p0, Lcom/samsung/scloud/api/services/docs/GDataXmlClient;->namespaceDictionary:Lcom/google/api/client/xml/XmlNamespaceDictionary;

    invoke-direct {v0, v2, p2, p3}, Lcom/google/api/client/googleapis/xml/atom/AtomPatchRelativeToOriginalContent;-><init>(Lcom/google/api/client/xml/XmlNamespaceDictionary;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 75
    .local v0, "content":Lcom/google/api/client/googleapis/xml/atom/AtomPatchRelativeToOriginalContent;
    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 76
    .local v1, "parseAsType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-virtual {p0, p1, v0, v1, p4}, Lcom/samsung/scloud/api/services/docs/GDataXmlClient;->executePatchRelativeToOriginal(Lcom/google/api/client/googleapis/GoogleUrl;Lcom/google/api/client/http/AbstractHttpContent;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    return-object v2
.end method

.method protected final executePost(Lcom/google/api/client/googleapis/GoogleUrl;ZLjava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "url"    # Lcom/google/api/client/googleapis/GoogleUrl;
    .param p2, "isFeed"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/api/client/googleapis/GoogleUrl;",
            "ZTT;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    .local p3, "content":Ljava/lang/Object;, "TT;"
    if-eqz p2, :cond_0

    iget-object v2, p0, Lcom/samsung/scloud/api/services/docs/GDataXmlClient;->namespaceDictionary:Lcom/google/api/client/xml/XmlNamespaceDictionary;

    invoke-static {v2, p3}, Lcom/google/api/client/http/xml/atom/AtomContent;->forFeed(Lcom/google/api/client/xml/XmlNamespaceDictionary;Ljava/lang/Object;)Lcom/google/api/client/http/xml/atom/AtomContent;

    move-result-object v0

    .line 83
    .local v0, "atomContent":Lcom/google/api/client/http/xml/atom/AtomContent;
    :goto_0
    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 84
    .local v1, "parseAsType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/scloud/api/services/docs/GDataXmlClient;->executePost(Lcom/google/api/client/googleapis/GoogleUrl;Lcom/google/api/client/http/AbstractHttpContent;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    return-object v2

    .line 80
    .end local v0    # "atomContent":Lcom/google/api/client/http/xml/atom/AtomContent;
    .end local v1    # "parseAsType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    :cond_0
    iget-object v2, p0, Lcom/samsung/scloud/api/services/docs/GDataXmlClient;->namespaceDictionary:Lcom/google/api/client/xml/XmlNamespaceDictionary;

    invoke-static {v2, p3}, Lcom/google/api/client/http/xml/atom/AtomContent;->forEntry(Lcom/google/api/client/xml/XmlNamespaceDictionary;Ljava/lang/Object;)Lcom/google/api/client/http/xml/atom/AtomContent;

    move-result-object v0

    goto :goto_0
.end method

.method protected final executePost(Lcom/google/api/client/googleapis/GoogleUrl;ZLjava/lang/Object;ZLjava/lang/String;)Ljava/lang/Object;
    .locals 6
    .param p1, "url"    # Lcom/google/api/client/googleapis/GoogleUrl;
    .param p2, "isFeed"    # Z
    .param p4, "overwrite"    # Z
    .param p5, "eTag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/api/client/googleapis/GoogleUrl;",
            "ZTT;Z",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 87
    .local p3, "content":Ljava/lang/Object;, "TT;"
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/samsung/scloud/api/services/docs/GDataXmlClient;->namespaceDictionary:Lcom/google/api/client/xml/XmlNamespaceDictionary;

    invoke-static {v0, p3}, Lcom/google/api/client/http/xml/atom/AtomContent;->forFeed(Lcom/google/api/client/xml/XmlNamespaceDictionary;Ljava/lang/Object;)Lcom/google/api/client/http/xml/atom/AtomContent;

    move-result-object v2

    .line 90
    .local v2, "atomContent":Lcom/google/api/client/http/xml/atom/AtomContent;
    :goto_0
    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    .local v3, "parseAsType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    move-object v0, p0

    move-object v1, p1

    move v4, p4

    move-object v5, p5

    .line 91
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/scloud/api/services/docs/GDataXmlClient;->executePost(Lcom/google/api/client/googleapis/GoogleUrl;Lcom/google/api/client/http/AbstractHttpContent;Ljava/lang/Class;ZLjava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 87
    .end local v2    # "atomContent":Lcom/google/api/client/http/xml/atom/AtomContent;
    .end local v3    # "parseAsType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    :cond_0
    iget-object v0, p0, Lcom/samsung/scloud/api/services/docs/GDataXmlClient;->namespaceDictionary:Lcom/google/api/client/xml/XmlNamespaceDictionary;

    invoke-static {v0, p3}, Lcom/google/api/client/http/xml/atom/AtomContent;->forEntry(Lcom/google/api/client/xml/XmlNamespaceDictionary;Ljava/lang/Object;)Lcom/google/api/client/http/xml/atom/AtomContent;

    move-result-object v2

    goto :goto_0
.end method

.method protected final executePostDelete(Lcom/samsung/scloud/api/services/docs/DocsUrl;Lcom/samsung/scloud/api/services/docs/DocsUrl;ZLjava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    .locals 6
    .param p1, "insertUrl"    # Lcom/samsung/scloud/api/services/docs/DocsUrl;
    .param p2, "deleteUrl"    # Lcom/samsung/scloud/api/services/docs/DocsUrl;
    .param p3, "isFeed"    # Z
    .param p5, "eTag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/samsung/scloud/api/services/docs/DocsUrl;",
            "Lcom/samsung/scloud/api/services/docs/DocsUrl;",
            "ZTT;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 94
    .local p4, "insertContent":Ljava/lang/Object;, "TT;"
    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/samsung/scloud/api/services/docs/GDataXmlClient;->namespaceDictionary:Lcom/google/api/client/xml/XmlNamespaceDictionary;

    invoke-static {v0, p4}, Lcom/google/api/client/http/xml/atom/AtomContent;->forFeed(Lcom/google/api/client/xml/XmlNamespaceDictionary;Ljava/lang/Object;)Lcom/google/api/client/http/xml/atom/AtomContent;

    move-result-object v3

    .line 97
    .local v3, "atomInsertContent":Lcom/google/api/client/http/xml/atom/AtomContent;
    :goto_0
    invoke-virtual {p4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    .local v5, "parseAsType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p5

    .line 98
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/scloud/api/services/docs/GDataXmlClient;->executePostDelete(Lcom/samsung/scloud/api/services/docs/DocsUrl;Lcom/samsung/scloud/api/services/docs/DocsUrl;Lcom/google/api/client/http/AbstractHttpContent;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 94
    .end local v3    # "atomInsertContent":Lcom/google/api/client/http/xml/atom/AtomContent;
    .end local v5    # "parseAsType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    :cond_0
    iget-object v0, p0, Lcom/samsung/scloud/api/services/docs/GDataXmlClient;->namespaceDictionary:Lcom/google/api/client/xml/XmlNamespaceDictionary;

    invoke-static {v0, p4}, Lcom/google/api/client/http/xml/atom/AtomContent;->forEntry(Lcom/google/api/client/xml/XmlNamespaceDictionary;Ljava/lang/Object;)Lcom/google/api/client/http/xml/atom/AtomContent;

    move-result-object v3

    goto :goto_0
.end method

.method protected final executePut(Lcom/samsung/scloud/api/services/docs/DocsUrl;Ljava/lang/Object;ZLjava/lang/String;)Ljava/lang/Object;
    .locals 3
    .param p1, "insertUrl"    # Lcom/samsung/scloud/api/services/docs/DocsUrl;
    .param p3, "isFeed"    # Z
    .param p4, "eTag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/samsung/scloud/api/services/docs/DocsUrl;",
            "TT;Z",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 102
    .local p2, "updatedContent":Ljava/lang/Object;, "TT;"
    if-eqz p3, :cond_0

    iget-object v2, p0, Lcom/samsung/scloud/api/services/docs/GDataXmlClient;->namespaceDictionary:Lcom/google/api/client/xml/XmlNamespaceDictionary;

    invoke-static {v2, p2}, Lcom/google/api/client/http/xml/atom/AtomContent;->forFeed(Lcom/google/api/client/xml/XmlNamespaceDictionary;Ljava/lang/Object;)Lcom/google/api/client/http/xml/atom/AtomContent;

    move-result-object v0

    .line 105
    .local v0, "atomUpdateContent":Lcom/google/api/client/http/xml/atom/AtomContent;
    :goto_0
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 106
    .local v1, "parseAsType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-virtual {p0, p1, v0, v1, p4}, Lcom/samsung/scloud/api/services/docs/GDataXmlClient;->executePut(Lcom/google/api/client/googleapis/GoogleUrl;Lcom/google/api/client/http/AbstractHttpContent;Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    return-object v2

    .line 102
    .end local v0    # "atomUpdateContent":Lcom/google/api/client/http/xml/atom/AtomContent;
    .end local v1    # "parseAsType":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    :cond_0
    iget-object v2, p0, Lcom/samsung/scloud/api/services/docs/GDataXmlClient;->namespaceDictionary:Lcom/google/api/client/xml/XmlNamespaceDictionary;

    invoke-static {v2, p2}, Lcom/google/api/client/http/xml/atom/AtomContent;->forEntry(Lcom/google/api/client/xml/XmlNamespaceDictionary;Ljava/lang/Object;)Lcom/google/api/client/http/xml/atom/AtomContent;

    move-result-object v0

    goto :goto_0
.end method

.method public getNamespaceDictionary()Lcom/google/api/client/xml/XmlNamespaceDictionary;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/scloud/api/services/docs/GDataXmlClient;->namespaceDictionary:Lcom/google/api/client/xml/XmlNamespaceDictionary;

    return-object v0
.end method

.method public final getPartialResponse()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/samsung/scloud/api/services/docs/GDataXmlClient;->partialResponse:Z

    return v0
.end method

.method protected prepare(Lcom/google/api/client/http/HttpRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/google/api/client/http/HttpRequest;

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/samsung/scloud/api/services/docs/GDataClient;->prepare(Lcom/google/api/client/http/HttpRequest;)V

    .line 51
    new-instance v0, Lcom/google/api/client/http/xml/atom/AtomParser;

    iget-object v1, p0, Lcom/samsung/scloud/api/services/docs/GDataXmlClient;->namespaceDictionary:Lcom/google/api/client/xml/XmlNamespaceDictionary;

    invoke-direct {v0, v1}, Lcom/google/api/client/http/xml/atom/AtomParser;-><init>(Lcom/google/api/client/xml/XmlNamespaceDictionary;)V

    invoke-virtual {p1, v0}, Lcom/google/api/client/http/HttpRequest;->addParser(Lcom/google/api/client/http/HttpParser;)V

    .line 52
    return-void
.end method

.method protected prepareUrl(Lcom/google/api/client/googleapis/GoogleUrl;Ljava/lang/Class;)V
    .locals 1
    .param p1, "url"    # Lcom/google/api/client/googleapis/GoogleUrl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/client/googleapis/GoogleUrl;",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 64
    .local p2, "parseAsType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-super {p0, p1, p2}, Lcom/samsung/scloud/api/services/docs/GDataClient;->prepareUrl(Lcom/google/api/client/googleapis/GoogleUrl;Ljava/lang/Class;)V

    .line 65
    iget-boolean v0, p0, Lcom/samsung/scloud/api/services/docs/GDataXmlClient;->partialResponse:Z

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 66
    invoke-static {p2}, Lcom/google/api/client/googleapis/xml/atom/GoogleAtom;->getFieldsFor(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/api/client/googleapis/GoogleUrl;->setFields(Ljava/lang/String;)V

    .line 68
    :cond_0
    return-void
.end method

.method public final setPartialResponse(Z)V
    .locals 0
    .param p1, "partialResponse"    # Z

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/samsung/scloud/api/services/docs/GDataXmlClient;->partialResponse:Z

    .line 60
    return-void
.end method
