.class public Lcom/samsung/scloud/api/services/docs/model/Entry;
.super Ljava/lang/Object;
.source "Entry.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field public aclRole:Lcom/samsung/scloud/api/services/docs/model/AclRole;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "gAcl:role"
    .end annotation
.end field

.field public aclScope:Lcom/samsung/scloud/api/services/docs/model/AclScope;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "gAcl:scope"
    .end annotation
.end field

.field public author:Lcom/samsung/scloud/api/services/docs/model/Author;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "author"
    .end annotation
.end field

.field public category:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/scloud/api/services/docs/model/Category;",
            ">;"
        }
    .end annotation
.end field

.field public changestamp:Lcom/samsung/scloud/api/services/docs/model/Changestamp;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "docs:changestamp"
    .end annotation
.end field

.field public content:Lcom/samsung/scloud/api/services/docs/model/Content;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public description:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "docs:description"
    .end annotation
.end field

.field public docUploadData:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "docs:maxUploadSize"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/scloud/api/services/docs/model/DocUploadData;",
            ">;"
        }
    .end annotation
.end field

.field public etag:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "@gd:etag"
    .end annotation
.end field

.field public feedLink:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "gd:feedLink"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/scloud/api/services/docs/model/FeedLink;",
            ">;"
        }
    .end annotation
.end field

.field public id:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public largestChangestamp:Lcom/samsung/scloud/api/services/docs/model/LargestChangestamp;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "docs:largestChangestamp"
    .end annotation
.end field

.field public lastModified:Lcom/samsung/scloud/api/services/docs/model/LastModifiedInfo;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "gd:lastModifiedBy"
    .end annotation
.end field

.field public lastViewed:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "gd:lastViewed"
    .end annotation
.end field

.field public links:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "link"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/scloud/api/services/docs/model/Link;",
            ">;"
        }
    .end annotation
.end field

.field public published:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public quotaBytesTotal:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "gd:quotaBytesTotal"
    .end annotation
.end field

.field public quotaBytesUsed:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "gd:quotaBytesUsed"
    .end annotation
.end field

.field public quotaBytesUsedInTrash:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "docs:quotaBytesUsedInTrash"
    .end annotation
.end field

.field public resourceId:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "gd:resourceId"
    .end annotation
.end field

.field public size:J
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "docs:size"
    .end annotation
.end field

.field public summary:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public title:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field

.field public updated:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/scloud/api/services/docs/model/Entry;->category:Ljava/util/List;

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/scloud/api/services/docs/model/Entry;->docUploadData:Ljava/util/List;

    return-void
.end method


# virtual methods
.method protected clone()Lcom/samsung/scloud/api/services/docs/model/Entry;
    .locals 3

    .prologue
    .line 101
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/scloud/api/services/docs/model/Entry;

    .line 102
    .local v1, "result":Lcom/samsung/scloud/api/services/docs/model/Entry;
    invoke-static {p0, v1}, Lcom/google/api/client/util/Data;->deepCopy(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    return-object v1

    .line 104
    .end local v1    # "result":Lcom/samsung/scloud/api/services/docs/model/Entry;
    :catch_0
    move-exception v0

    .line 105
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method protected bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/samsung/scloud/api/services/docs/model/Entry;->clone()Lcom/samsung/scloud/api/services/docs/model/Entry;

    move-result-object v0

    return-object v0
.end method

.method public getResumableUpdateLink()Ljava/lang/String;
    .locals 4

    .prologue
    .line 110
    iget-object v1, p0, Lcom/samsung/scloud/api/services/docs/model/Entry;->links:Ljava/util/List;

    const-string v2, "http://schemas.google.com/g/2005#resumable-edit-media"

    invoke-static {v1, v2}, Lcom/samsung/scloud/api/services/docs/model/Link;->find(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 112
    .local v0, "link":Ljava/lang/String;
    const-string v1, "GoogleDrive"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Resumable upload link:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    return-object v0
.end method
