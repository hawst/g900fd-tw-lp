.class public Lcom/samsung/scloud/api/services/docs/model/Link;
.super Ljava/lang/Object;
.source "Link.java"


# instance fields
.field public href:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "@href"
    .end annotation
.end field

.field public rel:Ljava/lang/String;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "@rel"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static find(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "rel"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/scloud/api/services/docs/model/Link;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 31
    .local p0, "links":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/scloud/api/services/docs/model/Link;>;"
    if-eqz p0, :cond_1

    .line 32
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/scloud/api/services/docs/model/Link;

    .line 33
    .local v1, "link":Lcom/samsung/scloud/api/services/docs/model/Link;
    iget-object v2, v1, Lcom/samsung/scloud/api/services/docs/model/Link;->rel:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 34
    iget-object v2, v1, Lcom/samsung/scloud/api/services/docs/model/Link;->href:Ljava/lang/String;

    .line 38
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "link":Lcom/samsung/scloud/api/services/docs/model/Link;
    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
