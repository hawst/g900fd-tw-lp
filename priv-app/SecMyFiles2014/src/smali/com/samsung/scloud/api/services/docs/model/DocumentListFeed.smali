.class public Lcom/samsung/scloud/api/services/docs/model/DocumentListFeed;
.super Lcom/samsung/scloud/api/services/docs/model/Feed;
.source "DocumentListFeed.java"


# instance fields
.field public docs:Ljava/util/List;
    .annotation runtime Lcom/google/api/client/util/Key;
        value = "entry"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/scloud/api/services/docs/model/DocumentListEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/samsung/scloud/api/services/docs/model/Feed;-><init>()V

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/scloud/api/services/docs/model/DocumentListFeed;->docs:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public hasDocListEntry()Z
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/scloud/api/services/docs/model/DocumentListFeed;->docs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 32
    const/4 v0, 0x0

    .line 34
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
