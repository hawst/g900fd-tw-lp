.class public Lcom/samsung/scloud/api/services/docs/model/DocumentListEntry;
.super Lcom/samsung/scloud/api/services/docs/model/Entry;
.source "DocumentListEntry.java"


# instance fields
.field public mCategory:Lcom/samsung/scloud/api/services/docs/model/Category;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/samsung/scloud/api/services/docs/model/Entry;-><init>()V

    .line 22
    const-string v0, "file"

    invoke-static {v0}, Lcom/samsung/scloud/api/services/docs/model/Category;->newKind(Ljava/lang/String;)Lcom/samsung/scloud/api/services/docs/model/Category;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/scloud/api/services/docs/model/DocumentListEntry;->mCategory:Lcom/samsung/scloud/api/services/docs/model/Category;

    return-void
.end method


# virtual methods
.method public clone()Lcom/samsung/scloud/api/services/docs/model/DocumentListEntry;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/samsung/scloud/api/services/docs/model/Entry;->clone()Lcom/samsung/scloud/api/services/docs/model/Entry;

    move-result-object v0

    check-cast v0, Lcom/samsung/scloud/api/services/docs/model/DocumentListEntry;

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/samsung/scloud/api/services/docs/model/Entry;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/samsung/scloud/api/services/docs/model/DocumentListEntry;->clone()Lcom/samsung/scloud/api/services/docs/model/DocumentListEntry;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/samsung/scloud/api/services/docs/model/DocumentListEntry;->clone()Lcom/samsung/scloud/api/services/docs/model/DocumentListEntry;

    move-result-object v0

    return-object v0
.end method
