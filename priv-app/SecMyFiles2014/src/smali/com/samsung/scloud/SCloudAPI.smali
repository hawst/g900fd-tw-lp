.class public interface abstract Lcom/samsung/scloud/SCloudAPI;
.super Ljava/lang/Object;
.source "SCloudAPI.java"


# static fields
.field public static final COLLABORATION_STATUS_ACCEPTED:Ljava/lang/String; = "accepted"

.field public static final COLLABORATION_STATUS_PENDING:Ljava/lang/String; = "pending"

.field public static final DOWNLOAD_COMPLETED:Ljava/lang/String; = "Download completed"

.field public static final DOWNLOAD_PROGRESS:Ljava/lang/String; = "Download Progess"

.field public static final SCLOUD_PROVIDER_BOX:Ljava/lang/String; = "Box"

.field public static final SCLOUD_PROVIDER_DROPBOX:Ljava/lang/String; = "Dropbox"

.field public static final SCLOUD_PROVIDER_GOOGLE_DRIVE:Ljava/lang/String; = "GOOGLE_DRIVE"

.field public static final SCLOUD_PROVIDER_SPC:Ljava/lang/String; = "SPC"

.field public static final SHARE_HAS_EXPIRE:I = 0x1

.field public static final SHARE_HAS_NO_SHARE:I = 0x4

.field public static final SHARE_HAS_PASSWORD:I = 0x2

.field public static final STATUS_SERVICE_UNAVAILABLE:I = 0x1

.field public static final STATUS_UNKNOWN_HTTP_RESPONSE_CODE:I = 0x0

.field public static final TAG:Ljava/lang/String; = "SCLOUD_SDK"

.field public static final THUMBNAIL_1024x768:I = 0x1

.field public static final THUMBNAIL_128x128:I = 0x7

.field public static final THUMBNAIL_2048x1536:I = 0x0

.field public static final THUMBNAIL_256x256:I = 0x6

.field public static final THUMBNAIL_320x240:I = 0x5

.field public static final THUMBNAIL_480x320:I = 0x4

.field public static final THUMBNAIL_640x480:I = 0x3

.field public static final THUMBNAIL_64x64:I = 0x8

.field public static final THUMBNAIL_960x640:I = 0x2

.field public static final THUMBNAIL_JPEG:I = 0x0

.field public static final THUMBNAIL_PNG:I = 0x1


# virtual methods
.method public abstract addComment(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/Comment;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract copy(Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/scloud/data/SCloudNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract createRepository(Lcom/samsung/scloud/data/Repository;)Lcom/samsung/scloud/data/Repository;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract createRepository(Ljava/lang/String;J)Lcom/samsung/scloud/data/Repository;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract createShareURL(Ljava/lang/String;Ljava/lang/String;J)Lcom/samsung/scloud/data/ShareInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract delete(Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract deleteDeviceProfile()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract deleteRepository(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract editComment(Ljava/lang/String;JLjava/lang/String;)Lcom/samsung/scloud/data/Comment;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract finishAuth()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract getAuthToken()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract getCollaboration(Ljava/lang/String;)Lcom/samsung/scloud/data/Collaboration;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract getCollaborations()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Collaboration;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract getComments(Ljava/lang/String;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Comment;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract getDeltaList(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/scloud/data/SCloudNodeList",
            "<",
            "Lcom/samsung/scloud/data/SCloudNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract getDeviceProfile()Lcom/samsung/scloud/data/Device;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract getDeviceProfile(Ljava/lang/String;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract getDownloadURL(Ljava/lang/String;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract getFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/scloud/response/ProgressListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract getFile(Ljava/io/File;Ljava/lang/String;ZLcom/samsung/scloud/response/ProgressListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract getFileInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract getFilesAsZip(Ljava/io/File;Ljava/util/List;ZLcom/samsung/scloud/response/ProgressListener;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z",
            "Lcom/samsung/scloud/response/ProgressListener;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract getFolderInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract getList(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract getMetadata(Ljava/lang/String;)Lcom/samsung/scloud/data/Metadata;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract getMusicCoverArt(Ljava/lang/String;Ljava/io/File;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract getMyCloudProvider()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract getRecycleBin(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract getRepository(Ljava/lang/String;)Lcom/samsung/scloud/data/Repository;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract getRepositoryInfo()Lcom/samsung/scloud/data/RepositoryInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract getShareURLInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/ShareInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract getStreamingInfoForMusic(Ljava/lang/String;)Lcom/samsung/scloud/data/StreamingInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract getStreamingInfoForVideo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/StreamingInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract getThumbnail(Ljava/io/File;Ljava/lang/String;IILcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract getUpdate(JJ)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Update;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract getUserInfo()Lcom/samsung/scloud/data/User;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract getVersions(Ljava/lang/String;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Version;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract getWholeFileList(ILjava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/scloud/data/SCloudNodeList",
            "<",
            "Lcom/samsung/scloud/data/SCloudFile;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract getWholeFolderList(ILjava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/scloud/data/SCloudNodeList",
            "<",
            "Lcom/samsung/scloud/data/SCloudFolder;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract inviteCollaborators(Ljava/lang/String;Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Collaborator;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract isAuthSucceed()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract login(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/User;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract logout()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract makeCurrentVersion(Ljava/lang/String;J)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Version;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract makeFolder(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract move(Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/scloud/data/SCloudNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract putFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract putFile(Ljava/io/File;Ljava/lang/String;ZLjava/lang/String;Lcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract putFile(Ljava/io/FileInputStream;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract removeCollaboration(J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract removeComment(Ljava/lang/String;J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract removeShareURL(Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract rename(Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/scloud/data/SCloudNode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract restoreFromRecycleBin(Ljava/lang/String;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract search(Ljava/lang/String;Ljava/lang/String;IZ)Lcom/samsung/scloud/data/SearchResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract setActiveRepository(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract setAuthToken(Ljava/lang/String;)V
.end method

.method public abstract setDescription(Ljava/lang/String;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract setDeviceProfile(Lcom/samsung/scloud/data/Device;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract setDeviceProfile(Ljava/lang/String;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract startAuth(Landroid/content/Context;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract unsetDescription(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract updateRepository(Lcom/samsung/scloud/data/Repository;)Lcom/samsung/scloud/data/Repository;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method

.method public abstract updateRepository(Ljava/lang/String;J)Lcom/samsung/scloud/data/Repository;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation
.end method
