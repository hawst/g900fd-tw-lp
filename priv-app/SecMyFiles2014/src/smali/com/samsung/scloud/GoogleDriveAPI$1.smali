.class Lcom/samsung/scloud/GoogleDriveAPI$1;
.super Ljava/lang/Object;
.source "GoogleDriveAPI.java"

# interfaces
.implements Lcom/google/api/client/googleapis/media/MediaHttpUploaderProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/scloud/GoogleDriveAPI;->putNewFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/scloud/GoogleDriveAPI;

.field final synthetic val$fileName:Ljava/lang/String;

.field final synthetic val$progressListener:Lcom/samsung/scloud/response/ProgressListener;

.field final synthetic val$uploadName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/scloud/GoogleDriveAPI;Ljava/lang/String;Lcom/samsung/scloud/response/ProgressListener;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 513
    iput-object p1, p0, Lcom/samsung/scloud/GoogleDriveAPI$1;->this$0:Lcom/samsung/scloud/GoogleDriveAPI;

    iput-object p2, p0, Lcom/samsung/scloud/GoogleDriveAPI$1;->val$uploadName:Ljava/lang/String;

    iput-object p3, p0, Lcom/samsung/scloud/GoogleDriveAPI$1;->val$progressListener:Lcom/samsung/scloud/response/ProgressListener;

    iput-object p4, p0, Lcom/samsung/scloud/GoogleDriveAPI$1;->val$fileName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public progressChanged(Lcom/google/api/client/googleapis/media/MediaHttpUploader;)V
    .locals 8
    .param p1, "arg0"    # Lcom/google/api/client/googleapis/media/MediaHttpUploader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x64

    .line 517
    invoke-virtual {p1}, Lcom/google/api/client/googleapis/media/MediaHttpUploader;->getProgress()D

    move-result-wide v2

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    mul-double/2addr v2, v4

    double-to-long v0, v2

    .line 518
    .local v0, "progress":J
    const-string v2, "SCLOUD_SDK"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Upload["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/scloud/GoogleDriveAPI$1;->val$uploadName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] Progress: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    iget-object v2, p0, Lcom/samsung/scloud/GoogleDriveAPI$1;->val$progressListener:Lcom/samsung/scloud/response/ProgressListener;

    if-eqz v2, :cond_0

    .line 520
    cmp-long v2, v0, v6

    if-gez v2, :cond_1

    .line 521
    iget-object v2, p0, Lcom/samsung/scloud/GoogleDriveAPI$1;->val$progressListener:Lcom/samsung/scloud/response/ProgressListener;

    iget-object v3, p0, Lcom/samsung/scloud/GoogleDriveAPI$1;->val$fileName:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4, v0, v1}, Lcom/samsung/scloud/response/ProgressListener;->onUpdate(Ljava/lang/String;IJ)V

    .line 526
    :cond_0
    :goto_0
    return-void

    .line 523
    :cond_1
    iget-object v2, p0, Lcom/samsung/scloud/GoogleDriveAPI$1;->val$progressListener:Lcom/samsung/scloud/response/ProgressListener;

    iget-object v3, p0, Lcom/samsung/scloud/GoogleDriveAPI$1;->val$fileName:Ljava/lang/String;

    const/4 v4, 0x2

    invoke-interface {v2, v3, v4, v6, v7}, Lcom/samsung/scloud/response/ProgressListener;->onUpdate(Ljava/lang/String;IJ)V

    goto :goto_0
.end method
