.class public Lcom/samsung/scloud/GoogleDriveAPI;
.super Ljava/lang/Object;
.source "GoogleDriveAPI.java"

# interfaces
.implements Lcom/samsung/scloud/SCloudAPI;


# static fields
.field private static mInstance:Lcom/samsung/scloud/GoogleDriveAPI;


# instance fields
.field protected accessToken:Ljava/lang/String;

.field protected accountName:Ljava/lang/String;

.field public credential:Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx;

.field public driveInstance:Lcom/google/api/services/drive/Drive;

.field public hasRefreshToken:Z

.field public jsonFactory:Lcom/google/api/client/json/JsonFactory;

.field private mContext:Landroid/content/Context;

.field mIsRetryAuth:Z

.field private mLatch:Ljava/util/concurrent/CountDownLatch;

.field protected refreshToken:Ljava/lang/String;

.field public requestFactory:Lcom/google/api/client/http/HttpRequestFactory;

.field public rootFolderId:Ljava/lang/String;

.field public transport:Lcom/google/api/client/http/HttpTransport;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput-object v1, p0, Lcom/samsung/scloud/GoogleDriveAPI;->accessToken:Ljava/lang/String;

    .line 96
    iput-object v1, p0, Lcom/samsung/scloud/GoogleDriveAPI;->refreshToken:Ljava/lang/String;

    .line 97
    iput-object v1, p0, Lcom/samsung/scloud/GoogleDriveAPI;->accountName:Ljava/lang/String;

    .line 99
    iput-boolean v2, p0, Lcom/samsung/scloud/GoogleDriveAPI;->hasRefreshToken:Z

    .line 106
    iput-object v1, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    .line 107
    iput-boolean v2, p0, Lcom/samsung/scloud/GoogleDriveAPI;->mIsRetryAuth:Z

    .line 108
    iput-object v1, p0, Lcom/samsung/scloud/GoogleDriveAPI;->rootFolderId:Ljava/lang/String;

    .line 111
    iput-object p1, p0, Lcom/samsung/scloud/GoogleDriveAPI;->mContext:Landroid/content/Context;

    .line 113
    new-instance v1, Lcom/google/api/client/http/javanet/NetHttpTransport;

    invoke-direct {v1}, Lcom/google/api/client/http/javanet/NetHttpTransport;-><init>()V

    iput-object v1, p0, Lcom/samsung/scloud/GoogleDriveAPI;->transport:Lcom/google/api/client/http/HttpTransport;

    .line 114
    new-instance v1, Lcom/google/api/client/json/jackson/JacksonFactory;

    invoke-direct {v1}, Lcom/google/api/client/json/jackson/JacksonFactory;-><init>()V

    iput-object v1, p0, Lcom/samsung/scloud/GoogleDriveAPI;->jsonFactory:Lcom/google/api/client/json/JsonFactory;

    .line 115
    new-instance v1, Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx$Builder;

    invoke-direct {v1}, Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx$Builder;-><init>()V

    iget-object v2, p0, Lcom/samsung/scloud/GoogleDriveAPI;->jsonFactory:Lcom/google/api/client/json/JsonFactory;

    invoke-virtual {v1, v2}, Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx$Builder;->setJsonFactory(Lcom/google/api/client/json/JsonFactory;)Lcom/google/api/client/googleapis/auth/oauth2/GoogleCredential$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/scloud/GoogleDriveAPI;->transport:Lcom/google/api/client/http/HttpTransport;

    invoke-virtual {v1, v2}, Lcom/google/api/client/googleapis/auth/oauth2/GoogleCredential$Builder;->setTransport(Lcom/google/api/client/http/HttpTransport;)Lcom/google/api/client/googleapis/auth/oauth2/GoogleCredential$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/api/client/googleapis/auth/oauth2/GoogleCredential$Builder;->build()Lcom/google/api/client/googleapis/auth/oauth2/GoogleCredential;

    move-result-object v1

    check-cast v1, Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx;

    iput-object v1, p0, Lcom/samsung/scloud/GoogleDriveAPI;->credential:Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx;

    .line 119
    iget-object v1, p0, Lcom/samsung/scloud/GoogleDriveAPI;->transport:Lcom/google/api/client/http/HttpTransport;

    iget-object v2, p0, Lcom/samsung/scloud/GoogleDriveAPI;->credential:Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx;

    invoke-virtual {v1, v2}, Lcom/google/api/client/http/HttpTransport;->createRequestFactory(Lcom/google/api/client/http/HttpRequestInitializer;)Lcom/google/api/client/http/HttpRequestFactory;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/scloud/GoogleDriveAPI;->requestFactory:Lcom/google/api/client/http/HttpRequestFactory;

    .line 121
    new-instance v1, Lcom/google/api/services/drive/Drive$Builder;

    iget-object v2, p0, Lcom/samsung/scloud/GoogleDriveAPI;->transport:Lcom/google/api/client/http/HttpTransport;

    iget-object v3, p0, Lcom/samsung/scloud/GoogleDriveAPI;->jsonFactory:Lcom/google/api/client/json/JsonFactory;

    iget-object v4, p0, Lcom/samsung/scloud/GoogleDriveAPI;->credential:Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/api/services/drive/Drive$Builder;-><init>(Lcom/google/api/client/http/HttpTransport;Lcom/google/api/client/json/JsonFactory;Lcom/google/api/client/http/HttpRequestInitializer;)V

    new-instance v2, Lcom/google/api/client/googleapis/services/GoogleKeyInitializer;

    const-string v3, "AIzaSyAEKSS6tGrKKGR25Emd-gXKs22SlF3QjBQ"

    invoke-direct {v2, v3}, Lcom/google/api/client/googleapis/services/GoogleKeyInitializer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/google/api/services/drive/Drive$Builder;->setJsonHttpRequestInitializer(Lcom/google/api/client/http/json/JsonHttpRequestInitializer;)Lcom/google/api/services/drive/Drive$Builder;

    move-result-object v0

    .line 123
    .local v0, "builder":Lcom/google/api/services/drive/Drive$Builder;
    invoke-virtual {v0}, Lcom/google/api/services/drive/Drive$Builder;->build()Lcom/google/api/services/drive/Drive;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    .line 125
    invoke-virtual {p0}, Lcom/samsung/scloud/GoogleDriveAPI;->enableLogging()V

    .line 126
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/scloud/GoogleDriveAPI;)Ljava/util/concurrent/CountDownLatch;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/scloud/GoogleDriveAPI;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/scloud/GoogleDriveAPI;->mLatch:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method

.method private checkForAuthException(Ljava/lang/Throwable;)Z
    .locals 3
    .param p1, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 1929
    instance-of v1, p1, Lcom/google/api/client/http/HttpResponseException;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 1930
    check-cast v0, Lcom/google/api/client/http/HttpResponseException;

    .line 1931
    .local v0, "exception":Lcom/google/api/client/http/HttpResponseException;
    invoke-virtual {v0}, Lcom/google/api/client/http/HttpResponseException;->getStatusCode()I

    move-result v1

    const/16 v2, 0x191

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/google/api/client/http/HttpResponseException;->getStatusCode()I

    move-result v1

    const/16 v2, 0x193

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/google/api/client/http/HttpResponseException;->getStatusCode()I

    move-result v1

    const/16 v2, 0x194

    if-ne v1, v2, :cond_1

    .line 1932
    :cond_0
    const/4 v1, 0x1

    .line 1935
    .end local v0    # "exception":Lcom/google/api/client/http/HttpResponseException;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private convertToLong(Ljava/lang/String;)J
    .locals 7
    .param p1, "dateString"    # Ljava/lang/String;

    .prologue
    .line 1944
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyy-MM-dd HH:mm:ss.SSS"

    invoke-direct {v0, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1945
    .local v0, "dateFormat":Ljava/text/SimpleDateFormat;
    const/4 v1, 0x0

    .line 1947
    .local v1, "dateTime":Ljava/util/Date;
    const-string v3, "T"

    const-string v6, " "

    invoke-virtual {p1, v3, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 1948
    const-string v3, "Z"

    const-string v6, ""

    invoke-virtual {p1, v3, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 1951
    :try_start_0
    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1956
    :goto_0
    const-wide/16 v4, -0x1

    .line 1957
    .local v4, "returnValue":J
    if-eqz v1, :cond_0

    .line 1958
    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    .line 1960
    :cond_0
    return-wide v4

    .line 1952
    .end local v4    # "returnValue":J
    :catch_0
    move-exception v2

    .line 1953
    .local v2, "e":Ljava/text/ParseException;
    invoke-virtual {v2}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/scloud/GoogleDriveAPI;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 129
    const-class v1, Lcom/samsung/scloud/GoogleDriveAPI;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/scloud/GoogleDriveAPI;->mInstance:Lcom/samsung/scloud/GoogleDriveAPI;

    if-nez v0, :cond_0

    .line 130
    new-instance v0, Lcom/samsung/scloud/GoogleDriveAPI;

    invoke-direct {v0, p0}, Lcom/samsung/scloud/GoogleDriveAPI;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/scloud/GoogleDriveAPI;->mInstance:Lcom/samsung/scloud/GoogleDriveAPI;

    .line 132
    :cond_0
    sget-object v0, Lcom/samsung/scloud/GoogleDriveAPI;->mInstance:Lcom/samsung/scloud/GoogleDriveAPI;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 129
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getMediaType(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 1939
    const/4 v1, 0x0

    const-string v2, "/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1940
    .local v0, "type":Ljava/lang/String;
    return-object v0
.end method

.method private getRootFolderId()Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudIOException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;
        }
    .end annotation

    .prologue
    .line 1917
    iget-object v3, p0, Lcom/samsung/scloud/GoogleDriveAPI;->rootFolderId:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/scloud/GoogleDriveAPI;->rootFolderId:Ljava/lang/String;

    .line 1925
    :goto_0
    return-object v3

    .line 1919
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v3}, Lcom/google/api/services/drive/Drive;->about()Lcom/google/api/services/drive/Drive$About;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/api/services/drive/Drive$About;->get()Lcom/google/api/services/drive/Drive$About$Get;

    move-result-object v3

    const-string v4, "rootFolderId"

    invoke-virtual {v3, v4}, Lcom/google/api/services/drive/Drive$About$Get;->setFields(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$About$Get;

    move-result-object v2

    .line 1920
    .local v2, "getUserRequest":Lcom/google/api/services/drive/Drive$About$Get;
    invoke-direct {p0, v2}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/About;

    .line 1921
    .local v0, "about":Lcom/google/api/services/drive/model/About;
    invoke-virtual {v0}, Lcom/google/api/services/drive/model/About;->getRootFolderId()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/scloud/GoogleDriveAPI;->rootFolderId:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1925
    .end local v0    # "about":Lcom/google/api/services/drive/model/About;
    .end local v2    # "getUserRequest":Lcom/google/api/services/drive/Drive$About$Get;
    :goto_1
    iget-object v3, p0, Lcom/samsung/scloud/GoogleDriveAPI;->rootFolderId:Ljava/lang/String;

    goto :goto_0

    .line 1922
    :catch_0
    move-exception v1

    .line 1923
    .local v1, "e":Ljava/io/IOException;
    const-string v3, "getRootFolderId - getting root folder id failed"

    invoke-direct {p0, v1, v3}, Lcom/samsung/scloud/GoogleDriveAPI;->throwIOorAuthException(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private loadToken()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1839
    iget-boolean v1, p0, Lcom/samsung/scloud/GoogleDriveAPI;->hasRefreshToken:Z

    if-eqz v1, :cond_0

    .line 1852
    :goto_0
    return-void

    .line 1841
    :cond_0
    iget-object v1, p0, Lcom/samsung/scloud/GoogleDriveAPI;->mContext:Landroid/content/Context;

    const-string v2, "googleCredential"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1842
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "accessToken"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/scloud/GoogleDriveAPI;->accessToken:Ljava/lang/String;

    .line 1843
    const-string v1, "curAccountName"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/scloud/GoogleDriveAPI;->accountName:Ljava/lang/String;

    .line 1845
    const-string v1, "SCLOUD_SDK"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Loading Access token : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/scloud/GoogleDriveAPI;->accessToken:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Account name : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/scloud/GoogleDriveAPI;->accountName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1847
    iget-object v1, p0, Lcom/samsung/scloud/GoogleDriveAPI;->accessToken:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/scloud/GoogleDriveAPI;->credential:Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx;

    if-eqz v1, :cond_1

    .line 1848
    iget-object v1, p0, Lcom/samsung/scloud/GoogleDriveAPI;->credential:Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx;

    iget-object v2, p0, Lcom/samsung/scloud/GoogleDriveAPI;->accessToken:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx;->setAccessToken(Ljava/lang/String;)Lcom/google/api/client/googleapis/auth/oauth2/GoogleCredential;

    goto :goto_0

    .line 1850
    :cond_1
    const-string v1, "SCLOUD_SDK"

    const-string v2, "Token is null !"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private parseDriveFile(Lcom/google/api/services/drive/model/File;)Lcom/samsung/scloud/data/SCloudNode;
    .locals 10
    .param p1, "f"    # Lcom/google/api/services/drive/model/File;

    .prologue
    .line 1971
    if-nez p1, :cond_1

    const/4 v4, 0x0

    .line 2047
    :cond_0
    :goto_0
    return-object v4

    .line 1972
    :cond_1
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getMimeType()Ljava/lang/String;

    move-result-object v6

    const-string v7, "application/vnd.google-apps.folder"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1973
    new-instance v4, Lcom/samsung/scloud/data/SCloudFolder;

    invoke-direct {v4}, Lcom/samsung/scloud/data/SCloudFolder;-><init>()V

    .line 1974
    .local v4, "ret":Lcom/samsung/scloud/data/SCloudNode;
    sget-object v6, Lcom/samsung/scloud/data/SCloudNode;->TYPE_FOLDER:Ljava/lang/String;

    invoke-virtual {v4, v6}, Lcom/samsung/scloud/data/SCloudNode;->setNodeType(Ljava/lang/String;)V

    .line 1980
    :goto_1
    const/4 v1, 0x0

    .line 1981
    .local v1, "index":I
    const/4 v5, 0x0

    .line 1982
    .local v5, "somethingAdded":Z
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1983
    .local v2, "pList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getParents()Ljava/util/List;

    move-result-object v6

    if-eqz v6, :cond_6

    .line 1984
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getParents()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/drive/model/ParentReference;

    .line 1985
    .local v3, "pr":Lcom/google/api/services/drive/model/ParentReference;
    invoke-virtual {v3}, Lcom/google/api/services/drive/model/ParentReference;->getId()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {v3}, Lcom/google/api/services/drive/model/ParentReference;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_3

    .line 1986
    const/4 v5, 0x1

    .line 1987
    if-nez v1, :cond_2

    invoke-virtual {v3}, Lcom/google/api/services/drive/model/ParentReference;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/samsung/scloud/data/SCloudNode;->setParentFolderId(Ljava/lang/String;)V

    .line 1988
    :cond_2
    invoke-virtual {v3}, Lcom/google/api/services/drive/model/ParentReference;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1990
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1976
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "index":I
    .end local v2    # "pList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "pr":Lcom/google/api/services/drive/model/ParentReference;
    .end local v4    # "ret":Lcom/samsung/scloud/data/SCloudNode;
    .end local v5    # "somethingAdded":Z
    :cond_4
    new-instance v4, Lcom/samsung/scloud/data/SCloudFile;

    invoke-direct {v4}, Lcom/samsung/scloud/data/SCloudFile;-><init>()V

    .restart local v4    # "ret":Lcom/samsung/scloud/data/SCloudNode;
    goto :goto_1

    .line 1992
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v1    # "index":I
    .restart local v2    # "pList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v5    # "somethingAdded":Z
    :cond_5
    if-eqz v5, :cond_6

    invoke-virtual {v4, v2}, Lcom/samsung/scloud/data/SCloudNode;->setParentFolderIds(Ljava/util/List;)V

    .line 1995
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_6
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getPermissionsLink()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/samsung/scloud/data/SCloudNode;->setAccessControlUrl(Ljava/lang/String;)V

    .line 1996
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getEtag()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/samsung/scloud/data/SCloudNode;->seteTag(Ljava/lang/String;)V

    .line 1997
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getLastViewedByMeDate()Lcom/google/api/client/util/DateTime;

    move-result-object v6

    if-eqz v6, :cond_7

    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getLastViewedByMeDate()Lcom/google/api/client/util/DateTime;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/api/client/util/DateTime;->getValue()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/samsung/scloud/data/SCloudNode;->setLastViewedTimestamp(J)V

    .line 1998
    :cond_7
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getLabels()Lcom/google/api/services/drive/model/File$Labels;

    move-result-object v6

    if-eqz v6, :cond_8

    .line 1999
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getLabels()Lcom/google/api/services/drive/model/File$Labels;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/api/services/drive/model/File$Labels;->getTrashed()Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    invoke-virtual {v4, v6}, Lcom/samsung/scloud/data/SCloudNode;->setTrashed(Z)V

    .line 2000
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getLabels()Lcom/google/api/services/drive/model/File$Labels;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/api/services/drive/model/File$Labels;->getStarred()Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    invoke-virtual {v4, v6}, Lcom/samsung/scloud/data/SCloudNode;->setStarred(Z)V

    .line 2004
    :goto_3
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getOwnerNames()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/samsung/scloud/data/SCloudNode;->setOwnerNames(Ljava/util/List;)V

    .line 2005
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getUserPermission()Lcom/google/api/services/drive/model/Permission;

    move-result-object v6

    if-eqz v6, :cond_9

    .line 2006
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getUserPermission()Lcom/google/api/services/drive/model/Permission;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/api/services/drive/model/Permission;->getRole()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/samsung/scloud/data/SCloudNode;->setRole(Ljava/lang/String;)V

    .line 2010
    :goto_4
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getDownloadUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/samsung/scloud/data/SCloudNode;->setDownloadUrl(Ljava/lang/String;)V

    .line 2011
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/samsung/scloud/data/SCloudNode;->setId(Ljava/lang/String;)V

    .line 2012
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/samsung/scloud/data/SCloudNode;->setName(Ljava/lang/String;)V

    .line 2013
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getCreatedDate()Lcom/google/api/client/util/DateTime;

    move-result-object v6

    if-eqz v6, :cond_a

    .line 2014
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getCreatedDate()Lcom/google/api/client/util/DateTime;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/api/client/util/DateTime;->getValue()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/samsung/scloud/data/SCloudNode;->setCreatedTimestamp(J)V

    .line 2018
    :goto_5
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getModifiedDate()Lcom/google/api/client/util/DateTime;

    move-result-object v6

    if-eqz v6, :cond_b

    .line 2019
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getModifiedDate()Lcom/google/api/client/util/DateTime;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/api/client/util/DateTime;->getValue()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/samsung/scloud/data/SCloudNode;->setUpdatedTimestamp(J)V

    .line 2023
    :goto_6
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getDescription()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/samsung/scloud/data/SCloudNode;->setDescription(Ljava/lang/String;)V

    .line 2026
    instance-of v6, v4, Lcom/samsung/scloud/data/SCloudFile;

    if-eqz v6, :cond_0

    .line 2027
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getQuotaBytesUsed()Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/samsung/scloud/data/SCloudNode;->setQuotaBytesUsed(J)V

    .line 2028
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getFileSize()Ljava/lang/Long;

    move-result-object v6

    if-eqz v6, :cond_c

    .line 2029
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getFileSize()Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/samsung/scloud/data/SCloudNode;->setSize(J)V

    .line 2033
    :goto_7
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getThumbnailLink()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/samsung/scloud/data/SCloudNode;->setThumbnailUrl(Ljava/lang/String;)V

    .line 2034
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getQuotaBytesUsed()Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, 0x1

    cmp-long v6, v6, v8

    if-gez v6, :cond_d

    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getExportLinks()Ljava/util/Map;

    move-result-object v6

    if-eqz v6, :cond_d

    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getExportLinks()Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Map;->size()I

    move-result v6

    if-lez v6, :cond_d

    .line 2035
    sget-object v6, Lcom/samsung/scloud/data/SCloudNode;->TYPE_GOOGLEAPP:Ljava/lang/String;

    invoke-virtual {v4, v6}, Lcom/samsung/scloud/data/SCloudNode;->setNodeType(Ljava/lang/String;)V

    :goto_8
    move-object v6, v4

    .line 2039
    check-cast v6, Lcom/samsung/scloud/data/SCloudFile;

    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getMd5Checksum()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/scloud/data/SCloudFile;->setMd5Checksum(Ljava/lang/String;)V

    .line 2040
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getMimeType()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_e

    move-object v6, v4

    .line 2041
    check-cast v6, Lcom/samsung/scloud/data/SCloudFile;

    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getMimeType()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/scloud/data/SCloudFile;->setMimeType(Ljava/lang/String;)V

    move-object v6, v4

    .line 2042
    check-cast v6, Lcom/samsung/scloud/data/SCloudFile;

    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getMimeType()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/samsung/scloud/GoogleDriveAPI;->getMediaType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/samsung/scloud/data/SCloudFile;->setMediaType(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2002
    :cond_8
    const-string v6, "SCLOUD_SDK"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[parseDriveFile] Label is null - title="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 2008
    :cond_9
    const-string v6, "SCLOUD_SDK"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[parseDriveFile] Role is null - title="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    .line 2016
    :cond_a
    const-string v6, "SCLOUD_SDK"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[parseDriveFile] CreatedDate is null - title="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 2021
    :cond_b
    const-string v6, "SCLOUD_SDK"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[parseDriveFile] UpdatedTime is null - title="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    .line 2031
    :cond_c
    const-wide/16 v6, 0x0

    invoke-virtual {v4, v6, v7}, Lcom/samsung/scloud/data/SCloudNode;->setSize(J)V

    goto/16 :goto_7

    .line 2037
    :cond_d
    sget-object v6, Lcom/samsung/scloud/data/SCloudNode;->TYPE_FILE:Ljava/lang/String;

    invoke-virtual {v4, v6}, Lcom/samsung/scloud/data/SCloudNode;->setNodeType(Ljava/lang/String;)V

    goto/16 :goto_8

    .line 2044
    :cond_e
    const-string v6, "SCLOUD_SDK"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[parseDriveFile] MimeType is null - title="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/google/api/services/drive/model/File;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private parsePermissionFeed(Lcom/google/api/services/drive/model/PermissionList;)Lcom/samsung/scloud/data/Collaboration;
    .locals 6
    .param p1, "permissionList"    # Lcom/google/api/services/drive/model/PermissionList;

    .prologue
    .line 1809
    if-nez p1, :cond_0

    .line 1810
    const/4 v0, 0x0

    .line 1835
    :goto_0
    return-object v0

    .line 1812
    :cond_0
    new-instance v0, Lcom/samsung/scloud/data/Collaboration;

    invoke-direct {v0}, Lcom/samsung/scloud/data/Collaboration;-><init>()V

    .line 1813
    .local v0, "collaboration":Lcom/samsung/scloud/data/Collaboration;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1814
    .local v2, "collaborators":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/Collaborator;>;"
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/PermissionList;->getEtag()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/samsung/scloud/data/Collaboration;->setEtag(Ljava/lang/String;)V

    .line 1815
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/PermissionList;->getItems()Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 1816
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/PermissionList;->getItems()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/api/services/drive/model/Permission;

    .line 1817
    .local v4, "permission":Lcom/google/api/services/drive/model/Permission;
    new-instance v1, Lcom/samsung/scloud/data/Collaborator;

    invoke-direct {v1}, Lcom/samsung/scloud/data/Collaborator;-><init>()V

    .line 1818
    .local v1, "collaborator":Lcom/samsung/scloud/data/Collaborator;
    invoke-virtual {v4}, Lcom/google/api/services/drive/model/Permission;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/samsung/scloud/data/Collaborator;->setCollaborationId(Ljava/lang/String;)V

    .line 1819
    invoke-virtual {v4}, Lcom/google/api/services/drive/model/Permission;->getRole()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/samsung/scloud/data/Collaborator;->setRole(Ljava/lang/String;)V

    .line 1820
    invoke-virtual {v4}, Lcom/google/api/services/drive/model/Permission;->getType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/samsung/scloud/data/Collaborator;->setType(Ljava/lang/String;)V

    .line 1821
    invoke-virtual {v4}, Lcom/google/api/services/drive/model/Permission;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/samsung/scloud/data/Collaborator;->setValue(Ljava/lang/String;)V

    .line 1822
    invoke-virtual {v4}, Lcom/google/api/services/drive/model/Permission;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/samsung/scloud/data/Collaborator;->setUserName(Ljava/lang/String;)V

    .line 1823
    invoke-virtual {v4}, Lcom/google/api/services/drive/model/Permission;->getEtag()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/samsung/scloud/data/Collaborator;->setEtag(Ljava/lang/String;)V

    .line 1824
    invoke-virtual {v4}, Lcom/google/api/services/drive/model/Permission;->getSelfLink()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/samsung/scloud/data/Collaborator;->setSelfLink(Ljava/lang/String;)V

    .line 1826
    invoke-virtual {v4}, Lcom/google/api/services/drive/model/Permission;->getAuthKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/samsung/scloud/data/Collaborator;->setAuthkey(Ljava/lang/String;)V

    .line 1827
    invoke-virtual {v4}, Lcom/google/api/services/drive/model/Permission;->getPhotoLink()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/samsung/scloud/data/Collaborator;->setPhotoLink(Ljava/lang/String;)V

    .line 1828
    invoke-virtual {v4}, Lcom/google/api/services/drive/model/Permission;->getAdditionalRoles()Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 1829
    invoke-virtual {v4}, Lcom/google/api/services/drive/model/Permission;->getAdditionalRoles()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/samsung/scloud/data/Collaborator;->setAdditionalRoles(Ljava/util/List;)V

    .line 1831
    :cond_1
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1834
    .end local v1    # "collaborator":Lcom/samsung/scloud/data/Collaborator;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "permission":Lcom/google/api/services/drive/model/Permission;
    :cond_2
    invoke-virtual {v0, v2}, Lcom/samsung/scloud/data/Collaboration;->setCollaborators(Ljava/util/ArrayList;)V

    goto/16 :goto_0
.end method

.method private parseRevisionFeedAndMap(Lcom/google/api/services/drive/model/RevisionList;)Ljava/util/ArrayList;
    .locals 8
    .param p1, "revisions"    # Lcom/google/api/services/drive/model/RevisionList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/services/drive/model/RevisionList;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Version;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1733
    if-nez p1, :cond_1

    .line 1734
    const/4 v3, 0x0

    .line 1748
    :cond_0
    return-object v3

    .line 1736
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1738
    .local v3, "revisionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/Version;>;"
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/RevisionList;->getItems()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/drive/model/Revision;

    .line 1739
    .local v1, "rev":Lcom/google/api/services/drive/model/Revision;
    new-instance v2, Lcom/samsung/scloud/data/Version;

    invoke-direct {v2}, Lcom/samsung/scloud/data/Version;-><init>()V

    .line 1740
    .local v2, "revision":Lcom/samsung/scloud/data/Version;
    invoke-virtual {v1}, Lcom/google/api/services/drive/model/Revision;->getModifiedDate()Lcom/google/api/client/util/DateTime;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/api/client/util/DateTime;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/samsung/scloud/GoogleDriveAPI;->convertToLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/samsung/scloud/data/Version;->setUpdateTimestamp(J)V

    .line 1741
    invoke-virtual {v1}, Lcom/google/api/services/drive/model/Revision;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/samsung/scloud/data/Version;->setRevisoinId(Ljava/lang/String;)V

    .line 1744
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1745
    const-string v4, "GoogleDrive"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updateTime:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/samsung/scloud/data/Version;->getUpdateTimestamp()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\nrevison id:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/samsung/scloud/data/Version;->getRevisionId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private parseSearchList(Lcom/google/api/services/drive/model/FileList;Ljava/lang/String;)Ljava/util/List;
    .locals 8
    .param p1, "fileList"    # Lcom/google/api/services/drive/model/FileList;
    .param p2, "folderId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/api/services/drive/model/FileList;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/scloud/data/SCloudFile;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 1709
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v7, 0x1

    if-ge v5, v7, :cond_2

    :cond_0
    move-object v4, v6

    .line 1725
    :cond_1
    :goto_0
    return-object v4

    .line 1712
    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1713
    .local v4, "searchList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/scloud/data/SCloudFile;>;"
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/FileList;->getItems()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/drive/model/File;

    .line 1714
    .local v0, "file":Lcom/google/api/services/drive/model/File;
    invoke-virtual {v0}, Lcom/google/api/services/drive/model/File;->getParents()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v2

    .line 1715
    .local v2, "iterator":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lcom/google/api/services/drive/model/ParentReference;>;"
    :cond_4
    invoke-interface {v2}, Ljava/util/ListIterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1716
    invoke-interface {v2}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/api/services/drive/model/ParentReference;

    .line 1717
    .local v3, "reference":Lcom/google/api/services/drive/model/ParentReference;
    invoke-virtual {v3}, Lcom/google/api/services/drive/model/ParentReference;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1718
    invoke-direct {p0, v0}, Lcom/samsung/scloud/GoogleDriveAPI;->parseDriveFile(Lcom/google/api/services/drive/model/File;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v5

    check-cast v5, Lcom/samsung/scloud/data/SCloudFile;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1723
    .end local v0    # "file":Lcom/google/api/services/drive/model/File;
    .end local v2    # "iterator":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lcom/google/api/services/drive/model/ParentReference;>;"
    .end local v3    # "reference":Lcom/google/api/services/drive/model/ParentReference;
    :cond_5
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    move-object v4, v6

    .line 1724
    goto :goto_0
.end method

.method private parseUserInfo(Lcom/google/api/services/drive/model/About;)Lcom/samsung/scloud/data/User;
    .locals 6
    .param p1, "userInfo"    # Lcom/google/api/services/drive/model/About;

    .prologue
    .line 1759
    if-nez p1, :cond_1

    .line 1760
    const/4 v3, 0x0

    .line 1787
    :cond_0
    return-object v3

    .line 1762
    :cond_1
    new-instance v3, Lcom/samsung/scloud/data/User;

    invoke-direct {v3}, Lcom/samsung/scloud/data/User;-><init>()V

    .line 1763
    .local v3, "user":Lcom/samsung/scloud/data/User;
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/About;->getEtag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/scloud/data/User;->setEtag(Ljava/lang/String;)V

    .line 1764
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/About;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/scloud/data/User;->setName(Ljava/lang/String;)V

    .line 1765
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/About;->getLargestChangeId()Ljava/lang/Long;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {p1}, Lcom/google/api/services/drive/model/About;->getLargestChangeId()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/scloud/data/User;->setLargestChangeId(Ljava/lang/String;)V

    .line 1766
    :cond_2
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/About;->getQuotaBytesTotal()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/samsung/scloud/data/User;->setQuotaBytesTotal(J)V

    .line 1767
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/About;->getQuotaBytesUsed()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/samsung/scloud/data/User;->setQuotaBytesUsed(J)V

    .line 1768
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/About;->getQuotaBytesUsedInTrash()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/samsung/scloud/data/User;->setQuotaBytesUsedInTrash(J)V

    .line 1769
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/About;->getRootFolderId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/scloud/data/User;->setRootFolderId(Ljava/lang/String;)V

    .line 1770
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/About;->getPermissionId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/scloud/data/User;->setPermissionId(Ljava/lang/String;)V

    .line 1771
    invoke-virtual {p1}, Lcom/google/api/services/drive/model/About;->getMaxUploadSizes()Ljava/util/List;

    move-result-object v1

    .line 1772
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/drive/model/About$MaxUploadSizes;>;"
    if-eqz v1, :cond_0

    .line 1773
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/drive/model/About$MaxUploadSizes;

    .line 1774
    .local v2, "max":Lcom/google/api/services/drive/model/About$MaxUploadSizes;
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/About$MaxUploadSizes;->getType()Ljava/lang/String;

    move-result-object v4

    const-string v5, "application/vnd.google-apps.drawing"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1775
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/About$MaxUploadSizes;->getSize()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/samsung/scloud/data/User;->setDrawingMaxUploadSize(J)V

    goto :goto_0

    .line 1776
    :cond_4
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/About$MaxUploadSizes;->getType()Ljava/lang/String;

    move-result-object v4

    const-string v5, "application/vnd.google-apps.presentation"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1777
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/About$MaxUploadSizes;->getSize()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/samsung/scloud/data/User;->setPresentationMaxUploadSize(J)V

    goto :goto_0

    .line 1778
    :cond_5
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/About$MaxUploadSizes;->getType()Ljava/lang/String;

    move-result-object v4

    const-string v5, "application/vnd.google-apps.document"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1779
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/About$MaxUploadSizes;->getSize()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/samsung/scloud/data/User;->setDocumentMaxUploadSize(J)V

    goto :goto_0

    .line 1780
    :cond_6
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/About$MaxUploadSizes;->getType()Ljava/lang/String;

    move-result-object v4

    const-string v5, "application/vnd.google-apps.spreadsheet"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1781
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/About$MaxUploadSizes;->getSize()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/samsung/scloud/data/User;->setSpreadsheetMaxUploadSize(J)V

    goto :goto_0

    .line 1782
    :cond_7
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/About$MaxUploadSizes;->getType()Ljava/lang/String;

    move-result-object v4

    const-string v5, "application/pdf"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1783
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/About$MaxUploadSizes;->getSize()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/samsung/scloud/data/User;->setPdfMaxUploadSize(J)V

    goto/16 :goto_0
.end method

.method private safeExecute(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9
    .param p1, "req"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudIOException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 2073
    if-nez p1, :cond_1

    .line 2095
    :cond_0
    :goto_0
    return-object v5

    .line 2076
    :cond_1
    const/4 v4, 0x0

    .line 2077
    .local v4, "tokenExceptionOccur":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    const/4 v6, 0x3

    if-ge v2, v6, :cond_5

    .line 2078
    const/4 v4, 0x0

    .line 2079
    const/4 v1, 0x0

    .line 2081
    .local v1, "execute":Ljava/lang/reflect/Method;
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    const-string v7, "execute"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Class;

    invoke-virtual {v6, v7, v8}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 2092
    if-eqz v1, :cond_0

    .line 2093
    const/4 v6, 0x0

    :try_start_1
    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v1, p1, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_4

    move-result-object v5

    goto :goto_0

    .line 2082
    :catch_0
    move-exception v0

    .line 2083
    .local v0, "e":Ljava/lang/SecurityException;
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    .line 2084
    new-instance v5, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getting excute() method of"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " failed [SecurityException] -"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/SecurityException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 2086
    .end local v0    # "e":Ljava/lang/SecurityException;
    :catch_1
    move-exception v0

    .line 2087
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    .line 2088
    new-instance v5, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getting excute() method of"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " failed [NoSuchMethodException] -"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 2097
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_2
    move-exception v0

    .line 2098
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 2099
    new-instance v5, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Executing excute() method of"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " failed [IllegalArgumentException] -"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 2101
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v0

    .line 2102
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    .line 2103
    new-instance v5, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Executing excute() method of"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " failed [IllegalAccessException] -"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 2105
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v0

    .line 2106
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    .line 2107
    .local v3, "t":Ljava/lang/Throwable;
    instance-of v6, v3, Ljavax/net/ssl/SSLException;

    if-eqz v6, :cond_2

    .line 2108
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    .line 2109
    const-string v6, "SCLOUD_SDK"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[safeExcute] - handle SSL Exception(retry #"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") causeMsg="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2110
    const-wide/16 v6, 0x7d0

    invoke-direct {p0, v6, v7}, Lcom/samsung/scloud/GoogleDriveAPI;->someSleep(J)V

    .line 2077
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 2113
    :cond_2
    invoke-direct {p0, v3}, Lcom/samsung/scloud/GoogleDriveAPI;->checkForAuthException(Ljava/lang/Throwable;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2114
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    .line 2115
    const/4 v4, 0x1

    .line 2116
    invoke-virtual {p0}, Lcom/samsung/scloud/GoogleDriveAPI;->handleTokenExpire()V

    .line 2117
    invoke-direct {p0}, Lcom/samsung/scloud/GoogleDriveAPI;->loadToken()V

    .line 2118
    const-string v6, "SCLOUD_SDK"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[safeExcute] - handle Auth Exception(retry #"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") causeMsg="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 2120
    :cond_3
    instance-of v5, v3, Ljava/io/IOException;

    if-eqz v5, :cond_4

    .line 2121
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    .line 2122
    new-instance v5, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException occured during executing "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " exception="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 2125
    :cond_4
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    .line 2126
    new-instance v5, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Other(not IO) Exception occured during executing "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " exception="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 2131
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    .end local v1    # "execute":Ljava/lang/reflect/Method;
    .end local v3    # "t":Ljava/lang/Throwable;
    :cond_5
    if-eqz v4, :cond_6

    new-instance v5, Lcom/samsung/scloud/exception/SCloudAuthException;

    const-string v6, "server token expired"

    invoke-direct {v5, v6}, Lcom/samsung/scloud/exception/SCloudAuthException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 2132
    :cond_6
    new-instance v5, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v6, "[safeExcute] - IOException"

    invoke-direct {v5, v6}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v5
.end method

.method private someSleep(J)V
    .locals 1
    .param p1, "ms"    # J

    .prologue
    .line 2052
    :try_start_0
    invoke-static {p1, p2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2056
    :goto_0
    return-void

    .line 2053
    :catch_0
    move-exception v0

    .line 2054
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method private throwIOorAuthException(Ljava/lang/Exception;Ljava/lang/String;)V
    .locals 3
    .param p1, "e"    # Ljava/lang/Exception;
    .param p2, "cause"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudIOException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;
        }
    .end annotation

    .prologue
    .line 1791
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 1792
    instance-of v0, p1, Lcom/samsung/scloud/exception/SCloudIOException;

    if-eqz v0, :cond_0

    .line 1793
    new-instance v0, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1794
    :cond_0
    instance-of v0, p1, Lcom/samsung/scloud/exception/SCloudAuthException;

    if-eqz v0, :cond_1

    .line 1795
    new-instance v0, Lcom/samsung/scloud/exception/SCloudAuthException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudAuthException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1797
    :cond_1
    new-instance v0, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public addComment(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/Comment;
    .locals 1
    .param p1, "Id"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2397
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public copy(Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/scloud/data/SCloudNode;
    .locals 8
    .param p1, "fromId"    # Ljava/lang/String;
    .param p2, "toId"    # Ljava/lang/String;
    .param p3, "overwrite"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudIOException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 317
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    if-lt v6, v7, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v6, v7, :cond_1

    .line 318
    :cond_0
    new-instance v6, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v6}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v6

    .line 320
    :cond_1
    invoke-direct {p0}, Lcom/samsung/scloud/GoogleDriveAPI;->loadToken()V

    .line 323
    const/4 v2, 0x0

    .line 324
    .local v2, "copiedFile":Lcom/google/api/services/drive/model/File;
    new-instance v1, Lcom/google/api/services/drive/model/File;

    invoke-direct {v1}, Lcom/google/api/services/drive/model/File;-><init>()V

    .line 326
    .local v1, "content":Lcom/google/api/services/drive/model/File;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 327
    .local v5, "parentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/api/services/drive/model/ParentReference;>;"
    new-instance v6, Lcom/google/api/services/drive/model/ParentReference;

    invoke-direct {v6}, Lcom/google/api/services/drive/model/ParentReference;-><init>()V

    invoke-virtual {v6, p2}, Lcom/google/api/services/drive/model/ParentReference;->setId(Ljava/lang/String;)Lcom/google/api/services/drive/model/ParentReference;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 328
    invoke-virtual {v1, v5}, Lcom/google/api/services/drive/model/File;->setParents(Ljava/util/List;)Lcom/google/api/services/drive/model/File;

    .line 331
    :try_start_0
    iget-object v6, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v6}, Lcom/google/api/services/drive/Drive;->files()Lcom/google/api/services/drive/Drive$Files;

    move-result-object v6

    invoke-virtual {v6, p1, v1}, Lcom/google/api/services/drive/Drive$Files;->copy(Ljava/lang/String;Lcom/google/api/services/drive/model/File;)Lcom/google/api/services/drive/Drive$Files$Copy;

    move-result-object v3

    .line 332
    .local v3, "copyRequest":Lcom/google/api/services/drive/Drive$Files$Copy;
    invoke-direct {p0, v3}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lcom/google/api/services/drive/model/File;

    move-object v2, v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 337
    .end local v3    # "copyRequest":Lcom/google/api/services/drive/Drive$Files$Copy;
    :goto_0
    invoke-direct {p0, v2}, Lcom/samsung/scloud/GoogleDriveAPI;->parseDriveFile(Lcom/google/api/services/drive/model/File;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v6

    return-object v6

    .line 333
    :catch_0
    move-exception v4

    .line 334
    .local v4, "e1":Ljava/io/IOException;
    const-string v6, "copy - file copy failed"

    invoke-direct {p0, v4, v6}, Lcom/samsung/scloud/GoogleDriveAPI;->throwIOorAuthException(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public createRepository(Lcom/samsung/scloud/data/Repository;)Lcom/samsung/scloud/data/Repository;
    .locals 1
    .param p1, "repo"    # Lcom/samsung/scloud/data/Repository;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2220
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public createRepository(Ljava/lang/String;J)Lcom/samsung/scloud/data/Repository;
    .locals 1
    .param p1, "repositoryName"    # Ljava/lang/String;
    .param p2, "spaceAmount"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2212
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public createShareURL(Ljava/lang/String;Ljava/lang/String;J)Lcom/samsung/scloud/data/ShareInfo;
    .locals 1
    .param p1, "Id"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .param p3, "endTimestamp"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2413
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public declared-synchronized delete(Ljava/lang/String;)Z
    .locals 4
    .param p1, "fileId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudIOException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 444
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v3, v2, :cond_1

    .line 445
    :cond_0
    new-instance v2, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v2}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 444
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 447
    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/samsung/scloud/GoogleDriveAPI;->loadToken()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 451
    :try_start_2
    iget-object v3, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v3}, Lcom/google/api/services/drive/Drive;->files()Lcom/google/api/services/drive/Drive$Files;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/api/services/drive/Drive$Files;->trash(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$Trash;

    move-result-object v1

    .line 452
    .local v1, "trashRequest":Lcom/google/api/services/drive/Drive$Files$Trash;
    invoke-direct {p0, v1}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 457
    .end local v1    # "trashRequest":Lcom/google/api/services/drive/Drive$Files$Trash;
    :goto_0
    monitor-exit p0

    return v2

    .line 454
    :catch_0
    move-exception v0

    .line 455
    .local v0, "e":Ljava/io/IOException;
    :try_start_3
    const-string v2, "delete - deleting file failed"

    invoke-direct {p0, v0, v2}, Lcom/samsung/scloud/GoogleDriveAPI;->throwIOorAuthException(Ljava/lang/Exception;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 457
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public deleteDeviceProfile()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2204
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public deleteRepository(Ljava/lang/String;)V
    .locals 1
    .param p1, "repositoryName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2260
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public editComment(Ljava/lang/String;JLjava/lang/String;)Lcom/samsung/scloud/data/Comment;
    .locals 1
    .param p1, "Id"    # Ljava/lang/String;
    .param p2, "commentID"    # J
    .param p4, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2389
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public enableLogging()V
    .locals 2

    .prologue
    .line 1682
    const-class v1, Lcom/google/api/client/http/HttpTransport;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    .line 1683
    .local v0, "logger":Ljava/util/logging/Logger;
    sget-object v1, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->setLevel(Ljava/util/logging/Level;)V

    .line 1684
    new-instance v1, Lcom/samsung/scloud/GoogleDriveAPI$6;

    invoke-direct {v1, p0}, Lcom/samsung/scloud/GoogleDriveAPI$6;-><init>(Lcom/samsung/scloud/GoogleDriveAPI;)V

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->addHandler(Ljava/util/logging/Handler;)V

    .line 1696
    return-void
.end method

.method public finishAuth()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2292
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getAuthToken()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 1341
    iget-object v0, p0, Lcom/samsung/scloud/GoogleDriveAPI;->accessToken:Ljava/lang/String;

    return-object v0
.end method

.method public getCollaboration(Ljava/lang/String;)Lcom/samsung/scloud/data/Collaboration;
    .locals 5
    .param p1, "Id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudIOException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;
        }
    .end annotation

    .prologue
    .line 1061
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x1

    if-ge v3, v4, :cond_1

    .line 1062
    :cond_0
    new-instance v3, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v3}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v3

    .line 1064
    :cond_1
    invoke-direct {p0}, Lcom/samsung/scloud/GoogleDriveAPI;->loadToken()V

    .line 1066
    const/4 v2, 0x0

    .line 1068
    .local v2, "result":Lcom/google/api/services/drive/model/PermissionList;
    :try_start_0
    iget-object v3, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v3}, Lcom/google/api/services/drive/Drive;->permissions()Lcom/google/api/services/drive/Drive$Permissions;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/api/services/drive/Drive$Permissions;->list(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Permissions$List;

    move-result-object v1

    .line 1069
    .local v1, "permissionList":Lcom/google/api/services/drive/Drive$Permissions$List;
    invoke-direct {p0, v1}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "result":Lcom/google/api/services/drive/model/PermissionList;
    check-cast v2, Lcom/google/api/services/drive/model/PermissionList;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1074
    .restart local v2    # "result":Lcom/google/api/services/drive/model/PermissionList;
    invoke-direct {p0, v2}, Lcom/samsung/scloud/GoogleDriveAPI;->parsePermissionFeed(Lcom/google/api/services/drive/model/PermissionList;)Lcom/samsung/scloud/data/Collaboration;

    move-result-object v3

    return-object v3

    .line 1070
    .end local v1    # "permissionList":Lcom/google/api/services/drive/Drive$Permissions$List;
    .end local v2    # "result":Lcom/google/api/services/drive/model/PermissionList;
    :catch_0
    move-exception v0

    .line 1071
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 1072
    new-instance v3, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v4, "getCollaboration - getting Collaboration failed.."

    invoke-direct {v3, v4}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public getCollaborations()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Collaboration;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2268
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getComments(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "Id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Comment;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2380
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getDeltaList(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;
    .locals 1
    .param p1, "cursor"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/scloud/data/SCloudNodeList",
            "<",
            "Lcom/samsung/scloud/data/SCloudNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2461
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getDeviceProfile()Lcom/samsung/scloud/data/Device;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2172
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getDeviceProfile(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2180
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getDownloadURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2453
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/scloud/response/ProgressListener;)V
    .locals 20
    .param p1, "localFile"    # Ljava/io/File;
    .param p2, "serverFileId"    # Ljava/lang/String;
    .param p3, "version"    # Ljava/lang/String;
    .param p4, "overwrite"    # Z
    .param p5, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudIOException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;
        }
    .end annotation

    .prologue
    .line 781
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v16

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-lt v0, v1, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v16

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-ge v0, v1, :cond_1

    .line 783
    :cond_0
    new-instance v16, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct/range {v16 .. v16}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v16

    .line 785
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/samsung/scloud/GoogleDriveAPI;->loadToken()V

    .line 787
    const/4 v11, 0x0

    .line 790
    .local v11, "fileOut":Ljava/io/FileOutputStream;
    const/4 v15, 0x0

    .line 792
    .local v15, "revision":Lcom/google/api/services/drive/model/Revision;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/google/api/services/drive/Drive;->revisions()Lcom/google/api/services/drive/Drive$Revisions;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/google/api/services/drive/Drive$Revisions;->get(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Revisions$Get;

    move-result-object v14

    .line 793
    .local v14, "request":Lcom/google/api/services/drive/Drive$Revisions$Get;
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    .end local v15    # "revision":Lcom/google/api/services/drive/model/Revision;
    check-cast v15, Lcom/google/api/services/drive/model/Revision;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 800
    .restart local v15    # "revision":Lcom/google/api/services/drive/model/Revision;
    if-eqz v15, :cond_4

    invoke-virtual {v15}, Lcom/google/api/services/drive/model/Revision;->getDownloadUrl()Ljava/lang/String;

    move-result-object v16

    if-eqz v16, :cond_4

    invoke-virtual {v15}, Lcom/google/api/services/drive/model/Revision;->getDownloadUrl()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v16

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-le v0, v1, :cond_4

    .line 802
    const/4 v4, 0x0

    .line 803
    .local v4, "downloadFile":Lcom/google/api/services/drive/model/File;
    const/4 v5, 0x0

    .line 805
    .local v5, "downloadRequest":Lcom/google/api/services/drive/Drive$Files$Get;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/google/api/services/drive/Drive;->files()Lcom/google/api/services/drive/Drive$Files;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/api/services/drive/Drive$Files;->get(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$Get;

    move-result-object v5

    .line 806
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    move-object/from16 v0, v16

    check-cast v0, Lcom/google/api/services/drive/model/File;

    move-object v4, v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 812
    :goto_0
    if-eqz v4, :cond_4

    .line 813
    const-wide/16 v6, 0x0

    .line 814
    .local v6, "downloadOffset":J
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->exists()Z

    move-result v16

    if-eqz v16, :cond_2

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v16

    invoke-virtual {v4}, Lcom/google/api/services/drive/model/File;->getFileSize()Ljava/lang/Long;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    cmp-long v16, v16, v18

    if-nez v16, :cond_2

    if-nez p4, :cond_2

    .line 815
    new-instance v16, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    const-string v17, "getFile - File already exists,if you still wish to download please set overwrite to true"

    invoke-direct/range {v16 .. v17}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v16

    .line 794
    .end local v4    # "downloadFile":Lcom/google/api/services/drive/model/File;
    .end local v5    # "downloadRequest":Lcom/google/api/services/drive/Drive$Files$Get;
    .end local v6    # "downloadOffset":J
    .end local v14    # "request":Lcom/google/api/services/drive/Drive$Revisions$Get;
    .end local v15    # "revision":Lcom/google/api/services/drive/model/Revision;
    :catch_0
    move-exception v8

    .line 795
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    .line 796
    new-instance v16, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "getFile - Getting download url failed"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v8}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v16 .. v17}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v16

    .line 807
    .end local v8    # "e":Ljava/io/IOException;
    .restart local v4    # "downloadFile":Lcom/google/api/services/drive/model/File;
    .restart local v5    # "downloadRequest":Lcom/google/api/services/drive/Drive$Files$Get;
    .restart local v14    # "request":Lcom/google/api/services/drive/Drive$Revisions$Get;
    .restart local v15    # "revision":Lcom/google/api/services/drive/model/Revision;
    :catch_1
    move-exception v10

    .line 808
    .local v10, "e2":Ljava/io/IOException;
    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    .line 809
    const-string v16, "getFile - Getting download url failed"

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v10, v1}, Lcom/samsung/scloud/GoogleDriveAPI;->throwIOorAuthException(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0

    .line 816
    .end local v10    # "e2":Ljava/io/IOException;
    .restart local v6    # "downloadOffset":J
    :cond_2
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->exists()Z

    move-result v16

    if-eqz v16, :cond_3

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v16

    invoke-virtual {v4}, Lcom/google/api/services/drive/model/File;->getFileSize()Ljava/lang/Long;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    cmp-long v16, v16, v18

    if-eqz v16, :cond_3

    .line 817
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v6

    .line 819
    :cond_3
    const/4 v13, 0x0

    .local v13, "i":I
    move-object v12, v11

    .end local v11    # "fileOut":Ljava/io/FileOutputStream;
    .local v12, "fileOut":Ljava/io/FileOutputStream;
    :goto_1
    const/16 v16, 0x3

    move/from16 v0, v16

    if-ge v13, v0, :cond_a

    .line 821
    const-wide/16 v16, 0x0

    cmp-long v16, v6, v16

    if-lez v16, :cond_5

    .line 822
    :try_start_2
    new-instance v11, Ljava/io/FileOutputStream;

    const/16 v16, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-direct {v11, v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 825
    .end local v12    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v11    # "fileOut":Ljava/io/FileOutputStream;
    :goto_2
    :try_start_3
    invoke-virtual {v5}, Lcom/google/api/services/drive/Drive$Files$Get;->getMediaHttpDownloader()Lcom/google/api/client/googleapis/media/MediaHttpDownloader;

    move-result-object v16

    new-instance v17, Lcom/samsung/scloud/GoogleDriveAPI$4;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    move-object/from16 v2, p5

    move-object/from16 v3, p1

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/scloud/GoogleDriveAPI$4;-><init>(Lcom/samsung/scloud/GoogleDriveAPI;Lcom/samsung/scloud/response/ProgressListener;Ljava/io/File;)V

    invoke-virtual/range {v16 .. v17}, Lcom/google/api/client/googleapis/media/MediaHttpDownloader;->setProgressListener(Lcom/google/api/client/googleapis/media/MediaHttpDownloaderProgressListener;)Lcom/google/api/client/googleapis/media/MediaHttpDownloader;

    move-result-object v16

    const/high16 v17, 0x280000

    invoke-virtual/range {v16 .. v17}, Lcom/google/api/client/googleapis/media/MediaHttpDownloader;->setChunkSize(I)Lcom/google/api/client/googleapis/media/MediaHttpDownloader;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v6, v7}, Lcom/google/api/client/googleapis/media/MediaHttpDownloader;->setBytesDownloaded(J)Lcom/google/api/client/googleapis/media/MediaHttpDownloader;

    move-result-object v16

    new-instance v17, Lcom/google/api/client/http/GenericUrl;

    invoke-virtual {v15}, Lcom/google/api/services/drive/model/Revision;->getDownloadUrl()Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Lcom/google/api/client/http/GenericUrl;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v11}, Lcom/google/api/client/googleapis/media/MediaHttpDownloader;->download(Lcom/google/api/client/http/GenericUrl;Ljava/io/OutputStream;)V

    .line 841
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5

    .line 874
    .end local v4    # "downloadFile":Lcom/google/api/services/drive/model/File;
    .end local v5    # "downloadRequest":Lcom/google/api/services/drive/Drive$Files$Get;
    .end local v6    # "downloadOffset":J
    .end local v13    # "i":I
    :cond_4
    :goto_3
    return-void

    .line 824
    .end local v11    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v4    # "downloadFile":Lcom/google/api/services/drive/model/File;
    .restart local v5    # "downloadRequest":Lcom/google/api/services/drive/Drive$Files$Get;
    .restart local v6    # "downloadOffset":J
    .restart local v12    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v13    # "i":I
    :cond_5
    :try_start_4
    new-instance v11, Ljava/io/FileOutputStream;

    move-object/from16 v0, p1

    invoke-direct {v11, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .end local v12    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v11    # "fileOut":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 843
    .end local v11    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v12    # "fileOut":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v8

    move-object v11, v12

    .line 844
    .end local v12    # "fileOut":Ljava/io/FileOutputStream;
    .local v8, "e":Ljava/io/FileNotFoundException;
    .restart local v11    # "fileOut":Ljava/io/FileOutputStream;
    :goto_4
    invoke-virtual {v8}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 845
    new-instance v16, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "getFile - Input file not found "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v8}, Ljava/io/FileNotFoundException;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v16 .. v17}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v16

    .line 846
    .end local v8    # "e":Ljava/io/FileNotFoundException;
    .end local v11    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v12    # "fileOut":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v8

    move-object v11, v12

    .line 848
    .end local v12    # "fileOut":Ljava/io/FileOutputStream;
    .local v8, "e":Ljava/io/IOException;
    .restart local v11    # "fileOut":Ljava/io/FileOutputStream;
    :goto_5
    :try_start_5
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 852
    :goto_6
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/samsung/scloud/GoogleDriveAPI;->checkForAuthException(Ljava/lang/Throwable;)Z

    move-result v16

    if-eqz v16, :cond_7

    .line 853
    const/16 v16, 0x2

    move/from16 v0, v16

    if-ge v13, v0, :cond_6

    .line 854
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/scloud/GoogleDriveAPI;->handleTokenExpire()V

    .line 819
    :goto_7
    add-int/lit8 v13, v13, 0x1

    move-object v12, v11

    .end local v11    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v12    # "fileOut":Ljava/io/FileOutputStream;
    goto/16 :goto_1

    .line 849
    .end local v12    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v11    # "fileOut":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v9

    .line 850
    .local v9, "e1":Ljava/io/IOException;
    const-string v16, "getThumbnail - closing fileOutputStream failed"

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v9, v1}, Lcom/samsung/scloud/GoogleDriveAPI;->throwIOorAuthException(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_6

    .line 857
    .end local v9    # "e1":Ljava/io/IOException;
    :cond_6
    new-instance v16, Lcom/samsung/scloud/exception/SCloudAuthException;

    const-string v17, "server token expired"

    invoke-direct/range {v16 .. v17}, Lcom/samsung/scloud/exception/SCloudAuthException;-><init>(Ljava/lang/String;)V

    throw v16

    .line 859
    :cond_7
    instance-of v0, v8, Ljavax/net/ssl/SSLException;

    move/from16 v16, v0

    if-eqz v16, :cond_9

    .line 860
    const/16 v16, 0x2

    move/from16 v0, v16

    if-ge v13, v0, :cond_8

    .line 861
    const-wide/16 v16, 0x7d0

    move-object/from16 v0, p0

    move-wide/from16 v1, v16

    invoke-direct {v0, v1, v2}, Lcom/samsung/scloud/GoogleDriveAPI;->someSleep(J)V

    goto :goto_7

    .line 864
    :cond_8
    new-instance v16, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "getFile - file download failed  "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v8}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v16 .. v17}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v16

    .line 867
    :cond_9
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    .line 868
    new-instance v16, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "getFile - file download failed  "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual {v8}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v16 .. v17}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v16

    .line 846
    .end local v8    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v8

    goto :goto_5

    .line 843
    :catch_6
    move-exception v8

    goto/16 :goto_4

    .end local v11    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v12    # "fileOut":Ljava/io/FileOutputStream;
    :cond_a
    move-object v11, v12

    .end local v12    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v11    # "fileOut":Ljava/io/FileOutputStream;
    goto/16 :goto_3
.end method

.method public getFile(Ljava/io/File;Ljava/lang/String;ZLcom/samsung/scloud/response/ProgressListener;)V
    .locals 18
    .param p1, "localFile"    # Ljava/io/File;
    .param p2, "serverFileId"    # Ljava/lang/String;
    .param p3, "overwrite"    # Z
    .param p4, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudIOException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;
        }
    .end annotation

    .prologue
    .line 680
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v13

    const/4 v14, 0x1

    if-ge v13, v14, :cond_1

    .line 681
    :cond_0
    new-instance v13, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v13}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v13

    .line 683
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/samsung/scloud/GoogleDriveAPI;->loadToken()V

    .line 686
    const/4 v11, 0x0

    .line 687
    .local v11, "getRequest":Lcom/google/api/services/drive/Drive$Files$Get;
    const/4 v4, 0x0

    .line 688
    .local v4, "downloadFile":Lcom/google/api/services/drive/model/File;
    const/4 v9, 0x0

    .line 690
    .local v9, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v13}, Lcom/google/api/services/drive/Drive;->files()Lcom/google/api/services/drive/Drive$Files;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Lcom/google/api/services/drive/Drive$Files;->get(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$Get;

    move-result-object v11

    .line 691
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    move-object v0, v13

    check-cast v0, Lcom/google/api/services/drive/model/File;

    move-object v4, v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 697
    :goto_0
    if-eqz v4, :cond_4

    .line 698
    const-wide/16 v6, 0x0

    .line 700
    .local v6, "downloadOffset":J
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->exists()Z

    move-result v13

    if-eqz v13, :cond_2

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v14

    invoke-virtual {v4}, Lcom/google/api/services/drive/model/File;->getFileSize()Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    cmp-long v13, v14, v16

    if-nez v13, :cond_2

    if-nez p3, :cond_2

    .line 701
    new-instance v13, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    const-string v14, "getFile - File already exists,if you still wish to download please set overwrite to true"

    invoke-direct {v13, v14}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 692
    .end local v6    # "downloadOffset":J
    :catch_0
    move-exception v5

    .line 693
    .local v5, "e":Ljava/io/IOException;
    const-string v13, "getFile - Getting download url failed"

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v13}, Lcom/samsung/scloud/GoogleDriveAPI;->throwIOorAuthException(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0

    .line 702
    .end local v5    # "e":Ljava/io/IOException;
    .restart local v6    # "downloadOffset":J
    :cond_2
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->exists()Z

    move-result v13

    if-eqz v13, :cond_3

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v14

    invoke-virtual {v4}, Lcom/google/api/services/drive/model/File;->getFileSize()Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    cmp-long v13, v14, v16

    if-eqz v13, :cond_3

    .line 703
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v6

    .line 706
    :cond_3
    const/4 v12, 0x0

    .local v12, "i":I
    move-object v10, v9

    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .local v10, "fos":Ljava/io/FileOutputStream;
    :goto_1
    const/4 v13, 0x3

    if-ge v12, v13, :cond_a

    .line 708
    const-wide/16 v14, 0x0

    cmp-long v13, v6, v14

    if-lez v13, :cond_5

    .line 709
    :try_start_1
    new-instance v9, Ljava/io/FileOutputStream;

    const/4 v13, 0x1

    move-object/from16 v0, p1

    invoke-direct {v9, v0, v13}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 712
    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    :goto_2
    :try_start_2
    invoke-virtual {v11}, Lcom/google/api/services/drive/Drive$Files$Get;->getMediaHttpDownloader()Lcom/google/api/client/googleapis/media/MediaHttpDownloader;

    move-result-object v13

    new-instance v14, Lcom/samsung/scloud/GoogleDriveAPI$3;

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, p1

    invoke-direct {v14, v0, v1, v2}, Lcom/samsung/scloud/GoogleDriveAPI$3;-><init>(Lcom/samsung/scloud/GoogleDriveAPI;Lcom/samsung/scloud/response/ProgressListener;Ljava/io/File;)V

    invoke-virtual {v13, v14}, Lcom/google/api/client/googleapis/media/MediaHttpDownloader;->setProgressListener(Lcom/google/api/client/googleapis/media/MediaHttpDownloaderProgressListener;)Lcom/google/api/client/googleapis/media/MediaHttpDownloader;

    move-result-object v13

    const/high16 v14, 0x280000

    invoke-virtual {v13, v14}, Lcom/google/api/client/googleapis/media/MediaHttpDownloader;->setChunkSize(I)Lcom/google/api/client/googleapis/media/MediaHttpDownloader;

    move-result-object v13

    invoke-virtual {v13, v6, v7}, Lcom/google/api/client/googleapis/media/MediaHttpDownloader;->setBytesDownloaded(J)Lcom/google/api/client/googleapis/media/MediaHttpDownloader;

    move-result-object v13

    new-instance v14, Lcom/google/api/client/http/GenericUrl;

    invoke-virtual {v4}, Lcom/google/api/services/drive/model/File;->getDownloadUrl()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Lcom/google/api/client/http/GenericUrl;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v14, v9}, Lcom/google/api/client/googleapis/media/MediaHttpDownloader;->download(Lcom/google/api/client/http/GenericUrl;Ljava/io/OutputStream;)V

    .line 730
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    .line 762
    .end local v6    # "downloadOffset":J
    .end local v12    # "i":I
    :cond_4
    :goto_3
    return-void

    .line 711
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "downloadOffset":J
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v12    # "i":I
    :cond_5
    :try_start_3
    new-instance v9, Ljava/io/FileOutputStream;

    move-object/from16 v0, p1

    invoke-direct {v9, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    goto :goto_2

    .line 732
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v5

    move-object v9, v10

    .line 733
    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .local v5, "e":Ljava/io/FileNotFoundException;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    :goto_4
    invoke-virtual {v5}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 734
    new-instance v13, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "getFile - Input file not found "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v5}, Ljava/io/FileNotFoundException;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 735
    .end local v5    # "e":Ljava/io/FileNotFoundException;
    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v5

    move-object v9, v10

    .line 737
    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .local v5, "e":Ljava/io/IOException;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    :goto_5
    :try_start_4
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 741
    :goto_6
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/samsung/scloud/GoogleDriveAPI;->checkForAuthException(Ljava/lang/Throwable;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 742
    const/4 v13, 0x2

    if-ge v12, v13, :cond_6

    .line 743
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/scloud/GoogleDriveAPI;->handleTokenExpire()V

    .line 706
    :goto_7
    add-int/lit8 v12, v12, 0x1

    move-object v10, v9

    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    goto :goto_1

    .line 738
    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v8

    .line 739
    .local v8, "e1":Ljava/io/IOException;
    const-string v13, "getThumbnail - closing fileOutputStream failed"

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v13}, Lcom/samsung/scloud/GoogleDriveAPI;->throwIOorAuthException(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_6

    .line 746
    .end local v8    # "e1":Ljava/io/IOException;
    :cond_6
    new-instance v13, Lcom/samsung/scloud/exception/SCloudAuthException;

    const-string v14, "server token expired"

    invoke-direct {v13, v14}, Lcom/samsung/scloud/exception/SCloudAuthException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 748
    :cond_7
    instance-of v13, v5, Ljavax/net/ssl/SSLException;

    if-eqz v13, :cond_9

    .line 749
    const/4 v13, 0x2

    if-ge v12, v13, :cond_8

    .line 750
    const-wide/16 v14, 0x7d0

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/samsung/scloud/GoogleDriveAPI;->someSleep(J)V

    goto :goto_7

    .line 753
    :cond_8
    new-instance v13, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "getFile - file download failed  "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v5}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 756
    :cond_9
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    .line 757
    new-instance v13, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "getFile - file download failed  "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v5}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 735
    .end local v5    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v5

    goto :goto_5

    .line 732
    :catch_5
    move-exception v5

    goto/16 :goto_4

    .end local v9    # "fos":Ljava/io/FileOutputStream;
    .restart local v10    # "fos":Ljava/io/FileOutputStream;
    :cond_a
    move-object v9, v10

    .end local v10    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "fos":Ljava/io/FileOutputStream;
    goto/16 :goto_3
.end method

.method public getFileInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 6
    .param p1, "fileId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudIOException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;
        }
    .end annotation

    .prologue
    .line 288
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x1

    if-ge v4, v5, :cond_1

    .line 289
    :cond_0
    new-instance v4, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v4}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v4

    .line 291
    :cond_1
    invoke-direct {p0}, Lcom/samsung/scloud/GoogleDriveAPI;->loadToken()V

    .line 293
    const/4 v2, 0x0

    .line 295
    .local v2, "fileMeta":Lcom/google/api/services/drive/model/File;
    :try_start_0
    iget-object v4, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v4}, Lcom/google/api/services/drive/Drive;->files()Lcom/google/api/services/drive/Drive$Files;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/google/api/services/drive/Drive$Files;->get(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$Get;

    move-result-object v3

    .line 296
    .local v3, "getFileRequest":Lcom/google/api/services/drive/Drive$Files$Get;
    invoke-direct {p0, v3}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/google/api/services/drive/model/File;

    move-object v2, v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 301
    .end local v3    # "getFileRequest":Lcom/google/api/services/drive/Drive$Files$Get;
    :goto_0
    invoke-direct {p0, v2}, Lcom/samsung/scloud/GoogleDriveAPI;->parseDriveFile(Lcom/google/api/services/drive/model/File;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v4

    check-cast v4, Lcom/samsung/scloud/data/SCloudFile;

    return-object v4

    .line 297
    :catch_0
    move-exception v1

    .line 298
    .local v1, "e":Ljava/io/IOException;
    const-string v4, "getFileInfo failed"

    invoke-direct {p0, v1, v4}, Lcom/samsung/scloud/GoogleDriveAPI;->throwIOorAuthException(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getFileInfo(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "folderId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/scloud/data/SCloudFile;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 1315
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-lt v4, v5, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v4, v5, :cond_1

    .line 1316
    :cond_0
    new-instance v4, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v4}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v4

    .line 1318
    :cond_1
    invoke-direct {p0}, Lcom/samsung/scloud/GoogleDriveAPI;->loadToken()V

    .line 1325
    :try_start_0
    iget-object v4, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v4}, Lcom/google/api/services/drive/Drive;->files()Lcom/google/api/services/drive/Drive$Files;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/api/services/drive/Drive$Files;->list()Lcom/google/api/services/drive/Drive$Files$List;

    move-result-object v1

    .line 1326
    .local v1, "get":Lcom/google/api/services/drive/Drive$Files$List;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "title=\'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1327
    .local v3, "searchParam":Ljava/lang/StringBuilder;
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/api/services/drive/Drive$Files$List;->setQ(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$List;

    .line 1328
    invoke-direct {p0, v1}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/drive/model/FileList;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1333
    .local v2, "searchList":Lcom/google/api/services/drive/model/FileList;
    invoke-direct {p0, v2, p2}, Lcom/samsung/scloud/GoogleDriveAPI;->parseSearchList(Lcom/google/api/services/drive/model/FileList;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    return-object v4

    .line 1329
    .end local v1    # "get":Lcom/google/api/services/drive/Drive$Files$List;
    .end local v2    # "searchList":Lcom/google/api/services/drive/model/FileList;
    .end local v3    # "searchParam":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v0

    .line 1330
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 1331
    new-instance v4, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v5, "getFileInfo - Failed to search files"

    invoke-direct {v4, v5}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public getFilesAsZip(Ljava/io/File;Ljava/util/List;ZLcom/samsung/scloud/response/ProgressListener;)V
    .locals 1
    .param p1, "localFile"    # Ljava/io/File;
    .param p3, "overwrite"    # Z
    .param p4, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z",
            "Lcom/samsung/scloud/response/ProgressListener;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2276
    .local p2, "Id":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getFolderInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;
    .locals 6
    .param p1, "folderId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudIOException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;
        }
    .end annotation

    .prologue
    .line 223
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x1

    if-ge v4, v5, :cond_1

    .line 224
    :cond_0
    new-instance v4, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v4}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v4

    .line 226
    :cond_1
    invoke-direct {p0}, Lcom/samsung/scloud/GoogleDriveAPI;->loadToken()V

    .line 227
    const/4 v2, 0x0

    .line 229
    .local v2, "folderMeta":Lcom/google/api/services/drive/model/File;
    :try_start_0
    iget-object v4, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v4}, Lcom/google/api/services/drive/Drive;->files()Lcom/google/api/services/drive/Drive$Files;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/google/api/services/drive/Drive$Files;->get(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$Get;

    move-result-object v3

    .line 230
    .local v3, "getFileRequest":Lcom/google/api/services/drive/Drive$Files$Get;
    invoke-direct {p0, v3}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/google/api/services/drive/model/File;

    move-object v2, v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 235
    .end local v3    # "getFileRequest":Lcom/google/api/services/drive/Drive$Files$Get;
    :goto_0
    invoke-direct {p0, v2}, Lcom/samsung/scloud/GoogleDriveAPI;->parseDriveFile(Lcom/google/api/services/drive/model/File;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v4

    check-cast v4, Lcom/samsung/scloud/data/SCloudFolder;

    return-object v4

    .line 231
    :catch_0
    move-exception v1

    .line 232
    .local v1, "e":Ljava/io/IOException;
    const-string v4, "getFolderInfo failed"

    invoke-direct {p0, v1, v4}, Lcom/samsung/scloud/GoogleDriveAPI;->throwIOorAuthException(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getList(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;
    .locals 9
    .param p1, "folderId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudIOException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;
        }
    .end annotation

    .prologue
    .line 249
    const/4 v5, 0x0

    .line 250
    .local v5, "result":Lcom/samsung/scloud/data/SearchResult;
    const/4 v3, 0x0

    .line 251
    .local v3, "nextToken":Ljava/lang/String;
    const/4 v6, 0x0

    .line 252
    .local v6, "ret":Lcom/samsung/scloud/data/SCloudFolder;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 253
    .local v0, "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/SCloudFile;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 255
    .local v1, "folders":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/SCloudFolder;>;"
    :cond_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "\'"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\' in parents"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x1f4

    invoke-virtual {p0, v7, v8, v3}, Lcom/samsung/scloud/GoogleDriveAPI;->search(Ljava/lang/String;ILjava/lang/String;)Lcom/samsung/scloud/data/SearchResult;

    move-result-object v5

    .line 256
    if-nez v5, :cond_1

    if-nez v6, :cond_1

    const/4 v7, 0x0

    .line 271
    :goto_0
    return-object v7

    .line 257
    :cond_1
    if-nez v5, :cond_3

    .line 269
    :cond_2
    :goto_1
    invoke-virtual {v6, v0}, Lcom/samsung/scloud/data/SCloudFolder;->setFiles(Ljava/util/ArrayList;)V

    .line 270
    invoke-virtual {v6, v1}, Lcom/samsung/scloud/data/SCloudFolder;->setFolders(Ljava/util/ArrayList;)V

    move-object v7, v6

    .line 271
    goto :goto_0

    .line 258
    :cond_3
    if-nez v6, :cond_4

    new-instance v6, Lcom/samsung/scloud/data/SCloudFolder;

    .end local v6    # "ret":Lcom/samsung/scloud/data/SCloudFolder;
    invoke-direct {v6}, Lcom/samsung/scloud/data/SCloudFolder;-><init>()V

    .line 259
    .restart local v6    # "ret":Lcom/samsung/scloud/data/SCloudFolder;
    :cond_4
    invoke-virtual {v5}, Lcom/samsung/scloud/data/SearchResult;->getSearchResults()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/scloud/data/SCloudNode;

    .line 260
    .local v4, "node":Lcom/samsung/scloud/data/SCloudNode;
    instance-of v7, v4, Lcom/samsung/scloud/data/SCloudFile;

    if-eqz v7, :cond_6

    .line 261
    check-cast v4, Lcom/samsung/scloud/data/SCloudFile;

    .end local v4    # "node":Lcom/samsung/scloud/data/SCloudNode;
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 262
    .restart local v4    # "node":Lcom/samsung/scloud/data/SCloudNode;
    :cond_6
    instance-of v7, v4, Lcom/samsung/scloud/data/SCloudFolder;

    if-eqz v7, :cond_5

    .line 263
    check-cast v4, Lcom/samsung/scloud/data/SCloudFolder;

    .end local v4    # "node":Lcom/samsung/scloud/data/SCloudNode;
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 266
    :cond_7
    invoke-virtual {v5}, Lcom/samsung/scloud/data/SearchResult;->getNextPageToken()Ljava/lang/String;

    move-result-object v3

    .line 267
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    const/4 v8, 0x1

    if-ge v7, v8, :cond_0

    goto :goto_1
.end method

.method public getMetadata(Ljava/lang/String;)Lcom/samsung/scloud/data/Metadata;
    .locals 1
    .param p1, "fileId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2371
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getMusicCoverArt(Ljava/lang/String;Ljava/io/File;)V
    .locals 1
    .param p1, "serverFileIdOrPath"    # Ljava/lang/String;
    .param p2, "localFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2470
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getMyCloudProvider()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 1229
    const-string v0, "GOOGLE_DRIVE"

    return-object v0
.end method

.method public getRecycleBin(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;
    .locals 1
    .param p1, "Id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2340
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getRefreshToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1526
    iget-object v0, p0, Lcom/samsung/scloud/GoogleDriveAPI;->refreshToken:Ljava/lang/String;

    return-object v0
.end method

.method public getRepository(Ljava/lang/String;)Lcom/samsung/scloud/data/Repository;
    .locals 1
    .param p1, "repositoryName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2236
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getRepositoryInfo()Lcom/samsung/scloud/data/RepositoryInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2228
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getShareURLInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/ShareInfo;
    .locals 1
    .param p1, "Id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2421
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getStreamingInfoForMusic(Ljava/lang/String;)Lcom/samsung/scloud/data/StreamingInfo;
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2445
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getStreamingInfoForVideo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/StreamingInfo;
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "screenResolution"    # Ljava/lang/String;
    .param p3, "connectionType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2437
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getThumbnail(Ljava/io/File;Ljava/lang/String;IILcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 10
    .param p1, "localFile"    # Ljava/io/File;
    .param p2, "serverFileId"    # Ljava/lang/String;
    .param p3, "thumbnail_type"    # I
    .param p4, "thumbnail_format"    # I
    .param p5, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudIOException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;,
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 897
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v7

    const/4 v8, 0x1

    if-ge v7, v8, :cond_1

    .line 898
    :cond_0
    new-instance v7, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v7}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v7

    .line 900
    :cond_1
    invoke-direct {p0}, Lcom/samsung/scloud/GoogleDriveAPI;->loadToken()V

    .line 901
    const/4 v4, 0x0

    .line 902
    .local v4, "fileOut":Ljava/io/FileOutputStream;
    invoke-virtual {p0, p2}, Lcom/samsung/scloud/GoogleDriveAPI;->getFileInfo(Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFile;

    move-result-object v3

    .line 903
    .local v3, "file":Lcom/samsung/scloud/data/SCloudFile;
    const/4 v0, 0x0

    .line 904
    .local v0, "donwloadRequest":Lcom/google/api/services/drive/Drive$Files$Get;
    if-eqz v3, :cond_7

    invoke-virtual {v3}, Lcom/samsung/scloud/data/SCloudFile;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_7

    invoke-virtual {v3}, Lcom/samsung/scloud/data/SCloudFile;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    const/4 v8, 0x1

    if-le v7, v8, :cond_7

    .line 906
    :try_start_0
    iget-object v7, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v7}, Lcom/google/api/services/drive/Drive;->files()Lcom/google/api/services/drive/Drive$Files;

    move-result-object v7

    invoke-virtual {v7, p2}, Lcom/google/api/services/drive/Drive$Files;->get(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$Get;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 911
    const/4 v6, 0x0

    .local v6, "i":I
    move-object v5, v4

    .end local v4    # "fileOut":Ljava/io/FileOutputStream;
    .local v5, "fileOut":Ljava/io/FileOutputStream;
    :goto_0
    const/4 v7, 0x3

    if-ge v6, v7, :cond_8

    .line 913
    :try_start_1
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 914
    .end local v5    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v4    # "fileOut":Ljava/io/FileOutputStream;
    if-eqz v0, :cond_2

    .line 915
    :try_start_2
    invoke-virtual {v0}, Lcom/google/api/services/drive/Drive$Files$Get;->getMediaHttpDownloader()Lcom/google/api/client/googleapis/media/MediaHttpDownloader;

    move-result-object v7

    new-instance v8, Lcom/samsung/scloud/GoogleDriveAPI$5;

    invoke-direct {v8, p0, p5, p1}, Lcom/samsung/scloud/GoogleDriveAPI$5;-><init>(Lcom/samsung/scloud/GoogleDriveAPI;Lcom/samsung/scloud/response/ProgressListener;Ljava/io/File;)V

    invoke-virtual {v7, v8}, Lcom/google/api/client/googleapis/media/MediaHttpDownloader;->setProgressListener(Lcom/google/api/client/googleapis/media/MediaHttpDownloaderProgressListener;)Lcom/google/api/client/googleapis/media/MediaHttpDownloader;

    move-result-object v7

    const/high16 v8, 0x40000

    invoke-virtual {v7, v8}, Lcom/google/api/client/googleapis/media/MediaHttpDownloader;->setChunkSize(I)Lcom/google/api/client/googleapis/media/MediaHttpDownloader;

    move-result-object v7

    new-instance v8, Lcom/google/api/client/http/GenericUrl;

    invoke-virtual {v3}, Lcom/samsung/scloud/data/SCloudFile;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/google/api/client/http/GenericUrl;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v8, v4}, Lcom/google/api/client/googleapis/media/MediaHttpDownloader;->download(Lcom/google/api/client/http/GenericUrl;Ljava/io/OutputStream;)V

    .line 928
    :cond_2
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    .line 929
    const/4 v7, 0x0

    .line 965
    :goto_1
    return-object v7

    .line 907
    .end local v6    # "i":I
    :catch_0
    move-exception v2

    .line 909
    .local v2, "e1":Ljava/io/IOException;
    new-instance v7, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v8, "getThumbnail - creating thumbnail download request failed"

    invoke-direct {v7, v8}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 930
    .end local v2    # "e1":Ljava/io/IOException;
    .end local v4    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v5    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v6    # "i":I
    :catch_1
    move-exception v1

    move-object v4, v5

    .line 931
    .end local v5    # "fileOut":Ljava/io/FileOutputStream;
    .local v1, "e":Ljava/io/FileNotFoundException;
    .restart local v4    # "fileOut":Ljava/io/FileOutputStream;
    :goto_2
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 932
    new-instance v7, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getThumbnail - Input file not found "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 933
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    .end local v4    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v5    # "fileOut":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v1

    move-object v4, v5

    .line 935
    .end local v5    # "fileOut":Ljava/io/FileOutputStream;
    .local v1, "e":Ljava/io/IOException;
    .restart local v4    # "fileOut":Ljava/io/FileOutputStream;
    :goto_3
    :try_start_3
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 939
    :goto_4
    invoke-direct {p0, v1}, Lcom/samsung/scloud/GoogleDriveAPI;->checkForAuthException(Ljava/lang/Throwable;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 940
    const/4 v7, 0x2

    if-ge v6, v7, :cond_3

    .line 941
    invoke-virtual {p0}, Lcom/samsung/scloud/GoogleDriveAPI;->handleTokenExpire()V

    .line 911
    :goto_5
    add-int/lit8 v6, v6, 0x1

    move-object v5, v4

    .end local v4    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v5    # "fileOut":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 936
    .end local v5    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v4    # "fileOut":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v2

    .line 937
    .restart local v2    # "e1":Ljava/io/IOException;
    const-string v7, "getThumbnail - closing fileOutputStream failed"

    invoke-direct {p0, v2, v7}, Lcom/samsung/scloud/GoogleDriveAPI;->throwIOorAuthException(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_4

    .line 944
    .end local v2    # "e1":Ljava/io/IOException;
    :cond_3
    new-instance v7, Lcom/samsung/scloud/exception/SCloudAuthException;

    const-string v8, "server token expired"

    invoke-direct {v7, v8}, Lcom/samsung/scloud/exception/SCloudAuthException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 946
    :cond_4
    instance-of v7, v1, Ljavax/net/ssl/SSLException;

    if-eqz v7, :cond_6

    .line 947
    const/4 v7, 0x2

    if-ge v6, v7, :cond_5

    .line 948
    const-wide/16 v8, 0x7d0

    invoke-direct {p0, v8, v9}, Lcom/samsung/scloud/GoogleDriveAPI;->someSleep(J)V

    goto :goto_5

    .line 951
    :cond_5
    new-instance v7, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getThumbnail - thumbnail download failed"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 954
    :cond_6
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 955
    new-instance v7, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getThumbnail - thumbnail download failed"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 961
    .end local v1    # "e":Ljava/io/IOException;
    .end local v6    # "i":I
    :cond_7
    const-string v7, "SCLOUD_SDK"

    const-string v8, "getThumbnail - this file has no thumbnail or no file"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 962
    new-instance v7, Lcom/samsung/scloud/exception/SCloudException;

    const-string v8, "getThumbnail - this file has no thumbnail or no file"

    invoke-direct {v7, v8}, Lcom/samsung/scloud/exception/SCloudException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 965
    .end local v4    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v5    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v6    # "i":I
    :cond_8
    const/4 v7, 0x0

    move-object v4, v5

    .end local v5    # "fileOut":Ljava/io/FileOutputStream;
    .restart local v4    # "fileOut":Ljava/io/FileOutputStream;
    goto/16 :goto_1

    .line 933
    :catch_4
    move-exception v1

    goto :goto_3

    .line 930
    :catch_5
    move-exception v1

    goto/16 :goto_2
.end method

.method public getTrashedFileList(ILjava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;
    .locals 11
    .param p1, "maxResult"    # I
    .param p2, "nextToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/scloud/data/SCloudNodeList",
            "<",
            "Lcom/samsung/scloud/data/SCloudFile;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    const/4 v10, 0x1

    .line 1370
    invoke-direct {p0}, Lcom/samsung/scloud/GoogleDriveAPI;->loadToken()V

    .line 1373
    const/4 v6, 0x0

    .line 1375
    .local v6, "trashList":Lcom/google/api/services/drive/model/FileList;
    :try_start_0
    iget-object v8, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v8}, Lcom/google/api/services/drive/Drive;->files()Lcom/google/api/services/drive/Drive$Files;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/api/services/drive/Drive$Files;->list()Lcom/google/api/services/drive/Drive$Files$List;

    move-result-object v7

    .line 1377
    .local v7, "trashListRequest":Lcom/google/api/services/drive/Drive$Files$List;
    const-string v8, "trashed=true"

    invoke-virtual {v7, v8}, Lcom/google/api/services/drive/Drive$Files$List;->setQ(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$List;

    .line 1379
    if-le p1, v10, :cond_0

    .line 1380
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/api/services/drive/Drive$Files$List;->setMaxResults(Ljava/lang/Integer;)Lcom/google/api/services/drive/Drive$Files$List;

    .line 1382
    :cond_0
    if-eqz p2, :cond_1

    .line 1383
    invoke-virtual {v7, p2}, Lcom/google/api/services/drive/Drive$Files$List;->setPageToken(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$List;

    .line 1385
    :cond_1
    invoke-direct {p0, v7}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Lcom/google/api/services/drive/model/FileList;

    move-object v6, v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1390
    .end local v7    # "trashListRequest":Lcom/google/api/services/drive/Drive$Files$List;
    :goto_0
    if-eqz v6, :cond_2

    invoke-virtual {v6}, Lcom/google/api/services/drive/model/FileList;->getItems()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ge v8, v10, :cond_4

    :cond_2
    move-object v4, v9

    .line 1410
    :cond_3
    return-object v4

    .line 1386
    :catch_0
    move-exception v1

    .line 1387
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 1388
    const-string v8, "getTrashedFileList - getting trash list failed"

    invoke-direct {p0, v1, v8}, Lcom/samsung/scloud/GoogleDriveAPI;->throwIOorAuthException(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0

    .line 1394
    .end local v1    # "e":Ljava/io/IOException;
    :cond_4
    new-instance v4, Lcom/samsung/scloud/data/SCloudNodeList;

    invoke-direct {v4}, Lcom/samsung/scloud/data/SCloudNodeList;-><init>()V

    .line 1395
    .local v4, "nodes":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<Lcom/samsung/scloud/data/SCloudFile;>;"
    invoke-virtual {v6}, Lcom/google/api/services/drive/model/FileList;->getNextLink()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_5

    invoke-virtual {v6}, Lcom/google/api/services/drive/model/FileList;->getNextLink()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v8, v10, :cond_8

    .line 1396
    :cond_5
    invoke-virtual {v4, v9}, Lcom/samsung/scloud/data/SCloudNodeList;->setNextURL(Ljava/lang/String;)V

    .line 1400
    :goto_1
    invoke-virtual {v6}, Lcom/google/api/services/drive/model/FileList;->getNextPageToken()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_6

    invoke-virtual {v6}, Lcom/google/api/services/drive/model/FileList;->getNextPageToken()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-le v8, v10, :cond_6

    .line 1401
    invoke-virtual {v6}, Lcom/google/api/services/drive/model/FileList;->getNextPageToken()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/samsung/scloud/data/SCloudNodeList;->setNextPageToken(Ljava/lang/String;)V

    .line 1405
    :cond_6
    invoke-virtual {v6}, Lcom/google/api/services/drive/model/FileList;->getItems()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_7
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/drive/model/File;

    .line 1406
    .local v2, "f":Lcom/google/api/services/drive/model/File;
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/File;->getMimeType()Ljava/lang/String;

    move-result-object v8

    const-string v9, "application/vnd.google-apps.folder"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_7

    .line 1407
    invoke-direct {p0, v2}, Lcom/samsung/scloud/GoogleDriveAPI;->parseDriveFile(Lcom/google/api/services/drive/model/File;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v5

    check-cast v5, Lcom/samsung/scloud/data/SCloudFile;

    .line 1408
    .local v5, "retf":Lcom/samsung/scloud/data/SCloudFile;
    if-eqz v5, :cond_7

    invoke-virtual {v4}, Lcom/samsung/scloud/data/SCloudNodeList;->getNodes()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1398
    .end local v2    # "f":Lcom/google/api/services/drive/model/File;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v5    # "retf":Lcom/samsung/scloud/data/SCloudFile;
    :cond_8
    invoke-virtual {v6}, Lcom/google/api/services/drive/model/FileList;->getNextLink()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/samsung/scloud/data/SCloudNodeList;->setNextURL(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public getTrashedFolderList(ILjava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;
    .locals 11
    .param p1, "maxResult"    # I
    .param p2, "nextToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/scloud/data/SCloudNodeList",
            "<",
            "Lcom/samsung/scloud/data/SCloudFolder;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    const/4 v10, 0x1

    .line 1423
    invoke-direct {p0}, Lcom/samsung/scloud/GoogleDriveAPI;->loadToken()V

    .line 1426
    const/4 v6, 0x0

    .line 1428
    .local v6, "trashList":Lcom/google/api/services/drive/model/FileList;
    :try_start_0
    iget-object v8, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v8}, Lcom/google/api/services/drive/Drive;->files()Lcom/google/api/services/drive/Drive$Files;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/api/services/drive/Drive$Files;->list()Lcom/google/api/services/drive/Drive$Files$List;

    move-result-object v7

    .line 1430
    .local v7, "trashListRequest":Lcom/google/api/services/drive/Drive$Files$List;
    const-string v8, "trashed=true"

    invoke-virtual {v7, v8}, Lcom/google/api/services/drive/Drive$Files$List;->setQ(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$List;

    .line 1432
    if-le p1, v10, :cond_0

    .line 1433
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/api/services/drive/Drive$Files$List;->setMaxResults(Ljava/lang/Integer;)Lcom/google/api/services/drive/Drive$Files$List;

    .line 1435
    :cond_0
    if-eqz p2, :cond_1

    .line 1436
    invoke-virtual {v7, p2}, Lcom/google/api/services/drive/Drive$Files$List;->setPageToken(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$List;

    .line 1438
    :cond_1
    invoke-direct {p0, v7}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Lcom/google/api/services/drive/model/FileList;

    move-object v6, v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1443
    .end local v7    # "trashListRequest":Lcom/google/api/services/drive/Drive$Files$List;
    :goto_0
    if-eqz v6, :cond_2

    invoke-virtual {v6}, Lcom/google/api/services/drive/model/FileList;->getItems()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ge v8, v10, :cond_4

    :cond_2
    move-object v4, v9

    .line 1463
    :cond_3
    return-object v4

    .line 1439
    :catch_0
    move-exception v1

    .line 1440
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 1441
    const-string v8, "getTrashedFolderList - getting trash list failed"

    invoke-direct {p0, v1, v8}, Lcom/samsung/scloud/GoogleDriveAPI;->throwIOorAuthException(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0

    .line 1447
    .end local v1    # "e":Ljava/io/IOException;
    :cond_4
    new-instance v4, Lcom/samsung/scloud/data/SCloudNodeList;

    invoke-direct {v4}, Lcom/samsung/scloud/data/SCloudNodeList;-><init>()V

    .line 1448
    .local v4, "nodes":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<Lcom/samsung/scloud/data/SCloudFolder;>;"
    invoke-virtual {v6}, Lcom/google/api/services/drive/model/FileList;->getNextLink()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_5

    invoke-virtual {v6}, Lcom/google/api/services/drive/model/FileList;->getNextLink()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v8, v10, :cond_8

    .line 1449
    :cond_5
    invoke-virtual {v4, v9}, Lcom/samsung/scloud/data/SCloudNodeList;->setNextURL(Ljava/lang/String;)V

    .line 1453
    :goto_1
    invoke-virtual {v6}, Lcom/google/api/services/drive/model/FileList;->getNextPageToken()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_6

    invoke-virtual {v6}, Lcom/google/api/services/drive/model/FileList;->getNextPageToken()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-le v8, v10, :cond_6

    .line 1454
    invoke-virtual {v6}, Lcom/google/api/services/drive/model/FileList;->getNextPageToken()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/samsung/scloud/data/SCloudNodeList;->setNextPageToken(Ljava/lang/String;)V

    .line 1458
    :cond_6
    invoke-virtual {v6}, Lcom/google/api/services/drive/model/FileList;->getItems()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_7
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/drive/model/File;

    .line 1459
    .local v2, "f":Lcom/google/api/services/drive/model/File;
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/File;->getMimeType()Ljava/lang/String;

    move-result-object v8

    const-string v9, "application/vnd.google-apps.folder"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 1460
    invoke-direct {p0, v2}, Lcom/samsung/scloud/GoogleDriveAPI;->parseDriveFile(Lcom/google/api/services/drive/model/File;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v5

    check-cast v5, Lcom/samsung/scloud/data/SCloudFolder;

    .line 1461
    .local v5, "retf":Lcom/samsung/scloud/data/SCloudFolder;
    if-eqz v5, :cond_7

    invoke-virtual {v4}, Lcom/samsung/scloud/data/SCloudNodeList;->getNodes()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1451
    .end local v2    # "f":Lcom/google/api/services/drive/model/File;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v5    # "retf":Lcom/samsung/scloud/data/SCloudFolder;
    :cond_8
    invoke-virtual {v6}, Lcom/google/api/services/drive/model/FileList;->getNextLink()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/samsung/scloud/data/SCloudNodeList;->setNextURL(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public getUpdate(Ljava/lang/String;Ljava/lang/String;J)Lcom/samsung/scloud/data/GDataChanges;
    .locals 17
    .param p1, "startChangeId"    # Ljava/lang/String;
    .param p2, "pageToken"    # Ljava/lang/String;
    .param p3, "maxResults"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudIOException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;
        }
    .end annotation

    .prologue
    .line 1164
    invoke-direct/range {p0 .. p0}, Lcom/samsung/scloud/GoogleDriveAPI;->loadToken()V

    .line 1166
    const/4 v3, 0x0

    .line 1167
    .local v3, "changeInfo":Lcom/google/api/services/drive/model/ChangeList;
    new-instance v7, Lcom/samsung/scloud/data/GDataChanges;

    invoke-direct {v7}, Lcom/samsung/scloud/data/GDataChanges;-><init>()V

    .line 1168
    .local v7, "gdataChanges":Lcom/samsung/scloud/data/GDataChanges;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1171
    .local v4, "changes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/SCloudNode;>;"
    :try_start_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v11}, Lcom/google/api/services/drive/Drive;->changes()Lcom/google/api/services/drive/Drive$Changes;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/api/services/drive/Drive$Changes;->list()Lcom/google/api/services/drive/Drive$Changes$List;

    move-result-object v8

    .line 1172
    .local v8, "getChangesRequest":Lcom/google/api/services/drive/Drive$Changes$List;
    if-eqz p1, :cond_0

    .line 1173
    new-instance v11, Ljava/math/BigInteger;

    move-object/from16 v0, p1

    invoke-direct {v11, v0}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v11}, Lcom/google/api/services/drive/Drive$Changes$List;->setStartChangeId(Ljava/math/BigInteger;)Lcom/google/api/services/drive/Drive$Changes$List;

    .line 1175
    :cond_0
    const-wide/16 v12, 0x0

    cmp-long v11, p3, v12

    if-lez v11, :cond_1

    .line 1176
    move-wide/from16 v0, p3

    long-to-int v11, v0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v8, v11}, Lcom/google/api/services/drive/Drive$Changes$List;->setMaxResults(Ljava/lang/Integer;)Lcom/google/api/services/drive/Drive$Changes$List;

    .line 1178
    :cond_1
    if-eqz p2, :cond_2

    .line 1179
    move-object/from16 v0, p2

    invoke-virtual {v8, v0}, Lcom/google/api/services/drive/Drive$Changes$List;->setPageToken(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Changes$List;

    .line 1181
    :cond_2
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    move-object v0, v11

    check-cast v0, Lcom/google/api/services/drive/model/ChangeList;

    move-object v3, v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1185
    .end local v8    # "getChangesRequest":Lcom/google/api/services/drive/Drive$Changes$List;
    :goto_0
    invoke-virtual {v3}, Lcom/google/api/services/drive/model/ChangeList;->getEtag()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Lcom/samsung/scloud/data/GDataChanges;->setEtag(Ljava/lang/String;)V

    .line 1186
    invoke-virtual {v3}, Lcom/google/api/services/drive/model/ChangeList;->getLargestChangeId()Ljava/math/BigInteger;

    move-result-object v11

    invoke-virtual {v11}, Ljava/math/BigInteger;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Lcom/samsung/scloud/data/GDataChanges;->setLargestChangeId(Ljava/lang/String;)V

    .line 1187
    invoke-virtual {v3}, Lcom/google/api/services/drive/model/ChangeList;->getNextLink()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_3

    .line 1188
    invoke-virtual {v3}, Lcom/google/api/services/drive/model/ChangeList;->getNextPageToken()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Lcom/samsung/scloud/data/GDataChanges;->setNextPageToken(Ljava/lang/String;)V

    .line 1189
    invoke-virtual {v3}, Lcom/google/api/services/drive/model/ChangeList;->getNextLink()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Lcom/samsung/scloud/data/GDataChanges;->setNextLink(Ljava/lang/String;)V

    .line 1193
    :cond_3
    invoke-virtual {v3}, Lcom/google/api/services/drive/model/ChangeList;->getItems()Ljava/util/List;

    move-result-object v11

    if-eqz v11, :cond_7

    .line 1194
    invoke-virtual {v3}, Lcom/google/api/services/drive/model/ChangeList;->getItems()Ljava/util/List;

    move-result-object v10

    .line 1195
    .local v10, "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/drive/model/Change;>;"
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/drive/model/Change;

    .line 1197
    .local v2, "change":Lcom/google/api/services/drive/model/Change;
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/Change;->getFile()Lcom/google/api/services/drive/model/File;

    move-result-object v11

    if-nez v11, :cond_5

    .line 1198
    new-instance v6, Lcom/samsung/scloud/data/SCloudFile;

    invoke-direct {v6}, Lcom/samsung/scloud/data/SCloudFile;-><init>()V

    .line 1199
    .local v6, "filemeta":Lcom/samsung/scloud/data/SCloudNode;
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/Change;->getId()Ljava/math/BigInteger;

    move-result-object v11

    invoke-virtual {v11}, Ljava/math/BigInteger;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Lcom/samsung/scloud/data/SCloudNode;->setChangeId(Ljava/lang/String;)V

    .line 1200
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/Change;->getDeleted()Ljava/lang/Boolean;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    invoke-virtual {v6, v11}, Lcom/samsung/scloud/data/SCloudNode;->setDeleted(Z)V

    .line 1201
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/Change;->getDeleted()Ljava/lang/Boolean;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    if-eqz v11, :cond_4

    sget-object v11, Lcom/samsung/scloud/data/SCloudNode;->TYPE_UNKNOWN:Ljava/lang/String;

    invoke-virtual {v6, v11}, Lcom/samsung/scloud/data/SCloudNode;->setNodeType(Ljava/lang/String;)V

    .line 1202
    :cond_4
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/Change;->getId()Ljava/math/BigInteger;

    move-result-object v11

    invoke-virtual {v11}, Ljava/math/BigInteger;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Lcom/samsung/scloud/data/SCloudNode;->setId(Ljava/lang/String;)V

    .line 1203
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1182
    .end local v2    # "change":Lcom/google/api/services/drive/model/Change;
    .end local v6    # "filemeta":Lcom/samsung/scloud/data/SCloudNode;
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v10    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/drive/model/Change;>;"
    :catch_0
    move-exception v5

    .line 1183
    .local v5, "e":Ljava/io/IOException;
    const-string v11, "getUpdate - getChangesRequest failed"

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v11}, Lcom/samsung/scloud/GoogleDriveAPI;->throwIOorAuthException(Ljava/lang/Exception;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1206
    .end local v5    # "e":Ljava/io/IOException;
    .restart local v2    # "change":Lcom/google/api/services/drive/model/Change;
    .restart local v9    # "i$":Ljava/util/Iterator;
    .restart local v10    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/drive/model/Change;>;"
    :cond_5
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/Change;->getFile()Lcom/google/api/services/drive/model/File;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/api/services/drive/model/File;->getMimeType()Ljava/lang/String;

    move-result-object v11

    const-string v12, "application/vnd.google-apps.folder"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 1207
    new-instance v6, Lcom/samsung/scloud/data/SCloudFolder;

    invoke-direct {v6}, Lcom/samsung/scloud/data/SCloudFolder;-><init>()V

    .line 1211
    .restart local v6    # "filemeta":Lcom/samsung/scloud/data/SCloudNode;
    :goto_2
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/Change;->getFile()Lcom/google/api/services/drive/model/File;

    move-result-object v11

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/samsung/scloud/GoogleDriveAPI;->parseDriveFile(Lcom/google/api/services/drive/model/File;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v6

    .line 1212
    const-string v11, "SCLOUD_SDK"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "change ID value ............"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v2}, Lcom/google/api/services/drive/model/Change;->getId()Ljava/math/BigInteger;

    move-result-object v13

    invoke-virtual {v13}, Ljava/math/BigInteger;->longValue()J

    move-result-wide v14

    invoke-virtual {v12, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1213
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/Change;->getId()Ljava/math/BigInteger;

    move-result-object v11

    invoke-virtual {v11}, Ljava/math/BigInteger;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Lcom/samsung/scloud/data/SCloudNode;->setChangeId(Ljava/lang/String;)V

    .line 1214
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/Change;->getDeleted()Ljava/lang/Boolean;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    invoke-virtual {v6, v11}, Lcom/samsung/scloud/data/SCloudNode;->setDeleted(Z)V

    .line 1215
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1209
    .end local v6    # "filemeta":Lcom/samsung/scloud/data/SCloudNode;
    :cond_6
    new-instance v6, Lcom/samsung/scloud/data/SCloudFile;

    invoke-direct {v6}, Lcom/samsung/scloud/data/SCloudFile;-><init>()V

    .restart local v6    # "filemeta":Lcom/samsung/scloud/data/SCloudNode;
    goto :goto_2

    .line 1219
    .end local v2    # "change":Lcom/google/api/services/drive/model/Change;
    .end local v6    # "filemeta":Lcom/samsung/scloud/data/SCloudNode;
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v10    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/api/services/drive/model/Change;>;"
    :cond_7
    invoke-virtual {v7, v4}, Lcom/samsung/scloud/data/GDataChanges;->setChanges(Ljava/util/List;)V

    .line 1220
    return-object v7
.end method

.method public getUpdate(JJ)Ljava/util/ArrayList;
    .locals 1
    .param p1, "beginTimeStamp"    # J
    .param p3, "endTimeStamp"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Update;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2324
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public getUserInfo()Lcom/samsung/scloud/data/User;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudIOException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;
        }
    .end annotation

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/samsung/scloud/GoogleDriveAPI;->loadToken()V

    .line 145
    const/4 v3, 0x0

    .line 147
    .local v3, "userInfo":Lcom/google/api/services/drive/model/About;
    :try_start_0
    iget-object v4, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v4}, Lcom/google/api/services/drive/Drive;->about()Lcom/google/api/services/drive/Drive$About;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/api/services/drive/Drive$About;->get()Lcom/google/api/services/drive/Drive$About$Get;

    move-result-object v2

    .line 148
    .local v2, "getUserRequest":Lcom/google/api/services/drive/Drive$About$Get;
    invoke-direct {p0, v2}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/google/api/services/drive/model/About;

    move-object v3, v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 152
    .end local v2    # "getUserRequest":Lcom/google/api/services/drive/Drive$About$Get;
    :goto_0
    invoke-direct {p0, v3}, Lcom/samsung/scloud/GoogleDriveAPI;->parseUserInfo(Lcom/google/api/services/drive/model/About;)Lcom/samsung/scloud/data/User;

    move-result-object v4

    return-object v4

    .line 149
    :catch_0
    move-exception v1

    .line 150
    .local v1, "e":Ljava/io/IOException;
    const-string v4, "getUserInfo - getUserInfo failed"

    invoke-direct {p0, v1, v4}, Lcom/samsung/scloud/GoogleDriveAPI;->throwIOorAuthException(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getVersions(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 5
    .param p1, "Id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Version;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudIOException;,
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;
        }
    .end annotation

    .prologue
    .line 1244
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    if-ge v2, v3, :cond_1

    .line 1245
    :cond_0
    new-instance v2, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v2}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v2

    .line 1247
    :cond_1
    const/4 v1, 0x0

    .line 1249
    .local v1, "revisions":Lcom/google/api/services/drive/model/RevisionList;
    :try_start_0
    iget-object v2, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v2}, Lcom/google/api/services/drive/Drive;->revisions()Lcom/google/api/services/drive/Drive$Revisions;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/api/services/drive/Drive$Revisions;->list(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Revisions$List;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "revisions":Lcom/google/api/services/drive/model/RevisionList;
    check-cast v1, Lcom/google/api/services/drive/model/RevisionList;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1253
    .restart local v1    # "revisions":Lcom/google/api/services/drive/model/RevisionList;
    invoke-direct {p0, v1}, Lcom/samsung/scloud/GoogleDriveAPI;->parseRevisionFeedAndMap(Lcom/google/api/services/drive/model/RevisionList;)Ljava/util/ArrayList;

    move-result-object v2

    return-object v2

    .line 1250
    .end local v1    # "revisions":Lcom/google/api/services/drive/model/RevisionList;
    :catch_0
    move-exception v0

    .line 1251
    .local v0, "e":Ljava/io/IOException;
    new-instance v2, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getVersions - getting Revisions failed "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getWholeFileList(ILjava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;
    .locals 10
    .param p1, "maxResults"    # I
    .param p2, "nextToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/scloud/data/SCloudNodeList",
            "<",
            "Lcom/samsung/scloud/data/SCloudFile;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v9, 0x1

    .line 1551
    invoke-direct {p0}, Lcom/samsung/scloud/GoogleDriveAPI;->loadToken()V

    .line 1554
    const/4 v5, 0x0

    .line 1556
    .local v5, "request":Lcom/google/api/services/drive/Drive$Files$List;
    :try_start_0
    iget-object v8, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v8}, Lcom/google/api/services/drive/Drive;->files()Lcom/google/api/services/drive/Drive$Files;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/api/services/drive/Drive$Files;->list()Lcom/google/api/services/drive/Drive$Files$List;

    move-result-object v5

    .line 1557
    if-le p1, v9, :cond_0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/google/api/services/drive/Drive$Files$List;->setMaxResults(Ljava/lang/Integer;)Lcom/google/api/services/drive/Drive$Files$List;

    .line 1558
    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {v5, p2}, Lcom/google/api/services/drive/Drive$Files$List;->setPageToken(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1565
    :cond_1
    const/4 v2, 0x0

    .line 1567
    .local v2, "files":Lcom/google/api/services/drive/model/FileList;
    :try_start_1
    invoke-direct {p0, v5}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "files":Lcom/google/api/services/drive/model/FileList;
    check-cast v2, Lcom/google/api/services/drive/model/FileList;
    :try_end_1
    .catch Lcom/samsung/scloud/exception/SCloudIOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1572
    .restart local v2    # "files":Lcom/google/api/services/drive/model/FileList;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/google/api/services/drive/model/FileList;->getItems()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ge v8, v9, :cond_4

    :cond_2
    move-object v4, v7

    .line 1591
    :cond_3
    return-object v4

    .line 1559
    .end local v2    # "files":Lcom/google/api/services/drive/model/FileList;
    :catch_0
    move-exception v0

    .line 1560
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 1561
    new-instance v7, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getWholeFileList - build request failed "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 1568
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 1569
    .local v0, "e":Lcom/samsung/scloud/exception/SCloudIOException;
    invoke-virtual {v0}, Lcom/samsung/scloud/exception/SCloudIOException;->printStackTrace()V

    .line 1570
    throw v0

    .line 1575
    .end local v0    # "e":Lcom/samsung/scloud/exception/SCloudIOException;
    .restart local v2    # "files":Lcom/google/api/services/drive/model/FileList;
    :cond_4
    new-instance v4, Lcom/samsung/scloud/data/SCloudNodeList;

    invoke-direct {v4}, Lcom/samsung/scloud/data/SCloudNodeList;-><init>()V

    .line 1576
    .local v4, "nodes":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<Lcom/samsung/scloud/data/SCloudFile;>;"
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/FileList;->getNextLink()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_5

    invoke-virtual {v2}, Lcom/google/api/services/drive/model/FileList;->getNextLink()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v8, v9, :cond_8

    .line 1577
    :cond_5
    invoke-virtual {v4, v7}, Lcom/samsung/scloud/data/SCloudNodeList;->setNextURL(Ljava/lang/String;)V

    .line 1581
    :goto_0
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/FileList;->getNextPageToken()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_6

    invoke-virtual {v2}, Lcom/google/api/services/drive/model/FileList;->getNextPageToken()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-le v7, v9, :cond_6

    .line 1582
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/FileList;->getNextPageToken()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/samsung/scloud/data/SCloudNodeList;->setNextPageToken(Ljava/lang/String;)V

    .line 1586
    :cond_6
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/FileList;->getItems()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_7
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/drive/model/File;

    .line 1587
    .local v1, "f":Lcom/google/api/services/drive/model/File;
    invoke-virtual {v1}, Lcom/google/api/services/drive/model/File;->getMimeType()Ljava/lang/String;

    move-result-object v7

    const-string v8, "application/vnd.google-apps.folder"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_7

    .line 1588
    invoke-direct {p0, v1}, Lcom/samsung/scloud/GoogleDriveAPI;->parseDriveFile(Lcom/google/api/services/drive/model/File;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v6

    check-cast v6, Lcom/samsung/scloud/data/SCloudFile;

    .line 1589
    .local v6, "retf":Lcom/samsung/scloud/data/SCloudFile;
    if-eqz v6, :cond_7

    invoke-virtual {v4}, Lcom/samsung/scloud/data/SCloudNodeList;->getNodes()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1579
    .end local v1    # "f":Lcom/google/api/services/drive/model/File;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v6    # "retf":Lcom/samsung/scloud/data/SCloudFile;
    :cond_8
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/FileList;->getNextLink()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/samsung/scloud/data/SCloudNodeList;->setNextURL(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getWholeFolderList(ILjava/lang/String;)Lcom/samsung/scloud/data/SCloudNodeList;
    .locals 10
    .param p1, "maxResults"    # I
    .param p2, "nextToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Lcom/samsung/scloud/data/SCloudNodeList",
            "<",
            "Lcom/samsung/scloud/data/SCloudFolder;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v9, 0x1

    .line 1603
    invoke-direct {p0}, Lcom/samsung/scloud/GoogleDriveAPI;->loadToken()V

    .line 1606
    const/4 v5, 0x0

    .line 1608
    .local v5, "request":Lcom/google/api/services/drive/Drive$Files$List;
    :try_start_0
    iget-object v8, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v8}, Lcom/google/api/services/drive/Drive;->files()Lcom/google/api/services/drive/Drive$Files;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/api/services/drive/Drive$Files;->list()Lcom/google/api/services/drive/Drive$Files$List;

    move-result-object v5

    .line 1609
    const-string v8, "mimeType=\'application/vnd.google-apps.folder\'"

    invoke-virtual {v5, v8}, Lcom/google/api/services/drive/Drive$Files$List;->setQ(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$List;

    .line 1610
    if-le p1, v9, :cond_0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/google/api/services/drive/Drive$Files$List;->setMaxResults(Ljava/lang/Integer;)Lcom/google/api/services/drive/Drive$Files$List;

    .line 1611
    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {v5, p2}, Lcom/google/api/services/drive/Drive$Files$List;->setPageToken(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1617
    :cond_1
    const/4 v2, 0x0

    .line 1619
    .local v2, "files":Lcom/google/api/services/drive/model/FileList;
    :try_start_1
    invoke-direct {p0, v5}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "files":Lcom/google/api/services/drive/model/FileList;
    check-cast v2, Lcom/google/api/services/drive/model/FileList;
    :try_end_1
    .catch Lcom/samsung/scloud/exception/SCloudIOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1624
    .restart local v2    # "files":Lcom/google/api/services/drive/model/FileList;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/google/api/services/drive/model/FileList;->getItems()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    if-ge v8, v9, :cond_4

    :cond_2
    move-object v4, v7

    .line 1643
    :cond_3
    return-object v4

    .line 1612
    .end local v2    # "files":Lcom/google/api/services/drive/model/FileList;
    :catch_0
    move-exception v0

    .line 1613
    .local v0, "e":Ljava/io/IOException;
    new-instance v7, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getWholeFolderList - build request failed "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 1620
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 1621
    .local v0, "e":Lcom/samsung/scloud/exception/SCloudIOException;
    invoke-virtual {v0}, Lcom/samsung/scloud/exception/SCloudIOException;->printStackTrace()V

    .line 1622
    throw v0

    .line 1627
    .end local v0    # "e":Lcom/samsung/scloud/exception/SCloudIOException;
    .restart local v2    # "files":Lcom/google/api/services/drive/model/FileList;
    :cond_4
    new-instance v4, Lcom/samsung/scloud/data/SCloudNodeList;

    invoke-direct {v4}, Lcom/samsung/scloud/data/SCloudNodeList;-><init>()V

    .line 1628
    .local v4, "nodes":Lcom/samsung/scloud/data/SCloudNodeList;, "Lcom/samsung/scloud/data/SCloudNodeList<Lcom/samsung/scloud/data/SCloudFolder;>;"
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/FileList;->getNextLink()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_5

    invoke-virtual {v2}, Lcom/google/api/services/drive/model/FileList;->getNextLink()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v8, v9, :cond_8

    .line 1629
    :cond_5
    invoke-virtual {v4, v7}, Lcom/samsung/scloud/data/SCloudNodeList;->setNextURL(Ljava/lang/String;)V

    .line 1633
    :goto_0
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/FileList;->getNextPageToken()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_6

    invoke-virtual {v2}, Lcom/google/api/services/drive/model/FileList;->getNextPageToken()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    if-le v7, v9, :cond_6

    .line 1634
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/FileList;->getNextPageToken()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/samsung/scloud/data/SCloudNodeList;->setNextPageToken(Ljava/lang/String;)V

    .line 1638
    :cond_6
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/FileList;->getItems()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_7
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/drive/model/File;

    .line 1639
    .local v1, "f":Lcom/google/api/services/drive/model/File;
    invoke-virtual {v1}, Lcom/google/api/services/drive/model/File;->getMimeType()Ljava/lang/String;

    move-result-object v7

    const-string v8, "application/vnd.google-apps.folder"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1640
    invoke-direct {p0, v1}, Lcom/samsung/scloud/GoogleDriveAPI;->parseDriveFile(Lcom/google/api/services/drive/model/File;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v6

    check-cast v6, Lcom/samsung/scloud/data/SCloudFolder;

    .line 1641
    .local v6, "retf":Lcom/samsung/scloud/data/SCloudFolder;
    if-eqz v6, :cond_7

    invoke-virtual {v4}, Lcom/samsung/scloud/data/SCloudNodeList;->getNodes()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1631
    .end local v1    # "f":Lcom/google/api/services/drive/model/File;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v6    # "retf":Lcom/samsung/scloud/data/SCloudFolder;
    :cond_8
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/FileList;->getNextLink()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/samsung/scloud/data/SCloudNodeList;->setNextURL(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public handleTokenExpire()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudAuthException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 1855
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v3}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/scloud/GoogleDriveAPI;->mLatch:Ljava/util/concurrent/CountDownLatch;

    .line 1856
    const-string v0, "SCLOUD_SDK"

    const-string v2, "Token expired !! Retry get access token..."

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1859
    new-instance v6, Lcom/google/api/client/googleapis/extensions/android2/auth/GoogleAccountManager;

    iget-object v0, p0, Lcom/samsung/scloud/GoogleDriveAPI;->mContext:Landroid/content/Context;

    invoke-direct {v6, v0}, Lcom/google/api/client/googleapis/extensions/android2/auth/GoogleAccountManager;-><init>(Landroid/content/Context;)V

    .line 1860
    .local v6, "accountManager":Lcom/google/api/client/googleapis/extensions/android2/auth/GoogleAccountManager;
    iget-object v0, p0, Lcom/samsung/scloud/GoogleDriveAPI;->credential:Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx;

    invoke-virtual {v0}, Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx;->getAccessToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/google/api/client/googleapis/extensions/android2/auth/GoogleAccountManager;->invalidateAuthToken(Ljava/lang/String;)V

    .line 1861
    iput-object v5, p0, Lcom/samsung/scloud/GoogleDriveAPI;->accessToken:Ljava/lang/String;

    .line 1862
    iget-object v0, p0, Lcom/samsung/scloud/GoogleDriveAPI;->credential:Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx;

    invoke-virtual {v0, v5}, Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx;->setAccessToken(Ljava/lang/String;)Lcom/google/api/client/googleapis/auth/oauth2/GoogleCredential;

    .line 1864
    iget-object v0, p0, Lcom/samsung/scloud/GoogleDriveAPI;->mContext:Landroid/content/Context;

    const-string v2, "googleCredential"

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v9

    .line 1865
    .local v9, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v9}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    .line 1866
    .local v8, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v0, "accessToken"

    invoke-interface {v8, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1867
    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1869
    iget-object v0, p0, Lcom/samsung/scloud/GoogleDriveAPI;->accountName:Ljava/lang/String;

    invoke-virtual {v6, v0}, Lcom/google/api/client/googleapis/extensions/android2/auth/GoogleAccountManager;->getAccountByName(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    .line 1870
    .local v1, "account":Landroid/accounts/Account;
    if-nez v1, :cond_1

    .line 1871
    const-string v0, "SCLOUD_SDK"

    const-string v2, "Account is null !"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1907
    :cond_0
    :goto_0
    return-void

    .line 1876
    :cond_1
    const-string v0, "SCLOUD_SDK"

    const-string v2, "Before retry get access token ..."

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1877
    invoke-virtual {v6}, Lcom/google/api/client/googleapis/extensions/android2/auth/GoogleAccountManager;->getAccountManager()Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v2, "oauth2:https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/drive"

    new-instance v4, Lcom/samsung/scloud/GoogleDriveAPI$7;

    invoke-direct {v4, p0}, Lcom/samsung/scloud/GoogleDriveAPI$7;-><init>(Lcom/samsung/scloud/GoogleDriveAPI;)V

    invoke-virtual/range {v0 .. v5}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;ZLandroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 1900
    :try_start_0
    iget-object v0, p0, Lcom/samsung/scloud/GoogleDriveAPI;->mLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 1901
    iget-boolean v0, p0, Lcom/samsung/scloud/GoogleDriveAPI;->mIsRetryAuth:Z

    if-eqz v0, :cond_0

    .line 1902
    new-instance v0, Lcom/samsung/scloud/exception/SCloudAuthException;

    const-string v2, "server account info changed"

    invoke-direct {v0, v2}, Lcom/samsung/scloud/exception/SCloudAuthException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1904
    :catch_0
    move-exception v7

    .line 1905
    .local v7, "e1":Ljava/lang/InterruptedException;
    invoke-virtual {v7}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method public inviteCollaborators(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 7
    .param p1, "Id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Collaborator;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudIOException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;
        }
    .end annotation

    .prologue
    .local p2, "collaborators":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/Collaborator;>;"
    const/4 v5, 0x1

    .line 1128
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-lt v4, v5, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v4, v5, :cond_1

    .line 1129
    :cond_0
    new-instance v4, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v4}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v4

    .line 1131
    :cond_1
    invoke-direct {p0}, Lcom/samsung/scloud/GoogleDriveAPI;->loadToken()V

    .line 1133
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/scloud/data/Collaborator;

    .line 1134
    .local v3, "temp":Lcom/samsung/scloud/data/Collaborator;
    new-instance v2, Lcom/google/api/services/drive/model/Permission;

    invoke-direct {v2}, Lcom/google/api/services/drive/model/Permission;-><init>()V

    .line 1135
    .local v2, "newPermission":Lcom/google/api/services/drive/model/Permission;
    invoke-virtual {v3}, Lcom/samsung/scloud/data/Collaborator;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/api/services/drive/model/Permission;->setValue(Ljava/lang/String;)Lcom/google/api/services/drive/model/Permission;

    .line 1136
    invoke-virtual {v3}, Lcom/samsung/scloud/data/Collaborator;->getType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/api/services/drive/model/Permission;->setType(Ljava/lang/String;)Lcom/google/api/services/drive/model/Permission;

    .line 1137
    invoke-virtual {v3}, Lcom/samsung/scloud/data/Collaborator;->getRole()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/api/services/drive/model/Permission;->setRole(Ljava/lang/String;)Lcom/google/api/services/drive/model/Permission;

    .line 1139
    :try_start_0
    iget-object v4, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v4}, Lcom/google/api/services/drive/Drive;->permissions()Lcom/google/api/services/drive/Drive$Permissions;

    move-result-object v4

    invoke-virtual {v4, p1, v2}, Lcom/google/api/services/drive/Drive$Permissions;->insert(Ljava/lang/String;Lcom/google/api/services/drive/model/Permission;)Lcom/google/api/services/drive/Drive$Permissions$Insert;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1145
    const-string v4, "SCLOUD_SDK"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "inviteCollaborators successFull For ..."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/samsung/scloud/data/Collaborator;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1140
    :catch_0
    move-exception v0

    .line 1141
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 1142
    const-string v4, "SCLOUD_SDK"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "inviteCollaborator - failed for"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/samsung/scloud/data/Collaborator;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "and continue with the next one"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1143
    new-instance v4, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "inviteCollaborator - failed for"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/samsung/scloud/data/Collaborator;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1147
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "newPermission":Lcom/google/api/services/drive/model/Permission;
    .end local v3    # "temp":Lcom/samsung/scloud/data/Collaborator;
    :cond_2
    return-void
.end method

.method public isAuthSucceed()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2300
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public login(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/User;
    .locals 1
    .param p1, "ID"    # Ljava/lang/String;
    .param p2, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2308
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public logout()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2316
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public makeCurrentVersion(Ljava/lang/String;J)Ljava/util/ArrayList;
    .locals 1
    .param p1, "Id"    # Ljava/lang/String;
    .param p2, "versionId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/scloud/data/Version;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2348
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public makeFolder(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/scloud/data/SCloudFolder;
    .locals 12
    .param p1, "folderName"    # Ljava/lang/String;
    .param p2, "parentId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudIOException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    .line 168
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    if-lt v8, v9, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v8, v9, :cond_1

    .line 169
    :cond_0
    new-instance v8, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v8}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v8

    .line 171
    :cond_1
    invoke-direct {p0}, Lcom/samsung/scloud/GoogleDriveAPI;->loadToken()V

    .line 174
    new-instance v5, Lcom/google/api/services/drive/model/File;

    invoke-direct {v5}, Lcom/google/api/services/drive/model/File;-><init>()V

    .line 175
    .local v5, "folderContent":Lcom/google/api/services/drive/model/File;
    invoke-virtual {v5, p1}, Lcom/google/api/services/drive/model/File;->setTitle(Ljava/lang/String;)Lcom/google/api/services/drive/model/File;

    .line 176
    const-string v8, "application/vnd.google-apps.folder"

    invoke-virtual {v5, v8}, Lcom/google/api/services/drive/model/File;->setMimeType(Ljava/lang/String;)Lcom/google/api/services/drive/model/File;

    .line 179
    const/4 v2, 0x0

    .line 181
    .local v2, "createdFolder":Lcom/google/api/services/drive/model/File;
    :try_start_0
    iget-object v8, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v8}, Lcom/google/api/services/drive/Drive;->files()Lcom/google/api/services/drive/Drive$Files;

    move-result-object v8

    invoke-virtual {v8, v5}, Lcom/google/api/services/drive/Drive$Files;->insert(Lcom/google/api/services/drive/model/File;)Lcom/google/api/services/drive/Drive$Files$Insert;

    move-result-object v6

    .line 182
    .local v6, "folderRequest":Lcom/google/api/services/drive/Drive$Files$Insert;
    invoke-direct {p0, v6}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Lcom/google/api/services/drive/model/File;

    move-object v2, v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 187
    .end local v6    # "folderRequest":Lcom/google/api/services/drive/Drive$Files$Insert;
    :goto_0
    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/samsung/scloud/GoogleDriveAPI;->getRootFolderId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p2, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 190
    :try_start_1
    iget-object v8, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v8}, Lcom/google/api/services/drive/Drive;->parents()Lcom/google/api/services/drive/Drive$Parents;

    move-result-object v9

    invoke-virtual {v2}, Lcom/google/api/services/drive/model/File;->getId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2}, Lcom/google/api/services/drive/model/File;->getParents()Ljava/util/List;

    move-result-object v8

    const/4 v11, 0x0

    invoke-interface {v8, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/api/services/drive/model/ParentReference;

    invoke-virtual {v8}, Lcom/google/api/services/drive/model/ParentReference;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9, v10, v8}, Lcom/google/api/services/drive/Drive$Parents;->delete(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Parents$Delete;

    move-result-object v3

    .line 192
    .local v3, "delRootRequest":Lcom/google/api/services/drive/Drive$Parents$Delete;
    invoke-direct {p0, v3}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/File;->getParents()Ljava/util/List;

    move-result-object v8

    const/4 v9, 0x0

    invoke-interface {v8, v9}, Ljava/util/List;->remove(I)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 199
    .end local v3    # "delRootRequest":Lcom/google/api/services/drive/Drive$Parents$Delete;
    :goto_1
    :try_start_2
    iget-object v8, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v8}, Lcom/google/api/services/drive/Drive;->parents()Lcom/google/api/services/drive/Drive$Parents;

    move-result-object v8

    invoke-virtual {v2}, Lcom/google/api/services/drive/model/File;->getId()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Lcom/google/api/services/drive/model/ParentReference;

    invoke-direct {v10}, Lcom/google/api/services/drive/model/ParentReference;-><init>()V

    invoke-virtual {v10, p2}, Lcom/google/api/services/drive/model/ParentReference;->setId(Ljava/lang/String;)Lcom/google/api/services/drive/model/ParentReference;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lcom/google/api/services/drive/Drive$Parents;->insert(Ljava/lang/String;Lcom/google/api/services/drive/model/ParentReference;)Lcom/google/api/services/drive/Drive$Parents$Insert;

    move-result-object v1

    .line 201
    .local v1, "addParent":Lcom/google/api/services/drive/Drive$Parents$Insert;
    invoke-direct {p0, v1}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/api/services/drive/model/ParentReference;

    .line 202
    .local v7, "parent":Lcom/google/api/services/drive/model/ParentReference;
    invoke-virtual {v2}, Lcom/google/api/services/drive/model/File;->getParents()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 207
    .end local v1    # "addParent":Lcom/google/api/services/drive/Drive$Parents$Insert;
    .end local v7    # "parent":Lcom/google/api/services/drive/model/ParentReference;
    :cond_2
    :goto_2
    invoke-direct {p0, v2}, Lcom/samsung/scloud/GoogleDriveAPI;->parseDriveFile(Lcom/google/api/services/drive/model/File;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v8

    check-cast v8, Lcom/samsung/scloud/data/SCloudFolder;

    return-object v8

    .line 183
    :catch_0
    move-exception v4

    .line 184
    .local v4, "e":Ljava/io/IOException;
    const-string v8, "makeFolder - inserting into root folder failed"

    invoke-direct {p0, v4, v8}, Lcom/samsung/scloud/GoogleDriveAPI;->throwIOorAuthException(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0

    .line 194
    .end local v4    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v4

    .line 195
    .restart local v4    # "e":Ljava/io/IOException;
    const-string v8, "makeFolder - deleting root parent failed"

    invoke-direct {p0, v4, v8}, Lcom/samsung/scloud/GoogleDriveAPI;->throwIOorAuthException(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_1

    .line 203
    .end local v4    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v4

    .line 204
    .restart local v4    # "e":Ljava/io/IOException;
    const-string v8, "makeFolder - adding target folder as parent failed"

    invoke-direct {p0, v4, v8}, Lcom/samsung/scloud/GoogleDriveAPI;->throwIOorAuthException(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public move(Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/scloud/data/SCloudNode;
    .locals 9
    .param p1, "fromId"    # Ljava/lang/String;
    .param p2, "toId"    # Ljava/lang/String;
    .param p3, "overwrite"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudIOException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    .line 353
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    if-lt v7, v8, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v7, v8, :cond_1

    .line 354
    :cond_0
    new-instance v7, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v7}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v7

    .line 356
    :cond_1
    invoke-direct {p0}, Lcom/samsung/scloud/GoogleDriveAPI;->loadToken()V

    .line 360
    const/4 v4, 0x0

    .line 362
    .local v4, "fileMetadata":Lcom/google/api/services/drive/model/File;
    :try_start_0
    iget-object v7, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v7}, Lcom/google/api/services/drive/Drive;->files()Lcom/google/api/services/drive/Drive$Files;

    move-result-object v7

    invoke-virtual {v7, p1}, Lcom/google/api/services/drive/Drive$Files;->get(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$Get;

    move-result-object v3

    .line 363
    .local v3, "fileMetaRequest":Lcom/google/api/services/drive/Drive$Files$Get;
    invoke-direct {p0, v3}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/google/api/services/drive/model/File;

    move-object v4, v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 369
    .end local v3    # "fileMetaRequest":Lcom/google/api/services/drive/Drive$Files$Get;
    :goto_0
    if-eqz v4, :cond_2

    .line 370
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 371
    .local v5, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/api/services/drive/model/ParentReference;>;"
    new-instance v7, Lcom/google/api/services/drive/model/ParentReference;

    invoke-direct {v7}, Lcom/google/api/services/drive/model/ParentReference;-><init>()V

    invoke-virtual {v7, p2}, Lcom/google/api/services/drive/model/ParentReference;->setId(Ljava/lang/String;)Lcom/google/api/services/drive/model/ParentReference;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 372
    invoke-virtual {v4, v5}, Lcom/google/api/services/drive/model/File;->setParents(Ljava/util/List;)Lcom/google/api/services/drive/model/File;

    .line 378
    :try_start_1
    iget-object v7, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v7}, Lcom/google/api/services/drive/Drive;->files()Lcom/google/api/services/drive/Drive$Files;

    move-result-object v7

    invoke-virtual {v7, p1, v4}, Lcom/google/api/services/drive/Drive$Files;->update(Ljava/lang/String;Lcom/google/api/services/drive/model/File;)Lcom/google/api/services/drive/Drive$Files$Update;

    move-result-object v6

    .line 379
    .local v6, "updateRequest":Lcom/google/api/services/drive/Drive$Files$Update;
    invoke-direct {p0, v6}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/drive/model/File;

    .line 380
    .local v1, "copiedFile":Lcom/google/api/services/drive/model/File;
    invoke-direct {p0, v1}, Lcom/samsung/scloud/GoogleDriveAPI;->parseDriveFile(Lcom/google/api/services/drive/model/File;)Lcom/samsung/scloud/data/SCloudNode;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v7

    .line 385
    .end local v1    # "copiedFile":Lcom/google/api/services/drive/model/File;
    .end local v5    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/api/services/drive/model/ParentReference;>;"
    .end local v6    # "updateRequest":Lcom/google/api/services/drive/Drive$Files$Update;
    :goto_1
    return-object v7

    .line 364
    :catch_0
    move-exception v2

    .line 365
    .local v2, "e":Ljava/io/IOException;
    const-string v7, "move - getting file metadata failed"

    invoke-direct {p0, v2, v7}, Lcom/samsung/scloud/GoogleDriveAPI;->throwIOorAuthException(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0

    .line 381
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v5    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/api/services/drive/model/ParentReference;>;"
    :catch_1
    move-exception v2

    .line 382
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v7, "move - copying file to target folder failed"

    invoke-direct {p0, v2, v7}, Lcom/samsung/scloud/GoogleDriveAPI;->throwIOorAuthException(Ljava/lang/Exception;Ljava/lang/String;)V

    .line 385
    .end local v2    # "e":Ljava/io/IOException;
    .end local v5    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/api/services/drive/model/ParentReference;>;"
    :cond_2
    const/4 v7, 0x0

    goto :goto_1
.end method

.method public putFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 2
    .param p1, "sourceFile"    # Ljava/io/File;
    .param p2, "fileName"    # Ljava/lang/String;
    .param p3, "destinationFolderId"    # Ljava/lang/String;
    .param p4, "overwrite"    # Z
    .param p5, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudIOException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 559
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lt v0, v1, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-ge v0, v1, :cond_1

    .line 561
    :cond_0
    new-instance v0, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v0

    .line 563
    :cond_1
    invoke-direct {p0}, Lcom/samsung/scloud/GoogleDriveAPI;->loadToken()V

    .line 565
    if-eqz p4, :cond_2

    .line 566
    new-instance v0, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    const-string v1, "Google Drive not support the overwriting the file with the filename, you should overwrite the file with file Id"

    invoke-direct {v0, v1}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 568
    :cond_2
    invoke-virtual {p0, p1, p2, p3, p5}, Lcom/samsung/scloud/GoogleDriveAPI;->putNewFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;

    move-result-object v0

    return-object v0
.end method

.method public putFile(Ljava/io/File;Ljava/lang/String;ZLcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 14
    .param p1, "sourceFile"    # Ljava/io/File;
    .param p2, "destinationFileId"    # Ljava/lang/String;
    .param p3, "overwrite"    # Z
    .param p4, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudIOException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;
        }
    .end annotation

    .prologue
    .line 587
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v11

    const/4 v12, 0x1

    if-ge v11, v12, :cond_1

    .line 589
    :cond_0
    new-instance v11, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v11}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v11

    .line 591
    :cond_1
    invoke-direct {p0}, Lcom/samsung/scloud/GoogleDriveAPI;->loadToken()V

    .line 594
    const/4 v5, 0x0

    .line 596
    .local v5, "fileContent":Lcom/google/api/services/drive/model/File;
    const/4 v7, 0x0

    .line 597
    .local v7, "fis":Ljava/io/FileInputStream;
    invoke-static {}, Ljava/net/URLConnection;->getFileNameMap()Ljava/net/FileNameMap;

    move-result-object v11

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-interface {v11, v12}, Ljava/net/FileNameMap;->getContentTypeFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 600
    .local v2, "contentType":Ljava/lang/String;
    :try_start_0
    new-instance v7, Ljava/io/FileInputStream;

    .end local v7    # "fis":Ljava/io/FileInputStream;
    invoke-direct {v7, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 606
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    new-instance v8, Lcom/google/api/client/http/InputStreamContent;

    invoke-direct {v8, v2, v7}, Lcom/google/api/client/http/InputStreamContent;-><init>(Ljava/lang/String;Ljava/io/InputStream;)V

    .line 607
    .local v8, "mediaContent":Lcom/google/api/client/http/InputStreamContent;
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v12

    invoke-virtual {v8, v12, v13}, Lcom/google/api/client/http/InputStreamContent;->setLength(J)Lcom/google/api/client/http/InputStreamContent;

    .line 608
    invoke-virtual {v8, v2}, Lcom/google/api/client/http/InputStreamContent;->setType(Ljava/lang/String;)Lcom/google/api/client/http/InputStreamContent;

    .line 613
    :try_start_1
    iget-object v11, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v11}, Lcom/google/api/services/drive/Drive;->files()Lcom/google/api/services/drive/Drive$Files;

    move-result-object v11

    move-object/from16 v0, p2

    invoke-virtual {v11, v0}, Lcom/google/api/services/drive/Drive$Files;->get(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$Get;

    move-result-object v6

    .line 614
    .local v6, "fileMetaRequest":Lcom/google/api/services/drive/Drive$Files$Get;
    invoke-direct {p0, v6}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    move-object v0, v11

    check-cast v0, Lcom/google/api/services/drive/model/File;

    move-object v5, v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 621
    .end local v6    # "fileMetaRequest":Lcom/google/api/services/drive/Drive$Files$Get;
    :goto_0
    const/4 v10, 0x0

    .line 622
    .local v10, "updatedFile":Lcom/google/api/services/drive/model/File;
    if-eqz v5, :cond_2

    .line 625
    :try_start_2
    iget-object v11, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v11}, Lcom/google/api/services/drive/Drive;->files()Lcom/google/api/services/drive/Drive$Files;

    move-result-object v11

    move-object/from16 v0, p2

    invoke-virtual {v11, v0, v5, v8}, Lcom/google/api/services/drive/Drive$Files;->update(Ljava/lang/String;Lcom/google/api/services/drive/model/File;Lcom/google/api/client/http/AbstractInputStreamContent;)Lcom/google/api/services/drive/Drive$Files$Update;

    move-result-object v9

    .line 626
    .local v9, "updateReq":Lcom/google/api/services/drive/Drive$Files$Update;
    if-eqz p3, :cond_3

    .line 627
    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-virtual {v9, v11}, Lcom/google/api/services/drive/Drive$Files$Update;->setNewRevision(Ljava/lang/Boolean;)Lcom/google/api/services/drive/Drive$Files$Update;

    .line 630
    :goto_1
    invoke-virtual {v9}, Lcom/google/api/services/drive/Drive$Files$Update;->getMediaHttpUploader()Lcom/google/api/client/googleapis/media/MediaHttpUploader;

    move-result-object v11

    new-instance v12, Lcom/samsung/scloud/GoogleDriveAPI$2;

    move-object/from16 v0, p4

    invoke-direct {v12, p0, v0, p1}, Lcom/samsung/scloud/GoogleDriveAPI$2;-><init>(Lcom/samsung/scloud/GoogleDriveAPI;Lcom/samsung/scloud/response/ProgressListener;Ljava/io/File;)V

    invoke-virtual {v11, v12}, Lcom/google/api/client/googleapis/media/MediaHttpUploader;->setProgressListener(Lcom/google/api/client/googleapis/media/MediaHttpUploaderProgressListener;)Lcom/google/api/client/googleapis/media/MediaHttpUploader;

    move-result-object v11

    const/high16 v12, 0x40000

    invoke-virtual {v11, v12}, Lcom/google/api/client/googleapis/media/MediaHttpUploader;->setChunkSize(I)Lcom/google/api/client/googleapis/media/MediaHttpUploader;

    .line 648
    invoke-direct {p0, v9}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    move-object v0, v11

    check-cast v0, Lcom/google/api/services/drive/model/File;

    move-object v10, v0

    .line 649
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 660
    .end local v9    # "updateReq":Lcom/google/api/services/drive/Drive$Files$Update;
    :cond_2
    :goto_2
    invoke-direct {p0, v10}, Lcom/samsung/scloud/GoogleDriveAPI;->parseDriveFile(Lcom/google/api/services/drive/model/File;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v11

    check-cast v11, Lcom/samsung/scloud/data/SCloudFile;

    return-object v11

    .line 601
    .end local v7    # "fis":Ljava/io/FileInputStream;
    .end local v8    # "mediaContent":Lcom/google/api/client/http/InputStreamContent;
    .end local v10    # "updatedFile":Lcom/google/api/services/drive/model/File;
    :catch_0
    move-exception v4

    .line 602
    .local v4, "e1":Ljava/io/FileNotFoundException;
    invoke-virtual {v4}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 603
    new-instance v11, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "putFile - source file does not exist for "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 615
    .end local v4    # "e1":Ljava/io/FileNotFoundException;
    .restart local v7    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "mediaContent":Lcom/google/api/client/http/InputStreamContent;
    :catch_1
    move-exception v4

    .line 616
    .local v4, "e1":Ljava/io/IOException;
    const-string v11, "putFile - getting file metadata failed"

    invoke-direct {p0, v4, v11}, Lcom/samsung/scloud/GoogleDriveAPI;->throwIOorAuthException(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0

    .line 629
    .end local v4    # "e1":Ljava/io/IOException;
    .restart local v9    # "updateReq":Lcom/google/api/services/drive/Drive$Files$Update;
    .restart local v10    # "updatedFile":Lcom/google/api/services/drive/model/File;
    :cond_3
    const/4 v11, 0x1

    :try_start_3
    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-virtual {v9, v11}, Lcom/google/api/services/drive/Drive$Files$Update;->setNewRevision(Ljava/lang/Boolean;)Lcom/google/api/services/drive/Drive$Files$Update;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 650
    .end local v9    # "updateReq":Lcom/google/api/services/drive/Drive$Files$Update;
    :catch_2
    move-exception v3

    .line 652
    .local v3, "e":Ljava/io/IOException;
    :try_start_4
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 657
    const-string v11, "putFile - closing fileInputStream failed"

    invoke-direct {p0, v3, v11}, Lcom/samsung/scloud/GoogleDriveAPI;->throwIOorAuthException(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_2

    .line 653
    :catch_3
    move-exception v4

    .line 654
    .restart local v4    # "e1":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    .line 655
    new-instance v11, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "putFile - close file failed"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v4}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v11
.end method

.method public putFile(Ljava/io/File;Ljava/lang/String;ZLjava/lang/String;Lcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 1
    .param p1, "sourceFile"    # Ljava/io/File;
    .param p2, "destinationFolderIdOrPath"    # Ljava/lang/String;
    .param p3, "overwrite"    # Z
    .param p4, "parentRev"    # Ljava/lang/String;
    .param p5, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2164
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public putFile(Ljava/io/FileInputStream;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 1
    .param p1, "sourceFileInputStream"    # Ljava/io/FileInputStream;
    .param p2, "filename"    # Ljava/lang/String;
    .param p3, "destinationFolderId"    # Ljava/lang/String;
    .param p4, "overwrite"    # Z
    .param p5, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2155
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public putNewFile(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/scloud/response/ProgressListener;)Lcom/samsung/scloud/data/SCloudFile;
    .locals 20
    .param p1, "sourceFile"    # Ljava/io/File;
    .param p2, "fileName"    # Ljava/lang/String;
    .param p3, "destinationFolderId"    # Ljava/lang/String;
    .param p4, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudIOException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;
        }
    .end annotation

    .prologue
    .line 474
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-lt v15, v0, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ge v15, v0, :cond_1

    .line 476
    :cond_0
    new-instance v15, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v15}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v15

    .line 478
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/samsung/scloud/GoogleDriveAPI;->loadToken()V

    .line 481
    const/4 v12, 0x0

    .line 482
    .local v12, "mediaContent":Lcom/google/api/client/http/InputStreamContent;
    const/4 v8, 0x0

    .line 483
    .local v8, "fis":Ljava/io/FileInputStream;
    invoke-static {}, Ljava/net/URLConnection;->getFileNameMap()Ljava/net/FileNameMap;

    move-result-object v15

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v16

    invoke-interface/range {v15 .. v16}, Ljava/net/FileNameMap;->getContentTypeFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 486
    .local v4, "contentType":Ljava/lang/String;
    :try_start_0
    new-instance v9, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v9, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 487
    .end local v8    # "fis":Ljava/io/FileInputStream;
    .local v9, "fis":Ljava/io/FileInputStream;
    :try_start_1
    const-string v15, "SCLOUD_SDK"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "[putNewFile]startPutFile filename="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " size="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v18

    move-object/from16 v0, v16

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_3

    .line 493
    new-instance v12, Lcom/google/api/client/http/InputStreamContent;

    .end local v12    # "mediaContent":Lcom/google/api/client/http/InputStreamContent;
    invoke-direct {v12, v4, v9}, Lcom/google/api/client/http/InputStreamContent;-><init>(Ljava/lang/String;Ljava/io/InputStream;)V

    .line 494
    .restart local v12    # "mediaContent":Lcom/google/api/client/http/InputStreamContent;
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v12, v0, v1}, Lcom/google/api/client/http/InputStreamContent;->setLength(J)Lcom/google/api/client/http/InputStreamContent;

    .line 495
    invoke-virtual {v12, v4}, Lcom/google/api/client/http/InputStreamContent;->setType(Ljava/lang/String;)Lcom/google/api/client/http/InputStreamContent;

    .line 497
    new-instance v7, Lcom/google/api/services/drive/model/File;

    invoke-direct {v7}, Lcom/google/api/services/drive/model/File;-><init>()V

    .line 498
    .local v7, "fileContent":Lcom/google/api/services/drive/model/File;
    move-object/from16 v0, p2

    invoke-virtual {v7, v0}, Lcom/google/api/services/drive/model/File;->setTitle(Ljava/lang/String;)Lcom/google/api/services/drive/model/File;

    .line 499
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->length()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-virtual {v7, v15}, Lcom/google/api/services/drive/model/File;->setFileSize(Ljava/lang/Long;)Lcom/google/api/services/drive/model/File;

    .line 500
    invoke-virtual {v7, v4}, Lcom/google/api/services/drive/model/File;->setMimeType(Ljava/lang/String;)Lcom/google/api/services/drive/model/File;

    .line 501
    new-instance v13, Lcom/google/api/services/drive/model/ParentReference;

    invoke-direct {v13}, Lcom/google/api/services/drive/model/ParentReference;-><init>()V

    .line 502
    .local v13, "parent":Lcom/google/api/services/drive/model/ParentReference;
    const-string v15, "drive#parentReference"

    invoke-virtual {v13, v15}, Lcom/google/api/services/drive/model/ParentReference;->setKind(Ljava/lang/String;)Lcom/google/api/services/drive/model/ParentReference;

    .line 503
    move-object/from16 v0, p3

    invoke-virtual {v13, v0}, Lcom/google/api/services/drive/model/ParentReference;->setId(Ljava/lang/String;)Lcom/google/api/services/drive/model/ParentReference;

    .line 504
    const/4 v15, 0x1

    new-array v15, v15, [Lcom/google/api/services/drive/model/ParentReference;

    const/16 v16, 0x0

    aput-object v13, v15, v16

    invoke-static {v15}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v15

    invoke-virtual {v7, v15}, Lcom/google/api/services/drive/model/File;->setParents(Ljava/util/List;)Lcom/google/api/services/drive/model/File;

    .line 508
    const/4 v11, 0x0

    .line 509
    .local v11, "insertedFile":Lcom/google/api/services/drive/model/File;
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v14

    .line 512
    .local v14, "uploadName":Ljava/lang/String;
    :try_start_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v15}, Lcom/google/api/services/drive/Drive;->files()Lcom/google/api/services/drive/Drive$Files;

    move-result-object v15

    invoke-virtual {v15, v7, v12}, Lcom/google/api/services/drive/Drive$Files;->insert(Lcom/google/api/services/drive/model/File;Lcom/google/api/client/http/AbstractInputStreamContent;)Lcom/google/api/services/drive/Drive$Files$Insert;

    move-result-object v10

    .line 513
    .local v10, "insertRequest":Lcom/google/api/services/drive/Drive$Files$Insert;
    invoke-virtual {v10}, Lcom/google/api/services/drive/Drive$Files$Insert;->getMediaHttpUploader()Lcom/google/api/client/googleapis/media/MediaHttpUploader;

    move-result-object v15

    new-instance v16, Lcom/samsung/scloud/GoogleDriveAPI$1;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, p4

    move-object/from16 v3, p2

    invoke-direct {v0, v1, v14, v2, v3}, Lcom/samsung/scloud/GoogleDriveAPI$1;-><init>(Lcom/samsung/scloud/GoogleDriveAPI;Ljava/lang/String;Lcom/samsung/scloud/response/ProgressListener;Ljava/lang/String;)V

    invoke-virtual/range {v15 .. v16}, Lcom/google/api/client/googleapis/media/MediaHttpUploader;->setProgressListener(Lcom/google/api/client/googleapis/media/MediaHttpUploaderProgressListener;)Lcom/google/api/client/googleapis/media/MediaHttpUploader;

    move-result-object v15

    const/high16 v16, 0x280000

    invoke-virtual/range {v15 .. v16}, Lcom/google/api/client/googleapis/media/MediaHttpUploader;->setChunkSize(I)Lcom/google/api/client/googleapis/media/MediaHttpUploader;

    .line 528
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    move-object v0, v15

    check-cast v0, Lcom/google/api/services/drive/model/File;

    move-object v11, v0

    .line 529
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 538
    .end local v10    # "insertRequest":Lcom/google/api/services/drive/Drive$Files$Insert;
    :goto_0
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/samsung/scloud/GoogleDriveAPI;->parseDriveFile(Lcom/google/api/services/drive/model/File;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v15

    check-cast v15, Lcom/samsung/scloud/data/SCloudFile;

    return-object v15

    .line 488
    .end local v7    # "fileContent":Lcom/google/api/services/drive/model/File;
    .end local v9    # "fis":Ljava/io/FileInputStream;
    .end local v11    # "insertedFile":Lcom/google/api/services/drive/model/File;
    .end local v13    # "parent":Lcom/google/api/services/drive/model/ParentReference;
    .end local v14    # "uploadName":Ljava/lang/String;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    :catch_0
    move-exception v5

    .line 489
    .local v5, "e":Ljava/io/FileNotFoundException;
    :goto_1
    invoke-virtual {v5}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 490
    new-instance v15, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "putNewFile - source file does not exist for "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v15

    .line 530
    .end local v5    # "e":Ljava/io/FileNotFoundException;
    .end local v8    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "fileContent":Lcom/google/api/services/drive/model/File;
    .restart local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v11    # "insertedFile":Lcom/google/api/services/drive/model/File;
    .restart local v13    # "parent":Lcom/google/api/services/drive/model/ParentReference;
    .restart local v14    # "uploadName":Ljava/lang/String;
    :catch_1
    move-exception v5

    .line 532
    .local v5, "e":Ljava/io/IOException;
    :try_start_3
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 536
    :goto_2
    const-string v15, "putNewFile - upload file failed"

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v15}, Lcom/samsung/scloud/GoogleDriveAPI;->throwIOorAuthException(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0

    .line 533
    :catch_2
    move-exception v6

    .line 534
    .local v6, "e1":Ljava/io/IOException;
    const-string v15, "putNewFile - closing fileInoutStream failed"

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v15}, Lcom/samsung/scloud/GoogleDriveAPI;->throwIOorAuthException(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_2

    .line 488
    .end local v5    # "e":Ljava/io/IOException;
    .end local v6    # "e1":Ljava/io/IOException;
    .end local v7    # "fileContent":Lcom/google/api/services/drive/model/File;
    .end local v11    # "insertedFile":Lcom/google/api/services/drive/model/File;
    .end local v13    # "parent":Lcom/google/api/services/drive/model/ParentReference;
    .end local v14    # "uploadName":Ljava/lang/String;
    :catch_3
    move-exception v5

    move-object v8, v9

    .end local v9    # "fis":Ljava/io/FileInputStream;
    .restart local v8    # "fis":Ljava/io/FileInputStream;
    goto :goto_1
.end method

.method public removeCollaboration(J)V
    .locals 1
    .param p1, "collaborationId"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 1088
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public removeCollaboration(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "Id"    # Ljava/lang/String;
    .param p2, "collaborationId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 1102
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v2, v3, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v2, v3, :cond_1

    .line 1103
    :cond_0
    new-instance v2, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v2}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v2

    .line 1105
    :cond_1
    invoke-direct {p0}, Lcom/samsung/scloud/GoogleDriveAPI;->loadToken()V

    .line 1108
    :try_start_0
    iget-object v2, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v2}, Lcom/google/api/services/drive/Drive;->permissions()Lcom/google/api/services/drive/Drive$Permissions;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Lcom/google/api/services/drive/Drive$Permissions;->delete(Ljava/lang/String;Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Permissions$Delete;

    move-result-object v0

    .line 1109
    .local v0, "deletePerm":Lcom/google/api/services/drive/Drive$Permissions$Delete;
    invoke-direct {p0, v0}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1114
    return-void

    .line 1110
    .end local v0    # "deletePerm":Lcom/google/api/services/drive/Drive$Permissions$Delete;
    :catch_0
    move-exception v1

    .line 1111
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 1112
    new-instance v2, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v3, "removeCollaboration - remove Collaboration failed.."

    invoke-direct {v2, v3}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public removeComment(Ljava/lang/String;J)V
    .locals 1
    .param p1, "Id"    # Ljava/lang/String;
    .param p2, "commentID"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2405
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public removeShareURL(Ljava/lang/String;)Z
    .locals 1
    .param p1, "Id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2429
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public rename(Ljava/lang/String;Ljava/lang/String;Z)Lcom/samsung/scloud/data/SCloudNode;
    .locals 8
    .param p1, "Id"    # Ljava/lang/String;
    .param p2, "newName"    # Ljava/lang/String;
    .param p3, "overwrite"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudIOException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 401
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    if-lt v6, v7, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v6, v7, :cond_1

    .line 402
    :cond_0
    new-instance v6, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v6}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v6

    .line 404
    :cond_1
    invoke-direct {p0}, Lcom/samsung/scloud/GoogleDriveAPI;->loadToken()V

    .line 408
    const/4 v3, 0x0

    .line 410
    .local v3, "fileMetadata":Lcom/google/api/services/drive/model/File;
    :try_start_0
    iget-object v6, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v6}, Lcom/google/api/services/drive/Drive;->files()Lcom/google/api/services/drive/Drive$Files;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/google/api/services/drive/Drive$Files;->get(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$Get;

    move-result-object v2

    .line 411
    .local v2, "fileMetaRequest":Lcom/google/api/services/drive/Drive$Files$Get;
    invoke-direct {p0, v2}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lcom/google/api/services/drive/model/File;

    move-object v3, v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 417
    .end local v2    # "fileMetaRequest":Lcom/google/api/services/drive/Drive$Files$Get;
    :goto_0
    if-eqz v3, :cond_2

    .line 418
    invoke-virtual {v3, p2}, Lcom/google/api/services/drive/model/File;->setTitle(Ljava/lang/String;)Lcom/google/api/services/drive/model/File;

    .line 424
    :try_start_1
    iget-object v6, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v6}, Lcom/google/api/services/drive/Drive;->files()Lcom/google/api/services/drive/Drive$Files;

    move-result-object v6

    invoke-virtual {v6, p1, v3}, Lcom/google/api/services/drive/Drive$Files;->update(Ljava/lang/String;Lcom/google/api/services/drive/model/File;)Lcom/google/api/services/drive/Drive$Files$Update;

    move-result-object v5

    .line 425
    .local v5, "updateRequest":Lcom/google/api/services/drive/Drive$Files$Update;
    invoke-direct {p0, v5}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/api/services/drive/model/File;

    .line 426
    .local v4, "renamedFile":Lcom/google/api/services/drive/model/File;
    invoke-direct {p0, v4}, Lcom/samsung/scloud/GoogleDriveAPI;->parseDriveFile(Lcom/google/api/services/drive/model/File;)Lcom/samsung/scloud/data/SCloudNode;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v6

    .line 431
    .end local v4    # "renamedFile":Lcom/google/api/services/drive/model/File;
    .end local v5    # "updateRequest":Lcom/google/api/services/drive/Drive$Files$Update;
    :goto_1
    return-object v6

    .line 412
    :catch_0
    move-exception v1

    .line 413
    .local v1, "e":Ljava/io/IOException;
    const-string v6, "rename - getting file metadata failed"

    invoke-direct {p0, v1, v6}, Lcom/samsung/scloud/GoogleDriveAPI;->throwIOorAuthException(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0

    .line 427
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 428
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v6, "rename - copying file to target folder failed"

    invoke-direct {p0, v1, v6}, Lcom/samsung/scloud/GoogleDriveAPI;->throwIOorAuthException(Ljava/lang/Exception;Ljava/lang/String;)V

    .line 431
    .end local v1    # "e":Ljava/io/IOException;
    :cond_2
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public restoreFromRecycleBin(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "recycledBinId"    # Ljava/lang/String;
    .param p2, "Id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2332
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public restoreFromTrash(Ljava/lang/String;)V
    .locals 4
    .param p1, "Id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;
        }
    .end annotation

    .prologue
    .line 1476
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    if-ge v2, v3, :cond_1

    .line 1477
    :cond_0
    new-instance v2, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v2}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v2

    .line 1479
    :cond_1
    invoke-direct {p0}, Lcom/samsung/scloud/GoogleDriveAPI;->loadToken()V

    .line 1483
    :try_start_0
    iget-object v2, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v2}, Lcom/google/api/services/drive/Drive;->files()Lcom/google/api/services/drive/Drive$Files;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/api/services/drive/Drive$Files;->untrash(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$Untrash;

    move-result-object v1

    .line 1484
    .local v1, "unTrashRequest":Lcom/google/api/services/drive/Drive$Files$Untrash;
    invoke-direct {p0, v1}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1489
    return-void

    .line 1485
    .end local v1    # "unTrashRequest":Lcom/google/api/services/drive/Drive$Files$Untrash;
    :catch_0
    move-exception v0

    .line 1486
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 1487
    new-instance v2, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v3, "restoreFromRecycleBin - restore failed"

    invoke-direct {v2, v3}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public search(Ljava/lang/String;ILjava/lang/String;)Lcom/samsung/scloud/data/SearchResult;
    .locals 11
    .param p1, "keyWord"    # Ljava/lang/String;
    .param p2, "maxResults"    # I
    .param p3, "nextToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;,
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;
        }
    .end annotation

    .prologue
    .line 1269
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_1

    :cond_0
    new-instance v8, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v8}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v8

    .line 1270
    :cond_1
    invoke-direct {p0}, Lcom/samsung/scloud/GoogleDriveAPI;->loadToken()V

    .line 1272
    const/4 v4, 0x0

    .line 1273
    .local v4, "list":Lcom/google/api/services/drive/Drive$Files$List;
    const/4 v7, 0x0

    .line 1275
    .local v7, "searchList":Lcom/google/api/services/drive/model/FileList;
    :try_start_0
    iget-object v8, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v8}, Lcom/google/api/services/drive/Drive;->files()Lcom/google/api/services/drive/Drive$Files;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/api/services/drive/Drive$Files;->list()Lcom/google/api/services/drive/Drive$Files$List;

    move-result-object v4

    .line 1276
    const/4 v8, 0x1

    if-le p2, v8, :cond_2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/google/api/services/drive/Drive$Files$List;->setMaxResults(Ljava/lang/Integer;)Lcom/google/api/services/drive/Drive$Files$List;

    .line 1277
    :cond_2
    if-eqz p3, :cond_3

    invoke-virtual {v4, p3}, Lcom/google/api/services/drive/Drive$Files$List;->setPageToken(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$List;

    .line 1278
    :cond_3
    invoke-virtual {v4, p1}, Lcom/google/api/services/drive/Drive$Files$List;->setQ(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$List;

    .line 1279
    invoke-direct {p0, v4}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "searchList":Lcom/google/api/services/drive/model/FileList;
    check-cast v7, Lcom/google/api/services/drive/model/FileList;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudAuthException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/scloud/exception/SCloudIOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 1290
    .restart local v7    # "searchList":Lcom/google/api/services/drive/model/FileList;
    new-instance v6, Lcom/samsung/scloud/data/SearchResult;

    invoke-direct {v6}, Lcom/samsung/scloud/data/SearchResult;-><init>()V

    .line 1291
    .local v6, "searchInfo":Lcom/samsung/scloud/data/SearchResult;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1292
    .local v5, "seachFileInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/SCloudNode;>;"
    invoke-virtual {v7}, Lcom/google/api/services/drive/model/FileList;->getEtag()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/samsung/scloud/data/SearchResult;->setEtag(Ljava/lang/String;)V

    .line 1293
    invoke-virtual {v7}, Lcom/google/api/services/drive/model/FileList;->getNextPageToken()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/samsung/scloud/data/SearchResult;->setNextPageToken(Ljava/lang/String;)V

    .line 1294
    invoke-virtual {v7}, Lcom/google/api/services/drive/model/FileList;->getNextLink()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/samsung/scloud/data/SearchResult;->setNextLink(Ljava/lang/String;)V

    .line 1296
    invoke-virtual {v7}, Lcom/google/api/services/drive/model/FileList;->getItems()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/drive/model/File;

    .line 1297
    .local v1, "f":Lcom/google/api/services/drive/model/File;
    invoke-direct {p0, v1}, Lcom/samsung/scloud/GoogleDriveAPI;->parseDriveFile(Lcom/google/api/services/drive/model/File;)Lcom/samsung/scloud/data/SCloudNode;

    move-result-object v2

    .line 1298
    .local v2, "filemeta":Lcom/samsung/scloud/data/SCloudNode;
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1280
    .end local v1    # "f":Lcom/google/api/services/drive/model/File;
    .end local v2    # "filemeta":Lcom/samsung/scloud/data/SCloudNode;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v5    # "seachFileInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/SCloudNode;>;"
    .end local v6    # "searchInfo":Lcom/samsung/scloud/data/SearchResult;
    .end local v7    # "searchList":Lcom/google/api/services/drive/model/FileList;
    :catch_0
    move-exception v0

    .line 1281
    .local v0, "e":Lcom/samsung/scloud/exception/SCloudAuthException;
    invoke-virtual {v0}, Lcom/samsung/scloud/exception/SCloudAuthException;->printStackTrace()V

    .line 1282
    new-instance v8, Lcom/samsung/scloud/exception/SCloudAuthException;

    invoke-direct {v8}, Lcom/samsung/scloud/exception/SCloudAuthException;-><init>()V

    throw v8

    .line 1283
    .end local v0    # "e":Lcom/samsung/scloud/exception/SCloudAuthException;
    :catch_1
    move-exception v0

    .line 1284
    .local v0, "e":Lcom/samsung/scloud/exception/SCloudIOException;
    invoke-virtual {v0}, Lcom/samsung/scloud/exception/SCloudIOException;->printStackTrace()V

    .line 1285
    new-instance v8, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "search - Failed to search files"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Lcom/samsung/scloud/exception/SCloudIOException;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 1286
    .end local v0    # "e":Lcom/samsung/scloud/exception/SCloudIOException;
    :catch_2
    move-exception v0

    .line 1287
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 1288
    new-instance v8, Lcom/samsung/scloud/exception/SCloudIOException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "search - Failed to search files"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 1300
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v5    # "seachFileInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/scloud/data/SCloudNode;>;"
    .restart local v6    # "searchInfo":Lcom/samsung/scloud/data/SearchResult;
    .restart local v7    # "searchList":Lcom/google/api/services/drive/model/FileList;
    :cond_4
    invoke-virtual {v6, v5}, Lcom/samsung/scloud/data/SearchResult;->setSearchResults(Ljava/util/List;)V

    .line 1301
    return-object v6
.end method

.method public search(Ljava/lang/String;Ljava/lang/String;IZ)Lcom/samsung/scloud/data/SearchResult;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "fileLimit"    # I
    .param p4, "includeDeleted"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2361
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public searchFolder(Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .param p1, "folderName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/scloud/data/SCloudNode;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;,
            Lcom/samsung/scloud/exception/SCloudIOException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1656
    if-nez p1, :cond_0

    new-instance v2, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v2}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v2

    .line 1657
    :cond_0
    invoke-direct {p0}, Lcom/samsung/scloud/GoogleDriveAPI;->loadToken()V

    .line 1658
    const/4 v1, 0x0

    .line 1660
    .local v1, "result":Lcom/samsung/scloud/data/SearchResult;
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mimeType = \'application/vnd.google-apps.folder\' and title = \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x1f4

    const/4 v5, 0x0

    invoke-virtual {p0, v3, v4, v5}, Lcom/samsung/scloud/GoogleDriveAPI;->search(Ljava/lang/String;ILjava/lang/String;)Lcom/samsung/scloud/data/SearchResult;
    :try_end_0
    .catch Lcom/samsung/scloud/exception/SCloudAuthException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/scloud/exception/SCloudIOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/samsung/scloud/exception/SCloudInvalidParameterException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v1

    .line 1671
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/samsung/scloud/data/SearchResult;->getSearchResults()Ljava/util/List;

    move-result-object v2

    .line 1672
    :cond_1
    return-object v2

    .line 1661
    :catch_0
    move-exception v0

    .line 1662
    .local v0, "e":Lcom/samsung/scloud/exception/SCloudAuthException;
    invoke-virtual {v0}, Lcom/samsung/scloud/exception/SCloudAuthException;->printStackTrace()V

    .line 1663
    throw v0

    .line 1664
    .end local v0    # "e":Lcom/samsung/scloud/exception/SCloudAuthException;
    :catch_1
    move-exception v0

    .line 1665
    .local v0, "e":Lcom/samsung/scloud/exception/SCloudIOException;
    invoke-virtual {v0}, Lcom/samsung/scloud/exception/SCloudIOException;->printStackTrace()V

    .line 1666
    throw v0

    .line 1667
    .end local v0    # "e":Lcom/samsung/scloud/exception/SCloudIOException;
    :catch_2
    move-exception v0

    .line 1668
    .local v0, "e":Lcom/samsung/scloud/exception/SCloudInvalidParameterException;
    invoke-virtual {v0}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;->printStackTrace()V

    .line 1669
    throw v0
.end method

.method public setActiveRepository(Ljava/lang/String;)V
    .locals 1
    .param p1, "repositoryName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2145
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public setAuthToken(Ljava/lang/String;)V
    .locals 5
    .param p1, "token"    # Ljava/lang/String;

    .prologue
    .line 1350
    iput-object p1, p0, Lcom/samsung/scloud/GoogleDriveAPI;->accessToken:Ljava/lang/String;

    .line 1352
    iget-object v2, p0, Lcom/samsung/scloud/GoogleDriveAPI;->mContext:Landroid/content/Context;

    const-string v3, "googleCredential"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 1353
    .local v1, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1354
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "accessToken"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1355
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1357
    iget-object v2, p0, Lcom/samsung/scloud/GoogleDriveAPI;->credential:Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/scloud/GoogleDriveAPI;->credential:Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx;

    invoke-virtual {v2, p1}, Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx;->setAccessToken(Ljava/lang/String;)Lcom/google/api/client/googleapis/auth/oauth2/GoogleCredential;

    .line 1358
    :cond_0
    return-void
.end method

.method public setDescription(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "Id"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudIOException;,
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 981
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-lt v5, v6, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v5, v6, :cond_1

    .line 982
    :cond_0
    new-instance v5, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v5}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v5

    .line 984
    :cond_1
    invoke-direct {p0}, Lcom/samsung/scloud/GoogleDriveAPI;->loadToken()V

    .line 986
    const/4 v2, 0x0

    .line 988
    .local v2, "fileInfo":Lcom/google/api/services/drive/model/File;
    :try_start_0
    iget-object v5, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v5}, Lcom/google/api/services/drive/Drive;->files()Lcom/google/api/services/drive/Drive$Files;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/google/api/services/drive/Drive$Files;->get(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$Get;

    move-result-object v3

    .line 989
    .local v3, "getFileMetaRequest":Lcom/google/api/services/drive/Drive$Files$Get;
    invoke-direct {p0, v3}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "fileInfo":Lcom/google/api/services/drive/model/File;
    check-cast v2, Lcom/google/api/services/drive/model/File;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 994
    .restart local v2    # "fileInfo":Lcom/google/api/services/drive/model/File;
    if-eqz v2, :cond_2

    .line 995
    invoke-virtual {v2, p2}, Lcom/google/api/services/drive/model/File;->setDescription(Ljava/lang/String;)Lcom/google/api/services/drive/model/File;

    .line 999
    :try_start_1
    iget-object v5, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v5}, Lcom/google/api/services/drive/Drive;->files()Lcom/google/api/services/drive/Drive$Files;

    move-result-object v5

    invoke-virtual {v5, p1, v2}, Lcom/google/api/services/drive/Drive$Files;->update(Ljava/lang/String;Lcom/google/api/services/drive/model/File;)Lcom/google/api/services/drive/Drive$Files$Update;

    move-result-object v4

    .line 1000
    .local v4, "updateRequest":Lcom/google/api/services/drive/Drive$Files$Update;
    invoke-direct {p0, v4}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1007
    .end local v4    # "updateRequest":Lcom/google/api/services/drive/Drive$Files$Update;
    :cond_2
    return-void

    .line 990
    .end local v2    # "fileInfo":Lcom/google/api/services/drive/model/File;
    .end local v3    # "getFileMetaRequest":Lcom/google/api/services/drive/Drive$Files$Get;
    :catch_0
    move-exception v1

    .line 991
    .local v1, "e1":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 992
    new-instance v5, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v6, "setDescription - getting file metadata failed"

    invoke-direct {v5, v6}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1001
    .end local v1    # "e1":Ljava/io/IOException;
    .restart local v2    # "fileInfo":Lcom/google/api/services/drive/model/File;
    .restart local v3    # "getFileMetaRequest":Lcom/google/api/services/drive/Drive$Files$Get;
    :catch_1
    move-exception v0

    .line 1002
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 1003
    new-instance v5, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v6, "setDescription - setting file description failed"

    invoke-direct {v5, v6}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v5
.end method

.method public setDeviceProfile(Lcom/samsung/scloud/data/Device;)V
    .locals 1
    .param p1, "deviceProfile"    # Lcom/samsung/scloud/data/Device;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2188
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public setDeviceProfile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2196
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public setRefreshToken(Ljava/lang/String;)V
    .locals 4
    .param p1, "token"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 1497
    iput-object p1, p0, Lcom/samsung/scloud/GoogleDriveAPI;->refreshToken:Ljava/lang/String;

    .line 1498
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/scloud/GoogleDriveAPI;->hasRefreshToken:Z

    .line 1500
    iget-object v1, p0, Lcom/samsung/scloud/GoogleDriveAPI;->credential:Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx;

    if-eqz v1, :cond_0

    .line 1501
    iget-object v1, p0, Lcom/samsung/scloud/GoogleDriveAPI;->credential:Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx;

    invoke-virtual {v1, p1}, Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx;->setRefreshToken(Ljava/lang/String;)Lcom/google/api/client/googleapis/auth/oauth2/GoogleCredential;

    .line 1504
    :try_start_0
    iget-object v1, p0, Lcom/samsung/scloud/GoogleDriveAPI;->credential:Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx;

    invoke-virtual {v1}, Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx;->refreshToken()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1510
    iget-object v1, p0, Lcom/samsung/scloud/GoogleDriveAPI;->credential:Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx;

    invoke-virtual {v1}, Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx;->getAccessToken()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1511
    const-string v1, "SCLOUD_SDK"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "new Access Token="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/scloud/GoogleDriveAPI;->credential:Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx;

    invoke-virtual {v3}, Lcom/samsung/scloud/auth/gdrive/GoogleCredentialEx;->getAccessToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1512
    new-instance v1, Lcom/samsung/scloud/exception/SCloudException;

    const-string v2, "Cannot get Access token"

    invoke-direct {v1, v2}, Lcom/samsung/scloud/exception/SCloudException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1505
    :catch_0
    move-exception v0

    .line 1506
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "SCLOUD_SDK"

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1507
    new-instance v1, Lcom/samsung/scloud/exception/SCloudException;

    const-string v2, "Token refresh Failed"

    invoke-direct {v1, v2}, Lcom/samsung/scloud/exception/SCloudException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1515
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    const-string v1, "SCLOUD_SDK"

    const-string v2, "credential is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1516
    new-instance v1, Lcom/samsung/scloud/exception/SCloudException;

    const-string v2, "Credential is null"

    invoke-direct {v1, v2}, Lcom/samsung/scloud/exception/SCloudException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1518
    :cond_1
    return-void
.end method

.method public startAuth(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2284
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public unsetDescription(Ljava/lang/String;)V
    .locals 7
    .param p1, "Id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudIOException;,
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;
        }
    .end annotation

    .prologue
    .line 1021
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x1

    if-ge v5, v6, :cond_1

    .line 1022
    :cond_0
    new-instance v5, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    invoke-direct {v5}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>()V

    throw v5

    .line 1024
    :cond_1
    invoke-direct {p0}, Lcom/samsung/scloud/GoogleDriveAPI;->loadToken()V

    .line 1026
    const/4 v2, 0x0

    .line 1028
    .local v2, "fileInfo":Lcom/google/api/services/drive/model/File;
    :try_start_0
    iget-object v5, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v5}, Lcom/google/api/services/drive/Drive;->files()Lcom/google/api/services/drive/Drive$Files;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/google/api/services/drive/Drive$Files;->get(Ljava/lang/String;)Lcom/google/api/services/drive/Drive$Files$Get;

    move-result-object v3

    .line 1029
    .local v3, "getFileMetaRequest":Lcom/google/api/services/drive/Drive$Files$Get;
    invoke-direct {p0, v3}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "fileInfo":Lcom/google/api/services/drive/model/File;
    check-cast v2, Lcom/google/api/services/drive/model/File;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1034
    .restart local v2    # "fileInfo":Lcom/google/api/services/drive/model/File;
    if-eqz v2, :cond_2

    .line 1035
    const-string v5, ""

    invoke-virtual {v2, v5}, Lcom/google/api/services/drive/model/File;->setDescription(Ljava/lang/String;)Lcom/google/api/services/drive/model/File;

    .line 1039
    :try_start_1
    iget-object v5, p0, Lcom/samsung/scloud/GoogleDriveAPI;->driveInstance:Lcom/google/api/services/drive/Drive;

    invoke-virtual {v5}, Lcom/google/api/services/drive/Drive;->files()Lcom/google/api/services/drive/Drive$Files;

    move-result-object v5

    invoke-virtual {v5, p1, v2}, Lcom/google/api/services/drive/Drive$Files;->update(Ljava/lang/String;Lcom/google/api/services/drive/model/File;)Lcom/google/api/services/drive/Drive$Files$Update;

    move-result-object v4

    .line 1040
    .local v4, "updateRequest":Lcom/google/api/services/drive/Drive$Files$Update;
    invoke-direct {p0, v4}, Lcom/samsung/scloud/GoogleDriveAPI;->safeExecute(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1047
    .end local v4    # "updateRequest":Lcom/google/api/services/drive/Drive$Files$Update;
    :cond_2
    return-void

    .line 1030
    .end local v2    # "fileInfo":Lcom/google/api/services/drive/model/File;
    .end local v3    # "getFileMetaRequest":Lcom/google/api/services/drive/Drive$Files$Get;
    :catch_0
    move-exception v1

    .line 1031
    .local v1, "e1":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 1032
    new-instance v5, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v6, "unsetDescription - getting file metadata failed"

    invoke-direct {v5, v6}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1041
    .end local v1    # "e1":Ljava/io/IOException;
    .restart local v2    # "fileInfo":Lcom/google/api/services/drive/model/File;
    .restart local v3    # "getFileMetaRequest":Lcom/google/api/services/drive/Drive$Files$Get;
    :catch_1
    move-exception v0

    .line 1042
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 1043
    new-instance v5, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v6, "unsetDescription - unsetting file description failed"

    invoke-direct {v5, v6}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v5
.end method

.method public updateRepository(Lcom/samsung/scloud/data/Repository;)Lcom/samsung/scloud/data/Repository;
    .locals 1
    .param p1, "repo"    # Lcom/samsung/scloud/data/Repository;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2252
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method

.method public updateRepository(Ljava/lang/String;J)Lcom/samsung/scloud/data/Repository;
    .locals 1
    .param p1, "repositoryName"    # Ljava/lang/String;
    .param p2, "spaceAmount"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudException;
        }
    .end annotation

    .prologue
    .line 2244
    new-instance v0, Lcom/samsung/scloud/exception/SCloudNotSupportedException;

    invoke-direct {v0}, Lcom/samsung/scloud/exception/SCloudNotSupportedException;-><init>()V

    throw v0
.end method
