.class public Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;
.super Ljava/lang/Object;
.source "DropboxAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/scloud/DropboxAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CancelableThumbnail"
.end annotation


# instance fields
.field private doRetryThumb:Z

.field private localFile:Ljava/io/File;

.field private progressListener:Lcom/samsung/scloud/response/ProgressListener;

.field private serverFilePath:Ljava/lang/String;

.field final synthetic this$0:Lcom/samsung/scloud/DropboxAPI;

.field private thumb:Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;

.field private thumbnail_format:I

.field private thumbnail_type:I


# direct methods
.method public constructor <init>(Lcom/samsung/scloud/DropboxAPI;Ljava/io/File;Ljava/lang/String;IILcom/samsung/scloud/response/ProgressListener;)V
    .locals 1
    .param p2, "localFile"    # Ljava/io/File;
    .param p3, "serverFilePath"    # Ljava/lang/String;
    .param p4, "thumbnail_type"    # I
    .param p5, "thumbnail_format"    # I
    .param p6, "progressListener"    # Lcom/samsung/scloud/response/ProgressListener;

    .prologue
    .line 2747
    iput-object p1, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->this$0:Lcom/samsung/scloud/DropboxAPI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2738
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->doRetryThumb:Z

    .line 2748
    iput-object p2, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->localFile:Ljava/io/File;

    .line 2749
    iput-object p3, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->serverFilePath:Ljava/lang/String;

    .line 2750
    iput p4, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->thumbnail_type:I

    .line 2751
    iput p5, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->thumbnail_format:I

    .line 2752
    iput-object p6, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->progressListener:Lcom/samsung/scloud/response/ProgressListener;

    .line 2753
    return-void
.end method

.method private getThumbCancelable(Ljava/lang/String;Ljava/io/OutputStream;Lcom/dropbox/client2/DropboxAPI$ThumbSize;Lcom/dropbox/client2/DropboxAPI$ThumbFormat;Lcom/dropbox/client2/ProgressListener;)Lcom/dropbox/client2/DropboxAPI$DropboxFileInfo;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "os"    # Ljava/io/OutputStream;
    .param p3, "size"    # Lcom/dropbox/client2/DropboxAPI$ThumbSize;
    .param p4, "format"    # Lcom/dropbox/client2/DropboxAPI$ThumbFormat;
    .param p5, "listener"    # Lcom/dropbox/client2/ProgressListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dropbox/client2/exception/DropboxException;
        }
    .end annotation

    .prologue
    .line 2833
    iget-object v0, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->this$0:Lcom/samsung/scloud/DropboxAPI;

    # getter for: Lcom/samsung/scloud/DropboxAPI;->dAPI:Lcom/dropbox/client2/SamsungDropboxAPI;
    invoke-static {v0}, Lcom/samsung/scloud/DropboxAPI;->access$600(Lcom/samsung/scloud/DropboxAPI;)Lcom/dropbox/client2/SamsungDropboxAPI;

    move-result-object v0

    invoke-virtual {v0, p1, p3, p4}, Lcom/dropbox/client2/SamsungDropboxAPI;->getThumbnailStream(Ljava/lang/String;Lcom/dropbox/client2/DropboxAPI$ThumbSize;Lcom/dropbox/client2/DropboxAPI$ThumbFormat;)Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->thumb:Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;

    .line 2834
    iget-boolean v0, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->doRetryThumb:Z

    if-nez v0, :cond_0

    .line 2835
    invoke-virtual {p0}, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->cancelThumbnail()Z

    .line 2836
    :cond_0
    iget-object v0, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->thumb:Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;

    invoke-virtual {v0, p2, p5}, Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;->copyStreamToOutput(Ljava/io/OutputStream;Lcom/dropbox/client2/ProgressListener;)V

    .line 2837
    iget-object v0, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->thumb:Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;

    invoke-virtual {v0}, Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;->getFileInfo()Lcom/dropbox/client2/DropboxAPI$DropboxFileInfo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public cancelThumbnail()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2842
    const/4 v2, 0x0

    :try_start_0
    iput-boolean v2, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->doRetryThumb:Z

    .line 2843
    iget-object v2, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->thumb:Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;

    if-eqz v2, :cond_0

    .line 2844
    iget-object v2, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->thumb:Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;

    invoke-virtual {v2}, Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2845
    const/4 v1, 0x1

    .line 2850
    :cond_0
    :goto_0
    return v1

    .line 2847
    :catch_0
    move-exception v0

    .line 2848
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public getThumbnailCancelable()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/scloud/exception/SCloudInvalidParameterException;,
            Lcom/samsung/scloud/exception/SCloudAuthException;,
            Lcom/samsung/scloud/exception/SCloudIOException;,
            Lcom/samsung/scloud/exception/SCloudCancelException;
        }
    .end annotation

    .prologue
    .line 2760
    iget v2, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->thumbnail_format:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 2761
    sget-object v6, Lcom/dropbox/client2/DropboxAPI$ThumbFormat;->PNG:Lcom/dropbox/client2/DropboxAPI$ThumbFormat;

    .line 2769
    .local v6, "format":Lcom/dropbox/client2/DropboxAPI$ThumbFormat;
    :goto_0
    iget-object v2, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->this$0:Lcom/samsung/scloud/DropboxAPI;

    iget v3, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->thumbnail_type:I

    # invokes: Lcom/samsung/scloud/DropboxAPI;->getDropboxThumbSize(I)Lcom/dropbox/client2/DropboxAPI$ThumbSize;
    invoke-static {v2, v3}, Lcom/samsung/scloud/DropboxAPI;->access$1000(Lcom/samsung/scloud/DropboxAPI;I)Lcom/dropbox/client2/DropboxAPI$ThumbSize;

    move-result-object v5

    .line 2771
    .local v5, "thumbSize":Lcom/dropbox/client2/DropboxAPI$ThumbSize;
    const/4 v4, 0x0

    .line 2773
    .local v4, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    iget-object v2, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->localFile:Ljava/io/File;

    const/4 v3, 0x0

    invoke-direct {v4, v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2778
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    iget-object v2, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->localFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    .line 2779
    .local v9, "filename":Ljava/lang/String;
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    const/4 v2, 0x5

    if-ge v10, v2, :cond_5

    .line 2780
    iget-boolean v2, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->doRetryThumb:Z

    if-nez v2, :cond_2

    .line 2781
    new-instance v2, Lcom/samsung/scloud/exception/SCloudCancelException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "file download cancelled"

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v11, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->serverFilePath:Ljava/lang/String;

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/scloud/exception/SCloudCancelException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2762
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .end local v5    # "thumbSize":Lcom/dropbox/client2/DropboxAPI$ThumbSize;
    .end local v6    # "format":Lcom/dropbox/client2/DropboxAPI$ThumbFormat;
    .end local v9    # "filename":Ljava/lang/String;
    .end local v10    # "i":I
    :cond_0
    iget v2, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->thumbnail_format:I

    if-nez v2, :cond_1

    .line 2763
    sget-object v6, Lcom/dropbox/client2/DropboxAPI$ThumbFormat;->JPEG:Lcom/dropbox/client2/DropboxAPI$ThumbFormat;

    .restart local v6    # "format":Lcom/dropbox/client2/DropboxAPI$ThumbFormat;
    goto :goto_0

    .line 2765
    .end local v6    # "format":Lcom/dropbox/client2/DropboxAPI$ThumbFormat;
    :cond_1
    new-instance v2, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;

    const-string v3, "wrong thumbnail type"

    invoke-direct {v2, v3}, Lcom/samsung/scloud/exception/SCloudInvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2774
    .restart local v5    # "thumbSize":Lcom/dropbox/client2/DropboxAPI$ThumbSize;
    .restart local v6    # "format":Lcom/dropbox/client2/DropboxAPI$ThumbFormat;
    :catch_0
    move-exception v8

    .line 2775
    .local v8, "e":Ljava/lang/Exception;
    new-instance v2, Lcom/samsung/scloud/exception/SCloudIOException;

    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2786
    .end local v8    # "e":Ljava/lang/Exception;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "filename":Ljava/lang/String;
    .restart local v10    # "i":I
    :cond_2
    :try_start_1
    new-instance v7, Lcom/samsung/scloud/DropboxAPI$ThumbnailProgressListener;

    iget-object v2, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->progressListener:Lcom/samsung/scloud/response/ProgressListener;

    invoke-direct {v7, v2, v9}, Lcom/samsung/scloud/DropboxAPI$ThumbnailProgressListener;-><init>(Lcom/samsung/scloud/response/ProgressListener;Ljava/lang/String;)V

    .line 2788
    .local v7, "pListener":Lcom/samsung/scloud/DropboxAPI$ThumbnailProgressListener;
    if-eqz v5, :cond_3

    .line 2789
    iget-object v3, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->serverFilePath:Ljava/lang/String;

    move-object v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->getThumbCancelable(Ljava/lang/String;Ljava/io/OutputStream;Lcom/dropbox/client2/DropboxAPI$ThumbSize;Lcom/dropbox/client2/DropboxAPI$ThumbFormat;Lcom/dropbox/client2/ProgressListener;)Lcom/dropbox/client2/DropboxAPI$DropboxFileInfo;

    .line 2792
    iget-object v2, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->progressListener:Lcom/samsung/scloud/response/ProgressListener;

    if-eqz v2, :cond_3

    .line 2793
    iget-object v2, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->progressListener:Lcom/samsung/scloud/response/ProgressListener;

    iget-object v3, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->localFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    const/4 v11, 0x2

    const-wide/16 v12, 0x64

    invoke-interface {v2, v3, v11, v12, v13}, Lcom/samsung/scloud/response/ProgressListener;->onUpdate(Ljava/lang/String;IJ)V

    .line 2799
    :cond_3
    if-eqz v4, :cond_4

    .line 2800
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2817
    :cond_4
    if-eqz v4, :cond_5

    .line 2819
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 2826
    .end local v7    # "pListener":Lcom/samsung/scloud/DropboxAPI$ThumbnailProgressListener;
    :cond_5
    :goto_2
    return-void

    .line 2820
    .restart local v7    # "pListener":Lcom/samsung/scloud/DropboxAPI$ThumbnailProgressListener;
    :catch_1
    move-exception v8

    .line 2821
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 2803
    .end local v7    # "pListener":Lcom/samsung/scloud/DropboxAPI$ThumbnailProgressListener;
    .end local v8    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v8

    .line 2804
    .local v8, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    :try_start_3
    iput-object v2, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->thumb:Lcom/dropbox/client2/DropboxAPI$DropboxInputStream;

    .line 2805
    instance-of v2, v8, Lcom/dropbox/client2/exception/DropboxServerException;

    if-eqz v2, :cond_7

    .line 2806
    move-object v0, v8

    check-cast v0, Lcom/dropbox/client2/exception/DropboxServerException;

    move-object v2, v0

    iget v2, v2, Lcom/dropbox/client2/exception/DropboxServerException;->error:I

    const/16 v3, 0x19f

    if-ne v2, v3, :cond_7

    .line 2807
    new-instance v2, Lcom/samsung/scloud/exception/SCloudIOException;

    const-string v3, "no thumbnail"

    invoke-direct {v2, v3}, Lcom/samsung/scloud/exception/SCloudIOException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2817
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v4, :cond_6

    .line 2819
    :try_start_4
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    .line 2822
    :cond_6
    :goto_3
    throw v2

    .line 2811
    .restart local v8    # "e":Ljava/lang/Exception;
    :cond_7
    :try_start_5
    iget-object v2, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->this$0:Lcom/samsung/scloud/DropboxAPI;

    # invokes: Lcom/samsung/scloud/DropboxAPI;->isRetryNeeded(Ljava/lang/Exception;)Z
    invoke-static {v2, v8}, Lcom/samsung/scloud/DropboxAPI;->access$300(Lcom/samsung/scloud/DropboxAPI;Ljava/lang/Exception;)Z

    move-result v2

    if-eqz v2, :cond_9

    const/4 v2, 0x4

    if-ge v10, v2, :cond_9

    .line 2812
    iget-object v2, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->this$0:Lcom/samsung/scloud/DropboxAPI;

    # invokes: Lcom/samsung/scloud/DropboxAPI;->doBackOff(I)V
    invoke-static {v2, v10}, Lcom/samsung/scloud/DropboxAPI;->access$400(Lcom/samsung/scloud/DropboxAPI;I)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2817
    if-eqz v4, :cond_8

    .line 2819
    :try_start_6
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 2779
    .end local v8    # "e":Ljava/lang/Exception;
    :cond_8
    :goto_4
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    .line 2820
    .restart local v8    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v8

    .line 2821
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 2815
    .local v8, "e":Ljava/lang/Exception;
    :cond_9
    :try_start_7
    iget-object v2, p0, Lcom/samsung/scloud/DropboxAPI$CancelableThumbnail;->this$0:Lcom/samsung/scloud/DropboxAPI;

    # invokes: Lcom/samsung/scloud/DropboxAPI;->handleDropboxExceptions(Ljava/lang/Exception;)V
    invoke-static {v2, v8}, Lcom/samsung/scloud/DropboxAPI;->access$500(Lcom/samsung/scloud/DropboxAPI;Ljava/lang/Exception;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 2817
    if-eqz v4, :cond_8

    .line 2819
    :try_start_8
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    goto :goto_4

    .line 2820
    :catch_4
    move-exception v8

    .line 2821
    .local v8, "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 2820
    .end local v8    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v8

    .line 2821
    .restart local v8    # "e":Ljava/io/IOException;
    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3
.end method
