.class public Lcom/samsung/scloud/exception/SCloudAuthException;
.super Lcom/samsung/scloud/exception/SCloudException;
.source "SCloudAuthException.java"


# static fields
.field public static final SERVER_ACCOUNT_INFO_CHANGED:Ljava/lang/String; = "server account info changed"

.field public static final SERVER_TOKEN_EXPIRED:Ljava/lang/String; = "server token expired"

.field public static final SERVER_TOKEN_INVALID:Ljava/lang/String; = "server token invalid"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/samsung/scloud/exception/SCloudException;-><init>()V

    .line 18
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "cause"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/samsung/scloud/exception/SCloudException;-><init>(Ljava/lang/String;)V

    .line 15
    return-void
.end method


# virtual methods
.method public getExceptionCause()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    invoke-super {p0}, Lcom/samsung/scloud/exception/SCloudException;->getExceptionCause()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
