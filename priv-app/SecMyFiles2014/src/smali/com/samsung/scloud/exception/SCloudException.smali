.class public Lcom/samsung/scloud/exception/SCloudException;
.super Ljava/lang/Exception;
.source "SCloudException.java"


# static fields
.field private static final serialVersionUID:J = 0x21bcf0a6afa131d1L


# instance fields
.field cause:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/scloud/exception/SCloudException;->cause:Ljava/lang/String;

    .line 40
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "cause"    # Ljava/lang/String;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/scloud/exception/SCloudException;->cause:Ljava/lang/String;

    .line 35
    iput-object p1, p0, Lcom/samsung/scloud/exception/SCloudException;->cause:Ljava/lang/String;

    .line 36
    return-void
.end method


# virtual methods
.method public getExceptionCause()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/scloud/exception/SCloudException;->cause:Ljava/lang/String;

    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/scloud/exception/SCloudException;->cause:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/scloud/exception/SCloudException;->cause:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-super {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 46
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/scloud/exception/SCloudException;->cause:Ljava/lang/String;

    goto :goto_0
.end method

.method public setExceptionCause(Ljava/lang/String;)V
    .locals 0
    .param p1, "cause"    # Ljava/lang/String;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/samsung/scloud/exception/SCloudException;->cause:Ljava/lang/String;

    .line 32
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/scloud/exception/SCloudException;->cause:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/scloud/exception/SCloudException;->cause:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-super {p0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    .line 52
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/scloud/exception/SCloudException;->cause:Ljava/lang/String;

    goto :goto_0
.end method
