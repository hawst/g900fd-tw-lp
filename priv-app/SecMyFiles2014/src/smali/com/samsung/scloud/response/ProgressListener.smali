.class public interface abstract Lcom/samsung/scloud/response/ProgressListener;
.super Ljava/lang/Object;
.source "ProgressListener.java"

# interfaces
.implements Lcom/samsung/scloud/response/SCloudResponseListener;


# static fields
.field public static final SC_INTERNAL_SERVER_ERROR:I = 0x4

.field public static final SERVICE_UNAVAILABLE:I = 0x3

.field public static final STATUS_TRANSFER_COMPLETE:I = 0x2

.field public static final STATUS_TRANSFER_ONGOING:I = 0x1

.field public static final STATUS_TRY_TO_START:I


# virtual methods
.method public abstract onUpdate(Ljava/lang/String;IJ)V
.end method
